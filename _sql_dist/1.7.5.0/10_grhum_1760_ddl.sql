SET DEFINE OFF;


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paye is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(moisordre number, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paf is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

function getMoisCompletTexte(borId integer) return varchar;

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(lemois varchar, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/


CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Paf IS

/*
PARISDESCARTES 
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paye IS


PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
mois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
WHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paye.jefy_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois_ordre = moisordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr?par? les ?critures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D';

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C';

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paye.set_bord_brouillard_visa(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_retenues(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_sacd(borid);

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, moiscomplet,NULL);

  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paye.set_bord_brouillard_paiement(moisordre, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -

 UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
    WHERE ges_code=tmpBordereau.ges_code
      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';
/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet jefy_paye.paye_mois.mois_complet%TYPE;
cpt INTEGER;
mois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
WHERE mois_ordre  = moisordre;


SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT SALAIRES '||moiscomplet;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT SALAIRES',
 currentecriture.pco_num,
 'PAIEMENT SALAIRES '||moiscomplet,
 moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from jefy_paye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.JEFY_ECRITURES WHERE mois_ordre = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||currentbordereau.ges_code||' , moisordre : '||mois);


  OPEN ecriturescredit64(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'VISA SALAIRES',
 currentecriture.pco_num,
 'VISA SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecriturescredit64;

END;



PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN

-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(lemandat.man_ttc),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,              --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

SELECT COUNT(*) INTO cpt
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

IF (cpt = 1 )      -- Bulletins n�gatifs, on va chercher la contrepartie dans jefy_paye.jefy_ecritures_reversement
THEN

  SELECT PCO_NUM_CONTREPARTIE INTO classe4
  FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
  WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

ELSE                -- OR Manuel

  classe4 := jefy_paye.get_contrepartie(classe6, depid);

  IF (classe4 IS NULL)
  THEN

        SELECT pco_num_ctrepartie INTO classe4
      FROM PLANCO_VISA WHERE pco_num_ordonnateur = classe6 and exe_ordre = lemandat.exe_ordre;

  END IF;

END IF;

  -- creation du brouillard DEBITEUR CLASSE 4 !
  INSERT INTO MANDAT_BROUILLARD VALUES
   (
     NULL,          --ECD_ORDRE,
     lemandat.exe_ordre,                     --EXE_ORDRE,
     lemandat.ges_code,              --GES_CODE,
     ABS(montant),                                 --MAB_MONTANT,
     'VISA MANDAT',       --MAB_OPERATION,
     mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
     'D',         --MAB_SENS,
     manid,        --MAN_ID,
     classe4             --PCO_NU
  );

 END LOOP;
 CLOSE plancos;

END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- recup du dpcoid de ce mandat papaye : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)
SELECT PCO_NUM_CONTREPARTIE,e.pco_num ,ecr_mont INTO classe4, classe6 , montant
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;


-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);

END;


-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES SALAIRES',
 currentecriture.pco_num,
 'RETENUES SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois AND ecr_comp = lacomp AND ecr_type='18';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'SACD SALAIRES',
 currentecriture.pco_num,
 'SACD SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituressacd;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA SALAIRES',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paf IS

function getMoisCompletTexte(borId integer) return varchar
-- renvoie le mois sous le forme JANVIER 2009 (a partir d un bordereau PAF)
is
    exeOrdre number;
    moisComplet varchar2(50);
    lemois varchar2(3);
    leMoisNum integer;
    lemoisTxt varchar2(20);
begin
    SELECT DISTINCT exe_ordre   INTO exeordre
    FROM maracuja.bordereau m
    WHERE m.bor_id = borid;
    
    
    SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    --lemois := lpad(to_char(to_number(lemois)),2,'0' );
    leMoisNum := to_number(lemois);
    
 --   SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
  --  WHERE mois_code = exeOrdre || lemois;

    if leMoisNum = 1 then
       lemoisTxt := 'JANVIER'; 
    elsif leMoisNum = 2 then
       lemoisTxt := 'FEVRIER'; 
    elsif leMoisNum = 3 then
       lemoisTxt := 'MARS'; 
    elsif leMoisNum = 4 then
       lemoisTxt := 'AVRIL';     
    elsif leMoisNum = 5 then
       lemoisTxt := 'MAI';           
    elsif leMoisNum = 6 then
       lemoisTxt := 'JUIN';     
    elsif leMoisNum = 7 then
       lemoisTxt := 'JUILLET'; 
    elsif leMoisNum = 8 then
       lemoisTxt := 'AOUT'; 
    elsif leMoisNum = 9 then
       lemoisTxt := 'SEPTEMBRE'; 
    elsif leMoisNum = 10 then
       lemoisTxt := 'OCTOBRE'; 
    elsif leMoisNum = 11 then
       lemoisTxt := 'NOVEMBRE'; 
    elsif leMoisNum = 12 then
       lemoisTxt := 'DECEMBRE'; 
    end if;                                                     
     
    return lemoisTxt||' '||exeordre;    
end;

PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
lemois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
--fredSELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
--fredWHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paf.paf_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois = lemois and exe_ordre = tmpBordereau.exe_ordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas prepare les ecritures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D'
 and exe_ordre = tmpBordereau.exe_ordre;

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C'
 and exe_ordre = tmpBordereau.exe_ordre;

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paf.set_bord_brouillard_visa(borid);
   --Bordereau_Abricot_Paf.set_bord_brouillard_retenues(borid);
   --Bordereau_Abricot_Paf.set_bord_brouillard_sacd(borid);

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, getMoisCompletTexte(borId),NULL);
  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paf.set_bord_brouillard_paiement(lemois, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -
  update jefy_paf.paf_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois=lemois and exe_ordre=tmpBordereau.exe_ordre and liq_ETAT='LIQUIDEE';

--fred UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
--fred    WHERE ges_code=tmpBordereau.ges_code
--fred      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';

 UPDATE jefy_paf.paf_etape SET bor_id=borid, pae_etat='MANDATEE', mois_libelle = getMoisCompletTexte(borid)
    WHERE ges_code=tmpBordereau.ges_code
        and  mois=lemois and exe_ordre=tmpBordereau.exe_ordre
        AND pae_ETAT='LIQUIDEE';



/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

select 1 into cpt from dual;
/*
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;
*/
END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
select 1 into cpt from dual;

/*
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;
*/

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(lemois varchar, borid NUMBER, exeordre NUMBER)
IS


currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet VARCHAR2(50);
mois2 varchar2(50);

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paf.paf_ecritures WHERE mois = lemois AND ecr_type='45' and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
--fred lemois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
--fred SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
--fred WHERE mois_ordre  = moisordre;
SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

mois2 := getMoisCompletTexte(borid);


--SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
--WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||moiscomplet;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||mois2;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT PAF',
 currentecriture.pco_num,
 'PAIEMENT PAF '||mois2, -- 'PAIEMENT PAF '||moiscomplet,
 mois2, --moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from jefy_paye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';



currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ECRITURES WHERE mois = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ))
and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
moisordre INTEGER;
lemoiscomplet varchar2(50);
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

--fred moislibelle jefy_paf.paye_mois.mois_complet%TYPE;
lemois VARCHAR2(50);
mois2 varchar2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT substr(dep_numero,5,3) INTO lemois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;



SELECT DISTINCT dep_numero INTO lemoiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;


-- recup du moiordre
--fred  SELECT mois_ordre INTO moisordre FROM jefy_paf.paye_mois
--WHERE mois_complet = mois;

--fred SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||currentbordereau.ges_code||' , moisordre : '||lemois);

mois2 := getMoisCompletTexte(borid);

  OPEN ecriturescredit64(lemois, currentbordereau.ges_code);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'VISA PAF',
 currentecriture.pco_num,
 'VISA PAF '|| mois2, --'VISA PAF '||lemois,
 mois2, --lemoiscomplet,
 NULL
 );

 END LOOP;
  CLOSE ecriturescredit64;

END;



PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN

-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;
/*
classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(lemandat.man_ttc),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,              --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

SELECT COUNT(*) INTO cpt
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

IF (cpt = 1 )      -- Bulletins n�gatifs, on va chercher la contrepartie dans jefy_paye.jefy_ecritures_reversement
THEN

  SELECT PCO_NUM_CONTREPARTIE INTO classe4
  FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
  WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

ELSE                -- OR Manuel

  classe4 := jefy_paye.get_contrepartie(classe6, depid);

  IF (classe4 IS NULL)
  THEN

        SELECT pco_num_ctrepartie INTO classe4
      FROM PLANCO_VISA WHERE pco_num_ordonnateur = classe6 and exe_ordre = lemandat.exe_ordre;

  END IF;

END IF;

  -- creation du brouillard DEBITEUR CLASSE 4 !
  INSERT INTO MANDAT_BROUILLARD VALUES
   (
     NULL,          --ECD_ORDRE,
     lemandat.exe_ordre,                     --EXE_ORDRE,
     lemandat.ges_code,              --GES_CODE,
     ABS(montant),                                 --MAB_MONTANT,
     'VISA MANDAT',       --MAB_OPERATION,
     mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
     'D',         --MAB_SENS,
     manid,        --MAN_ID,
     classe4             --PCO_NU
  );

 END LOOP;
 CLOSE plancos;

*/
END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;
/*
-- recup du dpcoid de ce mandat papaye : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)
SELECT PCO_NUM_CONTREPARTIE,e.pco_num ,ecr_mont INTO classe4, classe6 , montant
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;


-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);
*/
END;


-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
/*
SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES PAF',
 currentecriture.pco_num,
 'RETENUES PAF '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;
*/
END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;


CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois AND ecr_comp = lacomp AND ecr_type='18'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
mois2 varchar2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
--SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
--WHERE mois_complet = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;
mois2 := getMoisCompletTexte(borid);

  OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'SACD PAF',
 currentecriture.pco_num,
 'SACD PAF '||mois2,
 mois2,
 NULL
 );

 END LOOP;
  CLOSE ecrituressacd;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA PAF',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;


/*
PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;

*/
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT" AS

PROCEDURE creer_bordereau (abrid INTEGER) IS
cpt INTEGER;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
monborid_dep INTEGER;
monborid_rec INTEGER;

cursor lesmandats is
select man_id from mandat where bor_id = monborid_dep;

cursor lestitres is
select tit_id from titre where bor_id = monborid_rec;

tmpmandid integer;
tmptitid integer;
tboordre integer;
BEGIN

-- est ce une selection vide ???
SELECT COUNT(*) INTO cpt
FROM ABRICOT_BORD_SELECTION
where abr_id = abrid;


if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/


-- recup du group by pour traiter les cursors
abrgroupby := recup_groupby(abrid);

IF (abrgroupby = 'bordereau_1R1T') THEN
 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1R1T(abrid,monborid_rec);


END IF;

IF (abrgroupby = 'bordereau_NR1T') THEN
 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_NR1T(abrid,monborid_rec);

-- controle RA
 select count(*) into cpt
 from titre
 where ori_ordre is not null
 and bor_id =monborid_rec;

if cpt != 0 then
  raise_application_error (-20001,'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !');
end if;

END IF;

IF (abrgroupby = 'bordereau_1D1M') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);


END IF;


/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);
 bordereau_1R1T(abrid,monborid_rec);


END IF;
*/

IF (abrgroupby NOT IN ( 'bordereau_1R1T','bordereau_NR1T','bordereau_1D1M','bordereau_1D1M1R1T')) THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_ND1M(abrid,monborid_dep);



END IF;


IF (monborid_dep is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_dep);

 open lesmandats;
 loop
 fetch lesmandats into tmpmandid;
 exit when lesmandats%notfound;
 get_depense_jefy_depense(tmpmandid);
 end loop;
 close lesmandats;

 controle_bordereau(monborid_dep);

END IF;

IF (monborid_rec  is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_rec);

  open lestitres;
 loop
 fetch lestitres into tmptitid;
 exit when lestitres%notfound;
 -- recup du brouillard
 get_recette_jefy_recette(tmptitid);
 Set_Titre_Brouillard(tmptitid);
 Get_recette_prelevements(tmptitid);

 end loop;
 close lestitres;

controle_bordereau(monborid_rec);
END IF;

-- maj de l etat dans la selection
IF (monborid_dep is not null OR monborid_rec  is not null) then

 if monborid_rec  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_rec
  WHERE abr_id = abrid;
 end if;

 if monborid_dep  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_dep
  WHERE abr_id = abrid;

  select tbo_ordre into tboordre from bordereau where bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
  if tboordre = 3  then
   bordereau_abricot_paye.basculer_bouillard_paye(monborid_dep);
  end if;


-- pour les bordereaux d'orv de papaye on retravaille le brouillard
  if tboordre = 18  then
   bordereau_abricot_paye.basculer_bouillard_paye_orv(monborid_dep);
  end if;


-- pour les bordereaux de regul de papaye on retravaille le brouillard
  if tboordre = 19  then
   bordereau_abricot_paye.basculer_bouillard_paye_regul(monborid_dep);
  end if;

-- pour les bordereaux de PAF on retravaille le brouillard
 if tboordre = 20  then
   bordereau_abricot_paf.basculer_bouillard_paye(monborid_dep);
  end if;


-- pour les bordereaux d'orv de PAF on retravaille le brouillard
-- if tboordre = 21  then
--  bordereau_abricot_paf.basculer_bouillard_paye_orv(monborid_dep);
--  end if;


-- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;


 end if;

end if;


end if;


END;

PROCEDURE viser_bordereau_rejet (brjordre INTEGER) IS
cpt INTEGER;
manid maracuja.mandat.man_id%type;
titid maracuja.titre.tit_id%type;
tboordre integer;

reduction integer;
utlordre integer;
dpcoid integer;
depsuppression varchar2(20);
rpcoid integer;
recsuppression varchar2(20);
exeordre integer;

cursor mandats is
select man_id from  maracuja.mandat where brj_ordre = brjordre;

cursor depenses is
select dep_ordre,dep_suppression from  maracuja.depense where man_id = manid;


cursor titres is
select tit_id from  maracuja.titre where brj_ordre = brjordre;

cursor recettes is
select rec_ordre,rec_suppression from  maracuja.recette where tit_id = titid;

deliq integer;
BEGIN

open mandats;
loop
fetch mandats into manid;
exit when mandats%notfound;

open depenses;
loop
fetch depenses into dpcoid,depsuppression;
exit when depenses%notfound;
 -- casser le liens des mand_id dans depense_ctrl_planco
   -- supprimer le liens compteble <-> depense dans l inventaire
 jefy_depense.ABRICOT.upd_depense_ctrl_planco (dpcoid,null);

select tbo_ordre,exe_ordre into tboordre,exeordre from jefy_depense.depense_ctrl_planco
where dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 201
 if depsuppression = 'OUI' and  tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
  deliq:=jefy_depense.Get_Fonction('DELIQ');

  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
  uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

    if utlordre is null then
    deliq:=jefy_depense.Get_Fonction('DELIQINV');

  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre
  AND ufe.exe_ordre=exeordre
  AND uf.fon_ordre=deliq
  AND ufe.exe_ordre=e.exe_ordre
  AND exe_stat_eng='R';
  end if;
*/
 select utl_ordre into utlordre from jefy_depense.depense_budget
    where dep_id in (select dep_id from jefy_depense.depense_ctrl_planco where dpco_id=dpcoid);

  jefy_depense.ABRICOT.del_depense_ctrl_planco (dpcoid,utlordre);
 end if;

end loop;
close depenses;

end loop;
close mandats;

open titres;
loop
fetch titres into titid;
exit when titres%notfound;

open recettes;
loop
fetch recettes into rpcoid,recsuppression;
exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
 SELECT r.rec_id_reduction INTO reduction
 FROM jefy_recette.RECETTE r, jefy_recette.RECETTE_CTRL_PLANCO rpco
 WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

 IF reduction IS NOT NULL THEN
  jefy_recette.API.upd_reduction_ctrl_planco (rpcoid,null);
 ELSE
  jefy_recette.API.upd_recette_ctrl_planco (rpcoid,null);
 END IF;

-- PAS DE GESTION DES SUPPRESSIONS

end loop;
close recettes;


end loop;
close titres;

-- on passe le brjordre a VISE
update bordereau_rejet set brj_etat = 'VISE'
where brj_ordre = brjordre;

END;



function get_selection_id (info varchar ) return integer is
selection integer;
begin
select
maracuja.ABRICOT_BORD_SELECTION_SEQ.nextval into selection from dual;
return selection;
end;


function get_selection_borid (abrid integer) return integer is
borid integer;
begin
select distinct bor_id into borid from maracuja.ABRICOT_BORD_SELECTION where abr_id = abrid;
return borid;
end;


procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar) is

chaine varchar(32000);
premier integer;
tmpdepid integer;
tmprecid integer;
cpt integer;

begin
/*
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

-- traitement de la chaine des depid
if a02lesdepid is not null OR length(a02lesdepid) >0 then
chaine := a02lesdepid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdepid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
tmpdepid,--DEP_ID
null,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

 -- traitement de la chaine des recid
if a03lesrecid is not null OR length(a03lesrecid) >0 then
chaine := a03lesrecid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmprecid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
null,--DEP_ID
tmprecid,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

select count(*) into cpt
from jefy_depense.depense_ctrl_planco
where dpco_id in ( select DEP_ID from abricot_bord_selection where abr_id = a01abrid)
and man_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
end if;


select count(*) into cpt
from jefy_recette.recette_ctrl_planco
where rpco_id in ( select REC_ID from abricot_bord_selection where abr_id = a01abrid)
and tit_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
end if;


bordereau_abricot.creer_bordereau(a01abrid);
end;


procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
begin

-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

-- les mandats
set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,201 ,'bordereau_1D1M' ,a08gescodemandat );

-- les titres
set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
boridtmp integer;
moisordre integer;
begin
/*
 -- a07abrgroupby = mois
 select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

 -- CONTROLES
 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

 if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

 select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

 if (cpt = 1) then
  raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
 end if;
*/
-- ATENTION
-- tboordre : 3 salaires

-- les mandats de papaye
 set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,3 ,'bordereau_1D1M' ,a08gescodemandat );
 boridtmp :=get_selection_borid (a01abrid);

 /*
-- maj de l etat de papaye_compta et du bor_ordre -
 update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

-- bascule du brouillard de papaye


-- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


-- creer bordereau (tbo_ordre) + numerotation
FUNCTION get_num_borid (tboordre INTEGER,exeordre INTEGER,gescode VARCHAR,utlordre INTEGER ) RETURN INTEGER IS
cpt INTEGER;
borid INTEGER;
bornum INTEGER;
BEGIN
-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
bornum :=-1;
INSERT INTO BORDEREAU
(
BOR_DATE_VISA,
BOR_ETAT,
BOR_ID,
BOR_NUM,
BOR_ORDRE,
EXE_ORDRE,
GES_CODE,
TBO_ORDRE,
UTL_ORDRE,
UTL_ORDRE_VISA,
BOR_DATE_CREATION
)
VALUES (
NULL ,                         --BOR_DATE_VISA,
'VALIDE',              --BOR_ETAT,
borid,                  --BOR_ID,
bornum,                              --BOR_NUM,
-borid,                             --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
exeordre,                --EXE_ORDRE,
gescode,                          --GES_CODE,
tboordre,              --TBO_ORDRE,
utlordre,               --UTL_ORDRE,
NULL,                  --UTL_ORDRE_VISA
sysdate
);

RETURN borid;

END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
PROCEDURE bordereau_1R1T(abrid INTEGER,monborid INTEGER) IS

cpt INTEGER;
tmprecette jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;

CURSOR rec_tit IS
SELECT r.*
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by r.pco_num ASC;

BEGIN


OPEN rec_tit;
LOOP
FETCH rec_tit INTO tmprecette;
EXIT WHEN rec_tit%NOTFOUND;
cpt:=set_titre_recette(tmprecette.rpco_id,monborid);
END LOOP;
CLOSE rec_tit;

END;

PROCEDURE bordereau_NR1T(abrid INTEGER,monborid INTEGER) IS

ht  number(12,2);
tva number(12,2);
ttc number(12,2);
pconumero varchar(20);
nbpieces integer;

cpt INTEGER;
titidtemp integer;
tmprecette jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;

-- curseur de regroupement
CURSOR rec_tit_group_by IS
SELECT
r.PCO_NUM ,
sum(r.RPCO_HT_SAISIE) ,
sum(r.RPCO_TVA_SAISIE) ,
sum(r.RPCO_TTC_SAISIE)
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
group by r.PCO_NUM
order by r.pco_num ASC;


CURSOR rec_tit IS
SELECT r.*
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and r.pco_num = pconumero
order by r.pco_num ASC;

BEGIN

open rec_tit_group_by;
loop
FETCH rec_tit_group_by INTO pconumero,ht,tva,ttc;
EXIT WHEN rec_tit_group_by%NOTFOUND;

titidtemp := 0;
OPEN rec_tit;
LOOP
FETCH rec_tit INTO tmprecette;
EXIT WHEN rec_tit%NOTFOUND;
 if titidtemp = 0 then
  titidtemp:=set_titre_recette(tmprecette.rpco_id,monborid);
 else
  update jefy_recette.RECETTE_CTRL_PLANCO
  set tit_id =titidtemp
  where rpco_id = tmprecette.rpco_id;
 end if;

END LOOP;
CLOSE rec_tit;

-- recup du nombre de pieces
-- TODO
 nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

-- maj des montants du titre
 update titre set
  TIT_HT = ht,
  TIT_NB_PIECE = nbpieces,
  TIT_TTC = ttc,
  TIT_TVA = tva,
  TIT_LIBELLE = 'TITRE COLLECTIF'
 where tit_id =titidtemp;

-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);

end loop;
close rec_tit_group_by;

END;

PROCEDURE bordereau_ND1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;

tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
CURSOR mand_dep_convra IS
SELECT distinct d.*
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.engage_budget e,
maracuja.V_CONVENTION_LIMITATIVE c
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND  db.dep_id = d.dep_id
AND e.eng_id = db.eng_id
AND e.org_id = c.org_id (+)
AND e.exe_ordre = c.exe_ordre (+)
AND c.org_id IS NOT NULL
and d.man_id is null
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;


-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.


BEGIN


OPEN mand_dep_convra;
LOOP
FETCH mand_dep_convra INTO tmpdepense;
EXIT WHEN mand_dep_convra%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco_mod'  ) THEN
 cpt:=ndep_mand_org_fou_rib_pco_mod(abrid ,monborid );
END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_fou_rib_pco_mod') THEN
 cpt:=ndep_mand_fou_rib_pco_mod(abrid ,monborid );
END IF;


END;

PROCEDURE bordereau_1D1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;
tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;


CURSOR dep_mand IS
SELECT d.*
FROM ABRICOT_BORD_SELECTION ab,jefy_depense.depense_CTRL_PLANCO d
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;

BEGIN


OPEN dep_mand;
LOOP
FETCH dep_mand INTO tmpdepense;
EXIT WHEN dep_mand%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE dep_mand;

END;

PROCEDURE bordereau_1D1M1R1T(abrid INTEGER,boridep INTEGER,boridrec INTEGER) IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
bordereau_1D1M(abrid,boridep);
bordereau_1R1T(abrid,boridrec);


END;


-- les mandats et titres


FUNCTION set_mandat_depense (dpcoid INTEGER,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;


ladepense       jefy_depense.depense_ctrl_planco%ROWTYPE;
ladepensepapier jefy_depense.depense_papier%ROWTYPE;
leengagebudget  jefy_depense.engage_budget%ROWTYPE;

gescode     GESTION.ges_code%TYPE;
manid         MANDAT.man_id%TYPE;

MANORGINE_KEY   MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB  MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE     MANDAT.ORI_ORDRE%TYPE;
PRESTID     MANDAT.PREST_ID%TYPE;
TORORDRE     MANDAT.TOR_ORDRE%TYPE;
VIRORDRE     MANDAT.PAI_ORDRE%TYPE;
MANNUMERO       MANDAT.MAN_NUMERO%TYPE;
BEGIN

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

SELECT * INTO ladepense
FROM  jefy_depense.depense_ctrl_planco
WHERE dpco_id = dpcoid;

SELECT DISTINCT dpp.* INTO ladepensepapier
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and dpco_id = dpcoid;

SELECT eb.* INTO leengagebudget
FROM jefy_depense.ENGAGE_BUDGET eb,jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.eng_id = eb.eng_id
and  db.dep_id = dpco.dep_id
and dpco_id = dpcoid;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(leengagebudget.org_id,leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.dep_id = ladepense.dep_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 and d.dep_id = ladepense.dep_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- recup du numero de mandat
MANNUMERO := -1;
INSERT INTO MANDAT
(
BOR_ID,
BRJ_ORDRE,
EXE_ORDRE,
FOU_ORDRE,
GES_CODE,
MAN_DATE_REMISE,
MAN_DATE_VISA_PRINC,
MAN_ETAT,
MAN_ETAT_REMISE,
MAN_HT,
MAN_ID,
MAN_MOTIF_REJET,
MAN_NB_PIECE,
MAN_NUMERO,
MAN_NUMERO_REJET,
MAN_ORDRE,
MAN_ORGINE_KEY,
MAN_ORIGINE_LIB,
MAN_TTC,
MAN_TVA,
MOD_ORDRE,
ORI_ORDRE,
PCO_NUM,
PREST_ID,
TOR_ORDRE,
PAI_ORDRE,
ORG_ORDRE,
RIB_ORDRE_ORDONNATEUR,
RIB_ORDRE_COMPTABLE
)
VALUES
(
borid ,                   --BOR_ID,
NULL,                        --BRJ_ORDRE,
ladepensepapier.exe_ordre,                   --EXE_ORDRE,
ladepensepapier.fou_ordre,--FOU_ORDRE,
gescode,                --GES_CODE,
NULL,                    --MAN_DATE_REMISE,
NULL,                    --MAN_DATE_VISA_PRINC,
'ATTENTE',                --MAN_ETAT,
'ATTENTE',                --MAN_ETAT_REMISE,
ladepense.dpco_montant_budgetaire, --MAN_HT,
manid,                    --MAN_ID,
NULL,                    --MAN_MOTIF_REJET,
ladepensepapier.dpp_nb_piece,--MAN_NB_PIECE,
MANNUMERO,--MAN_NUMERO,
NULL,                    --MAN_NUMERO_REJET,
-manid,--MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
MANORGINE_KEY,            --MAN_ORGINE_KEY,
MANORIGINE_LIB,        --MAN_ORIGINE_LIB,
ladepense.dpco_ttc_saisie, --MAN_TTC,
ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,  --MAN_TVA,
ladepensepapier.mod_ordre,                --MOD_ORDRE,
ORIORDRE,                --ORI_ORDRE,
ladepense.pco_num,    --PCO_NUM,
PRESTID,                --PREST_ID,
TORORDRE,                --TOR_ORDRE,
VIRORDRE,                --VIR_ORDRE
leengagebudget.org_id,  --org_ordre
ladepensepapier.rib_ordre, --rib ordo
ladepensepapier.rib_ordre -- rib_comptable
);

-- maj du man_id  dans la depense
UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
SET man_id = manid
WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

-- recup du brouillard
set_mandat_brouillard(manid);


RETURN manid;

END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
FUNCTION set_mandat_depenses (lesdpcoid VARCHAR,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;
premier integer;
tmpdpcoid integer;
chaine varchar(5000);
premierdpcoid integer;
manid integer;

ttc mandat.man_ttc%type;
tva mandat.man_tva%type;
ht mandat.man_ht%type;

utlordre integer;
nb_pieces integer;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);


premierdpcoid:=null;
 -- traitement de la chaine des depid xx$xx$xx$.....$x$$
if lesdpcoid is not null OR length(lesdpcoid) >0 then
chaine := lesdpcoid;
LOOP
  premier:=1;
  -- On recupere le depid
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdpcoid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;
-- creation du mandat lie au borid
 if premierdpcoid is null then
  manid:=set_mandat_depense(tmpdpcoid,borid);
  -- suppression du brouillard car il est uniquement sur la premiere depense
  delete from mandat_brouillard where man_id = manid;
  premierdpcoid:=tmpdpcoid;
 else
  -- maj du man_id  dans la depense
  UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
  SET man_id = manid
  WHERE dpco_id = tmpdpcoid;

  -- recup de la depense (maracuja)
  SELECT DISTINCT dpp.utl_ordre INTO utlordre
  FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
  WHERE db.dep_id = dpco.dep_id
  AND dpp.dpp_id = db.dpp_id
  and dpco_id = tmpdpcoid;

--  get_depense_jefy_depense(manid,utlordre);

 end if;

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
select
sum(dpco_ttc_saisie),
sum(dpco_ttc_saisie - dpco_montant_budgetaire),
sum(dpco_montant_budgetaire)
into
ttc,
tva,
ht
from jefy_depense.DEPENSE_CTRL_PLANCO
where man_id = manid;

-- recup du nb de pieces
SELECT sum(dpp.dpp_nb_piece) INTO nb_pieces
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and man_id = manid;


-- maj du mandat
update mandat set
man_ht = ht,
man_tva = tva,
man_ttc = ttc,
man_nb_piece = nb_pieces
where man_id = manid;

-- recup du brouillard
set_mandat_brouillard(manid);

RETURN cpt;

END;



FUNCTION set_titre_recette (rpcoid INTEGER,borid INTEGER)  RETURN INTEGER IS

jefytitre       jefy.TITRE%ROWTYPE;
gescode         GESTION.ges_code%TYPE;
titid           TITRE.tit_id%TYPE;

TITORGINEKEY    TITRE.tit_ORGINE_KEY%TYPE;
TITORIGINELIB   TITRE.tit_ORIGINE_LIB%TYPE;
ORIORDRE        TITRE.ORI_ORDRE%TYPE;
PRESTID         TITRE.PREST_ID%TYPE;
TORORDRE        TITRE.TOR_ORDRE%TYPE;
modordre        TITRE.mod_ordre%TYPE;
presid          INTEGER;
cpt             INTEGER;
VIRORDRE        INTEGER;


recettepapier   jefy_recette.recette_papier%ROWTYPE;
recettebudget   jefy_recette.recette_budget%ROWTYPE;
facturebudget   jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco    jefy_recette. RECETTE_CTRL_PLANCO%rowtype;

BEGIN


-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);

SELECT * INTO recettectrlplanco
FROM  jefy_recette. RECETTE_CTRL_PLANCO
WHERE rpco_id = rpcoid;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;


-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
TITORGINEKEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
TITORIGINELIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(factureBUDGET.org_id,factureBUDGET.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.rec_id = recettectrlplanco.rec_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 AND d.rec_id = recettectrlplanco.rec_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

SELECT titre_seq.NEXTVAL INTO titid FROM dual;

        INSERT INTO TITRE (
           BOR_ID,
           BOR_ORDRE,
           BRJ_ORDRE,
           EXE_ORDRE,
           GES_CODE,
           MOD_ORDRE,
           ORI_ORDRE,
           PCO_NUM,
           PREST_ID,
           TIT_DATE_REMISE,
           TIT_DATE_VISA_PRINC,
           TIT_ETAT,
           TIT_ETAT_REMISE,
           TIT_HT,
           TIT_ID,
           TIT_MOTIF_REJET,
           TIT_NB_PIECE,
           TIT_NUMERO,
           TIT_NUMERO_REJET,
           TIT_ORDRE,
           TIT_ORGINE_KEY,
           TIT_ORIGINE_LIB,
           TIT_TTC,
           TIT_TVA,
           TOR_ORDRE,
           UTL_ORDRE,
           ORG_ORDRE,
           FOU_ORDRE,
           MOR_ORDRE,
           PAI_ORDRE,
           rib_ordre_ordonnateur,
           rib_ordre_comptable,
           tit_libelle)
        VALUES
            (
            borid,--BOR_ID,
            -borid,--BOR_ORDRE,
            NULL,--BRJ_ORDRE,
            recettepapier.exe_ordre,--EXE_ORDRE,
            gescode,--GES_CODE,
            null,--MOD_ORDRE, n existe plus en 2007 vestige des ORVs
            oriordre,--ORI_ORDRE,
            recettectrlplanco.pco_num,--PCO_NUM,
            PRESTID,--PREST_ID,
            sysdate,--TIT_DATE_REMISE,
            NULL,--TIT_DATE_VISA_PRINC,
            'ATTENTE',--TIT_ETAT,
            'ATTENTE',--TIT_ETAT_REMISE,
            recettectrlplanco.rpco_HT_SAISIE,--TIT_HT,
            titid,--TIT_ID,
            NULL,--TIT_MOTIF_REJET,
            recettepapier.rpp_nb_piece,--TIT_NB_PIECE,
            -1,--TIT_NUMERO, numerotation en fin de transaction
            NULL,--TIT_NUMERO_REJET,
            -titid,--TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
            TITORGINEKEY,--TIT_ORGINE_KEY,
            TITORIGINELIB,--TIT_ORIGINE_LIB,
            recettectrlplanco.rpco_TTC_SAISIE,--TIT_TTC,
            recettectrlplanco.rpco_TVA_SAISIE,--TIT_TVA,
            TORORDRE,--TOR_ORDRE,
            recettepapier.utl_ordre,--UTL_ORDRE
            facturebudget.org_id,        --ORG_ORDRE,
            recettepapier.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
            facturebudget.mor_ordre,                 --MOR_ORDRE
            NULL, -- VIR_ORDRE
            recettepapier.rib_ordre,
            recettepapier.rib_ordre,
            recettebudget.rec_lib
            );


-- maj du tit_id dans la recette
update jefy_recette.RECETTE_CTRL_PLANCO
set tit_id = titid
where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);

RETURN titid;
END;



FUNCTION set_titre_recettes (lesrpcoid VARCHAR,borid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RAISE_APPLICATION_ERROR (-20001,'OPERATION NON TRAITEE');
RETURN cpt;
END;

/*

FUNCTION ndep_mand_org_fou_rib_pco (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_org_fou_rib_pco IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num;

CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;

CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco;
LOOP
FETCH ndep_mand_org_fou_rib_pco INTO
orgid,fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;
  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco;

RETURN cpt;
END;

*/

FUNCTION ndep_mand_org_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;


CURSOR ndep_mand_org_fou_rib_pco_mod IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_org_fou_rib_pco_mod INTO
orgid,fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco_mod;

RETURN cpt;
END;
/*
FUNCTION ndep_mand_fou_rib_pco  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN

OPEN ndep_mand_fou_rib_pco;
LOOP
FETCH ndep_mand_fou_rib_pco INTO
fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco;

RETURN cpt;
END;
*/

FUNCTION ndep_mand_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco_mod IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;

CURSOR lesdpcoidsnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_fou_rib_pco_mod INTO
fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
   OPEN lesdpcoids;
   LOOP
   FETCH lesdpcoids INTO tmpdpcoid;
   EXIT WHEN lesdpcoids%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoids;
 else
   OPEN lesdpcoidsnull;
   LOOP
   FETCH lesdpcoidsnull INTO tmpdpcoid;
   EXIT WHEN lesdpcoidsnull%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoidsnull;
 end if;

 chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco_mod;


RETURN cpt;
END;

-- procedures de verifications

FUNCTION selection_valide (abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- meme exercice

-- si PI somme recette = somme depense

-- recette_valides

-- depense_valides

RETURN cpt;

END;

FUNCTION recette_valide (recid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

RETURN cpt;

END;

FUNCTION depense_valide (depid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RETURN cpt;

END;

FUNCTION verif_bordereau_selection(borid INTEGER,abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

-- verifier sum TTC recette selection = sum TTC titre du bord

-- verifier sum TTC depense  = sum TTC mandat du bord

-- verifier sum TTC recette  = sum TTC titre  du bord


RETURN cpt;

END;



-- procedures de locks de transaction
PROCEDURE lock_mandats IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE lock_titres IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE get_depense_jefy_depense (manid INTEGER)
IS

    depid               DEPENSE.dep_id%TYPE;
    jefydepensebudget       jefy_depense.depense_budget%ROWTYPE;
        tmpdepensepapier        jefy_depense.depense_papier%ROWTYPE;
        jefydepenseplanco       jefy_depense.depense_ctrl_planco%ROWTYPE;
    lignebudgetaire         DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
    fouadresse              DEPENSE.dep_adresse%TYPE;
    founom          DEPENSE.dep_fournisseur%TYPE;
    lotordre          DEPENSE.dep_lot%TYPE;
    marordre        DEPENSE.dep_marches%TYPE;
    fouordre        DEPENSE.fou_ordre%TYPE;
    gescode            DEPENSE.ges_code%TYPE;
    modordre        DEPENSE.mod_ordre%TYPE;
    cpt                    INTEGER;
    tcdordre        TYPE_CREDIT.TCD_ORDRE%TYPE;
    tcdcode            TYPE_CREDIT.tcd_code%TYPE;
    ecd_ordre_ema            ECRITURE_DETAIL.ecd_ordre%TYPE;
        orgid   integer;

        CURSOR depenses IS
     SELECT db.* FROM jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
         WHERE dpco.man_id = manid
         and db.dep_id = dpco.dep_id;

BEGIN

    OPEN depenses;
    LOOP
        FETCH depenses INTO jefydepensebudget;
              EXIT WHEN depenses%NOTFOUND;

        -- creation du depid --
        SELECT depense_seq.NEXTVAL INTO depid FROM dual;

            -- creation de lignebudgetaire--
            SELECT org_ub||' '||org_cr||' '||org_souscr
                INTO lignebudgetaire
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            --recuperer le type de credit a partir de la commande
            SELECT tcd_ordre INTO tcdordre
                        FROM jefy_depense.engage_budget
                        WHERE eng_id = jefydepensebudget.eng_id;
                        --AND eng_stat !='A'

            SELECT org_ub,org_id
                INTO gescode,orgid
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouadresse --
            SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
                INTO fouadresse
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            -- founom --
            SELECT adr_nom||' '||adr_prenom
                INTO founom
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouordre --
                 SELECT fou_ordre INTO fouordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id;
                 --AND eng_stat !='A'

            -- lotordre --
            SELECT COUNT(*) INTO cpt
                FROM marches.attribution
                WHERE att_ordre =
                (
                 SELECT att_ordre
                 FROM jefy_depense.engage_ctrl_marche
                                 WHERE eng_id = jefydepensebudget.eng_id
                );

             IF cpt = 0 THEN
                  lotordre :=NULL;
             ELSE
                  SELECT lot_ordre
                  INTO lotordre
                  FROM marches.attribution
                  WHERE att_ordre =
                  (
                                   SELECT att_ordre
                   FROM jefy_depense.engage_ctrl_marche
                                   WHERE eng_id = jefydepensebudget.eng_id
                  );
             END IF;


        -- marordre --
        SELECT COUNT(*) INTO cpt
            FROM marches.lot
            WHERE lot_ordre = lotordre;

        IF cpt = 0 THEN
                  marordre :=NULL;
        ELSE
                        SELECT mar_ordre
                        INTO marordre
                        FROM marches.lot
                        WHERE lot_ordre = lotordre;
        END IF;



                --MOD_ORDRE --
                SELECT mod_ordre INTO modordre
                FROM jefy_depense.depense_papier
                WHERE dpp_id =jefydepensebudget.dpp_id;


        -- recuperer l'ecriture_detail pour emargements semi-auto
        SELECT ecd_ordre INTO ecd_ordre_ema
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;

                -- recup de la depense papier
                SELECT * INTO tmpdepensepapier
                FROM jefy_depense.DEPENSE_PAPIER
                WHERE dpp_id = jefydepensebudget.dpp_id;

                -- recup des infos de depense_ctrl_planco

                SELECT * INTO jefydepenseplanco
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;
        -- creation de la depense --
        INSERT INTO DEPENSE VALUES
            (
            fouadresse ,           --DEP_ADRESSE,
            NULL ,                   --DEP_DATE_COMPTA,
            tmpdepensepapier.dpp_date_reception,  --DEP_DATE_RECEPTION,
            tmpdepensepapier.dpp_date_service_fait , --DEP_DATE_SERVICE,
            'VALIDE' ,               --DEP_ETAT,
            founom ,               --DEP_FOURNISSEUR,
            jefydepenseplanco.dpco_montant_budgetaire , --DEP_HT,
            depense_seq.NEXTVAL ,  --DEP_ID,
            lignebudgetaire ,       --DEP_LIGNE_BUDGETAIRE,
            lotordre ,               --DEP_LOT,
            marordre ,               --DEP_MARCHES,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_MONTANT_DISQUETTE,
            NULL,-- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
            substr(tmpdepensepapier.dpp_numero_facture,1,199)  ,--DEP_NUMERO,
            jefydepenseplanco.dpco_id ,--DEP_ORDRE,
            NULL ,                   --DEP_REJET,
            tmpdepensepapier.rib_ordre ,--DEP_RIB,
            'NON' ,                   --DEP_SUPPRESSION,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_TTC,
            jefydepenseplanco.dpco_ttc_saisie - jefydepenseplanco.dpco_montant_budgetaire, -- DEP_TVA,
            tmpdepensepapier.exe_ordre ,               --EXE_ORDRE,
            fouordre,                --FOU_ORDRE,
            gescode,                 --GES_CODE,
            manid ,                   --MAN_ID,
            jefydepenseplanco.man_id, --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
            modordre,
            jefydepenseplanco.pco_num ,  --PCO_ORDRE,
            tmpdepensepapier.utl_ordre,               --UTL_ORDRE
            orgid, --org_ordre
            tcdordre,
            ecd_ordre_ema, -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
            tmpdepensepapier.dpp_date_facture);

    END LOOP;
    CLOSE depenses;

END;

PROCEDURE get_recette_jefy_recette (titid INTEGER) IS

recettepapier         jefy_recette.recette_papier%ROWTYPE;
recettebudget         jefy_recette.recette_budget%ROWTYPE;
facturebudget         jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco     jefy_recette. RECETTE_CTRL_PLANCO%rowtype;
recettectrlplancotva  jefy_recette.recette_ctrl_planco_tva%rowtype;
maracujatitre         maracuja.titre%rowtype;

adrnom                varchar2(200);
letyperecette         varchar2(200);
titinterne            varchar2(200);
lbud            varchar2(200);
tboordre        integer;
cpt integer;

cursor c_recette is
SELECT *
FROM  jefy_recette.RECETTE_CTRL_PLANCO
WHERE tit_id = titid;

BEGIN



--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;

open c_recette;
loop
fetch c_recette into recettectrlplanco;
exit when c_recette%notfound;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;

select * into maracujatitre
from maracuja.titre
where tit_id = titid;

if (recettebudget.REC_ID_REDUCTION is null) then
 letyperecette := 'R';
else
 letyperecette := 'T';
end if;

select count(*) into cpt
from JEFY_RECETTE.PI_DEP_REC
where rec_id =recettectrlplanco.rec_id;

if cpt > 0 then
 titinterne := 'O';
else
 titinterne := 'N';
end if;

select adr_nom into adrnom from grhum.v_fournis_grhum where fou_ordre = recettepapier.fou_ordre;

select org_ub||'/'||org_cr||'/'||org_souscr into lbud
from jefy_admin.organ
where org_id = facturebudget.org_id;


select distinct tbo_ordre  into tboordre
from maracuja.titre t,maracuja.bordereau b
where b.bor_id = t.bor_id
and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
if tboordre = 200 then
tboordre:=null;
else
tboordre:=facturebudget.org_id;
end if;



    INSERT INTO RECETTE VALUES
        (
        recettectrlplanco.exe_ordre,--EXE_ORDRE,
        maracujatitre.ges_code,--GES_CODE,
        null,--MOD_CODE,
        recettectrlplanco.pco_num,--PCO_NUM,
        recettebudget.REC_DATE_SAISIE,--jefytitre.tit_date,-- REC_DATE,
        adrnom,-- REC_DEBITEUR,
        recette_seq.nextval,-- REC_ID,
        null,-- REC_IMPUTTVA,
        null,-- REC_INTERNE, // TODO ROD
        facturebudget.FAC_LIB,-- REC_LIBELLE,
        lbud,-- REC_LIGNE_BUDGETAIRE,
        'E',-- REC_MONNAIE,
        recettectrlplanco.RPCO_HT_SAISIE,--HT,
        recettectrlplanco.RPCO_TTC_SAISIE,--TTC,
        recettectrlplanco.RPCO_TTC_SAISIE,--DISQUETTE,
        recettectrlplanco.RPCO_TVA_SAISIE,--   REC_MONTTVA,
        facturebudget.FAC_NUMERO,--   REC_NUM,
        recettectrlplanco.rpco_id,--   REC_ORDRE,
        recettepapier.RPP_NB_PIECE,--   REC_PIECE,
        facturebudget.FAC_NUMERO,--   REC_REF,
        'VALIDE',--   REC_STAT,
        'NON',--    REC_SUPPRESSION,  Modif Rod
        letyperecette,--     REC_TYPE,
        NULL,--     REC_VIREMENT,
        titid,--      TIT_ID,
        -titid,--      TIT_ORDRE,
        recettebudget.utl_ordre,--       UTL_ORDRE
        facturebudget.org_id,        --       ORG_ORDRE --ajout rod
        facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
        null, --mod_ordre
        recettepapier.mor_ordre,  --mor_ordre
        recettepapier.rib_ordre,
        NULL
        );

end loop;
close c_recette;

END;


-- procedures du brouillard

PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta     MANDAT.ges_code%TYPE;
   PVICONTREPARTIE_GESTION      PLANCO_VISA.PVI_CONTREPARTIE_GESTION%type;
   MODCONTREPARTIE_GESTION mode_paiement.MOD_CONTREPARTIE_GESTION%type;
       pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt               INTEGER;
       tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
       sens ECRITURE_DETAIL.ecd_sens%TYPE;

BEGIN

   SELECT * INTO lemandat
       FROM MANDAT
       WHERE man_id = manid;

       SELECT DISTINCT tbo_ordre INTO tboordre
       FROM BORDEREAU
       WHERE bor_id IN (SELECT bor_id FROM MANDAT WHERE man_id = manid);



--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
   IF lemandat.prest_id IS NULL THEN
       -- creation du mandat_brouillard visa DEBIT--
               sens:=inverser_sens_orv(tboordre,'D');
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                             --ECD_ORDRE,
           lemandat.exe_ordre,               --EXE_ORDRE,
           lemandat.ges_code,               --GES_CODE,
           ABS(lemandat.man_ht),               --MAB_MONTANT,
           'VISA MANDAT',                   --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
           sens,                           --MAB_SENS,
           manid,                           --MAN_ID,
           lemandat.pco_num               --PCO_NU
           );


       -- credit=ctrepartie
       --debit = ordonnateur
       -- recup des infos du VISA CREDIT --
       SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre = lemandat.exe_ordre;

       IF cpt = 0 THEN
          RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
       END IF;

       SELECT pco_num_ctrepartie, PCO_NUM_TVA,PVI_CONTREPARTIE_GESTION
           INTO pconum_ctrepartie, PCONUM_TVA,PVICONTREPARTIE_GESTION
           FROM PLANCO_VISA
           WHERE pco_num_ordonnateur = lemandat.pco_num
           and exe_ordre = lemandat.exe_ordre;


       SELECT  COUNT(*) INTO cpt
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;

       IF cpt != 0 THEN
           SELECT  pco_num_visa,mod_CONTREPARTIE_GESTION INTO pconum_ctrepartie,MODCONTREPARTIE_GESTION
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;
       END IF;




           -- modif 15/09/2005 compatibilite avec new gestion_exercice
       SELECT c.ges_code,ge.PCO_NUM_185
           INTO gescodecompta,pconum_185
           FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
           WHERE g.ges_code = lemandat.ges_code
           AND g.com_ordre = c.com_ordre
           AND g.ges_code=ge.ges_code
           AND ge.exe_ordre=lemandat.EXE_ORDRE;

               -- 5/12/2007
               -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
               -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
               -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                     --SELECT par_value   INTO parvalue
       --    FROM PARAMETRE
       --    WHERE par_key ='CONTRE PARTIE VISA'
       --    AND exe_ordre = lemandat.exe_ordre;

       parvalue := PVICONTREPARTIE_GESTION;
       if (MODCONTREPARTIE_GESTION is not null) then
           parValue := MODCONTREPARTIE_GESTION;
       end if;
              IF parvalue = 'COMPOSANTE' THEN
          gescodecompta := lemandat.ges_code;
       END IF;

       IF pconum_185 IS NULL THEN
           -- creation du mandat_brouillard visa CREDIT --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;
           INSERT INTO MANDAT_BROUILLARD VALUES (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           gescodecompta,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       ELSE
           --au SACD --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;

           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       END IF;

       IF lemandat.man_tva != 0 THEN
           -- creation du mandat_brouillard visa CREDIT TVA --
                       sens:=inverser_sens_orv(tboordre,'D');
           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
       END IF;

   ELSE
       Bordereau_abricot.set_mandat_brouillard_intern(manid);
   END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   leplancomptable   PLAN_COMPTABLE%ROWTYPE;

   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta MANDAT.ges_code%TYPE;
   pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt INTEGER;
   lepconum plan_comptable_exer.pco_num%type;

BEGIN

    SELECT * INTO lemandat
    FROM MANDAT
    WHERE man_id = manid;


       -- modif 15/09/2005 compatibilite avec new gestion_exercice
     SELECT c.ges_code,ge.PCO_NUM_185
     INTO gescodecompta,pconum_185
     FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
     WHERE g.ges_code = lemandat.ges_code
     AND g.com_ordre = c.com_ordre
     AND g.ges_code=ge.ges_code
     AND ge.exe_ordre=lemandat.EXE_ORDRE;



   -- recup des infos du VISA CREDIT --
   SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   IF cpt = 0 THEN
      RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
   END IF;
   SELECT pco_num_ctrepartie, PCO_NUM_TVA
       INTO pconum_ctrepartie, PCONUM_TVA
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

--   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

--    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--

   lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, lemandat.pco_num);




--    lemandat.pco_num := '18'||lemandat.pco_num;

   -- creation du mandat_brouillard visa DEBIT--
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                             --ECD_ORDRE,
       lemandat.exe_ordre,               --EXE_ORDRE,
       lemandat.ges_code,               --GES_CODE,
       ABS(lemandat.man_ht),               --MAB_MONTANT,
       'VISA MANDAT',                   --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
       'D',                           --MAB_SENS,
       manid,                           --MAN_ID,
       '18'||lemandat.pco_num               --PCO_NU
       );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

--   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

--   END IF;

    lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, '181');


   -- planco de CREDIT 181
   -- creation du mandat_brouillard visa CREDIT --
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                            --ECD_ORDRE,
       lemandat.exe_ordre,              --EXE_ORDRE,
       gescodecompta,                  --GES_CODE,
       ABS(lemandat.man_ttc),                --MAB_MONTANT,
       'VISA MANDAT',                  --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
       'C',                          --MAB_SENS,
       manid,                          --MAN_ID,
       '181'                  --PCO_NU
       );



   IF lemandat.man_tva != 0 THEN
       -- creation du mandat_brouillard visa CREDIT TVA --
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           'D',                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
   END IF;

END;

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--    niv INTEGER;
--BEGIN


--


--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE
--         (
--         PCO_BUDGETAIRE,
--         PCO_EMARGEMENT,
--         PCO_LIBELLE,
--         PCO_NATURE,
--         PCO_NIVEAU,
--         PCO_NUM,
--         PCO_SENS_EMARGEMENT,
--         PCO_VALIDITE,
--         PCO_J_EXERCICE,
--         PCO_J_FIN_EXERCICE,
--         PCO_J_BE
--         )
--    VALUES
--     (
--     'N',--PCO_BUDGETAIRE,
--     'O',--PCO_EMARGEMENT,
--     libelle,--PCO_LIBELLE,
--     nature,--PCO_NATURE,
--     niv,--PCO_NIVEAU,
--     pconum,--PCO_NUM,
--     2,--PCO_SENS_EMARGEMENT,
--     'VALIDE',--PCO_VALIDITE,
--     'O',--PCO_J_EXERCICE,
--     'N',--PCO_J_FIN_EXERCICE,
--     'N'--PCO_J_BE
--     );
--END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      recid   integer;

      cursor c_recettes is
      select * from jefy_recette.RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN

    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
     select max(rb.REC_ID_REDUCTION) into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

-- si dans le cas d une reduction
if (REDUCTION is not null) then
  lesens :='D';
else
  lesens :='C';
end if;


    IF letitre.prest_id IS NULL THEN

      open c_recettes;
      loop
      fetch c_recettes into RECETTEctrlplanco;
      exit when c_recettes%notfound;

    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  RECETTEctrlplanco.pco_num,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;



               -- recette_ctrl_planco_ctp
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;

    ELSE
     Set_Titre_Brouillard_intern(titid);
    END IF;

    -- suppression des lignes d ecritures a ZERO
    delete from TITRE_BROUILLARD where TIB_MONTANT = 0;
END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      lepconum            maracuja.PLAN_COMPTABLE.pco_num%type;
      libelle             maracuja.PLAN_COMPTABLE.pco_libelle%type;
      chap                varchar2(2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;

      cursor c_recettes is
      select * from jefy_recette. RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN



    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

    -- modif fred 04/2007
-- brouillard de PI mauvais des contre partie
-- gescodecompta,
--   -- ges_code,               --GES_CODE,
      SELECT c.ges_code
      INTO gescodecompta
      FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
      WHERE g.ges_code = letitre.ges_code
      AND g.com_ordre = c.com_ordre
      AND g.ges_code=ge.ges_code
      AND ge.exe_ordre=letitre.EXE_ORDRE;


-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     select rb.REC_ID_REDUCTION into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

    -- si dans le cas d une reduction
    if (REDUCTION is not null) then
     lesens :='D';
    else
     lesens :='C';
    end if;

    open c_recettes;
    loop
    fetch c_recettes into RECETTEctrlplanco;
    exit when c_recettes%notfound;



    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

    -- recup des 2 premiers caracteres du compte
    select substr(RECETTEctrlplanco.pco_num,1,2) into chap from dual;

    if chap != '18' then
--     select pco_libelle into libelle
--     from maracuja.plan_comptable
--     where pco_num = RECETTEctrlplanco.pco_num;

--     lepconum := '18'||RECETTEctrlplanco.pco_num;
--     maj_plancomptable_titre('R',libelle,lepconum);

        lepconum := api_planco.CREER_PLANCO_PI(RECETTEctrlplanco.exe_ordre, RECETTEctrlplanco.pco_num);


    end if;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  lepconum,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
 gescodecompta,
               -- ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;



               -- recette_ctrl_planco_ctp on force le 181
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
 gescodecompta,
               -- ges_code,               --GES_CODE,
                  '181',               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;
END;



--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--  niv INTEGER;
--  cpt integer;
--BEGIN

--select count(*) into cpt from PLAN_COMPTABLE
--where pco_num = pconum;

--if cpt = 0 then
--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
--        VALUES
--        (
--        'N',--PCO_BUDGETAIRE,
--        'O',--PCO_EMARGEMENT,
--        libelle,--PCO_LIBELLE,
--        nature,--PCO_NATURE,
--        niv,--PCO_NIVEAU,
--        pconum,--PCO_NUM,
--        2,--PCO_SENS_EMARGEMENT,
--        'VALIDE',--PCO_VALIDITE,
--        'O',--PCO_J_EXERCICE,
--        'N',--PCO_J_FIN_EXERCICE,
--        'N'--PCO_J_BE
--        );
--end if;

--END;

-- outils

FUNCTION inverser_sens_orv (tboordre INTEGER,sens VARCHAR) RETURN VARCHAR IS
cpt INTEGER;

BEGIN

-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
SELECT count(*)  INTO cpt
FROM TYPE_BORDEREAU
WHERE tbo_sous_type ='REVERSEMENTS'
AND tbo_ordre = tboordre;

IF (cpt != 0) THEN
 IF (sens = 'C') THEN
  RETURN 'D';
 ELSE
  RETURN 'C';
 END IF;
END IF;
RETURN sens;
END ;




FUNCTION recup_gescode (abrid INTEGER) RETURN VARCHAR IS
gescode BORDEREAU.GES_CODE%TYPE;
BEGIN

SELECT DISTINCT ges_code INTO gescode FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN gescode;
END;

FUNCTION recup_utlordre (abrid INTEGER) RETURN INTEGER IS
utlordre BORDEREAU.utl_ordre%TYPE;
BEGIN
SELECT DISTINCT utl_ordre INTO utlordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN utlordre;
END;

FUNCTION recup_exeordre (abrid INTEGER) RETURN INTEGER IS
exeordre BORDEREAU.exe_ordre%TYPE;
BEGIN
SELECT DISTINCT exe_ordre INTO exeordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN exeordre;
END;

FUNCTION recup_tboordre (abrid INTEGER) RETURN INTEGER IS
tboordre BORDEREAU.tbo_ordre%TYPE;
BEGIN
SELECT DISTINCT tbo_ordre INTO tboordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN tboordre;
END;

FUNCTION recup_groupby (abrid INTEGER) RETURN VARCHAR IS
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
BEGIN
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN abrgroupby;
END;


function inverser_sens (sens varchar) return varchar is
begin
if sens = 'D' then
return 'C';
else
return 'D';
end if;

end;



PROCEDURE numeroter_bordereau(borid INTEGER) IS
cpt_mandat integer;
cpt_titre integer;
BEGIN

select count(*) into cpt_mandat from mandat
where bor_id = borid;

select count(*) into cpt_titre from titre
where bor_id = borid;

if cpt_mandat + cpt_titre = 0 then
raise_application_error (-20001,'Bordereau  vide');
else
Numerotationobject.numeroter_bordereau(borid);

-- boucle mandat
Numerotationobject.numeroter_mandat(borid);

-- boucle titre
Numerotationobject.numeroter_titre(borid);
end if;

END;




FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
BEGIN

IF orgid IS NULL THEN RETURN NULL; END IF;

SELECT COUNT(*) INTO cpt
FROM accords.CONVENTION_LIMITATIVE
WHERE org_id = orgid AND exe_ordre = exeordre;

IF cpt >0 THEN
 -- recup du type_origine CONVENTION--
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

 SELECT DISTINCT con_ordre INTO convordre
 FROM accords.CONVENTION_LIMITATIVE
 WHERE org_id = orgid
 AND exe_ordre = exeordre;


 SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET)
 INTO orilibelle
 FROM accords.contrat
 WHERE con_ordre = convordre;

ELSE
 SELECT COUNT(*) INTO cpt
 FROM jefy_admin.organ
 WHERE org_id = orgid
 AND org_lucrativite = 1;

 IF cpt = 1 THEN
 -- recup du type_origine OPERATION LUCRATIVE --
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'OPERATION LUCRATIVE';

 --le libelle utilisateur pour le suivie en compta --
 SELECT org_UB||'-'||org_CR||'-'||org_souscr
 INTO orilibelle
 FROM jefy_admin.organ
 WHERE org_id = orgid;

 ELSE
  RETURN NULL;
 END IF;
END IF;

-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ID'
AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
AND ORI_KEY_ENTITE    =orgid;

IF cpt >= 1 THEN
    SELECT ori_ordre INTO cpt FROM ORIGINE
    WHERE ORI_KEY_NAME = 'ORG_ID'
    AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
    AND ORI_KEY_ENTITE = orgid
    AND ROWNUM=1;

ELSE
    SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

    INSERT INTO ORIGINE
    (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
    VALUES ('JEFY_ADMIN','ORG_ID',orilibelle,cpt,orgid,topordre);

END IF;

RETURN cpt;

END;




procedure controle_bordereau(borid integer) is

ttc         maracuja.titre.TIT_TTC%type;
detailttc   maracuja.titre.TIT_TTC%type;
ordottc     maracuja.titre.TIT_TTC%type;
debit       maracuja.titre.TIT_TTC%type;
credit      maracuja.titre.TIT_TTC%type;
cpt         integer;
message     varchar2(50);
messagedetail varchar2(50);
begin

select count(*) into cpt
from maracuja.titre
where bor_id = borid;

if cpt = 0 then
-- somme des maracuja.titre
 select sum(man_ttc) into ttc
 from maracuja.mandat
 where bor_id = borid;

--somme des maracuja.recette
 select sum(d.dep_ttc) into detailttc
 from maracuja.mandat m,maracuja.depense d
 where m.man_id = d.man_id
 and m.bor_id = borid;

-- la somme des credits
 select sum(mab_montant) into credit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'C';

-- la somme des debits
 select sum(mab_montant) into debit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'D';

-- somme des jefy.recette
 select sum(d.dpco_ttc_saisie) into ordottc
 from maracuja.mandat m,jefy_depense.depense_ctrl_planco d
 where m.man_id = d.man_id
 and m.bor_id = borid;

message := ' mandats ';
messagedetail := ' depenses ';

else
-- somme des maracuja.titre
 select sum(tit_ttc) into ttc
 from maracuja.titre
 where bor_id = borid;

--somme des maracuja.recette
 select sum(r.rec_monttva+r.rec_mont) into detailttc
 from maracuja.titre t,maracuja.recette r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

-- la somme des credits
 select sum(tib_montant) into credit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'C';

-- la somme des debits
 select sum(tib_montant) into debit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'D';

-- somme des jefy.recette
 select sum(r.rpco_ttc_saisie) into ordottc
 from maracuja.titre t,jefy_recette.RECETTE_CTRL_PLANCO r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

message := ' titres ';
messagedetail := ' recettes ';

end if;


-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  debit <> credit : '||debit||' '||credit);
end if;

-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  ecriture <> budgetaire : '||debit||' '||ttc);
end if;

-- somme des maracuja.titre = somme des maracuja.recette
if (nvl(ttc,0) != nvl(detailttc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant des '||messagedetail||' :'||ttc||' '||detailttc);
end if;

-- somme des jefy.recette = somme des maracuja.recette
if (nvl(ttc,0) != nvl(ordottc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant ordonnateur des '||messagedetail||' :'||ttc||' '||ordottc);
end if;


bordereau_abricot.ctrl_date_exercice(borid);
end;


procedure  Get_recette_prelevements (titid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;

ORIORDRE INTEGER;
modordre INTEGER;
recid integer;

echeid                  integer;
echeancier_data         jefy_echeancier.ECHEANCIER%ROWTYPE;
echeancier_prelev_data  jefy_echeancier.ECHEANCIER_prelev%ROWTYPE;
facture_data            jefy_recette.facture_budget%rowtype;
personne_data           grhum.v_personne%ROWTYPE;
premieredate            date;
BEGIN

-- verifier s il existe un echancier pour ce titre
select count(*) into cpt
from
jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;


IF (cpt != 1) THEN
      RETURN;
END IF;


-- recup du eche_id / ech_id
select eche_id into echeid
from jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
SELECT * INTO echeancier_data FROM jefy_echeancier.echeancier where ech_id = echeid;
select * into echeancier_prelev_data from jefy_echeancier.echeancier_prelev where ech_id = echeid;
select * into facture_data from jefy_recette.facture_budget where eche_id = echeid;

SELECT * INTO personne_data  FROM GRHUM.V_PERSONNE WHERE pers_id = facture_data.pers_id;

select echd_date_prevue into premieredate
from jefy_echeancier.echeancier_detail
where echd_numero = 1
and ech_id = echeid;

select rec_id into recid from recette where tit_id = titid;


/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
         RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/


-- recup ??
ORIORDRE :=Gestionorigine.traiter_orgid(facture_data.org_id,facture_data.exe_ordre);


INSERT INTO MARACUJA.ECHEANCIER (ECHE_AUTORIS_SIGNEE,
FOU_ORDRE_CLIENT, CON_ORDRE,
ECHE_DATE_1ERE_ECHEANCE, ECHE_DATE_CREATION, ECHE_DATE_MODIF,
ECHE_ECHEANCIER_ORDRE,
ECHE_ETAT_PRELEVEMENT, FT_ORDRE, ECHE_LIBELLE, ECHE_MONTANT, ECHE_MONTANT_EN_LETTRES,
ECHE_NOMBRE_ECHEANCES, ECHE_NUMERO_INDEX, ORG_ORDRE, PREST_ORDRE, ECHE_PRISE_EN_CHARGE,
ECHE_REF_FACTURE_EXTERNE, ECHE_SUPPRIME, EXE_ORDRE, TIT_ID, REC_ID, TIT_ORDRE, ORI_ORDRE,
PERS_ID, ORG_ID, PERS_DESCRIPTION)  VALUES
(
'O'  ,--ECHE_AUTORIS_SIGNEE
facture_data.FOU_ORDRE  ,--FOU_ORDRE_CLIENT
null,--echancier_data.CON_ORDRE  ,--CON_ORDRE
premieredate,--echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
sysdate,--echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
sysdate,--echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echeancier_data.ech_id,--echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
'V',--echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
facture_data.fac_id,--echancier_data.FT_ORDRE  ,--FT_ORDRE
echeancier_data.ech_libelle,--echancier_data.LIBELLE,--ECHE_LIBELLE
echeancier_data.ech_MONTANT  ,--ECHE_MONTANT
echeancier_data.ech_MONTANT_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echeancier_data.ech_NB_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echeancier_data.ech_id,--echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
facture_data.org_id,--echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
null,--echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
'O'  ,--ECHE_PRISE_EN_CHARGE
facture_data.fac_lib,--cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
'N'  ,--ECHE_SUPPRIME
facture_data.exe_ordre  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
-titid,
ORIORDRE,--ORI_ORDRE,
personne_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
facture_data.org_id,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE,
RECO_ORDRE, FOU_ORDRE,
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT,
PREL_PRELEV_DATE_SAISIE,
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT,
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ech_id,--ECHE_ECHEANCIER_ORDRE
null,--PREL_FICP_ORDRE
facture_data.FOU_ORDRE,--FOU_ORDRE
echd_COMMENTAIRE,--PREL_COMMENTAIRE
sysdate,--DATE_MODIF,--PREL_DATE_MODIF
echd_date_prevue,--PREL_DATE_PRELEVEMENT
sysdate,--,--PREL_PRELEV_DATE_SAISIE
'ATTENTE',--PREL_PRELEV_ETAT
echd_numero,--PREL_NUMERO_INDEX
echd_MONTANT,--PREL_PRELEV_MONTANT
echd_id,--PREL_PRELEV_ORDRE
echeancier_prelev_data.RIB_ORDRE_DEBITEUR,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM jefy_echeancier.echeancier_detail
WHERE ech_id = echeancier_data.ECH_id;

END;


PROCEDURE ctrl_date_exercice(borid INTEGER) IS
exeordre integer;
annee integer;
BEGIN

select to_char(bor_date_creation,'YYYY'), exe_ordre into annee, exeordre
from bordereau
where bor_id = borid
and exe_ordre >= 2007;

IF exeordre <> annee THEN
update bordereau set bor_date_creation = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update mandat set man_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update titre set tit_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
end if;

END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

    SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentmab.mab_montant,
    NULL,
    currentmab.mab_sens,
    ecrordre,
    currentmab.ges_code,
    currentmab.pco_num
    );

    SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

    -- Insertion des mandat_detail_ecritures
    INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
    ecdordre,
    currentmab.exe_ordre,
    currentmab.man_id,
    SYSDATE,
    mdeordre,
    'VISA',
    oriordre
    );



    END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
        boretat := 'RETENUES';
  ELSE
        boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

  IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;
  ELSE
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;
  END IF;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

    IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET jou_ordre_sacd = ecrordre WHERE bor_ordre = borordre;
  ELSE
        UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_sacd = ecrordre WHERE bor_id = borid;
  END IF;

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
                ecrordre := creerecriture(
                1,
                SYSDATE,
                'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
                exeordre,
                oriordre,
                14,
                9,
                0
                );

              -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
              OPEN bordereaux;
              LOOP
                FETCH bordereaux INTO currentbob;
                EXIT WHEN bordereaux%NOTFOUND;

                        ecdordre := creerecrituredetail (
                        NULL,
                        'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
                        currentbob.bob_montant,
                        NULL,
                        currentbob.bob_sens,
                        ecrordre,
                        currentbob.ges_code,
                        currentbob.pco_num
                        );

                END LOOP;
              CLOSE bordereaux;

              Api_Plsql_Journal.validerecriture(ecrordre);


              --UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;

  END IF;

END IF;



          UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
          UPDATE BORDEREAU
          SET
          bor_date_visa = SYSDATE,
          utl_ordre_visa = 0,
          bor_etat = 'PAIEMENT'
          WHERE bor_id = borid;

          SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

              IF (exeordre<2007) THEN
                           UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;
               ELSE
                              UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'PAIEMENT', ECR_ORDRE_OPP = ecrordre WHERE bor_id = borid;
               END IF;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
AND e.ecr_sens='D'
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
     gescodes := '';
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
         IF (LENGTH(gescodes)>0) THEN
             gescodes := gescodes || ', ';
         END IF;
          gescodes := gescodes || curgescode;
    END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT' AND B.BOR_ETAT <> 'PAYE';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;



IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
    ecrordre := creerecriture(
    1,
    SYSDATE,
    'PAIEMENT SALAIRES '||borlibelle_mois,
    exeordre,
    oriordre,
    14,
    6,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'PAIEMENT SALAIRES '||borlibelle_mois,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);


          IF (exeordre<2007) THEN
         UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
      ELSE
            UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
      END IF;
  END IF;

 ELSE
          IF (exeordre<2007) THEN
           UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;
      ELSE
            UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;
      END IF;


END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bor_id in (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
    localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
    localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE         --        INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Paf IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA PAF';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA PAF' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 20 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

    SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentmab.mab_montant,
    NULL,
    currentmab.mab_sens,
    ecrordre,
    currentmab.ges_code,
    currentmab.pco_num
    );

    SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

    -- Insertion des mandat_detail_ecritures
    INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
    ecdordre,
    currentmab.exe_ordre,
    currentmab.man_id,
    SYSDATE,
    mdeordre,
    'VISA',
    oriordre
    );



    END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  boretat := 'PAIEMENT';

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

/*
  IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;
  ELSE
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;
  END IF;
*/

 UPDATE jefy_paf.paf_etape SET pae_etat=boretat WHERE bor_id=borid;

END IF;

END ;






-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT PAF'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
AND e.ecr_sens='D'
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
     gescodes := '';
     /*
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
         IF (LENGTH(gescodes)>0) THEN
             gescodes := gescodes || ', ';
         END IF;
          gescodes := gescodes || curgescode;
    END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;
  */

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT' AND B.BOR_ETAT <> 'PAYE';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;



IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT b.exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20 and  rownum=1;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT PAF '||borlibelle_mois;

   IF (cpt = 0)
   THEN
    ecrordre := creerecriture(
    1,
    SYSDATE,
    'PAIEMENT PAF '||borlibelle_mois,
    exeordre,
    oriordre,
    14,
    6,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'PAIEMENT PAF '||borlibelle_mois,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

/*
          IF (exeordre<2007) THEN
         UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
      ELSE
            UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
      END IF;
  */
  
   
  END IF;
/*
 ELSE

          IF (exeordre<2007) THEN
           UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;
      ELSE
            UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;
      END IF;
*/
    UPDATE jefy_paf.paf_etape SET pae_etat='TERMINEE' WHERE MOIS_LIBELLE=borlibelle_mois;
  END IF;


  --UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bob_libelle2 = borlibelle_mois;
  
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bor_id in (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE MANDAT
  SET
  man_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
    localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
    localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE         --        INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/






SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Util  IS

    PROCEDURE annuler_visa_bor_mandat(borid INTEGER);

    PROCEDURE supprimer_visa_btme(borid INTEGER);
    
    PROCEDURE supprimer_visa_btms(borid INTEGER);

    PROCEDURE supprimer_visa_btte(borid INTEGER);

    procedure creer_ecriture_annulation(ecrordre INTEGER);
    
    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    procedure supprimer_bordereau_dep(borid integer);
    
    procedure supprimer_bordereau_rec(borid integer);
    
    procedure supprimer_bordereau_btms(borid integer);
    
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer);
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Util IS

    -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
    -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

 -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE annuler_visa_bor_mandat(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, mandat_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.man_id=tmpmanid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME' and tbotype <> 'BTRU') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME ou BTRU');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandats associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
--             
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

--                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

--                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;



       -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE supprimer_visa_btme(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
                            if (flag>0) then
                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                            end if;
                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;


 -- suppression des ecritures du visa d'un bordereau de depense BTMS 
       PROCEDURE supprimer_visa_btms(borid INTEGER) IS
         flag INTEGER;
         lebordereau BORDEREAU%ROWTYPE;
         tbotype TYPE_BORDEREAU.tbo_type%TYPE;
         tmpmanid MANDAT.man_id%TYPE;
         tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

         CURSOR lesecritures IS
         SELECT distinct ed.ecr_ordre from ecriture_detail ed, mandat_detail_ecriture mde, mandat m WHERE m.man_id = mde.man_id and mde.ecd_ordre= ed.ecd_ordre and m.bor_id = borid;

         CURSOR lesmandats IS
         SELECT man_id FROM MANDAT WHERE bor_id = borid;

         BEGIN
           SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTMS') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTMS');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat = 'VALIDE'  ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

--            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;

            OPEN lesecritures;
             LOOP
              FETCH lesecritures INTO tmpEcrOrdre;
             EXIT WHEN lesecritures%NOTFOUND;

                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE ecd_ordre in (select ecd_ordre from ecriture_detail where ecr_ordre = tmpEcrOrdre);

                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;

                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

             END LOOP;
             CLOSE lesecritures;


            OPEN lesmandats;
            LOOP
            FETCH lesmandats INTO tmpmanid;
            EXIT WHEN lesmandats%NOTFOUND;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;

             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

            UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_visa = null,ecr_ordre_sacd = null,ecr_ordre_opp = null  WHERE bor_id IN (borid);

       END;




   -- annulation du visa d'un bordereau de depense BTTE
       PROCEDURE supprimer_visa_btte(borid INTEGER) IS
                    flag INTEGER;
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmptitid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associ� au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmptitid;
                             EXIT WHEN lestitres%NOTFOUND;

                            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, TITRE_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.tit_id=tmptitid;

                            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                            DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmptitid;
                            UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmptitid;


             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;
       
       
       
       procedure creer_ecriture_annulation(ecrordre INTEGER) is
            ecr maracuja.ecriture%rowtype;
            ecd maracuja.ecriture_detail%rowtype;
            flag integer;
            str ecriture.ecr_libelle%type;
            newEcrOrdre ecriture.ecr_ordre%type;
            newEcdOrdre ecriture_detail.ecd_ordre%type;
            x  integer;
            cursor c1 is select * from maracuja.ecriture_detail where ecr_ordre=ecrordre;
       
       begin
            
            
            select count(*) into flag from maracuja.ecriture where ecr_ordre=ecrordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture retrouvee pour ecr_ordre= '|| ecrordre || ' .');
            end if;

            select * into ecr from maracuja.ecriture where ecr_ordre=ecrordre;              
            
            str := 'Annulation Ecriture '|| ecr.exe_ordre || '/'||ecr.ecr_numero;
            
            -- verifier que l'ecriture n'a pas deja ete annulee
            select count(*) into flag from maracuja.ecriture where ecr_libelle = str; 
            if (flag > 0) then
                select ecr_numero into flag from maracuja.ecriture where ecr_libelle = str; 
                RAISE_APPLICATION_ERROR (-20001,'L''ecriture numero '||ecr.ecr_numero ||' (ecr_ordre= '|| ecrordre || ') a deja ete annulee par l''ecriture numero '|| flag ||'.');
            end if;
                   
            
            -- verifier que les details ne sont pas emarges
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                if (ecd.ecd_reste_emarger<abs(ecd.ecd_montant) ) then
                    RAISE_APPLICATION_ERROR (-20001,'L''ecriture '||ecr.ecr_numero ||' ecr_ordre= '|| ecrordre || ' a ete emargee pour le compte '||ecd.pco_num||'. Impossible d''annuler');                    
                end if;

             END LOOP;
             CLOSE c1;


--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...              
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--                    
--                    

--             END LOOP;
--             CLOSE c1;

            newEcrOrdre := api_plsql_journal.creerecriture(
                ecr.com_ordre,
                SYSDATE,
                str,
                ecr.exe_ordre,
                ecr.ori_ordre,
                ecr.tjo_ordre,
                ecr.top_ordre,
                ecr.utl_ordre);            
            
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                newEcdOrdre := api_plsql_journal.creerEcritureDetail (
                        ecd.ecd_commentaire,
                        ecd.ecd_libelle,  
                        -ecd.ECD_MONTANT,
                        ecd.ECD_SECONDAIRE,
                        ecd.ECD_SENS,
                        newEcrOrdre,
                        ecd.GES_CODE,
                        ecd.PCO_NUM );
                        
                x := creerEmargementMemeSens(ecd.ecd_ordre, newEcdOrdre, 1);
             END LOOP;
             CLOSE c1;                  
            NUMEROTATIONOBJECT.numeroter_ecriture(newEcrOrdre);

       end;
           
       
       
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
        flag integer;
        emargementdetail EMARGEMENT_DETAIL%ROWTYPE;
        CURSOR c1 IS SELECT * INTO emargementdetail FROM EMARGEMENT_DETAIL WHERE ema_ordre = emaordre;
    BEGIN
        select count(*) into flag from emargement where ema_etat='VALIDE' and ema_ordre = emaordre;
        
        if (flag=1) then
             OPEN c1;
            LOOP
            FETCH c1 INTO emargementdetail;
            EXIT WHEN c1%NOTFOUND;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_source;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_destination;

            END LOOP;
            CLOSE c1;
   
            -- annulation de la emargement
            UPDATE EMARGEMENT SET ema_etat = 'ANNULE'
            WHERE ema_ordre = emaordre;        
        
        end if;

       
   
    END;
    
    
    
        
    procedure supprimer_bordereau_dep(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des mandats
        select count(*) into flag from mandat where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de depense.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les mandat_brouillards
        DELETE FROM maracuja.mandat_BROUILLARD WHERE man_id in (select man_id from mandat where bor_id=borid);
                
        -- supprimer les depenses
        DELETE FROM maracuja.depense WHERE man_id in (select man_id from mandat where bor_id=borid);

        -- mettre a jour les depense_ctrl_planco
        UPDATE jefy_depense.depense_ctrl_planco SET man_id = NULL WHERE man_id IN (SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid) ;

        -- supprimer les mandats        
        delete from  maracuja.mandat where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
        
        
    end;
    
    
    procedure supprimer_bordereau_btms(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau b, type_bordereau tb where bor_id=borid and b.tbo_ordre=tb.tbo_ordre and tb.TBO_TYPE='BTMS';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas ou n''est pas de type BTMS');
        end if;
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;          
        
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        
        supprimer_bordereau_dep(borid);
        
    end;
    
    
    
    procedure supprimer_bordereau_rec(borid integer)  as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des titres
        select count(*) into flag from titre where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de recette.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les titre_brouillards
        DELETE FROM maracuja.titre_BROUILLARD WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);
                
        -- supprimer les recettes
        DELETE FROM maracuja.recette WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);

        -- mettre a jour les recette_ctrl_planco
        UPDATE jefy_recette.recette_ctrl_planco SET tit_id = NULL WHERE tit_id IN (SELECT tit_id FROM maracuja.titre WHERE bor_id = borid) ;

        -- supprimer les titres        
        delete from  maracuja.titre where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
    
    end;
 



    /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer) IS
        flag INTEGER;                 
        leRecouvrement recouvrement%rowtype;
        tmpemaOrdre emargement.ema_ordre%type;
        tmpEcrOrdre ecriture.ecr_ordre%type;

        cursor emargements is 
                select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' 
                        and rec_id in (
                                select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ))) 
                    union
                        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where 
                        eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )));

                    
       cursor ecritures is
               select distinct e.ecr_ordre from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ));

        begin
        
            -- recuperer les numeros des ecritures min et max pour les passer en parametre
            --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
        
            select count(*) into flag from prelevement where RECO_ORDRE = recoordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucun prelevement correspondant a recoordre= '|| recoordre);
            end if;
            
            select * into leRecouvrement from recouvrement where RECO_ORDRE = recoordre;
            
            -- verifier les dates des ecritures avec la date du prelevement
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMin;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if; 
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMax;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if;             
            

        -- supprimer les emargements
        OPEN emargements;
             LOOP
                 FETCH emargements INTO tmpemaOrdre;
                      EXIT WHEN emargements%NOTFOUND;
                      
                annuler_emargement(tmpEmaOrdre);      
        END LOOP;
        CLOSE emargements;        
        
        -- modifier les etats des prelevements
        update prelevement set PREL_ETAT_MARACUJA = 'ATTENTE' where reco_ordre=recoOrdre;

        --supprimer le contenu du fichier (prelevement_fichier)
        delete from prelevement_fichier where reco_ordre=recoordre; 

        -- supprimer les �critures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
        OPEN ecritures;
             LOOP
                 FETCH ecritures INTO tmpecrOrdre;
                      EXIT WHEN ecritures%NOTFOUND;
                      
                creer_ecriture_annulation(tmpEcrOrdre);
        END LOOP;
        CLOSE ecritures;         

        delete from titre_detail_ecriture where ecd_ordre in (select distinct ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
        where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre 
        from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in 
        (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )) );

        -- supprimer association des pr�l�vements au recouvrement
        update  prelevement set reco_ordre=null where reco_ordre=recoOrdre;

        -- supprimer l'objet recouvrement
        delete from recouvrement where reco_ordre=recoOrdre;





--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))


--        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))) 
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--                 
                 
                 
                     
    end;
END;
/




