SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, GES_LIB, IMP_LIB, BOR_DATE_VISA, BOR_NUM, 
 MAN_TTC)
AS 
SELECT mr.exe_ordre, mr.ecr_date_saisie, mr.ecr_date,
      mr.ges_code, mr.pco_num, mr.bor_id, mr.tbo_ordre, mr.man_id, mr.man_num, mr.man_lib,
      mr.man_mont, sign(man_mont)*mr.man_tva, mr.man_etat, mr.fou_ordre, mr.ecr_ordre,
      mr.ecr_sacd, o.org_lib, api_planco.get_pco_libelle(mr.pco_num, mr.exe_ordre) pco_libelle, b.bor_date_visa, b.bor_num,
      man_mont + sign(man_mont)*man_tva
 FROM v_mandat_reimp_0 mr, v_organ_exer o, BORDEREAU b
WHERE mr.ges_code = o.org_ub
  AND o.org_niv = 2
  AND mr.EXE_ORDRE=o.exe_ordre
  AND mr.bor_id = b.bor_id
/