SET DEFINE OFF;

grant select, update on jefy_paf.paf_etape to maracuja;

ALTER TABLE maracuja.PRELEVEMENT_FICHIER drop constraint PRELEVEMENT_utilisateur_FK; 

ALTER TABLE maracuja.PRELEVEMENT_FICHIER ADD CONSTRAINT PRELEVEMENT_utilisateur_FK 
FOREIGN KEY (UTL_ORDRE) REFERENCES jefy_admin.UTILISATEUR (UTL_ORDRE) DEFERRABLE INITIALLY DEFERRED;







CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_ext (exeordre number, pco_condition VARCHAR2, sens_compte CHAR, comp VARCHAR2, sacd CHAR, vue varchar2)
RETURN NUMBER

IS
solde NUMBER(12,2);
 LC$req varchar2(1000);
 lavue varchar2(50);
BEGIN

lavue := vue;
if lavue is null then
  lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
end if;

IF exeordre >= 2005 THEN
   IF sens_compte = 'D'   THEN
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSE
           LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   ELSE
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSE
           LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   END IF;
ELSE
   --- A Refaire ---
   solde := 0;
END IF;

-- dbms_output.put_line( LC$req  );

EXECUTE IMMEDIATE LC$req INTO solde ;

IF solde < 0
   THEN solde := 0;
END IF;



RETURN solde;
END ;
/ 