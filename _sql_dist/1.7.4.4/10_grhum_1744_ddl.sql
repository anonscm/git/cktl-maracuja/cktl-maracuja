set define off;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND tbo_ordre<>9 AND tbo_ordre<>11 AND tbo_ordre<>200 
  AND ed.pco_num NOT LIKE '185%' 
  and t.tit_id in (select tit_id from (select tit_id, count(*) from recette group by tit_id having count(*)=1))
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- titres co
SELECT x.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, x.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ecd_libelle, ecd_credit,
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, e.ecr_ordre,
      ecr_sacd, tde_origine, ecd_montant
 FROM BORDEREAU b,
      TITRE t,
      (select ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre, pco_num, tde_origine, 'Titre collectif' ecd_libelle, sum(ecd_montant) ecd_montant, sum(ecd_credit) ecd_credit 
        from TITRE_DETAIL_ECRITURE tde,
        ECRITURE_DETAIL ed,
        v_ecriture_infos ei
        where 
            tde.ecd_ordre = ed.ecd_ordre 
            and ed.ecd_ordre = ei.ecd_ordre
            and ecd_credit<>0
            AND ed.pco_num NOT LIKE '185%'
            AND tde_origine IN ('VISA', 'REIMPUTATION')
        group by ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre,pco_num, tde_origine) x,             
        ECRITURE e,             
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id 
  AND x.ecr_ordre = e.ecr_ordre
  and t.tit_id = x.tit_id
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = x.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tit_etat IN ('VISE', 'PAYE')
 -- AND ecd_credit<>0
  AND tbo_ordre<>9 AND tbo_ordre<>11 AND tbo_ordre<>200  
  and t.tit_id in (select tit_id from (select tit_id, count(*) from recette group by tit_id having count(*)>1))
  AND ABS (ecd_montant)=ABS(tit_ht)
union all
-- titres de recettes des PI
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND (tbo_ordre=11 or tbo_ordre=200) 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- reductions de recettes 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie AS ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_DEBIT, 
      (SIGN(ecd_debit))*ABS(t.tit_tva), ecd_debit+((SIGN(ecd_debit))*ABS(t.tit_tva)), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_debit<>0 
  AND tbo_ordre=9 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht);
