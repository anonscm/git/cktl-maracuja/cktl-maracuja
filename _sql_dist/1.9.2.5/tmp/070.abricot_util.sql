
CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_util
is
-- Methodes utilitaires pour la generation des mandats et titres
--
--//FIXME 06/01/2012 adapter les fonctions lorsque la respartition par taux de TVA sera active, pour l'instant le taux n'est pas exploité
--

   /* indique si la TVA doit etre collectee pour la depense (depend du mode de paiement affecte) res=0 sinon*/
   function is_dpco_tva_collectee (dpcoid integer)
      return integer
   as
      res            integer;
      modcode        mode_paiement.mod_code%type;
      pconumtvactp   mode_paiement.pco_num_tva_ctp%type;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      if (res > 0) then
         select mod_code,
                pco_num_tva_ctp
         into   modcode,
                pconumtvactp
         from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid and pco_num_tva_ctp is not null;

         if (pconumtvactp is null) then
            raise_application_error (-20001, 'Le compte de TVA collectee n''est pas defini pour le mode de paiement ' || modcode || ' alors que le mode de paiement est défini comme "Paiement HT"');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* >0 si le mandat est passe sur un sacd */
   function is_man_sur_sacd (manid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   mandat m, gestion_exercice g
      where  m.ges_code = g.ges_code and m.exe_ordre = g.exe_ordre and g.pco_num_185 is not null and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* renvoie le taux de prorata affecte a la depense */
   function get_dpco_taux_prorata (dpcoid integer)
      return number
   as
      res   number;
   begin
      select tap_taux
      into   res
      from   jefy_admin.taux_prorata tp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.tap_id = tp.tap_id and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie le montant budgetaire de la depense*/
   function get_dpco_montant_budgetaire (dpcoid integer)
      return number
   as
      res   number;
   begin
      -- s'il s'agit dune liquidation définitive sur extourne,
      -- le montant budgétaire stocké au niveau de la liquidation est à 0, donc on le recalcule
      if (extourne.is_dpco_extourne_def (dpcoid) > 0 or extourne.is_dpco_extourne (dpcoid) > 0) then
         res := extourne.calcul_montant_budgetaire (dpcoid);
      else
         select dpco_montant_budgetaire
         into   res
         from   jefy_depense.depense_ctrl_planco dpco
         where  dpco_id = dpcoid;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant de TVA de la facture pour le taux specifié */
   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      -- //FIXME adapter ca lorsque la respartition par taux de TVA sera active
      select dpco_tva_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la part de TVA a deduire : TTC - budgetaire */
   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      --res := get_dep_montant_tva (depid, tauxtva) * get_dep_taux_prorata (depid) / 100;
      --select (dpco_ttc_saisie - dpco_montant_budgetaire)
      select (dpco_ttc_saisie - get_dpco_montant_budgetaire (dpcoid))
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie la part de TVA a collecter : Montant de la TVA si mode de paiement collecte la TVA (i.e. fournisseur soumis TVA intra)  */
   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
      cpt   plan_comptable_exer.pco_num%type;
   begin
      res := 0;

      if (is_dpco_tva_collectee (dpcoid) > 0) then
         res := get_dpco_montant_tva (dpcoid, tauxtva);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la TVA a reverser : collectee - deduite  */
   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      res := get_dpco_montant_tva_coll (dpcoid, tauxtva) - get_dpco_montant_tva_ded (dpcoid, tauxtva);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   jefy_depense.depense_ctrl_planco
         where  dpco_id = dpcoid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant TTC pour un depense_ctrl_planco  */
   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      select dpco.dpco_ttc_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant a payer pour un depense_ctrl_planco : TTC - TVA collectee */
   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      res := get_dpco_montant_ttc (dpcoid) - get_dpco_montant_tva_coll (dpcoid, null);
      return res;
   end;

   /*  Renvoie le type de credit de la depense*/
   function get_dpco_tcd_ordre (dpcoid integer)
      return number
   as
      res   number;
   begin
      select min (tcd_ordre)
      into   res
      from   jefy_depense.engage_budget eb inner join jefy_depense.depense_budget db on db.eng_id = eb.eng_id
             inner join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = db.dep_id
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   /*  Renvoie le fournisseur de la depense*/
   function get_dpco_fou_ordre (dpcoid integer)
      return number
   as
      res   number;
   begin
      select fou_ordre
      into   res
      from   jefy_depense.depense_papier dpp inner join jefy_depense.depense_budget db on dpp.dpp_id = db.dpp_id
             inner join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = db.dep_id
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   /* Renvoie le lot (du marché) associé à la depense s'il existe */
   function get_dpco_lot_ordre (dpcoid integer)
      return number
   as
      res    number;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   jefy_depense.depense_ctrl_marche dcm inner join jefy_depense.depense_budget db on dcm.dep_id = db.dep_id
             inner join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = db.dep_id
      where  dpco.dpco_id = dpcoid;

      if (flag > 0) then
         select lot_ordre
         into   res
         from   (select att.lot_ordre
                 from   jefy_marches.attribution att inner join jefy_depense.depense_ctrl_marche dcm on (dcm.att_ordre = att.att_ordre)
                        inner join jefy_depense.depense_budget db on dcm.dep_id = db.dep_id
                        inner join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = db.dep_id
                 where  dpco.dpco_id = dpcoid)
         where  rownum = 1;
      end if;

      return res;
   end;

   /* Renvoie le marché dont dépend le lot */
   function get_lot_mar_ordre (lotordre integer)
      return number
   as
      res   number;
   begin
      if (lotordre is null) then
         return null;
      end if;

      select mar_ordre
      into   res
      from   jefy_marches.lot
      where  lot_ordre = lotordre;

      return res;
   end;

   /* Renvoie l'org_id affecté à l'engagement dont dépend la depense */
   function get_dpco_org_id (dpcoid integer)
      return number
   as
      res   number;
   begin
      select distinct org_id
      into            res
      from            jefy_depense.engage_budget eb inner join jefy_depense.depense_budget db on eb.eng_id = db.eng_id
                      inner join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = db.dep_id
      where           dpco.dpco_id = dpcoid;

      return res;
   end;

   /* Renvoie l'adresse sous forme de chaine simple, avec un max de taille charactères  */
   function get_dpco_adresse (dpcoid integer, taille integer)
      return varchar
   as
      res      varchar (2000);
      founom   varchar (2000);
      flag     integer;
   begin
      select count (*)
      into   flag
      from   v_depense_adresse
      where  dpco_id = dpcoid;

      if (flag = 0) then
         founom := get_fournis_nom (get_dpco_fou_ordre (dpcoid));
         raise_application_error (-20001, 'Pas d''adresse de facturation pour le fournisseur ' || founom);
      end if;

      select substr (adresse, 1, taille)
      into   res
      from   (select *
              from   v_depense_adresse
              where  dpco_id = dpcoid)
      where  rownum = 1;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le tbo_ordre du bordereau du mandat */
   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type
   as
      res   type_bordereau.tbo_ordre%type;
   begin
      select b.tbo_ordre
      into   res
      from   bordereau b, mandat m
      where  m.bor_id = b.bor_id and m.man_id = manid;

      return res;
   end;

   function get_man_tcdsect (manid integer)
      return jefy_admin.type_credit.tcd_sect%type
   as
      tcdsect   jefy_admin.type_credit.tcd_sect%type;
   begin
      select distinct tc.tcd_sect
      into            tcdsect
      from            jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.type_credit tc
      where           dpco.dep_id = db.dep_id and db.eng_id = eb.eng_id and eb.tcd_ordre = tc.tcd_ordre and man_id = manid;

      return tcdsect;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement du mandat s'il existe et est actif */
   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de ctp affecte au mode de paiement
      res := get_mp_compte_ctp (modordre);

      -- si null, on recupere le compte de  ctp affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_ctp (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
 /* Renvoie le code gestion a utiliser pour la contrepartie du mandat (la composante ou agence suivant les cas) */
   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type
   as
      res                  gestion.ges_code%type;
      gescode_composante   gestion.ges_code%type;
      gescode_agence       gestion.ges_code%type;
      exeordre             mandat.exe_ordre%type;
      pconumordo           mandat.pco_num%type;
      typegestion          planco_visa.pvi_contrepartie_gestion%type;
      modordre             mandat.mod_ordre%type;
      flag                 integer;
   begin
      -- si la ctp est definie dans le mandat on la prend, sinon on prend celle definie dans planco_visa
      -- si c'est un SACD la ctp va forcement sur le SACD
      select m.ges_code,
             m.exe_ordre,
             m.pco_num,
             c.ges_code,
             m.mod_ordre
      into   gescode_composante,
             exeordre,
             pconumordo,
             gescode_agence,
             modordre
      from   mandat m, gestion g, comptabilite c
      where  m.ges_code = g.ges_code and g.com_ordre = c.com_ordre and m.man_id = manid;

      if (is_man_sur_sacd (manid) > 0) then
         res := gescode_composante;
      else
         select mod_contrepartie_gestion
         into   typegestion
         from   mode_paiement mp
         where  mp.mod_ordre = modordre;

         if (typegestion is null) then
            select count (*)
            into   flag
            from   planco_visa pv
            where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

            if (flag > 0) then
               select pvi_contrepartie_gestion
               into   typegestion
               from   planco_visa pv
               where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;
            end if;
         end if;

         -- si non specifie, on prend l'agence
         if (typegestion = 'COMPOSANTE') then
            res := gescode_composante;
         else
            res := gescode_agence;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le montant de la TVA a collecter pour le mandat (depend du mode de paiement) */
   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_coll (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_ded (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_apayer (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_ttc (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_budgetaire (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA de collecte affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva_ctp,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_visa,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_tva
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_ctrepartie
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de contrepartie ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type)
   is
   begin
      insert into mandat_brouillard
      values      (null,   --ECD_ORDRE,
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   montant,   --MAB_MONTANT,
                   operation,   --MAB_OPERATION,
                   mandat_brouillard_seq.nextval,   --MAB_ORDRE,
                   sens,   --MAB_SENS,
                   manid,
                   --MAN_ID,
                   pconum   --PCO_NU
                  );
   end;

   function get_fournis_nom (fouordre integer)
      return varchar
   is
      founom   varchar2 (200);
   begin
      founom := '';

      select substr (nom || ' ' || prenom, 1, 200)
      into   founom
      from   v_fournis_light
      where  fou_ordre = fouordre;

      return founom;
   end;

   /* Renvoie le chemin de la ligne budgetaire */
   function get_organ_path (orgid integer, taille integer)
      return varchar
   as
      res   varchar (2000);
   begin
      select substr (org_ub || decode (org_cr, null, '', '/' || org_cr) || decode (org_souscr, null, '', '/' || org_souscr), 1, taille)
      into   res
      from   jefy_admin.organ
      where  org_id = orgid;

      return res;
   end;

   function get_organ_ub (orgid integer)
      return varchar
   as
      res   varchar (2000);
   begin
      select org_ub
      into   res
      from   jefy_admin.organ
      where  org_id = orgid;

      return res;
   end;
   
   
   
   
   procedure bord_corrige_date_exercice (borid integer)
   is
      exeordre   integer;
      annee      integer;
   begin
      select to_char (bor_date_creation, 'YYYY'),
             exe_ordre
      into   annee,
             exeordre
      from   bordereau
      where  bor_id = borid and exe_ordre >= 2007;

      if exeordre < annee then
         update bordereau
            set bor_date_creation = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update mandat
            set man_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update titre
            set tit_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;
      end if;
   end;
   
   
   
      function get_tit_tboordre (titid integer)
      return type_bordereau.tbo_ordre%type
   as
      res   type_bordereau.tbo_ordre%type;
   begin
      select b.tbo_ordre
      into   res
      from   bordereau b, titre t
      where  t.bor_id = b.bor_id and t.tit_id = titid;

      return res;
   end;

   
end;
/


