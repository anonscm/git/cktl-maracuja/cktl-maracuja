SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.3
-- Date de publication : 29/02/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_maracuja_1913;
commit;

drop procedure grhum.inst_patch_maracuja_1913;


