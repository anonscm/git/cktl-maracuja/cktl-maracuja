SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.3
-- Date de publication : 29/02/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Corrections de messages d'erreurs Abricot dans le cas d'un problème de parametrage
-- Ajout des taux de prorata dans la vue des depense à mandater (pour abricot)
-- correction vue v_fournis_adr et v_fournisseur dans le cas ou plusieurs adresses principale sont définies pour la personne 
-- correction des ecritures passées par erreur sur le compte 18181 pour les prestations internes.
-- reorganisation de l'ecriture de solde classe 6 et 7, résultat dans les dernières lignes
----------------------------------------------



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.1.2', 'MARACUJA' );
exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.1.3', null );
commit;



CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_util
is
-- Methodes utilitaires pour la generation des mandats et titres
--
--//FIXME 06/01/2012 adapter les fonctions lorsque la respartition par taux de TVA sera active, pour l'instant le taux n'est pas exploité
--

   /* indique si la TVA doit etre collectee pour la depense (depend du mode de paiement affecte) res=0 sinon*/
   function is_dpco_tva_collectee (dpcoid integer)
      return integer
   as
      res            integer;
      modcode        mode_paiement.mod_code%type;
      pconumtvactp   mode_paiement.pco_num_tva_ctp%type;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      if (res > 0) then
         select mod_code,
                pco_num_tva_ctp
         into   modcode,
                pconumtvactp
         from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid and pco_num_tva_ctp is not null;

         if (pconumtvactp is null) then
            raise_application_error (-20001, 'Le compte de TVA collectee n''est pas defini pour le mode de paiement ' || modcode || ' alors que le mode de paiement est défini comme "Paiement HT"');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* >0 si le mandat est passe sur un sacd */
   function is_man_sur_sacd (manid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   mandat m, gestion_exercice g
      where  m.ges_code = g.ges_code and m.exe_ordre = g.exe_ordre and g.pco_num_185 is not null and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* renvoie le taux de prorata affecte a la depense */
   function get_dpco_taux_prorata (dpcoid integer)
      return number
   as
      res   number;
   begin
      select tap_taux
      into   res
      from   jefy_admin.taux_prorata tp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.tap_id = tp.tap_id and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie le montant budgetaire de la depense*/
   function get_dpco_montant_budgetaire (dpcoid integer)
      return number
   as
      res   number;
   begin
      select dpco_montant_budgetaire
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant de TVA de la facture pour le taux specifié */
   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      -- //FIXME adapter ca lorsque la respartition par taux de TVA sera active
      select dpco_tva_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la part de TVA a deduire : TTC - budgetaire */
   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      --res := get_dep_montant_tva (depid, tauxtva) * get_dep_taux_prorata (depid) / 100;
      select (dpco_ttc_saisie - dpco_montant_budgetaire)
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie la part de TVA a collecter : Montant de la TVA si mode de paiement collecte la TVA (i.e. fournisseur soumis TVA intra)  */
   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
      cpt   plan_comptable_exer.pco_num%type;
   begin
      res := 0;

      if (is_dpco_tva_collectee (dpcoid) > 0) then
         res := get_dpco_montant_tva (dpcoid, tauxtva);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la TVA a reverser : collectee - deduite  */
   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      res := get_dpco_montant_tva_coll (dpcoid, tauxtva) - get_dpco_montant_tva_ded (dpcoid, tauxtva);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   jefy_depense.depense_ctrl_planco
         where  dpco_id = dpcoid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant TTC pour un depense_ctrl_planco  */
   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      select dpco.dpco_ttc_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant a payer pour un depense_ctrl_planco : TTC - TVA collectee */
   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      res := get_dpco_montant_ttc (dpcoid) - get_dpco_montant_tva_coll (dpcoid, null);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le tbo_ordre du bordereau du mandat */
   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type
   as
      res   type_bordereau.tbo_ordre%type;
   begin
      select b.tbo_ordre
      into   res
      from   bordereau b, mandat m
      where  m.bor_id = b.bor_id and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement du mandat s'il existe et est actif */
   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de ctp affecte au mode de paiement
      res := get_mp_compte_ctp (modordre);

      -- si null, on recupere le compte de  ctp affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_ctp (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
 /* Renvoie le code gestion a utiliser pour la contrepartie du mandat (la composante ou agence suivant les cas) */
   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type
   as
      res                  gestion.ges_code%type;
      gescode_composante   gestion.ges_code%type;
      gescode_agence       gestion.ges_code%type;
      exeordre             mandat.exe_ordre%type;
      pconumordo           mandat.pco_num%type;
      typegestion          planco_visa.pvi_contrepartie_gestion%type;
      modordre             mandat.mod_ordre%type;
      flag integer;
   begin
      -- si la ctp est definie dans le mandat on la prend, sinon on prend celle definie dans planco_visa
      -- si c'est un SACD la ctp va forcement sur le SACD
      select m.ges_code,
             m.exe_ordre,
             m.pco_num,
             c.ges_code,
             m.mod_ordre
      into   gescode_composante,
             exeordre,
             pconumordo,
             gescode_agence,
             modordre
      from   mandat m, gestion g, comptabilite c
      where  m.ges_code = g.ges_code and g.com_ordre = c.com_ordre and m.man_id = manid;

      if (is_man_sur_sacd (manid) > 0) then
         res := gescode_composante;
      else
         select mod_contrepartie_gestion
         into   typegestion
         from   mode_paiement mp
         where  mp.mod_ordre = modordre;

         if (typegestion is null) then
            select count (*)
            into   flag
            from   planco_visa pv
            where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

            if (flag > 0) then
               select pvi_contrepartie_gestion
               into   typegestion
               from   planco_visa pv
               where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;
            end if;
         end if;

         -- si non specifie, on prend l'agence
         if (typegestion = 'COMPOSANTE') then
            res := gescode_composante;
         else
            res := gescode_agence;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le montant de la TVA a collecter pour le mandat (depend du mode de paiement) */
   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_coll (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_ded (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_apayer (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_ttc (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_budgetaire (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA de collecte affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva_ctp,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_visa,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_tva
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_ctrepartie
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de contrepartie ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type)
   is
   begin
      insert into mandat_brouillard
      values      (null,   --ECD_ORDRE,
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   montant,   --MAB_MONTANT,
                   operation,   --MAB_OPERATION,
                   mandat_brouillard_seq.nextval,   --MAB_ORDRE,
                   sens,   --MAB_SENS,
                   manid,
                   --MAN_ID,
                   pconum   --PCO_NU
                  );
   end;
end;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT"
as
   procedure creer_bordereau (abrid integer)
   is
      cpt            integer;
      abrgroupby     abricot_bord_selection.abr_group_by%type;
      monborid_dep   integer;
      monborid_rec   integer;
      flag           integer;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = monborid_dep;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = monborid_rec;

      tmpmandid      integer;
      tmptitid       integer;
      tboordre       integer;
   begin
-- est ce une selection vide ???
      select count (*)
      into   cpt
      from   abricot_bord_selection
      where  abr_id = abrid;

      if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

         -- verifier l etat de l exercice
         select count (*)
         into   flag
         from   jefy_admin.exercice
         where  exe_ordre = recup_exeordre (abrid) and exe_stat in ('O', 'R');

         if (flag = 0) then
            raise_application_error (-20001, 'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.');
         end if;

         -- recup du group by pour traiter les cursors
         abrgroupby := recup_groupby (abrid);

         if (abrgroupby = 'bordereau_1R1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1r1t (abrid, monborid_rec);
         end if;

         if (abrgroupby = 'bordereau_NR1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nr1t (abrid, monborid_rec);

-- controle RA
            select count (*)
            into   cpt
            from   titre
            where  ori_ordre is not null and bor_id = monborid_rec;

            if cpt != 0 then
               raise_application_error (-20001, 'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !');
            end if;
         end if;

         if (abrgroupby = 'bordereau_1D1M') then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1d1m (abrid, monborid_dep);
         end if;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
monborid_dep := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

monborid_rec := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

bordereau_1D1M(abrid,monborid_dep);
bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
         if (abrgroupby not in ('bordereau_1R1T', 'bordereau_NR1T', 'bordereau_1D1M', 'bordereau_1D1M1R1T')) then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nd1m (abrid, monborid_dep);
         end if;

         if (monborid_dep is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_dep);

            open lesmandats;

            loop
               fetch lesmandats
               into  tmpmandid;

               exit when lesmandats%notfound;
               get_depense_jefy_depense (tmpmandid);
            end loop;

            close lesmandats;

            controle_bordereau (monborid_dep);
         end if;

         if (monborid_rec is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_rec);

            open lestitres;

            loop
               fetch lestitres
               into  tmptitid;

               exit when lestitres%notfound;
               -- recup du brouillard
               get_recette_jefy_recette (tmptitid);
               set_titre_brouillard (tmptitid);
               get_recette_prelevements (tmptitid);
            end loop;

            close lestitres;

            controle_bordereau (monborid_rec);
         end if;

-- maj de l etat dans la selection
         if (monborid_dep is not null or monborid_rec is not null) then
            if monborid_rec is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_rec
                where abr_id = abrid;
            end if;

            if monborid_dep is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_dep
                where abr_id = abrid;

               select tbo_ordre
               into   tboordre
               from   bordereau
               where  bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
               if tboordre = 3 then
                  bordereau_abricot_paye.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
               if tboordre = 18 then
                  bordereau_abricot_paye.basculer_bouillard_paye_orv (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 19 then
                  bordereau_abricot_paye.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 22 then
                  bordereau_abricot_paf.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de PAF on retravaille le brouillard
               if tboordre = 20 then
                  bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de PAF on retravaille le brouillard
               if tboordre = 21 then
                  bordereau_abricot_paf.basculer_bouillard_paye_orv (monborid_dep);
               end if;
           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
            end if;
         end if;
      end if;
   end;

   procedure viser_bordereau_rejet (brjordre integer)
   is
      cpt              integer;
      flag             integer;
      manid            maracuja.mandat.man_id%type;
      titid            maracuja.titre.tit_id%type;
      tboordre         integer;
      reduction        integer;
      utlordre         integer;
      dpcoid           integer;
      recid            integer;
      depsuppression   varchar2 (20);
      rpcoid           integer;
      recsuppression   varchar2 (20);
      exeordre         integer;
      depid            integer;
      boridinitial     integer;

      cursor mandats
      is
         select man_id
         from   maracuja.mandat
         where  brj_ordre = brjordre;

      cursor depenses
      is
         select dep_ordre,
                dep_suppression
         from   maracuja.depense
         where  man_id = manid;

      cursor titres
      is
         select tit_id
         from   maracuja.titre
         where  brj_ordre = brjordre;

      cursor recettes
      is
         select rec_ordre,
                rec_suppression
         from   maracuja.recette
         where  tit_id = titid;

      deliq            integer;
   begin
      -- verifier si le bordereau est deja vise
      select count (*)
      into   flag
      from   bordereau_rejet
      where  brj_etat = 'VISE' and brj_ordre = brjordre;

      if (flag > 0) then
         raise_application_error (-20001, 'Ce bordereau a déjà été visé');
      end if;

      open mandats;

      loop
         fetch mandats
         into  manid;

         exit when mandats%notfound;

         select bor_id
         into   boridinitial
         from   mandat
         where  man_id = manid;

         -- memoriser le bor-id du mandat
         open depenses;

         loop
            fetch depenses
            into  dpcoid,
                  depsuppression;

            exit when depenses%notfound;
            -- casser le liens des mand_id dans depense_ctrl_planco
              -- supprimer le liens compteble <-> depense dans l inventaire
            jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, null);

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
            if depsuppression = 'OUI' and tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
               select utl_ordre
               into   utlordre
               from   jefy_depense.depense_budget
               where  dep_id in (select dep_id
                                 from   jefy_depense.depense_ctrl_planco
                                 where  dpco_id = dpcoid);

               select dep_id
               into   depid
               from   jefy_depense.depense_ctrl_planco
               where  dpco_id = dpcoid;

               -- si cest le rejet d'un bordereau de paye
               if (tboordre = 18) then
                  jefy_paye.paye_reversement.viser_rejet_reversement (depid);
               end if;

               jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
            end if;
         end loop;

         close depenses;
      end loop;

      close mandats;

      -- pour les bordereau de PAF, appeler la proc
      jefy_paf.paf_budget.viser_rejet_paf (boridinitial);
      jefy_paye.paye_budget.viser_rejet_papaye (boridinitial);

      open titres;

      loop
         fetch titres
         into  titid;

         exit when titres%notfound;

         open recettes;

         loop
            fetch recettes
            into  rpcoid,
                  recsuppression;

            exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
            select r.rec_id_reduction
            into   reduction
            from   jefy_recette.recette r, jefy_recette.recette_ctrl_planco rpco
            where  rpco.rpco_id = rpcoid and rpco.rec_id = r.rec_id;

            if reduction is not null then
               jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, null);
            else
               jefy_recette.api.upd_recette_ctrl_planco (rpcoid, null);
            end if;

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
            if recsuppression = 'OUI' and tboordre != 200 then
               select utl_ordre
               into   utlordre
               from   jefy_recette.recette_budget
               where  rec_id in (select rec_id
                                 from   jefy_recette.recette_ctrl_planco
                                 where  rpco_id = rpcoid);

               select rec_id
               into   recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = rpcoid;

               if reduction is not null then
                  jefy_recette.api.del_reduction (recid, utlordre);
               else
                  jefy_recette.api.del_recette (recid, utlordre);
               end if;
            end if;
         end loop;

         close recettes;
      end loop;

      close titres;

-- on passe le brjordre a VISE
      update bordereau_rejet
         set brj_etat = 'VISE'
       where brj_ordre = brjordre;
   end;

   function get_selection_id (info varchar)
      return integer
   is
      selection   integer;
   begin
      select maracuja.abricot_bord_selection_seq.nextval
      into   selection
      from   dual;

      return selection;
   end;

   function get_selection_borid (abrid integer)
      return integer
   is
      borid   integer;
   begin
      select distinct bor_id
      into            borid
      from            maracuja.abricot_bord_selection
      where           abr_id = abrid;

      return borid;
   end;

   procedure set_selection_id (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a06tboordre integer, a07abrgroupby varchar, a08gescode varchar)
   is
      chaine     varchar (32000);
      premier    integer;
      tmpdepid   integer;
      tmprecid   integer;
      cpt        integer;
   begin
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

      -- traitement de la chaine des depid
      if a02lesdepid is not null or length (a02lesdepid) > 0 then
         chaine := a02lesdepid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdepid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         tmpdepid,   --DEP_ID
                         null,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      -- traitement de la chaine des recid
      if a03lesrecid is not null or length (a03lesrecid) > 0 then
         chaine := a03lesrecid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmprecid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         null,   --DEP_ID
                         tmprecid,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      select count (*)
      into   cpt
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id in (select dep_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and man_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
      end if;

      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id in (select rec_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and tit_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
      end if;

      bordereau_abricot.creer_bordereau (a01abrid);
   end;

   procedure set_selection_intern (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boriddep   bordereau.bor_id%type;
      boridrec   bordereau.bor_id%type;
      flag       integer;
   begin
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

      -- les mandats
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 201, 'bordereau_1D1M', a08gescodemandat);
-- les titres
      set_selection_id (-a01abrid, null, a03lesrecid, a04utlordre, a05exeordre, 200, 'bordereau_1R1T', a09gescodetitre);

      -- verifier que les bordereaux crees sont coherents entre eux
      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || a01abrid);
      end if;

      select max (bor_id)
      into   boriddep
      from   abricot_bord_selection
      where  abr_id = a01abrid;

      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = -a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || -a01abrid);
      end if;

      select max (bor_id)
      into   boridrec
      from   abricot_bord_selection
      where  abr_id = -a01abrid;

      -- verifier qu'on a 1 prest_id par titre/mandat
      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct tit_id) nb
              from     titre
              where    bor_id = boridrec
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs titres concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct man_id) nb
              from     mandat
              where    bor_id = boriddep
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs mandats concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      ctrl_bordereaux_pi (boriddep, boridrec);
   end;

   procedure set_selection_paye (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boridtmp    integer;
      moisordre   integer;
   begin
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

      -- les mandats de papaye
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 3, 'bordereau_1D1M', a08gescodemandat);
      boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

   -- bascule du brouillard de papaye

   -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
   end;

-- creer bordereau (tbo_ordre) + numerotation
   function get_num_borid (tboordre integer, exeordre integer, gescode varchar, utlordre integer)
      return integer
   is
      cpt      integer;
      borid    integer;
      bornum   integer;
   begin
-- creation du bor_id --
      select bordereau_seq.nextval
      into   borid
      from   dual;

-- creation du bordereau --
      bornum := -1;

      insert into bordereau
                  (bor_date_visa,
                   bor_etat,
                   bor_id,
                   bor_num,
                   bor_ordre,
                   exe_ordre,
                   ges_code,
                   tbo_ordre,
                   utl_ordre,
                   utl_ordre_visa,
                   bor_date_creation
                  )
      values      (null,   --BOR_DATE_VISA,
                   'VALIDE',   --BOR_ETAT,
                   borid,   --BOR_ID,
                   bornum,   --BOR_NUM,
                   -borid,   --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   tboordre,   --TBO_ORDRE,
                   utlordre,   --UTL_ORDRE,
                   null,   --UTL_ORDRE_VISA
                   sysdate
                  );

      return borid;
   end;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   procedure bordereau_1r1t (abrid integer, monborid integer)
   is
      cpt          integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by r.pco_num, r.rec_id asc;
   begin
      open rec_tit;

      loop
         fetch rec_tit
         into  tmprecette;

         exit when rec_tit%notfound;
         cpt := set_titre_recette (tmprecette.rpco_id, monborid);
      end loop;

      close rec_tit;
   end;

   procedure bordereau_nr1t (abrid integer, monborid integer)
   is
      ht           number (12, 2);
      tva          number (12, 2);
      ttc          number (12, 2);
      pconumero    varchar (20);
      nbpieces     integer;
      cpt          integer;
      titidtemp    integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

-- curseur de regroupement
      cursor rec_tit_group_by
      is
         select   r.pco_num,
                  sum (r.rpco_ht_saisie),
                  sum (r.rpco_tva_saisie),
                  sum (r.rpco_ttc_saisie)
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         group by r.pco_num
         order by r.pco_num asc;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and r.pco_num = pconumero
         order by r.pco_num asc, r.rec_id;
   begin
      open rec_tit_group_by;

      loop
         fetch rec_tit_group_by
         into  pconumero,
               ht,
               tva,
               ttc;

         exit when rec_tit_group_by%notfound;
         titidtemp := 0;

         open rec_tit;

         loop
            fetch rec_tit
            into  tmprecette;

            exit when rec_tit%notfound;

            if titidtemp = 0 then
               titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
            else
               update jefy_recette.recette_ctrl_planco
                  set tit_id = titidtemp
                where rpco_id = tmprecette.rpco_id;
            end if;
         end loop;

         close rec_tit;

-- recup du nombre de pieces
-- TODO
         nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

         -- maj des montants du titre
         update titre
            set tit_ht = ht,
                tit_nb_piece = nbpieces,
                tit_ttc = ttc,
                tit_tva = tva,
                tit_libelle = 'TITRE COLLECTIF'
          where tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
      end loop;

      close rec_tit_group_by;
   end;

   procedure bordereau_nd1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;
      abrgroupby   abricot_bord_selection.abr_group_by%type;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
      cursor mand_dep_convra
      is
         select distinct d.*
         from            abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.engage_budget e, maracuja.v_convention_limitative c
         where           d.dpco_id = ab.dep_id and abr_id = abrid and db.dep_id = d.dep_id and e.eng_id = db.eng_id and e.org_id = c.org_id(+) and e.exe_ordre = c.exe_ordre(+) and c.org_id is not null and d.man_id is null and ab.abr_etat = 'ATTENTE'
         order by        d.pco_num asc, d.dep_id;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
   begin
      open mand_dep_convra;

      loop
         fetch mand_dep_convra
         into  tmpdepense;

         exit when mand_dep_convra%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod') then
         cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
      end if;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_fou_rib_pco_mod') then
         cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
      end if;
   end;

   procedure bordereau_1d1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dep_mand
      is
         select   d.*
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d
         where    d.dpco_id = ab.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by d.pco_num asc, d.dep_id;
   begin
      open dep_mand;

      loop
         fetch dep_mand
         into  tmpdepense;

         exit when dep_mand%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close dep_mand;
   end;

   procedure bordereau_1d1m1r1t (abrid integer, boridep integer, boridrec integer)
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      bordereau_1d1m (abrid, boridep);
      bordereau_1r1t (abrid, boridrec);
      ctrl_bordereaux_pi (boridep, boridrec);
   end;

-- les mandats et titres
   function set_mandat_depense (dpcoid integer, borid integer)
      return integer
   is
      cpt               integer;
      flag              integer;
      ladepense         jefy_depense.depense_ctrl_planco%rowtype;
      ladepensepapier   jefy_depense.depense_papier%rowtype;
      leengagebudget    jefy_depense.engage_budget%rowtype;
      gescode           gestion.ges_code%type;
      manid             mandat.man_id%type;
      manorgine_key     mandat.man_orgine_key%type;
      manorigine_lib    mandat.man_origine_lib%type;
      oriordre          mandat.ori_ordre%type;
      prestid           mandat.prest_id%type;
      torordre          mandat.tor_ordre%type;
      virordre          mandat.pai_ordre%type;
      mannumero         mandat.man_numero%type;
      -- montantapayer     mandat.man_ttc%type;
      montantbud        mandat.man_ht%type;
      montanttva        mandat.man_tva%type;
      ttc               mandat.man_ttc%type;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

      select *
      into   ladepense
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select distinct dpp.*
      into            ladepensepapier
      from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = dpcoid;

      select eb.*
      into   leengagebudget
      from   jefy_depense.engage_budget eb, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.eng_id = eb.eng_id and db.dep_id = dpco.dep_id and dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = leengagebudget.org_id and exe_ordre = leengagebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a l''engagement num. ' || leengagebudget.eng_numero || ' n''est pas ouverte sur ' || leengagebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      manorgine_key := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      manorigine_lib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (leengagebudget.org_id, leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

-- creation du man_id --
      select mandat_seq.nextval
      into   manid
      from   dual;

-- recup du numero de mandat
      mannumero := -1;
      montantbud := abricot_util.get_dpco_montant_budgetaire (ladepense.dpco_id);
      --montantapayer := abricot_util.get_dpco_montant_apayer (ladepense.dpco_id);
      ttc := abricot_util.get_dpco_montant_ttc (ladepense.dpco_id);
      montanttva := ttc - montantbud;

      insert into mandat
                  (bor_id,
                   brj_ordre,
                   exe_ordre,
                   fou_ordre,
                   ges_code,
                   man_date_remise,
                   man_date_visa_princ,
                   man_etat,
                   man_etat_remise,
                   man_ht,
                   man_id,
                   man_motif_rejet,
                   man_nb_piece,
                   man_numero,
                   man_numero_rejet,
                   man_ordre,
                   man_orgine_key,
                   man_origine_lib,
                   man_ttc,
                   man_tva,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tor_ordre,
                   pai_ordre,
                   org_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable
                  )
      values      (borid,   --BOR_ID,
                   null,   --BRJ_ORDRE,
                   ladepensepapier.exe_ordre,   --EXE_ORDRE,
                   ladepensepapier.fou_ordre,   --FOU_ORDRE,
                   gescode,   --GES_CODE,
                   null,   --MAN_DATE_REMISE,
                   null,
                   
                   --MAN_DATE_VISA_PRINC,
                   'ATTENTE',   --MAN_ETAT,
                   'ATTENTE',   --MAN_ETAT_REMISE,
                   montantbud,   --MAN_HT,
                   manid,   --MAN_ID,
                   null,   --MAN_MOTIF_REJET,
                   ladepensepapier.dpp_nb_piece,   --MAN_NB_PIECE,
                   mannumero,
                   --MAN_NUMERO,
                   null,   --MAN_NUMERO_REJET,
                   -manid,   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                   manorgine_key,   --MAN_ORGINE_KEY,
                   manorigine_lib,   --MAN_ORIGINE_LIB,
                   --ladepense.dpco_ttc_saisie,   --MAN_TTC,
                   ttc,   --man_ttc
                   --ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,   --MAN_TVA,
                   montanttva,   --man_tva
                   ladepensepapier.mod_ordre,   --MOD_ORDRE,
                   oriordre,   --ORI_ORDRE,
                   ladepense.pco_num,
                   --PCO_NUM,
                   prestid,   --PREST_ID,
                   torordre,   --TOR_ORDRE,
                   virordre,   --VIR_ORDRE
                   leengagebudget.org_id,
                   --org_ordre
                   ladepensepapier.rib_ordre,   --rib ordo
                   ladepensepapier.rib_ordre   -- rib_comptable
                  );

-- maj du man_id  dans la depense
      update jefy_depense.depense_ctrl_planco
         set man_id = manid
       where dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

      -- recup du brouillard
      set_mandat_brouillard (manid);
      return manid;
   end;

-- lesdepid XX$FF$....$DDD$ZZZ$$
   function set_mandat_depenses (lesdpcoid varchar, borid integer)
      return integer
   is
      cpt             integer;
      premier         integer;
      tmpdpcoid       integer;
      chaine          varchar (5000);
      premierdpcoid   integer;
      manid           integer;
      ttc             mandat.man_ttc%type;
      tva             mandat.man_tva%type;
      ht              mandat.man_ht%type;
      utlordre        integer;
      nb_pieces       integer;
   begin
      select count (*)
      into   cpt
      from   dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
      premierdpcoid := null;

      -- traitement de la chaine des depid xx$xx$xx$.....$x$$
      if lesdpcoid is not null or length (lesdpcoid) > 0 then
         chaine := lesdpcoid;

         loop
            premier := 1;

            -- On recupere le depid
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdpcoid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

-- creation du mandat lie au borid
            if premierdpcoid is null then
               manid := set_mandat_depense (tmpdpcoid, borid);

               -- suppression du brouillard car il est uniquement sur la premiere depense
               delete from mandat_brouillard
                     where man_id = manid;

               premierdpcoid := tmpdpcoid;
            else
               -- maj du man_id  dans la depense
               update jefy_depense.depense_ctrl_planco
                  set man_id = manid
                where dpco_id = tmpdpcoid;

               -- recup de la depense (maracuja)
               select distinct dpp.utl_ordre
               into            utlordre
               from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
               where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
            end if;

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
--      select sum (dpco_ttc_saisie),
--             sum (dpco_ttc_saisie - dpco_montant_budgetaire),
--             sum (dpco_montant_budgetaire)
--      into   ttc,
--             tva,
--             ht
--      from   jefy_depense.depense_ctrl_planco
--      where  man_id = manid;
      ttc := abricot_util.get_man_montant_ttc (manid);
      ht := abricot_util.get_man_montant_budgetaire (manid);
      tva := ttc - ht;

-- recup du nb de pieces
      select sum (dpp.dpp_nb_piece)
      into   nb_pieces
      from   jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and man_id = manid;

-- maj du mandat
      update mandat
         set man_ht = ht,
             man_tva = tva,
             man_ttc = ttc,
             man_nb_piece = nb_pieces
       where man_id = manid;

-- recup du brouillard
      set_mandat_brouillard (manid);
      return cpt;
   end;

   function set_titre_recette (rpcoid integer, borid integer)
      return integer
   is
--     jefytitre           jefy.titre%ROWTYPE;
      gescode             gestion.ges_code%type;
      titid               titre.tit_id%type;
      titorginekey        titre.tit_orgine_key%type;
      titoriginelib       titre.tit_origine_lib%type;
      oriordre            titre.ori_ordre%type;
      prestid             titre.prest_id%type;
      torordre            titre.tor_ordre%type;
      modordre            titre.mod_ordre%type;
      presid              integer;
      cpt                 integer;
      virordre            integer;
      flag                integer;
      recettepapier       jefy_recette.recette_papier%rowtype;
      recettebudget       jefy_recette.recette_budget%rowtype;
      facturebudget       jefy_recette.facture_budget%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
      select *
      into   recettectrlplanco
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id = rpcoid;

      select *
      into   recettebudget
      from   jefy_recette.recette_budget
      where  rec_id = recettectrlplanco.rec_id;

      select *
      into   facturebudget
      from   jefy_recette.facture_budget
      where  fac_id = recettebudget.fac_id;

      select *
      into   recettepapier
      from   jefy_recette.recette_papier
      where  rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = facturebudget.org_id and exe_ordre = facturebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a la recette num. ' || recettebudget.rec_numero || ' n''est pas ouverte sur ' || facturebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      titorginekey := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      titoriginelib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (facturebudget.org_id, facturebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

      select titre_seq.nextval
      into   titid
      from   dual;

      insert into titre
                  (bor_id,
                   bor_ordre,
                   brj_ordre,
                   exe_ordre,
                   ges_code,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tit_date_remise,
                   tit_date_visa_princ,
                   tit_etat,
                   tit_etat_remise,
                   tit_ht,
                   tit_id,
                   tit_motif_rejet,
                   tit_nb_piece,
                   tit_numero,
                   tit_numero_rejet,
                   tit_ordre,
                   tit_orgine_key,
                   tit_origine_lib,
                   tit_ttc,
                   tit_tva,
                   tor_ordre,
                   utl_ordre,
                   org_ordre,
                   fou_ordre,
                   mor_ordre,
                   pai_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable,
                   tit_libelle
                  )
      values      (borid,   --BOR_ID,
                   -borid,   --BOR_ORDRE,
                   null,   --BRJ_ORDRE,
                   recettepapier.exe_ordre,   --EXE_ORDRE,
                   gescode,
                   --GES_CODE,
                   null,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                   oriordre,   --ORI_ORDRE,
                   recettectrlplanco.pco_num,   --PCO_NUM,
                   prestid,
                   --PREST_ID,
                   sysdate,   --TIT_DATE_REMISE,
                   null,   --TIT_DATE_VISA_PRINC,
                   'ATTENTE',   --TIT_ETAT,
                   'ATTENTE',   --TIT_ETAT_REMISE,
                   recettectrlplanco.rpco_ht_saisie,   --TIT_HT,
                   titid,   --TIT_ID,
                   null,
                   --TIT_MOTIF_REJET,
                   recettepapier.rpp_nb_piece,   --TIT_NB_PIECE,
                   -1,
                   --TIT_NUMERO, numerotation en fin de transaction
                   null,   --TIT_NUMERO_REJET,
                   -titid,
                   --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                   titorginekey,   --TIT_ORGINE_KEY,
                   titoriginelib,   --TIT_ORIGINE_LIB,
                   recettectrlplanco.rpco_ttc_saisie,   --TIT_TTC,
                   recettectrlplanco.rpco_tva_saisie,   --TIT_TVA,
                   torordre,   --TOR_ORDRE,
                   recettepapier.utl_ordre,   --UTL_ORDRE
                   facturebudget.org_id,   --ORG_ORDRE,
                   recettepapier.fou_ordre,
                   -- FOU_ORDRE  --TOCHECK certains sont nuls...
                   facturebudget.mor_ordre,
                   --MOR_ORDRE
                   null,
                   -- VIR_ORDRE
                   recettepapier.rib_ordre,
                   recettepapier.rib_ordre,
                   recettebudget.rec_lib
                  );

-- maj du tit_id dans la recette
      update jefy_recette.recette_ctrl_planco
         set tit_id = titid
       where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
      return titid;
   end;

   function set_titre_recettes (lesrpcoid varchar, borid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      raise_application_error (-20001, 'OPERATION NON TRAITEE');
      return cpt;
   end;

   function ndep_mand_org_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_org_fou_rib_pco_mod
      is
         select   e.org_id,
                  dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e, jefy_admin.organ vo, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and e.eng_id = db.eng_id and vo.org_id = e.org_id and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, e.org_id, dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      cursor lesdpcoidsribnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_org_fou_rib_pco_mod;

      loop
         fetch ndep_mand_org_fou_rib_pco_mod
         into  orgid,
               fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_org_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsribnull;

            loop
               fetch lesdpcoidsribnull
               into  tmpdpcoid;

               exit when lesdpcoidsribnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsribnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_org_fou_rib_pco_mod;

      return cpt;
   end;

   function ndep_mand_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_fou_rib_pco_mod
      is
         select   dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      cursor lesdpcoidsnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_fou_rib_pco_mod;

      loop
         fetch ndep_mand_fou_rib_pco_mod
         into  fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsnull;

            loop
               fetch lesdpcoidsnull
               into  tmpdpcoid;

               exit when lesdpcoidsnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_fou_rib_pco_mod;

      return cpt;
   end;

-- procedures de verifications
   function selection_valide (abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- meme exercice

      -- si PI somme recette = somme depense

      -- recette_valides

      -- depense_valides
      return cpt;
   end;

   function recette_valide (recid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function depense_valide (depid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function verif_bordereau_selection (borid integer, abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

      -- verifier sum TTC recette selection = sum TTC titre du bord

      -- verifier sum TTC depense  = sum TTC mandat du bord

      -- verifier sum TTC recette  = sum TTC titre  du bord
      return cpt;
   end;

-- procedures de locks de transaction
   procedure lock_mandats
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure lock_titres
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure get_depense_jefy_depense (manid integer)
   is
      depid               depense.dep_id%type;
      jefydepensebudget   jefy_depense.depense_budget%rowtype;
      tmpdepensepapier    jefy_depense.depense_papier%rowtype;
      jefydepenseplanco   jefy_depense.depense_ctrl_planco%rowtype;
      lignebudgetaire     depense.dep_ligne_budgetaire%type;
      fouadresse          depense.dep_adresse%type;
      founom              depense.dep_fournisseur%type;
      lotordre            depense.dep_lot%type;
      marordre            depense.dep_marches%type;
      fouordre            depense.fou_ordre%type;
      gescode             depense.ges_code%type;
      modordre            depense.mod_ordre%type;
      cpt                 integer;
      tcdordre            type_credit.tcd_ordre%type;
      tcdcode             type_credit.tcd_code%type;
      ecd_ordre_ema       ecriture_detail.ecd_ordre%type;
      orgid               integer;
      montantbudgetaire   depense.dep_ht%type;
      montanttva          depense.dep_ht%type;
      montantttc          depense.dep_ht%type;
      montantapayer       depense.dep_ht%type;

      cursor depenses
      is
         select db.*
         from   jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  dpco.man_id = manid and db.dep_id = dpco.dep_id;
   begin
      open depenses;

      loop
         fetch depenses
         into  jefydepensebudget;

         exit when depenses%notfound;

         -- creation du depid --
         select depense_seq.nextval
         into   depid
         from   dual;

         -- creation de lignebudgetaire--
         select org_ub || ' ' || org_cr || ' ' || org_souscr
         into   lignebudgetaire
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         --recuperer le type de credit a partir de la commande
         select tcd_ordre
         into   tcdordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'
         select org_ub,
                org_id
         into   gescode,
                orgid
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         -- fouordre --
         select fou_ordre
         into   fouordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         -- founom --
         founom := getfournisnom (fouordre);

         -- fouadresse --
         select count (*)
         into   cpt
         from   v_depense_adresse
         where  dep_id = jefydepensebudget.dep_id;

         if (cpt = 0) then
            raise_application_error (-20001, 'Pas d''adresse de facturation pour le fournisseur ' || founom);
         end if;

         select substr (adresse, 1, 196)
         into   fouadresse
         from   (select *
                 from   v_depense_adresse
                 where  dep_id = jefydepensebudget.dep_id)
         where  rownum = 1;

         --AND eng_stat !='A'

         -- lotordre --
         select count (*)
         into   cpt
         from   jefy_marches.attribution
         where  att_ordre = (select att_ordre
                             from   jefy_depense.engage_ctrl_marche
                             where  eng_id = jefydepensebudget.eng_id);

         if cpt = 0 then
            lotordre := null;
         else
            select lot_ordre
            into   lotordre
            from   jefy_marches.attribution
            where  att_ordre = (select att_ordre
                                from   jefy_depense.engage_ctrl_marche
                                where  eng_id = jefydepensebudget.eng_id);
         end if;

         -- marordre --
         select count (*)
         into   cpt
         from   jefy_marches.lot
         where  lot_ordre = lotordre;

         if cpt = 0 then
            marordre := null;
         else
            select mar_ordre
            into   marordre
            from   jefy_marches.lot
            where  lot_ordre = lotordre;
         end if;

         --MOD_ORDRE --
         select mod_ordre
         into   modordre
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recuperer l'ecriture_detail pour emargements semi-auto
         select ecd_ordre
         into   ecd_ordre_ema
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         -- recup de la depense papier
         select *
         into   tmpdepensepapier
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recup des infos de depense_ctrl_planco
         select *
         into   jefydepenseplanco
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         montantbudgetaire := abricot_util.get_dpco_montant_budgetaire (jefydepenseplanco.dpco_id);
         montantapayer := abricot_util.get_dpco_montant_apayer (jefydepenseplanco.dpco_id);
         montantttc := abricot_util.get_dpco_montant_ttc (jefydepenseplanco.dpco_id);
         montanttva := montantttc - montantbudgetaire;

         -- creation de la depense --
         insert into depense
         values      (fouadresse,   --DEP_ADRESSE,
                      null,   --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,   --DEP_DATE_RECEPTION,
                      tmpdepensepapier.dpp_date_service_fait,   --DEP_DATE_SERVICE,
                      'VALIDE',   --DEP_ETAT,
                      founom,   --DEP_FOURNISSEUR,
                      montantbudgetaire,   --DEP_HT,
                      depense_seq.nextval,   --DEP_ID,
                      lignebudgetaire,   --DEP_LIGNE_BUDGETAIRE,
                      lotordre,   --DEP_LOT,
                      marordre,   --DEP_MARCHES,
                      montantapayer,   --DEP_MONTANT_DISQUETTE,
                      null,   -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                      substr (tmpdepensepapier.dpp_numero_facture, 1, 199),   --DEP_NUMERO,
                      jefydepenseplanco.dpco_id,   --DEP_ORDRE,
                      null,   --DEP_REJET,
                      tmpdepensepapier.rib_ordre,   --DEP_RIB,
                      'NON',   --DEP_SUPPRESSION,
                      montantttc,   --DEP_TTC,
                      montanttva,   -- DEP_TVA,
                      tmpdepensepapier.exe_ordre,   --EXE_ORDRE,
                      fouordre,   --FOU_ORDRE,
                      gescode,   --GES_CODE,
                      manid,   --MAN_ID,
                      jefydepenseplanco.man_id,   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                      modordre,
                      jefydepenseplanco.pco_num,   --PCO_ORDRE,
                      tmpdepensepapier.utl_ordre,   --UTL_ORDRE
                      orgid,   --org_ordre
                      tcdordre,
                      ecd_ordre_ema,   -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                      tmpdepensepapier.dpp_date_facture
                     );
      end loop;

      close depenses;
   end;

   procedure get_recette_jefy_recette (titid integer)
   is
      recettepapier          jefy_recette.recette_papier%rowtype;
      recettebudget          jefy_recette.recette_budget%rowtype;
      facturebudget          jefy_recette.facture_budget%rowtype;
      recettectrlplanco      jefy_recette.recette_ctrl_planco%rowtype;
      recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%rowtype;
      maracujatitre          maracuja.titre%rowtype;
      adrnom                 varchar2 (200);
      letyperecette          varchar2 (200);
      titinterne             varchar2 (200);
      lbud                   varchar2 (200);
      tboordre               integer;
      cpt                    integer;

      cursor c_recette
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
      open c_recette;

      loop
         fetch c_recette
         into  recettectrlplanco;

         exit when c_recette%notfound;

         select *
         into   recettebudget
         from   jefy_recette.recette_budget
         where  rec_id = recettectrlplanco.rec_id;

         select *
         into   facturebudget
         from   jefy_recette.facture_budget
         where  fac_id = recettebudget.fac_id;

         select *
         into   recettepapier
         from   jefy_recette.recette_papier
         where  rpp_id = recettebudget.rpp_id;

         select *
         into   maracujatitre
         from   maracuja.titre
         where  tit_id = titid;

         if (recettebudget.rec_id_reduction is null) then
            letyperecette := 'R';
         else
            letyperecette := 'T';
         end if;

         select count (*)
         into   cpt
         from   jefy_recette.pi_dep_rec
         where  rec_id = recettectrlplanco.rec_id;

         if cpt > 0 then
            titinterne := 'O';
         else
            titinterne := 'N';
         end if;

         adrnom := getfournisnom (recettepapier.fou_ordre);

         select org_ub || '/' || org_cr || '/' || org_souscr
         into   lbud
         from   jefy_admin.organ
         where  org_id = facturebudget.org_id;

         select distinct tbo_ordre
         into            tboordre
         from            maracuja.titre t, maracuja.bordereau b
         where           b.bor_id = t.bor_id and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
         if tboordre = 200 then
            tboordre := null;
         else
            tboordre := facturebudget.org_id;
         end if;

         insert into recette
         values      (recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      maracujatitre.ges_code,
                      --GES_CODE,
                      null,   --MOD_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM,
                      recettebudget.rec_date_saisie,
                      --jefytitre.tit_date,-- REC_DATE,
                      adrnom,   -- REC_DEBITEUR,
                      recette_seq.nextval,   -- REC_ID,
                      null,   -- REC_IMPUTTVA,
                      null,
                      -- REC_INTERNE, // TODO ROD
                      facturebudget.fac_lib,
                      -- REC_LIBELLE,
                      lbud,   -- REC_LIGNE_BUDGETAIRE,
                      'E',   -- REC_MONNAIE,
                      recettectrlplanco.rpco_ht_saisie,   --HT,
                      recettectrlplanco.rpco_ttc_saisie,   --TTC,
                      recettectrlplanco.rpco_ttc_saisie,
                      --DISQUETTE,
                      recettectrlplanco.rpco_tva_saisie,   --   REC_MONTTVA,
                      facturebudget.fac_numero,   --   REC_NUM,
                      recettectrlplanco.rpco_id,
                      --   REC_ORDRE,
                      recettepapier.rpp_nb_piece,   --   REC_PIECE,
                      facturebudget.fac_numero,
                      
                      --   REC_REF,
                      'VALIDE',   --   REC_STAT,
                      'NON',   --    REC_SUPPRESSION,  Modif Rod
                      letyperecette,   --     REC_TYPE,
                      null,   --     REC_VIREMENT,
                      titid,   --      TIT_ID,
                      -titid,
                      --      TIT_ORDRE,
                      recettebudget.utl_ordre,   --       UTL_ORDRE
                      facturebudget.org_id,
                      --       ORG_ORDRE --ajout rod
                      facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
                      null,   --mod_ordre
                      recettepapier.mor_ordre,
                      --mor_ordre
                      recettepapier.rib_ordre,
                      null
                     );
      end loop;

      close c_recette;
   end;

-- procedures du brouillard
   procedure set_mandat_brouillard (manid integer)
   is
      lemandat              mandat%rowtype;
      tboordre              type_bordereau.tbo_ordre%type;
      sens                  mandat_brouillard.mab_sens%type;
      tcdsect               jefy_admin.type_credit.tcd_sect%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
      modcode               mode_paiement.mod_code%type;
      modlibelle            mode_paiement.mod_libelle%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      tboordre := abricot_util.get_man_tboordre (manid);

      if lemandat.prest_id is null then
         -- creation du brouillard imputation budgetaire --
         sens := inverser_sens_orv (tboordre, 'D');
         montantbudgetaire := lemandat.man_ht;
         pconumbudgetaire := lemandat.pco_num;
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
         -- creation du brouillard de contrepartie
         sens := inverser_sens_orv (tboordre, 'C');

         -- si on est sur un ORV, on recupere le compte de ctp à utiliser
         if sens = 'D' then
            tcdsect := recup_tcdsect (manid);
            pconumctp := util.getpconumvalidefromparam (lemandat.exe_ordre, 'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section' || tcdsect || '.compte');

            if (pconumctp is null) then
               pconumctp := '4632';
            end if;
         else
            pconumctp := abricot_util.get_man_compte_ctp (manid);
            if (pconumctp is null) then
                raise_application_error (-20001, 'Le compte de contrepartie n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;
         end if;

         gescodectp := abricot_util.get_man_gestion_ctp (manid);
         montantctp := abricot_util.get_man_montant_apayer (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, pconumctp);
         --si presence de TVA, on gere la TVA deduite et l eventuelle TVA collectee
         montanttvadeduite := abricot_util.get_man_montant_tva_ded (manid, null);
         montanttvacollectee := abricot_util.get_man_montant_tva_coll (manid, null);

         if (montanttvadeduite <> 0) then
            sens := inverser_sens_orv (tboordre, 'D');
            pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);

            if (pconumtvadeduite is null) then
               raise_application_error (-20001, 'Le compte par defaut de TVA a deduire n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
         end if;

         if (montanttvacollectee <> 0) then
            sens := inverser_sens_orv (tboordre, 'C');
            pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);

            if (pconumtvacollectee is null) then
               select mod_code,
                      mod_libelle
               into   modcode,
                      modlibelle
               from   mode_paiement
               where  mod_ordre = lemandat.mod_ordre;

               raise_application_error (-20001, 'Le compte de TVA à collecter n''est pas parametré pour le mode de paiement (' || modcode || '-' || modlibelle || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
         end if;
      else
         bordereau_abricot.set_mandat_brouillard_intern (manid);
      end if;
   end;

   procedure set_mandat_brouillard_intern (manid integer)
   is
      lemandat              mandat%rowtype;
      chap                  varchar2 (2);
      sens                  mandat_brouillard.mab_sens%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- recup des 2 premiers caracteres du compte
      select substr (lemandat.pco_num, 1, 2)
      into   chap
      from   dual;

      if chap = '18' then
         raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num || ')');
      end if;

      --lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      sens := 'D';
      montantbudgetaire := lemandat.man_ht;
      pconumbudgetaire := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
      sens := 'C';
      gescodectp := abricot_util.get_man_gestion_ctp (manid);
      montantctp := abricot_util.get_man_montant_apayer (manid);
      --pconumctp := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, '181');

      if (montanttvadeduite <> 0) then
         sens := 'D';
         pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
      end if;

      if (montanttvacollectee <> 0) then
         sens := 'C';
         pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
      end if;
   end;

   procedure set_titre_brouillard (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      recid               integer;
      flag                integer;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
      select max (rb.rec_id_reduction)
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

-- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      if letitre.prest_id is null then
         open c_recettes;

         loop
            fetch c_recettes
            into  recettectrlplanco;

            exit when c_recettes%notfound;

            select max (rec_id)
            into   recid
            from   recette
            where  rec_ordre = recettectrlplanco.rpco_id;

            -- verifier que la contrepartie n'est pas passée sur l'imputation du titre
            select count (*)
            into   flag
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id and pco_num = letitre.pco_num;

            if (flag > 0) then
               raise_application_error (-20001, 'La contrepartie ne doit pas etre identique à l''imputation ( : ' || letitre.tit_libelle || ' / ' || letitre.pco_num || ' / ' || letitre.tit_ttc || ')');
            end if;

            -- creation du titre_brouillard visa --
            --  RECETTE_CTRL_PLANCO
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      letitre.ges_code,
                      --GES_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM
                      abs (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                      'VISA TITRE',
                      --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_tva
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,   --PCO_NUM
                      abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                      'VISA TITRE',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_tva
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_ctp
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,
                      --PCO_NUM
                      abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                      'VISA TITRE',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      inverser_sens (lesens),
                      --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_ctp
               where  rpco_id = recettectrlplanco.rpco_id;
         end loop;

         close c_recettes;
      else
         set_titre_brouillard_intern (titid);
      end if;

      -- suppression des lignes d ecritures a ZERO
      delete from titre_brouillard
            where tib_montant = 0;
   end;

   procedure set_titre_brouillard_intern (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      lepconum            maracuja.plan_comptable.pco_num%type;
      libelle             maracuja.plan_comptable.pco_libelle%type;
      chap                varchar2 (2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;
      ctpgescode          titre.ges_code%type;
      pconum_185          gestion_exercice.pco_num_185%type;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

      -- modif fred 04/2007
      select c.ges_code,
             ge.pco_num_185
      into   gescodecompta,
             pconum_185
      from   gestion g, comptabilite c, gestion_exercice ge
      where  g.ges_code = letitre.ges_code and g.com_ordre = c.com_ordre and g.ges_code = ge.ges_code and ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
      select rb.rec_id_reduction
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

      -- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      open c_recettes;

      loop
         fetch c_recettes
         into  recettectrlplanco;

         exit when c_recettes%notfound;

         select max (rec_id)
         into   recid
         from   recette
         where  rec_ordre = recettectrlplanco.rpco_id;

         -- recup des 2 premiers caracteres du compte
         select substr (recettectrlplanco.pco_num, 1, 2)
         into   chap
         from   dual;

         if chap != '18' then
            lepconum := api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
         else
            raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num || ')');
         end if;

         -- creation du titre_brouillard visa --
         --  RECETTE_CTRL_PLANCO
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   letitre.ges_code,
                   --GES_CODE,
                   lepconum,   --PCO_NUM
                   abs (recettectrlplanco.rpco_ht_saisie),
                   
                   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   lesens,
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = recettectrlplanco.rpco_id;

         -- recette_ctrl_planco_tva
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   exe_ordre,   --EXE_ORDRE,
                   gescodecompta,
                   -- ges_code,               --GES_CODE,
                   pco_num,   --PCO_NUM
                   abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_tva
            where  rpco_id = recettectrlplanco.rpco_id;

         -- si on est sur un sacd, la contrepartie reste sur le sacd
         if (pconum_185 is not null) then
            ctpgescode := letitre.ges_code;
         else
            ctpgescode := gescodecompta;
         end if;

         -- recette_ctrl_planco_ctp on force le 181
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   ctpgescode,   --GES_CODE,
                   '181',
                   --PCO_NUM
                   abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id;
      end loop;

      close c_recettes;
   end;

   -- outils
   function inverser_sens_orv (tboordre integer, sens varchar)
      return varchar
   is
      cpt   integer;
   begin
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
      select count (*)
      into   cpt
      from   type_bordereau
      where  tbo_sous_type = 'REVERSEMENTS' and tbo_ordre = tboordre;

      if (cpt != 0) then
         if (sens = 'C') then
            return 'D';
         else
            return 'C';
         end if;
      end if;

      return sens;
   end;

   function recup_gescode (abrid integer)
      return varchar
   is
      gescode   bordereau.ges_code%type;
   begin
      select distinct ges_code
      into            gescode
      from            abricot_bord_selection
      where           abr_id = abrid;

      return gescode;
   end;

   function recup_utlordre (abrid integer)
      return integer
   is
      utlordre   bordereau.utl_ordre%type;
   begin
      select distinct utl_ordre
      into            utlordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return utlordre;
   end;

   function recup_exeordre (abrid integer)
      return integer
   is
      exeordre   bordereau.exe_ordre%type;
   begin
      select distinct exe_ordre
      into            exeordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return exeordre;
   end;

   function recup_tboordre (abrid integer)
      return integer
   is
      tboordre   bordereau.tbo_ordre%type;
   begin
      select distinct tbo_ordre
      into            tboordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return tboordre;
   end;

   function recup_groupby (abrid integer)
      return varchar
   is
      abrgroupby   abricot_bord_selection.abr_group_by%type;
   begin
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

      return abrgroupby;
   end;

   function inverser_sens (sens varchar)
      return varchar
   is
   begin
      if sens = 'D' then
         return 'C';
      else
         return 'D';
      end if;
   end;

   procedure numeroter_bordereau (borid integer)
   is
      cpt_mandat   integer;
      cpt_titre    integer;
   begin
      select count (*)
      into   cpt_mandat
      from   mandat
      where  bor_id = borid;

      select count (*)
      into   cpt_titre
      from   titre
      where  bor_id = borid;

      if cpt_mandat + cpt_titre = 0 then
         raise_application_error (-20001, 'Bordereau  vide');
      else
         numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
         numerotationobject.numeroter_mandat (borid);
-- boucle titre
         numerotationobject.numeroter_titre (borid);
      end if;
   end;

   function traiter_orgid (orgid integer, exeordre integer)
      return integer
   is
      topordre     integer;
      cpt          integer;
      orilibelle   origine.ori_libelle%type;
      convordre    integer;
   begin
      if orgid is null then
         return null;
      end if;

      select count (*)
      into   cpt
      from   accords.convention_limitative
      where  org_id = orgid and exe_ordre = exeordre;

      if cpt > 0 then
         -- recup du type_origine CONVENTION--
         select top_ordre
         into   topordre
         from   type_operation
         where  top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

         select distinct con_ordre
         into            convordre
         from            accords.convention_limitative
         where           org_id = orgid and exe_ordre = exeordre;

         select (exe_ordre || '-' || lpad (con_index, 5, '0') || ' ' || con_objet)
         into   orilibelle
         from   accords.contrat
         where  con_ordre = convordre;
      else
         select count (*)
         into   cpt
         from   jefy_admin.organ
         where  org_id = orgid and org_lucrativite = 1;

         if cpt = 1 then
            -- recup du type_origine OPERATION LUCRATIVE --
            select top_ordre
            into   topordre
            from   type_operation
            where  top_libelle = 'OPERATION LUCRATIVE';

            --le libelle utilisateur pour le suivie en compta --
            select org_ub || '-' || org_cr || '-' || org_souscr
            into   orilibelle
            from   jefy_admin.organ
            where  org_id = orgid;
         else
            return null;
         end if;
      end if;

-- l origine est t elle deja  suivie --
      select count (*)
      into   cpt
      from   origine
      where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid;

      if cpt >= 1 then
         select ori_ordre
         into   cpt
         from   origine
         where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid and rownum = 1;
      else
         select origine_seq.nextval
         into   cpt
         from   dual;

         insert into origine
                     (ori_entite,
                      ori_key_name,
                      ori_libelle,
                      ori_ordre,
                      ori_key_entite,
                      top_ordre
                     )
         values      ('JEFY_ADMIN',
                      'ORG_ID',
                      orilibelle,
                      cpt,
                      orgid,
                      topordre
                     );
      end if;

      return cpt;
   end;

   procedure controle_bordereau (borid integer)
   is
      ttc             maracuja.titre.tit_ttc%type;
      detailttc       maracuja.titre.tit_ttc%type;
      ordottc         maracuja.titre.tit_ttc%type;
      debit           maracuja.titre.tit_ttc%type;
      credit          maracuja.titre.tit_ttc%type;
      cpt             integer;
      message         varchar2 (50);
      messagedetail   varchar2 (50);
   begin
      select count (*)
      into   cpt
      from   maracuja.titre
      where  bor_id = borid;

      if cpt = 0 then
-- somme des maracuja.titre
         select sum (man_ttc)
         into   ttc
         from   maracuja.mandat
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (d.dep_ttc)
         into   detailttc
         from   maracuja.mandat m, maracuja.depense d
         where  m.man_id = d.man_id and m.bor_id = borid;

-- la somme des credits
         select sum (mab_montant)
         into   credit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'C';

-- la somme des debits
         select sum (mab_montant)
         into   debit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'D';

-- somme des jefy.recette
         select sum (d.dpco_ttc_saisie)
         into   ordottc
         from   maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         where  m.man_id = d.man_id and m.bor_id = borid;

         message := ' mandats ';
         messagedetail := ' depenses ';
      else
-- somme des maracuja.titre
         select sum (tit_ttc)
         into   ttc
         from   maracuja.titre
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (r.rec_monttva + r.rec_mont)
         into   detailttc
         from   maracuja.titre t, maracuja.recette r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

-- la somme des credits
         select sum (tib_montant)
         into   credit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'C';

-- la somme des debits
         select sum (tib_montant)
         into   debit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'D';

-- somme des jefy.recette
         select sum (r.rpco_ttc_saisie)
         into   ordottc
         from   maracuja.titre t, jefy_recette.recette_ctrl_planco r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

         message := ' titres ';
         messagedetail := ' recettes ';
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  debit <> credit : ' || debit || ' ' || credit);
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  ecriture <> budgetaire : ' || debit || ' ' || ttc);
      end if;

-- somme des maracuja.titre = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (detailttc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant des ' || messagedetail || ' :' || ttc || ' ' || detailttc);
      end if;

-- somme des jefy.recette = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (ordottc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant ordonnateur des ' || messagedetail || ' :' || ttc || ' ' || ordottc);
      end if;

      bordereau_abricot.ctrl_date_exercice (borid);
   end;

   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_pi (boriddep integer, boridrec integer)
   is
      flag         integer;
      nbdep        integer;
      nbrec        integer;
      manid        mandat.man_id%type;
      titid        titre.tit_id%type;
      tmpmandat    mandat%rowtype;
      tmptitre     titre%rowtype;
      tmpprest     integer;
      montantdep   mandat.man_ht%type;
      montantrec   titre.tit_ht%type;

      cursor prests
      is
         select distinct prest_id
         from            (select prest_id
                          from   mandat
                          where  bor_id = boriddep
                          union
                          select prest_id
                          from   titre
                          where  bor_id = boridrec);
   begin
      if (boriddep is null) then
         raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.');
      end if;

      if (boridrec is null) then
         raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.');
      end if;

      -- verifier qu'il s'agit bien de bordereaux de PI
      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 201 and bor_id = boriddep;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.');
      end if;

      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 200 and bor_id = boridrec;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.');
      end if;

      -- comparer le nombre de titres et de mandats
      select count (*)
      into   nbdep
      from   mandat
      where  bor_id = boriddep;

      select count (*)
      into   nbrec
      from   titre
      where  bor_id = boridrec;

      if (nbdep = 0) then
         raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
      end if;

      if (nbdep <> nbrec) then
         raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbdep || ' / Titres : ' || nbrec);
      end if;

      open prests;

      loop
         fetch prests
         into  tmpprest;

         exit when prests%notfound;

         select count (*)
         into   nbdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select count (*)
         into   nbrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (nbdep <> nbrec) then
            raise_application_error (-20001, 'Incoherence : Nombre de titres (' || nbrec || ') different du nombre de mandats (' || nbdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;

         select sum (man_ht)
         into   montantdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select sum (tit_ht)
         into   montantrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (montantdep <> montantrec) then
            raise_application_error (-20001, 'Incoherence : Montant des titres (' || montantrec || ') different du montant des mandats  (' || montantdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;
      end loop;

      close prests;

      select sum (man_ht)
      into   montantdep
      from   mandat
      where  bor_id = boriddep;

      select sum (tit_ht)
      into   montantrec
      from   titre
      where  bor_id = boriddep;

      if (montantdep <> montantrec) then
         raise_application_error (-20001, 'Incoherence : Montant total des titres (' || montantrec || ') different du montant total des mandats  (' || montantdep || ')');
      end if;
   end;

   procedure get_recette_prelevements (titid integer)
   is
      cpt                      integer;
      facture_titre_data       prestation.facture_titre%rowtype;
--     client_data              prelev.client%ROWTYPE;
      oriordre                 integer;
      modordre                 integer;
      recid                    integer;
      echeid                   integer;
      echeancier_data          jefy_echeancier.echeancier%rowtype;
      echeancier_prelev_data   jefy_echeancier.echeancier_prelev%rowtype;
      facture_data             jefy_recette.facture_budget%rowtype;
      personne_data            grhum.v_personne%rowtype;
      premieredate             date;
   begin
-- verifier s il existe un echancier pour ce titre
      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

      if (cpt != 1) then
         return;
      end if;

-- recup du eche_id / ech_id
      select eche_id
      into   echeid
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

-- recup du des infos du prelevements
      select *
      into   echeancier_data
      from   jefy_echeancier.echeancier
      where  ech_id = echeid;

      select *
      into   echeancier_prelev_data
      from   jefy_echeancier.echeancier_prelev
      where  ech_id = echeid;

      select *
      into   facture_data
      from   jefy_recette.facture_budget
      where  eche_id = echeid;

      select *
      into   personne_data
      from   grhum.v_personne
      where  pers_id = facture_data.pers_id;

      select echd_date_prevue
      into   premieredate
      from   jefy_echeancier.echeancier_detail
      where  echd_numero = 1 and ech_id = echeid;

      select rec_id
      into   recid
      from   recette
      where  tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

      -- recup ??
      oriordre := gestionorigine.traiter_orgid (facture_data.org_id, facture_data.exe_ordre);

      insert into maracuja.echeancier
                  (eche_autoris_signee,
                   fou_ordre_client,
                   con_ordre,
                   eche_date_1ere_echeance,
                   eche_date_creation,
                   eche_date_modif,
                   eche_echeancier_ordre,
                   eche_etat_prelevement,
                   ft_ordre,
                   eche_libelle,
                   eche_montant,
                   eche_montant_en_lettres,
                   eche_nombre_echeances,
                   eche_numero_index,
                   org_ordre,
                   prest_ordre,
                   eche_prise_en_charge,
                   eche_ref_facture_externe,
                   eche_supprime,
                   exe_ordre,
                   tit_id,
                   rec_id,
                   tit_ordre,
                   ori_ordre,
                   pers_id,
                   org_id,
                   pers_description
                  )
      values      ('O',   --ECHE_AUTORIS_SIGNEE
                   facture_data.fou_ordre,   --FOU_ORDRE_CLIENT
                   null,
                   --echancier_data.CON_ORDRE  ,--CON_ORDRE
                   premieredate,
                   --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                   sysdate,
                   --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                   sysdate,   --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                   echeancier_data.ech_id,
                   
                   --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                   'V',
                   --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                   facture_data.fac_id,
                   --echancier_data.FT_ORDRE  ,--FT_ORDRE
                   echeancier_data.ech_libelle,
                   --echancier_data.LIBELLE,--ECHE_LIBELLE
                   echeancier_data.ech_montant,   --ECHE_MONTANT
                   echeancier_data.ech_montant_lettres,
                   --ECHE_MONTANT_EN_LETTRES
                   echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                   echeancier_data.ech_id,
                   --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                   facture_data.org_id,
                   --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                   null,
                   
                   --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                   'O',   --ECHE_PRISE_EN_CHARGE
                   facture_data.fac_lib,
                   
                   --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                   'N',   --ECHE_SUPPRIME
                   facture_data.exe_ordre,
                   --EXE_ORDRE
                   titid,
                   recid,   --REC_ID,
                   -titid,
                   oriordre,   --ORI_ORDRE,
                   personne_data.pers_id,
                   --CLIENT_data.pers_id  ,--PERS_ID
                   facture_data.org_id,   --orgid a faire plus tard....
                   personne_data.pers_libelle   --    PERS_DESCRIPTION
                  );

      insert into maracuja.prelevement
                  (eche_echeancier_ordre,
                   reco_ordre,
                   fou_ordre,
                   prel_commentaire,
                   prel_date_modif,
                   prel_date_prelevement,
                   prel_prelev_date_saisie,
                   prel_prelev_etat,
                   prel_numero_index,
                   prel_prelev_montant,
                   prel_prelev_ordre,
                   rib_ordre,
                   prel_etat_maracuja
                  )
         select ech_id,   --ECHE_ECHEANCIER_ORDRE
                null,   --PREL_FICP_ORDRE
                facture_data.fou_ordre,   --FOU_ORDRE
                echd_commentaire,
                --PREL_COMMENTAIRE
                sysdate,
                --DATE_MODIF,--PREL_DATE_MODIF
                echd_date_prevue,   --PREL_DATE_PRELEVEMENT
                sysdate,   --,--PREL_PRELEV_DATE_SAISIE
                'ATTENTE',   --PREL_PRELEV_ETAT
                echd_numero,
                --PREL_NUMERO_INDEX
                echd_montant,
                --PREL_PRELEV_MONTANT
                echd_id,   --PREL_PRELEV_ORDRE
                echeancier_prelev_data.rib_ordre_debiteur,
                
                --RIB_ORDRE
                'ATTENTE'   --PREL_ETAT_MARACUJA
         from   jefy_echeancier.echeancier_detail
         where  ech_id = echeancier_data.ech_id;
   end;

   procedure ctrl_date_exercice (borid integer)
   is
      exeordre   integer;
      annee      integer;
   begin
      select to_char (bor_date_creation, 'YYYY'),
             exe_ordre
      into   annee,
             exeordre
      from   bordereau
      where  bor_id = borid and exe_ordre >= 2007;

      if exeordre <> annee then
         update bordereau
            set bor_date_creation = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update mandat
            set man_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update titre
            set tit_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;
      end if;
   end;

-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.
   function get_ges_code_for_man_id (manid number)
      return comptabilite.ges_code%type
   is
      current_mandat          mandat%rowtype;
      pconumsacd              gestion_exercice.pco_num_185%type;
      visa_mode_paiement      planco_visa.pvi_contrepartie_gestion%type;
      visa_planco             planco_visa.pvi_contrepartie_gestion%type;
      code_agence_comptable   comptabilite.ges_code%type;
   begin
      select ges_code
      into   code_agence_comptable
      from   comptabilite;

      select *
      into   current_mandat
      from   mandat
      where  man_id = manid;

      -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
      select pco_num_185
      into   pconumsacd
      from   gestion_exercice
      where  exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

      if (pconumsacd is not null) then
         return current_mandat.ges_code;
      else   -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
         -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
         select mod_contrepartie_gestion
         into   visa_mode_paiement
         from   mode_paiement
         where  mod_ordre = current_mandat.mod_ordre;

         if (visa_mode_paiement is not null) then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         else   -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
            select pvi_contrepartie_gestion
            into   visa_planco
            from   planco_visa
            where  pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         end if;
      end if;
   end;

   function getfournisnom (fouordre integer)
      return varchar
   is
      founom   varchar2 (200);
   begin
      founom := '';

      select substr (nom || ' ' || prenom, 1, 200)
      into   founom
      from   v_fournis_light
      where  fou_ordre = fouordre;

      return founom;
   end;

   function recup_tcdsect (manid integer)
      return varchar
   is
      tcdsect   jefy_admin.type_credit.tcd_sect%type;
   begin
      select tc.tcd_sect
      into   tcdsect
      from   jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.type_credit tc
      where  dpco.dep_id = db.dep_id and db.eng_id = eb.eng_id and eb.tcd_ordre = tc.tcd_ordre and man_id = manid;

      return tcdsect;
   end;
end;
/



create or replace force view maracuja.v_fournis_adr (fou_ordre, pers_id, adr_ordre)
as
   select f.fou_ordre,
          f.pers_id,
          a.adr_ordre
   from   grhum.fournis_ulr f,
          grhum.adresse a,
          grhum.personne p,
          (select pers_id,
                  max(a.adr_ordre) adr_ordre
           from   grhum.repart_personne_adresse rpa, grhum.adresse a
           where  rpa.adr_ordre = a.adr_ordre and rpa.tadr_code = 'FACT' and rpa.rpa_valide = 'O' and rpa.rpa_principal = 'O'
           group by pers_id
           union
           select   pers_id,
                    max (a.adr_ordre) adr_ordre
           from     grhum.repart_personne_adresse rpa, grhum.adresse a
           where    rpa.adr_ordre = a.adr_ordre and rpa.tadr_code = 'FACT' and rpa.rpa_valide = 'O' 
           and not exists (select 1
                                from   grhum.repart_personne_adresse rpa2, grhum.adresse a
                                where  rpa2.adr_ordre = a.adr_ordre and rpa2.tadr_code = 'FACT' and rpa2.rpa_valide = 'O' and rpa2.rpa_principal = 'O' and rpa2.pers_id=rpa.pers_id)
           group by pers_id) x
   where  p.pers_id = x.pers_id(+) and x.adr_ordre = a.adr_ordre(+) and f.pers_id = p.pers_id;
   /
   
create or replace force view maracuja.v_fournisseur (fou_ordre,
                                                     pers_id,
                                                     adr_ordre,
                                                     fou_code,
                                                     fou_date,
                                                     fou_marche,
                                                     fou_valide,
                                                     agt_ordre,
                                                     fou_type,
                                                     d_creation,
                                                     d_modification,
                                                     cpt_ordre,
                                                     fou_etranger,
                                                     adr_adresse1,
                                                     adr_adresse2,
                                                     adr_cp,
                                                     adr_ville,
                                                     adr_nom,
                                                     adr_prenom,
                                                     adr_civilite,
                                                     cp_etranger,
                                                     lc_pays
                                                    )
as
   select f.fou_ordre,
          f.pers_id,
          a.adr_ordre,
          f.fou_code,
          f.fou_date,
          f.fou_marche,
          f.fou_valide,
          f.agt_ordre,
          f.fou_type,
          f.d_creation,
          f.d_modification,
          f.cpt_ordre,
          f.fou_etranger,
          a.adr_adresse1,
          a.adr_adresse2,
          a.code_postal as adr_adr_cp,
          a.ville as adr_ville,
          p.pers_libelle,
          p.pers_lc,
          p.pers_type,
          cp_etranger,
          lc_pays
   from   grhum.fournis_ulr f,
          grhum.adresse a,
          grhum.personne p,
 (select   pers_id,
                    max (a.adr_ordre) adr_ordre
           from     grhum.repart_personne_adresse rpa, grhum.adresse a
           where    rpa.adr_ordre = a.adr_ordre and rpa.tadr_code = 'FACT' and rpa.rpa_valide = 'O' and rpa.rpa_principal = 'O'
           group by pers_id
           union
           select   pers_id,
                    max (a.adr_ordre) adr_ordre
           from     grhum.repart_personne_adresse rpa, grhum.adresse a
           where    rpa.adr_ordre = a.adr_ordre
           and      rpa.tadr_code = 'FACT'
           and      rpa.rpa_valide = 'O'
           and      not exists (select 1
                                from   grhum.repart_personne_adresse rpa2, grhum.adresse a
                                where  rpa2.adr_ordre = a.adr_ordre and rpa2.tadr_code = 'FACT' and rpa2.rpa_valide = 'O' and rpa2.rpa_principal = 'O' and rpa2.pers_id = rpa.pers_id)
           group by pers_id) x,
          grhum.pays pa
   where  p.pers_id = x.pers_id(+) and x.adr_ordre = a.adr_ordre(+) and f.pers_id = p.pers_id and a.c_pays = pa.c_pays(+);
/


CREATE OR REPLACE VIEW MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 CLE_RIB, DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, 
 PERS_TYPE, PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, 
 ORG_UB, ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, 
 TCD_LIBELLE, DPP_NUMERO_FACTURE, DPP_ID, TAP_TAUX, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, 
 DPP_NB_PIECE, UTL_ORDRE, TBO_ORDRE, DEP_ID, PCO_NUM, 
 DPCO_ID, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, ADR_CIVILITE, 
 ADR_NOM, ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, 
 ADR_CP, UTLNOMPRENOM)
AS 
SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic, r.cle_rib,
          bq.domiciliation, mp.mod_libelle, mp.mod_code, mp.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, eb.org_id,
          o.org_ub, o.org_cr, o.org_souscr, eb.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, dp.dpp_numero_facture, dp.dpp_id, tp.tap_taux, dp.dpp_ht_saisie,
          dp.dpp_tva_saisie, dp.dpp_ttc_saisie, dp.dpp_date_facture,
          dp.dpp_date_saisie, dp.dpp_date_reception, dp.dpp_date_service_fait,
          dp.dpp_nb_piece, dp.utl_ordre, dcp.tbo_ordre, dcp.dep_id dep_id,
          dcp.pco_num, dcp.dpco_id dpco_id, dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie, dcp.dpco_ttc_saisie, f.adr_civilite, f.adr_nom,
          f.adr_prenom, f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o,
          jefy_admin.taux_prorata tp                                               --,
--jefy_admin.type_credit tcd
   WHERE  eb.eng_id = db.eng_id
      AND db.dpp_id = dp.dpp_id
      AND db.dep_id = dcp.dep_id
      AND db.tap_id = tp.tap_id
      AND dp.fou_ordre = f.fou_ordre
      AND dp.exe_ordre = e.exe_ordre
      AND dp.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND dp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mp.mod_ordre = dp.mod_ordre
      AND eb.tcd_ordre = tc.tcd_ordre
      AND o.org_id = eb.org_id
      AND u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
      AND man_id IS NULL
      AND tbo_ordre != 201;
/

------------------------

CREATE OR REPLACE PACKAGE MARACUJA.api_plsql_journal
is
/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

   -- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
   procedure validerecriture (ecrordre integer);

-- permet d'annuler une ecriture saisie .
   procedure annulerecriture (ecrordre integer);

-- permet de creer une ecriture de balance d entree --
   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer;

   -- permet de creer une ecriture d exercice --
   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer;

   -- permet de creer une ecriture de fin d exercice --
   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer;

-- permet de creer une ecriture --
   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL
   )
      return integer;

-- permet d ajouter des details a une ecriture.
   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer;

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type);

-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      ecrnumero_brouillard   number,
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_plsql_journal
is
-- PUBLIC --
   procedure validerecriture (ecrordre integer)
   is
      cpt      integer;
      debit    number;
      credit   number;
   begin
      select count (*)
      into   cpt
      from   ecriture_detail
      where  ecr_ordre = ecrordre;

      if cpt >= 2 then
         select sum (ecd_debit)
         into   debit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         select sum (ecd_credit)
         into   credit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         if (credit <> debit) then
            raise_application_error (-20001, 'Ecriture desequilibree');
         end if;

         numerotationobject.numeroter_ecriture (ecrordre);
      else
         raise_application_error (-20001, 'MAUVAIS FORMAT D ECRITURE !');
      end if;
   end;

   procedure annulerecriture (ecrordre integer)
   is
      cpt   integer;
   begin
      select ecr_numero
      into   cpt
      from   ecriture
      where  ecr_ordre = ecrordre;

      if cpt = 0 then
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      end if;
   end;

   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      tjoordre   number;
   begin
-- recup du type_journal....
      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL BALANCE ENTREE';

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer
   is
      cpt   number;
   begin
-- recup du type_journal....
      select count (*)
      into   cpt
      from   type_journal
      where  tjo_ordre = tjoordre;

      if cpt = 0 then
         raise_application_error (-20001, 'TYPE DE JOURNAL INCONNU !');
      end if;

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select 1
      into   cpt
      from   dual;

      return cpt;
   end;

   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      cpt   integer;
   begin
      return api_plsql_journal.creerecritureprivate (null, comordre,   --        NOT NULL,
                                                     ecrdate,   --         NOT NULL,
                                                     ecrlibelle,   -- (200)  NOT NULL,
                                                     null, exeordre,   --        NOT NULL,
                                                     oriordre, tjoordre,   --        NOT NULL,
                                                     topordre,   --        NOT NULL,
                                                     utlordre   --        NOT NULL,
                                                             );
   end;

   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      -- EXEORDRE          NUMBER,--        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer
   is
      exeordre   number;
   begin
      select exe_ordre
      into   exeordre
      from   ecriture
      where  ecr_ordre = ecrordre;

      return api_plsql_journal.creerecrituredetailprivate (ecdcommentaire,   -- (200),
                                                           ecdlibelle,   -- (200),
                                                           ecdmontant,   -- (12,2) NOT NULL,
                                                           ecdsecondaire,   -- (20),
                                                           ecdsens,   -- (1)  NOT NULL,
                                                           ecrordre,   --        NOT NULL,
                                                           exeordre,   --        NOT NULL,
                                                           gescode,   -- (10)  NOT NULL,
                                                           pconum   -- (20)  NOT NULL
                                                                 );
   end;

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type)
   is
   begin
      update ecriture_detail
         set ecd_index = ecdindex
       where ecd_ordre = ecdordre;
   end;

-- PRIVATE --
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      --ECRNUMERO             NUMBER,-- (32),
      ecrnumero_brouillard   number,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer
   is
      ecrordre      integer;
      ecrdatenew    date;
      exercicerec   exercice%rowtype;
   begin
      ecrdatenew := ecrdate;

      select *
      into   exercicerec
      from   exercice
      where  exe_ordre = exeordre;

-- si exercice pas ouvert et pas restreint on bloque
      if (exercicerec.exe_stat <> 'O' and exercicerec.exe_stat <> 'R') then
         raise_application_error (-20001, 'Impossible de creer une ecriture sur un exercice non ouvert ou non restreint.');
      end if;

-- si exercice restreint, changer la date
      if (exercicerec.exe_stat = 'R') then
         ecrdatenew := to_date ('31/12/' || exeordre, 'dd/mm/yyyy');
      end if;

      select ecriture_seq.nextval
      into   ecrordre
      from   dual;

      insert into ecriture
      values      (broordre,
                   comordre,
                   sysdate,
                   ecrdatenew,
                   'VALIDE',
                   ecrlibelle,
                   0,
                   ecrnumero_brouillard,
                   ecrordre,
                   ecrlibelle,
                   exeordre,
                   oriordre,
                   tjoordre,
                   topordre,
                   utlordre
                  );

      return ecrordre;
   end;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer
   is
      ecdordre    number;
      ecdcredit   number;
      ecddebit    number;
   begin
      select ecriture_detail_seq.nextval
      into   ecdordre
      from   dual;

      if ecdsens = 'C' then
         ecdcredit := ecdmontant;
         ecddebit := 0;
      else
         ecdcredit := 0;
         ecddebit := ecdmontant;
      end if;

      insert into ecriture_detail
      values      (ecdcommentaire,   -- (200),
                   ecdcredit,   -- (12,2),
                   ecddebit,   -- (12,2),
                   ecrordre,   --        NOT NULL,
                   ecdlibelle,   -- (200),
                   ecdmontant,   -- (12,2) NOT NULL,
                   ecdordre,   --        NOT NULL,
                   null,   -- (200),
                   abs (ecdmontant),   -- (12,2) NOT NULL,
                   ecdsecondaire,   -- (20),
                   ecdsens,   -- (1)  NOT NULL,
                   ecrordre,   --        NOT NULL,
                   exeordre,   --        NOT NULL,
                   gescode,   -- (10)  NOT NULL,
                   pconum   -- (20)  NOT NULL
                  );

      return ecdordre;
   end;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;


--------------------------------

CREATE OR REPLACE PACKAGE MARACUJA.cptefiutil IS
    PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER,libelle VARCHAR, rtatcomp VARCHAR);
    PROCEDURE annuler_ecriture (monecdordre INTEGER);
    PROCEDURE numeroter_ecriture (ecrordre INTEGER);
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.cptefiutil
is
   procedure solde_6_7 (exeordre integer, utlordre integer, libelle varchar, rtatcomp varchar)
   is
      legescode       gestion.ges_code%type;
      lepconum        plan_comptable_exer.pco_num%type;
      lesolde         number (12, 2);
      debit           number (12, 2);
      credit          number (12, 2);
      credit120       number (12, 2);
      debit120        number (12, 2);
      credit129       number (12, 2);
      debit129        number (12, 2);

      cursor solde6
      is
         select   ges_code,
                  pco_num,
                  solde_crediteur
         from     cfi_solde_6_7
         where    solde_crediteur != 0 and (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre
         order by ges_code, pco_num;

      cursor solde7
      is
         select   ges_code,
                  pco_num,
                  solde_crediteur
         from     cfi_solde_6_7
         where    solde_crediteur != 0 and (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre
         order by ges_code, pco_num;

      cursor lesgescode
      is
         select distinct ges_code,
                         pco_num_185
         from            gestion_exercice
         where           exe_ordre = exeordre
         order by        ges_code;

      cursor lessacds
      is
         select distinct ges_code
         from            gestion_exercice
         where           exe_ordre = exeordre and pco_num_185 is not null
         order by        ges_code;

      nb              integer;
      gescodeagence   comptabilite.ges_code%type;
      comordre        comptabilite.com_ordre%type;
      topordre        type_operation.top_ordre%type;
      tjoordre        type_journal.tjo_ordre%type;
      monecdordre     ecriture.ecr_ordre%type;
      ecdordre        ecriture_detail.ecd_ordre%type;
      exeexercice     exercice.exe_exercice%type;
      ecdindex        ecriture_detail.ecd_index%type;
   begin
-- creation de l'ecriture de solde --
      select top_ordre
      into   topordre
      from   type_operation
      where  top_libelle = 'ECRITURE SOLDE 6 ET 7';

      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL FIN EXERCICE';

      select com_ordre,
             ges_code
      into   comordre,
             gescodeagence
      from   comptabilite;

      select exe_exercice
      into   exeexercice
      from   exercice
      where  exe_ordre = exeordre;

-- creation de lecriture  --
      monecdordre := maracuja.api_plsql_journal.creerecriture (comordre, to_date ('31/12/' || exeexercice, 'dd/mm/yyyy'), libelle, exeordre, null,   --ORIORDRE,
                                                               tjoordre, topordre, utlordre);
      ecdindex := 1;

-- Solde classe 6 et 186, par gescode
      open solde6;

      loop
         fetch solde6
         into  legescode,
               lepconum,
               lesolde;

         exit when solde6%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;

         api_plsql_journal.setecdindex (ecdordre, ecdindex);
         ecdindex := ecdindex + 1;
      end loop;

      close solde6;

-- Solde classe 7 et 187, par gescode
      open solde7;

      loop
         fetch solde7
         into  legescode,
               lepconum,
               lesolde;

         exit when solde7%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;

         api_plsql_journal.setecdindex (ecdordre, ecdindex);
         ecdindex := ecdindex + 1;
      end loop;

      close solde7;

-- Calcul du résultat par composante ---
-- Etablissement + SACD --
      if rtatcomp = 'O' then
         -- creation de l ecriture de resultat --
         open lesgescode;

         loop
            fetch lesgescode
            into  legescode,
                  lepconum;

            exit when lesgescode%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  composante SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129

                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --
                                    -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
               end if;
            end if;
         end loop;

         close lesgescode;
      else
         -- resultat = 'Etablissement' --
         select count (*)
         into   nb
         from   cfi_solde_6_7
         where  exe_ordre = exeordre and ges_code in (select ges_code
                                                      from   gestion_exercice
                                                      where  exe_ordre = exeordre and pco_num_185 is null);

         if nb != 0 then
            select nvl (-sum (solde_crediteur), 0)
            into   debit
            from   cfi_solde_6_7
            where  (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            select nvl (sum (solde_crediteur), 0)
            into   credit
            from   cfi_solde_6_7
            where  (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            if debit > credit then
               -- RESULTAT SOLDE DEBITEUR 129
                              --  contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
               api_plsql_journal.setecdindex (ecdordre, ecdindex);
               ecdindex := ecdindex + 1;
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
               api_plsql_journal.setecdindex (ecdordre, ecdindex);
               ecdindex := ecdindex + 1;
            else
               -- RESULTAT SOLDE CREDITEUR 120 --
                              -- contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
               api_plsql_journal.setecdindex (ecdordre, ecdindex);
               ecdindex := ecdindex + 1;
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
               api_plsql_journal.setecdindex (ecdordre, ecdindex);
               ecdindex := ecdindex + 1;
            end if;
         end if;

         -- Résultat des SACD ---
         open lessacds;

         loop
            fetch lessacds
            into  legescode;

            exit when lessacds%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129

                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --

                  -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  api_plsql_journal.setecdindex (ecdordre, ecdindex);
                  ecdindex := ecdindex + 1;
               end if;
            end if;
         end loop;

         close lessacds;
      end if;

-- on numerote pour eviter une numerotation automatique.
-- voir procedure numeroter_ecriture
      update ecriture
         set ecr_numero = 99999999
       where ecr_ordre = monecdordre;
   end;

--------------------------------------------------------------------------------
   procedure numeroter_ecriture (ecrordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour numeroter
--definitivement lecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = ecrordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE MODIFIER UNE ECRITURE DU JOURNAL !');
      else
         update ecriture
            set ecr_numero = 0
          where ecr_ordre = ecrordre;

         maracuja.api_plsql_journal.validerecriture (ecrordre);
      end if;
   end;

--------------------------------------------------------------------------------
   procedure annuler_ecriture (monecdordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour ANNULER
--definitivement l'ecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = monecdordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         delete from ecriture_detail
               where ecr_ordre = monecdordre;

         delete from ecriture
               where ecr_ordre = monecdordre;
      end if;
   end;
end;
/


---------------------







create or replace procedure grhum.inst_patch_maracuja_1913 is
begin
	
    update maracuja.mandat_brouillard set pco_num='181' where mab_ordre in (
    select mab_ordre from maracuja.mandat_brouillard mab, maracuja.mandat m where m.man_id=mab.man_id and prest_id is not null and mab.pco_num ='18181' and mab.exe_ordre>=2011
    );
    
    update maracuja.ecriture_detail set pco_num='181' where ecd_ordre in (
    select ecd.ecd_ordre from maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.mandat m where ecd.ecd_ordre=mde.ecd_ordre and m.man_id=mde.man_id and prest_id is not null and ecd.pco_num ='18181' and ecd.exe_ordre>=2011
    );
	JEFY_ADMIN.PATCH_UTIL.end_PATCH ( 4, '1.9.1.3');       
end;
/












