SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.4.0
-- Date de publication :  11/01/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Correction d'une vue
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

CREATE OR REPLACE FORCE VIEW maracuja.v_jd_depense_ctrl_planco (dpco_id,
                                                                exe_ordre,
                                                                dep_id,
                                                                pco_num,
                                                                man_id,
                                                                dpco_montant_budgetaire,
                                                                dpco_ht_saisie,
                                                                dpco_tva_saisie,
                                                                dpco_ttc_saisie,
                                                                tbo_ordre,
                                                                ecd_ordre,
                                                                dpp_id,
                                                                dpp_date_reception,
                                                                dpp_date_service_fait,
                                                                dpp_numero_facture,
                                                                dpp_date_facture,
                                                                dpp_im_dgp,
                                                                date_debut_dgp
                                                               )
AS
   SELECT dpco_id, dpco.exe_ordre, dpco.dep_id, pco_num, man_id,
          dpco_montant_budgetaire, dpco_ht_saisie, dpco_tva_saisie,
          dpco_ttc_saisie, tbo_ordre, dpco.ecd_ordre, db.dpp_id,
          dpp.dpp_date_reception, dpp.dpp_date_service_fait,
          dpp.dpp_numero_facture, dpp.dpp_date_facture, dpp_im_dgp,
          GREATEST (TRUNC (dpp_date_service_fait),
                    TRUNC (dpp_date_reception)
                   ) date_debut_dgp
     FROM jefy_depense.depense_ctrl_planco dpco,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dpp
    WHERE dpco.dep_id = db.dep_id AND db.dpp_id = dpp.dpp_id;
/




update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.4.0';
commit;
    







