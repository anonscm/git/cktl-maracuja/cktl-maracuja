SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.v_bordereau_rejet
(EXE_ORDRE, BT_TYPE, TYPE_BORDEREAU_ORIGINE, GES_CODE, NUMERO_OBJET, 
 BOR_NUMERO_ORIGINE, BORDEREAU_REJET_NUMERO, PCO_NUM, MONTANT_HT, MONTANT_TVA, 
 MONTANT_TTC, MOTIF_REJET, DATE_REJET, REJETE_PAR, BORDEREAU_REJET_ETAT)
AS 
select 
m.exe_ordre, tbo2.tbo_libelle as BT_type, 
tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.man_numero numero_objet, b.bor_num bor_numero_origine,
brj.brj_num bordereau_rejet_numero, pco_num, man_ht montant_ht, man_tva montant_tva, man_ttc montant_ttc, man_motif_rejet motif_rejet, b.BOR_DATE_VISA date_rejet,(u.nom_usuel ||' '|| u.prenom) as REJETE_PAR,  
brj.brj_etat bordereau_rejet_etat
from maracuja.mandat m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo, 
maracuja.type_bordereau tbo2, 
jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
union all
select m.exe_ordre, tbo2.tbo_libelle as BT_type, tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.tit_numero numero_objet, b.bor_num bor_numero_origine, 
brj.brj_num bordereau_rejet_numero, pco_num, tit_ht montant_ht, tit_tva montant_tva, tit_ttc montant_ttc, tit_motif_rejet motif_rejet,b.BOR_DATE_VISA date_rejet, (u.nom_usuel ||' '|| u.prenom) as REJETE_PAR ,  
brj.brj_etat bordereau_rejet_etat
from maracuja.titre m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo,  maracuja.type_bordereau tbo2, jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre;




CREATE OR REPLACE FORCE VIEW COMPTEFI.V_DVLOP_DEP_LISTE
(EXE_ORDRE, GES_CODE, SECTION, PCO_NUM_BDN, PCO_NUM,
MANDATS, REVERSEMENTS, CR_OUVERTS, DEP_DATE)
AS
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, SUM(man_ht), 0, 0, e.DATE_SAISIE
-- Mandat d�penses
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND tbo_ordre <> 8 AND tbo_ordre <>18 AND tbo_ordre <> 16
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement avant 2007
SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0, SUM(tit_ht), 0, e.DATE_SAISIE
FROM maracuja.TITRE t, maracuja.bordereau b, comptefi.v_planco p, (
  SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
  WHERE
  m.tit_id=mde.tit_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.tit_id ) e
WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 8
AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id
GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement � partir de 2007
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, 0, -SUM(man_ht), 0, e.DATE_SAISIE
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND (tbo_ordre = 8 OR tbo_ordre=18)
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- Cr�dits ouverts
SELECT EXE_ORDRE, GES_CODE, '1' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '6%' OR pco_num LIKE '7%')
AND BDN_QUOI ='D' AND co <> 0
UNION ALL
SELECT EXE_ORDRE, GES_CODE, '2' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%')
AND BDN_QUOI ='D' AND co <> 0 ;