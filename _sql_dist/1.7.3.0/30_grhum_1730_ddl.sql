SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.EpN3_EDRB
IS

procedure epn_genere_edrb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_edrb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);

procedure prv_genere_edrb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_edrb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER);
procedure prv_insert_edrb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_EDRB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_EDRB(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_REC_BUD_1 where EPNRB1_TYPE_DOC=typeFichier and EPNRB1_COD_BUD=codeBudget and EPNRB_ordre=rang and EPNRB1_EXERCICE=exeOrdre;
    delete from EPN_REC_BUD_2 where EPNRB2_TYPE_DOC=typeFichier and EPNRB2_COD_BUD=codeBudget and EPNRB_ORDRE=rang and EPNRB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_EDRB(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_EDRB(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_EDRB_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_EDRB_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_EDRB_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;
        
    end if;

end;





-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_EDRB_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < vDateFichier AND exe_ordre = exeOrdre     
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
          

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_EDRB_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_EDRB_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE = gescodeSACD
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);     
    
end;







-- se charge de remplir EPN_EDRB_2
procedure prv_insert_EDRB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
         INSERT INTO EPN.EPN_REC_BUD_2  
            VALUES (
                    to_number(rang),    --EPNRB_ORDRE, 
                    gesCode,    --EPNRB2_GES_CODE, 
                    '2',    --EPNRB2_TYPE, 
                     numLigne,   --EPNRB2_NUMERO, 
                     1,   --EPNRB2_TYPE_CPT, 
                     (SUBSTR(vCompte,1,15)),   --EPNRB2_COMPTE, 
                     depensesMontant*deviseTaux,  --EPNRB2_MNTBRUT, 
                     depensesSurCreditExtourne*deviseTaux,   --EPNRB2_DEP_CREEXT, 
                     reversementsMontant*deviseTaux,   --EPNRB2_MNTREVERS, 
                     depensesExtournes*deviseTaux,   --EPNRB2_DEP_DEPEXT, 
                     (depensesMontant-reversementsMontant)*deviseTaux,   --EPNRB2_MNTNET, 
                     exeOrdre,   --EPNRB2_EXERCICE, 
                     codeBudget,   --EPNRB2_COD_BUD, 
                     typeFichier   --EPNRB2_TYPE_DOC
                    );                           

    --INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;

end;



procedure prv_insert_EDRB1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_REC_BUD_1.EPNRB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_REC_BUD_1 
            VALUES (
                    to_number(rang), --EPNRB_ORDRE, 
                    gesCode, --EPNRB1_GES_CODE, 
                    '1', --EPNRB1_TYPE, 
                    1, --EPNRB1_NUMERO, 
                    identifiantDGCP, --EPNRB1_IDENTIFIANT, 
                    typeFichier,--EPNRB1_TYPE_DOC, 
                    codeNomenclature, --EPNRB1_COD_NOMEN, 
                    codeBudget,--EPNRB1_COD_BUD, 
                    exeOrdre,--EPNRB1_EXERCICE, 
                    rang, --EPNRB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNRB1_DATE, 
                    siren, --EPNRB1_SIREN, 
                    siret,--EPNRB1_SIRET, 
                    nbEnregistrement --EPNRB1_NBENREG
                    ); 
        
        --INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,                     EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);
    

end;








END;
/


