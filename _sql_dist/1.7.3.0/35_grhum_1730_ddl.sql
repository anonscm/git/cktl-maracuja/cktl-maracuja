SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.Epn3
IS

FUNCTION epn_getDeviseTaux(exeOrdre NUMBER) RETURN NUMBER;
FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN maracuja.PARAMETRE.par_value%type;
function prv_getCodeSIREN(exeOrdre NUMBER) return varchar2;
function prv_getCodeSIRET(exeOrdre NUMBER) return varchar2;
function prv_getDateForRang(rang varchar2, exercice number, dateFournie varchar2) return date;

procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure epn_genere_edrb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure epn_genere_eccb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);



END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3  IS
-- v. du 12/03/2007 (code nomen. M9-3, M9-1, etc.)
-- v. du 18/03/2008 (devise locale)
-- v. 3 (chenagement structure du package, prise en charge des fichiers aggreges)


procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
begin
    epn3_bal.epn_genere_bal(identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, gescodeSACD, v_date);
end;


procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
begin
    epn3_eddb.epn_genere_eddb(identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, gescodeSACD, v_date);
end;

procedure epn_genere_edrb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
begin
    epn3_edrb.epn_genere_edrb(identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, gescodeSACD, v_date);
end;

procedure epn_genere_eccb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
begin
    epn3_eccb.epn_genere_eccb(identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, gescodeSACD, v_date);
end;

FUNCTION epn_getDeviseTaux(exeOrdre NUMBER) RETURN NUMBER  
-- Ajout by ProfesseurBougna le 18 mars 2008   
IS
  deviseTaux jefy_admin.parametre.par_value%TYPE;
BEGIN
    SELECT PAR_VALUE INTO deviseTaux FROM jefy_admin.PARAMETRE
        WHERE PAR_KEY = 'DEVISE_TAUX' AND exe_ordre=exeOrdre;

    IF deviseTaux IS NULL THEN deviseTaux:=1;
    END IF;
    RETURN deviseTaux;
END;


FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN maracuja.PARAMETRE.par_value%type
IS
  codeNomenclature epn_bal_1.EPNB1_COD_NOMEN%type;
BEGIN
    SELECT substr(PAR_VALUE,1,2) INTO codeNomenclature FROM maracuja.PARAMETRE
        WHERE PAR_KEY = 'EPN_CODE_NOMENCLATURE' AND exe_ordre=exeOrdre;

    IF codeNomenclature IS NULL THEN
        RAISE_APPLICATION_ERROR (-20002,'EPN_CODE_NOMENCLATURE doit etre renseigne dans maracuja.PARAMETRE');
    END IF;
    RETURN codeNomenclature;
END;

function prv_getCodeSIREN(exeOrdre NUMBER) return varchar2
is
     SIRET_C maracuja.PARAMETRE.PAR_VALUE%TYPE;
begin
    SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
    WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=exeOrdre;
    
    return TO_NUMBER(SUBSTR(SIRET_C,1,9));

end;

function prv_getCodeSIRET(exeOrdre NUMBER) return varchar2
is
    SIRET_C maracuja.PARAMETRE.PAR_VALUE%TYPE;
begin
    SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
    WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=exeOrdre;
    return TO_NUMBER(SUBSTR(SIRET_C,1,14));

end;


-- determine la date en fonction du rang du fichier (pour les rang 05 et 06, la date est fournie par l'utilisateur)
function prv_getDateForRang(rang varchar2, exercice number, dateFournie varchar2) return date
is
    res varchar2(8);
begin
    res := null;
       
    if (rang='01') then
        res := '3103';
    elsif (rang='02') then
        res :='3006';
    elsif (rang='03') then
        res :='3009';
    elsif (rang='04') then
        res :='3112';   
    end if;    
    
    
    if (res is not null) then
        return to_date(res||exercice  , 'ddmmyyyy');
    else
       begin
        if (dateFournie is null) then
            RAISE_APPLICATION_ERROR (-20002,'La date n''a pas ete fournie.');
        end if;
        return to_date(dateFournie,'ddmmyyyy');
      end;
    end if;
    
end;



END;
/


GRANT EXECUTE ON  EPN.EPN3 TO MARACUJA;

