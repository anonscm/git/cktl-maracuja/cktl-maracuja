SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.EpN3_EDDB
IS

procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_eddb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);

procedure prv_genere_eddb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eddb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eddb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_eddb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER);
procedure prv_insert_eddb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_EDDB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_eddb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_dep_bud_1 where EPNDB1_TYPE_DOC=typeFichier and EPNDB1_COD_BUD=codeBudget and EPNDB_ordre=rang and EPNDB1_EXERCICE=exeOrdre;
    delete from EPN_dep_bud_2 where EPNDB2_TYPE_DOC=typeFichier and EPNDB2_COD_BUD=codeBudget and EPNDB_ORDRE=rang and EPNDB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_eddb(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_eddb_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_eddb_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_eddb_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;
        
    end if;

end;


-- generation des fichiers de balance consolides (type 01)
-- NB : Maracuja ne gere pas les comptabilites secondaires, donc ce fichier sera identique aux fichiers 02 (aggreges) 



-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_eddb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND exe_ordre = exeOrdre
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);    
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_eddb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND exe_ordre = exeOrdre
    AND GES_CODE in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_eddb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND exe_ordre = exeOrdre
    AND GES_CODE = gesCodeSACD
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;







-- se charge de remplir EPN_eddb_2
procedure prv_insert_eddb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
         INSERT INTO EPN.EPN_DEP_BUD_2  
            VALUES (
                    to_number(rang),    --EPNDB_ORDRE, 
                    gesCode,    --EPNDB2_GES_CODE, 
                    '2',    --EPNDB2_TYPE, 
                     numLigne,   --EPNDB2_NUMERO, 
                     1,   --EPNDB2_TYPE_CPT, 
                     (SUBSTR(vCompte,1,15)),   --EPNDB2_COMPTE, 
                     depensesMontant*deviseTaux,  --EPNDB2_MNTBRUT, 
                     depensesSurCreditExtourne*deviseTaux,   --EPNDB2_DEP_CREEXT, 
                     reversementsMontant*deviseTaux,   --EPNDB2_MNTREVERS, 
                     depensesExtournes*deviseTaux,   --EPNDB2_DEP_DEPEXT, 
                     (depensesMontant-reversementsMontant)*deviseTaux,   --EPNDB2_MNTNET, 
                     exeOrdre,   --EPNDB2_EXERCICE, 
                     codeBudget,   --EPNDB2_COD_BUD, 
                     typeFichier   --EPNDB2_TYPE_DOC
                    );                           

    --INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;

end;



procedure prv_insert_eddb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_dep_bud_1.EPNDB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_DEP_BUD_1 
            VALUES (
                    to_number(rang), --EPNDB_ORDRE, 
                    gesCode, --EPNDB1_GES_CODE, 
                    '1', --EPNDB1_TYPE, 
                    1, --EPNDB1_NUMERO, 
                    identifiantDGCP, --EPNDB1_IDENTIFIANT, 
                    typeFichier,--EPNDB1_TYPE_DOC, 
                    codeNomenclature, --EPNDB1_COD_NOMEN, 
                    codeBudget,--EPNDB1_COD_BUD, 
                    exeOrdre,--EPNDB1_EXERCICE, 
                    rang, --EPNDB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNDB1_DATE, 
                    siren, --EPNDB1_SIREN, 
                    siret,--EPNDB1_SIRET, 
                    nbEnregistrement --EPNDB1_NBENREG
                    ); 
        

end;









--PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
--IS


--NUM_SEQ NUMBER(5);
--PCO_NUM VARCHAR2(15);
--EPN_DATE DATE;
--DEP_MONT  NUMBER(15,2);
--REVERS NUMBER(15,2);
--EXERC VARCHAR2(4);
--EXERCN1 VARCHAR2(4);
--SIRET NUMBER(14);
--SIRET_C VARCHAR2(14);
--IDENTIFIANT NUMBER(10);
--SIREN NUMBER(9);
--VDATE VARCHAR2(9);
--VDATE1 VARCHAR2(9);
--VARDATE DATE;
--VARDATE1 DATE;
--RANG VARCHAR2(2);
--compte VARCHAR2(30);
--CPT NUMBER;
--COD_BUDGET VARCHAR2(2);
--ordre_num NUMBER;
--TYP_DOC VARCHAR2(2);
--codeNomenclature VARCHAR2(2);
--flag INTEGER;
--deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008  

--    CURSOR c_tot IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS)
--     FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
--     GROUP BY PCO_NUM
--     ORDER BY PCO_NUM;

--    CURSOR c_sacd  IS SELECT PCO_NUM,SUM( DEP_MONT), SUM( REVERS)
--    FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
--    GROUP BY PCO_NUM
--    ORDER BY PCO_NUM;

--    CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) FROM EPN_DEP_BUD
--    WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
--    AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
--     GROUP BY PCO_NUM
--     ORDER BY PCO_NUM;


--BEGIN

--/*DELETE TAB_MANDAT_MAJ;

--INSERT INTO TAB_MANDAT_MAJ SELECT * FROM MANDAT_MAJ;*/

--    ORDRE_NUM := ordre;
--    EXERC := EXE_ORDRE;
--    codeNomenclature := epn_getCodeNomenclature(EXERC);
--    deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008    

--    IF comp = 'HSA'  THEN
--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
--        AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
--            AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
--        END IF;

--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
--        AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
--            AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
--        END IF;

--    ELSE
--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp
--            AND EPNDB1_EXERCICE = exerc;
--        END IF;

--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp AND EPNDB2_EXERCICE = exerc;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp
--            AND EPNDB2_EXERCICE = exerc;
--        END IF;

--    END IF;


--    -- SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

--    SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
--    WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
--    SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
--    SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

--    SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
--    WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre = EXERC;
--    IF IDENTIFIANT IS NULL THEN
--        RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans     maracuja.PARAMETRE');
--    END IF;

--    IF ordre = '1' THEN
--        RANG := '01';
--        VDATE := '3103' || EXERC;
--        VDATE1 := '3103' || EXERC;
--    ELSE IF ordre =  '2' THEN
--            RANG := '02';
--            VDATE := '3006' || EXERC;
--            VDATE1 := '3006' || EXERC;
--        ELSE IF ordre = '3' THEN
--                RANG := '03';
--                VDATE := '3009' || EXERC;
--                VDATE1 := '3009' || EXERC;
--            ELSE IF ordre = '4' THEN
--                    RANG := '04';
--                    VDATE := '3112' || EXERC;
--                    VDATE1 := '3112' || EXERC;
--                ELSE IF ordre = '5' THEN
--                    RANG := '05';
--                    VDATE := V_DATE;
--                    VDATE1 := '3112' || EXERC;
--                    ELSE IF ordre = '6' THEN
--                            RANG := '06';
--                            VDATE := V_DATE;
--                            VDATE1 := '3112' || EXERC;
--                        ELSE
--                            RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
--                        END IF;
--                    END IF;
--                END IF;
--            END IF;
--        END IF;
--    END IF;

--    VARDATE := TO_DATE(VDATE,'ddmmyyyy');
--    VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

--        /*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
--        IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
--                  TYP_DOC := '02';
--        ELSE
--               TYP_DOC := '03';
--           END IF;*/

--    -- Correction type document --
--    TYP_DOC := '03';

--    /*    IF  comp = 'ALL' THEN
--               COD_BUDGET := '01';
--        ELSE  IF comp =  'HSA' THEN
--                COD_BUDGET := '00';
--            ELSE
--                SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
--            END IF;
--        END IF;*/

--    -- Correction du code budget pour l'�tablissement ou SACD --
--    IF comp = 'HSA' THEN
--        COD_BUDGET := '01';
--    ELSE
--        SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
--        WHERE sacd = comp AND exe_ordre = exerc;
--        IF (flag=0) THEN
--           RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
--        END IF;
--        SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
--        WHERE sacd = comp AND EXE_ORDRE = exerc;
--    END IF;

--    INSERT INTO EPN_DEP_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,                     EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

--    NUM_SEQ := 2;
--    /*
--      IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
--      SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
--    --        DATE := '31122005';
--           VDATE := '3112'|| EXERCN1;
--            VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
--      END IF;
--    */


--    -- Pas de consolidation Etablissement + SACD
--    /* IF comp = 'ALL'  THEN
--    -- Etablissement + SACD --
--    OPEN c_tot;
--    LOOP
--        FETCH c_tot INTO  PCO_NUM,  DEP_MONT, REVERS;
--        EXIT WHEN c_tot%NOTFOUND;
--        compte := PCO_NUM || '              ';
--        INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0,  DEP_MONT-REVERS);
--        NUM_SEQ := NUM_SEQ + 1;
--    END LOOP;
--    CLOSE c_tot;

--    ELSE */
--    IF comp = 'HSA' THEN
--        --  hors SACD
--        OPEN c_hors_sacd ;
--        LOOP
--            FETCH c_hors_sacd INTO  PCO_NUM, DEP_MONT, REVERS ;
--            EXIT WHEN c_hors_sacd%NOTFOUND ;
--            compte := PCO_NUM || '              ' ;

--            -- Modifi� by ProfesseurBougna le 18 mars 2008  
--            INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;
--            NUM_SEQ := NUM_SEQ + 1 ;
--            END LOOP;
--        CLOSE c_hors_sacd;

--    ELSE
--        --  cas SACD
--        OPEN c_sacd;
--        LOOP
--            FETCH c_sacd INTO  PCO_NUM,  DEP_MONT, REVERS;
--            EXIT WHEN c_sacd%NOTFOUND;
--            compte := PCO_NUM || '              ';
--            
--            -- Modifi� by ProfesseurBougna le 18 mars 2008  
--            INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc);
--            NUM_SEQ := NUM_SEQ + 1;
--        END LOOP;
--        CLOSE c_sacd;
--    END IF;

--    -- END IF;

--    -- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
--    UPDATE EPN_DEP_BUD_1 SET EPNDB1_NBENREG = num_seq
--    WHERE EPNDB_ORDRE = ORDRE_NUM AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;

--END;


END;
/


