SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.EpN3_ECCB
IS

procedure epn_genere_eccb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_eccb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);
--procedure prv_genere_eccb_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_ECCB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, credPrevOuvert NUMBER, credPrevOB NUMBER, credPrevNonEmpl NUMBER, credPrevExtourneNonEmpl NUMBER, sensCompte VARCHAR2, deviseTaux NUMBER);
procedure prv_insert_eccb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

--procedure prv_prepare_budnat_planco(exeOrdre NUMBER);
--procedure prv_prepare_ECCB_DEP(exeOrdre NUMBER);
--procedure prv_prepare_ECCB_REC(exeOrdre NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_ECCB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_ECCB(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_CRE_BUD_1 where EPNCCB1_TYPE_DOC=typeFichier and EPNCCB1_COD_BUD=codeBudget and EPNCCB_ordre=rang and EPNCCB1_EXERCICE=exeOrdre;
    delete from EPN_CRE_BUD_2 where EPNCCB2_TYPE_DOC=typeFichier and EPNCCB2_COD_BUD=codeBudget and EPNCCB_ORDRE=rang and EPNCCB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_ECCB(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_ECCB(typeFichier, codeBudget, exeOrdre, rang);
--    prv_prepare_budnat_planco(exeOrdre);
--    prv_prepare_ECCB_DEP(exeOrdre);
--    prv_prepare_ECCB_REC(exeOrdre);
    
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_ECCB_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_ECCB_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_ECCB_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;
        
    end if;

end;




---- remplit la table BUDNAT_PLANCO a partir du plan comptable
--procedure prv_prepare_budnat_planco(exeOrdre NUMBER)
--is
--begin

--    -- construire la liste des comptes budgetaires
--    DELETE FROM BUDNAT_PLANCO;

--    INSERT INTO BUDNAT_PLANCO
--    SELECT PCO_NUM, SUBSTR(PCO_NUM,1,2), '1'
--    FROM maracuja.PLAN_COMPTABLE_exer
--    WHERE (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%') AND PCO_VALIDITE = 'VALIDE' and exe_ordre=exeOrdre;

--    INSERT INTO BUDNAT_PLANCO
--    SELECT PCO_NUM, SUBSTR(PCO_NUM,1,3), '2'
--    FROM maracuja.PLAN_COMPTABLE_exer
--    WHERE ( PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%')AND PCO_VALIDITE = 'VALIDE' and exe_ordre=exeOrdre;
--    
--    
--end;


---- construit le cadre 2 (depenses + ORV + previsions budgetaires)
--procedure prv_prepare_ECCB_DEP(exeOrdre NUMBER)
--is
--begin
--   -- construire le cadre 2
--    DELETE FROM EPN_DEP where exe_ordre=exeOrdre;
--    
--    --depenses + OR --
--    INSERT INTO EPN_DEP
--        SELECT 
--            db.PCO_NUM,
--            bp.PCO_NUM_BDN,
--            db.GES_CODE,
--            db.DEP_MONT MDT_MONT,
--            db.REVERS TIT_MONT,
--            0 CO,
--            bp.SECTION,
--            db.EPN_DATE EPN_DATE,
--            db.EXE_ORDRE
--            FROM EPN_DEP_BUD db, BUDNAT_PLANCO bp
--            WHERE db.PCO_NUM = bp.PCO_NUM and db.exe_ordre=exeOrdre;    
--    DELETE FROM EPN_DEP WHERE MDT_MONT = 0 AND CO = 0 AND TIT_MONT = 0;
--    
--    -- prévisions budgétaires --
--    INSERT INTO EPN_DEP       
--        SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
--            FROM EPN_BUDNAT bn , BUDNAT_PLANCO bp
--            WHERE bn.PCO_NUM = bp.PCO_NUM_bdn
--            AND (bn.PCO_NUM LIKE '6%' OR bn.PCO_NUM LIKE '2%')
--            AND CO <> 0
--            and bn.exe_ordre=exeOrdre
--            ORDER BY exe_ordre, ges_code, bn.pco_num;
--            
--        
--    
--end; 


---- construit le cadre 3 (recettes + Annulations + previsions budgetaires)
--procedure prv_prepare_ECCB_REC(exeOrdre NUMBER)
--is
--begin
--   -- construire le cadre 3
--    DELETE FROM EPN_REC where exe_ordre=exeOrdre;
--     DELETE FROM EPN_REC;

-- -- Recettes et Déductions--
--    INSERT INTO EPN_REC     
--        SELECT erb.PCO_NUM,
--          bp.PCO_NUM_BDN,
--          erb.GES_CODE,
--          erb.MONT_REC TIT_MONT,
--          erb.ANNUL_TIT_REC RED_MONT,
--          0 CO,
--          bp.SECTION,
--          erb.EPN_DATE,
--          erb.exe_ordre
--        FROM EPN_REC_BUD erb, BUDNAT_PLANCO bp
--        WHERE erb.PCO_NUM = bp.PCO_NUM and erb.exe_ordre=exeOrdre; 

--    DELETE FROM EPN_REC WHERE TIT_MONT = 0 AND RED_MONT = 0 AND CO = 0;

--    -- Prévisions budgétaires --
--    INSERT INTO EPN_REC  
--    SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
--    FROM EPN_BUDNAT bn, BUDNAT_PLANCO bp
--    WHERE bp.PCO_NUM_BDN = bn.PCO_NUM
--    AND ( bn.PCO_NUM LIKE '7%' OR bn.PCO_NUM LIKE '1%')
--    AND CO <> 0
--    and bn.exe_ordre=exeOrdre;
--    
--end; 

-- generation des fichiers aggreges (type 02)
procedure prv_genere_ECCB_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    
    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

--    cursor c_ECCB_DEP is SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
--            FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
--            GROUP BY pco_num_bdn
--            order by pco_num_bdn;
            
--    CURSOR c_ECCB_REC IS
--            SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
--            FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre           
--            GROUP BY pco_num_bdn
--            order by pco_num_bdn;     
            
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_ECCB_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

  
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;                  
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;

    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_ECCB_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;

    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

  
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and ges_code = gesCodeSACD
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and ges_code = gesCodeSACD
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;                  
         
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
  
    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;







-- se charge de remplir EPN_ECCB_2
procedure prv_insert_ECCB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, credPrevOuvert NUMBER, credPrevOB NUMBER, credPrevNonEmpl NUMBER, credPrevExtourneNonEmpl NUMBER, sensCompte VARCHAR2, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
        
        INSERT INTO EPN.EPN_CRE_BUD_2 
        VALUES (
                  to_number(rang),   --EPNCCB_ORDRE, 
                  gesCode,  --EPNCCB2_GES_CODE, 
                  '2',  --EPNCCB2_TYPE, 
                  numLigne,  --EPNCCB2_NUMERO, 
                  1,  --EPNCCB2_TYPE_CPT, 
                  (SUBSTR(vCompte,1,15)),  --EPNCCB2_COMPTE, 
                  credPrevOuvert*deviseTaux,  --EPNCCB2_CREPREVOUV, 
                  credPrevOB*deviseTaux,  --EPNCCB2_CREPREVOB, 
                  credPrevNonEmpl*deviseTaux ,  --EPNCCB2_CREPREVNOEMPL, 
                  credPrevExtourneNonEmpl*deviseTaux ,  --EPNCCB2_CREPREVEXTNOEMPL, 
                  sensCompte,  --EPNCCB2_SENSCPT, 
                  exeOrdre,  --EPNCCB2_EXERCICE, 
                  codeBudget,  --EPNCCB2_COD_BUD, 
                  typeFichier  --EPNCCB2_TYPE_DOC
                );
end;



procedure prv_insert_ECCB1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_CRE_BUD_1.EPNCCB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_CRE_BUD_1 
            VALUES (
                    to_number(rang), --EPNCCB_ORDRE, 
                    gesCode, --EPNCCB1_GES_CODE, 
                    '1', --EPNCCB1_TYPE, 
                    1, --EPNCCB1_NUMERO, 
                    identifiantDGCP, --EPNCCB1_IDENTIFIANT, 
                    typeFichier,--EPNCCB1_TYPE_DOC, 
                    codeNomenclature, --EPNCCB1_COD_NOMEN, 
                    codeBudget,--EPNCCB1_COD_BUD, 
                    exeOrdre,--EPNCCB1_EXERCICE, 
                    rang, --EPNCCB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNCCB1_DATE, 
                    siren, --EPNCCB1_SIREN, 
                    siret,--EPNCCB1_SIRET, 
                    nbEnregistrement --EPNCCB1_NBENREG
                    ); 
    

end;








END;
/


