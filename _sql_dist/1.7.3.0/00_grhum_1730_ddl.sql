ALTER TABLE EPN.EPN_BAL_2 ADD (EPNB2_COD_BUD  VARCHAR2(2));
ALTER TABLE EPN.EPN_BAL_2 ADD (EPNB2_TYPE_DOC VARCHAR2(2));

ALTER TABLE EPN.EPN_DEP_BUD_2 ADD (EPNDB2_COD_BUD  VARCHAR2(2));
ALTER TABLE EPN.EPN_DEP_BUD_2 ADD (EPNDB2_TYPE_DOC VARCHAR2(2));


ALTER TABLE EPN.EPN_REC_BUD_2 ADD (EPNRB2_COD_BUD  VARCHAR2(2));
ALTER TABLE EPN.EPN_REC_BUD_2 ADD (EPNRB2_TYPE_DOC VARCHAR2(2));

ALTER TABLE EPN.EPN_CRE_BUD_2 ADD (EPNCCB2_COD_BUD  VARCHAR2(2));
ALTER TABLE EPN.EPN_CRE_BUD_2 ADD (EPNCCB2_TYPE_DOC VARCHAR2(2));

grant select on maracuja.plan_comptable_exer to epn;
grant select on maracuja.mandat to epn;
grant select on comptefi.v_dvlop_dep to epn;
grant select on comptefi.v_dvlop_rec to epn;

grant select, insert, update on epn.code_budget_sacd to maracuja;
