SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.9.1
-- Date de publication : 05/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- mise à jour du plan comptable de reference
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.9.1',  null, '');
commit;



create procedure grhum.inst_patch_maracuja_1891 is
begin

	
-- mise à jour du plan comptable de reference
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '103121', 'Dotations pérennes représentatives d''actifs inaliénables', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1031211', 'Dotations pérennes représentatives de biens immobiliers inaliénables', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1031212', 'Dotations pérennes représentatives de biens mobiliers inaliénables', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1031213', 'Dotations pérennes représentatives d''autres actifs inaliénables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '103122', 'Dotations pérennes représentatives d''actifs aliénables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '10314', 'Apports sans droit de reprise', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '10315', 'Legs et donations avec contrepartie d''actifs immobilisés', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '10316', 'Subventions d''investissement affectées à des biens renouvelables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '10317', 'Autres fonds propres - dotations consomptibles', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '105', 'Écarts de réévaluation', NULL, '105', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1051', 'Ecart de réévaluation sur des biens sans droit de reprise', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1054', 'Ecart de réévaluation libre', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
    
 Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '1856', 'Autres', NULL, 'N', 
    'N', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');   
    
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '44586', 'TVA sur factures non parvenues', NULL, NULL, 
    'N', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');    
    
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '4671', 'Autres comptes créditeurs', NULL, null, 
    'N', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');    
    
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '4672', 'Autres comptes débiteurs', NULL, null, 
    'N', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');            
 
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '68151', 'Dotations aux provisions d''exploitation sur charges de personnel', NULL, null, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');     

Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '68152', 'Dotations aux provisions d''exploitation sur autres charges', NULL, null, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');         


Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '7683', 'Produits financiers provenant de l''annulation d''ordres de dépenses des exercices antérieurs', NULL, null, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');         

Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (octobre 2011)', '77188', 'Autres produits exceptionnels divers', NULL, null, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');         
          
 
    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.9.1';           
end;
/






