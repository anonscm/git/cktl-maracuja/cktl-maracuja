declare
        cursor cinvent is
        select clic_id, clic_duree_amort,  pca_duree
        from jefy_inventaire.cle_inventaire_comptable cic, maracuja.PLANCO_AMORTISSEMENT pa
        where cic.PCO_NUM = pa.PCO_NUM
        and cic.exe_ordre = pa.exe_ordre
        and pca_duree <> clic_duree_amort
        and pa.exe_ordre=2008;
        
        clicid number;
        duree_ko number;
        duree_ok number;

begin
    -- correction des durees d'amortissement
    update maracuja.PLANCO_AMORTISSEMENT  p1
    set p1.pca_duree=(
        select p2.pca_duree 
        from maracuja.PLANCO_AMORTISSEMENT p2  
        where  p2.exe_ordre=2007 
        and p1.PCOA_ID=p2.pcoa_id 
        and p1.PCO_num=p2.pco_num        
    )
    where p1.exe_ordre=2008
    and p1.pcoa_id in (select pcoa_id from maracuja.planco_amortissement where exe_ordre=2007);
    
    
    -- correction inventaire
        open cinvent;
        loop
            fetch cinvent into clicid, duree_ko, duree_ok;
            exit when cinvent%notfound;
        
            update jefy_inventaire.cle_inventaire_comptable set clic_duree_amort = duree_ok where clic_id = clicid;
            jefy_inventaire.API_COROSSOL.AMORTISSEMENT_CLE (clicid,2);
        end loop;
        close cinvent;    
    commit;
end;    
