declare
    vName varchar2(50);
    flag integer;

begin
    select count(*) into flag from all_constraints where owner = 'MARACUJA' and table_name='PRELEVEMENT_FICHIER' and constraint_type='P';  
    if (flag>0) then
        select constraint_name into vName from all_constraints where owner = 'MARACUJA' and table_name='PRELEVEMENT_FICHIER' and constraint_type='P';  
        if (vName is not null) then 
            execute immediate 'alter table maracuja.prelevement_fichier drop constraint ' || vName;
            GRHUM.DROP_OBJECT ( 'MARACUJA', vName, 'INDEX' );
        end if;
    end if;
    
    execute immediate 'alter table maracuja.PRELEVEMENT_FICHIER add (constraint pk_PRELEVEMENT_FICHIER primary key ( FICP_ORDRE ) using index tablespace GFC_INDX)  '   ;
    -------
    
   select count(*) into flag from all_constraints where owner = 'MARACUJA' and table_name='VIREMENT_FICHIER' and constraint_type='P';  
    if (flag>0) then
        select constraint_name into vName from all_constraints where owner = 'MARACUJA' and table_name='VIREMENT_FICHIER' and constraint_type='P';  
        if (vName is not null) then 
            execute immediate 'alter table maracuja.virement_fichier drop constraint ' || vName;
            GRHUM.DROP_OBJECT ( 'MARACUJA', vName, 'INDEX' );
        end if;
    end if;
    
    execute immediate 'alter table maracuja.VIREMENT_FICHIER add (constraint pk_VIREMENT_FICHIER primary key (VIR_ORDRE ) using index tablespace GFC_INDX)  '   ;
     

---
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.7.9.0',  SYSDATE, '');
    commit;
end;