SET DEFINE OFF;


  CREATE OR REPLACE FORCE VIEW "MARACUJA"."V_TITRE_REJETE" ("EXE_ORDRE", "MAN_ID", "GES_CODE", "TIT_NUMERO", "DATE_TITRE", "BOR_NUM_ORIGINE", "TBO_LIBELLE_ORIGINE", "PCO_NUM", fou_code, fournisseur, "BRJ_NUM", "TBO_LIBELLE_REJET", "DATE_REJET", "TIT_MOTIF_REJET", "TIT_HT", "UTL_ORDRE") AS 
  select m.exe_ordre,  m.tit_id, m.ges_code,  m.tit_numero, b.BOR_DATE_CREATION date_titre, b.BOR_NUM bor_num_initial, tb1.TBO_LIBELLE TBO_LIBELLE_ORIGINE, m.pco_num,   f.fou_code, f.adr_nom ||' '|| nvl(f.adr_prenom,'') fournisseur, br.BRJ_NUM, tb2.tbo_libelle TBO_LIBELLE_REJET,   b.BOR_DATE_VISA date_rejet, m.tit_motif_rejet, m.TIT_HT, br.UTL_ORDRE
from bordereau_rejet br, titre m, bordereau b, type_bordereau tb1, type_bordereau tb2, v_fournisseur f
where m.brj_ordre=br.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tb1.tbo_ordre
and br.tbo_ordre=tb2.tbo_ordre
and m.fou_ordre=f.fou_ordre;
/


  CREATE OR REPLACE FORCE VIEW "MARACUJA"."V_MANDAT_REJETE" ("EXE_ORDRE", "MAN_ID", "GES_CODE", "MAN_NUMERO", "DATE_MANDAT", "BOR_NUM_ORIGINE", "TBO_LIBELLE_ORIGINE", "PCO_NUM", fou_code, fournisseur,"BRJ_NUM", "TBO_LIBELLE_REJET", "DATE_REJET", "MAN_MOTIF_REJET", "MAN_HT", "UTL_ORDRE") AS 
select m.exe_ordre,  m.man_id, m.ges_code,  m.man_numero, b.BOR_DATE_CREATION date_mandat, b.BOR_NUM bor_num_initial, tb1.TBO_LIBELLE TBO_LIBELLE_ORIGINE, m.pco_num,  f.fou_code, f.adr_nom ||' '|| nvl(f.adr_prenom,'') fournisseur,br.BRJ_NUM,   tb2.tbo_libelle TBO_LIBELLE_REJET,   b.BOR_DATE_VISA date_rejet, m.man_motif_rejet, m.MAN_HT, br.UTL_ORDRE
from bordereau_rejet br, mandat m, bordereau b, type_bordereau tb1, type_bordereau tb2, v_fournisseur f
where m.brj_ordre=br.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tb1.tbo_ordre
and br.tbo_ordre=tb2.tbo_ordre
and m.fou_ordre=f.fou_ordre;
/


  CREATE OR REPLACE FORCE VIEW "MARACUJA"."V_FOURNISSEUR" ("FOU_ORDRE", "PERS_ID", "ADR_ORDRE", "FOU_CODE", "FOU_DATE", "FOU_MARCHE", "FOU_VALIDE", "AGT_ORDRE", "FOU_TYPE", "D_CREATION", "D_MODIFICATION", "CPT_ORDRE", "FOU_ETRANGER", "ADR_ADRESSE1", "ADR_ADRESSE2", "ADR_CP", "ADR_VILLE", "ADR_NOM", "ADR_PRENOM", "ADR_CIVILITE", "CP_ETRANGER", "LC_PAYS") AS 
  SELECT f.*, 
a.adr_adresse1, a.adr_adresse2, a.code_postal AS adr_ADR_CP, a.ville AS adr_ville,
p.pers_libelle, p.pers_lc, p.pers_type, cp_etranger, lc_pays
FROM grhum.fournis_ulr f, 
    grhum.adresse a, 
    grhum.personne p,
    (select pers_id, max(a.adr_ordre) adr_ordre
   from grhum.repart_personne_adresse rpa, grhum.adresse a
   where rpa.adr_ordre=a.adr_ordre
   and rpa.TADR_CODE='FACT'
   and rpa.rpa_valide='O'
   group by pers_id 
   ) x ,
    grhum.pays pa
WHERE 
    p.pers_id=x.pers_id(+)
   and x.adr_ordre = a.adr_ordre(+)
   AND f.pers_id = p.pers_id
   AND a.c_pays=pa.c_pays(+) ;
/ 


create or replace force view maracuja.v_emargement_detail (ecd_ordre, ecd_ordre_em, emd_ordre)
as
select ecdSource.ecd_ordre ecd_ordre, ecdDest.ecd_ordre ecd_ordre_em, emd.emd_ordre
        from ecriture_detail ecdSource, emargement_detail emd, ecriture_detail ecdDest, emargement ema
        where (ecdSource.ecd_ordre=emd.ecd_ordre_source and ecdDest.ecd_ordre=emd.ecd_ordre_destination)
        and emd.EMA_ORDRE=ema.ema_ordre
        and substr(ema.EMA_ETAT,1,1)='V'
union all
  select ecdDest.ecd_ordre ecd_ordre, ecdSource.ecd_ordre ecd_ordre_ema, emd.emd_ordre
        from ecriture_detail ecdSource, emargement_detail emd, ecriture_detail ecdDest, emargement ema
        where (ecdSource.ecd_ordre=emd.ecd_ordre_source and ecdDest.ecd_ordre=emd.ecd_ordre_destination)
        and emd.EMA_ORDRE=ema.ema_ordre
        and substr(ema.EMA_ETAT,1,1)='V'
         and emd_ordre in (
          select emd_ordre from emargement_detail 
          minus 
          select emd_ordre from 
            (select ecdSource.ecd_ordre ecd_ordre, ecdDest.ecd_ordre ecd_ordre_em, emd.emd_ordre
              from ecriture_detail ecdSource, emargement_detail emd, ecriture_detail ecdDest, emargement ema
              where (ecdSource.ecd_ordre=emd.ecd_ordre_source and ecdDest.ecd_ordre=emd.ecd_ordre_destination)
              and emd.EMA_ORDRE=ema.ema_ordre
              and substr(ema.EMA_ETAT,1,1)='V'
            )
        )
;
/ 
        