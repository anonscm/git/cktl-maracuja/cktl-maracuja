SET DEFINE OFF;

ALTER TABLE maracuja.PRELEVEMENT_FICHIER MODIFY ( FICP_CONTENU clob);

ALTER TABLE maracuja.virement_FICHIER MODIFY ( vir_CONTENU clob);

grant execute on grhum.chaine_sans_accents to maracuja with grant option;
grant execute on grhum.chaine_sans_accents_simple to maracuja with grant option;

