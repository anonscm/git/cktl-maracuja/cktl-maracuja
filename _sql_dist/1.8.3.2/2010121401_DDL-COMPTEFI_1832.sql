-- Attention, ce fichier est encodé en UTF-8.
-- Exécutez-le dans un environnement UTF-8
-- à partir d'un user DBA (grhum par exemple)

set define off;

whenever sqlerror exit sql.sqlcode ;

INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.3.2',  null, '');
commit;

CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_EXECUTION_BUDGET (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  montant NUMBER(12,2);
  montant_depenses NUMBER(12,2);
  montant_recettes NUMBER(12,2);
  ebe NUMBER(12,2);
  montant_dep NUMBER(12,2);
  montant_rec NUMBER(12,2);

  groupe1 VARCHAR2(50);
  groupe2 VARCHAR2(50);
  --groupe3 varchar2(50);

  lib_dep VARCHAR2(100);
  lib_rec VARCHAR2(100);
  --formule varchar2(1000);
  flag INTEGER;

BEGIN

IF sacd = 'O' THEN
    DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE from EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
ELSE
    DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



    SELECT COUNT(*) INTO flag FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N' AND formule = 'RTAT';

    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Vous devez calculer la CAF a partir du resultat avant de calculer le cadre 4.');
    END IF;


--************* 1ère section FONCTIONNEMENT ***********************************
   groupe1 := '1ERE SECTION : FONCTIONNEMENT';
   montant_depenses := 0;
   montant_recettes := 0;

-- Dépenses
   groupe2 := ' ';

    lib_dep := 'Charges de fonctionnement';
    montant_dep := Execution_Bud(exeordre, '60%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '61%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '62%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '63', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '634%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '635%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '636%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '637%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '638%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '639%', gescode, sacd, 'D')
        --+ Execution_Bud(exeordre, '64%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '65%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '681%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '66%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '686%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '186%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

    lib_rec := 'Produits de fonctionnement';
    montant_rec := Execution_Bud(exeordre, '70%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '71%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '72%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '74%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '781%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '791%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '75%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '76%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '786%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '796%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '187%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

-- personnel
   groupe2 := ' ';
    lib_dep := 'Charges de personnel';
    montant_dep := 
        Execution_Bud(exeordre, '631%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '632%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '633%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '64%', gescode, sacd, 'D');

    montant_depenses := montant_depenses+montant_dep;

    lib_rec := ' ';
    montant_rec :=0;
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

    lib_dep := 'Charges exceptionnelles';
    montant_dep := Execution_Bud(exeordre, '67%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '687%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

       lib_rec := 'Produits exceptionnels';
    montant_rec := Execution_Bud(exeordre, '77%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '787%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '797%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);




-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';



    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N'  AND formule = 'RTAT';
    IF montant >= 0 THEN
           montant_dep := montant;
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := ABS(montant);
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Excédent de l''exercice';
    lib_rec := 'Déficit de l''exercice';

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);



--***************** 2EME SECTION : CAPITAL *******************
    groupe1 := '2EME SECTION : OPERATIONS EN CAPITAL';
       montant_depenses := 0;
      montant_recettes := 0;

       groupe2 := ' ';

     lib_dep := 'Dépenses en capital';
    montant_dep := Execution_Bud(exeordre, '2%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '1%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

     lib_rec := 'Recettes en capital (1)';
    montant_rec := Execution_Bud(exeordre, '2%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '1%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '775%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';

    -- CAF ou IAF
    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N' AND formule = 'CAF';
    IF montant < 0 THEN
           montant_dep := ABS(montant);
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant;
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;
    lib_dep:= 'Insuffisance d''autofinancement';
    lib_rec := 'Capacité d''autofinancement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


    -- AFR ou DFR
    IF montant_depenses < montant_recettes THEN
        montant_dep := montant_recettes - montant_depenses;
        montant_rec := 0;
        montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant_depenses - montant_recettes;
        montant_dep := 0;
        montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Augmentation du fond de roulement';
    lib_rec := 'Diminution du fond de roulement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


END;
/

    
update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.3.2';
commit;






