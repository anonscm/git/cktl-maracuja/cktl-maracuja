--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.8.3.3 
-- Date de publication :  03/01/2011
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- Modifications pour Abricot (Ajout d'un message d'erreur lorsque tentative de visa d'un bordereau de rejet déjà visé)
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_maracuja_1833;
commit;

drop procedure grhum.inst_patch_maracuja_1833;