SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.9.5
-- Date de publication : 15/11/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Correction des procedures de BE pour récupérer le compte de BE correct.
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.9.5',  null, '');
commit;



CREATE OR REPLACE FORCE VIEW maracuja.v_be_reprise_consolidee (com_ordre,
                                                               exe_ordre,
                                                               ges_code,
                                                               pco_num,
                                                               pco_libelle,
                                                               compte_be,
                                                               compte_be_libelle,
                                                               debits,
                                                               credits
                                                              )
AS
   SELECT   e.com_ordre, e.exe_ordre, c.ges_code, p.pco_num, p.pco_libelle,
            DECODE (p1.pco_num, NULL, p.pco_num, p1.pco_num) compte_be,
            DECODE (p1.pco_num,
                    NULL, p.pco_libelle,
                    p1.pco_libelle
                   ) compte_be_libelle,
            SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_debit
                 / DECODE (ecd_debit, 0, 1, ecd_debit)
                ) AS debits,
            SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_credit
                 / DECODE (ecd_credit, 0, 1, ecd_credit)
                ) AS credits
       FROM ecriture_detail ecd,
            ecriture e,
            comptabilite c,
            plan_comptable_exer p,
            plan_comptable_exer p1,
            gestion_exercice ge,
            (SELECT ecd_ordre
               FROM ecriture_detail ecd, ecriture e
              WHERE ecd.ecr_ordre = e.ecr_ordre
                AND SUBSTR (ecr_etat, 1, 1) = 'V'
             MINUS
             SELECT ecd_ordre
               FROM ecriture_detail_be_log) x
      WHERE ecd.ecd_ordre = x.ecd_ordre
        AND ecd.ecr_ordre = e.ecr_ordre
        AND e.com_ordre = c.com_ordre
        AND ecd.pco_num = p.pco_num
        AND ecd.exe_ordre = p.exe_ordre
        AND ecd.exe_ordre = ge.exe_ordre
        AND ecd.ges_code = ge.ges_code
        AND ecd.ges_code NOT IN (
               SELECT ge1.ges_code
                 FROM gestion_exercice ge1, gestion_exercice ge2
                WHERE ge1.pco_num_185 IS NOT NULL
                  AND ge2.pco_num_185 IS NOT NULL
                  AND ge1.ges_code = ge2.ges_code
                  AND ge1.exe_ordre = ge2.exe_ordre - 1
                  AND ge1.exe_ordre = ecd.exe_ordre)
        AND p.pco_compte_be = p1.pco_num(+)
        AND p.exe_ordre + 1 = p1.exe_ordre(+)
   GROUP BY e.com_ordre,
            e.exe_ordre,
            c.ges_code,
            p.pco_num,
            p.pco_libelle,
            p1.pco_num,
            p1.pco_libelle
     HAVING SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_debit
                 / DECODE (ecd_debit, 0, 1, ecd_debit)
                ) <> 0
         OR SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_credit
                 / DECODE (ecd_credit, 0, 1, ecd_credit)
                ) <> 0
   ORDER BY com_ordre, exe_ordre, ges_code, pco_num;
/

CREATE OR REPLACE FORCE VIEW maracuja.v_be_reprise_gestion (com_ordre,
                                                            exe_ordre,
                                                            ges_code,
                                                            pco_num,
                                                            pco_libelle,
                                                            compte_be,
                                                            compte_be_libelle,
                                                            debits,
                                                            credits
                                                           )
AS
   SELECT   e.com_ordre, e.exe_ordre, ecd.ges_code, p.pco_num, p.pco_libelle,
            DECODE (p1.pco_num, NULL, p.pco_num, p1.pco_num) compte_be,
            DECODE (p1.pco_num,
                    NULL, p.pco_libelle,
                    p1.pco_libelle
                   ) compte_be_libelle,
            SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_debit
                 / DECODE (ecd_debit, 0, 1, ecd_debit)
                ) AS debits,
            SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_credit
                 / DECODE (ecd_credit, 0, 1, ecd_credit)
                ) AS credits
       FROM ecriture_detail ecd,
            ecriture e,
            comptabilite c,
            plan_comptable_exer p,
            plan_comptable_exer p1,
            (SELECT ecd_ordre
               FROM ecriture_detail ecd, ecriture e
              WHERE ecd.ecr_ordre = e.ecr_ordre
                AND SUBSTR (ecr_etat, 1, 1) = 'V'
             MINUS
             SELECT ecd_ordre
               FROM ecriture_detail_be_log) x
      WHERE ecd.ecd_ordre = x.ecd_ordre
        AND ecd.ecr_ordre = e.ecr_ordre
        AND e.com_ordre = c.com_ordre
        AND ecd.pco_num = p.pco_num
        AND ecd.exe_ordre = p.exe_ordre
        AND p.pco_compte_be = p1.pco_num(+)
        AND p.exe_ordre + 1 = p1.exe_ordre(+)
   GROUP BY e.com_ordre,
            e.exe_ordre,
            ecd.ges_code,
            p.pco_num,
            p.pco_libelle,
            p1.pco_num,
            p1.pco_libelle
     HAVING SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_debit
                 / DECODE (ecd_debit, 0, 1, ecd_debit)
                ) <> 0
         OR SUM (  ecd_reste_emarger
                 * SIGN (ecd_montant)
                 * ecd_credit
                 / DECODE (ecd_credit, 0, 1, ecd_credit)
                ) <> 0
   ORDER BY com_ordre, exe_ordre, ges_code, pco_num;
/




CREATE OR REPLACE PACKAGE MARACUJA.Basculer_Be IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree.

Version du 30/09/2010 

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);
PROCEDURE basculer_detail_du_cpt_ges_n(pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT LONG);


-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR, gescode VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
 PROCEDURE priv_archiver_la_bascule_ecd ( ecdordre    INTEGER, utlordre   INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR, exeordreold jefy_admin.exercice.exe_ordre%type) RETURN VARCHAR;

FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER;
FUNCTION priv_getTopOrdre RETURN NUMBER;
FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR;

END Basculer_Be;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      topordre := priv_getTopOrdre;

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );
--

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   
   
-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retraité les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   
   -- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum, exeordreprec)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
      priv_nettoie_ecriture(monecdordre);
   END;

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;


        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN

            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retraité les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
            

      CURSOR lesd
      IS
        SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
            
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
          priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retraité les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;


 -- Crée une ecriture par ecriture_detail recupere
PROCEDURE basculer_detail_du_cpt_ges_n (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode            VARCHAR,
      ecrordres   OUT   LONG
   )
   IS
      cpt             INTEGER;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      leGesCode       ECRITURE_DETAIL.ges_code%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monEcrOrdre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
      oriordre          ecriture.ori_ordre%type;

      CURSOR lesEcd
      IS
         SELECT ecd_ordre, ecd_sens, ges_code, ecd_libelle,NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde, ori_ordre
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_reste_emarger <> 0
            AND pco_num = pconum
            AND ges_code=gescode;

   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      ecrordres := '';
      OPEN lesEcd;

      LOOP
         FETCH lesEcd
          INTO ecdordreOld, lesens, leGesCode, ecdlibelle, lemontant, oriordre;
         EXIT WHEN lesEcd%NOTFOUND;

        -- creation de lecriture  --
        monEcrOrdre := maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     oriordre,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

         -- creation du detail ecriture --
         ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,       --ECDSENS,
                                                     monecrordre,  --ECRORDRE,
                                                     leGesCode, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
          -- basculer les titre_ecriture_detail  --
          priv_histo_relance(ecdordreOld, ecdordre, exeordre);

          -- creation de du log pour ne plus retraiter les ecritures ! --
          priv_archiver_la_bascule_ecd(ecdordreOld, utlordre);

          IF (lesens='C') THEN
            lesens890 := 'D';
          ELSE
            lesens890 := 'C';
          END IF;
          -- creer contrepartie



          ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                    'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '|| pconum,--ECDLIBELLE,
                                                    lemontant,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    monecrordre,      --ECRORDRE,
                                                    priv_getGesCodeCtrePartie(exeordre, legescode),  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
          maracuja.Api_Plsql_Journal.validerecriture (monEcrOrdre);
          ecrordres := ecrordres || monecrordre || '$';

      END LOOP;
      CLOSE lesEcd;


   END;


-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

--      CURSOR lesc
--      IS
--         SELECT ecd_ordre, ecd_libelle,
--                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
--           FROM ECRITURE_DETAIL ecd, ECRITURE e
--          WHERE e.ecr_ordre = ecd.ecr_ordre
--            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--            AND e.exe_ordre = exeordreprec
--            AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                    FROM ECRITURE_DETAIL_BE_LOG)
--            AND ecd_sens = 'C'
--            AND ecd_reste_emarger != 0
--            AND pco_num = pconumtmp;
            
            
      CURSOR lesc
      IS
            SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
            
            

--      CURSOR lesd
--      IS
--         SELECT ecd_ordre, ecd_libelle,
--                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
--           FROM ECRITURE_DETAIL ecd, ECRITURE e
--          WHERE e.ecr_ordre = ecd.ecr_ordre
--            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--            AND e.exe_ordre = exeordreprec
--            AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                    FROM ECRITURE_DETAIL_BE_LOG)
--            AND ecd_sens = 'D'
--            AND ecd_reste_emarger != 0
--            AND pco_num = pconumtmp;
            
      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
            
            
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

              chaine := lespconums;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp, exeordreprec)        --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp, exeordreprec)        --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum, exeordreprec)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

           chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN

            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retraité les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  gescode,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum, exeordreprec)          --PCONUM
                                                     );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum, exeordreprec)          --PCONUM
                                                     );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retraité les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp, exeordreprec)       --PCONUM
                                                     );
                priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                       substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp, exeordreprec)       --PCONUM
                                                     );
                priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

       SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
        IF cpt = 0 THEN
           RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a été transférée en BE pour le compte '|| pconum);
        END IF;

        DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;




PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR, gescode VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      exeordre          ECRITURE.EXE_ORDRE%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;

      SELECT com_ordre, ges_code INTO comordre, gescodeagence
        FROM COMPTABILITE;

        -- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
        gescodeagence := priv_getGesCodeCtrePartie(exeordre, gescode);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         -- creation du detail pour l'ecriture
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
      END IF;
   END;






   PROCEDURE priv_archiver_la_bascule_ecd (
      ecdordre    INTEGER,
      utlordre   INTEGER
   )
   IS
   BEGIN

        INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre, ecdordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre = ecdordre
             AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG);

   END;


   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
   -- si consolidé, on exclue les sacds 
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL 
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum
               and ges_code not in (select ge1.ges_code from gestion_exercice ge1, gestion_exercice ge2
                where ge1.pco_num_185 is not null  and ge2.pco_num_185 is not null
                and ge1.ges_code=ge2.ges_code and ge1.exe_ordre=ge2.exe_ordre-1 and ge1.exe_ordre=exeordre);
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
           DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR, exeordreold jefy_admin.exercice.exe_ordre%type) RETURN VARCHAR
   IS
        pconumnew PLAN_COMPTABLE.pco_num%TYPE;
        flag integer;
   BEGIN
        SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE_exer WHERE pco_num=pconumold and exe_ordre=exeordreold;
        IF pconumnew IS NULL THEN
           pconumnew := pconumold;
        END IF;
        
        select count(*) into flag from PLAN_COMPTABLE_exer WHERE pco_num=pconumnew and exe_ordre=exeordreold+1 and PCO_VALIDITE='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le compte de BE '|| pconumnew || ' défini sur l''exercice '|| exeordreold || ' pour le compte '||pconumold|| ' n''est pas valide sur l''exercice '||exeordreold+1||'.' );
        end if;
        
        
        RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
        IF cpt = 1 THEN
            SELECT * INTO infos
               FROM TITRE_DETAIL_ECRITURE
            WHERE ecd_ordre = ecdordreold;
          INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
               VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
        END IF;


    END;

    -- renvoi le solde non emarge des ecritures non prises en compte en BE
    FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER
    IS
      lesolde NUMBER;
    BEGIN
       IF (gescode IS NULL) THEN
            -- si consolide, on ne prend pas en compte les SACD
              SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
                INTO lesolde
                FROM ECRITURE_DETAIL ecd, ECRITURE e
               WHERE e.ecr_ordre = ecd.ecr_ordre
                 AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
                 AND e.exe_ordre = exeordreprec
                  and ecd.ges_code not in (select ge1.ges_code from gestion_exercice ge1, gestion_exercice ge2
                where ge1.pco_num_185 is not null  and ge2.pco_num_185 is not null
                and ge1.ges_code=ge2.ges_code and ge1.exe_ordre=ge2.exe_ordre-1 and ge1.exe_ordre=exeordreprec)
                 AND ecd_ordre NOT IN (SELECT ecd_ordre
                                         FROM ECRITURE_DETAIL_BE_LOG)                                        
                 AND ecd_sens = sens
                 AND pco_num = pconum;
         ELSE
              SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
                INTO lesolde
                FROM ECRITURE_DETAIL ecd, ECRITURE e
               WHERE e.ecr_ordre = ecd.ecr_ordre
                 AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
                 AND e.exe_ordre = exeordreprec
                 AND ges_code = gescode
                 AND ecd_ordre NOT IN (SELECT ecd_ordre
                                         FROM ECRITURE_DETAIL_BE_LOG)
                 AND ecd_sens = sens
                 AND pco_num = pconum;
        END IF;
        RETURN lesolde;
    END;


    FUNCTION priv_getTopOrdre RETURN NUMBER
    IS
      topordre NUMBER;
    BEGIN
          SELECT top_ordre INTO topordre
                FROM TYPE_OPERATION
                   WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
            RETURN  topordre;
    END;

    -- renvoie gescode si sacd ou gescodeagence
    FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR
    IS
      gescodeagence COMPTABILITE.ges_code%TYPE;
      cpt INTEGER;
    BEGIN
      SELECT ges_code INTO gescodeagence
        FROM COMPTABILITE;

        -- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
        IF gescode IS NOT NULL THEN
                      SELECT COUNT(*) INTO cpt FROM GESTION_EXERCICE
                             WHERE exe_ordre = exeordre AND ges_code=gescode AND pco_num_185 IS NOT NULL;
                    IF cpt<>0 THEN
                                 gescodeagence := gescode;
                    END IF;
        END IF;

        RETURN gescodeagence;

    END;



END Basculer_Be;
/









create procedure grhum.inst_patch_maracuja_1895 is
begin
    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.9.5';           
end;
/






