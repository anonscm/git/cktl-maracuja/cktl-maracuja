
CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e
   WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont>=0
  AND ecd.ecd_credit=0
  AND ecd.ecd_debit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'
  and substr(ecd.pco_num,1,1)='4'
GROUP BY r.rec_id
union all
SELECT  r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
  FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e
  WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont>=0
  AND ecd.ecd_credit=0
  AND ecd.ecd_debit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='R'
  and substr(ecd.pco_num,1,1)='4'
  and not exists(select tde_ordre from titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe where xx.ecd_ordre=xecd.ecd_ordre and xecd.exe_ordre=xe.exe_ordre and xe.exe_stat='O' and xx.tit_id=r.tit_id)
GROUP BY r.rec_id
UNION ALL
SELECT   r.rec_id, -SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont<0
  AND ecd.ecd_debit=0
  AND ecd.ecd_credit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'
  and substr(ecd.pco_num,1,1)='4'
GROUP BY r.rec_id
union all
SELECT  r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
  FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e
  WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont<0
  AND ecd.ecd_debit=0
  AND ecd.ecd_credit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='R'
  and substr(ecd.pco_num,1,1)='4'
  and not exists(select tde_ordre from titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe where xx.ecd_ordre=xecd.ecd_ordre and xecd.exe_ordre=xe.exe_ordre and xe.exe_stat='O' and xx.tit_id=r.tit_id)
GROUP BY r.rec_id;
