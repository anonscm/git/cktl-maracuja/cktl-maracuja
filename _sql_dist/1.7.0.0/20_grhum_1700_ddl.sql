SET DEFINE OFF;
-- MARACUJA.V_BE_REPRISE_CONSOLIDEE
-- MARACUJA.V_BE_REPRISE_GESTION
-- MARACUJA.CFI_ECRITURES
-- MARACUJA.V_MANDAT_REIMP
-- MARACUJA.V_TITRE_REIMP
-- MARACUJA.V_TITRE_RESTE_RECOUVRER
-- MARACUJA.VCREDIT
-- MARACUJA.VCREDIT_BE
-- MARACUJA.VDEBIT
-- MARACUJA.VDEBIT_BE


CREATE OR REPLACE FORCE VIEW MARACUJA.V_BE_REPRISE_CONSOLIDEE
(COM_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, 
 COMPTE_BE, COMPTE_BE_LIBELLE, DEBITS, CREDITS)
AS 
SELECT   e.com_ordre, e.exe_ordre, c.ges_code, p.pco_num,p.pco_libelle, DECODE(p1.pco_num,NULL,p.pco_num,p1.pco_num) compte_be, DECODE(p1.pco_num,NULL,p.pco_libelle,p1.pco_libelle) compte_be_libelle,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit/ DECODE (ecd_debit, 0, 1, ecd_debit)) AS debits,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit) ) AS credits
    FROM ECRITURE_DETAIL ecd,
         ECRITURE e,
         COMPTABILITE c,
         PLAN_COMPTABLE_exer p,
         PLAN_COMPTABLE_exer p1,
         (SELECT ecd_ordre
            FROM ECRITURE_DETAIL ecd, ECRITURE e
           WHERE ecd.ecr_ordre = e.ecr_ordre AND SUBSTR (ecr_etat, 1, 1) = 'V'
          MINUS
          SELECT ecd_ordre
            FROM ECRITURE_DETAIL_BE_LOG) x
   WHERE ecd.ecd_ordre = x.ecd_ordre
     AND ecd.ecr_ordre = e.ecr_ordre
     AND e.com_ordre = c.com_ordre
     AND ecd.pco_num = p.pco_num
     and ecd.exe_ordre = p.exe_ordre
     AND p.pco_compte_be=p1.pco_num(+)
     AND p.exe_ordre=p1.exe_ordre(+)
GROUP BY e.com_ordre, e.exe_ordre, c.ges_code, p.pco_num, p.pco_libelle,p1.pco_num, p1.pco_libelle
  HAVING SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit / DECODE (ecd_debit, 0, 1, ecd_debit)) <> 0
      OR SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit)) <> 0
ORDER BY com_ordre, exe_ordre, ges_code, pco_num;

---------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_BE_REPRISE_GESTION
(COM_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, 
 COMPTE_BE, COMPTE_BE_LIBELLE, DEBITS, CREDITS)
AS 
SELECT   e.com_ordre, e.exe_ordre, ecd.ges_code, p.pco_num,p.pco_libelle, DECODE(p1.pco_num,NULL,p.pco_num,p1.pco_num) compte_be, DECODE(p1.pco_num,NULL,p.pco_libelle,p1.pco_libelle) compte_be_libelle,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit/ DECODE (ecd_debit, 0, 1, ecd_debit)) AS debits,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit) ) AS credits
    FROM ECRITURE_DETAIL ecd,
         ECRITURE e,
         COMPTABILITE c,
         PLAN_COMPTABLE_exer p,
         PLAN_COMPTABLE_exer p1,
         (SELECT ecd_ordre
            FROM ECRITURE_DETAIL ecd, ECRITURE e
           WHERE ecd.ecr_ordre = e.ecr_ordre AND SUBSTR (ecr_etat, 1, 1) = 'V'
          MINUS
          SELECT ecd_ordre
            FROM ECRITURE_DETAIL_BE_LOG) x
   WHERE ecd.ecd_ordre = x.ecd_ordre
     AND ecd.ecr_ordre = e.ecr_ordre
     AND e.com_ordre = c.com_ordre
     AND ecd.pco_num = p.pco_num
     and ecd.exe_ordre = p.exe_ordre
     AND p.pco_compte_be=p1.pco_num(+)
     AND p.exe_ordre=p1.exe_ordre(+)
GROUP BY e.com_ordre, e.exe_ordre, ecd.ges_code, p.pco_num, p.pco_libelle,p1.pco_num, p1.pco_libelle
  HAVING SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit / DECODE (ecd_debit, 0, 1, ecd_debit)) <> 0
      OR SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit)) <> 0
ORDER BY com_ordre, exe_ordre, ges_code, pco_num;

-----------------------------------------


CREATE OR REPLACE FORCE VIEW MARACUJA.CFI_ECRITURES
(EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, ECR_SACD, 
 CREDIT, DEBIT)
AS 
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, api_planco.get_pco_libelle(ed.pco_num, e.exe_ordre) as pco_libelle, ei.ECR_SACD ,SUM(ed.ecd_credit) credit, 0
        FROM ECRITURE_DETAIL ed, ECRITURE e, (SELECT d.ecd_ordre , DECODE(ge.pco_num_185 ,NULL,  'N', 'O') ecr_sacd
FROM ECRITURE_DETAIL d, GESTION_EXERCICE ge WHERE d.EXE_ORDRE =ge.exe_ordre AND d.ges_code=ge.ges_code ) ei
        WHERE ed.ecd_credit<>0
        AND ed.ecr_ordre = e.ecr_ordre
        AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,ei.ecr_sacd
UNION ALL
SELECT e.exe_ordre, ed.ges_code, ed.pco_num,  api_planco.get_pco_libelle(ed.pco_num, e.exe_ordre) as pco_libelle, ei.ECR_SACD, 0, SUM(ed.ecd_debit) debit
        FROM ECRITURE_DETAIL ed, ECRITURE e, (SELECT d.ecd_ordre , DECODE(ge.pco_num_185 ,NULL,  'N', 'O') ecr_sacd
FROM ECRITURE_DETAIL d, GESTION_EXERCICE ge WHERE d.EXE_ORDRE =ge.exe_ordre AND d.ges_code=ge.ges_code ) ei
        WHERE ed.ecd_debit<>0
        AND ed.ecr_ordre = e.ecr_ordre
        AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num, ei.ecr_sacd;
        
--------------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, GES_LIB, IMP_LIB, BOR_DATE_VISA, BOR_NUM, 
 MAN_TTC)
AS 
SELECT mr.exe_ordre, mr.ecr_date_saisie, mr.ecr_date,
      mr.ges_code, mr.pco_num, mr.bor_id, mr.tbo_ordre, mr.man_id, mr.man_num, mr.man_lib,
      mr.man_mont, mr.man_tva, mr.man_etat, mr.fou_ordre, mr.ecr_ordre,
      mr.ecr_sacd, o.org_lib, api_planco.get_pco_libelle(mr.pco_num, mr.exe_ordre) pco_libelle, b.bor_date_visa, b.bor_num,
      man_mont + man_tva
 FROM v_mandat_reimp_0 mr, v_organ_exer o, BORDEREAU b
WHERE mr.ges_code = o.org_ub
  AND o.org_niv = 2
  AND mr.EXE_ORDRE=o.exe_ordre
  AND mr.bor_id = b.bor_id;        


------------------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT, GES_LIB, IMP_LIB, DEBITEUR, BOR_NUM)
AS 
SELECT tr.*, o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, o2.ORG_UB||' '||o2.ORG_CR||' '||o2.ORG_souscr, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_organ_exer o2, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND rec_interne = o2.org_id AND tr.exe_ordre = o2.exe_ordre AND o2.org_niv >=2
AND tr.bor_id = b.bor_id
AND tr.rec_interne IS NOT NULL
UNION
SELECT tr.*, o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, f.ADR_NOM||' '||f.ADR_PRENOM, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_fournisseur f, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.fou_ordre = f.fou_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NOT NULL AND tr.rec_interne IS NULL
UNION
SELECT tr.*, o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, tr.rec_debiteur, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NULL AND tr.rec_debiteur IS NOT NULL AND LENGTH(tr.REC_DEBITEUR)<>0;

-----------------------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_RESTE_RECOUVRER
(EXE_ORDRE, EXE_EXERCICE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 TIT_NUMERO, TIT_DATE_EMISSION, REC_DEBITEUR, TIT_ID, FOU_CODE, 
 ADR_NOM, ADR_PRENOM, ADR_CP, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_VILLE, TIT_ETAT, TIT_HT, TIT_TTC, TIT_TVA, 
 TIT_LIBELLE, ORI_ENTITE, ORI_KEY_ENTITE, ECD_CREDIT, ECD_DEBIT, 
 RESTE_RECOUVRER)
AS 
SELECT t.exe_ordre, ex.exe_exercice, ecd.pco_num, t.ges_code, api_planco.get_pco_libelle(ecd.pco_num, t.exe_ordre) pco_libelle, t.TIT_NUMERO, x.tit_date_emission,  x.rec_debiteur,
t.tit_id, f.FOU_CODE, f.ADR_NOM, f.ADR_PRENOM, f.ADR_CP, f.ADR_ADRESSE1, f.ADR_ADRESSE2,f.ADR_VILLE,
    t.TIT_ETAT, t.tit_ht, t.TIT_TTC, t.TIT_TVA, t.TIT_LIBELLE, o.ORI_ENTITE ,
o.ORI_KEY_ENTITE, ecd.ECD_CREDIT, ecd.ecd_debit, ecd.ecd_reste_emarger reste_recouvrer
FROM ECRITURE_DETAIL ecd,
ECRITURE e,
TITRE_DETAIL_ECRITURE tde,
TITRE t,
v_fournisseur f,
ORIGINE o,
EXERCICE ex,
(SELECT TIT_ID, MAX(ecd1.EXE_ORDRE) EXE_ORDRE_MAX
FROM TITRE_DETAIL_ECRITURE td1, ECRITURE_DETAIL ecd1 WHERE td1.ecd_ordre=ecd1.ecd_ordre AND ((ecd1.pco_num LIKE '4%' AND ecd1.pco_num NOT LIKE '445%') OR ecd1.pco_num LIKE '5%')
GROUP BY tit_id) z,
(SELECT tit_id, MAX(rec_date) tit_date_emission , MAX(rec_debiteur) rec_debiteur, MAX(fou_ordre) fou_ordre FROM RECETTE GROUP BY tit_id) x
WHERE
    e.ecr_ordre = ecd.ecr_ordre
AND t.exe_ordre=ex.exe_ordre
AND tde.ecd_ordre=ecd.ecd_ordre
AND tde.tit_id=t.tit_id
AND tde.tit_id=z.tit_id
AND tde.EXE_ORDRE=z.exe_ordre_max
AND x.tit_id=t.tit_id
AND x.fou_ordre=f.fou_ordre(+)
AND t.ori_ordre=o.ori_ordre(+)
AND ((ecd.pco_num LIKE '4%' AND ecd.pco_num NOT LIKE '445%') OR ecd.pco_num LIKE '5%')
AND e.ecr_numero > 0
AND SUBSTR(e.ecr_etat,1,1)='V';

--------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS
SELECT j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,api_planco.get_pco_libelle(e.pco_num, j.exe_ordre) as pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, v_ECRITURE_INFOS ei
        WHERE e.ecd_credit<>0
        AND e.ecr_ordre = j.ecr_ordre
        AND SUBSTR(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre <> 2        
        AND e.ECd_ORDRE = ei.ECd_ORDRE
        GROUP BY j.exe_ordre,j.ecr_date_saisie,e.pco_num,e.ges_code,ei.ECR_SACD;
        
-----------------------------------------


CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,api_planco.get_pco_libelle(e.pco_num, j.exe_ordre) as pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, v_ECRITURE_INFOS ei
        WHERE e.ecd_credit<>0
        AND e.ecr_ordre = j.ecr_ordre
		AND SUBSTR(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre = 2
        AND e.ECd_ORDRE = ei.ECd_ORDRE 
        GROUP BY j.exe_ordre,j.ecr_date_saisie,e.pco_num,e.ges_code,ei.ECR_SACD;
        
-------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,api_planco.get_pco_libelle(e.pco_num, j.exe_ordre) as pco_libelle,ei.ecr_sacd,SUM(e.ecd_debit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, v_ECRITURE_INFOS ei
        WHERE e.ecd_debit<>0
        AND e.ecr_ordre = j.ecr_ordre
		AND SUBSTR(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre <> 2      
        AND e.ECd_ORDRE = ei.ECd_ORDRE
        GROUP BY j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,ei.ECR_SACD;
        
-------------------------------------        
        
CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,api_planco.get_pco_libelle(e.pco_num, j.exe_ordre) as pco_libelle,ei.ecr_sacd,SUM(e.ecd_debit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, v_ECRITURE_INFOS ei
        WHERE e.ecd_debit<>0
        AND e.ecr_ordre = j.ecr_ordre
		AND SUBSTR(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre = 2      
        AND e.ECd_ORDRE = ei.ECd_ORDRE
        GROUP BY j.exe_ordre, j.ecr_date_saisie,e.pco_num,e.ges_code,ei.ECR_SACD        ;
        
-------------------------------------  



