SET DEFINE OFF;
Set scan off; 

GRANT SELECT ON  JEFY_ADMIN.PARAMETRE TO EPN;


CREATE OR REPLACE PACKAGE EPN.Epn2
IS

FUNCTION epn_getDeviseTaux(exeOrdre NUMBER) RETURN NUMBER;
FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2;
PROCEDURE  Epn_Proc_Bal_1 (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2, comp VARCHAR2, exe_ordre NUMBER, v_date VARCHAR2);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn2  IS
-- v. du 12/03/2007 (code nomen. M9-3, M9-1, etc.)
-- v. du 18/03/2008 (devise locale)


FUNCTION epn_getDeviseTaux(exeOrdre NUMBER) RETURN NUMBER  
-- Ajout by ProfesseurBougna le 18 mars 2008   
IS
  deviseTaux jefy_admin.parametre.par_value%TYPE;
BEGIN
	SELECT PAR_VALUE INTO deviseTaux FROM jefy_admin.PARAMETRE
	WHERE PAR_KEY = 'DEVISE_TAUX' AND exe_ordre=exeOrdre;

	IF deviseTaux IS NULL THEN deviseTaux:=1;
	END IF;
	RETURN deviseTaux;
END;


FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2
IS
  codeNomenclature maracuja.parametre.par_value%TYPE;
BEGIN
	SELECT PAR_VALUE INTO codeNomenclature FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'EPN_CODE_NOMENCLATURE' AND exe_ordre=exeOrdre;

	IF codeNomenclature IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'EPN_CODE_NOMENCLATURE doit etre renseigne dans maracuja.PARAMETRE');
	END IF;
	RETURN codeNomenclature;
END;


PROCEDURE Epn_Proc_Bal_1 (ordre VARCHAR2,comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2)
IS

JOU_DATE DATE;
IMPUTATION VARCHAR2(15);
GESTION VARCHAR2(10);
ECR_SACD EPN_BALANCE.ECR_SACD%TYPE;
BE_CR EPN_BALANCE.BE_CR%TYPE;
MONTANT_CR EPN_BALANCE.MONTANT_CR%TYPE;
BE_DB EPN_BALANCE.BE_DB%TYPE;
MONTANT_DB EPN_BALANCE.MONTANT_DB%TYPE;
NUM_SEQ NUMBER(5);
num_seq_f NUMBER(5);
bsdeb NUMBER(15,2);
bscre NUMBER(15,2);
debtot NUMBER(15,2);
cretot NUMBER(15,2);
NEW_EPNB_ORDRE INTEGER;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
TYP_DOC VARCHAR2(2);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num NUMBER;
codeNomenclature VARCHAR2(2);
flag INTEGER;
deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008  

--erreur EXCEPTION(2);


	CURSOR c_tot IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), 	SUM(MONTANT_DB)
	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_sacd IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND GESTION = comp AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_hors_sacd IS SELECT IMPUTATION,ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
	AND GESTION NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY IMPUTATION, ECR_SACD
	ORDER BY IMPUTATION;


BEGIN


	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;

	codeNomenclature := epn_getCodeNomenclature(EXERC);
	deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008   

	IF comp =  'HSA'  THEN

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num  AND
			EPNB_GES_CODE NOT IN (SELECT ges_code FROM   SACD_PARAM );
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num
			AND EPNB_GES_CODE NOT IN (SELECT ges_code FROM jefy.SACD_PARAM );
		END IF;  */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_EXERCICE = exerc
			AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_EXERCICE = exerc
			AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL
		WHERE EPNB_ORDRE = ordre_num AND EPNB_GES_CODE = comp ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num;
		END IF;   */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_GES_CODE = comp
			AND EPNB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE = comp AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_GES_CODE = comp
			AND EPNB2_EXERCICE = exerc;
		END IF;

	END IF;

	--	SELECT SEQ_EPN_BAL.NEXTVAL INTO NEW_EPNB_ORDRE FROM dual;
	--  SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';


	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;

	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	-- TO_DATE(TO_CHAR(fin_premiere,'dd/mm/yyyy'),'dd/mm/yyyy')

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre = '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
	  		VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
			RANG := '03';
			VDATE := '3009' || EXERC;
	  		VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
	  			VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					VDATE:= V_DATE;
					RANG := '05';
	  				VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						VDATE:= V_DATE;
						RANG := '06';
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

--	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');
--	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');

	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');
	VARDATE := TO_DATE(VDATE,'ddmmyyyy');

    	/* INSERT INTO EPN_BAL VALUES (ORDRE, SYSDATE,comp); */

		--type doc := '03'

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;

		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_BAL_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,
EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET,0);

	NUM_SEQ := 2;

	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  		SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
		-- VDATE := '31122005';
		VDATE := '3112'|| EXERCN1;
	 	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	/* pas de consolidation Etablissement + SACD
	IF comp =  'ALL'  THEN
	-- Etablissement + SACD --
		OPEN c_tot;
		LOOP
		FETCH c_tot INTO IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_tot%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB,MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp = 'HSA' THEN
		-- 	Etablissement --
		OPEN c_hors_sacd;
	    LOOP
		FETCH c_hors_sacd INTO  IMPUTATION,  ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_hors_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		-- Modifi� by ProfesseurBougna le 18 mars 2008  
		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB*deviseTaux, MONTANT_DB*deviseTaux, debtot*deviseTaux, BE_CR*deviseTaux, MONTANT_CR*deviseTaux, cretot*deviseTaux, bsdeb*deviseTaux, bscre*deviseTaux, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- SACD	--
		OPEN c_sacd;
		LOOP
		FETCH c_sacd INTO  IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		-- Modifi� by ProfesseurBougna le 18 mars 2008  
		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), BE_DB*deviseTaux, MONTANT_DB*deviseTaux, debtot*deviseTaux, BE_CR*deviseTaux, MONTANT_CR*deviseTaux, cretot*deviseTaux, bsdeb*deviseTaux, bscre*deviseTaux, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_BAL_1 SET EPNB1_NB_ENREG = NUM_SEQ
	WHERE EPNB_ORDRe = ORDRE AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;

END;



PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
DEP_MONT  NUMBER(15,2);
REVERS NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;
deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008  

	CURSOR c_tot IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS)
 	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd  IS SELECT PCO_NUM,SUM( DEP_MONT), SUM( REVERS)
	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY PCO_NUM
	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) FROM EPN_DEP_BUD
	WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

/*DELETE TAB_MANDAT_MAJ;

INSERT INTO TAB_MANDAT_MAJ SELECT * FROM MANDAT_MAJ;*/

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);
	deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008    

	IF comp = 'HSA'  THEN
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
		AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
			AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
		AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
			AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp
			AND EPNDB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp AND EPNDB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp
			AND EPNDB2_EXERCICE = exerc;
		END IF;

	END IF;


	-- SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre = EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans 	maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE := V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';

	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND EXE_ORDRE = exerc;
	END IF;

	INSERT INTO EPN_DEP_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET, 					EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;
	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		DATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	-- Pas de consolidation Etablissement + SACD
	/* IF comp = 'ALL'  THEN
	-- Etablissement + SACD --
	OPEN c_tot;
	LOOP
		FETCH c_tot INTO  PCO_NUM,  DEP_MONT, REVERS;
		EXIT WHEN c_tot%NOTFOUND;
		compte := PCO_NUM || '              ';
		INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0,  DEP_MONT-REVERS);
		NUM_SEQ := NUM_SEQ + 1;
	END LOOP;
	CLOSE c_tot;

	ELSE */
	IF comp = 'HSA' THEN
		--  hors SACD
		OPEN c_hors_sacd ;
		LOOP
			FETCH c_hors_sacd INTO  PCO_NUM, DEP_MONT, REVERS ;
			EXIT WHEN c_hors_sacd%NOTFOUND ;
			compte := PCO_NUM || '              ' ;

			-- Modifi� by ProfesseurBougna le 18 mars 2008  
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;
			NUM_SEQ := NUM_SEQ + 1 ;
			END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		--  cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO  PCO_NUM,  DEP_MONT, REVERS;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			
			-- Modifi� by ProfesseurBougna le 18 mars 2008  
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;
	END IF;

	-- END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_DEP_BUD_1 SET EPNDB1_NBENREG = num_seq
	WHERE EPNDB_ORDRE = ORDRE_NUM AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;

END;


PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
MONT_REC  NUMBER(15,2);
ANNUL_TIT_REC NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;
deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008   

	CURSOR c_tot IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND GES_CODE = comp AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);
	deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008  

	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
		AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
			AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
		AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
			AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc AND EPNRB1_GES_CODE = comp;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_GES_CODE = comp
			AND EPNRB1_EXERCICE = exerc;
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num  AND EPNRB2_GES_CODE = comp AND EPNRB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_GES_CODE = comp
			AND EPNRB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
						RANG := '05';
						VDATE := V_DATE;
  						VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp,'1', 1, IDENTIFIANT,TYP_DOC, codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;

 	/*
	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
 	*/

	/*IF comp = 'ALL' THEN
	-- Etablissement + SACD
		OPEN c_tot;
		LOOP
			FETCH c_tot INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_tot%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp =  'HSA' THEN
	-- 	hors SACD
		OPEN c_hors_sacd;
		LOOP
			FETCH c_hors_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_hors_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			
			-- Modifi� by ProfesseurBougna le 18 mars 2008  
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, (SUBSTR(compte,1,15)),
				MONT_REC*deviseTaux, 0, ANNUL_TIT_REC*deviseTaux, 0,  (MONT_REC - ANNUL_TIT_REC)*deviseTaux, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			
            -- Modifi� by ProfesseurBougna le 18 mars 2008  			
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC*deviseTaux, 0, ANNUL_TIT_REC*deviseTaux, 0,  (MONT_REC - ANNUL_TIT_REC)*deviseTaux, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 - MAJ Nb enreg en fonction du ges_code
	UPDATE EPN_REC_BUD_1 SET EPNRB1_NBENREG = num_seq
	WHERE EPNRB_ORDRE = ORDRE_NUM AND EPNRB1_GES_CODE = comp AND EPNRB1_EXERCICE = exerc;


END;



PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)

IS

NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num     NUMBER;
PCONUM      VARCHAR2(6);
MNTOUV      NUMBER(15,2);
MNTNET      NUMBER(15,2);
MNTNOEMPLOI NUMBER(15,2);
SENSCPT		VARCHAR2(1);
TYP_DOC  VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;
deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008   

	CURSOR c2_tot IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_hors_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;

	CURSOR c3_tot  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_hors_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;


	/*
	CURSOR c2 IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP
	GROUP BY pco_num_bdn;

	CURSOR c3 IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC
	GROUP BY pco_num_bdn;
	*/

BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);
	deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008   	
	
	DELETE FROM BUDNAT_PLANCO;

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,2), '1'
	FROM maracuja.PLAN_COMPTABLE
	WHERE (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%') AND PCO_VALIDITE = 'VALIDE';

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,3), '2'
	FROM maracuja.PLAN_COMPTABLE
	WHERE ( PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%')AND PCO_VALIDITE = 'VALIDE';

	/* des donn�es du cadre 2 */

	DELETE FROM EPN_DEP;

	INSERT INTO EPN_DEP
	-- depenses + OR --
	SELECT -- Modifi� by ProfesseurBougna le 18 mars 2008  
		db.PCO_NUM,
		bp.PCO_NUM_BDN,
		db.GES_CODE,
		db.DEP_MONT*deviseTaux MDT_MONT,
		db.REVERS*deviseTaux TIT_MONT,
		0 CO,
		bp.SECTION,
		db.EPN_DATE EPN_DATE,
		db.EXE_ORDRE
		FROM EPN_DEP_BUD db, BUDNAT_PLANCO bp
		WHERE db.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_DEP
	-- OR --
	SELECT
	  EPN_DEP0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_DEP0.GES_CODE,
	  EPN_TITV.ECD_DATE EPN_DATE,
	  0,
	  EPN_TITV.ECD_MONTANT TIT_MONT,
	  EPN_DEP0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITV, EPN_DEP0, BUDNAT_PLANCO
	WHERE EPN_DEP0.GES_CODE = EPN_TITV.GES_CODE (+)
	AND EPN_DEP0.PCO_NUM = EPN_TITV.PCO_NUM (+)
	AND EPN_DEP0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_DEP 	SET MDT_MONT = 0
		WHERE MDT_MONT IS NULL;

	UPDATE EPN_DEP	SET TIT_MONT = 0
		WHERE TIT_MONT IS NULL;*/

	DELETE FROM EPN_DEP WHERE MDT_MONT = 0 AND CO = 0 AND TIT_MONT = 0;

	INSERT INTO EPN_DEP -- Modifi� by ProfesseurBougna le 18 mars 2008  
	-- pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO*deviseTaux, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn , BUDNAT_PLANCO bp
	WHERE bn.PCO_NUM = bp.PCO_NUM_bdn
	AND (bn.PCO_NUM LIKE '6%' OR bn.PCO_NUM LIKE '2%')
	AND CO <> 0
	ORDER BY exe_ordre, ges_code, bn.pco_num;


	/* Impossible de faire des pr�visions budg�taires de d�penses classe 1 dans jefyco --
	INSERT INTO EPN_DEP
	SELECT UNIQUE EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .GES_CODE, DATE_CO,
	0, 0, EPN_BUDNAT .CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT .PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT .PCO_NUM
	AND BUDNAT_PLANCO.PCO_NUM LIKE '1%';*/

/* des donn�es du cadre 3 */

	DELETE FROM EPN_REC;

	INSERT INTO EPN_REC -- Modifi� by ProfesseurBougna le 18 mars 2008  
	-- Recettes et D�ductions--
	SELECT erb.PCO_NUM,
	  bp.PCO_NUM_BDN,
	  erb.GES_CODE,
	  erb.MONT_REC*deviseTaux TIT_MONT,
	  erb.ANNUL_TIT_REC*deviseTaux RED_MONT,
	  0 CO,
	  bp.SECTION,
	  erb.EPN_DATE,
	  erb.exe_ordre
	FROM EPN_REC_BUD erb, BUDNAT_PLANCO bp
	WHERE erb.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_REC
	-- Reduction recettes --
	SELECT EPN_REC0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_REC0.GES_CODE,
	  EPN_TITD.ECD_DATE EPN_DATE,
	  0 TIT_MONT,
	  EPN_TITD.ECD_MONTANT RED_MONT,
	  EPN_REC0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITD,  EPN_REC0, BUDNAT_PLANCO
	WHERE EPN_REC0.GES_CODE = EPN_TITD.GES_CODE (+)
	AND EPN_REC0.PCO_NUM = EPN_TITD.PCO_NUM (+)
	AND EPN_REC0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_REC SET TIT_MONT = 0 WHERE TIT_MONT IS NULL;

	UPDATE EPN_REC SET RED_MONT = 0 WHERE RED_MONT IS NULL;*/

	DELETE FROM EPN_REC WHERE TIT_MONT = 0 AND RED_MONT = 0 AND CO = 0;


	INSERT INTO EPN_REC -- Modifi� by ProfesseurBougna le 18 mars 2008  
	-- Pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO*deviseTaux, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn, BUDNAT_PLANCO bp
	WHERE bp.PCO_NUM_BDN = bn.PCO_NUM
	AND ( bn.PCO_NUM LIKE '7%' OR bn.PCO_NUM LIKE '1%')
	AND CO <> 0;


	/* Impossible de faire des pr�visions budg�taires de recettes classe 2 dans jefyco
	INSERT INTO EPN_REC
	SELECT UNIQUE EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.GES_CODE, DATE_CO, 0, 0,
	EPN_BUDNAT.CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT.PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT.PCO_NUM
	AND (BUDNAT_PLANCO.PCO_NUM LIKE '26%' OR BUDNAT_PLANCO.PCO_NUM LIKE '27%');*/



	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
		AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
			AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
		AND EPNCCB2_GES_CODE  NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
			AND EPNCCB2_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_GES_CODE = comp
			AND EPNCCB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB2_GES_CODE = comp AND EPNCCB2_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_GES_CODE = comp
			AND EPNCCB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C
	FROM maracuja.PARAMETRE WHERE PAR_KEY = 	'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;


	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
				VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE:= V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						RANG := '06';
						VDATE := V_DATE;
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

	--	type doc = '03'
	-- SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;

	--IF (comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
	-- Correction M1
	TYP_DOC := '03';


	-- Correction M1 recherche du code du budget
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;

	INSERT INTO EPN_CRE_BUD_1 VALUES (ORDRE,comp, '1', 1, IDENTIFIANT,TYP_DOC , codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);


	/*
	  IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
     END IF;
	*/

	NUM_SEQ := 2;

	/*IF comp = 'ALL' THEN
		OPEN c2_tot;
		LOOP
			FETCH c2_tot INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
			ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2
			VALUES (ORDRE, comp, '2', num_seq, 1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_tot;

		OPEN c3_tot;
		LOOP
			FETCH c3_tot INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_tot;

	ELSE */
	IF comp =  'HSA' THEN

		OPEN c2_hors_sacd;
		LOOP
			FETCH c2_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_hors_sacd;

		OPEN c3_hors_sacd;
		LOOP
			FETCH c3_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_hors_sacd;

	ELSE
		OPEN c2_sacd;
		LOOP
			FETCH c2_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_sacd;


		OPEN c3_sacd;
		LOOP
			FETCH c3_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE  MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

			END LOOP;
			CLOSE c3_sacd;

	END IF;

	--END IF;

UPDATE EPN_CRE_BUD_1 SET EPNCCB1_NBENREG = num_seq
WHERE EPNCCB_ORDRE = ORDRE_NUM AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;

END;

END;
/










