SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.api_planco
IS


    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire              VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE;


    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco (
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;
      
      function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type;      
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_planco IS


    /** Creer un compte de prestation interne a partir du compte budgetaire */
    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE
    is
        flag integer;
        plancoexer plan_comptable_exer%rowtype; 
    begin
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        if (flag=0) then
            return null;
        end if;
        
        select * into plancoexer from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        
        return creer_planco(exeordre,    
            '18'||pconumBudgetaire,
            plancoexer.pco_libelle,
            plancoexer.pco_budgetaire,
            plancoexer.pco_emargement,
            plancoexer.pco_nature,
            plancoexer.pco_sens_emargement,
            plancoexer.pco_validite,
            plancoexer.pco_j_exercice,
            plancoexer.pco_j_fin_exercice,
            plancoexer.pco_j_be);
         
    end;

    /** creer un enregistrement dans plan comptable (plan_comptable_exer + plan_comptable)  */
    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE
    is
        res plan_comptable.pco_num%TYPE;
    begin
        res := prv_fn_creer_planco_exer(exeordre, pconum,pcolibelle,pcobudgetaire,pcoemargement,pconature,pcosensemargement,pcovalidite,pcojexercice,pcojfinexercice,pcojbe);
        return res;
    end;

----------------------------------------------

   FUNCTION prv_fn_creer_planco (pconum VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE
   IS
      flag   INTEGER;
      pconiveau integer;
   BEGIN
      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable
       WHERE pco_num = pconum;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable
                  (pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite,                   --PCO_VALIDITE,
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

      RETURN pconum;
   END;
   
   -----------------------------------------------------
   
   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
   RETURN plan_comptable_exer.pco_num%TYPE
    IS
      flag   INTEGER;
      pconiveau integer;
      res plan_comptable_exer.pco_num%TYPE;
   BEGIN
        

      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable_exer
       WHERE pco_num = pconum and exe_ordre=exeordre;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable_exer
                  (pcoe_id, exe_ordre, pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (plan_comptable_exer_seq.nextval, 
                   exeordre, 
                   pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite, --PCO_VALIDITE
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

        -- creer le compte dans plan comptable s'il n'existe pas
       res := prv_fn_creer_planco(pconum,
                    pcolibelle,
                    pcobudgetaire,
                    pcoemargement,
                    pconature,
                    pcosensemargement,
                    pcovalidite,
                    pcojexercice,
                    pcojfinexercice,
                    pcojbe);        
      RETURN pconum;
   END;
   
    function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type
    is
        res plan_comptable_exer.pco_libelle%type;
    begin
        select pco_libelle into res from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        return res;
    end;   
   
END;
/


