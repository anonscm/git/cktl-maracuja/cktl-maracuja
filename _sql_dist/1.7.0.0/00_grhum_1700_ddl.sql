SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.v_bordereau_rejet
(EXE_ORDRE, BT_TYPE, TYPE_BORDEREAU_ORIGINE, GES_CODE, NUMERO_OBJET, 
 BOR_NUMERO_ORIGINE, BORDEREAU_REJET_NUMERO, PCO_NUM, MONTANT_HT, MONTANT_TVA, 
 MONTANT_TTC, MOTIF_REJET, DATE_REJET, REJETE_PAR, BORDEREAU_REJET_ETAT)
AS 
select 
m.exe_ordre, tbo2.tbo_libelle as BT_type, 
tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.man_numero numero_objet, b.bor_num bor_numero_origine,
brj.brj_num bordereau_rejet_numero, pco_num, man_ht montant_ht, man_tva montant_tva, man_ttc montant_ttc, man_motif_rejet motif_rejet, b.BOR_DATE_VISA date_rejet,(u.nom_usuel ||' '|| u.prenom) as REJETE_PAR,  
brj.brj_etat bordereau_rejet_etat
from maracuja.mandat m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo, 
maracuja.type_bordereau tbo2, 
jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
union all
select m.exe_ordre, tbo2.tbo_libelle as BT_type, tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.tit_numero numero_objet, b.bor_num bor_numero_origine, 
brj.brj_num bordereau_rejet_numero, pco_num, tit_ht montant_ht, tit_tva montant_tva, tit_ttc montant_ttc, tit_motif_rejet motif_rejet,b.BOR_DATE_VISA date_rejet, (u.nom_usuel ||' '|| u.prenom) as REJETE_PAR ,  
brj.brj_etat bordereau_rejet_etat
from maracuja.titre m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo,  maracuja.type_bordereau tbo2, jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre;




CREATE OR REPLACE FORCE VIEW COMPTEFI.V_DVLOP_DEP_LISTE
(EXE_ORDRE, GES_CODE, SECTION, PCO_NUM_BDN, PCO_NUM,
MANDATS, REVERSEMENTS, CR_OUVERTS, DEP_DATE)
AS
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, SUM(man_ht), 0, 0, e.DATE_SAISIE
-- Mandat d�penses
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND tbo_ordre <> 8 AND tbo_ordre <>18 AND tbo_ordre <> 16
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement avant 2007
SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0, SUM(tit_ht), 0, e.DATE_SAISIE
FROM maracuja.TITRE t, maracuja.bordereau b, comptefi.v_planco p, (
  SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
  WHERE
  m.tit_id=mde.tit_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.tit_id ) e
WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 8
AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id
GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement � partir de 2007
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, 0, -SUM(man_ht), 0, e.DATE_SAISIE
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND (tbo_ordre = 8 OR tbo_ordre=18)
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- Cr�dits ouverts
SELECT EXE_ORDRE, GES_CODE, '1' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '6%' OR pco_num LIKE '7%')
AND BDN_QUOI ='D' AND co <> 0
UNION ALL
SELECT EXE_ORDRE, GES_CODE, '2' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%')
AND BDN_QUOI ='D' AND co <> 0 ;





CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_TITRES_CREDITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, tde_origine
  FROM maracuja.TITRE m,
       maracuja.BORDEREAU b,
       maracuja.TITRE_DETAIL_ECRITURE mde,
       maracuja.ECRITURE_DETAIL ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ECRITURE e
 WHERE m.bor_id = b.bor_id
   AND m.tit_id = mde.tit_id
   and mde.exe_ordre=m.exe_ordre
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   AND tde_origine IN ('VISA', 'REIMPUTATION')
   AND tit_etat IN ('VISE')
   AND ecd_credit <> 0
   AND tbo_ordre IN (7, 11, 200)
    AND e.top_ordre<>11
   AND ABS (ecd_montant) = ABS (tit_ht);
  
  
  
CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_MANDATS_DEBITS (EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 MAN_ID, MAN_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 MAN_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, MDE_ORIGINE)
AS   
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
      m.man_id, m.man_numero, ed.ecd_libelle, ecd_montant,
      (SIGN (ecd_montant)) * m.man_tva tva, m.man_etat, m.fou_ordre,
      ed.ecr_ordre, ei.ecr_sacd, mde_origine
 FROM maracuja.mandat m,
        maracuja.bordereau b,
      maracuja.type_bordereau tb,
      maracuja.mandat_detail_ecriture mde,
      maracuja.ecriture_detail ed,
      maracuja.v_ecriture_infos ei,
      maracuja.ecriture e
WHERE b.bor_id=m.bor_id
  AND  m.man_id = mde.man_id
  and mde.exe_ordre=m.exe_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  and b.TBO_ORDRE = tb.TBO_ORDRE
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine IN ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  AND ecd_debit <> 0
  AND tb.tbo_sous_type<> 'REVERSEMENTS'
  AND tb.tbo_sous_type<> 'SCOLARITE'
  AND ABS (ecd_montant) = ABS (man_ht);


CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_OR_CREDITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.man_id, m.man_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.man_tva tva, m.man_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, mde_origine
  FROM maracuja.mandat m,
       maracuja.bordereau b,
	   maracuja.type_bordereau tb,
       maracuja.mandat_detail_ecriture mde,
       maracuja.ecriture_detail ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ecriture e
 WHERE m.bor_id = b.bor_id
   AND b.tbo_ordre=tb.tbo_ordre
   AND m.man_id = mde.man_id
   and mde.exe_ordre=m.exe_ordre
   AND mde.ecd_ordre = ed.ecd_ordre   
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   --AND r.pco_num_ancien = ed.pco_num
   AND mde_origine IN ('VISA', 'REIMPUTATION')
   AND man_etat IN ('VISE')
   AND ecd_credit <> 0
   AND tb.TBO_SOUS_TYPE='REVERSEMENTS'
   AND ABS (ecd_montant) = ABS (man_ht)
   AND ed.exe_ordre>=2007
UNION ALL
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, tde_origine
  FROM maracuja.titre m,
       maracuja.bordereau b,
       maracuja.titre_detail_ecriture mde,
       maracuja.ecriture_detail ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ecriture e
 WHERE m.bor_id = b.bor_id
   AND m.tit_id = mde.tit_id
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   --AND r.pco_num_ancien = ed.pco_num
   AND tde_origine IN ('VISA', 'REIMPUTATION')
   AND tit_etat IN ('VISE')
   AND ecd_credit <> 0
   AND tbo_ordre = 8
   AND ABS (ecd_montant) = ABS (tit_ht)
   AND ed.exe_ordre<2007;
   
CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_REDUCTIONS_DEBITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, tde_origine
  FROM maracuja.titre m,
       maracuja.bordereau b,
       maracuja.titre_detail_ecriture mde,
       maracuja.ecriture_detail ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ecriture e
 WHERE m.bor_id = b.bor_id
   AND m.tit_id = mde.tit_id
   and mde.exe_ordre=m.exe_ordre
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   --AND r.pco_num_ancien = ed.pco_num
   AND tde_origine IN ('VISA', 'REIMPUTATION')
   AND tit_etat IN ('VISE')
   AND ecd_debit <> 0
   AND tbo_ordre = 9
   AND ABS (ecd_montant) = ABS (tit_ht);   



CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT)
AS 
-- titres de recettes 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND tbo_ordre<>9 AND tbo_ordre<>11 AND tbo_ordre<>200 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- titres de recettes des PI
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND (tbo_ordre=11 or tbo_ordre=200) 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- reductions de recettes 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie AS ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_DEBIT, 
      (SIGN(ecd_debit))*ABS(t.tit_tva), ecd_debit+((SIGN(ecd_debit))*ABS(t.tit_tva)), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_debit<>0 
  AND tbo_ordre=9 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht);


