

SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER);
PROCEDURE prv_apres_visa_bordereau (borid INTEGER);
PROCEDURE prv_apres_visa_reversement(borid INTEGER);
PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER);


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 1.7.4.2 - reimputations sur les liquidations


PROCEDURE apres_visa_bordereau (borid INTEGER)
IS
  ex INTEGER;
BEGIN
     SELECT exe_ordre INTO ex FROM BORDEREAU WHERE bor_id=borid;
     IF (ex < 2007) THEN
         prv_apres_visa_bordereau_2006(borid);
     ELSE
          prv_apres_visa_bordereau(borid);
     END IF;

END;



PROCEDURE prv_apres_visa_bordereau(borid INTEGER)
IS
  leBordereau BORDEREAU%ROWTYPE;
  leSousType TYPE_BORDEREAU.TBO_SOUS_TYPE%TYPE;
BEGIN
     SELECT * INTO leBordereau FROM BORDEREAU WHERE bor_id=borid;
     SELECT TBO_SOUS_TYPE INTO leSousType FROM TYPE_BORDEREAU WHERE tbo_ordre = leBordereau.tbo_ordre;
     IF leBordereau.bor_etat = 'VISE' THEN
             IF (leSousType = 'REVERSEMENTS') THEN
                 prv_apres_visa_reversement(borid);
             END IF;
    END IF;

END;


-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
PROCEDURE prv_apres_visa_reversement(borid INTEGER)
IS
  manid MANDAT.man_id%TYPE;

  CURSOR c_mandatVises IS
           SELECT man_id FROM MANDAT WHERE bor_id=borid AND man_etat='VISE';

BEGIN
     OPEN c_mandatVises;
        LOOP
            FETCH c_mandatVises INTO manid;
                  EXIT WHEN c_mandatVises%NOTFOUND;
            jefy_depense.apres_visa.viser_reversement(manid);
        END LOOP;
        CLOSE c_mandatVises;
END;



PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER)
IS

  tbotype  TYPE_BORDEREAU.tbo_type%TYPE;
  cpt INTEGER;
  lebordereau BORDEREAU%ROWTYPE;
  brjordre MANDAT.brj_ordre%TYPE;
  exeordre MANDAT.exe_ordre%TYPE;
  nbmandats INTEGER;
  nbtitres INTEGER;
  ex INTEGER;


  CURSOR c1 IS SELECT DISTINCT brj_ordre,exe_ordre FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
  CURSOR c2 IS SELECT DISTINCT brj_ordre,exe_ordre FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;

BEGIN
  SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

  SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;


  --raise_application_error (-20001, ''||ex|| '=' ||lebordereau.exe_ordre);


  IF lebordereau.exe_ordre=ex THEN

          -- mettre a jour le bordereau de jefy
          UPDATE jefy.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
            WHERE bor_ordre=lebordereau.bor_ordre;



            SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

            IF (nbmandats > 0) THEN
                -- mettre a jour les mandats VISE
                UPDATE jefy.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de mandats
                  OPEN C1;
                LOOP
                  FETCH C1 INTO brjordre,exeordre;
                  EXIT WHEN c1%NOTFOUND;
                  Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
                END LOOP;
                CLOSE C1;
            END IF;



            SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

            IF (nbtitres > 0) THEN
                -- mettre a jour les titres VISE
                UPDATE jefy.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de titres
                  OPEN C2;
                LOOP
                  FETCH C2 INTO brjordre,exeordre;
                  EXIT WHEN c2%NOTFOUND;
                  Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
                END LOOP;
                CLOSE C2;
            END IF;


            emarger_visa_bord_prelevement(borid);


  ELSE
        IF lebordereau.exe_ordre=2005 THEN

          -- mettre a jour le bordereau de jefy
          UPDATE jefy05.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
            WHERE bor_ordre=lebordereau.bor_ordre;



            SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

            IF (nbmandats > 0) THEN
                -- mettre a jour les mandats VISE
                UPDATE jefy05.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de mandats
                  OPEN C1;
                LOOP
                  FETCH C1 INTO brjordre,exeordre;
                  EXIT WHEN c1%NOTFOUND;
                  Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
                END LOOP;
                CLOSE C1;
            END IF;



            SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

            IF (nbtitres > 0) THEN
                -- mettre a jour les titres VISE
                UPDATE jefy05.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de titres
                  OPEN C2;
                LOOP
                  FETCH C2 INTO brjordre,exeordre;
                  EXIT WHEN c2%NOTFOUND;
                  Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
                END LOOP;
                CLOSE C2;
            END IF;
         ELSE
              RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
      END IF;
  END IF;

END ;



PROCEDURE apres_reimputation (reiordre INTEGER)
IS
    ex INTEGER;
    manid mandat.man_id%type;
    titid titre.tit_id%type;
    pconumnouveau plan_comptable.pco_num%type;
    pconumancien plan_comptable.pco_num%type;
BEGIN

     SELECT exe_ordre INTO ex FROM REIMPUTATION WHERE rei_ordre=reiordre;
     IF (ex < 2007) THEN
         prv_apres_reimputation_2006 (reiordre);
     ELSE          
        SELECT man_id, tit_id, pco_num_nouveau, pco_num_ancien INTO manid, titid, pconumnouveau, pconumancien FROM REIMPUTATION WHERE rei_ordre=reiordre;
        IF manid IS NOT NULL THEN
            update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;        
        end if;          
          
        IF titid IS NOT NULL THEN
            update jefy_recette.recette_ctrl_planco set pco_num=pconumnouveau where tit_id=titid and pco_num=pconumancien;        
        end if;          
          
     END IF;

END;

PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER)
IS
cpt INTEGER;
manid INTEGER;
titid INTEGER;
manordre INTEGER;
titordre INTEGER;
pconumnouveau INTEGER;
depordre INTEGER;
exeordre INTEGER;
ex INTEGER;
BEGIN

     SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

    -- mettre a jour le mandat ou le titre dans JEFY
    SELECT man_id, tit_id, pco_num_nouveau INTO manid, titid, pconumnouveau FROM REIMPUTATION WHERE rei_ordre=reiordre;

    IF manid IS NOT NULL THEN
       SELECT man_ordre, MANDAT.exe_ordre INTO manordre, exeordre FROM MANDAT, BORDEREAU b WHERE man_id = manid AND MANDAT.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTME','BTMS','BTPI'));

       IF exeordre=ex THEN
           UPDATE jefy.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
           UPDATE jefy.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
       ELSE IF exeordre=2005 THEN
               UPDATE jefy05.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
               UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
            END IF;
       END IF;
    END IF;


    IF titid IS NOT NULL THEN
       SELECT tit_ordre, TITRE.exe_ordre INTO titordre, exeordre FROM TITRE, BORDEREAU b WHERE tit_id = titid  AND TITRE.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTTE','BTPI'));


       IF exeordre=ex THEN
           UPDATE jefy.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
           -- pour les ORV, des fois que...
           SELECT dep_ordre INTO depordre FROM jefy.TITRE WHERE tit_ordre=titordre;
           IF (depordre IS NOT NULL ) THEN
                 UPDATE jefy.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
           END IF;

       ELSE IF exeordre=2005 THEN
           UPDATE jefy05.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
           -- pour les ORV, des fois que...
           SELECT dep_ordre INTO depordre FROM jefy05.TITRE WHERE tit_ordre=titordre;
           IF (depordre IS NOT NULL ) THEN
                 UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
           END IF;

            END IF;
       END IF;

    END IF;

END ;

PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
             --SELECT 1 INTO  cpt FROM dual;
         emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
         emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
     -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
             SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
         emarger_prelevement(recoordre);

END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS

CURSOR titres (lerecoordre INTEGER ) IS
 SELECT * FROM TITRE
 WHERE tit_id IN
 (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
 WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
 AND p.reco_ordre = lerecoordre)
 ;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lerecouvrement FROM RECOUVREMENT
 WHERE reco_ordre = recoordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lerecouvrement.exe_ordre,
  3,
  lerecouvrement.UTL_ORDRE,
  lerecouvrement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les titres du recrouvement
OPEN titres(recoordre);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  ecriture_debit.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_ordre = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/


