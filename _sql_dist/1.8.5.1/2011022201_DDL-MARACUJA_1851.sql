SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  MARACUJA/COMPTEFI
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.5.1
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Mise a jour/creation de certaines vues pour la sortie du compte financier
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.5.1',  null, '');
commit;


CREATE OR REPLACE FORCE VIEW maracuja.V_TITRE_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND tbo_ordre<>9 AND tbo_ordre<>11 AND tbo_ordre<>200 
  AND ed.pco_num NOT LIKE '185%' 
  and t.tit_id in (select tit_id from (select tit_id, count(*) from recette group by tit_id having count(*)=1))
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- titres co
SELECT x.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, x.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ecd_libelle, ecd_credit,
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, e.ecr_ordre,
      ecr_sacd, tde_origine, ecd_montant
 FROM BORDEREAU b,
      TITRE t,
      (select ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre, pco_num, tde_origine, 'Titre collectif' ecd_libelle, sum(ecd_montant) ecd_montant, sum(ecd_credit) ecd_credit 
        from TITRE_DETAIL_ECRITURE tde,
        ECRITURE_DETAIL ed,
        v_ecriture_infos ei
        where 
            tde.ecd_ordre = ed.ecd_ordre 
            and ed.ecd_ordre = ei.ecd_ordre
            and ecd_credit<>0
            AND ed.pco_num NOT LIKE '185%'
            AND tde_origine IN ('VISA', 'REIMPUTATION')
        group by ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre,pco_num, tde_origine) x,             
        ECRITURE e,             
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id 
  AND x.ecr_ordre = e.ecr_ordre
  and t.tit_id = x.tit_id
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = x.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tit_etat IN ('VISE', 'PAYE')
 -- AND ecd_credit<>0
  AND tbo_ordre<>9 AND tbo_ordre<>11 AND tbo_ordre<>200  
  and t.tit_id in (select tit_id from (select tit_id, count(*) from recette group by tit_id having count(*)>1))
  AND ABS (ecd_montant)=ABS(tit_ht)
union all
-- titres de recettes des PI
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_credit<>0 
  AND (tbo_ordre=11 or tbo_ordre=200) 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht) 
UNION ALL 
-- reductions de recettes 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie AS ecr_date, t.ges_code, ed.pco_num, t.bor_id, 
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_DEBIT, 
      (SIGN(ecd_debit))*ABS(t.tit_tva), ecd_debit+((SIGN(ecd_debit))*ABS(t.tit_tva)), 
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre, 
      ei.ecr_sacd, tde_origine, ecd_montant 
 FROM BORDEREAU b, 
      TITRE t, 
      TITRE_DETAIL_ECRITURE tde, 
      ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      V_JEFY_MULTIEX_TITRE jt 
WHERE t.bor_id = b.bor_id 
  AND t.tit_id = tde.tit_id 
  AND tde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND tde_origine IN ('VISA', 'REIMPUTATION') 
  AND tit_etat IN ('VISE', 'PAYE') 
  AND ecd_debit<>0 
  AND tbo_ordre=9 
  AND ed.pco_num NOT LIKE '185%' 
  AND ABS (ecd_montant)=ABS(tit_ht);
/



CREATE OR REPLACE FORCE VIEW comptefi.V_SITU_EXEBUDGREC0
(EXE_ORDRE, GES_CODE, TCD_SECT, SSDST_CODE, SUM_REC1, 
 SUM_REC2)
AS 
SELECT exe_ordre, org_ub, '1', r.dst_code, SUM(maracuja.En_Nombre(tit_mont)), 0
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'R' AND pco_num LIKE '7%'
GROUP BY exe_ordre, org_ub, '1', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '2', r.dst_code, 0, SUM(maracuja.En_Nombre(tit_mont))
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'R' AND pco_num LIKE '1%'
GROUP BY exe_ordre, org_ub, '2', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '1', r.dst_code, -SUM(maracuja.En_Nombre(tit_mont)), 0
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'D' AND pco_num LIKE '7%'
GROUP BY exe_ordre, org_ub, '1', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '2', r.dst_code, 0, -SUM(maracuja.En_Nombre(tit_mont))
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'D' AND pco_num LIKE '1%'
GROUP BY exe_ordre, org_ub, '2', r.dst_code;
/

CREATE OR REPLACE FORCE VIEW comptefi.V_SITU_EXEBUDGREC
(EXE_ORDRE, GES_CODE, DST_CODE, DST_LIB, SSDST_CODE, 
 SSDST_LIB, SUM_REC1, SUM_REC2)
AS 
select v.exe_ordre, v.ges_code, d0.DST_CODE, d0.dst_lib, SSDST_CODE, d1.dst_lib,
sum(SUM_REC1), sum(SUM_REC2)
from comptefi.v_situ_exebudgrec0 v, maracuja.v_jefy_multiex_destin_rec d1, maracuja.v_jefy_multiex_destin_rec d0
where v.ssdst_code = d1.dst_code and v.exe_ordre = d1.exe_exercice
and d0.exe_exercice = d1.exe_exercice
and d1.dst_code like d0.dst_code||'%' and d1.dst_niv = 1 and d0.dst_niv =0
group by v.exe_ordre, v.ges_code, d0.dst_code, d0.DST_LIB, ssdst_code, d1.DST_LIB;
/

CREATE OR REPLACE FORCE VIEW comptefi.V_SITU_EXEBUDGDEP0
(EXE_ORDRE, GES_CODE, TCD_CODE, SSDST_CODE, SUM_DEP10, 
 SUM_DEP20, SUM_DEP30)
AS 
SELECT s.exe_ordre, org_ub, tc.tcd_code, s.dst_code, SUM(dep_ttc) SUM_DEP10, 0 SUM_DEP20 , 0 SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o, maracuja.type_credit tc
WHERE s.org_ordre = o.org_id AND tc.tcd_code=s.tcd_code AND s.exe_ordre=tc.exe_ordre AND tc.tcd_sect=1 AND tc.tcd_code<>30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tc.tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tc.tcd_code, s.dst_code, 0 SUM_DEP10, SUM(dep_ttc) SUM_DEP20 , 0 SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o, maracuja.type_credit tc
WHERE s.org_ordre = o.org_id AND tc.tcd_code=s.tcd_code AND s.exe_ordre=tc.exe_ordre AND tc.tcd_sect=2 AND tc.tcd_code<>30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tc.tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tcd_code, s.dst_code, 0 SUM_DEP10, 0 SUM_DEP20, SUM(dep_ttc) SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o
WHERE s.org_ordre = o.org_id AND tcd_code = 30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tcd_code, s.dst_code;
/



CREATE OR REPLACE FORCE VIEW maracuja.V_ABRICOT_DEPENSE_A_MANDATER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 CLE_RIB, DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, 
 PERS_TYPE, PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, 
 ORG_UB, ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, 
 TCD_LIBELLE, DPP_NUMERO_FACTURE, DPP_ID, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, 
 DPP_NB_PIECE, UTL_ORDRE, TBO_ORDRE, DEP_ID, PCO_NUM, 
 DPCO_ID, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, ADR_CIVILITE, 
 ADR_NOM, ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, 
 ADR_CP, UTLNOMPRENOM)
AS 
select 
r.C_BANQUE,
r.C_GUICHET,
r.NO_COMPTE,
r.iban,
r.bic,
r.cle_rib,
bq.DOMICILIATION,
mp.MOD_libelle,
mp.MOD_CODE,
mp.MOD_DOM,
p.pers_type,
p.pers_libelle,
p.pers_lc,
e.EXE_EXERCICE,
eb.org_id,
o.ORG_UB,
o.ORG_CR,
o.ORG_SOUSCR,
eb.tcd_ordre,
tc.tcd_code,
tc.tcd_libelle,
dp.dpp_numero_facture,
dp.DPP_ID,
dp.DPP_HT_SAISIE,
dp.DPP_TVA_SAISIE,
dp.DPP_TTC_SAISIE,
dp.DPP_DATE_FACTURE,
dp.DPP_DATE_SAISIE,
dp.DPP_DATE_RECEPTION,
dp.DPP_DATE_SERVICE_FAIT,
dp.DPP_NB_PIECE,
dp.utl_ordre,
dcp.tbo_ordre,
dcp.dep_id dep_id,
dcp.pco_num,
dcp.DPCO_id  DPCO_id ,
dcp.DPCO_HT_SAISIE,
dcp.DPCO_TVA_SAISIE,
dcp.DPCO_TTC_SAISIE,
f.ADR_CIVILITE,
f.ADR_NOM,
f.ADR_PRENOM,
f.ADR_ADRESSE1,
f.ADR_ADRESSE2,
f.ADR_VILLE,
f.ADR_CP,
pu.pers_libelle||' '||pu.pers_lc utlNomPrenom
from
jefy_depense.engage_budget eb,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dp,
jefy_depense.depense_ctrl_planco dcp,
grhum.v_fournis_grhum f,
jefy_admin.EXERCICE e,
jefy_admin.utilisateur u,
grhum.personne p,
grhum.personne pu,
grhum.ribfour_ulr r,
grhum.banque bq,
maracuja.MODE_PAIEMENT mp,
jefy_admin.type_credit tc,
jefy_admin.organ o --,
--jefy_admin.type_credit tcd
where eb.eng_id = db.eng_id
and   db.dpp_id = dp.dpp_id
and   db.dep_id = dcp.dep_id
and   dp.fou_ordre = f.fou_ordre
and   dp.exe_ordre = e.exe_ordre
and   dp.utl_ordre = u.utl_ordre
and   p.pers_id = u.pers_id
and   dp.rib_ordre = r.rib_ordre(+)
and   r.C_BANQUE = bq.C_BANQUE(+)
and   r.C_GUICHET = bq.C_GUICHET(+)
and   mp.mod_ordre = dp.mod_ordre
and   eb.tcd_ordre = tc.tcd_ordre
and   o.org_id = eb.org_id
and u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
and   man_id is null
and tbo_ordre != 201;
/


CREATE OR REPLACE FORCE VIEW maracuja.V_TITRE_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT, GES_LIB, IMP_LIB, DEBITEUR, BOR_NUM)
AS 
SELECT tr."EXE_ORDRE",tr."ECR_DATE_SAISIE",tr."ECR_DATE",tr."GES_CODE",tr."PCO_NUM",tr."BOR_ID",tr."TBO_ORDRE",tr."TIT_ID",tr."TIT_NUM",tr."TIT_LIB",tr."TIT_MONT",tr."TIT_TVA",tr."TIT_TTC",tr."TIT_ETAT",tr."FOU_ORDRE",tr."REC_DEBITEUR",tr."REC_INTERNE",tr."ECR_ORDRE",tr."ECR_SACD",tr."TDE_ORIGINE",tr."ECD_MONTANT", o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, o2.ORG_UB||' '||o2.ORG_CR||' '||o2.ORG_souscr, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_organ_exer o2, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND rec_interne = o2.org_id AND tr.exe_ordre = o2.exe_ordre AND o2.org_niv >=2
AND tr.bor_id = b.bor_id
AND tr.rec_interne IS NOT NULL
UNION
SELECT tr."EXE_ORDRE",tr."ECR_DATE_SAISIE",tr."ECR_DATE",tr."GES_CODE",tr."PCO_NUM",tr."BOR_ID",tr."TBO_ORDRE",tr."TIT_ID",tr."TIT_NUM",tr."TIT_LIB",tr."TIT_MONT",tr."TIT_TVA",tr."TIT_TTC",tr."TIT_ETAT",tr."FOU_ORDRE",tr."REC_DEBITEUR",tr."REC_INTERNE",tr."ECR_ORDRE",tr."ECR_SACD",tr."TDE_ORIGINE",tr."ECD_MONTANT", o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, f.ADR_NOM||' '||f.ADR_PRENOM, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_fournisseur f, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.fou_ordre = f.fou_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NOT NULL AND tr.rec_interne IS NULL
UNION
SELECT tr."EXE_ORDRE",tr."ECR_DATE_SAISIE",tr."ECR_DATE",tr."GES_CODE",tr."PCO_NUM",tr."BOR_ID",tr."TBO_ORDRE",tr."TIT_ID",tr."TIT_NUM",tr."TIT_LIB",tr."TIT_MONT",tr."TIT_TVA",tr."TIT_TTC",tr."TIT_ETAT",tr."FOU_ORDRE",tr."REC_DEBITEUR",tr."REC_INTERNE",tr."ECR_ORDRE",tr."ECR_SACD",tr."TDE_ORIGINE",tr."ECD_MONTANT", o.ORG_LIB, api_planco.get_pco_libelle(tr.pco_num,tr.exe_ordre) PCO_LIBELLE, tr.rec_debiteur, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NULL AND tr.rec_debiteur IS NOT NULL AND LENGTH(tr.REC_DEBITEUR)<>0;
/

CREATE OR REPLACE FORCE VIEW comptefi.V_SITU_EXEBUDGDEP
(EXE_ORDRE, GES_CODE, DST_CODE, DST_LIB, SSDST_CODE, 
 SSDST_LIB, SUM_DEP10, SUM_DEP20, SUM_DEP30)
AS 
select v.exe_ordre, v.ges_code, d0.dst_code, d0.DST_LIB, ssdst_code, d1.DST_LIB,
sum(SUM_DEP10), sum(SUM_DEP20), sum(SUM_DEP30)
from comptefi.v_situ_exebudgdep0 v, maracuja.v_jefy_multiex_destin d1, maracuja.v_jefy_multiex_destin d0
where v.ssdst_code = d1.dst_code and v.exe_ordre = d1.exe_exercice
and d0.exe_exercice = d1.exe_exercice
and d1.dst_code like d0.dst_code||'%' and d1.dst_niv = 1 and d0.dst_niv =0
group by v.exe_ordre, v.ges_code, d0.dst_code, d0.DST_LIB, ssdst_code, d1.DST_LIB;
/

update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.5.1';       