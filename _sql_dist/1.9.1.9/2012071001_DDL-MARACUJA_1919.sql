SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.9
-- Date de publication : 10/07/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- correction bordereau_abricot
-- correction abricot_impression
-- correction bordereau_abricot_paf
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.1.8', 'MARACUJA' );
exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.1.9', null );
commit;


CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007
-- version du 01/10/2009 -- ajout de controles sur la generation des bd de PI

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- GES_CODE a prendre en compte en fonction du mandat.
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;
function getFournisNom (fouOrdre integer) return varchar;
function recup_tcdsect (manid integer) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);
procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer);


END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT"
as
   procedure creer_bordereau (abrid integer)
   is
      cpt            integer;
      abrgroupby     abricot_bord_selection.abr_group_by%type;
      monborid_dep   integer;
      monborid_rec   integer;
      flag           integer;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = monborid_dep;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = monborid_rec;

      tmpmandid      integer;
      tmptitid       integer;
      tboordre       integer;
   begin
-- est ce une selection vide ???
      select count (*)
      into   cpt
      from   abricot_bord_selection
      where  abr_id = abrid;

      if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

         -- verifier l etat de l exercice
         select count (*)
         into   flag
         from   jefy_admin.exercice
         where  exe_ordre = recup_exeordre (abrid) and exe_stat in ('O', 'R');

         if (flag = 0) then
            raise_application_error (-20001, 'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.');
         end if;

         -- recup du group by pour traiter les cursors
         abrgroupby := recup_groupby (abrid);

         if (abrgroupby = 'bordereau_1R1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1r1t (abrid, monborid_rec);
         end if;

         if (abrgroupby = 'bordereau_NR1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nr1t (abrid, monborid_rec);

-- controle RA
            select count (*)
            into   cpt
            from   titre
            where  ori_ordre is not null and bor_id = monborid_rec;

            if cpt != 0 then
               raise_application_error (-20001, 'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !');
            end if;
         end if;

         if (abrgroupby = 'bordereau_1D1M') then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1d1m (abrid, monborid_dep);
         end if;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
monborid_dep := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

monborid_rec := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

bordereau_1D1M(abrid,monborid_dep);
bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
         if (abrgroupby not in ('bordereau_1R1T', 'bordereau_NR1T', 'bordereau_1D1M', 'bordereau_1D1M1R1T')) then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nd1m (abrid, monborid_dep);
         end if;

         if (monborid_dep is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_dep);

            open lesmandats;

            loop
               fetch lesmandats
               into  tmpmandid;

               exit when lesmandats%notfound;
               get_depense_jefy_depense (tmpmandid);
            end loop;

            close lesmandats;

            controle_bordereau (monborid_dep);
         end if;

         if (monborid_rec is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_rec);

            open lestitres;

            loop
               fetch lestitres
               into  tmptitid;

               exit when lestitres%notfound;
               -- recup du brouillard
               get_recette_jefy_recette (tmptitid);
               set_titre_brouillard (tmptitid);
               get_recette_prelevements (tmptitid);
            end loop;

            close lestitres;

            controle_bordereau (monborid_rec);
         end if;

-- maj de l etat dans la selection
         if (monborid_dep is not null or monborid_rec is not null) then
            if monborid_rec is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_rec
                where abr_id = abrid;
            end if;

            if monborid_dep is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_dep
                where abr_id = abrid;

               select tbo_ordre
               into   tboordre
               from   bordereau
               where  bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
               if tboordre = 3 then
                  bordereau_abricot_paye.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
               if tboordre = 18 then
                  bordereau_abricot_paye.basculer_bouillard_paye_orv (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 19 then
                  bordereau_abricot_paye.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 22 then
                  bordereau_abricot_paf.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de PAF on retravaille le brouillard
               if tboordre = 20 then
                  bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de PAF on retravaille le brouillard
               if tboordre = 21 then
                  bordereau_abricot_paf.basculer_bouillard_paye_orv (monborid_dep);
               end if;
           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
            end if;
         end if;
      end if;
   end;

   procedure viser_bordereau_rejet (brjordre integer)
   is
      cpt              integer;
      flag             integer;
      manid            maracuja.mandat.man_id%type;
      titid            maracuja.titre.tit_id%type;
      tboordre         integer;
      reduction        integer;
      utlordre         integer;
      dpcoid           integer;
      recid            integer;
      depsuppression   varchar2 (20);
      rpcoid           integer;
      recsuppression   varchar2 (20);
      exeordre         integer;
      depid            integer;
      boridinitial     integer;

      cursor mandats
      is
         select man_id
         from   maracuja.mandat
         where  brj_ordre = brjordre;

      cursor depenses
      is
         select dep_ordre,
                dep_suppression
         from   maracuja.depense
         where  man_id = manid;

      cursor titres
      is
         select tit_id
         from   maracuja.titre
         where  brj_ordre = brjordre;

      cursor recettes
      is
         select rec_ordre,
                rec_suppression
         from   maracuja.recette
         where  tit_id = titid;

      deliq            integer;
   begin
      -- verifier si le bordereau est deja vise
      select count (*)
      into   flag
      from   bordereau_rejet
      where  brj_etat = 'VISE' and brj_ordre = brjordre;

      if (flag > 0) then
         raise_application_error (-20001, 'Ce bordereau a déjà été visé');
      end if;

      open mandats;

      loop
         fetch mandats
         into  manid;

         exit when mandats%notfound;

         select bor_id
         into   boridinitial
         from   mandat
         where  man_id = manid;

         -- memoriser le bor-id du mandat
         open depenses;

         loop
            fetch depenses
            into  dpcoid,
                  depsuppression;

            exit when depenses%notfound;
            -- casser le liens des mand_id dans depense_ctrl_planco
              -- supprimer le liens compteble <-> depense dans l inventaire
            jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, null);

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
            if depsuppression = 'OUI' and tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
               select utl_ordre
               into   utlordre
               from   jefy_depense.depense_budget
               where  dep_id in (select dep_id
                                 from   jefy_depense.depense_ctrl_planco
                                 where  dpco_id = dpcoid);

               select dep_id
               into   depid
               from   jefy_depense.depense_ctrl_planco
               where  dpco_id = dpcoid;

               -- si cest le rejet d'un bordereau de paye
               if (tboordre = 18) then
                  jefy_paye.paye_reversement.viser_rejet_reversement (depid);
               end if;

               jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
            end if;
         end loop;

         close depenses;
      end loop;

      close mandats;

      -- pour les bordereau de PAF, appeler la proc
      jefy_paf.paf_budget.viser_rejet_paf (boridinitial);
      jefy_paye.paye_budget.viser_rejet_papaye (boridinitial);

      open titres;

      loop
         fetch titres
         into  titid;

         exit when titres%notfound;

         open recettes;

         loop
            fetch recettes
            into  rpcoid,
                  recsuppression;

            exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
            select r.rec_id_reduction
            into   reduction
            from   jefy_recette.recette r, jefy_recette.recette_ctrl_planco rpco
            where  rpco.rpco_id = rpcoid and rpco.rec_id = r.rec_id;

            if reduction is not null then
               jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, null);
            else
               jefy_recette.api.upd_recette_ctrl_planco (rpcoid, null);
            end if;

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
            if recsuppression = 'OUI' and tboordre != 200 then
               select utl_ordre
               into   utlordre
               from   jefy_recette.recette_budget
               where  rec_id in (select rec_id
                                 from   jefy_recette.recette_ctrl_planco
                                 where  rpco_id = rpcoid);

               select rec_id
               into   recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = rpcoid;

               if reduction is not null then
                  jefy_recette.api.del_reduction (recid, utlordre);
               else
                  jefy_recette.api.del_recette (recid, utlordre);
               end if;
            end if;
         end loop;

         close recettes;
      end loop;

      close titres;

-- on passe le brjordre a VISE
      update bordereau_rejet
         set brj_etat = 'VISE'
       where brj_ordre = brjordre;
   end;

   function get_selection_id (info varchar)
      return integer
   is
      selection   integer;
   begin
      select maracuja.abricot_bord_selection_seq.nextval
      into   selection
      from   dual;

      return selection;
   end;

   function get_selection_borid (abrid integer)
      return integer
   is
      borid   integer;
   begin
      select distinct bor_id
      into            borid
      from            maracuja.abricot_bord_selection
      where           abr_id = abrid;

      return borid;
   end;

   procedure set_selection_id (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a06tboordre integer, a07abrgroupby varchar, a08gescode varchar)
   is
      chaine     varchar (32000);
      premier    integer;
      tmpdepid   integer;
      tmprecid   integer;
      cpt        integer;
   begin
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

      -- traitement de la chaine des depid
      if a02lesdepid is not null or length (a02lesdepid) > 0 then
         chaine := a02lesdepid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdepid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         tmpdepid,   --DEP_ID
                         null,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      -- traitement de la chaine des recid
      if a03lesrecid is not null or length (a03lesrecid) > 0 then
         chaine := a03lesrecid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmprecid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         null,   --DEP_ID
                         tmprecid,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      select count (*)
      into   cpt
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id in (select dep_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and man_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
      end if;

      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id in (select rec_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and tit_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
      end if;

      bordereau_abricot.creer_bordereau (a01abrid);
   end;

   procedure set_selection_intern (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boriddep   bordereau.bor_id%type;
      boridrec   bordereau.bor_id%type;
      flag       integer;
   begin
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

      -- les mandats
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 201, 'bordereau_1D1M', a08gescodemandat);
-- les titres
      set_selection_id (-a01abrid, null, a03lesrecid, a04utlordre, a05exeordre, 200, 'bordereau_1R1T', a09gescodetitre);

      -- verifier que les bordereaux crees sont coherents entre eux
      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || a01abrid);
      end if;

      select max (bor_id)
      into   boriddep
      from   abricot_bord_selection
      where  abr_id = a01abrid;

      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = -a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || -a01abrid);
      end if;

      select max (bor_id)
      into   boridrec
      from   abricot_bord_selection
      where  abr_id = -a01abrid;

      -- verifier qu'on a 1 prest_id par titre/mandat
      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct tit_id) nb
              from     titre
              where    bor_id = boridrec
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs titres concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct man_id) nb
              from     mandat
              where    bor_id = boriddep
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs mandats concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      ctrl_bordereaux_pi (boriddep, boridrec);
   end;

   procedure set_selection_paye (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boridtmp    integer;
      moisordre   integer;
   begin
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

      -- les mandats de papaye
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 3, 'bordereau_1D1M', a08gescodemandat);
      boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

   -- bascule du brouillard de papaye

   -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
   end;

-- creer bordereau (tbo_ordre) + numerotation
   function get_num_borid (tboordre integer, exeordre integer, gescode varchar, utlordre integer)
      return integer
   is
      cpt      integer;
      borid    integer;
      bornum   integer;
   begin
-- creation du bor_id --
      select bordereau_seq.nextval
      into   borid
      from   dual;

-- creation du bordereau --
      bornum := -1;

      insert into bordereau
                  (bor_date_visa,
                   bor_etat,
                   bor_id,
                   bor_num,
                   bor_ordre,
                   exe_ordre,
                   ges_code,
                   tbo_ordre,
                   utl_ordre,
                   utl_ordre_visa,
                   bor_date_creation
                  )
      values      (null,   --BOR_DATE_VISA,
                   'VALIDE',   --BOR_ETAT,
                   borid,   --BOR_ID,
                   bornum,   --BOR_NUM,
                   -borid,   --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   tboordre,   --TBO_ORDRE,
                   utlordre,   --UTL_ORDRE,
                   null,   --UTL_ORDRE_VISA
                   sysdate
                  );

      return borid;
   end;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   procedure bordereau_1r1t (abrid integer, monborid integer)
   is
      cpt          integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by r.pco_num, r.rec_id asc;
   begin
      open rec_tit;

      loop
         fetch rec_tit
         into  tmprecette;

         exit when rec_tit%notfound;
         cpt := set_titre_recette (tmprecette.rpco_id, monborid);
      end loop;

      close rec_tit;
   end;

   procedure bordereau_nr1t (abrid integer, monborid integer)
   is
      ht           number (12, 2);
      tva          number (12, 2);
      ttc          number (12, 2);
      pconumero    varchar (20);
      nbpieces     integer;
      cpt          integer;
      titidtemp    integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

-- curseur de regroupement
      cursor rec_tit_group_by
      is
         select   r.pco_num,
                  sum (r.rpco_ht_saisie),
                  sum (r.rpco_tva_saisie),
                  sum (r.rpco_ttc_saisie)
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         group by r.pco_num
         order by r.pco_num asc;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and r.pco_num = pconumero
         order by r.pco_num asc, r.rec_id;
   begin
      open rec_tit_group_by;

      loop
         fetch rec_tit_group_by
         into  pconumero,
               ht,
               tva,
               ttc;

         exit when rec_tit_group_by%notfound;
         titidtemp := 0;

         open rec_tit;

         loop
            fetch rec_tit
            into  tmprecette;

            exit when rec_tit%notfound;

            if titidtemp = 0 then
               titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
            else
               update jefy_recette.recette_ctrl_planco
                  set tit_id = titidtemp
                where rpco_id = tmprecette.rpco_id;
            end if;
         end loop;

         close rec_tit;

-- recup du nombre de pieces
-- TODO
         nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

         -- maj des montants du titre
         update titre
            set tit_ht = ht,
                tit_nb_piece = nbpieces,
                tit_ttc = ttc,
                tit_tva = tva,
                tit_libelle = 'TITRE COLLECTIF'
          where tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
      end loop;

      close rec_tit_group_by;
   end;

   procedure bordereau_nd1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;
      abrgroupby   abricot_bord_selection.abr_group_by%type;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
      cursor mand_dep_convra
      is
         select distinct d.*
         from            abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.engage_budget e, maracuja.v_convention_limitative c
         where           d.dpco_id = ab.dep_id and abr_id = abrid and db.dep_id = d.dep_id and e.eng_id = db.eng_id and e.org_id = c.org_id(+) and e.exe_ordre = c.exe_ordre(+) and c.org_id is not null and d.man_id is null and ab.abr_etat = 'ATTENTE'
         order by        d.pco_num asc, d.dep_id;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
   begin
      open mand_dep_convra;

      loop
         fetch mand_dep_convra
         into  tmpdepense;

         exit when mand_dep_convra%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod') then
         cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
      end if;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_fou_rib_pco_mod') then
         cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
      end if;
   end;

   procedure bordereau_1d1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dep_mand
      is
         select   d.*
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d
         where    d.dpco_id = ab.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by d.pco_num asc, d.dep_id;
   begin
      open dep_mand;

      loop
         fetch dep_mand
         into  tmpdepense;

         exit when dep_mand%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close dep_mand;
   end;

   procedure bordereau_1d1m1r1t (abrid integer, boridep integer, boridrec integer)
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      bordereau_1d1m (abrid, boridep);
      bordereau_1r1t (abrid, boridrec);
      ctrl_bordereaux_pi (boridep, boridrec);
   end;

-- les mandats et titres
   function set_mandat_depense (dpcoid integer, borid integer)
      return integer
   is
      cpt               integer;
      flag              integer;
      ladepense         jefy_depense.depense_ctrl_planco%rowtype;
      ladepensepapier   jefy_depense.depense_papier%rowtype;
      leengagebudget    jefy_depense.engage_budget%rowtype;
      gescode           gestion.ges_code%type;
      manid             mandat.man_id%type;
      manorgine_key     mandat.man_orgine_key%type;
      manorigine_lib    mandat.man_origine_lib%type;
      oriordre          mandat.ori_ordre%type;
      prestid           mandat.prest_id%type;
      torordre          mandat.tor_ordre%type;
      virordre          mandat.pai_ordre%type;
      mannumero         mandat.man_numero%type;
      -- montantapayer     mandat.man_ttc%type;
      montantbud        mandat.man_ht%type;
      montanttva        mandat.man_tva%type;
      ttc               mandat.man_ttc%type;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

      select *
      into   ladepense
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select distinct dpp.*
      into            ladepensepapier
      from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = dpcoid;

      select eb.*
      into   leengagebudget
      from   jefy_depense.engage_budget eb, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.eng_id = eb.eng_id and db.dep_id = dpco.dep_id and dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = leengagebudget.org_id and exe_ordre = leengagebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a l''engagement num. ' || leengagebudget.eng_numero || ' n''est pas ouverte sur ' || leengagebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      manorgine_key := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      manorigine_lib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (leengagebudget.org_id, leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

-- creation du man_id --
      select mandat_seq.nextval
      into   manid
      from   dual;

-- recup du numero de mandat
      mannumero := -1;
      montantbud := abricot_util.get_dpco_montant_budgetaire (ladepense.dpco_id);
      --montantapayer := abricot_util.get_dpco_montant_apayer (ladepense.dpco_id);
      ttc := abricot_util.get_dpco_montant_ttc (ladepense.dpco_id);
      montanttva := ttc - montantbud;

      insert into mandat
                  (bor_id,
                   brj_ordre,
                   exe_ordre,
                   fou_ordre,
                   ges_code,
                   man_date_remise,
                   man_date_visa_princ,
                   man_etat,
                   man_etat_remise,
                   man_ht,
                   man_id,
                   man_motif_rejet,
                   man_nb_piece,
                   man_numero,
                   man_numero_rejet,
                   man_ordre,
                   man_orgine_key,
                   man_origine_lib,
                   man_ttc,
                   man_tva,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tor_ordre,
                   pai_ordre,
                   org_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable
                  )
      values      (borid,   --BOR_ID,
                   null,   --BRJ_ORDRE,
                   ladepensepapier.exe_ordre,   --EXE_ORDRE,
                   ladepensepapier.fou_ordre,   --FOU_ORDRE,
                   gescode,   --GES_CODE,
                   null,   --MAN_DATE_REMISE,
                   null,
                   
                   --MAN_DATE_VISA_PRINC,
                   'ATTENTE',   --MAN_ETAT,
                   'ATTENTE',   --MAN_ETAT_REMISE,
                   montantbud,   --MAN_HT,
                   manid,   --MAN_ID,
                   null,   --MAN_MOTIF_REJET,
                   ladepensepapier.dpp_nb_piece,   --MAN_NB_PIECE,
                   mannumero,
                   --MAN_NUMERO,
                   null,   --MAN_NUMERO_REJET,
                   -manid,   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                   manorgine_key,   --MAN_ORGINE_KEY,
                   manorigine_lib,   --MAN_ORIGINE_LIB,
                   --ladepense.dpco_ttc_saisie,   --MAN_TTC,
                   ttc,   --man_ttc
                   --ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,   --MAN_TVA,
                   montanttva,   --man_tva
                   ladepensepapier.mod_ordre,   --MOD_ORDRE,
                   oriordre,   --ORI_ORDRE,
                   ladepense.pco_num,
                   --PCO_NUM,
                   prestid,   --PREST_ID,
                   torordre,   --TOR_ORDRE,
                   virordre,   --VIR_ORDRE
                   leengagebudget.org_id,
                   --org_ordre
                   ladepensepapier.rib_ordre,   --rib ordo
                   ladepensepapier.rib_ordre   -- rib_comptable
                  );

-- maj du man_id  dans la depense
      update jefy_depense.depense_ctrl_planco
         set man_id = manid
       where dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

      -- recup du brouillard
      set_mandat_brouillard (manid);
      return manid;
   end;

-- lesdepid XX$FF$....$DDD$ZZZ$$
   function set_mandat_depenses (lesdpcoid varchar, borid integer)
      return integer
   is
      cpt             integer;
      premier         integer;
      tmpdpcoid       integer;
      chaine          varchar (5000);
      premierdpcoid   integer;
      manid           integer;
      ttc             mandat.man_ttc%type;
      tva             mandat.man_tva%type;
      ht              mandat.man_ht%type;
      utlordre        integer;
      nb_pieces       integer;
   begin
      select count (*)
      into   cpt
      from   dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
      premierdpcoid := null;

      -- traitement de la chaine des depid xx$xx$xx$.....$x$$
      if lesdpcoid is not null or length (lesdpcoid) > 0 then
         chaine := lesdpcoid;

         loop
            premier := 1;

            -- On recupere le depid
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdpcoid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

-- creation du mandat lie au borid
            if premierdpcoid is null then
               manid := set_mandat_depense (tmpdpcoid, borid);

               -- suppression du brouillard car il est uniquement sur la premiere depense
               delete from mandat_brouillard
                     where man_id = manid;

               premierdpcoid := tmpdpcoid;
            else
               -- maj du man_id  dans la depense
               update jefy_depense.depense_ctrl_planco
                  set man_id = manid
                where dpco_id = tmpdpcoid;

               -- recup de la depense (maracuja)
               select distinct dpp.utl_ordre
               into            utlordre
               from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
               where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
            end if;

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
--      select sum (dpco_ttc_saisie),
--             sum (dpco_ttc_saisie - dpco_montant_budgetaire),
--             sum (dpco_montant_budgetaire)
--      into   ttc,
--             tva,
--             ht
--      from   jefy_depense.depense_ctrl_planco
--      where  man_id = manid;
      ttc := abricot_util.get_man_montant_ttc (manid);
      ht := abricot_util.get_man_montant_budgetaire (manid);
      tva := ttc - ht;

-- recup du nb de pieces
      select sum (dpp.dpp_nb_piece)
      into   nb_pieces
      from   jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and man_id = manid;

-- maj du mandat
      update mandat
         set man_ht = ht,
             man_tva = tva,
             man_ttc = ttc,
             man_nb_piece = nb_pieces
       where man_id = manid;

-- recup du brouillard
      set_mandat_brouillard (manid);
      return cpt;
   end;

   function set_titre_recette (rpcoid integer, borid integer)
      return integer
   is
--     jefytitre           jefy.titre%ROWTYPE;
      gescode             gestion.ges_code%type;
      titid               titre.tit_id%type;
      titorginekey        titre.tit_orgine_key%type;
      titoriginelib       titre.tit_origine_lib%type;
      oriordre            titre.ori_ordre%type;
      prestid             titre.prest_id%type;
      torordre            titre.tor_ordre%type;
      modordre            titre.mod_ordre%type;
      presid              integer;
      cpt                 integer;
      virordre            integer;
      flag                integer;
      recettepapier       jefy_recette.recette_papier%rowtype;
      recettebudget       jefy_recette.recette_budget%rowtype;
      facturebudget       jefy_recette.facture_budget%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
      select *
      into   recettectrlplanco
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id = rpcoid;

      select *
      into   recettebudget
      from   jefy_recette.recette_budget
      where  rec_id = recettectrlplanco.rec_id;

      select *
      into   facturebudget
      from   jefy_recette.facture_budget
      where  fac_id = recettebudget.fac_id;

      select *
      into   recettepapier
      from   jefy_recette.recette_papier
      where  rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = facturebudget.org_id and exe_ordre = facturebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a la recette num. ' || recettebudget.rec_numero || ' n''est pas ouverte sur ' || facturebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      titorginekey := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      titoriginelib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (facturebudget.org_id, facturebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

      select titre_seq.nextval
      into   titid
      from   dual;

      insert into titre
                  (bor_id,
                   bor_ordre,
                   brj_ordre,
                   exe_ordre,
                   ges_code,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tit_date_remise,
                   tit_date_visa_princ,
                   tit_etat,
                   tit_etat_remise,
                   tit_ht,
                   tit_id,
                   tit_motif_rejet,
                   tit_nb_piece,
                   tit_numero,
                   tit_numero_rejet,
                   tit_ordre,
                   tit_orgine_key,
                   tit_origine_lib,
                   tit_ttc,
                   tit_tva,
                   tor_ordre,
                   utl_ordre,
                   org_ordre,
                   fou_ordre,
                   mor_ordre,
                   pai_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable,
                   tit_libelle
                  )
      values      (borid,   --BOR_ID,
                   -borid,   --BOR_ORDRE,
                   null,   --BRJ_ORDRE,
                   recettepapier.exe_ordre,   --EXE_ORDRE,
                   gescode,
                   --GES_CODE,
                   null,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                   oriordre,   --ORI_ORDRE,
                   recettectrlplanco.pco_num,   --PCO_NUM,
                   prestid,
                   --PREST_ID,
                   sysdate,   --TIT_DATE_REMISE,
                   null,   --TIT_DATE_VISA_PRINC,
                   'ATTENTE',   --TIT_ETAT,
                   'ATTENTE',   --TIT_ETAT_REMISE,
                   recettectrlplanco.rpco_ht_saisie,   --TIT_HT,
                   titid,   --TIT_ID,
                   null,
                   --TIT_MOTIF_REJET,
                   recettepapier.rpp_nb_piece,   --TIT_NB_PIECE,
                   -1,
                   --TIT_NUMERO, numerotation en fin de transaction
                   null,   --TIT_NUMERO_REJET,
                   -titid,
                   --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                   titorginekey,   --TIT_ORGINE_KEY,
                   titoriginelib,   --TIT_ORIGINE_LIB,
                   recettectrlplanco.rpco_ttc_saisie,   --TIT_TTC,
                   recettectrlplanco.rpco_tva_saisie,   --TIT_TVA,
                   torordre,   --TOR_ORDRE,
                   recettepapier.utl_ordre,   --UTL_ORDRE
                   facturebudget.org_id,   --ORG_ORDRE,
                   recettepapier.fou_ordre,
                   -- FOU_ORDRE  --TOCHECK certains sont nuls...
                   facturebudget.mor_ordre,
                   --MOR_ORDRE
                   null,
                   -- VIR_ORDRE
                   recettepapier.rib_ordre,
                   recettepapier.rib_ordre,
                   recettebudget.rec_lib
                  );

-- maj du tit_id dans la recette
      update jefy_recette.recette_ctrl_planco
         set tit_id = titid
       where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
      return titid;
   end;

   function set_titre_recettes (lesrpcoid varchar, borid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      raise_application_error (-20001, 'OPERATION NON TRAITEE');
      return cpt;
   end;

   function ndep_mand_org_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_org_fou_rib_pco_mod
      is
         select   e.org_id,
                  dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e, jefy_admin.organ vo, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and e.eng_id = db.eng_id and vo.org_id = e.org_id and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, e.org_id, dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      cursor lesdpcoidsribnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_org_fou_rib_pco_mod;

      loop
         fetch ndep_mand_org_fou_rib_pco_mod
         into  orgid,
               fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_org_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsribnull;

            loop
               fetch lesdpcoidsribnull
               into  tmpdpcoid;

               exit when lesdpcoidsribnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsribnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_org_fou_rib_pco_mod;

      return cpt;
   end;

   function ndep_mand_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_fou_rib_pco_mod
      is
         select   dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      cursor lesdpcoidsnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_fou_rib_pco_mod;

      loop
         fetch ndep_mand_fou_rib_pco_mod
         into  fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsnull;

            loop
               fetch lesdpcoidsnull
               into  tmpdpcoid;

               exit when lesdpcoidsnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_fou_rib_pco_mod;

      return cpt;
   end;

-- procedures de verifications
   function selection_valide (abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- meme exercice

      -- si PI somme recette = somme depense

      -- recette_valides

      -- depense_valides
      return cpt;
   end;

   function recette_valide (recid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function depense_valide (depid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function verif_bordereau_selection (borid integer, abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

      -- verifier sum TTC recette selection = sum TTC titre du bord

      -- verifier sum TTC depense  = sum TTC mandat du bord

      -- verifier sum TTC recette  = sum TTC titre  du bord
      return cpt;
   end;

-- procedures de locks de transaction
   procedure lock_mandats
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure lock_titres
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure get_depense_jefy_depense (manid integer)
   is
      depid               depense.dep_id%type;
      jefydepensebudget   jefy_depense.depense_budget%rowtype;
      tmpdepensepapier    jefy_depense.depense_papier%rowtype;
      jefydepenseplanco   jefy_depense.depense_ctrl_planco%rowtype;
      lignebudgetaire     depense.dep_ligne_budgetaire%type;
      fouadresse          depense.dep_adresse%type;
      founom              depense.dep_fournisseur%type;
      lotordre            depense.dep_lot%type;
      marordre            depense.dep_marches%type;
      fouordre            depense.fou_ordre%type;
      gescode             depense.ges_code%type;
      modordre            depense.mod_ordre%type;
      cpt                 integer;
      tcdordre            type_credit.tcd_ordre%type;
      tcdcode             type_credit.tcd_code%type;
      ecd_ordre_ema       ecriture_detail.ecd_ordre%type;
      orgid               integer;
      montantbudgetaire   depense.dep_ht%type;
      montanttva          depense.dep_ht%type;
      montantttc          depense.dep_ht%type;
      montantapayer       depense.dep_ht%type;

      cursor depenses
      is
         select db.*
         from   jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  dpco.man_id = manid and db.dep_id = dpco.dep_id;
   begin
      open depenses;

      loop
         fetch depenses
         into  jefydepensebudget;

         exit when depenses%notfound;

         -- creation du depid --
         select depense_seq.nextval
         into   depid
         from   dual;

         -- creation de lignebudgetaire--
         select org_ub || ' ' || org_cr || ' ' || org_souscr
         into   lignebudgetaire
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         --recuperer le type de credit a partir de la commande
         select tcd_ordre
         into   tcdordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'
         select org_ub,
                org_id
         into   gescode,
                orgid
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         -- fouordre --
         select fou_ordre
         into   fouordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         -- founom --
         founom := getfournisnom (fouordre);

         -- fouadresse --
         select count (*)
         into   cpt
         from   v_depense_adresse
         where  dep_id = jefydepensebudget.dep_id;

         if (cpt = 0) then
            raise_application_error (-20001, 'Pas d''adresse de facturation pour le fournisseur ' || founom);
         end if;

         select substr (adresse, 1, 196)
         into   fouadresse
         from   (select *
                 from   v_depense_adresse
                 where  dep_id = jefydepensebudget.dep_id)
         where  rownum = 1;

         --AND eng_stat !='A'

         -- lotordre --
         select count (*)
         into   cpt
         from   jefy_marches.attribution
         where  att_ordre = (select att_ordre
                             from   jefy_depense.engage_ctrl_marche
                             where  eng_id = jefydepensebudget.eng_id);

         if cpt = 0 then
            lotordre := null;
         else
            select lot_ordre
            into   lotordre
            from   jefy_marches.attribution
            where  att_ordre = (select att_ordre
                                from   jefy_depense.engage_ctrl_marche
                                where  eng_id = jefydepensebudget.eng_id);
         end if;

         -- marordre --
         select count (*)
         into   cpt
         from   jefy_marches.lot
         where  lot_ordre = lotordre;

         if cpt = 0 then
            marordre := null;
         else
            select mar_ordre
            into   marordre
            from   jefy_marches.lot
            where  lot_ordre = lotordre;
         end if;

         --MOD_ORDRE --
         select mod_ordre
         into   modordre
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recuperer l'ecriture_detail pour emargements semi-auto
         select ecd_ordre
         into   ecd_ordre_ema
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         -- recup de la depense papier
         select *
         into   tmpdepensepapier
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recup des infos de depense_ctrl_planco
         select *
         into   jefydepenseplanco
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         montantbudgetaire := abricot_util.get_dpco_montant_budgetaire (jefydepenseplanco.dpco_id);
         montantapayer := abricot_util.get_dpco_montant_apayer (jefydepenseplanco.dpco_id);
         montantttc := abricot_util.get_dpco_montant_ttc (jefydepenseplanco.dpco_id);
         montanttva := montantttc - montantbudgetaire;

         -- creation de la depense --
         insert into depense
         values      (fouadresse,   --DEP_ADRESSE,
                      null,   --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,   --DEP_DATE_RECEPTION,
                      tmpdepensepapier.dpp_date_service_fait,   --DEP_DATE_SERVICE,
                      'VALIDE',   --DEP_ETAT,
                      founom,   --DEP_FOURNISSEUR,
                      montantbudgetaire,   --DEP_HT,
                      depense_seq.nextval,   --DEP_ID,
                      lignebudgetaire,   --DEP_LIGNE_BUDGETAIRE,
                      lotordre,   --DEP_LOT,
                      marordre,   --DEP_MARCHES,
                      montantapayer,   --DEP_MONTANT_DISQUETTE,
                      null,   -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                      substr (tmpdepensepapier.dpp_numero_facture, 1, 199),   --DEP_NUMERO,
                      jefydepenseplanco.dpco_id,   --DEP_ORDRE,
                      null,   --DEP_REJET,
                      tmpdepensepapier.rib_ordre,   --DEP_RIB,
                      'NON',   --DEP_SUPPRESSION,
                      montantttc,   --DEP_TTC,
                      montanttva,   -- DEP_TVA,
                      tmpdepensepapier.exe_ordre,   --EXE_ORDRE,
                      fouordre,   --FOU_ORDRE,
                      gescode,   --GES_CODE,
                      manid,   --MAN_ID,
                      jefydepenseplanco.man_id,   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                      modordre,
                      jefydepenseplanco.pco_num,   --PCO_ORDRE,
                      tmpdepensepapier.utl_ordre,   --UTL_ORDRE
                      orgid,   --org_ordre
                      tcdordre,
                      ecd_ordre_ema,   -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                      tmpdepensepapier.dpp_date_facture
                     );
      end loop;

      close depenses;
   end;

   procedure get_recette_jefy_recette (titid integer)
   is
      recettepapier          jefy_recette.recette_papier%rowtype;
      recettebudget          jefy_recette.recette_budget%rowtype;
      facturebudget          jefy_recette.facture_budget%rowtype;
      recettectrlplanco      jefy_recette.recette_ctrl_planco%rowtype;
      recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%rowtype;
      maracujatitre          maracuja.titre%rowtype;
      adrnom                 varchar2 (200);
      letyperecette          varchar2 (200);
      titinterne             varchar2 (200);
      lbud                   varchar2 (200);
      tboordre               integer;
      cpt                    integer;

      cursor c_recette
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
      open c_recette;

      loop
         fetch c_recette
         into  recettectrlplanco;

         exit when c_recette%notfound;

         select *
         into   recettebudget
         from   jefy_recette.recette_budget
         where  rec_id = recettectrlplanco.rec_id;

         select *
         into   facturebudget
         from   jefy_recette.facture_budget
         where  fac_id = recettebudget.fac_id;

         select *
         into   recettepapier
         from   jefy_recette.recette_papier
         where  rpp_id = recettebudget.rpp_id;

         select *
         into   maracujatitre
         from   maracuja.titre
         where  tit_id = titid;

         if (recettebudget.rec_id_reduction is null) then
            letyperecette := 'R';
         else
            letyperecette := 'T';
         end if;

         select count (*)
         into   cpt
         from   jefy_recette.pi_dep_rec
         where  rec_id = recettectrlplanco.rec_id;

         if cpt > 0 then
            titinterne := 'O';
         else
            titinterne := 'N';
         end if;

         adrnom := getfournisnom (recettepapier.fou_ordre);

         select org_ub || '/' || org_cr || '/' || org_souscr
         into   lbud
         from   jefy_admin.organ
         where  org_id = facturebudget.org_id;

         select distinct tbo_ordre
         into            tboordre
         from            maracuja.titre t, maracuja.bordereau b
         where           b.bor_id = t.bor_id and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
         if tboordre = 200 then
            tboordre := null;
         else
            tboordre := facturebudget.org_id;
         end if;

         insert into recette
         values      (recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      maracujatitre.ges_code,
                      --GES_CODE,
                      null,   --MOD_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM,
                      recettebudget.rec_date_saisie,
                      --jefytitre.tit_date,-- REC_DATE,
                      adrnom,   -- REC_DEBITEUR,
                      recette_seq.nextval,   -- REC_ID,
                      null,   -- REC_IMPUTTVA,
                      null,
                      -- REC_INTERNE, // TODO ROD
                      facturebudget.fac_lib,
                      -- REC_LIBELLE,
                      lbud,   -- REC_LIGNE_BUDGETAIRE,
                      'E',   -- REC_MONNAIE,
                      recettectrlplanco.rpco_ht_saisie,   --HT,
                      recettectrlplanco.rpco_ttc_saisie,   --TTC,
                      recettectrlplanco.rpco_ttc_saisie,
                      --DISQUETTE,
                      recettectrlplanco.rpco_tva_saisie,   --   REC_MONTTVA,
                      facturebudget.fac_numero,   --   REC_NUM,
                      recettectrlplanco.rpco_id,
                      --   REC_ORDRE,
                      recettepapier.rpp_nb_piece,   --   REC_PIECE,
                      facturebudget.fac_numero,
                      
                      --   REC_REF,
                      'VALIDE',   --   REC_STAT,
                      'NON',   --    REC_SUPPRESSION,  Modif Rod
                      letyperecette,   --     REC_TYPE,
                      null,   --     REC_VIREMENT,
                      titid,   --      TIT_ID,
                      -titid,
                      --      TIT_ORDRE,
                      recettebudget.utl_ordre,   --       UTL_ORDRE
                      facturebudget.org_id,
                      --       ORG_ORDRE --ajout rod
                      facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
                      null,   --mod_ordre
                      recettepapier.mor_ordre,
                      --mor_ordre
                      recettepapier.rib_ordre,
                      null
                     );
      end loop;

      close c_recette;
   end;

-- procedures du brouillard
   procedure set_mandat_brouillard (manid integer)
   is
      lemandat              mandat%rowtype;
      tboordre              type_bordereau.tbo_ordre%type;
      sens                  mandat_brouillard.mab_sens%type;
      tcdsect               jefy_admin.type_credit.tcd_sect%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
      modcode               mode_paiement.mod_code%type;
      modlibelle            mode_paiement.mod_libelle%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      tboordre := abricot_util.get_man_tboordre (manid);

      if lemandat.prest_id is null then
         -- creation du brouillard imputation budgetaire --
         sens := inverser_sens_orv (tboordre, 'D');
         montantbudgetaire := lemandat.man_ht;
         pconumbudgetaire := lemandat.pco_num;
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
         -- creation du brouillard de contrepartie
         sens := inverser_sens_orv (tboordre, 'C');

         -- si on est sur un ORV, on recupere le compte de ctp à utiliser
         if sens = 'D' then
            tcdsect := recup_tcdsect (manid);
            pconumctp := util.getpconumvalidefromparam (lemandat.exe_ordre, 'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section' || tcdsect || '.compte');

            if (pconumctp is null) then
               pconumctp := '4632';
            end if;
         else
            pconumctp := abricot_util.get_man_compte_ctp (manid);
            if (pconumctp is null) then
                raise_application_error (-20001, 'Le compte de contrepartie n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;
         end if;

         gescodectp := abricot_util.get_man_gestion_ctp (manid);
         montantctp := abricot_util.get_man_montant_apayer (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA CTP', sens, manid, pconumctp);
         --si presence de TVA, on gere la TVA deduite et l eventuelle TVA collectee
         montanttvadeduite := abricot_util.get_man_montant_tva_ded (manid, null);
         montanttvacollectee := abricot_util.get_man_montant_tva_coll (manid, null);

         if (montanttvadeduite <> 0) then
            sens := inverser_sens_orv (tboordre, 'D');
            pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);

            if (pconumtvadeduite is null) then
               raise_application_error (-20001, 'Le compte par defaut de TVA a deduire n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
         end if;

         if (montanttvacollectee <> 0) then
            sens := inverser_sens_orv (tboordre, 'C');
            pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);

            if (pconumtvacollectee is null) then
               select mod_code,
                      mod_libelle
               into   modcode,
                      modlibelle
               from   mode_paiement
               where  mod_ordre = lemandat.mod_ordre;

               raise_application_error (-20001, 'Le compte de TVA à collecter n''est pas parametré pour le mode de paiement (' || modcode || '-' || modlibelle || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
         end if;
      else
         bordereau_abricot.set_mandat_brouillard_intern (manid);
      end if;
   end;

   procedure set_mandat_brouillard_intern (manid integer)
   is
      lemandat              mandat%rowtype;
      chap                  varchar2 (2);
      sens                  mandat_brouillard.mab_sens%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- recup des 2 premiers caracteres du compte
      select substr (lemandat.pco_num, 1, 2)
      into   chap
      from   dual;

      if chap = '18' then
         raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num || ')');
      end if;

      --lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      sens := 'D';
      montantbudgetaire := lemandat.man_ht;
      pconumbudgetaire := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
      sens := 'C';
      gescodectp := abricot_util.get_man_gestion_ctp (manid);
      montantctp := abricot_util.get_man_montant_apayer (manid);
      --pconumctp := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, '181');

      if (montanttvadeduite <> 0) then
         sens := 'D';
         pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
      end if;

      if (montanttvacollectee <> 0) then
         sens := 'C';
         pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
      end if;
   end;

   procedure set_titre_brouillard (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      recid               integer;
      flag                integer;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
      select max (rb.rec_id_reduction)
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

-- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      if letitre.prest_id is null then
         open c_recettes;

         loop
            fetch c_recettes
            into  recettectrlplanco;

            exit when c_recettes%notfound;

            select max (rec_id)
            into   recid
            from   recette
            where  rec_ordre = recettectrlplanco.rpco_id;

            -- verifier que la contrepartie n'est pas passée sur l'imputation du titre
            select count (*)
            into   flag
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id and pco_num = letitre.pco_num;

            if (flag > 0) then
               raise_application_error (-20001, 'La contrepartie ne doit pas etre identique à l''imputation ( : ' || letitre.tit_libelle || ' / ' || letitre.pco_num || ' / ' || letitre.tit_ttc || ')');
            end if;

            -- creation du titre_brouillard visa --
            --  RECETTE_CTRL_PLANCO
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      letitre.ges_code,
                      --GES_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM
                      abs (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                      'VISA TITRE',
                      --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_tva
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,   --PCO_NUM
                      abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                      'VISA TVA',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_tva
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_ctp
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,
                      --PCO_NUM
                      abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                      'VISA CTP',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      inverser_sens (lesens),
                      --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_ctp
               where  rpco_id = recettectrlplanco.rpco_id;
         end loop;

         close c_recettes;
      else
         set_titre_brouillard_intern (titid);
      end if;

      -- suppression des lignes d ecritures a ZERO
      delete from titre_brouillard
            where tib_montant = 0;
   end;

   procedure set_titre_brouillard_intern (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      lepconum            maracuja.plan_comptable.pco_num%type;
      libelle             maracuja.plan_comptable.pco_libelle%type;
      chap                varchar2 (2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;
      ctpgescode          titre.ges_code%type;
      pconum_185          gestion_exercice.pco_num_185%type;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

      -- modif fred 04/2007
      select c.ges_code,
             ge.pco_num_185
      into   gescodecompta,
             pconum_185
      from   gestion g, comptabilite c, gestion_exercice ge
      where  g.ges_code = letitre.ges_code and g.com_ordre = c.com_ordre and g.ges_code = ge.ges_code and ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
      select rb.rec_id_reduction
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

      -- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      open c_recettes;

      loop
         fetch c_recettes
         into  recettectrlplanco;

         exit when c_recettes%notfound;

         select max (rec_id)
         into   recid
         from   recette
         where  rec_ordre = recettectrlplanco.rpco_id;

         -- recup des 2 premiers caracteres du compte
         select substr (recettectrlplanco.pco_num, 1, 2)
         into   chap
         from   dual;

         if chap != '18' then
            lepconum := api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
         else
            raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num || ')');
         end if;

         -- creation du titre_brouillard visa --
         --  RECETTE_CTRL_PLANCO
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   letitre.ges_code,
                   --GES_CODE,
                   lepconum,   --PCO_NUM
                   abs (recettectrlplanco.rpco_ht_saisie),
                   
                   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   lesens,
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = recettectrlplanco.rpco_id;

         -- recette_ctrl_planco_tva
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   exe_ordre,   --EXE_ORDRE,
                   gescodecompta,
                   -- ges_code,               --GES_CODE,
                   pco_num,   --PCO_NUM
                   abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_tva
            where  rpco_id = recettectrlplanco.rpco_id;

         -- si on est sur un sacd, la contrepartie reste sur le sacd
         if (pconum_185 is not null) then
            ctpgescode := letitre.ges_code;
         else
            ctpgescode := gescodecompta;
         end if;

         -- recette_ctrl_planco_ctp on force le 181
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   ctpgescode,   --GES_CODE,
                   '181',
                   --PCO_NUM
                   abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id;
      end loop;

      close c_recettes;
   end;

   -- outils
   function inverser_sens_orv (tboordre integer, sens varchar)
      return varchar
   is
      cpt   integer;
   begin
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
      select count (*)
      into   cpt
      from   type_bordereau
      where  tbo_sous_type = 'REVERSEMENTS' and tbo_ordre = tboordre;

      if (cpt != 0) then
         if (sens = 'C') then
            return 'D';
         else
            return 'C';
         end if;
      end if;

      return sens;
   end;

   function recup_gescode (abrid integer)
      return varchar
   is
      gescode   bordereau.ges_code%type;
   begin
      select distinct ges_code
      into            gescode
      from            abricot_bord_selection
      where           abr_id = abrid;

      return gescode;
   end;

   function recup_utlordre (abrid integer)
      return integer
   is
      utlordre   bordereau.utl_ordre%type;
   begin
      select distinct utl_ordre
      into            utlordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return utlordre;
   end;

   function recup_exeordre (abrid integer)
      return integer
   is
      exeordre   bordereau.exe_ordre%type;
   begin
      select distinct exe_ordre
      into            exeordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return exeordre;
   end;

   function recup_tboordre (abrid integer)
      return integer
   is
      tboordre   bordereau.tbo_ordre%type;
   begin
      select distinct tbo_ordre
      into            tboordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return tboordre;
   end;

   function recup_groupby (abrid integer)
      return varchar
   is
      abrgroupby   abricot_bord_selection.abr_group_by%type;
   begin
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

      return abrgroupby;
   end;

   function inverser_sens (sens varchar)
      return varchar
   is
   begin
      if sens = 'D' then
         return 'C';
      else
         return 'D';
      end if;
   end;

   procedure numeroter_bordereau (borid integer)
   is
      cpt_mandat   integer;
      cpt_titre    integer;
   begin
      select count (*)
      into   cpt_mandat
      from   mandat
      where  bor_id = borid;

      select count (*)
      into   cpt_titre
      from   titre
      where  bor_id = borid;

      if cpt_mandat + cpt_titre = 0 then
         raise_application_error (-20001, 'Bordereau  vide');
      else
         numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
         numerotationobject.numeroter_mandat (borid);
-- boucle titre
         numerotationobject.numeroter_titre (borid);
      end if;
   end;

   function traiter_orgid (orgid integer, exeordre integer)
      return integer
   is
      topordre     integer;
      cpt          integer;
      orilibelle   origine.ori_libelle%type;
      convordre    integer;
   begin
      if orgid is null then
         return null;
      end if;

      select count (*)
      into   cpt
      from   accords.convention_limitative
      where  org_id = orgid and exe_ordre = exeordre;

      if cpt > 0 then
         -- recup du type_origine CONVENTION--
         select top_ordre
         into   topordre
         from   type_operation
         where  top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

         select distinct con_ordre
         into            convordre
         from            accords.convention_limitative
         where           org_id = orgid and exe_ordre = exeordre;

         select (exe_ordre || '-' || lpad (con_index, 5, '0') || ' ' || con_objet)
         into   orilibelle
         from   accords.contrat
         where  con_ordre = convordre;
      else
         select count (*)
         into   cpt
         from   jefy_admin.organ
         where  org_id = orgid and org_lucrativite = 1;

         if cpt = 1 then
            -- recup du type_origine OPERATION LUCRATIVE --
            select top_ordre
            into   topordre
            from   type_operation
            where  top_libelle = 'OPERATION LUCRATIVE';

            --le libelle utilisateur pour le suivie en compta --
            select org_ub || '-' || org_cr || '-' || org_souscr
            into   orilibelle
            from   jefy_admin.organ
            where  org_id = orgid;
         else
            return null;
         end if;
      end if;

-- l origine est t elle deja  suivie --
      select count (*)
      into   cpt
      from   origine
      where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid;

      if cpt >= 1 then
         select ori_ordre
         into   cpt
         from   origine
         where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid and rownum = 1;
      else
         select origine_seq.nextval
         into   cpt
         from   dual;

         insert into origine
                     (ori_entite,
                      ori_key_name,
                      ori_libelle,
                      ori_ordre,
                      ori_key_entite,
                      top_ordre
                     )
         values      ('JEFY_ADMIN',
                      'ORG_ID',
                      orilibelle,
                      cpt,
                      orgid,
                      topordre
                     );
      end if;

      return cpt;
   end;

   procedure controle_bordereau (borid integer)
   is
      ttc             maracuja.titre.tit_ttc%type;
      detailttc       maracuja.titre.tit_ttc%type;
      ordottc         maracuja.titre.tit_ttc%type;
      debit           maracuja.titre.tit_ttc%type;
      credit          maracuja.titre.tit_ttc%type;
      cpt             integer;
      message         varchar2 (50);
      messagedetail   varchar2 (50);
   begin
      select count (*)
      into   cpt
      from   maracuja.titre
      where  bor_id = borid;

      if cpt = 0 then
-- somme des maracuja.titre
         select sum (man_ttc)
         into   ttc
         from   maracuja.mandat
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (d.dep_ttc)
         into   detailttc
         from   maracuja.mandat m, maracuja.depense d
         where  m.man_id = d.man_id and m.bor_id = borid;

-- la somme des credits
         select sum (mab_montant)
         into   credit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'C';

-- la somme des debits
         select sum (mab_montant)
         into   debit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'D';

-- somme des jefy.recette
         select sum (d.dpco_ttc_saisie)
         into   ordottc
         from   maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         where  m.man_id = d.man_id and m.bor_id = borid;

         message := ' mandats ';
         messagedetail := ' depenses ';
      else
-- somme des maracuja.titre
         select sum (tit_ttc)
         into   ttc
         from   maracuja.titre
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (r.rec_monttva + r.rec_mont)
         into   detailttc
         from   maracuja.titre t, maracuja.recette r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

-- la somme des credits
         select sum (tib_montant)
         into   credit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'C';

-- la somme des debits
         select sum (tib_montant)
         into   debit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'D';

-- somme des jefy.recette
         select sum (r.rpco_ttc_saisie)
         into   ordottc
         from   maracuja.titre t, jefy_recette.recette_ctrl_planco r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

         message := ' titres ';
         messagedetail := ' recettes ';
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  debit <> credit : ' || debit || ' ' || credit);
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  ecriture <> budgetaire : ' || debit || ' ' || ttc);
      end if;

-- somme des maracuja.titre = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (detailttc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant des ' || messagedetail || ' :' || ttc || ' ' || detailttc);
      end if;

-- somme des jefy.recette = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (ordottc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant ordonnateur des ' || messagedetail || ' :' || ttc || ' ' || ordottc);
      end if;

      bordereau_abricot.ctrl_date_exercice (borid);
   end;

   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_pi (boriddep integer, boridrec integer)
   is
      flag         integer;
      nbdep        integer;
      nbrec        integer;
      manid        mandat.man_id%type;
      titid        titre.tit_id%type;
      tmpmandat    mandat%rowtype;
      tmptitre     titre%rowtype;
      tmpprest     integer;
      montantdep   mandat.man_ht%type;
      montantrec   titre.tit_ht%type;

      cursor prests
      is
         select distinct prest_id
         from            (select prest_id
                          from   mandat
                          where  bor_id = boriddep
                          union
                          select prest_id
                          from   titre
                          where  bor_id = boridrec);
   begin
      if (boriddep is null) then
         raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.');
      end if;

      if (boridrec is null) then
         raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.');
      end if;

      -- verifier qu'il s'agit bien de bordereaux de PI
      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 201 and bor_id = boriddep;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.');
      end if;

      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 200 and bor_id = boridrec;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.');
      end if;

      -- comparer le nombre de titres et de mandats
      select count (*)
      into   nbdep
      from   mandat
      where  bor_id = boriddep;

      select count (*)
      into   nbrec
      from   titre
      where  bor_id = boridrec;

      if (nbdep = 0) then
         raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
      end if;

      if (nbdep <> nbrec) then
         raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbdep || ' / Titres : ' || nbrec);
      end if;

      open prests;

      loop
         fetch prests
         into  tmpprest;

         exit when prests%notfound;

         select count (*)
         into   nbdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select count (*)
         into   nbrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (nbdep <> nbrec) then
            raise_application_error (-20001, 'Incoherence : Nombre de titres (' || nbrec || ') different du nombre de mandats (' || nbdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;

         select sum (man_ht)
         into   montantdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select sum (tit_ht)
         into   montantrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (montantdep <> montantrec) then
            raise_application_error (-20001, 'Incoherence : Montant des titres (' || montantrec || ') different du montant des mandats  (' || montantdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;
      end loop;

      close prests;

      select sum (man_ht)
      into   montantdep
      from   mandat
      where  bor_id = boriddep;

      select sum (tit_ht)
      into   montantrec
      from   titre
      where  bor_id = boriddep;

      if (montantdep <> montantrec) then
         raise_application_error (-20001, 'Incoherence : Montant total des titres (' || montantrec || ') different du montant total des mandats  (' || montantdep || ')');
      end if;
   end;

   procedure get_recette_prelevements (titid integer)
   is
      cpt                      integer;
      facture_titre_data       prestation.facture_titre%rowtype;
--     client_data              prelev.client%ROWTYPE;
      oriordre                 integer;
      modordre                 integer;
      recid                    integer;
      echeid                   integer;
      echeancier_data          jefy_echeancier.echeancier%rowtype;
      echeancier_prelev_data   jefy_echeancier.echeancier_prelev%rowtype;
      facture_data             jefy_recette.facture_budget%rowtype;
      personne_data            grhum.v_personne%rowtype;
      premieredate             date;
   begin
-- verifier s il existe un echancier pour ce titre
      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

      if (cpt != 1) then
         return;
      end if;

-- recup du eche_id / ech_id
      select eche_id
      into   echeid
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

-- recup du des infos du prelevements
      select *
      into   echeancier_data
      from   jefy_echeancier.echeancier
      where  ech_id = echeid;

      select *
      into   echeancier_prelev_data
      from   jefy_echeancier.echeancier_prelev
      where  ech_id = echeid;

      select *
      into   facture_data
      from   jefy_recette.facture_budget
      where  eche_id = echeid;

      select *
      into   personne_data
      from   grhum.v_personne
      where  pers_id = facture_data.pers_id;

      select echd_date_prevue
      into   premieredate
      from   jefy_echeancier.echeancier_detail
      where  echd_numero = 1 and ech_id = echeid;

      select rec_id
      into   recid
      from   recette
      where  tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

      -- recup ??
      oriordre := gestionorigine.traiter_orgid (facture_data.org_id, facture_data.exe_ordre);

      insert into maracuja.echeancier
                  (eche_autoris_signee,
                   fou_ordre_client,
                   con_ordre,
                   eche_date_1ere_echeance,
                   eche_date_creation,
                   eche_date_modif,
                   eche_echeancier_ordre,
                   eche_etat_prelevement,
                   ft_ordre,
                   eche_libelle,
                   eche_montant,
                   eche_montant_en_lettres,
                   eche_nombre_echeances,
                   eche_numero_index,
                   org_ordre,
                   prest_ordre,
                   eche_prise_en_charge,
                   eche_ref_facture_externe,
                   eche_supprime,
                   exe_ordre,
                   tit_id,
                   rec_id,
                   tit_ordre,
                   ori_ordre,
                   pers_id,
                   org_id,
                   pers_description
                  )
      values      ('O',   --ECHE_AUTORIS_SIGNEE
                   facture_data.fou_ordre,   --FOU_ORDRE_CLIENT
                   null,
                   --echancier_data.CON_ORDRE  ,--CON_ORDRE
                   premieredate,
                   --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                   sysdate,
                   --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                   sysdate,   --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                   echeancier_data.ech_id,
                   
                   --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                   'V',
                   --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                   facture_data.fac_id,
                   --echancier_data.FT_ORDRE  ,--FT_ORDRE
                   echeancier_data.ech_libelle,
                   --echancier_data.LIBELLE,--ECHE_LIBELLE
                   echeancier_data.ech_montant,   --ECHE_MONTANT
                   echeancier_data.ech_montant_lettres,
                   --ECHE_MONTANT_EN_LETTRES
                   echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                   echeancier_data.ech_id,
                   --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                   facture_data.org_id,
                   --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                   null,
                   
                   --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                   'O',   --ECHE_PRISE_EN_CHARGE
                   facture_data.fac_lib,
                   
                   --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                   'N',   --ECHE_SUPPRIME
                   facture_data.exe_ordre,
                   --EXE_ORDRE
                   titid,
                   recid,   --REC_ID,
                   -titid,
                   oriordre,   --ORI_ORDRE,
                   personne_data.pers_id,
                   --CLIENT_data.pers_id  ,--PERS_ID
                   facture_data.org_id,   --orgid a faire plus tard....
                   personne_data.pers_libelle   --    PERS_DESCRIPTION
                  );

      insert into maracuja.prelevement
                  (eche_echeancier_ordre,
                   reco_ordre,
                   fou_ordre,
                   prel_commentaire,
                   prel_date_modif,
                   prel_date_prelevement,
                   prel_prelev_date_saisie,
                   prel_prelev_etat,
                   prel_numero_index,
                   prel_prelev_montant,
                   prel_prelev_ordre,
                   rib_ordre,
                   prel_etat_maracuja
                  )
         select ech_id,   --ECHE_ECHEANCIER_ORDRE
                null,   --PREL_FICP_ORDRE
                facture_data.fou_ordre,   --FOU_ORDRE
                echd_commentaire,
                --PREL_COMMENTAIRE
                sysdate,
                --DATE_MODIF,--PREL_DATE_MODIF
                echd_date_prevue,   --PREL_DATE_PRELEVEMENT
                sysdate,   --,--PREL_PRELEV_DATE_SAISIE
                'ATTENTE',   --PREL_PRELEV_ETAT
                echd_numero,
                --PREL_NUMERO_INDEX
                echd_montant,
                --PREL_PRELEV_MONTANT
                echd_id,   --PREL_PRELEV_ORDRE
                echeancier_prelev_data.rib_ordre_debiteur,
                
                --RIB_ORDRE
                'ATTENTE'   --PREL_ETAT_MARACUJA
         from   jefy_echeancier.echeancier_detail
         where  ech_id = echeancier_data.ech_id;
   end;

   procedure ctrl_date_exercice (borid integer)
   is
      exeordre   integer;
      annee      integer;
   begin
      select to_char (bor_date_creation, 'YYYY'),
             exe_ordre
      into   annee,
             exeordre
      from   bordereau
      where  bor_id = borid and exe_ordre >= 2007;

      if exeordre <> annee then
         update bordereau
            set bor_date_creation = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update mandat
            set man_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update titre
            set tit_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;
      end if;
   end;

-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.
   function get_ges_code_for_man_id (manid number)
      return comptabilite.ges_code%type
   is
      current_mandat          mandat%rowtype;
      pconumsacd              gestion_exercice.pco_num_185%type;
      visa_mode_paiement      planco_visa.pvi_contrepartie_gestion%type;
      visa_planco             planco_visa.pvi_contrepartie_gestion%type;
      code_agence_comptable   comptabilite.ges_code%type;
   begin
      select ges_code
      into   code_agence_comptable
      from   comptabilite;

      select *
      into   current_mandat
      from   mandat
      where  man_id = manid;

      -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
      select pco_num_185
      into   pconumsacd
      from   gestion_exercice
      where  exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

      if (pconumsacd is not null) then
         return current_mandat.ges_code;
      else   -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
         -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
         select mod_contrepartie_gestion
         into   visa_mode_paiement
         from   mode_paiement
         where  mod_ordre = current_mandat.mod_ordre;

         if (visa_mode_paiement is not null) then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         else   -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
            select pvi_contrepartie_gestion
            into   visa_planco
            from   planco_visa
            where  pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         end if;
      end if;
   end;

   function getfournisnom (fouordre integer)
      return varchar
   is
      founom   varchar2 (200);
   begin
      founom := '';

      select substr (nom || ' ' || prenom, 1, 200)
      into   founom
      from   v_fournis_light
      where  fou_ordre = fouordre;

      return founom;
   end;

   function recup_tcdsect (manid integer)
      return varchar
   is
      tcdsect   jefy_admin.type_credit.tcd_sect%type;
   begin
      select distinct tc.tcd_sect
      into   tcdsect
      from   jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.type_credit tc
      where  dpco.dep_id = db.dep_id and db.eng_id = eb.eng_id and eb.tcd_ordre = tc.tcd_ordre and man_id = manid;

      return tcdsect;
   end;
end;
/

CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_impressions
is
   function get_depense_cn (depid integer)
      return varchar2
   as
      ceordre            integer;
      lescodes           varchar2 (2000);
      lescodesdepenses   varchar2 (2000);

      cursor c1
      is
         select ce_ordre
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      lescodesdepenses := ' ';

      open c1;

      loop
         fetch c1
         into  ceordre;

         exit when c1%notfound;

         select cm.cm_code || ' ' || cm_lib
         into   lescodes
         --SELECT cm.cm_code INTO lescodes
         from   jefy_marches.code_exer ce, jefy_marches.code_marche cm
         where  ce.cm_ordre = cm.cm_ordre and ce.ce_ordre = ceordre;

         lescodesdepenses := lescodesdepenses || ' ' || lescodes;
      end loop;

      close c1;

      return lescodesdepenses;
   end;

   function get_depense_conv (depid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         select conv_reference
         from   jefy_depense.depense_ctrl_convention dcv, jefy_depense.v_convention c
         where  dcv.dep_id = depid and dcv.conv_ordre = c.conv_ordre;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_recette_conv (recid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select con_reference_externe
         from   jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  dcv.con_ordre = c.con_ordre and dcv.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_depense_actions (depid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);

      cursor c1
      is
         select lolf_code
         from   jefy_depense.depense_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c
         where  dca.dep_id = depid and dca.tyac_id = c.lolf_id and dca.exe_ordre = c.exe_ordre;
   begin
      leslolf := null;

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_recette_actions (recid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select distinct lolf_code
         from   jefy_recette.recette_ctrl_action rca, jefy_recette.v_lolf_nomenclature_recette l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.lolf_id = l.lolf_id and rca.rec_id = rpco.rec_id and rca.exe_ordre = l.exe_ordre and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      leslolf := null;

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_depense_inventaire (dpcoid integer)
      return varchar2
   as
      lesinv   varchar2 (4000);
      leinv    varchar2 (4000);

      cursor c1
      is
         select ci.clic_num_complet
         from   jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable ci
         where  ic.clic_id = ci.clic_id and dpco_id = dpcoid;
   begin
      lesinv := null;

      open c1;

      loop
         fetch c1
         into  leinv;

         exit when c1%notfound;

         if leinv is not null then
            leinv := leinv || ' ';
         end if;

         lesinv := lesinv || leinv;
      end loop;

      close c1;

      return lesinv;
   end;

   function get_depense_lbud (depid integer)
      return varchar2
   as
      lbudlib   varchar2 (2000);
   begin
      select o.org_ub || ' / ' || org_cr || ' / ' || org_souscr lbud
      into   lbudlib
      from   jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.organ o
      where  db.dep_id = depid and o.org_id = eb.org_id and eb.eng_id = db.eng_id;

      return lbudlib;
   end;

   function get_depense_infos (depid integer)
      return varchar2
   as
   begin
      return abricot_impressions.get_depense_cn (depid) || ' ' || abricot_impressions.get_depense_lbud (depid);
   end;

   function get_recette_analytiques (recid integer)
      return varchar2
   as
      lescodes   varchar2 (2000);
      coderef    varchar2 (500);

      cursor c1
      is
         select a.can_code
         from   jefy_recette.recette_ctrl_analytique rca, jefy_admin.code_analytique a, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.can_id = a.can_id and rca.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lescodes := null;

      open c1;

      loop
         fetch c1
         into  coderef;

         exit when c1%notfound;

         if lescodes is not null then
            lescodes := lescodes || ', ';
         end if;

         lescodes := lescodes || coderef;
      end loop;

      close c1;

      return lescodes;
   end;

   function get_depense_analytiques (depid integer)
      return varchar2
   as
      lescanal    varchar2 (2000);
      canalcode   varchar2 (500);

      cursor c1
      is
         select can_code
         from   jefy_depense.depense_ctrl_analytique dca, jefy_admin.v_code_analytique ca
         where  dca.dep_id = depid and dca.can_id = ca.can_id and dca.exe_ordre = ca.exe_ordre;
   begin
      lescanal := null;

      open c1;

      loop
         fetch c1
         into  canalcode;

         exit when c1%notfound;

         if lescanal is not null then
            lescanal := lescanal || ', ';
         end if;

         lescanal := lescanal || canalcode;
      end loop;

      close c1;

      return lescanal;
   end;
end abricot_impressions;
/


GRANT EXECUTE ON MARACUJA.ABRICOT_IMPRESSIONS TO JEFY_DEPENSE;

CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paf is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

function getMoisCompletTexte(borId integer) return varchar;

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(lemois varchar, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paf IS

function getMoisCompletTexte(borId integer) return varchar
-- renvoie le mois sous le forme JANVIER 2009 (a partir d un bordereau PAF)
is
    exeOrdre number;
    moisComplet varchar2(50);
    lemois varchar2(3);
    leMoisNum integer;
    lemoisTxt varchar2(20);
begin

    SELECT DISTINCT exe_ordre 
    INTO exeordre
    FROM maracuja.bordereau m
    WHERE m.bor_id = borid;    
    
    SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    --lemois := lpad(to_char(to_number(lemois)),2,'0' );
    leMoisNum := to_number(lemois);
    
    if leMoisNum = 1 then
       lemoisTxt := 'JANVIER'; 
    elsif leMoisNum = 2 then
       lemoisTxt := 'FEVRIER'; 
    elsif leMoisNum = 3 then
       lemoisTxt := 'MARS'; 
    elsif leMoisNum = 4 then
       lemoisTxt := 'AVRIL';     
    elsif leMoisNum = 5 then
       lemoisTxt := 'MAI';           
    elsif leMoisNum = 6 then
       lemoisTxt := 'JUIN';     
    elsif leMoisNum = 7 then
       lemoisTxt := 'JUILLET'; 
    elsif leMoisNum = 8 then
       lemoisTxt := 'AOUT'; 
    elsif leMoisNum = 9 then
       lemoisTxt := 'SEPTEMBRE'; 
    elsif leMoisNum = 10 then
       lemoisTxt := 'OCTOBRE'; 
    elsif leMoisNum = 11 then
       lemoisTxt := 'NOVEMBRE'; 
    elsif leMoisNum = 12 then
       lemoisTxt := 'DECEMBRE'; 
    end if;                                                     
     
    return lemoisTxt||' '||exeordre;    
end;

PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
lemois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

is_sacd gestion_exercice.pco_num_185%TYPE;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
--fredSELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
--fredWHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paf.paf_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois = lemois and exe_ordre = tmpBordereau.exe_ordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas prepare les ecritures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D'
 and exe_ordre = tmpBordereau.exe_ordre;

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C'
 and exe_ordre = tmpBordereau.exe_ordre;

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paf.set_bord_brouillard_visa(borid);

    select pco_num_185 into is_sacd from gestion_exercice where exe_ordre = tmpBordereau.exe_ordre and ges_code = tmpBordereau.ges_code;
    if (is_sacd is not null)
    then
       Bordereau_Abricot_Paf.set_bord_brouillard_sacd(borid);
    end if;

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, getMoisCompletTexte(borId),NULL);
  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paf.set_bord_brouillard_paiement(lemois, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -
  update jefy_paf.paf_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois=lemois and exe_ordre=tmpBordereau.exe_ordre and liq_ETAT='LIQUIDEE';

--fred UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
--fred    WHERE ges_code=tmpBordereau.ges_code
--fred      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';

 UPDATE jefy_paf.paf_etape SET bor_id=borid, pae_etat='MANDATEE', mois_libelle = getMoisCompletTexte(borid)
    WHERE ges_code=tmpBordereau.ges_code
        and  mois=lemois and exe_ordre=tmpBordereau.exe_ordre
        AND pae_ETAT='LIQUIDEE';



/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

select 1 into cpt from dual;

-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(lemois varchar, borid NUMBER, exeordre NUMBER)
IS


currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet VARCHAR2(50);
mois2 varchar2(50);

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paf.paf_ecritures WHERE mois = lemois AND ecr_type='45' and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
--fred lemois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
--fred SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
--fred WHERE mois_ordre  = moisordre;
SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

mois2 := getMoisCompletTexte(borid);


--SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
--WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||moiscomplet;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||mois2;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT PAF',
 currentecriture.pco_num,
 'PAIEMENT PAF '||mois2, -- 'PAIEMENT PAF '||moiscomplet,
 mois2, --moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ECRITURES WHERE mois = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ))
and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
moisordre INTEGER;
lemoiscomplet varchar2(50);
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

--fred moislibelle jefy_paf.paye_mois.mois_complet%TYPE;
lemois VARCHAR2(50);
mois2 varchar2(50);
BEGIN

    SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

    SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

    SELECT DISTINCT substr(dep_numero,5,3) INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT DISTINCT dep_numero INTO lemoiscomplet
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

    mois2 := getMoisCompletTexte(borid);

    OPEN ecriturescredit64(lemois, currentbordereau.ges_code);
    LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

         SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

         INSERT INTO BORDEREAU_BROUILLARD VALUES
         (
         bobordre,
         borid,
         currentbordereau.exe_ordre,
         currentecriture.ges_code,
         currentecriture.ecr_mont,
         currentecriture.ecr_sens,
         'VALIDE',
         'VISA PAF',
         currentecriture.pco_num,
         'VISA PAF '|| mois2, --'VISA PAF '||lemois,
         mois2, --lemoiscomplet,
         NULL
         );

    END LOOP;
    CLOSE ecriturescredit64;

END;


/******************************************
SET_MANDAT_ORV_BROUILLARD
******************************************/
PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

contre_partie_visa plan_comptable_exer.pco_num%TYPE;
ges_code_contrepartie comptabilite.ges_code%TYPE;

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN


-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

SELECT par_value INTO contre_partie_visa FROM PARAMETRE WHERE exe_ordre = lemandat.exe_ordre and par_key  = 'CONTRE PARTIE VISA';

if (contre_partie_visa = 'AGENCE')
then
    select ges_code into ges_code_contrepartie from maracuja.comptabilite where com_ordre = 1;
else
    ges_code_contrepartie := lemandat.ges_code;
end if;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,                            --ECD_ORDRE,
   lemandat.exe_ordre,              --EXE_ORDRE,
   lemandat.ges_code,               --GES_CODE,
   ABS(lemandat.man_ttc),           --MAB_MONTANT,
   'VISA MANDAT',                   --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,   --MAB_ORDRE,
   'C',                             --MAB_SENS,
   manid,                           --MAN_ID,
   classe6                          --PCO_NU
);

SELECT count(*) into cpt
FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

    SELECT COUNT(*) INTO cpt
    FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
    WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    IF (cpt = 1 )      -- Bulletins negatifs, on va chercher la contrepartie dans jefy_paf.paf_reversements
    THEN

      SELECT PCO_NUM_CONTREPARTIE INTO classe4
      FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
      WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    ELSE                -- OR Manuel

      classe4 := jefy_paf.get_contrepartie('REVERSEMENT', classe6, depid);
    
    END IF;

      -- creation du brouillard DEBITEUR CLASSE 4 !
      INSERT INTO MANDAT_BROUILLARD VALUES (
         NULL,                          --ECD_ORDRE,
         lemandat.exe_ordre,            --EXE_ORDRE,
         ges_code_contrepartie,             --GES_CODE,
         ABS(montant),                  --MAB_MONTANT,
         'VISA MANDAT',                 --MAB_OPERATION,
         mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
         'D',                           --MAB_SENS,
         manid,                         --MAN_ID,
         classe4                        --PCO_NU
      );

 END LOOP;
 CLOSE plancos;


END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
depid   INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;



BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- recup du dpcoid de ce mandat PAF : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)

select dep_id into depid from jefy_depense.depense_ctrl_planco where dpco_id = dpcoid;

select pco_num into classe6 from jefy_depense.depense_ctrl_planco where dpco_id = dpcoid;

select dep_ttc_saisie into montant from jefy_depense.depense_budget where dep_id = depid;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);

classe4 := jefy_paf.get_contrepartie('REGUL', classe6, depid );

-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);

END;




-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
/*
SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES PAF',
 currentecriture.pco_num,
 'RETENUES PAF '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;
*/
END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ecritures
WHERE mois = lemois AND ecr_type='18'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

mois VARCHAR2(50);
mois2 varchar2(50);

BEGIN

    mois2 := getMoisCompletTexte(borid);
    select count(*) into cpt from bordereau_brouillard where bob_operation = 'SACD PAF' and bob_libelle2 = mois2;

    if (cpt = 0)        -- On a pas encore passe les ecritures SACD
    then

        SELECT DISTINCT substr(dep_numero,5,3) INTO moisordre
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

        SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

        SELECT DISTINCT dep_numero INTO mois
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

         OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
         LOOP
         FETCH ecrituressacd INTO currentecriture;
         EXIT WHEN ecrituressacd%NOTFOUND;

             SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

             INSERT INTO BORDEREAU_BROUILLARD VALUES
             (
             bobordre,
             borid,
             currentbordereau.exe_ordre,
             currentecriture.ges_code,
             currentecriture.ecr_mont,
             currentecriture.ecr_sens,
             'VALIDE',
             'SACD PAF',
             currentecriture.pco_num,
             'SACD PAF '||mois2,
             mois2,
             NULL
             );

         END LOOP;
         CLOSE ecrituressacd;

    end if;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA PAF',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;


/*
PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;

*/
END;
/







create or replace procedure grhum.inst_patch_maracuja_1919 is
begin        
        
	    
	JEFY_ADMIN.PATCH_UTIL.end_PATCH ( 4, '1.9.1.9');       
end;
/












