CREATE OR REPLACE PACKAGE MARACUJA.extourne
is


   -- bordidn : bordereau sur l'exercice n
   -- bordidn1 : bordereau cree sur l'exercice n+1 par cette procedure
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type);
   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, utl_ordre jefy_admin.utilisateur.utl_ordre%type);

   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer;
      function is_exercice_existe (exeordre integer)
      return integer;
      
      function prv_creer_jdep_n1(depense_n maracuja.depense%rowtype) return jefy_depense.depense_budget.dep_id%type;
      
      function prv_chaine_planco_dep(depid jefy_depense.depense_budget.dep_id%type) return varchar2;
      
      function prv_chaine_action_dep(depid jefy_depense.depense_budget.dep_id%type) return varchar2;
      function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;
         function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;
      function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;
      function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;
      function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;
      
      procedure prv_creer_brouillard_inverse(manidn mandat.man_id%type, manidn1 mandat.man_id%type);
      
      function get_tboordre_mandat_ext return type_bordereau.tbo_ordre%type;
end extourne;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.extourne
is


   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type)
   is
      bordereau_n     bordereau%rowtype;
      mandat_n        mandat%rowtype;
      depense_n       depense%rowtype;
      flag            integer;
      depidn          jefy_depense.depense_budget.dep_id%type;
      depidn1         jefy_depense.depense_budget.dep_id%type;

      cursor c_mandat_n
      is
         select   m.*
         from     mandat m, mode_paiement mp
         where    m.mod_ordre = mp.mod_ordre and m.bor_id = bordereau_n.bor_id and mod_dom = 'EXTOURNE' and m.man_etat = 'VISE'
         order by ges_code, man_numero;

      cursor c_depense_n
      is
         select d.*
         from   depense d
         where  man_id = mandat_n.man_id;

      cursor c_depensectrlplanco_n1
      is
         select dpco.*
         from   jefy_depense.depense_ctrl_planco dpco
         where  dep_id = depidn1;

      chainedpcoids   varchar2 (30000);
      exeordren1      jefy_admin.exercice.exe_ordre%type;
      res             integer;
      manidn1         mandat.man_id%type;
   begin
      select *
      into   bordereau_n
      from   bordereau
      where  bor_id = boridn;

      exeordren1 := bordereau_n.exe_ordre + 1;

      -- verifier que le bordereau d'origine des mandats avec mode de paiement extourne
      if (is_bord_aextourner (boridn) = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner".');
      end if;

      -- verifier que le bordereau d'origine est visé
      if (bordereau_n.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau initial (bord_id=' || boridn || ' n''est pas vise.');
      end if;

      -- verifier qu'au moins un mandats a extourner du bordereau d'origine sont vises
      select count (*)
      into   flag
      from   mandat m, mode_paiement mp
      where  m.mod_ordre = mp.mod_ordre and bor_id = boridn and mod_dom = 'EXTOURNE' and m.man_etat = 'VISE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner" qui soitr VISE.');
      end if;

      -- verifier que l'exercice n+1 existe
      if (is_exercice_existe (exeordren1) = 0) then
         raise_application_error (-20001, 'L''exercice ' || (exeordren1) || ' n''existe pas, impossible de réserver des crédts d''extourne. Créez l''exercice en question ou attendez qu''il soit créé pour viser le bordereau initial.');
      end if;

      -- creer bordereau sur N+1
      boridn1 := bordereau_abricot.get_num_borid (get_tboordre_mandat_ext, exeordren1, bordereau_n.ges_code, bordereau_n.utl_ordre);

      -- créer chaque dépense négative sur n+1 correspondant à chaque dépense de n (intégrée dans un mandat d'extourne visé)
      open c_mandat_n;

      loop
         fetch c_mandat_n
         into  mandat_n;

         exit when c_mandat_n%notfound;
         chainedpcoids := '';

         open c_depense_n;

         loop
            fetch c_depense_n
            into  depense_n;

            exit when c_depense_n%notfound;
            -- creer la depense negative jefy_depense sur N+1
            depidn1 := prv_creer_jdep_n1 (depense_n);

            -- memoriser lien dep_idn/dep_idn1
            select dep_id
            into   depidn
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = depense_n.dep_ordre;

            insert into jefy_depense.extourne_liq
                        (el_id,
                         dep_id_n,
                         dep_id_n1
                        )
               select jefy_depense.extourne_liq_seq.nextval,
                      depidn,
                      depidn1
               from   dual;

            chainedpcoids := chainedpcoids || prv_chaine_dpcoids (depidn1) || '$';
         end loop;

         close c_depense_n;

         chainedpcoids := chainedpcoids || '$';
         -- creer mandat et brouillards sur N+1
         manidn1 := bordereau_abricot.set_mandat_depenses (chainedpcoids, boridn1);

         -- memoriser le lien man_idn/man_idn1
         insert into maracuja.extourne_mandat
                     (em_id,
                      man_id_n,
                      man_id_n1
                     )
            select maracuja.extourne_mandat_seq.nextval,
                   mandat_n.man_id,
                   manidn1
            from   dual;

         -- creer les depenses Maracuja sur N+1
         bordereau_abricot.get_depense_jefy_depense (manidn1);
         -- brouillard du mandat
         prv_creer_brouillard_inverse (mandat_n.man_id, manidn1);
      end loop;

      close c_mandat_n;

      bordereau_abricot.numeroter_bordereau (boridn1);
      bordereau_abricot.controle_bordereau (boridn1);
   end;

   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   mandat m, mode_paiement mp
      where  m.mod_ordre = mp.mod_ordre and bor_id = borid and mod_dom = 'EXTOURNE';

      return cpt;
   end;

   function is_exercice_existe (exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   jefy_admin.exercice
      where  exe_ordre = exeordre;

      return cpt;
   end;

   -- creation de la depense jefy_depense sur N+1 a partir de la depense maracuja sur N
   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type
   is
      a_dep_id                number;
      a_exe_ordre             number;
      a_dpp_id                number;
      a_dep_ht_saisie         number;
      a_dep_ttc_saisie        number;
      a_fou_ordre             number;
      a_org_id                number;
      a_tcd_ordre             number;
      a_tap_id                number;
      a_eng_libelle           varchar2 (500);
      a_tyap_id               number;
      a_utl_ordre             number;
      a_dep_id_reversement    number;
      a_chaine_action         varchar2 (30000);
      a_chaine_analytique     varchar2 (30000);
      a_chaine_convention     varchar2 (30000);
      a_chaine_hors_marche    varchar2 (30000);
      a_chaine_marche         varchar2 (30000);
      a_chaine_planco         varchar2 (30000);
      jdepense_ctrl_plancon   jefy_depense.depense_ctrl_planco%rowtype;
      jdepense_budgetn        jefy_depense.depense_budget%rowtype;
      jengage_budgetn         jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   jdepense_ctrl_plancon
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = depense_n.dep_ordre and man_id = depense_n.man_id;

      select *
      into   jdepense_budgetn
      from   jefy_depense.depense_budget
      where  dep_id = jdepense_ctrl_plancon.dep_id;

      select *
      into   jengage_budgetn
      from   jefy_depense.engage_budget
      where  eng_id = jdepense_budgetn.eng_id;

      a_dep_id := null;
      a_exe_ordre := jdepense_ctrl_plancon.exe_ordre + 1;
      a_dpp_id := null;
      a_dep_ht_saisie := -jdepense_ctrl_plancon.dpco_ht_saisie;
      a_dep_ttc_saisie := -jdepense_ctrl_plancon.dpco_ttc_saisie;
      a_fou_ordre := depense_n.fou_ordre;
      a_org_id := depense_n.org_ordre;
      a_tcd_ordre := depense_n.tcd_ordre;
      a_tap_id := jdepense_budgetn.tap_id;
      a_eng_libelle := substr ('Extourne : ' || jengage_budgetn.eng_libelle, 1, 500);
      a_tyap_id := jengage_budgetn.tyap_id;
      a_utl_ordre := jdepense_budgetn.utl_ordre;
      a_dep_id_reversement := null;
      -- on ne recupere pas les actions
      a_chaine_action := '';
      -- on ne recupere pas les codes nomenclature
      a_chaine_hors_marche := '';
      a_chaine_analytique := prv_chaine_analytique_dep (jdepense_budgetn.dep_id);
      a_chaine_convention := prv_chaine_convention_dep (jdepense_budgetn.dep_id);
      a_chaine_marche := prv_chaine_marche_dep (jdepense_budgetn.dep_id);
      a_chaine_planco := prv_chaine_planco_dep (jdepense_budgetn.dep_id);
      jefy_depense.liquider.ins_depense_directe (a_dep_id,
                                                 a_exe_ordre,
                                                 a_dpp_id,
                                                 a_dep_ht_saisie,
                                                 a_dep_ttc_saisie,
                                                 a_fou_ordre,
                                                 a_org_id,
                                                 a_tcd_ordre,
                                                 a_tap_id,
                                                 a_eng_libelle,
                                                 a_tyap_id,
                                                 a_utl_ordre,
                                                 a_dep_id_reversement,
                                                 a_chaine_action,
                                                 a_chaine_analytique,
                                                 a_chaine_convention,
                                                 a_chaine_hors_marche,
                                                 a_chaine_marche,
                                                 a_chaine_planco
                                                );
      return a_dep_id;
   end;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine               varchar2 (30000);
      chaine_inventaires   varchar2 (30000);
      repart               jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.pco_num || '$' || repart.dpco_ht_saisie || '$' || repart.dpco_ttc_saisie || '$' || repart.ecd_ordre || '$';
         -- FIXME recuperer les inventaires ????
         chaine_inventaires := '||';
         chaine := chaine || chaine_inventaires || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_action%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_action
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.tyac_id || '$' || repart.dact_ht_saisie || '$' || repart.dact_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_analytique%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_analytique
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.can_id || '$' || repart.dana_ht_saisie || '$' || repart.dana_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_convention%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_convention
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.conv_ordre || '$' || repart.dcon_ht_saisie || '$' || repart.dcon_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_hors_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.typa_id || '$' || repart.ce_ordre || '$' || repart.dhom_ht_saisie || '$' || repart.dhom_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.att_ordre || '$' || repart.dmar_ht_saisie || '$' || repart.dmar_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.dpco_id || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   -- creer un brouillard inverse a celui d'origine
   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type)
   is
   begin
      -- inverser le sens et l'affecter au mandat d'extourne
      delete from mandat_brouillard
            where man_id = manidn1;

      insert into maracuja.mandat_brouillard
                  (ecr_ordre,
                   exe_ordre,
                   ges_code,
                   mab_montant,
                   mab_operation,
                   mab_ordre,
                   mab_sens,
                   man_id,
                   pco_num
                  )
         select null,
                exe_ordre + 1,
                ges_code,
                mab_montant,
                mab_operation,
                mandat_brouillard_seq.nextval,
                decode (mab_sens, 'C', 'D', 'C'),
                manidn1,
                pco_num
         from   mandat_brouillard
         where  man_id = manidn;
   end;
   
   
   -- viser automatiquement le bordereau en entier
   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, utl_ordre jefy_admin.utilisateur.utl_ordre%type) is
   begin
   
   
   
   
   	--	leBordereau.setBorEtat(EOBordereau.BordereauVise);
	--	leBordereau.setBorDateVisa(Factory.getNow());
	--	leBordereau.setUtilisateurVisaRelationship(utilisateur);
      -- 
      --creer ecriture d'apres les brouillards
                
        
        
        
      -- creer mandat_ecriture_detail
        
        --	this.creerLesEcrituresDuMandat(ed, ((EOMandat) lesMandatsAcceptes.objectAtIndex(i)), utilisateur);
		--	// passage a l etat VISE
		--	((EOMandat) lesMandatsAcceptes.objectAtIndex(i)).setManEtat(EOMandat.mandatVise);
		--	((EOMandat) lesMandatsAcceptes.objectAtIndex(i)).setManDateRemise(dateRemiseVisa);
        
        
        
    return;
   end;
   
   
   
   function get_tboordre_mandat_ext return type_bordereau.tbo_ordre%type is 
   begin
    return 50;
   end;
   
   
   procedure prv_creer_ecritures is
   begin
        return;
   end;
   
   
   
   
end extourne;
/


