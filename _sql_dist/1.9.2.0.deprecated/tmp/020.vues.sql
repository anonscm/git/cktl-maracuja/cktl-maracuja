
create or replace force view maracuja.utilisateur_fonct (uf_ordre, utl_ordre, fon_ordre)
as
   select uf.uf_ordre,
          uf.utl_ordre,
          uf.fon_ordre
   from   jefy_admin.utilisateur_fonct uf, fonction f
   where  f.fon_ordre = uf.fon_ordre;
/



create or replace force view maracuja.preference (pref_id, pref_key, pref_default_value, pref_description, pref_personnalisable)
as
   select pref_id,
          pref_key,
          pref_default_value,
          pref_description,
          pref_personnalisable
   from   jefy_admin.preference;
/

