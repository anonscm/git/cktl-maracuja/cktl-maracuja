

CREATE OR REPLACE PACKAGE BODY MARACUJA.api_bordereau
is
   -- a appeler apres le visa d'un bordereau dans Maracuja
   procedure apres_visa (borid bordereau.bor_id%type)
   is
      lebordereau          bordereau%rowtype;
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
      boridn1              integer;
   begin
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      select tbo_sous_type
      into   lesoustype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      prv_numeroter_ecritures (borid);
      prv_traiter_bord_rejet (lebordereau);

      if (is_bord_mandats (borid) > 0) then
         if (lesoustype = 'REVERSEMENTS') then
            prv_apres_visa_reversement (borid);
         end if;

         -- si on est sur un bordereau de mandats a extourner, on cree les mandats d'extourne et on les vise
         if (extourne.is_bord_aextourner (borid) > 0) then
            extourne.creer_bordereau (borid, boridn1);
            extourne.viser_bord (boridn1, lebordereau.bor_date_visa, lebordereau.utl_ordre_visa);
         end if;
      end if;

      if (is_bord_titres (borid) > 0) then
         emarger_visa_bord_prelevement (borid);
      end if;
   end;

   -- permet de numeroter un bordereau de rejet s'il existe et de le viser en auto si le parametre de visa auto est a OUI
   procedure prv_traiter_bord_rejet (lebordereau bordereau%rowtype)
   is
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
   begin
      if lebordereau.bor_etat = 'VISE' then
         if (is_bord_mandats (lebordereau.bor_id) > 0) then
            select count (*)
            into   flag
            from   maracuja.mandat
            where  bor_id = lebordereau.bor_id and brj_ordre is not null;

            if (flag > 0) then
               select min (brj_ordre)
               into   brjordre
               from   maracuja.mandat
               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

               numerotationobject.numeroter_bordereaurejet (brjordre);
               autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_DEP', lebordereau.exe_ordre, 'NON');

               if (autoviserbordrejet = 'OUI') then
                  bordereau_abricot.viser_bordereau_rejet (brjordre);
               end if;
            end if;
         end if;

         if (is_bord_titres (lebordereau.bor_id) > 0) then
            select count (*)
            into   flag
            from   maracuja.titre
            where  bor_id = lebordereau.bor_id and brj_ordre is not null;

            if (flag > 0) then
               select min (brj_ordre)
               into   brjordre
               from   maracuja.titre
               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

               numerotationobject.numeroter_bordereaurejet (brjordre);
               autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_REC', lebordereau.exe_ordre, 'NON');

               if (autoviserbordrejet = 'OUI') then
                  bordereau_abricot.viser_bordereau_rejet (brjordre);
               end if;
            end if;
         end if;
      end if;
   end;

   procedure prv_apres_visa_reversement (borid bordereau.bor_id%type)
   is
      manid   mandat.man_id%type;

      cursor c_mandatvises
      is
         select man_id
         from   mandat
         where  bor_id = borid and man_etat = 'VISE';
   begin
      open c_mandatvises;

      loop
         fetch c_mandatvises
         into  manid;

         exit when c_mandatvises%notfound;
         jefy_depense.apres_visa.viser_reversement (manid);
      end loop;

      close c_mandatvises;
   end;

   procedure emarger_visa_bord_prelevement (borid bordereau.bor_id%type)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- numerote les ecritures non numerotees associees a un bordereau (de mandat ou de titre)
   procedure prv_numeroter_ecritures (borid bordereau.bor_id%type)
   is
      ecrordre     ecriture.ecr_ordre%type;
      ecrlibelle   ecriture.ecr_libelle%type;

      cursor lesecritures (leborid bordereau.bor_id%type)
      is
         select distinct e.ecr_ordre as ecr_ordre,
                         e.ecr_libelle as ecr_libelle
         from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                         inner join mandat_detail_ecriture mde on (ecd.ecd_ordre = mde.ecd_ordre)
                         inner join mandat m on (m.man_id = mde.man_id)
         where           m.bor_id = leborid and ecr_numero <= 0
         union all
         select distinct e.ecr_ordre as ecr_ordre,
                         e.ecr_libelle as ecr_libelle
         from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                         inner join titre_detail_ecriture tde on (ecd.ecd_ordre = tde.ecd_ordre)
                         inner join titre m on (m.tit_id = tde.tit_id)
         where           m.bor_id = leborid and ecr_numero <= 0
         order by        ecr_libelle;
   begin
      open lesecritures (borid);

      loop
         fetch lesecritures
         into  ecrordre,
               ecrlibelle;

         numerotationobject.numeroter_ecriture (ecrordre);
         exit when lesecritures%notfound;
      end loop;

      close lesecritures;
   end;

   function is_bord_titres (borid bordereau.bor_id%type)
      return integer
   as
      flag   integer;
   begin
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      return flag;
   end;

   function is_bord_mandats (borid bordereau.bor_id%type)
      return integer
   as
      flag   integer;
   begin
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      return flag;
   end;
end;
/


