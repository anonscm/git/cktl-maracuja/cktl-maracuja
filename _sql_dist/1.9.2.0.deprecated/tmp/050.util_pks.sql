CREATE OR REPLACE PACKAGE MARACUJA.util
is
   procedure annuler_visa_bor_mandat (borid integer);

   procedure annuler_visa_bor_titre (borid integer);

   procedure supprimer_visa_btme (borid integer);

   procedure supprimer_visa_btms (borid integer);

   procedure supprimer_visa_btte (borid integer);

   procedure creer_ecriture_annulation (ecrordre integer);

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer;

   procedure annuler_emargement (emaordre integer);

   procedure supprimer_bordereau_dep (borid integer);

   procedure supprimer_bordereau_rec (borid integer);

   procedure supprimer_bordereau_btms (borid integer);

   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer);

   procedure supprimer_paiement_virement (paiordre integer);

   procedure corriger_etat_mandats;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);
end;
/
