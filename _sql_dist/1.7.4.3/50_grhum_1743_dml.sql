SET DEFINE OFF;

begin 
    -- mise a jour des comptes sur les depenses a partir des comptes de mandat    
    update jefy_depense.depense_ctrl_planco d 
    set pco_num = (select pco_num from maracuja. mandat m where m.man_id=d.man_id)
    where dpco_id in (select dpco_id from jefy_depense.depense_ctrl_planco d1, maracuja.mandat m1 where d1.man_id=m1.man_id and m1.exe_ordre=2008 and d1.pco_num<>m1.pco_num)  ;
    
    -- mettre a jour l'inventaire a partir des depenses
    jefy_inventaire.inventaire_numero.controle_pcoliq_pcoinv;

    -- mise a jour des comptes sur les recettes a partir des comptes de titre    
    update jefy_recette.recette_ctrl_planco d 
    set pco_num = (select pco_num from maracuja.titre m where m.tit_id=d.tit_id)
    where rpco_id in (select rpco_id from jefy_recette.recette_ctrl_planco d1, maracuja.titre m1 where d1.tit_id=m1.tit_id and m1.exe_ordre=2008 and d1.pco_num<>m1.pco_num)  ;
    
   INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.7.4.3',  SYSDATE, '');


    commit;

END;
/


 
