begin
    
    -- Corrections pour les echeanciers

    update maracuja.prelevement set PREL_ETAT_MARACUJA='ATTENTE' where PREL_ETAT_MARACUJA='INVALIDE' 
    and ECHE_ECHEANCIER_ORDRE in (
    select eb.ECHE_ECHEANCIER_ORDRE
    from maracuja.echeancier_brouillard eb,
    maracuja.bordereau_brouillard bb,
    maracuja.ecriture_detail ecd
    where eb.bob_ordre=bb.bob_ordre
    and bb.BOB_LIBELLE1=ecd.ecd_libelle
    and bb.pco_num=ecd.pco_num
    and ecd.ecd_sens='D'
    and ecd.ecd_ordre not in (select ecd_ordre from maracuja.echeancier_detail_ecr)
    ) ;
    
    update maracuja.echeancier set ECHE_ETAT_PRELEVEMENT='V' where ECHE_ETAT_PRELEVEMENT='I' 
    and ECHE_ECHEANCIER_ORDRE in (
    select eb.ECHE_ECHEANCIER_ORDRE
    from maracuja.echeancier_brouillard eb,
    maracuja.bordereau_brouillard bb,
    maracuja.ecriture_detail ecd
    where eb.bob_ordre=bb.bob_ordre
    and bb.BOB_LIBELLE1=ecd.ecd_libelle
    and bb.pco_num=ecd.pco_num
    and ecd.ecd_sens='D'
    and ecd.ecd_ordre not in (select ecd_ordre from maracuja.echeancier_detail_ecr)
    ) ;    
    
    INSERT INTO MARACUJA.ECHEANCIER_DETAIL_ECR (
       EDE_ID, ECHE_ECHEANCIER_ORDRE, ECD_ORDRE)    
    select MARACUJA.ECHEANCIER_DETAIL_ECR_seq.nextval, eb.ECHE_ECHEANCIER_ORDRE, ecd.ECD_ORDRE 
    from maracuja.echeancier_brouillard eb,
    maracuja.bordereau_brouillard bb,
    maracuja.ecriture_detail ecd
    where eb.bob_ordre=bb.bob_ordre
    and bb.BOB_LIBELLE1=ecd.ecd_libelle
    and bb.pco_num=ecd.pco_num
    and ecd.ecd_sens='D'
    and ecd.ecd_ordre not in (select ecd_ordre from maracuja.echeancier_detail_ecr);    
    
    
    
	
	
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.7.7.0',  SYSDATE, '');
    
    commit;

end; 