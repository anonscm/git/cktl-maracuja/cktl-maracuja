-- creation des tables pour la recup des donnees jefy archivees
-- a creer meme si vous n'avez pas les anciens users jefy


CREATE TABLE MARACUJA.JEFY_OLD_TITRE
(
  EXE_EXERCICE  NUMBER,
  TIT_ORDRE     NUMBER(38)                 NOT NULL,
  TIT_DATE      DATE                       NOT NULL,
  TIT_MONT      VARCHAR2(12)               NOT NULL,
  TIT_MONTTVA   VARCHAR2(12)               NOT NULL,
  TIT_LIB       VARCHAR2(105)              NOT NULL,
  TIT_TYPE      CHAR(1)                    NOT NULL,
  TIT_PIECE     INTEGER,
  JOU_ORDRE     NUMBER(38),
  FOU_ORDRE     NUMBER(38),
  PCO_NUM       VARCHAR2(20),
  TIT_IMPUTTVA  VARCHAR2(20),
  BOR_ORDRE     NUMBER(38),
  ORG_ORDRE     NUMBER(38),
  TIT_INTERNE   NUMBER(38),
  TIT_NUM       NUMBER(38),
  AGT_ORDRE     NUMBER(38)                 NOT NULL,
  TIT_STAT      CHAR(1)                    NOT NULL,
  GES_CODE      VARCHAR2(10),
  TIT_DEBITEUR  VARCHAR2(120),
  DEP_ORDRE     NUMBER(38),
  RIB_ORDRE     NUMBER(38),
  TIT_MONNAIE   VARCHAR2(1),
  TIT_VIREMENT  VARCHAR2(1),
  MOD_CODE      VARCHAR2(3),
  DST_CODE      VARCHAR2(10),
  TIT_REF       VARCHAR2(15)               ,
  CONV_ORDRE    NUMBER,
  CAN_CODE      VARCHAR2(8),
  PRES_ORDRE    NUMBER
)
TABLESPACE GFC;




CREATE TABLE MARACUJA.JEFY_OLD_FACTURE
(
  EXE_EXERCICE        NUMBER,
  DEP_ORDRE           NUMBER(38)                NOT NULL,
  DEP_FACT            VARCHAR2(30)         ,
  DEP_DATE            DATE                      ,
  DEP_LIB             VARCHAR2(40)         ,
  DEP_TTC             NUMBER(12,2)              ,
  DEP_MONT            NUMBER(12,2)              ,
  DEP_PIECE           NUMBER                    ,
  DEP_SECT            CHAR(1),
  CAN_CODE            VARCHAR2(8),
  RIB_ORDRE           NUMBER(38),
  MOD_CODE            VARCHAR2(3)          ,
  PCO_NUM             VARCHAR2(20),
  DST_CODE            VARCHAR2(10)         ,
  AGT_ORDRE           NUMBER(38)                ,
  MAN_ORDRE           NUMBER(38),
  CDE_ORDRE           NUMBER(38)                ,
  DEP_MONNAIE         VARCHAR2(1),
  DEP_VIREMENT        VARCHAR2(1),
  DEP_INVENTAIRE      VARCHAR2(300),
  DEP_HT_SAISIE       NUMBER(12,2),
  DEP_CREATION        DATE,
  DEP_PRORATA         NUMBER,
  CM_ORDRE            NUMBER,
  CM_TYPE             VARCHAR2(10),
  CM_ACHAT            VARCHAR2(10),
  DEP_DATECOMPOSANTE  DATE,
  DEP_DATEDAF         DATE,
  DEP_LUCRATIVITE     VARCHAR2(1),
  DEP_RGP             INTEGER                   ,
  DEP_BUDNAT          VARCHAR2(8)
)
TABLESPACE GFC;

CREATE TABLE MARACUJA.JEFY_OLD_DESTIN_REC
(
  EXE_EXERCICE  NUMBER,
  DST_CODE      VARCHAR2(20)                NOT NULL,
  DST_LIB       VARCHAR2(200)              ,
  DST_DLIB      VARCHAR2(200)              ,
  DST_ABREGE    VARCHAR2(100)              ,
  DST_TYPE      VARCHAR2(1)                ,
  DST_NIV       NUMBER(38)                      
)
TABLESPACE GFC;


CREATE TABLE MARACUJA.JEFY_OLD_DESTIN
(
  EXE_EXERCICE  NUMBER,
  DST_CODE      VARCHAR2(20)                NOT NULL,
  DST_LIB       VARCHAR2(200)              ,
  DST_DLIB      VARCHAR2(200)              ,
  DST_ABREGE    VARCHAR2(100)              ,
  DST_TYPE      VARCHAR2(1)                ,
  DST_NIV       NUMBER(38)                      
)
TABLESPACE GFC;



CREATE TABLE MARACUJA.JEFY_OLD_ORGAN
(
  EXE_EXERCICE           NUMBER,
  ORG_ORDRE              NUMBER(38)             NOT NULL,
  ORG_UNIT               VARCHAR2(10),
  ORG_COMP               VARCHAR2(10),
  ORG_LBUD               VARCHAR2(10),
  ORG_UC                 VARCHAR2(50),
  ORG_LIB                VARCHAR2(200)     ,
  ORG_NIV                INTEGER           ,
  ORG_STAT               INTEGER           ,
  ORG_DATE               DATE              ,
  ORG_TVA                INTEGER           ,
  SCT_CODE               VARCHAR2(20),
  ORG_RAT                NUMBER(38),
  DST_CODE               VARCHAR2(3),
  ORG_LUCRATIVITE        INTEGER,
  ORG_TYPE_LIGNE         INTEGER,
  ORG_TYPE_FLECHAGE      VARCHAR2(5),
  ORG_OUVERTURE_CREDITS  VARCHAR2(5),
  ORG_OBSERVATION        VARCHAR2(1000),
  OTC_CODE               VARCHAR2(6),
  ORG_DELEGATION         VARCHAR2(2)
)
TABLESPACE GFC;



CREATE TABLE COMPTEFI.JEFY_OLD_SITUATION
(
  EXE_EXERCICE   NUMBER,
  CDE_ORDRE   NUMBER,
  CDE_STAT    VARCHAR2(1),
  FOU_ORDRE   NUMBER,
  TCD_CODE    VARCHAR2(2),
  PCO_NUM     VARCHAR2(20),
  DST_CODE    VARCHAR2(10),
  ENG_LIB     VARCHAR2(512),
  ENG_ORDRE   VARCHAR2(30),
  ENG_DATE    DATE,
  ENG_TTC     NUMBER(12,2),
  ORG_ORDRE   NUMBER,
  DEP_ORDRE   VARCHAR2(30),
  DEP_TTC     NUMBER(12,2),
  MAN_ORDRE   NUMBER,
  SIT_EXER    CHAR(2),
  MAN_DATE    DATE,
  CLE_ENG     NUMBER(38),
  CLE_DEP     NUMBER(38),
  CM_CODE     VARCHAR2(10),
  RGP_NUMERO  VARCHAR2(20),
  CAN_CODE    VARCHAR2(10)
)
TABLESPACE GFC;

CREATE TABLE COMPTEFI.JEFY_OLD_BUDNAT
(
  EXE_EXERCICE  NUMBER,
  GES_CODE      VARCHAR2(10),
  PCO_NUM       VARCHAR2(3),
  BDN_QUOI      CHAR(1),
  BDN_NUMERO    NUMBER,
  DATE_CO       DATE,
  CO            NUMBER
)
TABLESPACE GFC;