
CREATE OR REPLACE FORCE VIEW MARACUJA.V_PI_MAN_TIT
(BOR_ID_DEP, MAN_ID, BOR_ID_REC, TIT_ID, PREST_ID)
AS 
select distinct m.bor_id as bor_id_dep, m.man_id , t.bor_id as bor_id_rec,t.tit_id, t.prest_id 
from jefy_recette.pi_dep_rec pdr,
jefy_recette.recette_ctrl_planco rpco,
jefy_depense.depense_ctrl_planco dpco,
maracuja.mandat m,
maracuja.titre t
where 
pdr.DEP_ID=dpco.dep_id
and pdr.rec_id=rpco.rec_id
and  dpco.man_id=m.man_id
and rpco.tit_id=t.tit_id
and m.prest_id=t.prest_id;
/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_RESTE_RECOUVRER_LIGHT
(TIT_ID, ECD_DEBIT, RESTE_RECOUVRER)
AS 
SELECT tde.tit_id, SUM(ecd.ecd_debit) ecd_debit, SUM(ecd.ecd_reste_emarger) reste_recouvrer
FROM ECRITURE_DETAIL ecd,
ECRITURE e,
TITRE_DETAIL_ECRITURE tde,
(SELECT TIT_ID, MAX(ecd1.EXE_ORDRE) EXE_ORDRE_MAX
FROM TITRE_DETAIL_ECRITURE td1, ECRITURE_DETAIL ecd1 WHERE td1.ecd_ordre=ecd1.ecd_ordre AND ((ecd1.pco_num LIKE '4%' AND ecd1.pco_num NOT LIKE '445%') OR ecd1.pco_num LIKE '5%')
GROUP BY tit_id) z
WHERE e.ecr_ordre = ecd.ecr_ordre
AND tde.ecd_ordre=ecd.ecd_ordre
AND tde.tit_id=z.tit_id
AND tde.EXE_ORDRE=z.exe_ordre_max
AND ((ecd.pco_num LIKE '4%' AND ecd.pco_num NOT LIKE '445%') OR ecd.pco_num LIKE '5%')
AND e.ecr_numero > 0
AND ecd.ecd_debit>0
AND SUBSTR(e.ecr_etat,1,1)='V'
and tde.TDE_ORIGINE='VISA'
GROUP BY tde.tit_id;
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_RESTE_RECOUVRER
(EXE_ORDRE, EXE_EXERCICE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 TIT_NUMERO, TIT_DATE_EMISSION, REC_DEBITEUR, TIT_ID, FOU_CODE, 
 ADR_NOM, ADR_PRENOM, ADR_CP, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_VILLE, TIT_ETAT, TIT_HT, TIT_TTC, TIT_TVA, 
 TIT_LIBELLE, ORI_ENTITE, ORI_KEY_ENTITE, ECD_CREDIT, ECD_DEBIT, 
 RESTE_RECOUVRER)
AS 
SELECT t.exe_ordre, ex.exe_exercice, ecd.pco_num, t.ges_code, api_planco.get_pco_libelle(ecd.pco_num, t.exe_ordre) pco_libelle, t.TIT_NUMERO, x.tit_date_emission,  x.rec_debiteur,
t.tit_id, f.FOU_CODE, f.ADR_NOM, f.ADR_PRENOM, f.ADR_CP, f.ADR_ADRESSE1, f.ADR_ADRESSE2,f.ADR_VILLE,
    t.TIT_ETAT, t.tit_ht, t.TIT_TTC, t.TIT_TVA, t.TIT_LIBELLE, o.ORI_ENTITE ,
o.ORI_KEY_ENTITE, ecd.ECD_CREDIT, ecd.ecd_debit, ecd.ecd_reste_emarger reste_recouvrer
FROM ECRITURE_DETAIL ecd,
ECRITURE e,
TITRE_DETAIL_ECRITURE tde,
TITRE t,
v_fournisseur f,
ORIGINE o,
EXERCICE ex,
(SELECT TIT_ID, MAX(ecd1.EXE_ORDRE) EXE_ORDRE_MAX
FROM TITRE_DETAIL_ECRITURE td1, ECRITURE_DETAIL ecd1 WHERE td1.ecd_ordre=ecd1.ecd_ordre AND ((ecd1.pco_num LIKE '4%' AND ecd1.pco_num NOT LIKE '445%') OR ecd1.pco_num LIKE '5%')
GROUP BY tit_id) z,
(SELECT tit_id, MAX(rec_date) tit_date_emission , MAX(rec_debiteur) rec_debiteur, MAX(fou_ordre) fou_ordre FROM RECETTE GROUP BY tit_id) x
WHERE
    e.ecr_ordre = ecd.ecr_ordre
AND t.exe_ordre=ex.exe_ordre
AND tde.ecd_ordre=ecd.ecd_ordre
AND tde.tit_id=t.tit_id
AND tde.tit_id=z.tit_id
AND tde.EXE_ORDRE=z.exe_ordre_max
AND x.tit_id=t.tit_id
AND x.fou_ordre=f.fou_ordre(+)
AND t.ori_ordre=o.ori_ordre(+)
AND ((ecd.pco_num LIKE '4%' AND ecd.pco_num NOT LIKE '445%') OR ecd.pco_num LIKE '5%')
AND e.ecr_numero > 0
AND SUBSTR(e.ecr_etat,1,1)='V'
and tde.TDE_ORIGINE='VISA';
/









-- changement des vues pour supprimer les references aux anciens users jefy + nettoyage



CREATE OR REPLACE FORCE VIEW MARACUJA.V_MARCHE
(MAR_SUPPR, EXE_ORDRE, MAR_CLAUSES, MAR_DEBUT, MAR_FIN, 
 MAR_INDEX, MAR_LIBELLE, MAR_ORDRE, MAR_PASSATION, MAR_VALIDE)
AS 
SELECT "MAR_SUPPR","EXE_ORDRE","MAR_CLAUSES","MAR_DEBUT","MAR_FIN","MAR_INDEX","MAR_LIBELLE","MAR_ORDRE","MAR_PASSATION","MAR_VALIDE"
FROM jefy_marches.marche;
--------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_LOT
(LOT_CATALOGUE, LOT_DEBUT, LOT_FIN, LOT_HT, LOT_INDEX, 
 LOT_LIBELLE, LOT_MONNAIE, LOT_ORDRE, LOT_SUPPR, LOT_VALIDE, 
 MAR_ORDRE, LOT_SOUSTRAITANTS, LOT_COTITULAIRES)
AS 
select "LOT_CATALOGUE","LOT_DEBUT","LOT_FIN","LOT_HT","LOT_INDEX","LOT_LIBELLE","LOT_MONNAIE","LOT_ORDRE","LOT_SUPPR","LOT_VALIDE","MAR_ORDRE","LOT_SOUSTRAITANTS","LOT_COTITULAIRES" from jefy_marches.lot;

--------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_ATTRIBUTION
(ATT_DATE, ATT_ORDRE, ATT_SUPPR, ATT_VALIDE, FOU_ORDRE, 
 LOT_ORDRE, ATT_DEBUT, ATT_FIN, ATT_TYPE_CONTROLE, ATT_ACCEPTEE, 
 UTL_ORDRE, ATT_HT, TIT_ORDRE, ATT_INDEX)
AS
select "ATT_DATE","ATT_ORDRE","ATT_SUPPR","ATT_VALIDE","FOU_ORDRE","LOT_ORDRE","ATT_DEBUT","ATT_FIN","ATT_TYPE_CONTROLE","ATT_ACCEPTEE","UTL_ORDRE","ATT_HT","TIT_ORDRE","ATT_INDEX" from jefy_marches.ATTRIBUTION;

--------------------------





CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
select EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV from maracuja.jefy_old_destin;
GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN TO COMPTEFI;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN_REC
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
select EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV from maracuja.jefy_old_destin_rec;

GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN_REC TO COMPTEFI;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_FACTURE
(EXE_EXERCICE, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
 DEP_BUDNAT)
AS
select EXE_EXERCICE, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
 DEP_BUDNAT from maracuja.jefy_old_facture; 



CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_TITRE
(EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE)
AS 
select EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
 from maracuja.jefy_old_titre;
GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_TITRE TO COMPTEFI;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN
(ORG_ORDRE, ORG_UNIT, ORG_COMP, ORG_LBUD, ORG_UC, 
 ORG_LIB, ORG_NIV, ORG_STAT, ORG_DATE, ORG_TVA, 
 SCT_CODE, ORG_RAT, DST_CODE, ORG_LUCRATIVITE, ORG_TYPE_LIGNE, 
 ORG_TYPE_FLECHAGE, ORG_OUVERTURE_CREDITS, ORG_OBSERVATION, OTC_CODE, ORG_DELEGATION)
AS 
SELECT ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
FROM maracuja.jefy_old_organ where exe_exercice=2006
UNION ALL
SELECT ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
FROM maracuja.jefy_old_organ where exe_exercice=2005 and org_ordre IN (SELECT org_ordre FROM maracuja.jefy_old_organ where exe_exercice=2005 MINUS SELECT org_ordre FROM maracuja.jefy_old_organ where exe_exercice=2006);
 
 

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_JEFY_MULTIEX_SITU
(EXE_ORDRE, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_CODE, 
 PCO_NUM, DST_CODE, ENG_LIB, ENG_ORDRE, ENG_DATE, 
 ENG_TTC, ORG_ORDRE, DEP_ORDRE, DEP_TTC, MAN_ORDRE, 
 SI_EXER, MAN_DATE, CLE_ENG, CLE_DEP, CM_CODE, 
 RGP_NUMERO, CAN_CODE)
AS 
select EXE_EXERCICE, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_CODE, 
 PCO_NUM, DST_CODE, ENG_LIB, ENG_ORDRE, ENG_DATE, 
 ENG_TTC, ORG_ORDRE, DEP_ORDRE, DEP_TTC, MAN_ORDRE, 
 SIT_EXER, MAN_DATE, CLE_ENG, CLE_DEP, CM_CODE, 
 RGP_NUMERO, CAN_CODE from comptefi.jefy_old_situation;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN_EXER
(EXE_ORDRE, ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, 
 ORG_SOUSCR, ORG_LIB, ORG_NIV, ORG_PERE, ORG_LUCRATIVITE, 
 TYOR_ID)
AS 
SELECT EXE_ORDRE, ORG_ID, org_etab, org_ub, org_cr, org_souscr, org_lib, ORG_NIV, ORG_PERE, ORG_LUCRATIVITE, TYOR_ID
FROM jefy_admin.v_organ
WHERE exe_ordre>2006
UNION ALL
SELECT 2006,	ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_RAT,  ORG_LUCRATIVITE, 1
FROM maracuja.jefy_old_organ where exe_exercice=2006
UNION ALL
SELECT 2005,	ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_RAT,  ORG_LUCRATIVITE, 1
FROM maracuja.jefy_old_organ where exe_exercice=2005;

GRANT SELECT ON  MARACUJA.V_ORGAN_EXER TO COMPTEFI;
GRANT SELECT ON  MARACUJA.V_ORGAN_EXER TO EPN;






CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 1
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 2
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 3
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select EXE_EXERCICE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO,  DATE_CO, CO from comptefi.jefy_old_budnat;





drop view maracuja.v_agent_jefy;
drop view maracuja.v_jefy_multiex_mandat;
drop view maracuja.v_web_pays;











CREATE OR REPLACE PROCEDURE COMPTEFI.Prepare_Bilan (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  totalbrut NUMBER(12,2);
  totalamort NUMBER(12,2);
  totalnet NUMBER(12,2);
  totalnetant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);

  -- version du 16/06/2009

BEGIN

--*************** CREATION TABLE ACTIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- ACTIF IMMOBILISE ----
lib1:= 'ACTIF IMMOBILISE';
---- Immobilisations Incorporelles ---
lib2:= 'IMMOBILISATIONS INCORPORELLES';

-- Compte 201 ---
	totalbrut := Solde_Compte(exeordre, '201%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2831%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '201%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2831%', 'C', gescode, sacd));
	lib := 'Frais d''établissement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 203 ---
	totalbrut := Solde_Compte(exeordre, '203%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2833%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '203%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2833%', 'C', gescode, sacd));
	lib := 'Frais de recherche et de développement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 205 ---
	totalbrut := Solde_Compte(exeordre, '205%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2905%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '205%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2905%', 'C', gescode, sacd));
	lib := 'Concessions et droits similaires';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 206 ---
	totalbrut := Solde_Compte(exeordre, '206%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2906%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '206%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2906%', 'C', gescode, sacd);
	lib := 'Droit au bail';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 208 ---
	totalbrut := Solde_Compte(exeordre, '208%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2838%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '208%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2838%', 'C', gescode, sacd));
	lib := 'Autres immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 232 ---
	totalbrut := Solde_Compte(exeordre, '232%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2932%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '232%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2932%', 'C', gescode, sacd);
	lib := 'Immobilisations incorporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 237 ---
	totalbrut := Solde_Compte(exeordre, '237%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '237%','D', gescode, sacd);
	lib := 'Avances et acomptes versés sur immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);



---- Immobilisations Corporelles ---
lib2:= 'IMMOBILISATIONS CORPORELLES';

-- Compte 211 et 212 ---
	totalbrut := Solde_Compte(exeordre, '211%','D', gescode, sacd)
		+Solde_Compte(exeordre, '212%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2911%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2842%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '211%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '212%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2911%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2842%', 'C', gescode, sacd));
	lib := 'Terrains, agencements et aménagements de terrain';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 213 et 214 ---
	totalbrut := Solde_Compte(exeordre, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre, '214%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2814%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2844%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '214%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2814%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2844%', 'C', gescode, sacd));
	lib := 'Constructions et constructions sur sol d''autrui';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 215 ---
	totalbrut := Solde_Compte(exeordre, '215%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2845%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '215%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2845%', 'C', gescode, sacd));
	lib := 'Installations techniques, matériels et outillage';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 216 ---
	totalbrut := Solde_Compte(exeordre, '216%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2846%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '216%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2846%', 'C', gescode, sacd));
	lib := 'Collections';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 218 ---
	totalbrut := Solde_Compte(exeordre, '218%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2848%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '218%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2848%', 'C', gescode, sacd));
	lib := 'Autres immobilisations corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 231 ---
	totalbrut := Solde_Compte(exeordre, '231%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2931%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '231%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2931%', 'C', gescode, sacd);
	lib := 'Immobilisations corporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 238 ---
	totalbrut := Solde_Compte(exeordre, '238%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '238%','D', gescode, sacd);
	lib := 'Avances et acomptes versés sur commandes d''immobilisation corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Immobilisations Financières ---
lib2:= 'IMMOBILISATIONS FINANCIERES';

-- Compte 261 et 266 ---
	totalbrut := Solde_Compte(exeordre, '261%','D', gescode, sacd)
		+Solde_Compte(exeordre, '266%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2966%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '261%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '266%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2966%', 'C', gescode, sacd));
	lib := 'Participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 267 et 268 ---
	totalbrut := Solde_Compte(exeordre, '267%','D', gescode, sacd)
		+Solde_Compte(exeordre, '268%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2968%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '267%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '268%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2968%', 'C', gescode, sacd));
	lib := 'Créances rattachées à des participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 271 et 272 ---
	totalbrut := Solde_Compte(exeordre, '271%','D', gescode, sacd)
		+Solde_Compte(exeordre, '272%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2972%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '271%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '272%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2972%', 'C', gescode, sacd));
	lib := 'Autres titres immobilisés';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 274 --
	totalbrut := Solde_Compte(exeordre, '274%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2974%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '274%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2974%', 'C', gescode, sacd);
	lib := 'Prêts';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 275 et 2761  --
totalbrut := Solde_Compte(exeordre, '275%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2761%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2768%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2975%', 'C', gescode, sacd)
	+Solde_Compte(exeordre, '2976%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '275%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2761%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2768%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2975%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2976%', 'C', gescode, sacd));
	lib := 'Autres immobilisations financières';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


----- ACTIF CIRCULANT ----
lib1:= 'ACTIF CIRCULANT';
---- Stocks ---
lib2:= 'STOCKS ET EN-COURS';

-- Compte 31 et 32---
	totalbrut := Solde_Compte(exeordre, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre, '32%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '392%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '32%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '392%', 'C', gescode, sacd));
	lib := 'Matières premières et autres approvisionnements';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 33 et 34 ---
	totalbrut := Solde_Compte(exeordre, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre, '34%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '394%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '34%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '394%', 'C', gescode, sacd));
	lib := 'En-cours de production de biens et de services';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 35 ---
	totalbrut := Solde_Compte(exeordre, '35%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '395%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '35%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '395%', 'C', gescode, sacd);
	lib := 'Produits intermédiaires et finis';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 37 ---
	totalbrut := Solde_Compte(exeordre, '37%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '397%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '37%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '397%', 'C', gescode, sacd);
	lib := 'Marchandises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Avances et acomptes versés sur commande ---
lib2:= 'AVANCES ET ACOMPTES';
-- Compte 4091 et 4092 ---
	totalbrut := Solde_Compte(exeordre, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre, '4092%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '4092%','D', gescode, sacd);
	lib := 'Avances et acomptes versés sur commandes';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Créances exploitations ---
lib2:= 'CREANCES D''EXPLOITATION';

-- Compte 411, 412, 413, 416 et 418 ---
	totalbrut := Solde_Compte(exeordre, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '418%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '491%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '418%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '491%', 'C', gescode, sacd);
	lib := 'Créances clients et comptes rattachés';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte autres ---
	totalbrut := Solde_Compte(exeordre, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4417%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
--		+ Solde_Compte(exeordre, '472%','D', gescode, sacd)
--		--- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
		+ Solde_Compte(exeordre, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4417%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre-1, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd))
--		+ Solde_Compte(exeordre-1, '472%','D', gescode, sacd)
		---Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','D', gescode, sacd);
	lib := 'Autres créances d''exploitation';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Créances diverses ---
lib2:= 'CREANCES DIVERSES';

-- Compte TVA ---
	totalbrut := Solde_Compte(exeordre, '4456%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44581%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44583%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4456%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44581%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44583%','D', gescode, sacd);
	lib := 'TVA';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Comptes Autres --- Spécial LR 4412 4413 !!
	totalbrut :=
		Solde_Compte(exeordre, '429%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4687%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '495%', 'C', gescode, sacd)
	 	+ Solde_Compte(exeordre, '496%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (
		Solde_Compte(exeordre-1, '429%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4687%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '495%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '496%', 'C', gescode, sacd));
	lib := 'Autres créances diverses';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Trésorerie ---
lib2:= 'TRESORERIE';

-- Compte 50---
	totalbrut := Solde_Compte(exeordre, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre, '509%', 'C', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '590%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '509%', 'C', gescode, sacd))
		- Solde_Compte(exeordre-1, '590%', 'C', gescode, sacd);
	lib := 'Valeurs mobilières de placement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte Disponibilités ---
	totalbrut := Solde_Compte(exeordre, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre, '185%', 'C', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre-1, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '185%', 'C', gescode, sacd);
	lib:= 'Disponibilités';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 486 ---
	totalbrut := Solde_Compte(exeordre, '486%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '486%','D', gescode, sacd);
	lib := 'Charges constatées d''avance';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '';

-- Compte 481---
	totalbrut := Solde_Compte(exeordre, '481%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '481%','D', gescode, sacd);
	lib := 'Charges à répartir sur plusieurs exercices';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 476 ---
	totalbrut := Solde_Compte(exeordre, '476%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '476%','D', gescode, sacd);
	lib := 'Différences de conversion sur opérations en devises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


--*************** CREATION TABLE PASSIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- CAPITAUX PROPRES ----
lib1:= 'CAPITAUX PROPRES';
---- Capital---
lib2:= 'CAPITAL ET RESERVES';

-- Compte 1021 ---
	totalnet := Solde_Compte(exeordre, '1021%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1021%','C', gescode, sacd);
	lib := 'Dotation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1022 ---
	totalnet := Solde_Compte(exeordre, '1022%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1022%','C', gescode, sacd);
	lib := 'Complément de dotation (Etat)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1023 ---
	totalnet := Solde_Compte(exeordre, '1023%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1023%','C', gescode, sacd);
	lib := 'Complément de dotation (autres organismes)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1027 ---
	totalnet := Solde_Compte(exeordre, '1027%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1027%','C', gescode, sacd);
	lib := 'Affectation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 103 ---
	totalnet := Solde_Compte(exeordre, '103%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '103%','C', gescode, sacd);
	lib := 'Biens remis en pleine propriété aux établissements';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 105 ---
	totalnet := Solde_Compte(exeordre, '105%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '105%','C', gescode, sacd);
	lib := 'Ecarts de réévaluation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1068 ---
	totalnet := Solde_Compte(exeordre, '1068%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1068%','C', gescode, sacd);
	lib := 'Réserves';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1069 ---
	totalnet := Solde_Compte(exeordre, '1069%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1069%','D', gescode, sacd);
	lib := 'Dépréciation de l''actif';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, -totalnet, -totalnetant, gescode,exeordre);

-- Compte 110 ou 119 ---
	totalnet := Solde_Compte(exeordre, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre,'119%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'119%','D', gescode, sacd);
	lib := 'Report à nouveau';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 120 ou 129 ---
	totalnet := Solde_Compte(exeordre, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre,'129%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'129%','D', gescode, sacd);
	lib := 'Résultat de l''exercice (bénéfice ou perte)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

-- Compte 13 --- Spécial LR 130 !
	totalnet := Solde_Compte(exeordre, '130%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '131%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre,'139%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '131%','C', gescode, sacd)
	 	+ Solde_Compte(exeordre-1, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'139%','D', gescode, sacd);
	lib := 'Subventions d''investissement';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

----- PROVISIONS POUR RISQUES ET CHARGES ----
lib1:= 'PROVISIONS POUR RISQUES ET CHARGES';
lib2:= '  ';

-- Compte 151 ---
	totalnet := Solde_Compte(exeordre, '151%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '151%','C', gescode, sacd);
	lib := 'Provisions pour risques';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 157 et 158 ---
	totalnet := Solde_Compte(exeordre, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '158%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '158%','C', gescode, sacd);
	lib := 'Provisions pour charges';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- DETTES ----
	lib1:= 'DETTES';
---- DETTES FINANCIERES ---
lib2:= 'DETTES FINANCIERES';

-- Compte Emprunts etab crédits ---
	totalnet := Solde_Compte(exeordre, '164%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '164%','C', gescode, sacd);
	lib := 'Emprunts auprès des établissements de crédit';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres emprunts ---
	totalnet := Solde_Compte(exeordre, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd);
	lib := 'Emprunts divers';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 419 ---
	totalnet := Solde_Compte(exeordre, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4192%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4192%','C', gescode, sacd);
	lib := 'Avances et acomptes reçus sur commandes';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

---- DETTES EXPLOITATION ---
lib2:= 'DETTES D''EXPLOITATION';

-- Compte Dettes fournisseurs ---
	totalnet := Solde_Compte(exeordre, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4088%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4088%','C', gescode, sacd);
	lib := 'Fournisseurs et comptes rattachés';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Dettes fiscales ---
	totalnet := Solde_Compte(exeordre,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4486%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4486%','C', gescode, sacd);
	lib := 'Dettes fiscales et sociales';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres dettes ---
	totalnet := Solde_Compte(exeordre, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4731%','C', gescode, sacd)
		--+ Solde_Compte(exeordre, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4731%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','C', gescode, sacd);
	lib := 'Autres dettes d''exploitation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

--- DETTES diverses ---
lib2:= 'DETTES DIVERSES';

-- Compte Dettes sur immo ---
	totalnet := Solde_Compte(exeordre, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4084%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4084%','C', gescode, sacd);
	lib := 'Dettes sur immobilisations et comptes rattachés';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Autres Dettes ---
	totalnet := Solde_Compte(exeordre, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '509%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '509%','C', gescode, sacd);
	lib := 'Autres dettes diverses';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 487 ---
	totalnet := Solde_Compte(exeordre, '487%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '487%','C', gescode, sacd);
	lib := 'Produits constatés d''avance';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '  ';

-- Compte Emprunts etab crédits ---
	totalnet := Solde_Compte(exeordre, '477%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '477%','C', gescode, sacd);
	lib := 'Différences de conversion sur opérations en devises' ;
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

END;
/











-- suppression des references aux anciens users jefy 

CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER);

PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau (borid INTEGER);
PROCEDURE prv_apres_visa_reversement(borid INTEGER);


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007

PROCEDURE apres_visa_bordereau (borid INTEGER)
IS
BEGIN
          prv_apres_visa_bordereau(borid);
END;



PROCEDURE prv_apres_visa_bordereau(borid INTEGER)
IS
  leBordereau BORDEREAU%ROWTYPE;
  leSousType TYPE_BORDEREAU.TBO_SOUS_TYPE%TYPE;
BEGIN
     SELECT * INTO leBordereau FROM BORDEREAU WHERE bor_id=borid;
     SELECT TBO_SOUS_TYPE INTO leSousType FROM TYPE_BORDEREAU WHERE tbo_ordre = leBordereau.tbo_ordre;
     IF leBordereau.bor_etat = 'VISE' THEN
             IF (leSousType = 'REVERSEMENTS') THEN
                 prv_apres_visa_reversement(borid);
             END IF;
              emarger_visa_bord_prelevement(borid);
    END IF;

END;


-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
PROCEDURE prv_apres_visa_reversement(borid INTEGER)
IS
  manid MANDAT.man_id%TYPE;

  CURSOR c_mandatVises IS
           SELECT man_id FROM MANDAT WHERE bor_id=borid AND man_etat='VISE';

BEGIN
     OPEN c_mandatVises;
        LOOP
            FETCH c_mandatVises INTO manid;
                  EXIT WHEN c_mandatVises%NOTFOUND;
            jefy_depense.apres_visa.viser_reversement(manid);
        END LOOP;
        CLOSE c_mandatVises;
END;



PROCEDURE apres_reimputation (reiordre INTEGER)
IS
    ex INTEGER;
    manid mandat.man_id%type;
    titid titre.tit_id%type;
    pconumnouveau plan_comptable.pco_num%type;
    pconumancien plan_comptable.pco_num%type;
BEGIN

        SELECT man_id, tit_id, pco_num_nouveau, pco_num_ancien INTO manid, titid, pconumnouveau, pconumancien FROM REIMPUTATION WHERE rei_ordre=reiordre;
        IF manid IS NOT NULL THEN
            update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;        
        end if;          
          
        IF titid IS NOT NULL THEN
            update jefy_recette.recette_ctrl_planco set pco_num=pconumnouveau where tit_id=titid and pco_num=pconumancien;        
        end if;          
          

END;


PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
             --SELECT 1 INTO  cpt FROM dual;
         emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
         emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
     -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
             SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
         emarger_prelevement(recoordre);
END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS
begin
    if (recoordre is null) then
        RAISE_APPLICATION_ERROR (-20001,'Le parametre recoordre est null.');
    end if;
    emarger_prelev_ac_titre(recoordre);
    emarger_prelev_ac_ecr(recoordre);
end;



-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER) is

    CURSOR titres (lerecoordre INTEGER ) IS
     SELECT * FROM TITRE
     WHERE tit_id IN
     (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
     WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
     AND p.reco_ordre = lerecoordre)
     ;

    CURSOR non_emarge_credit (letitid INTEGER) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';

    letitre maracuja.TITRE%ROWTYPE;
    ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
    ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
    EMAORDRE EMARGEMENT.ema_ordre%TYPE;
    lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
    cpt INTEGER;

BEGIN

        -- recup infos
         SELECT * INTO lerecouvrement FROM RECOUVREMENT
         WHERE reco_ordre = recoordre;

        -- creation de l emargement !
         SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

         INSERT INTO EMARGEMENT
          (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
         VALUES
          (
          SYSDATE,
          -1,
          EMAORDRE,
          lerecouvrement.exe_ordre,
          3,
          lerecouvrement.UTL_ORDRE,
          lerecouvrement.COM_ORDRE,
          0,
          'VALIDE'
          );

        -- on fetch les titres du recrouvement
        OPEN titres(recoordre);
        LOOP
        FETCH titres INTO letitre;
        EXIT WHEN titres%NOTFOUND;
        -- on recupere les ecritures non emargees debits
         OPEN non_emarge_credit (letitre.tit_id);
         LOOP
         FETCH non_emarge_credit INTO ecriture_credit;
         EXIT WHEN non_emarge_credit%NOTFOUND;

        -- on recupere les ecritures non emargees credit
         OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
         LOOP
         FETCH non_emarge_debit_compte INTO ecriture_debit;
         EXIT WHEN non_emarge_debit_compte%NOTFOUND;

         IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
         THEN
          -- creation de l emargement detail !
          INSERT INTO EMARGEMENT_DETAIL
          (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
          VALUES
          (
          ecriture_credit.ecd_ordre,
          ecriture_debit.ecd_ordre,
          EMAORDRE,
          ecriture_credit.ecd_reste_emarger,
          emargement_detail_seq.NEXTVAL,
          ecriture_debit.exe_ordre
          );

          -- maj de l ecriture debit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_debit.ecd_ordre;

          -- maj de lecriture credit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_credit.ecd_ordre;
         END IF;

         END LOOP;
         CLOSE non_emarge_debit_compte;

        END LOOP;
        CLOSE non_emarge_credit;

        END LOOP;
        CLOSE titres;
        -- suppression de lemagenet si pas de details;
        SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
        WHERE ema_ordre = EMAORDRE;

        IF cpt = 0 THEN
            DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
        else
            Numerotationobject.numeroter_emargement(EMAORDRE);
        END IF;



END;




-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER)
is
  CURSOR lesPrelevements (lerecoordre INTEGER ) IS
     SELECT * FROM maracuja.prelevement
     WHERE reco_ordre=lerecoordre;
     
  ecrDetailCredit ecriture_detail%rowtype;       
  ecrDetailDebit ecriture_detail%rowtype;
    ecriture_credit ecriture_detail%rowtype;    
    ecriture_debit ecriture_detail%rowtype;         
  lePrelevement maracuja.prelevement%rowtype;
  flag integer;
  flag2 integer;
  retVal integer;
  utlOrdre integer;
  
    CURSOR non_emarge_credit (prelevOrdre INTEGER) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (prelevOrdre INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';  
    
begin

        select utl_ordre into utlOrdre from recouvrement where reco_ordre=recoordre;


        OPEN lesPrelevements(recoordre);
        LOOP
            FETCH lesPrelevements INTO lePrelevement;
            EXIT WHEN lesPrelevements%NOTFOUND;
            ecrDetailCredit := null;
            ecrDetailDebit := null;
            
            
            -- recuperation de l'ecriture de l'echeancier non emargee en debit
            SELECT count(*) into flag2 FROM maracuja.Ecriture_detail 
             where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
             select ecd_ordre from maracuja.echeancier_detail_ecr  
             WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
            if (flag2=1) then
                SELECT * into ecrDetailDebit FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
                 select ecd_ordre from maracuja.echeancier_detail_ecr  
                 WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
             
             
                 -- s'il y a une ecriture non emargee en debit sur l'echeancier, 
                 -- on emarge avec le credit correspondant du prelevement
                 -- recuperation de l'ecriture du prelevement non emargee en credit
             
                SELECT count(*) into flag FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                 select ecd_ordre from maracuja.prelevement_detail_ecr  
                 WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                if (flag=1) then
                     SELECT * into ecrDetailCredit FROM maracuja.Ecriture_detail 
                     where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                     select ecd_ordre from maracuja.prelevement_detail_ecr  
                     WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                    -- si les deux ecritures ont ete recuperees, on les emarge  
                    IF (Afaireaprestraitement.verifier_emar_exercice(ecrDetailCredit.ecr_ordre,ecrDetailDebit.ecr_ordre) = 1) then          
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecrDetailCredit.ecd_ordre, ecrDetailDebit.ecd_ordre, 3, UTLORDRE );
                    end if;             
                end if;

            end if;                        
            
            
            -- emargement entre les debits et credits associes au prelevement 
            -- (suite a saisie releve)
             
            -- on recupere les ecritures non emargees debits
             OPEN non_emarge_credit (lePrelevement.prel_prelev_ordre );
             LOOP
             FETCH non_emarge_credit INTO ecriture_credit;
             EXIT WHEN non_emarge_credit%NOTFOUND;

                -- on recupere les ecritures non emargees credit
                 OPEN non_emarge_debit_compte (lePrelevement.prel_prelev_ordre,ecriture_credit.pco_num);
                 LOOP
                 FETCH non_emarge_debit_compte INTO ecriture_debit;
                 EXIT WHEN non_emarge_debit_compte%NOTFOUND;
                    
                     IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
                     THEN
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, UTLORDRE );         
                     END IF;

                 END LOOP;
                 CLOSE non_emarge_debit_compte;

            END LOOP;
            CLOSE non_emarge_credit;            
            
            
            
            

            
   
            
         END LOOP;
        CLOSE lesPrelevements;

end;





PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_ordre = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
    DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
else
    Numerotationobject.numeroter_emargement(EMAORDRE);
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/

---------------------



CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

    SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentmab.mab_montant,
    NULL,
    currentmab.mab_sens,
    ecrordre,
    currentmab.ges_code,
    currentmab.pco_num
    );

    SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

    -- Insertion des mandat_detail_ecritures
    INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
    ecdordre,
    currentmab.exe_ordre,
    currentmab.man_id,
    SYSDATE,
    mdeordre,
    'VISA',
    oriordre
    );



    END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
        boretat := 'RETENUES';
  ELSE
        boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

  UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);


  UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_sacd = ecrordre WHERE bor_id = borid;
  

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
                ecrordre := creerecriture(
                1,
                SYSDATE,
                'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
                exeordre,
                oriordre,
                14,
                9,
                0
                );

              -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
              OPEN bordereaux;
              LOOP
                FETCH bordereaux INTO currentbob;
                EXIT WHEN bordereaux%NOTFOUND;

                        ecdordre := creerecrituredetail (
                        NULL,
                        'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
                        currentbob.bob_montant,
                        NULL,
                        currentbob.bob_sens,
                        ecrordre,
                        currentbob.ges_code,
                        currentbob.pco_num
                        );

                END LOOP;
              CLOSE bordereaux;

              Api_Plsql_Journal.validerecriture(ecrordre);


             

  END IF;

END IF;



          UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
          UPDATE BORDEREAU
          SET
          bor_date_visa = SYSDATE,
          utl_ordre_visa = 0,
          bor_etat = 'PAIEMENT'
          WHERE bor_id = borid;

          SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

          UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'PAIEMENT', ECR_ORDRE_OPP = ecrordre WHERE bor_id = borid;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
AND e.ecr_sens='D'
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
     gescodes := '';
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
         IF (LENGTH(gescodes)>0) THEN
             gescodes := gescodes || ', ';
         END IF;
          gescodes := gescodes || curgescode;
    END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT' AND B.BOR_ETAT <> 'PAYE';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;



IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
    ecrordre := creerecriture(
    1,
    SYSDATE,
    'PAIEMENT SALAIRES '||borlibelle_mois,
    exeordre,
    oriordre,
    14,
    6,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'PAIEMENT SALAIRES '||borlibelle_mois,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);



    UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
  END IF;

 ELSE
            UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;


END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bor_id in (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=3);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
    localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
    localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE         --        INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/




CREATE OR REPLACE PACKAGE MARACUJA.gestionOrigine IS

/*
ORI_ENTITE  	  USER.TABLE  -
ORI_KEY_NAME 	  PRIMARY KEY -
ORI_LIBELLE 	  LIBELLE UTILISATEUR -
ORI_ORDRE 		  ID -
ORI_KEY_ENTITE	  VALEUR DE LA KEY -
TOP_ORDRE		  type d origine -
*/

FUNCTION traiter_orgordre (orgordre INTEGER) RETURN INTEGER;
FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION traiter_convordre (convordre INTEGER) RETURN  INTEGER;
FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
PROCEDURE maj_origine;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006
-- version 1.5.6 29/03/2007 - corrections doublons

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

-- procedure obsolete

--	 	   IF orgordre IS NULL THEN RETURN NULL; END IF;

--		-- recup du type_origine --
--		SELECT top_ordre INTO topordre FROM TYPE_OPERATION
--		WHERE top_libelle = 'OPERATION LUCRATIVE';



--		-- l origine est t elle deja  suivie --
--		SELECT COUNT(*)INTO cpt FROM ORIGINE
--		WHERE ORI_KEY_NAME = 'ORG_ORDRE'
--		AND ORI_ENTITE ='JEFY.ORGAN'
--		AND ORI_KEY_ENTITE	=orgordre;

--		IF cpt >= 1 THEN
--			SELECT ori_ordre INTO cpt FROM ORIGINE
--			WHERE ORI_KEY_NAME = 'ORG_ORDRE'
--			AND ORI_ENTITE ='JEFY.ORGAN'
--			AND ORI_KEY_ENTITE	=orgordre
--			AND ROWNUM=1;

--		ELSE
--			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

--			-- recup de la LIGNE BUDGETAIRE --
--			--le libelle utilisateur pour le suivie en compta --
--			SELECT org_comp||'-'||org_lbud||'-'||org_uc
--			 INTO orilibelle
--			FROM jefy.organ
--			WHERE org_ordre = orgordre;

--			INSERT INTO ORIGINE
--			(ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
--			VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);

--		END IF;

--		RETURN cpt;
    return 0;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
typeOrgan v_organ_exer.TYOR_ID%TYPE;

BEGIN

	 	  IF orgid IS NULL THEN RETURN NULL; END IF;

		  SELECT tyor_id INTO typeOrgan FROM jefy_admin.organ WHERE org_id=orgid;

			SELECT COUNT(*) INTO cpt
					FROM accords.CONVENTION_LIMITATIVE
					WHERE org_id = orgid AND exe_ordre = exeordre;

		  -- si c une convention RA
		  IF (typeOrgan = 2 OR cpt>0) THEN
					 -- recup du type_origine CONVENTION RA--
					 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
					 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

					SELECT COUNT(*) INTO cpt
							FROM accords.CONVENTION_LIMITATIVE
							WHERE org_id = orgid AND exe_ordre = exeordre;

						-- si on a affaire a une convention RA identifiee dans convention
						IF cpt >0 THEN
									-- recup de la convention
									  SELECT MAX(con_ordre) INTO convordre
									 FROM accords.CONVENTION_LIMITATIVE
									 WHERE org_id = orgid
									 AND exe_ordre = exeordre;

									 SELECT SUBSTR(EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET,1,2000)
									 INTO orilibelle
									 FROM accords.contrat
									 WHERE con_ordre = convordre;

						ELSE -- c une convention RA mais pas dans accords
									 SELECT COUNT(*) INTO cpt
									 FROM jefy_admin.organ
									 WHERE org_id = orgid;

									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
											 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;

									 ELSE -- pas d'organ trouvee
									  	 	RETURN NULL;
									 END IF;
							END IF;

		  ELSE
		  -- c pas une convention RA, c peut-etre une ligne lucrative
		  -- a voir s'il ne faut pas la traiter d'une part en tant que convention RA et d'autre par en tant que lucrative si c le cas
							 -- recup du type_origine OPERATION LUCRATIVE --
							 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
							 WHERE top_libelle = 'OPERATION LUCRATIVE';

 		  	   	   	   			  SELECT COUNT(*) INTO cpt
									 FROM jefy_admin.organ
									 WHERE org_id = orgid
									 AND org_lucrativite = 1;

									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
										 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;

									 ELSE -- pas d'organ lucrative trouvee
									  	 	RETURN NULL;
									 END IF;
		  END IF;

		-- l origine est t elle deja  suivie --
		SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'ORG_ID'
		AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
		AND ORI_KEY_ENTITE	=orgid;

		IF cpt >= 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'ORG_ID'
			AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
			AND ORI_KEY_ENTITE	=orgid
			AND ROWNUM=1;

			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

		ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
			INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
			VALUES ('JEFY_ADMIN.ORGAN','ORG_ID',orilibelle,cpt,orgid,topordre);

		END IF;

		RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	IF convordre IS NULL THEN RETURN NULL; END IF;

	-- recup du type_origine --
	SELECT top_ordre INTO topordre FROM TYPE_OPERATION
	WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

	-- l origine est t elle deja  suivie --
	SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;


	-- recup de la convention --
	--le libelle utilisateur pour le suivie en compta --
--	select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
--	SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;
		SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM accords.contrat WHERE con_ordre=convordre;


	-- l'origine est deja referencee
	IF cpt = 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'CONV_ORDRE'
			AND ORI_ENTITE ='CONVENTION.CONTRAT'
			AND ORI_KEY_ENTITE	=convordre;

			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

	-- il faut creer l'origine
	ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
			INSERT INTO ORIGINE VALUES (
					'CONVENTION.CONTRAT' ,--	  USER.TABLE  -
					'CONV_ORDRE',-- 	  PRIMARY KEY -
					ORILIBELLE,-- 	  LIBELLE UTILISATEUR -
					cpt ,--		  ID -
					convordre,--	  VALEUR DE LA KEY -
					topordre--		  type d origine -
				);

	END IF;

	RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
--cpt 	 	INTEGER;

--lebordereau jefy.bordero%ROWTYPE;

--agtordre 	jefy.facture.agt_ordre%TYPE;
--borid 		BORDEREAU.bor_id%TYPE;
--tboordre 	BORDEREAU.tbo_ordre%TYPE;
--utlordre 	UTILISATEUR.utl_ordre%TYPE;
--letitrejefy jefy.TITRE%ROWTYPE;
BEGIN

-- procedure obsolete
--		IF exeordre=2005 THEN
--			SELECT * INTO lebordereau
--			FROM jefy05.bordero
--			WHERE bor_ordre = borordre;

--			-- recup de l agent de ce bordereau --
--			SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
--			   FROM jefy05.TITRE
--			   WHERE bor_ordre =lebordereau.bor_ordre);
--		ELSE
--			SELECT * INTO lebordereau
--			FROM jefy.bordero
--			WHERE bor_ordre = borordre;

--			-- recup de l agent de ce bordereau --
--			SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
--			   FROM jefy.TITRE
--			   WHERE bor_ordre =lebordereau.bor_ordre);
--		END IF;




--		IF letitrejefy.tit_type ='V' THEN
--		   SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
--		ELSE
--		  IF letitrejefy.tit_type ='D' THEN
--		    SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
--		  ELSE
--		    IF letitrejefy.tit_type ='P' THEN
--		      SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
--		    ELSE
--			  SELECT tbo_ordre INTO tboordre
--		      FROM TYPE_BORDEREAU
--		      WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
--		    END IF;
--		  END IF;
--		END IF;


--		RETURN tboordre;
    return 0;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

--cpt 	 	INTEGER;

--lebordereau jefy.bordero%ROWTYPE;
--fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
--agtordre 	jefy.facture.agt_ordre%TYPE;
--fouordre 	jefy.MANDAT.fou_ordre%TYPE;
--borid 		BORDEREAU.bor_id%TYPE;
--tboordre 	BORDEREAU.tbo_ordre%TYPE;
--utlordre 	UTILISATEUR.utl_ordre%TYPE;
--etat		jefy.bordero.bor_stat%TYPE;
BEGIN

--		IF exeordre=2005 THEN
--			-- recup des infos --
--			SELECT * INTO lebordereau
--			FROM jefy05.bordero
--			WHERE bor_ordre = borordre;

--			-- recup de l agent de ce bordereau --
--			SELECT MAX(agt_ordre) INTO agtordre
--			FROM jefy05.facture
--			WHERE man_ordre =
--			 ( SELECT MAX(man_ordre)
--			   FROM jefy05.MANDAT
--			   WHERE bor_ordre =borordre
--			  );

--			SELECT MAX(fou_ordre) INTO fouordre
--			   FROM jefy05.MANDAT
--			   WHERE bor_ordre =borordre;
--		ELSE
--			-- recup des infos --
--			SELECT * INTO lebordereau
--			FROM jefy.bordero
--			WHERE bor_ordre = borordre;

--			-- recup de l agent de ce bordereau --
--			SELECT MAX(agt_ordre) INTO agtordre
--			FROM jefy.facture
--			WHERE man_ordre =
--			 ( SELECT MAX(man_ordre)
--			   FROM jefy.MANDAT
--			   WHERE bor_ordre =borordre
--			  );

--			SELECT MAX(fou_ordre) INTO fouordre
--			   FROM jefy.MANDAT
--			   WHERE bor_ordre =borordre;
--		END IF;

--		-- recuperation du type de bordereau --
--		SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
--		 (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');


--		IF (fouordrepapaye <> 0) THEN
--		   SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
--		   (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
--		ELSE
--			fouordrepapaye := NULL;
--		END IF;



--		IF fouordre =fouordrepapaye THEN
--		  SELECT tbo_ordre INTO tboordre
--		  FROM TYPE_BORDEREAU
--		  WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
--		ELSE
--		  SELECT COUNT(*) INTO cpt
--		  FROM OLD_AGENT_TYPE_BORD
--		  WHERE agt_ordre = agtordre;

--		  IF cpt <> 0 THEN
--		   SELECT tbo_ordre INTO tboordre
--		   FROM OLD_AGENT_TYPE_BORD
--		   WHERE agt_ordre = agtordre;
--		  ELSE
--		   SELECT tbo_ordre INTO tboordre
--		   FROM TYPE_BORDEREAU
--		   WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
--		  END IF;

--		END IF;


--		RETURN tboordre;
    return 0;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
  -- cursor pour recuperer les organ lucratif + conventions RA
	CURSOR c1 IS
	SELECT org_id, exe_ordre FROM v_organ_exer o
	WHERE  exe_ordre>2006 AND (org_lucrativite !=0 OR tyor_id=2);

	CURSOR c2 IS
	SELECT org_id, exe_ordre FROM accords.convention_limitative WHERE org_id>0;
	orgordre INTEGER;
	conordre INTEGER;
	exeordre INTEGER;
	cpt INTEGER;
BEGIN
		-- On balaye les organ lucratives + taggees RA
		OPEN c1;
		LOOP
			FETCH C1 INTO orgordre, exeordre;
				  EXIT WHEN c1%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c1;

		-- on balaye les conventions RA
		OPEN c2;
		LOOP
			FETCH C2 INTO orgordre, exeordre;
				  EXIT WHEN c2%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c2;

-- Les conventions RA sont maintenant gerees a partir d'un Org_id
-- 		OPEN c2;
-- 		LOOP
-- 			FETCH C2 INTO conordre;
-- 				  EXIT WHEN c2%NOTFOUND;
-- 				  cpt := TRAITER_CONVORDRE ( conordre );
-- 		END LOOP;
-- 		CLOSE c2;


END;


END;
/


-----------------------------






