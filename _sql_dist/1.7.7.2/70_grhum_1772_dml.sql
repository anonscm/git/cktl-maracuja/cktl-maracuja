-- nettoyage des users
-- passage par une procedure pour eviter les msg d'erreur si les objets n'existent pas
 
declare 
     v_name varchar2(100);
     v_obj varchar2(100);

begin
		begin
	v_obj := 'MARACUJA.ZLD_CFI_ECRITURES';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
		EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
		EXCEPTION
		WHEN no_data_found THEN
		NULL;
	end;

	begin
	v_obj := 'MARACUJA.ZLD_V_BE_REPRISE_CONSOLIDEE';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
		EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
		EXCEPTION
		WHEN no_data_found THEN
		NULL;	
	end;
	
	begin
	v_obj := 'MARACUJA.ZLD_V_BE_REPRISE_GESTION';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
		EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
		EXCEPTION
		WHEN no_data_found THEN
		NULL;
	end;
	
	begin
	v_obj := 'MARACUJA.ZLD_V_CONVENTION_LIMITATIVE';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
		EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
		EXCEPTION
		WHEN no_data_found THEN
		NULL;	
	end;
	
	begin
	v_obj := 'MARACUJA.ZLD_V_ECRITURE_INFOS';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
		EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
		EXCEPTION
		WHEN no_data_found THEN
		NULL;	
	end;
	
	begin
	v_obj := 'MARACUJA.ZLD_V_JEFY_MULTIEX_BORDERO';
	SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_MANDAT_REIMP';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_ORGAN_EXER';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_RECETTE_RESTE_RECOUVRER';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_RIB';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_TAUX';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_TITRE_REIMP';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_V_TITRE_RESTE_RECOUVRER';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_VCREDIT';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_VCREDIT_BE';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_VDEBIT';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_VDEBIT_BE';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
    begin
    v_obj := 'MARACUJA.ZTMP_ORIGINE_CORRECT';
    SELECT owner ||'.'||view_name INTO v_name FROM all_views WHERE owner ||'.'||view_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop view '|| v_name ||'';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;    
    end;
    
        
    -------------------------
    

    begin
    v_obj := 'MARACUJA.ZLD_EXERCICE';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_AUTORISATION';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_AUTORISATION_GESTION';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_EXERCICE';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_FONCTION';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_PREFERENCE';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_TYPE_CREDIT';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_UTILISATEUR';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_UTILISATEUR_GESTION';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZLD_UTILISATEUR_PREFERENCE';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZOLD_ORIGINE';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.ZTMP_OC';
    SELECT owner ||'.'||table_name INTO v_name FROM all_tables WHERE owner ||'.'||table_name = upper(v_obj);
        EXECUTE IMMEDIATE 'drop table '|| v_name ||' cascade constraints';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    
    
    
    
    
    begin
    v_obj := 'MARACUJA.CORRIGE_ORIGINE_DOUBLONS';
    SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.CORRIGE_ORIGINE_DOUBLONS_SUITE';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    
    begin
    v_obj := 'MARACUJA.CORRIGE_TITRE_TVA';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.MAJ_ORIGINE';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.RECUP_BOURRIN';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.RECUP_BROUILLARDS_PAYES';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.RECUP_DUREE_AMORT';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.RECUP_PARAMETRES2';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.RECUP_PLANCO_AMORT';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.REGUL_ORV_PAPAYE';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.Corrige_Origines_2007';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.Init_Old_Exercice';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.Recup_Droits_Utilisateurs';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.corrige_ecriture_be_SACD';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'MARACUJA.annuler_emargement';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PROCEDURE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop procedure '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    -------
    
    begin
    v_obj := 'maracuja.bordereau_papaye';
    SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);    
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_mandat';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_mandat_non_admis';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_mandat_05';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_titre';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_titre_non_admis';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    
    begin
    v_obj := 'maracuja.bordereau_titre_05';
        SELECT distinct owner ||'.'||name into v_name FROM all_source WHERE type='PACKAGE' and upper(owner ||'.'||name) = upper(v_obj);
        EXECUTE IMMEDIATE 'drop package '|| v_name ||' ';
        EXCEPTION
        WHEN no_data_found THEN
        NULL;
    end;
    

    
end;