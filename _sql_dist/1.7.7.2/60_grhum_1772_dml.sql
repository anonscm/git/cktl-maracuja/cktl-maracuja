-- recuperation des donnees provenant des anciens user JEFY pour archivage dans le user Maracuja
-- Ceci afin de s'affranchir des contraintes de reference vers des users qui ne sont plus 'actifs'
 
declare 
     LC$cmd varchar2(5000);
     flag integer;

begin

	-- recuperation des TITRES
    
    -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_TITRE where exe_exercice=2005;
          LC$cmd := 'insert into maracuja.JEFY_OLD_TITRE  
					SELECT 2005 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
					TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
					AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
					MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '''' AS can_code, -1 AS pres_ordre
					FROM jefy05.TITRE f';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_TITRE where exe_exercice=2006;
          LC$cmd := 'insert into maracuja.JEFY_OLD_TITRE  
                SELECT 2006 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
                TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE,
                TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE,
                TIT_VIREMENT, MOD_CODE,
                DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
                FROM jefy.TITRE f
                WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key=''EXERCICE'')';

        EXECUTE IMMEDIATE LC$cmd;
    end if;

  commit;
    
    
    
    -- recuperation des factures
    
    -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_FACTURE where exe_exercice=2005;
          LC$cmd := 'insert into maracuja.JEFY_OLD_FACTURE  
					SELECT 2005 AS exe_exercice, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
					 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
					 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
					 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
					 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
					 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
					 DEP_BUDNAT FROM jefy05.FACTURE f';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_FACTURE where exe_exercice=2006;
          LC$cmd := 'insert into maracuja.JEFY_OLD_FACTURE  
                SELECT 2006 AS exe_exercice, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
				 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
				 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
				 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
				 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
				 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
				 DEP_BUDNAT FROM jefy.facture f
	   			WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key=''EXERCICE'')';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    commit;
    
    
    
    -- recuperation des destin    
    -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_DESTIN where exe_exercice=2005;
          LC$cmd := 'insert into maracuja.JEFY_OLD_DESTIN  
					SELECT 2005 AS exe_exercice, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 					DST_TYPE, DST_NIV FROM jefy05.destin d';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_DESTIN where exe_exercice=2006;
          LC$cmd := 'insert into maracuja.JEFY_OLD_DESTIN  
                SELECT 2006 AS exe_exercice, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 				DST_TYPE, DST_NIV FROM jefy.destin d
   				WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key=''EXERCICE'')';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    commit;

    
    -- recuperation des destin rec   
    -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_DESTIN_REC where exe_exercice=2005;
          LC$cmd := 'insert into maracuja.JEFY_OLD_DESTIN_REC  
                    SELECT 2005 AS exe_exercice, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
                     DST_TYPE, DST_NIV FROM jefy05.destin_rec d';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_DESTIN_REC where exe_exercice=2006;
          LC$cmd := 'insert into maracuja.JEFY_OLD_DESTIN_REC  
                SELECT 2006 AS exe_exercice, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
                     DST_TYPE, DST_NIV FROM jefy.destin_rec d
                   WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key=''EXERCICE'')';

        EXECUTE IMMEDIATE LC$cmd;
    end if;

  commit;

    
    
    -- recuperation des organ   
    -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_ORGAN where exe_exercice=2005;
          LC$cmd := 'insert into maracuja.JEFY_OLD_ORGAN  
                    SELECT 2005 as exe_exercice, ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
                      ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
                        FROM jefy05.organ';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from maracuja.JEFY_OLD_ORGAN where exe_exercice=2006;
          LC$cmd := 'insert into maracuja.JEFY_OLD_ORGAN  
                SELECT 2006 AS exe_exercice, ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
                  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
                FROM jefy.organ';

        EXECUTE IMMEDIATE LC$cmd;
    end if;

  
    commit;
    -- recuperation des SITUATION
    
        -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from comptefi.JEFY_OLD_SITUATION where exe_exercice=2005;
          LC$cmd := 'insert into comptefi.JEFY_OLD_SITUATION  
                    select 2005, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_code, pco_num, dst_code,
                    eng_lib, eng_ordre, eng_date,eng_ttc, org_ordre, dep_ordre, dep_ttc,
                    man_ordre, sit_exer, man_date, cle_eng, cle_dep, cm_code, rgp_numero, can_code from jefy05.situation';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from comptefi.JEFY_OLD_SITUATION where exe_exercice=2006;
          LC$cmd := 'insert into comptefi.JEFY_OLD_SITUATION  
                select 2006, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_code, pco_num, dst_code,
                eng_lib, eng_ordre, eng_date,eng_ttc, org_ordre, dep_ordre, dep_ttc,
                man_ordre, sit_exer, man_date, cle_eng, cle_dep, cm_code, rgp_numero, can_code from jefy.situation';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    
    
    -- recuperation des budnat
    
        -- verifier si le user JEFY05 est present
    select count(*) into flag from dba_users where username = 'JEFY05';
    if (flag>0) then
        delete from comptefi.JEFY_OLD_BUDNAT where exe_exercice=2005;
          LC$cmd := 'insert into comptefi.JEFY_OLD_BUDNAT  
                    SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
					FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
					WHERE e.EXR_TYPE = ''PRIM''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
					UNION ALL
					SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
					FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
					WHERE e.EXR_TYPE = ''PRIM''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
					UNION ALL
					SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
					FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
					WHERE e.EXR_TYPE = ''RELI''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
					UNION ALL
					SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
					FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
					WHERE e.EXR_TYPE = ''RELI''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
					UNION ALL
					SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
					SUM (bdn_dbm)
					FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
					WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = ''DBM''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
					UNION ALL
					SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
					SUM (bdn_dbm)
					FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
					WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = ''DBM''
					AND b.org_ordre = o.org_ordre
					AND o.org_niv = 2
					AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
					GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    -- verifier si le user JEFY est present
    select count(*) into flag from dba_users where username = 'JEFY';
    if (flag>0) then
        delete from comptefi.JEFY_OLD_BUDNAT where exe_exercice=2006;
          LC$cmd := 'insert into comptefi.JEFY_OLD_BUDNAT  
                SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
				FROM jefy.budnat b, jefy.exer e, jefy.organ o
				WHERE e.EXR_TYPE = ''PRIM''
				AND b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
				UNION ALL
				SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
				FROM jefy.budnat b, jefy.exer e, jefy.organ o
				WHERE e.EXR_TYPE = ''PRIM''
				AND b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
				UNION ALL
				SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE(''01/01/2006'',''DD/MM/YYYY'') exr_cloture, SUM (bdn_reliq)
				FROM jefy.budnat b, jefy.organ o
				WHERE b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE(''01/01/2006'',''DD/MM/YYYY'')
				UNION ALL
				SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE(''01/01/2006'',''DD/MM/YYYY'') exr_cloture, SUM (bdn_reliq)
				FROM jefy.budnat b, jefy.organ o
				WHERE b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE(''01/01/2006'',''DD/MM/YYYY'')
				UNION ALL
				SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
				SUM (bdn_dbm)
				FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
				WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = ''DBM''
				AND b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''6%'' OR PCO_NUM LIKE ''7%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
				UNION ALL
				SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
				SUM (bdn_dbm)
				FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
				WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = ''DBM''
				AND b.org_ordre = o.org_ordre
				AND o.org_niv = 2
				AND (PCO_NUM LIKE ''1%'' OR PCO_NUM LIKE ''2%'' )
				GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture';

        EXECUTE IMMEDIATE LC$cmd;
    end if;
    
    commit;
end;