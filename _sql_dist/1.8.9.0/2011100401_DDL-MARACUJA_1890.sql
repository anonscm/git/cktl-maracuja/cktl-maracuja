SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.9.0
-- Date de publication : 04/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- PRE-REQUIS : Patch du 03/10/2011 pour GRHUM version 1.7.0.0
-- Ajout d'un parametre pour modifie les dates calculees de premiere echeance pour les echeanciers des frais d'inscriptions (scolarix)
-- Modification des vues pointant vers les ribs pour faire référence aux code guichet, banque et bic de la banque
-- mise à jour du plan comptable de reference
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.9.0',  null, '');
commit;


CREATE OR REPLACE FORCE VIEW maracuja.v_rib (rib_ordre,
                                             fou_ordre,
                                             rib_bic,
                                             rib_cle,
                                             rib_codbanc,
                                             rib_guich,
                                             rib_iban,
                                             rib_domicil,
                                             rib_num,
                                             rib_titco,
                                             rib_valide,
                                             pers_id_creation,
                                             pers_id_modification
                                            )
AS
   SELECT r.rib_ordre, r.fou_ordre, b.bic, r.cle_rib, b.c_banque, b.c_guichet,
          r.iban, b.domiciliation, r.no_compte, r.rib_titco, r.rib_valide,
          r.pers_id_creation, r.pers_id_modification
     FROM grhum.ribfour_ulr r, grhum.banque b
    WHERE r.banq_ordre = b.banq_ordre;
/



CREATE OR REPLACE FORCE VIEW maracuja.v_abricot_depense_a_mandater (c_banque,
                                                                    c_guichet,
                                                                    no_compte,
                                                                    iban,
                                                                    bic,
                                                                    cle_rib,
                                                                    domiciliation,
                                                                    mod_libelle,
                                                                    mod_code,
                                                                    mod_dom,
                                                                    pers_type,
                                                                    pers_libelle,
                                                                    pers_lc,
                                                                    exe_exercice,
                                                                    org_id,
                                                                    org_ub,
                                                                    org_cr,
                                                                    org_souscr,
                                                                    tcd_ordre,
                                                                    tcd_code,
                                                                    tcd_libelle,
                                                                    dpp_numero_facture,
                                                                    dpp_id,
                                                                    dpp_ht_saisie,
                                                                    dpp_tva_saisie,
                                                                    dpp_ttc_saisie,
                                                                    dpp_date_facture,
                                                                    dpp_date_saisie,
                                                                    dpp_date_reception,
                                                                    dpp_date_service_fait,
                                                                    dpp_nb_piece,
                                                                    utl_ordre,
                                                                    tbo_ordre,
                                                                    dep_id,
                                                                    pco_num,
                                                                    dpco_id,
                                                                    dpco_ht_saisie,
                                                                    dpco_tva_saisie,
                                                                    dpco_ttc_saisie,
                                                                    adr_civilite,
                                                                    adr_nom,
                                                                    adr_prenom,
                                                                    adr_adresse1,
                                                                    adr_adresse2,
                                                                    adr_ville,
                                                                    adr_cp,
                                                                    utlnomprenom
                                                                   )
AS
   SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic, r.cle_rib,
          bq.domiciliation, mp.mod_libelle, mp.mod_code, mp.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, eb.org_id,
          o.org_ub, o.org_cr, o.org_souscr, eb.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, dp.dpp_numero_facture, dp.dpp_id, dp.dpp_ht_saisie,
          dp.dpp_tva_saisie, dp.dpp_ttc_saisie, dp.dpp_date_facture,
          dp.dpp_date_saisie, dp.dpp_date_reception, dp.dpp_date_service_fait,
          dp.dpp_nb_piece, dp.utl_ordre, dcp.tbo_ordre, dcp.dep_id dep_id,
          dcp.pco_num, dcp.dpco_id dpco_id, dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie, dcp.dpco_ttc_saisie, f.adr_civilite, f.adr_nom,
          f.adr_prenom, f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o                                               --,
--jefy_admin.type_credit tcd
   WHERE  eb.eng_id = db.eng_id
      AND db.dpp_id = dp.dpp_id
      AND db.dep_id = dcp.dep_id
      AND dp.fou_ordre = f.fou_ordre
      AND dp.exe_ordre = e.exe_ordre
      AND dp.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND dp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mp.mod_ordre = dp.mod_ordre
      AND eb.tcd_ordre = tc.tcd_ordre
      AND o.org_id = eb.org_id
      AND u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
      AND man_id IS NULL
      AND tbo_ordre != 201;


GRANT SELECT ON MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER TO JEFY_DEPENSE;
/


CREATE OR REPLACE FORCE VIEW maracuja.v_abricot_depense_pi (c_banque,
                                                            c_guichet,
                                                            no_compte,
                                                            iban,
                                                            bic,
                                                            domiciliation,
                                                            mod_libelle,
                                                            mod_code,
                                                            mod_dom,
                                                            pers_type,
                                                            pers_libelle,
                                                            pers_lc,
                                                            exe_exercice,
                                                            org_id,
                                                            org_ub,
                                                            org_cr,
                                                            org_souscr,
                                                            tcd_ordre,
                                                            tcd_code,
                                                            tcd_libelle,
                                                            dpp_numero_facture,
                                                            dpp_id,
                                                            dpp_ht_saisie,
                                                            dpp_tva_saisie,
                                                            dpp_ttc_saisie,
                                                            dpp_date_facture,
                                                            dpp_date_saisie,
                                                            dpp_date_reception,
                                                            dpp_date_service_fait,
                                                            dpp_nb_piece,
                                                            utl_ordre,
                                                            tbo_ordre,
                                                            dep_id,
                                                            pco_num,
                                                            dpco_id,
                                                            dpco_ht_saisie,
                                                            dpco_tva_saisie,
                                                            dpco_ttc_saisie,
                                                            adr_civilite,
                                                            adr_nom,
                                                            adr_prenom,
                                                            adr_adresse1,
                                                            adr_adresse2,
                                                            adr_ville,
                                                            adr_cp,
                                                            utlnomprenom
                                                           )
AS
   SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic,
          bq.domiciliation, mp.mod_libelle, mp.mod_code, mp.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, eb.org_id,
          o.org_ub, o.org_cr, o.org_souscr, eb.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, dp.dpp_numero_facture, dp.dpp_id, dp.dpp_ht_saisie,
          dp.dpp_tva_saisie, dp.dpp_ttc_saisie, dp.dpp_date_facture,
          dp.dpp_date_saisie, dp.dpp_date_reception, dp.dpp_date_service_fait,
          dp.dpp_nb_piece, dp.utl_ordre, dcp.tbo_ordre, dcp.dep_id dep_id,
          dcp.pco_num, dcp.dpco_id, dcp.dpco_ht_saisie, dcp.dpco_tva_saisie,
          dcp.dpco_ttc_saisie, f.adr_civilite, f.adr_nom, f.adr_prenom,
          f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o                                               --,
--jefy_admin.type_credit tcd
   WHERE  eb.eng_id = db.eng_id
      AND db.dpp_id = dp.dpp_id
      AND db.dep_id = dcp.dep_id
      AND dp.fou_ordre = f.fou_ordre
      AND dp.exe_ordre = e.exe_ordre
      AND dp.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND dp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mp.mod_ordre = dp.mod_ordre
      AND eb.tcd_ordre = tc.tcd_ordre
      AND o.org_id = eb.org_id
      AND u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
      AND man_id IS NULL
      AND tbo_ordre = 201;
/

CREATE OR REPLACE FORCE VIEW maracuja.v_abricot_recette_a_titrer (c_banque,
                                                                  c_guichet,
                                                                  no_compte,
                                                                  iban,
                                                                  bic,
                                                                  domiciliation,
                                                                  mod_libelle,
                                                                  mod_code,
                                                                  mod_dom,
                                                                  pers_type,
                                                                  pers_libelle,
                                                                  pers_lc,
                                                                  exe_exercice,
                                                                  org_id,
                                                                  org_ub,
                                                                  org_cr,
                                                                  org_souscr,
                                                                  tcd_ordre,
                                                                  tcd_code,
                                                                  tcd_libelle,
                                                                  fac_numero,
                                                                  rec_numero,
                                                                  fap_numero,
                                                                  rec_id,
                                                                  rec_ht_saisie,
                                                                  rec_tva_saisie,
                                                                  rec_ttc_saisie,
                                                                  fac_date_saisie,
                                                                  rec_nb_piece,
                                                                  utl_ordre,
                                                                  tbo_ordre,
                                                                  pco_num,
                                                                  rpco_id,
                                                                  rpco_ht_saisie,
                                                                  tva,
                                                                  ttc,
                                                                  adr_civilite,
                                                                  adr_nom,
                                                                  adr_prenom,
                                                                  adr_adresse1,
                                                                  adr_adresse2,
                                                                  adr_ville,
                                                                  adr_cp,
                                                                  utlnomprenom
                                                                 )
AS
   SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic,
          bq.domiciliation, mr.mod_libelle, mr.mod_code, mr.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, fac.org_id,
          o.org_ub, o.org_cr, o.org_souscr, fac.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, fac.fac_numero, jrec.rec_numero, fp.fap_numero,
          rec.rec_id, rec.rec_ht_saisie, rec.rec_tva_saisie,
          rec.rec_ttc_saisie, fac.fac_date_saisie, 0 rec_nb_piece,
          rec.utl_ordre, rcp.tbo_ordre, rcp.pco_num, rcp.rpco_id,
          rcp.rpco_ht_saisie, rcp.rpco_tva_saisie tva,
          rcp.rpco_ttc_saisie ttc, f.adr_civilite, f.adr_nom, f.adr_prenom,
          f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_recette.facture fac,
          jefy_recette.facture_papier fp,
          jefy_recette.recette jrec,
          jefy_recette.recette_budget rec,
          jefy_recette.recette_papier recp,
          jefy_recette.recette_ctrl_planco rcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_recouvrement mr,
          jefy_admin.type_credit tc,
          jefy_admin.organ o
    WHERE fac.fac_id = rec.fac_id
      AND rec.rpp_id = recp.rpp_id
      AND rec.rec_id = rcp.rec_id
      AND rec.rec_id = jrec.rec_id
      AND fac.fou_ordre = f.fou_ordre
      AND rec.exe_ordre = e.exe_ordre
      AND fac.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND recp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mr.mod_ordre = recp.mor_ordre
      AND fac.tcd_ordre = tc.tcd_ordre
      AND o.org_id = fac.org_id
      AND u.pers_id = pu.pers_id
      AND rcp.tit_id IS NULL
      AND tbo_ordre != 200
      AND fac.fac_id = fp.fac_id(+);
/


CREATE OR REPLACE FORCE VIEW maracuja.v_abricot_recette_pi (c_banque,
                                                            c_guichet,
                                                            no_compte,
                                                            iban,
                                                            bic,
                                                            domiciliation,
                                                            mod_libelle,
                                                            mod_code,
                                                            mod_dom,
                                                            pers_type,
                                                            pers_libelle,
                                                            pers_lc,
                                                            exe_exercice,
                                                            org_id,
                                                            org_ub,
                                                            org_cr,
                                                            org_souscr,
                                                            tcd_ordre,
                                                            tcd_code,
                                                            tcd_libelle,
                                                            fac_numero,
                                                            rec_id,
                                                            rec_ht_saisie,
                                                            rec_tva_saisie,
                                                            rec_ttc_saisie,
                                                            fac_date_saisie,
                                                            rec_nb_piece,
                                                            utl_ordre,
                                                            tbo_ordre,
                                                            pco_num,
                                                            rpco_ht_saisie,
                                                            rpco_id,
                                                            tva,
                                                            ttc,
                                                            adr_civilite,
                                                            adr_nom,
                                                            adr_prenom,
                                                            adr_adresse1,
                                                            adr_adresse2,
                                                            adr_ville,
                                                            adr_cp,
                                                            utlnomprenom
                                                           )
AS
   SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic,
          bq.domiciliation, mr.mod_libelle, mr.mod_code, mr.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, fac.org_id,
          o.org_ub, o.org_cr, o.org_souscr, fac.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, fac.fac_numero, rec.rec_id, rec.rec_ht_saisie,
          rec.rec_tva_saisie, rec.rec_ttc_saisie, fac.fac_date_saisie,
          0 rec_nb_piece, rec.utl_ordre, rcp.tbo_ordre,
--rcp.rec_id+tcd.tcd_ordre rec_id,
                                                       rcp.pco_num,
          rcp.rpco_ht_saisie, rcp.rpco_id, rcp.rpco_tva_saisie tva,
          rcp.rpco_ttc_saisie ttc, f.adr_civilite, f.adr_nom, f.adr_prenom,
          f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_recette.facture fac,
          jefy_recette.recette_budget rec,
          jefy_recette.recette_papier recp,
          jefy_recette.recette_ctrl_planco rcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_recouvrement mr,
          jefy_admin.type_credit tc,
          jefy_admin.organ o
    WHERE fac.fac_id = rec.fac_id
      AND rec.rpp_id = recp.rpp_id
      AND rec.rec_id = rcp.rec_id
      AND fac.fou_ordre = f.fou_ordre
      AND rec.exe_ordre = e.exe_ordre
      AND fac.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND recp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mr.mod_ordre = recp.mor_ordre
      AND fac.tcd_ordre = tc.tcd_ordre
      AND o.org_id = fac.org_id
      AND u.pers_id = pu.pers_id
      AND rcp.tit_id IS NULL
      AND tbo_ordre = 200;
/







create procedure grhum.inst_patch_maracuja_1890 is
begin
	update jefy_admin.fonction set fon_spec_gestion='O' where fon_id_interne ='IMPR013' and tyap_id=4;

	
-- mise à jour du plan comptable de reference

delete from maracuja.plan_comptable_ref_ext where ref_pco_num in (
103121,
1031211,
1031212,
1031213,
103122,
10314,
10315,
10316,
10317,
103179,
105,
1051,
1054,
4443,
4449,
4456711,
4456712,
4456721,
4456722,
4456731,
4456732,
6788
) and ref_pco_import_id='M9-3 (juillet 2011)';

update  maracuja.plan_comptable_ref_ext set ref_pco_import_id='M9-3 (octobre 2011)' where  ref_pco_import_id='M9-3 (juillet 2011)';
	
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Fonds propres et autres fonds' where ref_pco_num='10312' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Provisions pour allocation perte d''emploi et d''indemnités de licenciement' where ref_pco_num='1587' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Autres emprunts' where ref_pco_num='1681' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Salaires, appointements' where ref_pco_num='6411' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Cotisations aux caisses de retraites, aux régimes de pensions civiles et militaires et CNRACL' where ref_pco_num='6453' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Cotisations d''assurance vieillesse - agents non titulaires' where ref_pco_num='64534' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'AIT - Allocation d''invalidité temporaire' where ref_pco_num='64712' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'Médecine du travail, pharmacie' where ref_pco_num='6475' and ref_pco_import_id='M9-3 (octobre 2011)';
update maracuja.plan_comptable_ref_ext set ref_pco_libelle = 'BA immobilier' where ref_pco_num='1855' and ref_pco_import_id='M9-3 (octobre 2011)';



    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.9.0';           
end;
/






