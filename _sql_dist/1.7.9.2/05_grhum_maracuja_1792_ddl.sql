SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_MANDATS_DEBITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 MAN_ID, MAN_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 MAN_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, MDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
      m.man_id, m.man_numero, ed.ecd_libelle, ecd_montant,
      (SIGN (ecd_montant)) * m.man_tva tva, m.man_etat, m.fou_ordre,
      ed.ecr_ordre, ei.ecr_sacd, mde_origine
 FROM maracuja.mandat m,
        maracuja.bordereau b,
      maracuja.type_bordereau tb,
      maracuja.mandat_detail_ecriture mde,
      maracuja.ecriture_detail ed,
      maracuja.v_ecriture_infos ei,
      maracuja.ecriture e
WHERE b.bor_id=m.bor_id
  AND  m.man_id = mde.man_id
  and mde.exe_ordre=m.exe_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  and b.TBO_ORDRE = tb.TBO_ORDRE
  AND mde_origine IN ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  AND ecd_debit <> 0
  AND tb.tbo_sous_type<> 'REVERSEMENTS'
  AND tb.tbo_sous_type<> 'SCOLARITE'
  AND ABS (ecd_montant) = ABS (man_ht)   
  and m.ges_code=ed.ges_code;
/

SET DEFINE OFF;
CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_CPTE_RTAT (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);
  benef NUMBER(12,2);
  perte NUMBER(12,2);
  benefant NUMBER(12,2);
  perteant NUMBER(12,2);

  -- version du 03/03/2006 - Prestations Internes

BEGIN

--*************** CHARGES *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = gescode;
--ELSE
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

IF sacd = 'O' THEN
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


benef := 0;
perte := 0;
benefant := 0;
perteant := 0;

lib1 := 'I';

----- CHARGES EXPLOITATION ----
lib2:= 'CHARGES D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte(exeordre, '607%', gescode, sacd)
        + resultat_compte(exeordre, '6087%', gescode, sacd)
        + resultat_compte(exeordre, '6097%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '607%', gescode, sacd)
        + resultat_compte(exeordre-1, '6087%', gescode, sacd)
        + resultat_compte(exeordre-1, '6097%', gescode, sacd);
    lib := 'Achats de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Variation marchandises ---
    total := resultat_compte(exeordre, '6037%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6037%', gescode, sacd);
    lib := 'Variation de stocks de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CONSOMMATION EXERCICE ----
lib2:= 'CONSOMMATION DE L''EXERCICE EN PROVENANCE DES TIERS';

-- Achats mati�res 1�res ---
    total := resultat_compte(exeordre, '601%', gescode, sacd)
        + resultat_compte(exeordre, '6081%', gescode, sacd)
        + resultat_compte(exeordre, '6091%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '601%', gescode, sacd)
        + resultat_compte(exeordre-1, '6081%', gescode, sacd)
        + resultat_compte(exeordre-1, '6091%', gescode, sacd);
    lib := 'Achats stock�s - Mati�res premi�res';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats approvisionnements ---
    total := resultat_compte(exeordre, '602%', gescode, sacd)
        + resultat_compte(exeordre, '6082%', gescode, sacd)
        + resultat_compte(exeordre, '6092%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '602%', gescode, sacd)
        + resultat_compte(exeordre-1, '6082%', gescode, sacd)
        + resultat_compte(exeordre-1, '6092%', gescode, sacd);
    lib := 'Achats stock�s - Autres approvisionnements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- variation ---
    total := resultat_compte(exeordre, '6031%', gescode, sacd)
        + resultat_compte(exeordre, '6032%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6031%', gescode, sacd)
        + resultat_compte(exeordre-1, '6032%', gescode, sacd);
    lib := 'Variation de stocks d''approvisionnement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats sous-traitance ---
    total := resultat_compte(exeordre, '604%', gescode, sacd)
        + resultat_compte(exeordre, '6084%', gescode, sacd)
        + resultat_compte(exeordre, '6094%', gescode, sacd)
        + resultat_compte(exeordre, '605%', gescode, sacd)
        + resultat_compte(exeordre, '6085%', gescode, sacd)
        + resultat_compte(exeordre, '6095%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '604%', gescode, sacd)
        + resultat_compte(exeordre-1, '6084%', gescode, sacd)
        + resultat_compte(exeordre-1, '6094%', gescode, sacd)
        + resultat_compte(exeordre-1, '605%', gescode, sacd)
        + resultat_compte(exeordre-1, '6085%', gescode, sacd)
        + resultat_compte(exeordre-1, '6095%', gescode, sacd);
    lib := 'Achats de sous-traitance';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats mat�riel et fournitures ---
    total := resultat_compte(exeordre, '606%', gescode, sacd)
        + resultat_compte(exeordre, '6086%', gescode, sacd)
        + resultat_compte(exeordre, '6096%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '606%', gescode, sacd)
        + resultat_compte(exeordre-1, '6086%', gescode, sacd)
        + resultat_compte(exeordre-1, '6096%', gescode, sacd);
    lib := 'Achats non stock�s de mati�res et de fournitures';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces ext�rieurs  Prsel int�rim ---
    total := resultat_compte(exeordre, '6211%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6211%', gescode, sacd);
    lib := 'Services ext�rieurs : Personnel int�rimaire';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces ext�rieurs loyers cr�dit-bail ---
    total := resultat_compte(exeordre, '612%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '612%', gescode, sacd);
    lib := 'Services ext�rieurs : Loyers en cr�dit-bail';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres Sces ext�rieurs ---
    total := resultat_compte(exeordre, '61%', gescode, sacd)
        + resultat_compte(exeordre, '62%', gescode, sacd)
        -(resultat_compte(exeordre, '612%', gescode, sacd)
        + resultat_compte(exeordre, '6211%', gescode, sacd))
        + resultat_compte(exeordre, '619%', gescode, sacd)
        + resultat_compte(exeordre, '629%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '61%', gescode, sacd)
        + resultat_compte(exeordre-1, '62%', gescode, sacd)
        -(resultat_compte(exeordre-1, '612%', gescode, sacd)
        + resultat_compte(exeordre-1, '6211%', gescode, sacd))
        + resultat_compte(exeordre-1, '619%', gescode, sacd)
        + resultat_compte(exeordre-1, '629%', gescode, sacd);
    lib := 'Autres services ext�rieurs';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- IMPOTS----
lib2:= 'IMPOTS, TAXES ET VERSEMENTS ASSIMILES';

-- Sur r�mun�rations ---
    total := resultat_compte(exeordre, '631%', gescode, sacd)
        + resultat_compte(exeordre, '632%', gescode, sacd)
        + resultat_compte(exeordre, '633%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '631%', gescode, sacd)
        + resultat_compte(exeordre-1, '632%', gescode, sacd)
        + resultat_compte(exeordre-1, '633%', gescode, sacd);
    lib := 'Sur r�mun�rations';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres ---
    total := resultat_compte(exeordre, '635%', gescode, sacd)
        + resultat_compte(exeordre, '637%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '635%', gescode, sacd)
        + resultat_compte(exeordre-1, '637%', gescode, sacd);
    lib := 'Autres imp�ts, taxes et versements assimil�s';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CHARGES PERSONNEL ----
lib2:= 'CHARGES DE PERSONNEL';

-- Salaire ---
    total := resultat_compte(exeordre, '641%', gescode, sacd)
        + resultat_compte(exeordre, '642%', gescode, sacd)
        + resultat_compte(exeordre, '643%', gescode, sacd)
        + resultat_compte(exeordre, '644%', gescode, sacd)
        + resultat_compte(exeordre, '646%', gescode, sacd)
        + resultat_compte(exeordre, '648%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '641%', gescode, sacd)
        + resultat_compte(exeordre-1, '642%', gescode, sacd)
        + resultat_compte(exeordre-1, '643%', gescode, sacd)
        + resultat_compte(exeordre-1, '644%', gescode, sacd)
        + resultat_compte(exeordre-1, '646%', gescode, sacd)
        + resultat_compte(exeordre-1, '648%', gescode, sacd);
    lib := 'Salaires et traitements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sociales ---
    total := resultat_compte(exeordre, '645%', gescode, sacd)
        + resultat_compte(exeordre, '647%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '645%', gescode, sacd)
        + resultat_compte(exeordre-1, '647%', gescode, sacd);
    lib := 'Charges sociales';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- AMORTISSEMENTS ET PROVISIONS ----
lib2:= 'DOTATIONS AUX AMORTISSEMENTS ET AUX PROVISIONS';

-- Amort sur Immobilisations ---
    total := resultat_compte(exeordre, '6811%', gescode, sacd)
        + resultat_compte(exeordre, '6812%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6811%', gescode, sacd)
        + resultat_compte(exeordre-1, '6812%', gescode, sacd);
    lib := 'Sur immobilisations : Dotations aux amortissements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Immobilisations ---
    total := resultat_compte(exeordre, '6816%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6816%', gescode, sacd);
    lib := 'Sur immobilisations : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Actifs circulants ---
    total := resultat_compte(exeordre, '6817%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6817%', gescode, sacd);
    lib := 'Sur actif circulant : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov pour Risques et Charges ---
    total := resultat_compte(exeordre, '6815%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6815%', gescode, sacd);
    lib := 'Pour risques et charges : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- AUTRES CHARGES ----
lib2:= 'AUTRES CHARGES';

-- Autres charges ---
    total := resultat_compte(exeordre, '65%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '65%', gescode, sacd);
    lib := 'Autres charges de gestion courante' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations internes ---
    total := resultat_compte(exeordre, '186%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '186%', gescode, sacd);
    lib := 'Charges de Prestations Internes' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);


--********************************************
lib1 := 'II';

---- CHARGES FINANCIERES----
lib2:= 'CHARGES FINANCIERES';

-- Amort et provisions ---
    total := resultat_compte(exeordre, '686%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '686%', gescode, sacd);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Int�r�ts ---
    total := resultat_compte(exeordre, '661%', gescode, sacd)
        + resultat_compte(exeordre, '664%', gescode, sacd)
        + resultat_compte(exeordre, '665%', gescode, sacd)
        + resultat_compte(exeordre, '668%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '661%', gescode, sacd)
        + resultat_compte(exeordre-1, '664%', gescode, sacd)
        + resultat_compte(exeordre-1, '665%', gescode, sacd)
        + resultat_compte(exeordre-1, '668%', gescode, sacd);
    lib := 'Int�r�ts et charges assimil�es';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Perte de change---
    total := resultat_compte(exeordre, '666%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '666%', gescode, sacd);
    lib := 'Diff�rence n�gative de change';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sur cession VMP ---
    total := resultat_compte(exeordre, '667%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '667%', gescode, sacd);
    lib := 'Charges nettes sur cessions de valeurs mobili�res de placement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- CHARGES EXCEPTIONNELLES ----
lib2:= 'CHARGES EXCEPTIONNELLES';

-- Op�rations de gestion ---
    total := resultat_compte(exeordre, '671%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '671%', gescode, sacd);
    lib := 'Sur op�ration de gestion';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op�rations en capital VC �lement actifs c�d�s ---
    total := resultat_compte(exeordre, '675%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
    lib := 'Sur op�ration en capital : Valeur comptable des �l�ments d''actif c�d�s';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres Op�rations ---
    total := resultat_compte(exeordre, '678%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '678%', gescode, sacd);
    lib := 'Sur autres op�rations en capital';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et Provisions ---
    total := resultat_compte(exeordre, '687%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '687%', gescode, sacd);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- IMPOTS SUR BENEF----
lib2:= 'IMPOTS SUR LES BENEFICES';

-- impots sur les b�n�fices ---
    total := resultat_compte(exeordre, '69%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '69%', gescode, sacd);
    lib := '  ';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- B�n�fice ou Perte --
lib1 := 'III';
lib2 := '  ';
    IF sacd = 'O' THEN
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre-1;
    ELSif sacd = 'G' then
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre-1;        
    ELSE
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre-1;
    END IF;
    IF total >= 0 THEN
        benef := total;
        ELSE perte := -total;
    END IF;
    IF totalant >= 0 THEN
        benefant := totalant;
        ELSE perteant := -totalant;
    END IF;
    lib := 'SOLDE CREDITEUR = BENEFICE';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, benef, benefant, gescode, exeordre);


--*************** PRODUITS *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = gescode;
--ELSE
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

IF sacd = 'O' THEN
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;

lib1 := 'I';

----- PRODUITS EXPLOITATION ----
lib2:= 'PRODUITS D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte(exeordre, '707%', gescode, sacd)
        + resultat_compte(exeordre, '7097%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '707%', gescode, sacd)
        + resultat_compte(exeordre-1, '7097%', gescode, sacd);
    lib := 'Vente de marchandises';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Ventes ---
    total := resultat_compte(exeordre, '701%', gescode, sacd)
        + resultat_compte(exeordre, '702%', gescode, sacd)
        + resultat_compte(exeordre, '703%', gescode, sacd)
        + resultat_compte(exeordre, '7091%', gescode, sacd)
        + resultat_compte(exeordre, '7092%', gescode, sacd)
        + resultat_compte(exeordre, '7093%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '701%', gescode, sacd)
        + resultat_compte(exeordre-1, '702%', gescode, sacd)
        + resultat_compte(exeordre-1, '703%', gescode, sacd)
        + resultat_compte(exeordre-1, '7091%', gescode, sacd)
        + resultat_compte(exeordre-1, '7092%', gescode, sacd)
        + resultat_compte(exeordre-1, '7093%', gescode, sacd);
    lib := 'Production vendue : Ventes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Travaux ---
    total := resultat_compte(exeordre, '704%', gescode, sacd)
        + resultat_compte(exeordre, '7094%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '704%', gescode, sacd)
        + resultat_compte(exeordre-1, '7094%', gescode, sacd);
    lib := 'Production vendue : Travaux';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations ---
    total := resultat_compte(exeordre, '705%', gescode, sacd)
        + resultat_compte(exeordre, '706%', gescode, sacd)
        + resultat_compte(exeordre, '708%', gescode, sacd)
        + resultat_compte(exeordre, '7095%', gescode, sacd)
        + resultat_compte(exeordre, '7096%', gescode, sacd)
        + resultat_compte(exeordre, '7098%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '705%', gescode, sacd)
        + resultat_compte(exeordre-1, '706%', gescode, sacd)
        + resultat_compte(exeordre-1, '708%', gescode, sacd)
        + resultat_compte(exeordre-1, '7095%', gescode, sacd)
        + resultat_compte(exeordre-1, '7096%', gescode, sacd)
        + resultat_compte(exeordre-1, '7098%', gescode, sacd);
    lib := 'Production vendue : Prestations de services, �tudes et activit�s annexes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Biens en-cours ---
    total := resultat_compte(exeordre, '7133%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7133%', gescode, sacd);
    lib := 'Production stock�e : En-cours de production de biens';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Services en-cours ---
    total := resultat_compte(exeordre, '7134%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7134%', gescode, sacd);
    lib := 'Production stock�e : En-cours de production de services';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Produits ---
    total := resultat_compte(exeordre, '7135%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7135%', gescode, sacd);
    lib := 'Production stock�e : Produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Production Immobilis�e ---
    total := resultat_compte(exeordre, '72%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '72%', gescode, sacd);
    lib := 'Production immobilis�e';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Subventions exploitation ---
    total := resultat_compte(exeordre, '74%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '74%', gescode, sacd);
    lib := 'Subventions d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et provisions ---
    total := resultat_compte(exeordre, '781%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '781%', gescode, sacd);
    lib := 'Reprise sur amortissements et sur provisions';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Transfert de charges  ---
    total := resultat_compte(exeordre, '791%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '791%', gescode, sacd);
    lib := 'Transferts de charges d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres produits  ---
    total := resultat_compte(exeordre, '75%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '75%', gescode, sacd);
    lib := 'Autres produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Prestations internes ---
    total := resultat_compte(exeordre, '187%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '187%', gescode, sacd);
    lib := 'Produits de Prestations internes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);


lib1 := 'II';

----- PRODUITS FINANCIERS ----
lib2:= 'PRODUITS FINANCIERS';

-- Participations ---
    total := resultat_compte(exeordre, '761%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '761%', gescode, sacd);
    lib := 'De participation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres VM et cr�ances immobilis�es ---
    total := resultat_compte(exeordre, '762%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '762%', gescode, sacd);
    lib := 'D''autres valeurs mobili�res et cr�ances de l''actif immobilis�';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres produits ---
    total := resultat_compte(exeordre, '763%', gescode, sacd)
        + resultat_compte(exeordre, '764%', gescode, sacd)
        + resultat_compte(exeordre, '765%', gescode, sacd)
        + resultat_compte(exeordre, '768%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '763%', gescode, sacd)
        + resultat_compte(exeordre-1, '764%', gescode, sacd)
        + resultat_compte(exeordre-1, '765%', gescode, sacd)
        + resultat_compte(exeordre-1, '768%', gescode, sacd);
    lib := 'Autres int�r�ts et produits assimil�s';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte(exeordre, '786%', gescode, sacd)
        + resultat_compte(exeordre, '796%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '786%', gescode, sacd)
        + resultat_compte(exeordre-1, '796%', gescode, sacd);
    lib := 'Reprise sur provisions et transferts de charges financi�res';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Gain de change ---
    total := resultat_compte(exeordre, '766%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '766%', gescode, sacd);
    lib := 'Diff�rence positive de change';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Pduits sur cession VMP ---
    total := resultat_compte(exeordre, '767%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '767%', gescode, sacd);
    lib := 'Produits nets sur cession de valeurs mobili�res de placement';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- PRODUITS EXCEPTIONNELS ----
lib2:= 'PRODUITS EXCEPTIONNELS';

-- Op�ration de gestion ---
    total := resultat_compte(exeordre, '771%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '771%', gescode, sacd);
    lib := 'Sur op�ration de gestion';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital cession �l�m�nts actifs ---
    total := resultat_compte(exeordre, '775%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
    lib := 'Sur op�ration en capital : Produits des cessions d''�lements d''actif';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital subvention exploitation ---
    total := resultat_compte(exeordre, '777%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
    lib := 'Sur op�ration en capital : Subventions d''investissement vir�es au r�sultat';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital AUTRES ---
    total := resultat_compte(exeordre, '778%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '778%', gescode, sacd);
    lib := 'Sur autres op�rations en capital';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Neutralisation des amortissements ---
    total := resultat_compte(exeordre, '776%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
    lib := 'Neutralisation des amortissements';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte(exeordre, '787%', gescode, sacd)
        + resultat_compte(exeordre, '797%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '787%', gescode, sacd)
        + resultat_compte(exeordre-1, '797%', gescode, sacd);
    lib := 'Reprise sur provisions et transferts de charges exceptionnelles';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- B�n�fice ou Perte --
lib1 := 'III';
lib2 := '  ';
lib := 'SOLDE DEBITEUR = PERTE';
INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, perte, perteant, gescode, exeordre);

END;
/





CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_DETERMINATION_CAF" (exeordre NUMBER, gescode VARCHAR2, sacd CHAR, methodeEBE VARCHAR2)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  libant VARCHAR2(50);
  total_produits NUMBER(12,2);
  total_produits_ant NUMBER(12,2);
  total_charges NUMBER(12,2);
  total_charges_ant NUMBER(12,2);
  formule VARCHAR2(50);
  ebe NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat NUMBER(12,2);
  resultat_ant NUMBER(12,2);
  cpt NUMBER;

  -- version du 14/03/2007

BEGIN

--*************** DETERMINATION A PARTIR DE EBE  *********************************
IF methodeEBE = 'O' THEN

    IF sacd = 'O' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'O';
    ELSIF sacd = 'G' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'O';    
    ELSE
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'O';
    END IF;

    total_produits :=0 ;
    total_charges :=0;
    total_produits_ant :=0 ;
    total_charges_ant :=0;
    formule := '';

    --**** RECUPERATION EBE *******
    SELECT NVL(sig_montant,0), groupe2, sig_libelle, NVL(sig_montant_ant,0), groupe_ant
    INTO ebe, lib1, lib, ebe_ant, libant FROM SIG
    WHERE gescode = ges_code AND exe_ordre = exeordre
    AND groupe1 = 'R�sultat d''exploitation' AND commentaire = 'EBE';
    IF lib1 = 'charges' THEN
        ebe := -ebe;
    END IF;
    IF libant = 'charges' THEN
        ebe_ant := -ebe_ant;
    END IF;

    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, ebe, ebe_ant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte(exeordre, '75%', gescode, sacd)
        + resultat_compte(exeordre, '1875%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '75%', gescode, sacd)
        + resultat_compte(exeordre-1, '1875%', gescode, sacd);
    lib := '+ Autres produits "encaissables" d''exploitation';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


    total := resultat_compte(exeordre, '791%', gescode, sacd)
        + resultat_compte(exeordre, '18791%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '791%', gescode, sacd)
        + resultat_compte(exeordre-1, '18791%', gescode, sacd);
    lib := '+ Transferts de charges';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+resultat_compte(exeordre, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+ resultat_compte(exeordre-1, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
    lib := '+ Produits financiers "encaissables" (a)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '771%', gescode, sacd)+resultat_compte(exeordre, '778%', gescode, sacd)+resultat_compte(exeordre, '797%', gescode, sacd)+resultat_compte(exeordre, '18771%', gescode, sacd)+resultat_compte(exeordre, '18778%', gescode, sacd)+resultat_compte(exeordre, '18797%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '771%', gescode, sacd)+resultat_compte(exeordre-1, '778%', gescode, sacd)+resultat_compte(exeordre-1, '797%', gescode, sacd)+resultat_compte(exeordre-1, '18771%', gescode, sacd)+resultat_compte(exeordre-1, '18778%', gescode, sacd)+resultat_compte(exeordre-1, '18797%', gescode, sacd);
    lib := '+ Produits exceptionnels "encaissables" (b)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  charges ***********
    lib1 := 'charges';

    total := resultat_compte(exeordre, '65%', gescode, sacd)
        + resultat_compte(exeordre, '1865%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '65%', gescode, sacd)
        + resultat_compte(exeordre-1, '1865%', gescode, sacd);
    lib := '- Autres charges "d�caissables" d''exploitation';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '66%', gescode, sacd)+resultat_compte(exeordre, '1866%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '66%', gescode, sacd)+resultat_compte(exeordre-1, '1866%', gescode, sacd);
    lib := '- Charges financi�res "d�caissables" (c)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '671%', gescode, sacd)+resultat_compte(exeordre, '678%', gescode, sacd)+resultat_compte(exeordre, '18671%', gescode, sacd)+resultat_compte(exeordre, '18678%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '671%', gescode, sacd)+resultat_compte(exeordre-1, '678%', gescode, sacd)+resultat_compte(exeordre-1, '18671%', gescode, sacd)+resultat_compte(exeordre-1, '18678%', gescode, sacd);
    lib := '- Charges exceptionnelles "d�caissables" (d)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '695%', gescode, sacd)+resultat_compte(exeordre, '18695%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '695%', gescode, sacd)+resultat_compte(exeordre-1, '18695%', gescode, sacd);
    lib := '- Imp�ts sur les b�n�fices';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := ebe+total_produits-total_charges;
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;


    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


ELSE

    --********* D�termination � partir du r�sultat **********************

    IF sacd = 'O' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
    ELSIF sacd = 'G' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'N';        
    ELSE
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
    END IF;

    total_produits :=0 ;
    total_charges :=0;
    formule := '';

    --**** RECUPERATION RESULTAT ********
    totalant := 0;
    lib := 'R�sultat de l''exercice';
    IF sacd = 'O' THEN
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
    ELSIF sacd = 'G' THEN
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
    ELSE
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
    END IF;
    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, resultat, totalant, 'RTAT');

    -- ********  Charges ***********
    lib1 := 'charges';

    total := resultat_compte(exeordre, '681%', gescode, sacd)+resultat_compte(exeordre, '686%', gescode, sacd)+resultat_compte(exeordre, '687%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '681%', gescode, sacd)+resultat_compte(exeordre-1, '686%', gescode, sacd)+resultat_compte(exeordre-1, '687%', gescode, sacd);
    lib := '+ Dotations aux amortissements et provisions';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '675%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
    lib := '+ Valeur comptable des �l�ments actifs c�d�s';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte(exeordre, '781%', gescode, sacd)+resultat_compte(exeordre, '786%', gescode, sacd)+resultat_compte(exeordre, '787%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '781%', gescode, sacd)+resultat_compte(exeordre-1, '786%', gescode, sacd)+resultat_compte(exeordre-1, '787%', gescode, sacd);
    lib := '- Reprises sur amortissements et provisions';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '775%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
    lib := '- Produits de cessions des �l�ments actifs c�d�s';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '776%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
    lib := '- Produits issus de la neutralisation des amortissements';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '777%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
    lib := '- Quote-part des subventions d''investissement vir�es au compte de r�sultat';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := resultat+total_charges-total_produits;
    totalant := 0;
    formule := 'CAF';
    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;

    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


END IF;

END;
/



----------



SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.api_planco
IS


    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire              VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE;


    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco (
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;
      
      function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type;    
        
        
    procedure valider_Planco( exeordre number,pconum varchar, valide varchar);
    
    /* Creer un compte avec toutes les caracteristiques d'un autre */
    procedure creer_Planco_From_ref(
            exeordre           INTEGER,    
            pconumNew              VARCHAR,
            pcolibelle          VARCHAR,
            pconumRef           varchar);     
            
    procedure set_sens_solde(exeordre integer, pconum varchar, sensSolde varchar);             
    
    /** Affecter tous les types de credit tcdsect */
    procedure affecte_type_credit(exeordre integer, pconum varchar, tcdSect varchar, tcdType varchar, plaquoi varchar);
    
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_planco IS


    /** Creer un compte de prestation interne a partir du compte budgetaire */
    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE
    is
        flag integer;
        plancoexer plan_comptable_exer%rowtype; 
    begin
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        if (flag=0) then
            return null;
        end if;
        
        select * into plancoexer from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        
        return creer_planco(exeordre,    
            '18'||pconumBudgetaire,
            plancoexer.pco_libelle,
            plancoexer.pco_budgetaire,
            plancoexer.pco_emargement,
            plancoexer.pco_nature,
            plancoexer.pco_sens_emargement,
            plancoexer.pco_validite,
            plancoexer.pco_j_exercice,
            plancoexer.pco_j_fin_exercice,
            plancoexer.pco_j_be);
         
    end;

    /** creer un enregistrement dans plan comptable (plan_comptable_exer + plan_comptable)  */
    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE
    is
        res plan_comptable.pco_num%TYPE;
    begin
        res := prv_fn_creer_planco_exer(exeordre, pconum,pcolibelle,pcobudgetaire,pcoemargement,pconature,pcosensemargement,pcovalidite,pcojexercice,pcojfinexercice,pcojbe);
        return res;
    end;

----------------------------------------------

   FUNCTION prv_fn_creer_planco (pconum VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE
   IS
      flag   INTEGER;
      pconiveau integer;
   BEGIN
      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable
       WHERE pco_num = pconum;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable
                  (pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite,                   --PCO_VALIDITE,
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

      RETURN pconum;
   END;
   
   -----------------------------------------------------
   
   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
   RETURN plan_comptable_exer.pco_num%TYPE
    IS
      flag   INTEGER;
      pconiveau integer;
      res plan_comptable_exer.pco_num%TYPE;
   BEGIN
        

      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable_exer
       WHERE pco_num = pconum and exe_ordre=exeordre;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable_exer
                  (pcoe_id, exe_ordre, pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (plan_comptable_exer_seq.nextval, 
                   exeordre, 
                   pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite, --PCO_VALIDITE
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

        -- creer le compte dans plan comptable s'il n'existe pas
       res := prv_fn_creer_planco(pconum,
                    pcolibelle,
                    pcobudgetaire,
                    pcoemargement,
                    pconature,
                    pcosensemargement,
                    pcovalidite,
                    pcojexercice,
                    pcojfinexercice,
                    pcojbe);        
      RETURN pconum;
   END;
   
    function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type
    is
        res plan_comptable_exer.pco_libelle%type;
    begin
        select pco_libelle into res from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        return res;
    end;   
   
   
   
   
       procedure valider_Planco( exeordre number,pconum varchar, valide varchar)
       is
       begin
            if (valide='O' or valide='VALIDE') then
                 update plan_comptable_exer set pco_validite='VALIDE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
            if (valide='N' or valide='ANNULE') then
                 update plan_comptable_exer set pco_validite='ANNULE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
       
           
       end;
    
    /* Creer un compte avec toutes les caracteristiques d'un autre (planco_visa, planco_credit) */
    procedure creer_Planco_From_ref(
            exeordre           INTEGER,    
            pconumNew              VARCHAR,
            pcolibelle          VARCHAR,
            pconumRef           varchar)
      is
        pcoRefExiste integer;
        pcoNewExiste integer;
        plancovisaNewexiste integer;
        plancocreditNewexiste integer;
        planconew plan_comptable_exer.PCO_NUM%type;
        pcoref plan_comptable_exer%rowtype;
      begin
                pcoRefExiste := 0;
                pcoNewExiste := 0;
    
                -- verifier si le compte de reference existe
                select count(*) into pcoRefExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
    
                -- verifier si le nouveau compte existe
                select count(*) into pcoNewExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumNew;

    
                -- si le nouveau compte existe et s'il est invalide, on le valide
                -- si existe pas, on le cree 
                if (pcoNewExiste >0) then
                    valider_planco(pconumNew, exeordre, 'O');
                else
                    if (pcoRefExiste>0) then
                        select * into pcoref from plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
                    
                        planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    pcoref.pco_budgetaire,
                                    pcoref.pco_emargement,
                                    pcoref.pco_nature,
                                    pcoref.pco_sens_emargement,
                                    pcoref.pco_validite,
                                    pcoref.pco_j_exercice,
                                    pcoref.pco_j_fin_exercice,
                                    pcoref.pco_j_be);                                
                    else
                       planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    'O',
                                    'N',
                                    'D',
                                    '0',
                                    'VALIDE',
                                    'N',
                                    'N',
                                    'N');                        
                    end if;
                
                end if;
    
    
                -- dupliquer les donnees sur les tables peripheriques
                  -- planco_visa
                select count(*) into plancovisaNewexiste from planco_visa where pco_num_ordonnateur = pconumNew and exe_ordre=exeordre;
                if (plancovisaNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_VISA (
                                    PVI_ORDRE,
                                    EXE_ORDRE,
                                    PCO_NUM_ORDONNATEUR, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION 
                                    ) 
                            select  planco_visa_seq.nextval,
                                    exeordre,
                                    pconumNew, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION from planco_visa where pco_num_ordonnateur = pconumRef and exe_ordre=exeordre;                    
                end if;
                
                
                
                -- planco_credit                
                select count(*) into plancocreditNewexiste from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num = pconumNew and tc.exe_ordre = exeordre;
                if (plancocreditNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_CREDIT (
                           PCC_ORDRE, 
                           PCO_NUM, 
                           TCD_ORDRE,                            
                           PLA_QUOI, 
                           PCC_ETAT) 
                        select PLANCO_CREDIT_seq.nextval, 
                                pconumNew, 
                               tc.TCD_ORDRE,                                
                               PLA_QUOI, 
                               PCC_ETAT from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num=pconumRef and tc.exe_ordre = exeordre;                    
                end if;                
                
      
      end;
   
   
    procedure set_sens_solde(exeordre integer, pconum varchar, sensSolde varchar)
    is
    begin
        update plan_comptable_exer set PCO_SENS_SOLDE=sensSolde where pco_num=pconum and exe_ordre=exeordre;
    end;
 

    procedure affecte_type_credit(exeordre integer, pconum varchar, tcdSect varchar, tcdType varchar, plaquoi varchar)
    is
        flag integer;
     begin
        select count(*) into flag from type_credit where exe_ordre=exeordre and tcd_Sect=tcdsect and tcd_type=tcdType;
        if (flag=0) then
            raise_application_error (-20001,'tcd_sect '|| tcdsect || ' non trouve pour exercice '||exeordre);
        end if;
        
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        if (flag=0) then
            raise_application_error (-20001,'pco_num '|| pconum || ' non trouve pour exercice '||exeordre);
        end if;
        
        
        DELETE FROM MARACUJA.PLANCO_CREDIT where pco_num=pconum and tcd_ordre in (select tcd_ordre from type_credit where exe_ordre=exeordre and tcd_sect=tcdsect and tcd_type=tcdType); 
        
        INSERT INTO MARACUJA.PLANCO_CREDIT (
                    PCC_ORDRE, 
                    TCD_ORDRE, 
                    PCO_NUM, 
                    PLA_QUOI, 
                    PCC_ETAT) 
                select  
                    PLANCO_CREDIT_seq.nextval, 
                    TCD_ORDRE, 
                    pconum, 
                    plaquoi, 
                    'VALIDE'
                    from type_credit where exe_ordre = exeordre and tcd_sect = tcdsect and tcd_type=tcdType; 

        
        
     end;    
   
END;
/




