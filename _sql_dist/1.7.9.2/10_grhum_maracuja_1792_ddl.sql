SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.V_BUDGET_LOLF_EXEC
(EXERCICE, NATURE, MASSE, TYPE_CREDIT, TCD_LIBELLE, 
 BUDGET, UB, LOLF_CODE_PERE, LOLF_LIBELLE_PERE, CODE_LOLF, 
 LIBELLE_LOLF, MONTANT)
AS 
select exercice, nature, masse, type_credit, tcd_libelle,operation BUDGET, ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf,  sum(montant_budgetaire) montant
from
(
   select tcd.tcd_libelle tcd_libelle,tcd_sect masse,'execution' operation,o.org_ub ub,rpca.exe_ordre exercice, jlfr.lolf_code lolf_code_pere, jlfr.lolf_libelle lolf_libelle_pere,   
    lfr.lolf_code code_lolf, lfr.lolf_libelle libelle_lolf,rpca.RACT_HT_SAISIE montant_budgetaire, ' ' libelle,tcd.tcd_code type_credit,tcd.tcd_type nature
   from
   JEFY_recette.recette_CTRL_ACTION rpca ,
   jefy_recette.recette_BUDGET rb ,
   jefy_admin.LOLF_NOMENCLATURE_RECETTE lfr,
   jefy_admin.lolf_nomenclature_recette jlfr,
   jefy_recette.facture_budget fb,
   jefy_admin.organ o,
   jefy_admin.type_credit tcd
   where rb.rec_ID = rpca.rec_id
   and lfr.lolf_id = jefy_admin.api_lolf.GET_rec_LOLF_ID_PERE_AT_NIVEAU(rpca.lolf_id,2)
   and lfr.lolf_pere=jlfr.lolf_id
   and fb.fac_id = rb.fac_id
   and o.org_id = fb.org_id
   and tcd.tcd_ordre = fb.tcd_ordre 
   and rb.rec_id in (select distinct rec_id from JEFY_RECETTE.RECETTE_CTRL_planco where tit_id is not null)
union all
   select tcd.tcd_libelle tcd_libelle,tcd_sect masse,'execution' operation,o.org_ub ub,dpp.exe_ordre exercice, jlfd.lolf_code lolf_code_pere, 
   jlfd.lolf_libelle lolf_libelle_pere,lfd.lolf_code code_lolf,lfd.lolf_libelle libelle_lolf,dpca.dact_montant_budgetaire montant_budgetaire,
   ' ' libelle,tcd.tcd_code type_credit,tcd.tcd_type nature
   from JEFY_DEPENSE.DEPENSE_CTRL_ACTION dpca ,
   jefy_depense.DEPENSE_BUDGET db ,
   jefy_depense.DEPENSE_PAPIER dpp,
   jefy_admin.LOLF_NOMENCLATURE_DEPENSE lfd,
   jefy_admin.lolf_nomenclature_depense jlfd,
   jefy_depense.engage_budget eb,
   jefy_admin.organ o,
   jefy_admin.type_credit tcd
   where db.DPP_ID = dpp.dpp_id
   and db.DEP_ID = dpca.DEP_ID
   and lfd.lolf_id = jefy_admin.api_lolf.GET_DEP_LOLF_ID_PERE_AT_NIVEAU(tyac_id,2)
   and lfd.lolf_pere=jlfd.lolf_id  
  and eb.eng_id = db.eng_id
   and o.org_id = eb.org_id
   and tcd.tcd_ordre = eb.tcd_ordre
   and db.dep_id in (select distinct dep_id from JEFY_DEPENSE.DEPENSE_CTRL_planco where man_id is not null)
)
group by exercice, nature, masse, type_credit, tcd_libelle,operation , ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf
order by exercice, nature, masse, type_credit, tcd_libelle, BUDGET, ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf;
/





CREATE OR REPLACE FORCE VIEW COMPTEFI.V_DVLOP_DEP_LISTE
(EXE_ORDRE, GES_CODE, SECTION, PCO_NUM_BDN, PCO_NUM, 
 MANDATS, REVERSEMENTS, CR_OUVERTS, DEP_DATE)
AS 
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, SUM(man_ht), 0, 0, e.DATE_SAISIE
-- Mandat d�penses
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND tbo_ordre <> 8 and tbo_ordre<>21 AND tbo_ordre <>18 AND tbo_ordre <> 16
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement avant 2007
SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0, SUM(tit_ht), 0, e.DATE_SAISIE
FROM maracuja.TITRE t, maracuja.bordereau b, comptefi.v_planco p, (
  SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
  WHERE
  m.tit_id=mde.tit_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.tit_id ) e
WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 8
AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id
GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num, e.DATE_SAISIE
UNION ALL
-- ordre de reversement � partir de 2007
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, 0, -SUM(man_ht), 0, e.DATE_SAISIE
FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, (
  SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie
  FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m
  WHERE
  m.man_id=mde.MAN_ID
  AND mde.ecd_ordre=ecd.ecd_ordre
  AND ecd.ecr_ordre=e.ecr_ordre
  GROUP BY m.man_id ) e
WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND (tbo_ordre = 8 OR tbo_ordre=18 OR tbo_ordre=21)
AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE
UNION ALL
-- Cr�dits ouverts
SELECT EXE_ORDRE, GES_CODE, '1' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '6%' OR pco_num LIKE '7%')
AND BDN_QUOI ='D' AND co <> 0
UNION ALL
SELECT EXE_ORDRE, GES_CODE, '2' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO
FROM comptefi.v_budnat WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%')
AND BDN_QUOI ='D' AND co <> 0;
/

-------
CREATE OR REPLACE PROCEDURE comptefi.PREPARE_EXECUTION_BUDGET (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  montant NUMBER(12,2);
  montant_depenses NUMBER(12,2);
  montant_recettes NUMBER(12,2);
  ebe NUMBER(12,2);
  montant_dep NUMBER(12,2);
  montant_rec NUMBER(12,2);

  groupe1 VARCHAR2(50);
  groupe2 VARCHAR2(50);
  --groupe3 varchar2(50);

  lib_dep VARCHAR2(100);
  lib_rec VARCHAR2(100);
  --formule varchar2(1000);
  flag INTEGER;

BEGIN

IF sacd = 'O' THEN
	DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE from EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
ELSE
	DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



	SELECT COUNT(*) INTO flag FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
	AND methode_ebe = 'N' AND caf_libelle = 'R�sultat de l''exercice';

	IF (flag=0) THEN
	   RAISE_APPLICATION_ERROR (-20001,'Vous devez calculer la CAF a partir du resultat avant de calculer le cadre 4.');
	END IF;


--************* 1�re section FONCTIONNEMENT ***********************************
   groupe1 := '1ERE SECTION : FONCTIONNEMENT';
   montant_depenses := 0;
   montant_recettes := 0;

-- D�penses
   groupe2 := ' ';

    lib_dep := 'Charges de fonctionnement';
    montant_dep := Execution_Bud(exeordre, '60%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '61%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '62%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '63%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '64%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '65%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '681%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '66%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '686%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '186%', gescode, sacd, 'D');
	montant_depenses := montant_depenses+montant_dep;

	lib_rec := 'Produits de fonctionnement';
    montant_rec := Execution_Bud(exeordre, '70%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '71%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '72%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '74%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '781%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '791%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '75%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '76%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '786%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '796%', gescode, sacd, 'R')
			+ Execution_Bud(exeordre, '187%', gescode, sacd, 'R');
	montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

	lib_dep := 'Charges exceptionnelles';
    montant_dep := Execution_Bud(exeordre, '67%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '687%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

   	lib_rec := 'Produits exceptionnels';
    montant_rec := Execution_Bud(exeordre, '77%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '787%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '797%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

-- Equilibre
	groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';



	SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
	AND methode_ebe = 'N' AND caf_libelle = 'R�sultat de l''exercice';
	IF montant >= 0 THEN
	   	montant_dep := montant;
		montant_rec := 0;
	   	montant_depenses := montant_depenses+montant_dep;
	ELSE
		montant_rec := ABS(montant);
		montant_dep := 0;
	   	montant_recettes := montant_recettes+montant_rec;
	END IF;

	lib_dep := 'Exc�dent de l''exercice';
	lib_rec := 'D�ficit de l''exercice';

	INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);



--***************** 2EME SECTION : CAPITAL *******************
	groupe1 := '2EME SECTION : OPERATIONS EN CAPITAL';
   	montant_depenses := 0;
  	montant_recettes := 0;

   	groupe2 := ' ';

 	lib_dep := 'D�penses en capital';
    montant_dep := Execution_Bud(exeordre, '2%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '1%', gescode, sacd, 'D');
	montant_depenses := montant_depenses+montant_dep;

 	lib_rec := 'Recettes en capital (1)';
    montant_rec := Execution_Bud(exeordre, '2%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '1%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '775%', gescode, sacd, 'R');
	montant_recettes := montant_recettes+montant_rec;

	INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


-- Equilibre
	groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';

	-- CAF ou IAF
	SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
	AND methode_ebe = 'N' AND formule = 'CAF';
	IF montant < 0 THEN
	   	montant_dep := ABS(montant);
		montant_rec := 0;
	   	montant_depenses := montant_depenses+montant_dep;
	ELSE
		montant_rec := montant;
		montant_dep := 0;
	   	montant_recettes := montant_recettes+montant_rec;
	END IF;
	lib_dep:= 'Insuffisance d''autofinancement';
	lib_rec := 'Capacit� d''autofinancement';
	INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


	-- AFR ou DFR
	IF montant_depenses < montant_recettes THEN
		montant_dep := montant_recettes - montant_depenses;
		montant_rec := 0;
		montant_depenses := montant_depenses+montant_dep;
	ELSE
		montant_rec := montant_depenses - montant_recettes;
		montant_dep := 0;
		montant_recettes := montant_recettes+montant_rec;
	END IF;

	lib_dep := 'Augmentation du fond de roulement';
	lib_rec := 'Diminution du fond de roulement';
	INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


END;
/




SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Journal IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

-- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
PROCEDURE validerEcriture (ecrordre INTEGER) ;
-- permet d'annuler une ecriture saisie .
PROCEDURE annulerEcriture (ecrordre INTEGER) ;

-- permet de creer une ecriture de balance d entree --
FUNCTION creerEcritureBE (
COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

 -- permet de creer une ecriture d exercice --
FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  ) RETURN INTEGER;
 -- permet de creer une ecriture de fin d exercice --
FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER) RETURN INTEGER;
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;


-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
FUNCTION creerEcriturePrivate (

  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Journal IS
-- PUBLIC --

PROCEDURE validerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
debit number;
credit number;
BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL
WHERE ecr_ordre = ecrordre;

IF cpt >=2 THEN
    select sum(ecd_debit) into debit from ECRITURE_DETAIL
        WHERE ecr_ordre = ecrordre;
    select sum(ecd_credit) into credit from ECRITURE_DETAIL
        WHERE ecr_ordre = ecrordre;        
    if (credit<>debit) then
        RAISE_APPLICATION_ERROR (-20001,'Ecriture desequilibree');
    end if;    
    Numerotationobject.numeroter_ecriture(ecrordre);
ELSE
 RAISE_APPLICATION_ERROR (-20001,'MAUVAIS FORMAT D ECRITURE !');
END IF;

END;

PROCEDURE annulerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
BEGIN
 SELECT ecr_numero INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
END IF;

END;

FUNCTION creerEcritureBE (

  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,

  )RETURN INTEGER
IS
  TJOORDRE              NUMBER;
BEGIN
-- recup du type_journal....
SELECT tjo_ordre INTO tjoordre FROM TYPE_JOURNAL
WHERE tjo_libelle ='JOURNAL BALANCE ENTREE';


RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  )RETURN INTEGER
IS
  cpt              NUMBER;
BEGIN
-- recup du type_journal....
SELECT COUNT(*) INTO cpt FROM TYPE_JOURNAL
WHERE tjo_ordre =tjoordre;

IF cpt = 0 THEN
RAISE_APPLICATION_ERROR (-20001,'TYPE DE JOURNAL INCONNU !');
END IF;

RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER)RETURN INTEGER
IS
cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
RETURN cpt;
END;

FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;
BEGIN

RETURN Api_Plsql_Journal.creerEcriturePrivate(
  NULL              ,
  COMORDRE              ,--        NOT NULL,
  ECRDATE                ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
 NULL  ,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetailPrivate
(
  ECDCOMMENTAIRE    ,-- (200),
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


END;







-- PRIVATE --
FUNCTION creerEcriturePrivate (
  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )
  RETURN INTEGER
IS
ECRORDRE INTEGER;
ecrdateNew date;
exerciceRec exercice%rowtype;
BEGIN

ecrdateNew := ecrDate;
select * into exercicerec from exercice  where exe_ordre=exeOrdre; 

-- si exercice pas ouvert et pas restreint on bloque
if (exercicerec.exe_stat<>'O' and exercicerec.exe_stat<>'R') then
     RAISE_APPLICATION_ERROR (-20001,'Impossible de creer une ecriture sur un exercice non ouvert ou non restreint.');
end if;

-- si exercice restreint, changer la date
if (exercicerec.exe_stat='R') then
     ecrdateNew := to_date('31/12/'|| exeOrdre , 'dd/mm/yyyy');
end if;

SELECT ecriture_seq.NEXTVAL INTO ECRORDRE FROM dual;


INSERT INTO ECRITURE VALUES
(BROORDRE,
COMORDRE,
SYSDATE,
ecrdateNew,
'VALIDE',
ecrlibelle,
0,
ECRNUMERO_BROUILLARD,
ECRORDRE,
ECRLIBELLE,
EXEORDRE,
ORIORDRE,
TJOORDRE,
TOPORDRE,
UTLORDRE
);

RETURN ECRORDRE;
END;


FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
ECDORDRE          NUMBER;
ECDCREDIT         NUMBER;
ECDDEBIT          NUMBER;
BEGIN

SELECT ecriture_detail_seq.NEXTVAL  INTO ECDORDRE FROM dual;

IF ECDSENS = 'C' THEN
ECDCREDIT := ecdmontant;
ECDDEBIT := 0;
ELSE
ECDCREDIT := 0;
ECDDEBIT := ecdmontant;
END IF;


INSERT INTO ECRITURE_DETAIL VALUES (
  ECDCOMMENTAIRE    ,-- (200),
  ECDCREDIT         ,-- (12,2),
  ECDDEBIT          ,-- (12,2),
  ECRORDRE          ,--        NOT NULL,
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDORDRE          ,--        NOT NULL,
  NULL         ,-- (200),
  ABS(ECDMONTANT)  ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


RETURN ECDORDRE;
END;

END;
/


GRANT EXECUTE ON  MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;


