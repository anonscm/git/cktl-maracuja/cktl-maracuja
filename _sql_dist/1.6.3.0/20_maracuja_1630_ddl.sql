CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd,
         ECRITURE e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
	 AND rec_mont>=0
	 AND ecd.ecd_credit=0
	 AND ecd.ecd_debit<>0
     AND ecd.ecr_ordre = e.ecr_ordre
     AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
	 AND ecd.exe_ordre = (SELECT exe_ordre FROM exercice WHERE exe_stat='O')
GROUP BY r.rec_id
UNION ALL
SELECT   r.rec_id, -SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd,
         ECRITURE e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
	 AND rec_mont<0
	 AND ecd.ecd_debit=0
	 AND ecd.ecd_credit<>0
     AND ecd.ecr_ordre = e.ecr_ordre
     AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
	 AND ecd.exe_ordre = (SELECT exe_ordre FROM exercice WHERE exe_stat='O')
GROUP BY r.rec_id;
-----------------------------


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_SUIVI
(MAN_ID, EXE_ORDRE, BOR_ID, BOR_NUM, GES_CODE, 
 MAN_NUMERO, FOU_ORDRE, MAN_ETAT, BOR_DATE_VISA, BRJ_ORDRE, 
 MAN_NUMERO_REJET, MAN_MOTIF_REJET, MAN_HT, MAN_TVA, MAN_TTC, 
 PAI_NUMERO, TVI_LIBELLE, VIR_DATE_VALEUR, MOD_DOM, MOD_CODE, 
 MOD_LIBELLE, ORI_ORDRE, PCO_NUM, PREST_ID, PAI_ORDRE, 
 ORG_ORDRE, RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, MAN_ATTENTE_PAIEMENT, MAN_ATTENTE_DATE, 
 MAN_ATTENTE_OBJET, VIR_DATE_CREATION, BOR_DATE_CREATION)
AS 
SELECT
man_id, m.EXE_ORDRE, b.BOR_ID, b.bor_num,b.GES_CODE,MAN_NUMERO,
   FOU_ORDRE,  MAN_ETAT,b.BOR_DATE_VISA,BRJ_ORDRE,MAN_NUMERO_REJET,MAN_MOTIF_REJET,
   MAN_HT, MAN_TVA, MAN_TTC,
   pai_numero,tvi_libelle, VIR_DATE_VALEUR,
   mp.MOD_DOM, mp.MOD_CODE, mp.mod_libelle,
   ORI_ORDRE, PCO_NUM, PREST_ID,
   m.PAI_ORDRE, ORG_ORDRE,
   RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, MAN_ATTENTE_PAIEMENT,
   MAN_ATTENTE_DATE, MAN_ATTENTE_OBJET,
    vir_date_creation, bor_date_creation
FROM MARACUJA.MANDAT m,
maracuja.BORDEREAU b,
maracuja.MODE_PAIEMENT mp,
(
     SELECT p.pai_ordre, p.pai_numero,tvi.tvi_libelle, VIR_DATE_VALEUR, vir_date_creation FROM PAIEMENT p, TYPE_VIREMENT tvi, (
    SELECT vf.pai_ordre, vf.VIR_DATE_VALEUR, vf.vir_date_creation
    FROM VIREMENT_FICHIER vf,
    (SELECT pai_ordre , MAX(VIR_ordre) vir_ordre FROM maracuja.VIREMENT_FICHIER GROUP BY pai_ordre) x
    WHERE x.vir_ordre=vf.vir_ordre
    ) z
    WHERE
    p.tvi_ordre=tvi.tvi_ordre
    AND p.pai_ordre=z.pai_ordre(+)
) y
WHERE
m.bor_id=b.bor_id
AND m.mod_ordre=mp.mod_ordre
AND m.pai_ordre=y.pai_ordre(+)
/


GRANT SELECT ON  MARACUJA.V_MANDAT_SUIVI TO JEFY_DEPENSE WITH GRANT OPTION
/

GRANT SELECT ON  MARACUJA.V_MANDAT_SUIVI TO JEFY_MISSION WITH GRANT OPTION
/

GRANT SELECT ON  MARACUJA.V_MANDAT_SUIVI TO JEFY_PAYE
/

GRANT SELECT ON  MARACUJA.V_MANDAT_SUIVI TO JEFY_REPORT
/



---------------------------


ALTER TABLE MARACUJA.ORDRE_DE_PAIEMENT
ADD (ODP_reference_paiement VARCHAR2(50));
