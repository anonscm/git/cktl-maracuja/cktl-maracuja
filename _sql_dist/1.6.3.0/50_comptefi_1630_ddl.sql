CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi) 
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb, 
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o 
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre 
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM 
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 1 
and bdsa_date_validation is not null 
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation 
union all 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi) 
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb, 
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o 
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre 
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM 
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 2 
and bdsa_date_validation is not null 
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation 
union all 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy.budnat b, jefy.exer e, jefy.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy.budnat b, jefy.exer e, jefy.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq) 
FROM jefy.budnat b, jefy.organ o 
WHERE b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq) 
FROM jefy.budnat b, jefy.organ o 
WHERE b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'RELI' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'RELI' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture; 