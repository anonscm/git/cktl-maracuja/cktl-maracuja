begin
	INSERT INTO maracuja.PARAMETRE (SELECT 2010, 'Indique le nombre de jours depuis lesquels effectuer le controle de doublons lors des paiements (facultatif)', 'CTRL_DOUBLONS_PAIEMENT_NB_JOURS_DEPUIS', maracuja.parametre_seq.NEXTVAL, '730'
                             FROM MARACUJA.EXERCICE  WHERE exe_ordre=2010);
      commit;                                                   

---
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.0.0',  SYSDATE, '');
    commit;
end;