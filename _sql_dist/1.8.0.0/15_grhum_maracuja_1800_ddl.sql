grant select on jefy_budget.budget_masque_nature to maracuja with grant option;
grant select on jefy_budget.budget_saisie_nature to maracuja with grant option;

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_TITRES_CREDITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, tde_origine
  FROM maracuja.TITRE m,
       maracuja.recette r, 
       maracuja.BORDEREAU b,
       maracuja.TITRE_DETAIL_ECRITURE mde,
       maracuja.ECRITURE_DETAIL ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ECRITURE e       
 WHERE m.bor_id = b.bor_id
   AND m.tit_id = mde.tit_id
   and m.tit_id=r.tit_id
   and mde.exe_ordre=m.exe_ordre
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   AND tde_origine IN ('VISA', 'REIMPUTATION')
   AND tit_etat IN ('VISE')
   AND ecd_credit <> 0
   AND tbo_ordre IN (7, 11, 200)
   AND e.top_ordre<>11
   AND ABS (ecd_montant) = ABS (rec_mont);
/


CREATE OR REPLACE FORCE VIEW EPN.EPN_DEP_BUD
(EXE_ORDRE, PCO_NUM, GES_CODE, EPN_DATE, DEP_MONT, 
 REVERS)
AS 
SELECT m.EXE_ORDRE, pco_num, m.ges_code , bor_date_visa, SUM(man_ht), 0
-- Mandat d�penses
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b
 WHERE m.bor_id = b.bor_id AND ( tbo_ordre <> 8 and tbo_ordre<>21 AND tbo_ordre <>18 AND tbo_ordre <> 16) 
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE')
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
 GROUP BY m.exe_ordre, pco_num, m.ges_code, bor_date_visa
UNION ALL
-- Ordre de reversement avant 2007
 SELECT t.exe_ordre, pco_num , t.ges_code , bor_date_visa , 0 , SUM (tit_ht)
 FROM maracuja.TITRE t, maracuja.BORDEREAU b
 WHERE t.bor_id = b.bor_id AND tbo_ordre = 8
 AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE'
 GROUP BY t.exe_ordre, pco_num , t.ges_code , bor_date_visa
UNION ALL
 SELECT m.EXE_ORDRE, pco_num, m.ges_code , bor_date_visa, 0, -SUM(man_ht)
-- Ordre de reversement � partir de 2007
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b
 WHERE m.bor_id = b.bor_id AND (tbo_ordre = 8 OR tbo_ordre=18 OR tbo_ordre=21)
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE')
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
 GROUP BY m.exe_ordre, pco_num, m.ges_code, bor_date_visa;
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, MDE_ORIGINE, ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num,
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, m.man_tva,
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant
 --- mandats des d�penses ---
 FROM MANDAT m,
      MANDAT_DETAIL_ECRITURE mde,
       ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      BORDEREAU b,
      TYPE_BORDEREAU tb
WHERE m.man_id = mde.man_id
       AND m.BOR_ID=b.bor_id
      AND b.tbo_ordre=tb.tbo_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine IN ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  --AND ecd_debit<>0
  AND ecd_credit = 0 -- pour avoir les mandats de r�gul TVA
  AND ABS (ecd_montant)=ABS(man_ht)
  AND ED.pco_num NOT LIKE '185%'
  AND (tb.TBO_TYPE='BTME' OR tb.TBO_TYPE='BTMS')
  AND tb.tbo_sous_type <> 'REVERSEMENTS'  -- pour �liminer les �critures de d�bit des OR
UNION ALL
 ---  mandats des PI ---
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num,
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, m.man_tva,
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant
 FROM MANDAT m,
      MANDAT_DETAIL_ECRITURE mde,
       ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      BORDEREAU b,
      TYPE_BORDEREAU tb
WHERE m.man_id = mde.man_id
       AND m.BOR_ID=b.bor_id
      AND b.tbo_ordre=tb.tbo_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine IN ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  AND ecd_debit <> 0
  AND ABS (ecd_montant)=ABS(man_ht)
  AND ED.pco_num NOT LIKE '185%'
  AND (tb.TBO_TYPE='BTPI' OR tb.TBO_TYPE='BTPID') -- ajout du nouveau type BTPID --
UNION ALL
-- Ordres de Reversements � partir de 2007 --
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num,
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_credit, 
      (SIGN(ecd_credit))*abs(m.man_tva),
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant
 FROM MANDAT m,
      MANDAT_DETAIL_ECRITURE mde,
       ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      BORDEREAU b,
      TYPE_BORDEREAU tb
WHERE m.man_id = mde.man_id
       AND m.BOR_ID=b.bor_id
      AND b.tbo_ordre=tb.tbo_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine IN ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  AND ecd_credit <> 0
  AND ABS (ecd_montant)=ABS(man_ht)
  AND ED.pco_num NOT LIKE '185%'
  AND tb.tbo_sous_type = 'REVERSEMENTS'; -- Bordereau des OR
/


CREATE OR REPLACE PACKAGE EPN.EpN3_EDRB
IS

procedure epn_genere_edrb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_edrb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);

procedure prv_genere_edrb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_edrb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER);
procedure prv_insert_edrb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_EDRB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_EDRB(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_REC_BUD_1 where EPNRB1_TYPE_DOC=typeFichier and EPNRB1_COD_BUD=codeBudget and EPNRB_ordre=rang and EPNRB1_EXERCICE=exeOrdre;
    delete from EPN_REC_BUD_2 where EPNRB2_TYPE_DOC=typeFichier and EPNRB2_COD_BUD=codeBudget and EPNRB_ORDRE=rang and EPNRB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_EDRB(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_EDRB(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_EDRB_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_EDRB_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_EDRB_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;
        
    end if;

end;





-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_EDRB_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre     
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
          

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_EDRB_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_EDRB_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE = gescodeSACD
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);     
    
end;







-- se charge de remplir EPN_EDRB_2
procedure prv_insert_EDRB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
         INSERT INTO EPN.EPN_REC_BUD_2  
            VALUES (
                    to_number(rang),    --EPNRB_ORDRE, 
                    gesCode,    --EPNRB2_GES_CODE, 
                    '2',    --EPNRB2_TYPE, 
                     numLigne,   --EPNRB2_NUMERO, 
                     1,   --EPNRB2_TYPE_CPT, 
                     (SUBSTR(vCompte,1,15)),   --EPNRB2_COMPTE, 
                     depensesMontant*deviseTaux,  --EPNRB2_MNTBRUT, 
                     depensesSurCreditExtourne*deviseTaux,   --EPNRB2_DEP_CREEXT, 
                     reversementsMontant*deviseTaux,   --EPNRB2_MNTREVERS, 
                     depensesExtournes*deviseTaux,   --EPNRB2_DEP_DEPEXT, 
                     (depensesMontant-reversementsMontant)*deviseTaux,   --EPNRB2_MNTNET, 
                     exeOrdre,   --EPNRB2_EXERCICE, 
                     codeBudget,   --EPNRB2_COD_BUD, 
                     typeFichier   --EPNRB2_TYPE_DOC
                    );                           

    --INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;

end;



procedure prv_insert_EDRB1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_REC_BUD_1.EPNRB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_REC_BUD_1 
            VALUES (
                    to_number(rang), --EPNRB_ORDRE, 
                    gesCode, --EPNRB1_GES_CODE, 
                    '1', --EPNRB1_TYPE, 
                    1, --EPNRB1_NUMERO, 
                    identifiantDGCP, --EPNRB1_IDENTIFIANT, 
                    typeFichier,--EPNRB1_TYPE_DOC, 
                    codeNomenclature, --EPNRB1_COD_NOMEN, 
                    codeBudget,--EPNRB1_COD_BUD, 
                    exeOrdre,--EPNRB1_EXERCICE, 
                    rang, --EPNRB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNRB1_DATE, 
                    siren, --EPNRB1_SIREN, 
                    siret,--EPNRB1_SIRET, 
                    nbEnregistrement --EPNRB1_NBENREG
                    ); 
        
        --INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,                     EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);
    

end;








END;
/


CREATE OR REPLACE PACKAGE EPN.EpN3_BAL
IS

procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER);
procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_BAL  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_BAL_1 where EPNB1_TYPE_DOC=typeFichier and EPNB1_COD_BUD=codeBudget and EPNB_ordre=rang and EPNB1_EXERCICE=exeOrdre;
    delete from EPN_BAL_2 where EPNB2_TYPE_DOC=typeFichier and EPNB2_COD_BUD=codeBudget and EPNB_ORDRE=rang and EPNB2_EXERCICE=exeOrdre;
end;

-- genere le fichier de balance comptable
procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_bal(typeFichier, codeBudget, exeOrdre, rang);

    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_bal_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_bal_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_bal_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;

    end if;

end;


-- generation des fichiers de balance consolides (type 01)
-- NB : Maracuja ne gere pas les comptabilites secondaires, donc ce fichier sera identique aux fichiers 02 (aggreges)
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '01';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;



-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);


end;


-- generation des fichiers balance detailles (type 03) pour un SACD
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;

    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    ( select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    gesCode := gesCodeSACD;
    typeFichier := '03';

    numLigne := 2;
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

    -- insert entete
     prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;







-- se charge de remplir EPN_BAL_2
procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER)
is
    debit_total NUMBER;
    credit_total NUMBER;
    solde_debiteur NUMBER;
    solde_crediteur NUMBER;
    vCompte varchar2(60);

    vDebitBe number;
    vDebitExer number;
    vCreditBe number;
    vCreditExer number;


begin

    vDebitBe := round(debit_be*deviseTaux, 2);
    vDebitExer := round(debit_exer*deviseTaux, 2);
    vCreditBe := round(credit_be*deviseTaux, 2);
    vCreditExer := round(credit_exer*deviseTaux, 2);

    debit_total  :=  vDebitBe  +  vDebitExer;
    credit_total  :=  vCreditBe  +  vCreditExer;

    if (debit_total=0 and credit_total=0) then
        numLigne := numLigne - 1;
        return;
    end if;

    IF debit_total > credit_total THEN
        solde_debiteur := debit_total - credit_total;
        solde_crediteur := 0;
    ELSE
        solde_debiteur := 0;
        solde_crediteur := credit_total - debit_total;
    END IF;

    vCompte := compte || '               ';

    INSERT INTO EPN.EPN_BAL_2
        VALUES (to_number(rang),  --EPNB_ORDRE,
                gesCode,   --EPNB2_GES_CODE,
                '2',    --EPNB2_TYPE,
                numLigne,   --EPNB2_NUMERO,
                1,    --EPNB2_TYPE_CPT,
                (SUBSTR(vCompte,1,15)),    --EPNB2_COMPTE,
                 vDebitBe,   --EPNB2_DEBBE,
                 vDebitExer, --EPNB2_DEBCUM,
                 debit_total,   --EPNB2_DEBTOT,
                 vCreditBe,   --EPNB2_CREBE,
                 vCreditExer,   --EPNB2_CRECUM,
                 credit_total,   --EPNB2_CRETOT,
                 solde_debiteur,   --EPNB2_BSDEB,
                 solde_crediteur,   --EPNB2_BSCRE,
                  exeOrdre,  --EPNB2_EXERCICE
                  codeBudget,  --EPNB2_COD_BUD
                  typeFichier --EPNB2_TYPE_DOC
                  );

end;



procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER)
is

    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_bal_1.EPNB1_COD_NOMEN%type;
    vDateFichier2 date;

begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);

    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;


   -- Insert du header/footer du fichier
    INSERT INTO EPN.EPN_BAL_1 (EPNB_ORDRE, EPNB1_GES_CODE, EPNB1_TYPE,
               EPNB1_NUMERO, EPNB1_IDENTIFIANT, EPNB1_TYPE_DOC,
               EPNB1_COD_NOMEN, EPNB1_COD_BUD, EPNB1_EXERCICE,
               EPNB1_RANG, EPNB1_DATE, EPNB1_SIREN,
               EPNB1_SIRET, EPNB1_NB_ENREG)
        VALUES (   to_number(rang), --EPNB_ORDRE
                    gesCode, --EPNB1_GES_CODE
                    '1', --EPNB1_TYPE
                    1, --EPNB1_NUMERO (numero de ligne dans le fichier)
                    identifiantDGCP, --EPNB1_IDENTIFIANT
                    typeFichier, --EPNB1_TYPE_DOC
                    codeNomenclature, --EPNB1_COD_NOMEN
                    codeBudget, --EPNB1_COD_BUD
                    exeOrdre, --EPNB1_EXERCICE
                    rang, --EPNB1_RANG
                    TO_CHAR(vDateFichier2,'DDMMYYYY'), --EPNB1_DATE
                    siren, --EPNB1_SIREN
                    siret, --EPNB1_SIRET
                    nbEnregistrement --EPNB1_NB_ENREG);
                    );



end;


END;
/


GRANT EXECUTE ON  EPN.EPN3_BAL TO MARACUJA;







