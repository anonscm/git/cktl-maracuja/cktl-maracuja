grant execute on jefy_depense.engager to maracuja;
grant execute on jefy_depense.liquider to maracuja;
grant execute on jefy_depense.reverser to maracuja;
grant delete on jefy_depense.extourne_liq to maracuja;

create table maracuja.zlog_util (
    zlog_id number, 
    zlog_when date, 
    zlog_desc varchar2(500)
)
TABLESPACE GFC;


ALTER TABLE MARACUJA.zlog_util ADD (
  CONSTRAINT PK_zlog_util
 PRIMARY KEY
 (zlog_id)
    USING INDEX 
    TABLESPACE GFC_INDX);
    
CREATE SEQUENCE MARACUJA.zlog_util_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;    