-- nouvelle vue pour avoir l'ensemble des conventions
grant select on accords.contrat to maracuja with grant option;
grant references on accords.contrat to maracuja;
grant select on accords.type_classification_contrat to maracuja with grant option;

create or replace view maracuja.v_accords_contrat as
select s.pers_id as pers_id_partenaire, s.LL_STRUCTURE as ll_partenaire,s2.pers_id as pers_id_service, c.*
from accords.contrat c inner join accords.type_classification_contrat tcc on c.TCC_ID=tcc.tcc_id
left outer join accords.contrat_partenaire cp on cp.CON_ORDRE=c.con_ordre and cp.CP_PRINCIPAL='O'
inner join grhum.structure_ulr s on cp.pers_id=s.pers_id
left outer join grhum.structure_ulr s2 on c.con_cr=s2.c_structure
where tcc.TCC_CODE='CONV' ;

-- ajout colonne pour referencer les conventions
alter table maracuja.ecriture_detail add (con_ordre number null);
comment on column maracuja.ecriture_detail.con_ordre is 'Référence à la convention (accords.contrat). Cf. vue maracuja.v_accords_contrat';
alter table maracuja.ecriture_detail add (constraint fk_ecd_con_ordre  foreign key (con_ordre)  references accords.contrat (con_ordre));

 