CREATE OR REPLACE PACKAGE MARACUJA.api_plsql_journal
is
/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

   -- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
   procedure validerecriture (ecrordre integer);

-- permet d'annuler une ecriture saisie .
   procedure annulerecriture (ecrordre integer);

-- permet de creer une ecriture de balance d entree --
   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer;

   -- permet de creer une ecriture d exercice --
   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer;

   -- permet de creer une ecriture de fin d exercice --
   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer;

-- permet de creer une ecriture --
   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL
   )
      return integer;

-- permet d ajouter des details a une ecriture.
   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer;
     
-- permet d ajouter des details a une ecriture en referencant une convention    
 function creerecrituredetailconv (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre      number
   )
      return integer;      

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type);

-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      ecrnumero_brouillard   number,
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
       conordre          number
   )
      return integer;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_plsql_journal
is
-- PUBLIC --
   procedure validerecriture (ecrordre integer)
   is
      cpt      integer;
      debit    number;
      credit   number;
   begin
      select count (*)
      into   cpt
      from   ecriture_detail
      where  ecr_ordre = ecrordre;

      if cpt >= 2 then
         select sum (ecd_debit)
         into   debit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         select sum (ecd_credit)
         into   credit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         if (credit <> debit) then
            raise_application_error (-20001, 'Ecriture desequilibree');
         end if;

         numerotationobject.numeroter_ecriture (ecrordre);
      else
         raise_application_error (-20001, 'MAUVAIS FORMAT D ECRITURE !');
      end if;
   end;

   procedure annulerecriture (ecrordre integer)
   is
      cpt   integer;
   begin
      select ecr_numero
      into   cpt
      from   ecriture
      where  ecr_ordre = ecrordre;

      if cpt = 0 then
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      end if;
   end;

   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      tjoordre   number;
   begin
-- recup du type_journal....
      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL BALANCE ENTREE';

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer
   is
      cpt   number;
   begin
-- recup du type_journal....
      select count (*)
      into   cpt
      from   type_journal
      where  tjo_ordre = tjoordre;

      if cpt = 0 then
         raise_application_error (-20001, 'TYPE DE JOURNAL INCONNU !');
      end if;

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select 1
      into   cpt
      from   dual;

      return cpt;
   end;

   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      cpt   integer;
   begin
      return api_plsql_journal.creerecritureprivate (null, comordre,   --        NOT NULL,
                                                     ecrdate,   --         NOT NULL,
                                                     ecrlibelle,   -- (200)  NOT NULL,
                                                     null, exeordre,   --        NOT NULL,
                                                     oriordre, tjoordre,   --        NOT NULL,
                                                     topordre,   --        NOT NULL,
                                                     utlordre   --        NOT NULL,
                                                             );
   end;

   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      -- EXEORDRE          NUMBER,--        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer
   is
      exeordre   number;
   begin
      select exe_ordre
      into   exeordre
      from   ecriture
      where  ecr_ordre = ecrordre;

      return api_plsql_journal.creerecrituredetailprivate (ecdcommentaire,   -- (200),
                                                           ecdlibelle,   -- (200),
                                                           ecdmontant,   -- (12,2) NOT NULL,
                                                           ecdsecondaire,   -- (20),
                                                           ecdsens,   -- (1)  NOT NULL,
                                                           ecrordre,   --        NOT NULL,
                                                           exeordre,   --        NOT NULL,
                                                           gescode,   -- (10)  NOT NULL,
                                                           pconum,   -- (20)  NOT NULL
                                                           null      );
   end;
   
    function creerecrituredetailconv (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre      number
   )
      return integer
   is
      exeordre   number;
   begin
      select exe_ordre
      into   exeordre
      from   ecriture
      where  ecr_ordre = ecrordre;

      return api_plsql_journal.creerecrituredetailprivate (ecdcommentaire,   -- (200),
                                                           ecdlibelle,   -- (200),
                                                           ecdmontant,   -- (12,2) NOT NULL,
                                                           ecdsecondaire,   -- (20),
                                                           ecdsens,   -- (1)  NOT NULL,
                                                           ecrordre,   --        NOT NULL,
                                                           exeordre,   --        NOT NULL,
                                                           gescode,   -- (10)  NOT NULL,
                                                           pconum,   -- (20)  NOT NULL
                                                           conordre      );
   end;

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type)
   is
   begin
      update ecriture_detail
         set ecd_index = ecdindex
       where ecd_ordre = ecdordre;
   end;

-- PRIVATE --
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      --ECRNUMERO             NUMBER,-- (32),
      ecrnumero_brouillard   number,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer
   is
      ecrordre      integer;
      ecrdatenew    date;
      exercicerec   exercice%rowtype;
   begin
      ecrdatenew := ecrdate;

      select *
      into   exercicerec
      from   exercice
      where  exe_ordre = exeordre;

-- si exercice pas ouvert et pas restreint on bloque
      if (exercicerec.exe_stat <> 'O' and exercicerec.exe_stat <> 'R') then
         raise_application_error (-20001, 'Impossible de creer une ecriture sur un exercice non ouvert ou non restreint.');
      end if;

-- si exercice restreint, changer la date
      if (exercicerec.exe_stat = 'R') then
         ecrdatenew := to_date ('31/12/' || exeordre, 'dd/mm/yyyy');
      end if;

      select ecriture_seq.nextval
      into   ecrordre
      from   dual;

      insert into ecriture
      values      (broordre,
                   comordre,
                   sysdate,
                   ecrdatenew,
                   'VALIDE',
                   ecrlibelle,
                   0,
                   ecrnumero_brouillard,
                   ecrordre,
                   ecrlibelle,
                   exeordre,
                   oriordre,
                   tjoordre,
                   topordre,
                   utlordre
                  );

      return ecrordre;
   end;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre          number
   )
      return integer
   is
      ecdordre    number;
      ecdcredit   number;
      ecddebit    number;
   begin
      select ecriture_detail_seq.nextval
      into   ecdordre
      from   dual;

      if ecdsens = 'C' then
         ecdcredit := ecdmontant;
         ecddebit := 0;
      else
         ecdcredit := 0;
         ecddebit := ecdmontant;
      end if;

      insert into maracuja.ecriture_detail
                  (ecd_commentaire,
                   ecd_credit,
                   ecd_debit,
                   ecd_index,
                   ecd_libelle,
                   ecd_montant,
                   ecd_ordre,
                   ecd_postit,
                   ecd_reste_emarger,
                   ecd_secondaire,
                   ecd_sens,
                   ecr_ordre,
                   exe_ordre,
                   ges_code,
                   pco_num,
                   con_ordre
                  )
      values      (ecdcommentaire,   -- (200),
                   ecdcredit,   -- (12,2),
                   ecddebit,   -- (12,2),
                   ecrordre,   --        NOT NULL,
                   ecdlibelle,   -- (200),
                   ecdmontant,   -- (12,2) NOT NULL,
                   ecdordre,   --        NOT NULL,
                   null,   -- (200),
                   abs (ecdmontant),   -- (12,2) NOT NULL,
                   ecdsecondaire,   -- (20),
                   ecdsens,   -- (1)  NOT NULL,
                   ecrordre,   --        NOT NULL,
                   exeordre,   --        NOT NULL,
                   gescode,   -- (10)  NOT NULL,
                   pconum,   -- (20)  NOT NULL
                   conordre
                  );

      return ecdordre;
   end;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;

