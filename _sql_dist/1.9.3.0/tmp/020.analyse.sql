CREATE OR REPLACE PACKAGE MARACUJA."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco2(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    procedure checkCadre2VsBalance(exeOrdre INTEGER);
    procedure checkEcritureDetailGestion(exeordre integer); 
    procedure checkMandatVsJefyDepense(exeordre integer); 
    procedure checkMontantsEmargements(exeOrdre integer);
    procedure checkEcritureDates(exeordre integer);
    procedure checkAllProblemes(exeOrdre integer);
    
    
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  objet maracuja.mandat%ROWTYPE;
  manid mandat.man_id%type;
  manttc mandat.man_ttc%type;
  ecddebit ecriture_detail.ecd_debit%type;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and 
    (
    (man_ETAT   not in ('VISE','PAYE', 'ANNULE') and bor_etat  in ('VISE', 'PAYE', 'PAIEMENT'))
    or
    (man_ETAT   not in ('ANNULE') and brj_ordre is not null)
    )
    ;
    
    
    cursor c2 is
        select m.* from mandat m, mandat_detail_ecriture mde, ecriture_detail ecd, mode_paiement mp  
            where m.man_id=mde.man_id and m.exe_ordre=2011 and mde_origine='VISA' 
            and m.mod_ordre=mp.mod_ordre and m.pai_ordre is null
            and mp.MOD_DOM='VIREMENT'
            and mde.ecd_ordre=ecd.ecd_ordre
            and abs(ecd_reste_emarger)<>abs(ecd_montant)
            and man_ht>0;
            
            
     cursor c3 is 
        SELECT m.man_id, man_ttc, SUM(ecd_debit) 
        FROM mandat m, mandat_detail_ecriture mde, ecriture_detail ecd 
        WHERE m.man_id=mde.man_id AND mde.ecd_ordre=ecd.ecd_ordre
        AND man_ttc>=0
        AND m.exe_ordre=exeordre
        AND mde_origine LIKE 'VISA%'
        GROUP BY m.man_id, man_ttc
        HAVING man_ttc<> SUM(ecd_debit);
            
            
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau ('|| borEtat ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') ou bien mandat sur un bordereau de rejet et non ANNULE';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de données : remettre la bonne valeur dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
  
   OPEN C2;
  LOOP
    FETCH c2 INTO objet;
    EXIT
  WHEN c2%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'La contrepartie de prise en charge du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') a été émargée alors que le mode de paiement est de type VIREMENT.';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Provoque un message d''erreur lors de la création d''un paiement (écritures déséquilibrées)', -- ZAP_CONSEQUENCE
        'Vous devez supprimer l''émargement puis modifier le mode de paiement du mandat si le mandat a réellement été payé autrement que par virement via Maracuja.'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c2;
  
  
    OPEN C3;
  LOOP
    FETCH c3 INTO manid, manttc, ecddebit ;
    EXIT
  WHEN c3%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau b, mandat m
    where b.bor_id=m.bor_id and m.man_id=manid;
    
    probleme := 'Le montant de l''écriture de visa du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||
    ') n''est pas cohérent avec le montant du mandat (man: '||manttc|| ' / ecr: '||ecddebit||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Incohérence comptabilité ordonnateur/comptable. Impossible de générer un paiement', -- ZAP_CONSEQUENCE
        'Intervention dans la base de données pour régulariser (suppression de l''écriture en trop si le problème vient de là) .'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c3;
  
  
  
  
END;

PROCEDURE checkMandatVsJefyDepense
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  pco_num_depense jefy_depense.depense_ctrl_planco.PCO_NUM%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b, 
      jefy_depense.depense_ctrl_planco dpco
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and m.man_id=dpco.man_id
    and dpco.pco_num <> m.pco_num
   
    ;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    select pco_num into pco_num_depense from jefy_depense.depense_ctrl_planco where man_id = objet.man_id;

    
    probleme := 'Imputation du mandat (' || objet.pco_num || ') incoherente avec l''imputation  de la depense côté budgétaire ('|| pco_num_depense ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Une raison possible est qu''il y a eu une réimputation comptable du mandat qui n''a pas été réimpactée budgétairement. Il est nécessaire d''intervenir dans la base de données pour le man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



procedure checkEcritureDates(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture
    where exe_ordre=exeOrdre
    and to_char(ecr_date_saisie,'yyyy')<>to_char(exeOrdre) ;
BEGIN
  categorie := 'ECRITURES';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'La date de saisie de l''écriture est incohérente avec l''exercice (date '|| lecritureDetail.ecr_date_saisie ||' / code gestion ' || lecritureDetail.exe_ordre ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE.ECR_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecr_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Corriger le champ ecr_date_saisie dans la table maracuja.ecriture ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDetailGestion(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code not in
      (select ges_code
      from maracuja.gestion_exercice
      where exe_ordre              =exeOrdre
      );
BEGIN
  categorie := 'CODES GESTION';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un code gestion non actif sur l''exercice (numero '|| ecrNumero ||' / code gestion ' || lecritureDetail.ges_code ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Activer le code gestion sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco2
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   not in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre      
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte supprimé sur '|| exeOrdre || '(numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;

procedure checkCadre2VsBalance(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vgescode gestion.ges_code%type;
  vpconum plan_comptable_exer.pco_num%type;
  vbaldebit number;
  vbalCredit number;
  vmandate number;
  vreverse number;
  vsolde number;
  vnet  number;
  

    cursor c1 is 
        select ges_code, pco_num,  sum(bal_debit) bal_debit, sum(bal_credit) bal_credit,  sum(bal_solde) bal_solde , sum(mandate) mandate, sum(reverse) reverse, sum(montant_net) montant_net
        from (
        select ges_code, ecd.pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '6%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, substr(ecd.pco_num,3,10) pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '186%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, pco_num, 0,0,0,sum(MANDATS), sum(reversements), sum(montant_net)
        from COMPTEFI.V_DVLOP_DEP
        where exe_ordre=exeOrdre
        and pco_num like '6%'
        group by ges_code, pco_num
        )
        group by ges_code, pco_num
        having (sum(bal_debit)<>sum(mandate) or sum(bal_credit)<> sum(reverse))
        order by ges_code, pco_num;

begin
    categorie     := 'COMPTE FINANCIER';
    sousCategorie := 'DIFFERENCE BALANCE/CADRE 2';
  OPEN C1;
  LOOP
    FETCH c1 INTO vgescode ,vpconum ,vbaldebit,vbalCredit,vSolde,vmandate, vreverse, vnet;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Code gestion: ' || vgescode|| ' Compte: ' || vPcoNum || ' : Balance <> cadre 2 (balance : D='||vBalDebit ||', C='|| vBalCredit ||') (Cadre 2 :  Mandats='||vMandate ||', ORV='|| vReverse ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          'Normal si certain bordereaux ne sont pas encore visés. Sinon incoherence dans les tables.',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;



procedure checkMontantsEmargements(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vecd_ordre maracuja.ecriture_detail.ecd_ordre%type;
  vecr_numero maracuja.ecriture.ecr_numero%type;
  vecd_index maracuja.ecriture_detail.ecd_index%type;
  vecd_montant maracuja.ecriture_detail.ecd_montant%type;
  vecd_reste_emarge maracuja.ecriture_detail.ecd_reste_emarger%type;
  vemd_montant maracuja.emargement_detail.emd_montant%type;
  vcalcreste maracuja.ecriture_detail.ecd_reste_emarger%type;
  vpco_num maracuja.ecriture_detail.pco_num%type;

    cursor c1 is       
        select ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger , sum(emd_montant), abs(ecd_montant)-sum(emd_montant) 
        from (
            select distinct * from 
            (select ed.* from emargement_detail ed, emargement ema where ema.ema_ordre=ed.ema_ordre and ema_etat='VALIDE') emd, 
            ecriture_detail ecd ,
            ecriture ecr
            where (ecd_ordre=ecd_ordre_source or ecd_ordre = ecd_ordre_destination)
            and ecd.exe_ordre=exeOrdre
            and ecd.ecr_ordre=ecr.ecr_ordre
        ) 
        group by ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger
        having abs(ecd_montant)-sum(emd_montant)<> ecd_reste_emarger;

begin
    categorie     := 'ECRITURES';
    sousCategorie := 'MONTANTS EMARGEMENTS';
  OPEN C1;
  LOOP
    FETCH c1 INTO vecd_ordre, vpco_num, vecr_numero, vecd_index, vecd_montant , vecd_reste_emarge, vemd_montant, vcalcreste;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Ecriture : ' || vecr_numero || '/'||  vecd_index ||', Compte: ' || vpco_num || ' : Montant du reste à émarger (='|| vecd_reste_emarge ||') différent du reste à émarger calculé (='||vcalcreste ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les états de développement de solde', -- ZAP_CONSEQUENCE
          'Corrigez le reste à émarger de l''écriture (Bouton disponible dans l''écran de consultation des écritures de Maracuja).',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;


procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
   checkMandatVsJefyDepense(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkEcritureDetailPlanco2(exeordre);
  checkEcritureDetailGestion(exeordre); 
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
  checkCadre2VsBalance(exeOrdre);
  checkMontantsEmargements(exeOrdre);
  
end;
END;
/


