SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.3.0
-- Date de publication : 06/03/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_maracuja_1930;
commit;

drop procedure grhum.inst_patch_maracuja_1930;


