set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.3.0
-- Date de publication : 
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Modifications pour le lien entre les ecritures et les conventions
-- Corrections outil analyse
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.2.9', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.0', null );
commit ;



grant execute on jefy_depense.engager to maracuja;
grant execute on jefy_depense.liquider to maracuja;
grant execute on jefy_depense.reverser to maracuja;
grant delete on jefy_depense.extourne_liq to maracuja;

grant references on accords.contrat to maracuja;

create table maracuja.zlog_util (
    zlog_id number, 
    zlog_when date, 
    zlog_desc varchar2(500)
)
TABLESPACE GFC;


ALTER TABLE MARACUJA.zlog_util ADD (
  CONSTRAINT PK_zlog_util
 PRIMARY KEY
 (zlog_id)
    USING INDEX 
    TABLESPACE GFC_INDX);
    
CREATE SEQUENCE MARACUJA.zlog_util_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;    
  

create or replace procedure grhum.inst_vue_accord_tmp
is
  isCocolight integer;
  vuecoconuts varchar2(4000);
  vuecommon varchar2(4000);
   vuecocolight varchar2(4000);

begin
  vuecommon := 'create or replace force view accords.v_convention_partenaire (pers_id_partenaire,ll_partenaire,pers_id_service,con_ordre, exe_ordre, con_index,con_etablissement,con_cr,con_nature, con_reference_externe,con_objet,con_objet_court,con_observations,tr_ordre,utl_ordre_creation,con_date_creation,utl_ordre_modif,con_date_modif,utl_ordre_valid_adm,con_date_valid_adm,con_date_cloture,con_date_apurement,con_groupe_bud,con_suppr,org_id_composante,tcc_id,avis_favorable,avis_defavorable,contexte,remarques,motifs_avis,c_naf,con_groupe_partenaire,con_duree,date_migration,con_duree_mois) as ';
    vuecocolight := vuecommon || ' /* version de la vue pour cocolight */ select s.pers_id as pers_id_partenaire,s.pers_libelle as ll_partenaire,s2.pers_id as pers_id_service,c.CON_ORDRE,c.EXE_ORDRE,c.CON_INDEX,c.CON_ETABLISSEMENT,c.CON_CR,c.CON_NATURE,c.CON_REFERENCE_EXTERNE,c.CON_OBJET,c.CON_OBJET_COURT,c.CON_OBSERVATIONS,c.TR_ORDRE,c.UTL_ORDRE_CREATION,c.CON_DATE_CREATION,c.UTL_ORDRE_MODIF,c.CON_DATE_MODIF,c.UTL_ORDRE_VALID_ADM,c.CON_DATE_VALID_ADM,c.CON_DATE_CLOTURE,c.CON_DATE_APUREMENT,c.CON_GROUPE_BUD,c.CON_SUPPR,c.ORG_ID_COMPOSANTE,c.TCC_ID,c.AVIS_FAVORABLE,c.AVIS_DEFAVORABLE,c.CONTEXTE,c.REMARQUES,c.MOTIFS_AVIS,c.C_NAF,c.CON_GROUPE_PARTENAIRE,c.CON_DUREE,c.DATE_MIGRATION,c.CON_DUREE_MOIS   from   accords.contrat c inner join accords.type_classification_contrat tcc on c.tcc_id = tcc.tcc_id left outer join accords.contrat_partenaire cp on cp.con_ordre = c.con_ordre and cp.cp_principal = ''O'' inner join grhum.personne s on cp.pers_id = s.pers_id left outer join grhum.structure_ulr s2 on c.con_cr = s2.c_structure   where  tcc.tcc_code = ''CONV''';
    vuecoconuts := vuecommon || ' /* version de la vue pour coconuts */  select s.pers_id as pers_id_partenaire,s.pers_libelle as ll_partenaire,s2.pers_id as pers_id_service,  c.CON_ORDRE,c.EXE_ORDRE,c.CON_INDEX,c.CON_ETABLISSEMENT,c.CON_CR,c.CON_NATURE,c.CON_REFERENCE_EXTERNE,c.CON_OBJET,c.CON_OBJET_COURT,c.CON_OBSERVATIONS,c.TR_ORDRE,c.UTL_ORDRE_CREATION,c.CON_DATE_CREATION,c.UTL_ORDRE_MODIF,c.CON_DATE_MODIF,c.UTL_ORDRE_VALID_ADM,c.CON_DATE_VALID_ADM,c.CON_DATE_CLOTURE,c.CON_DATE_APUREMENT,c.CON_GROUPE_BUD,c.CON_SUPPR,c.ORG_ID_COMPOSANTE,to_number(null) as TCC_ID, to_char(null) as AVIS_FAVORABLE,to_char(null) as AVIS_DEFAVORABLE,to_char(null) as CONTEXTE,to_char(null) as REMARQUES,to_char(null) as MOTIFS_AVIS,to_char(null) as C_NAF,to_char(null) as CON_GROUPE_PARTENAIRE,to_number(null) as CON_DUREE,to_date(null) as DATE_MIGRATION,to_number(null) as CON_DUREE_MOIS from accords.contrat c, grhum.personne s, grhum.structure_ulr s2,(select con_ordre, pers_id from accords.avenant a, accords.avenant_partenaire ap where a.avt_ordre = ap.avt_ordre (+) and ap.ap_principal = ''O'' and avt_suppr = ''N'') part where c.con_ordre = part.con_ordre (+) and part.pers_id = s.pers_id (+) and c.con_cr = s2.c_structure and c.con_suppr = ''N''';

    select count(*) into isCocolight 
    from accords.version_histo where vh_num ='2.0.0';


    if (isCocolight>0) then
    -- cocolight installe
        execute immediate vuecocolight;
    else
    -- cocolight non installe
        execute immediate vuecoconuts;
    end if;
end;
/

execute grhum.inst_vue_accord_tmp;
drop procedure grhum.inst_vue_accord_tmp;

grant select on accords.v_convention_partenaire to maracuja;

create or replace force view maracuja.v_accords_contrat (pers_id_partenaire,
                                                         ll_partenaire,
                                                         pers_id_service,
                                                         con_ordre,
                                                         exe_ordre,
                                                         con_index,
                                                         con_etablissement,
                                                         con_cr,
                                                         con_nature,
                                                         con_reference_externe,
                                                         con_objet,
                                                         con_objet_court,
                                                         con_observations,
                                                         tr_ordre,
                                                         utl_ordre_creation,
                                                         con_date_creation,
                                                         utl_ordre_modif,
                                                         con_date_modif,
                                                         utl_ordre_valid_adm,
                                                         con_date_valid_adm,
                                                         con_date_cloture,
                                                         con_date_apurement,
                                                         con_groupe_bud,
                                                         con_suppr,
                                                         org_id_composante,
                                                         tcc_id,
                                                         avis_favorable,
                                                         avis_defavorable,
                                                         contexte,
                                                         remarques,
                                                         motifs_avis,
                                                         c_naf,
                                                         con_groupe_partenaire,
                                                         con_duree,
                                                         date_migration,
                                                         con_duree_mois
                                                        )
as
select pers_id_partenaire,
                                                         ll_partenaire,
                                                         pers_id_service,
                                                         con_ordre,
                                                         exe_ordre,
                                                         con_index,
                                                         con_etablissement,
                                                         con_cr,
                                                         con_nature,
                                                         con_reference_externe,
                                                         con_objet,
                                                         con_objet_court,
                                                         con_observations,
                                                         tr_ordre,
                                                         utl_ordre_creation,
                                                         con_date_creation,
                                                         utl_ordre_modif,
                                                         con_date_modif,
                                                         utl_ordre_valid_adm,
                                                         con_date_valid_adm,
                                                         con_date_cloture,
                                                         con_date_apurement,
                                                         con_groupe_bud,
                                                         con_suppr,
                                                         org_id_composante,
                                                         tcc_id,
                                                         avis_favorable,
                                                         avis_defavorable,
                                                         contexte,
                                                         remarques,
                                                         motifs_avis,
                                                         c_naf,
                                                         con_groupe_partenaire,
                                                         con_duree,
                                                         date_migration,
                                                         con_duree_mois 
	from accords.v_convention_partenaire;
/	


-- ajout colonne pour referencer les conventions
alter table maracuja.ecriture_detail add (con_ordre number null);
comment on column maracuja.ecriture_detail.con_ordre is 'Référence à la convention (accords.contrat). Cf. vue maracuja.v_accords_contrat';
alter table maracuja.ecriture_detail add (constraint fk_ecd_con_ordre  foreign key (con_ordre)  references accords.contrat (con_ordre));

 CREATE OR REPLACE PACKAGE MARACUJA.api_plsql_journal
is
/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

   -- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
   procedure validerecriture (ecrordre integer);

-- permet d'annuler une ecriture saisie .
   procedure annulerecriture (ecrordre integer);

-- permet de creer une ecriture de balance d entree --
   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer;

   -- permet de creer une ecriture d exercice --
   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer;

   -- permet de creer une ecriture de fin d exercice --
   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer;

-- permet de creer une ecriture --
   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL
   )
      return integer;

-- permet d ajouter des details a une ecriture.
   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer;
     
-- permet d ajouter des details a une ecriture en referencant une convention    
 function creerecrituredetailconv (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre      number
   )
      return integer;      

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type);

-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      ecrnumero_brouillard   number,
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
       conordre          number
   )
      return integer;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_plsql_journal
is
-- PUBLIC --
   procedure validerecriture (ecrordre integer)
   is
      cpt      integer;
      debit    number;
      credit   number;
   begin
      select count (*)
      into   cpt
      from   ecriture_detail
      where  ecr_ordre = ecrordre;

      if cpt >= 2 then
         select sum (ecd_debit)
         into   debit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         select sum (ecd_credit)
         into   credit
         from   ecriture_detail
         where  ecr_ordre = ecrordre;

         if (credit <> debit) then
            raise_application_error (-20001, 'Ecriture desequilibree');
         end if;

         numerotationobject.numeroter_ecriture (ecrordre);
      else
         raise_application_error (-20001, 'MAUVAIS FORMAT D ECRITURE !');
      end if;
   end;

   procedure annulerecriture (ecrordre integer)
   is
      cpt   integer;
   begin
      select ecr_numero
      into   cpt
      from   ecriture
      where  ecr_ordre = ecrordre;

      if cpt = 0 then
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      end if;
   end;

   function creerecriturebe (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      tjoordre   number;
   begin
-- recup du type_journal....
      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL BALANCE ENTREE';

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecritureexercicetype (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      topordre     number,   --        NOT NULL,
      utlordre     number,   --        NOT NULL,
      tjoordre     integer
   )
      return integer
   is
      cpt   number;
   begin
-- recup du type_journal....
      select count (*)
      into   cpt
      from   type_journal
      where  tjo_ordre = tjoordre;

      if cpt = 0 then
         raise_application_error (-20001, 'TYPE DE JOURNAL INCONNU !');
      end if;

      return api_plsql_journal.creerecriture (comordre,   --        NOT NULL,
                                              ecrdate,   --         NOT NULL,
                                              ecrlibelle,   -- (200)  NOT NULL,
                                              exeordre,   --        NOT NULL,
                                              oriordre, tjoordre,   --        NOT NULL,
                                              topordre,   --        NOT NULL,
                                              utlordre   --        NOT NULL,
                                                      );
   end;

   function creerecriturecloture (brjordre integer, exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select 1
      into   cpt
      from   dual;

      return cpt;
   end;

   function creerecriture (
      comordre     number,   --        NOT NULL,
      ecrdate      date,   --         NOT NULL,
      ecrlibelle   varchar2,   -- (200)  NOT NULL,
      exeordre     number,   --        NOT NULL,
      oriordre     number,
      tjoordre     number,   --        NOT NULL,
      topordre     number,   --        NOT NULL,
      utlordre     number   --        NOT NULL,
   )
      return integer
   is
      cpt   integer;
   begin
      return api_plsql_journal.creerecritureprivate (null, comordre,   --        NOT NULL,
                                                     ecrdate,   --         NOT NULL,
                                                     ecrlibelle,   -- (200)  NOT NULL,
                                                     null, exeordre,   --        NOT NULL,
                                                     oriordre, tjoordre,   --        NOT NULL,
                                                     topordre,   --        NOT NULL,
                                                     utlordre   --        NOT NULL,
                                                             );
   end;

   function creerecrituredetail (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      -- EXEORDRE          NUMBER,--        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2   -- (20)  NOT NULL
   )
      return integer
   is
      exeordre   number;
   begin
      select exe_ordre
      into   exeordre
      from   ecriture
      where  ecr_ordre = ecrordre;

      return api_plsql_journal.creerecrituredetailprivate (ecdcommentaire,   -- (200),
                                                           ecdlibelle,   -- (200),
                                                           ecdmontant,   -- (12,2) NOT NULL,
                                                           ecdsecondaire,   -- (20),
                                                           ecdsens,   -- (1)  NOT NULL,
                                                           ecrordre,   --        NOT NULL,
                                                           exeordre,   --        NOT NULL,
                                                           gescode,   -- (10)  NOT NULL,
                                                           pconum,   -- (20)  NOT NULL
                                                           null      );
   end;
   
    function creerecrituredetailconv (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre      number
   )
      return integer
   is
      exeordre   number;
   begin
      select exe_ordre
      into   exeordre
      from   ecriture
      where  ecr_ordre = ecrordre;

      return api_plsql_journal.creerecrituredetailprivate (ecdcommentaire,   -- (200),
                                                           ecdlibelle,   -- (200),
                                                           ecdmontant,   -- (12,2) NOT NULL,
                                                           ecdsecondaire,   -- (20),
                                                           ecdsens,   -- (1)  NOT NULL,
                                                           ecrordre,   --        NOT NULL,
                                                           exeordre,   --        NOT NULL,
                                                           gescode,   -- (10)  NOT NULL,
                                                           pconum,   -- (20)  NOT NULL
                                                           conordre      );
   end;

   procedure setecdindex (ecdordre ecriture_detail.ecd_ordre%type, ecdindex ecriture_detail.ecd_index%type)
   is
   begin
      update ecriture_detail
         set ecd_index = ecdindex
       where ecd_ordre = ecdordre;
   end;

-- PRIVATE --
   function creerecritureprivate (
      broordre               number,
      comordre               number,   --        NOT NULL,
      ecrdate                date,   --         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
      ecrlibelle             varchar2,   -- (200)  NOT NULL,
      --ECRNUMERO             NUMBER,-- (32),
      ecrnumero_brouillard   number,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
      exeordre               number,   --        NOT NULL,
      oriordre               number,
      tjoordre               number,   --        NOT NULL,
      topordre               number,   --        NOT NULL,
      utlordre               number   --        NOT NULL,
   )
      return integer
   is
      ecrordre      integer;
      ecrdatenew    date;
      exercicerec   exercice%rowtype;
   begin
      ecrdatenew := ecrdate;

      select *
      into   exercicerec
      from   exercice
      where  exe_ordre = exeordre;

-- si exercice pas ouvert et pas restreint on bloque
      if (exercicerec.exe_stat <> 'O' and exercicerec.exe_stat <> 'R') then
         raise_application_error (-20001, 'Impossible de creer une ecriture sur un exercice non ouvert ou non restreint.');
      end if;

-- si exercice restreint, changer la date
      if (exercicerec.exe_stat = 'R') then
         ecrdatenew := to_date ('31/12/' || exeordre, 'dd/mm/yyyy');
      end if;

      select ecriture_seq.nextval
      into   ecrordre
      from   dual;

      insert into ecriture
      values      (broordre,
                   comordre,
                   sysdate,
                   ecrdatenew,
                   'VALIDE',
                   ecrlibelle,
                   0,
                   ecrnumero_brouillard,
                   ecrordre,
                   ecrlibelle,
                   exeordre,
                   oriordre,
                   tjoordre,
                   topordre,
                   utlordre
                  );

      return ecrordre;
   end;

   function creerecrituredetailprivate (
      ecdcommentaire   varchar2,   -- (200),
      ecdlibelle       varchar2,   -- (200),
      ecdmontant       number,   -- (12,2) NOT NULL,
      ecdsecondaire    varchar2,   -- (20),
      ecdsens          varchar2,   -- (1)  NOT NULL,
      ecrordre         number,   --        NOT NULL,
      exeordre         number,   --        NOT NULL,
      gescode          varchar2,   -- (10)  NOT NULL,
      pconum           varchar2,   -- (20)  NOT NULL
      conordre          number
   )
      return integer
   is
      ecdordre    number;
      ecdcredit   number;
      ecddebit    number;
   begin
      select ecriture_detail_seq.nextval
      into   ecdordre
      from   dual;

      if ecdsens = 'C' then
         ecdcredit := ecdmontant;
         ecddebit := 0;
      else
         ecdcredit := 0;
         ecddebit := ecdmontant;
      end if;

      insert into maracuja.ecriture_detail
                  (ecd_commentaire,
                   ecd_credit,
                   ecd_debit,
                   ecd_index,
                   ecd_libelle,
                   ecd_montant,
                   ecd_ordre,
                   ecd_postit,
                   ecd_reste_emarger,
                   ecd_secondaire,
                   ecd_sens,
                   ecr_ordre,
                   exe_ordre,
                   ges_code,
                   pco_num,
                   con_ordre
                  )
      values      (ecdcommentaire,   -- (200),
                   ecdcredit,   -- (12,2),
                   ecddebit,   -- (12,2),
                   ecrordre,   --        NOT NULL,
                   ecdlibelle,   -- (200),
                   ecdmontant,   -- (12,2) NOT NULL,
                   ecdordre,   --        NOT NULL,
                   null,   -- (200),
                   abs (ecdmontant),   -- (12,2) NOT NULL,
                   ecdsecondaire,   -- (20),
                   ecdsens,   -- (1)  NOT NULL,
                   ecrordre,   --        NOT NULL,
                   exeordre,   --        NOT NULL,
                   gescode,   -- (10)  NOT NULL,
                   pconum,   -- (20)  NOT NULL
                   conordre
                  );

      return ecdordre;
   end;
end;
/


GRANT EXECUTE ON MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;

CREATE OR REPLACE PACKAGE MARACUJA.util
is
   procedure annuler_visa_bor_mandat (borid integer);

   procedure annuler_visa_bor_titre (borid integer);

   procedure supprimer_visa_btme (borid integer);

   procedure supprimer_visa_btms (borid integer);

   procedure supprimer_visa_btte (borid integer);

   procedure creer_ecriture_annulation (ecrordre integer);

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer;

   procedure annuler_emargement (emaordre integer);

   procedure supprimer_bordereau_dep (borid integer);

   procedure supprimer_bordereau_rec (borid integer);

   procedure supprimer_bordereau_btms (borid integer);

   procedure supprimer_bordereau_extourne (borid integer);

   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer);

   procedure supprimer_paiement_virement (paiordre integer);

   procedure corriger_etat_mandats;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);
   
 procedure log_operation(description varchar2);
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.util
is
   -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
   -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

   -- annulation du visa d'un bordereau de mandats (BTME + BTRU + BTMEX) (marche pas pour les bordereaux BTMS ni les OR)
   -- vider la table maracuja.zlog_util avant les appels
   procedure annuler_visa_bor_mandat (borid integer)
   is
      -- 3borid in 487,3493, 3485
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;
      tmpbordidex   bordereau.bor_id%type;
      is_btmex      integer;

      cursor lesmandats (aborid integer)
      is
         select man_id
         from   mandat
         where  bor_id = aborid;

      cursor lesecrs (amanid integer)
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, mandat_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = amanid;

      -- les bordereaux d'eextourne crees a partir du bordereau initial
      cursor lesbordex (aborid number)
      is
         select distinct m1.bor_id
         from            mandat mn inner join extourne_mandat me on me.man_id_n = mn.man_id
                         inner join mandat m1 on me.man_id_n1 = m1.man_id
         where           mn.bor_id = aborid;
   begin
   log_operation('ANNULATION VISA BORDEREAU '|| borid || '.');
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME' and tbotype <> 'BTRU' and tbotype <> 'BTMEX') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME ou BTRU ou BTMEX');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde.mde_origine not like 'VISA%';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandats associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      --si bordereau de credit d'extourne (sur N+1) il y aura des traitements particuliérs
      select count (*)
      into   flag
      from   mandat m inner join extourne_mandat me on me.man_id_n1 = m.man_id
      where  bor_id = borid;

      if (flag > 0) then
         is_btmex := 1;
      else
         is_btmex := 0;
      end if;

      --si bordereau ayant donne lieu a des mandats d''extourne, on supprime d'abord les credits d'extourne crees
      select count (*)
      into   flag
      from   mandat mn inner join extourne_mandat me on me.man_id_n = mn.man_id
             inner join mandat m1 on me.man_id_n1 = m1.man_id
      where  mn.bor_id = borid;

      if (flag > 0) then
          log_operation('Le bordereau '|| borid || ' a donne lieu a des mandats d''extourne, on supprime d''abord les mandats d''extourne avant de traiter le bordereau en cours.');
         open lesbordex (borid);

         loop
            -- on parcoure les bordereux d'extourne, on annule et supprime tout
            fetch lesbordex
            into  tmpbordidex;

            exit when lesbordex%notfound;
            supprimer_bordereau_extourne (tmpbordidex);
         end loop;

         close lesbordex;
      end if;

      open lesmandats (borid);

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         open lesecrs (tmpmanid);

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from mandat_detail_ecriture
               where man_id = tmpmanid;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
--
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

      --                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

      --                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression du bordereau de mandats d'extourne (avec depenses et engagements associés)
   procedure supprimer_bordereau_extourne (borid integer)
   is
      tmpdepid      jefy_depense.depense_budget.dep_id%type;
      tmpengid      jefy_depense.engage_budget.eng_id%type;
      tmpdppid      jefy_depense.depense_papier.dpp_id%type;
      tmputlordre   jefy_depense.depense_budget.utl_ordre%type;
      tmpmanidex    maracuja.mandat.man_id%type;

      cursor lesmandats (aborid integer)
      is
         select man_id
         from   mandat
         where  bor_id = aborid;

      -- les depenses et engagements de jefy_depense
      cursor lesdepjefydep (amanid integer)
      is
         select dep.dep_id,
                dep.eng_id,
                dep.dpp_id,
                dep.utl_ordre
         from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.depense_budget dep on dpco.dep_id = dep.dep_id
                inner join maracuja.depense d on d.dep_ordre = dpco.dpco_id
         where  d.man_id = amanid;
   begin
      log_operation('SUPPRESSION DU BORDEREAU D''EXTOURNE bor_id='|| borid || '.');
      log_operation('Annulation visa bordereau bor_id='|| borid || '.');
      annuler_visa_bor_mandat (borid);

      -- on parcourt les mandats
      log_operation('Parcours des mandats bor_id='|| borid || '.');
      open lesmandats (borid);

      loop
         fetch lesmandats
         into  tmpmanidex;

         exit when lesmandats%notfound;
        log_operation('traitement mandat man_id='|| tmpmanidex || '.');
         --on parcours les depenses
         open lesdepjefydep (tmpmanidex);

         loop
            fetch lesdepjefydep
            into  tmpdepid,
                  tmpengid,
                  tmpdppid,
                  tmputlordre;

            exit when lesdepjefydep%notfound;
            
            
            
            
            
            -- supprimer les depense
            delete from jefy_depense.extourne_liq where DEP_ID_N1 = tmpdepid;
            
            update jefy_depense.depense_ctrl_planco set man_id=null where dep_id=tmpdepid;
            log_operation('jefy_depense.liquider.del_depense_budget dep_id='|| tmpdepid || ' / utl_ordre='||tmputlordre ||'.');
            jefy_depense.liquider.del_depense_budget (tmpdepid, tmputlordre);
            log_operation('jefy_depense.liquider.del_depense_papier dpp_id='|| tmpdppid || ' / utl_ordre='||tmputlordre ||'.');
            jefy_depense.reverser.del_reverse_papier (tmpdppid, tmputlordre);
            -- supprimer les engagements
            log_operation('jefy_depense.engager.del_engage eng_id='|| tmpengid || ' / utl_ordre='||tmputlordre ||'.');
            jefy_depense.engager.del_engage (tmpengid, tmputlordre);
         end loop;

         close lesdepjefydep;

         -- supprimer les enregistrements de mandat_extourne
         log_operation('suppression extourne_mandat man_id_n1='|| tmpmanidex || '.');
         delete from maracuja.extourne_mandat
               where (man_id_n1 = tmpmanidex);
      end loop;

      close lesmandats;

      -- supprimer bordereau + mandats + etc.
      log_operation('supprimer_bordereau_dep borid='|| borid || '.');
      supprimer_bordereau_dep (borid);
      log_operation('Suppression du bordereau d''extourne effectuee bor_id='|| borid || '.');
   end;

   procedure annuler_visa_bor_titre (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      titre.tit_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmpmanid;

         exit when lestitres%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from titre_detail_ecriture
               where tit_id = tmpmanid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmpmanid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
   procedure supprimer_visa_btme (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde.mde_origine not like 'VISA%';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         select count (*)
         into   flag
         from   ecriture_detail ecd, mandat_detail_ecriture mde
         where  ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

         if (flag > 0) then
            select distinct ecr_ordre
            into            tmpecrordre
            from            ecriture_detail ecd, mandat_detail_ecriture mde
            where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

            delete from ecriture_detail
                  where ecr_ordre = tmpecrordre;

            delete from ecriture
                  where ecr_ordre = tmpecrordre;

            delete from mandat_detail_ecriture
                  where man_id = tmpmanid;
         end if;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTMS
   procedure supprimer_visa_btms (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesecritures
      is
         select distinct ed.ecr_ordre
         from            ecriture_detail ed, mandat_detail_ecriture mde, mandat m
         where           m.man_id = mde.man_id and mde.ecd_ordre = ed.ecd_ordre and m.bor_id = borid;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTMS') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTMS');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat = 'VALIDE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

      --            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;
      open lesecritures;

      loop
         fetch lesecritures
         into  tmpecrordre;

         exit when lesecritures%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre in (select ecd_ordre
                                   from   ecriture_detail
                                   where  ecr_ordre = tmpecrordre);

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;
      end loop;

      close lesecritures;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;

      update jefy_paye.jefy_paye_compta
         set ecr_ordre_visa = null,
             ecr_ordre_sacd = null,
             ecr_ordre_opp = null
       where bor_id in (borid);
   end;

   -- annulation du visa d'un bordereau de depense BTTE
   procedure supprimer_visa_btte (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmptitid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmptitid;

         exit when lestitres%notfound;

         select distinct ecr_ordre
         into            tmpecrordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmptitid;

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;

         delete from titre_detail_ecriture
               where tit_id = tmptitid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmptitid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure creer_ecriture_annulation (ecrordre integer)
   is
      ecr           maracuja.ecriture%rowtype;
      ecd           maracuja.ecriture_detail%rowtype;
      flag          integer;
      str           ecriture.ecr_libelle%type;
      newecrordre   ecriture.ecr_ordre%type;
      newecdordre   ecriture_detail.ecd_ordre%type;
      x             integer;

      cursor c1
      is
         select *
         from   maracuja.ecriture_detail
         where  ecr_ordre = ecrordre;
   begin
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucune ecriture retrouvee pour ecr_ordre= ' || ecrordre || ' .');
      end if;

      select *
      into   ecr
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      str := 'Annulation Ecriture ' || ecr.exe_ordre || '/' || ecr.ecr_numero;

      -- verifier que l'ecriture n'a pas deja ete annulee
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_libelle = str;

      if (flag > 0) then
         select ecr_numero
         into   flag
         from   maracuja.ecriture
         where  ecr_libelle = str;

         raise_application_error (-20001, 'L''ecriture numero ' || ecr.ecr_numero || ' (ecr_ordre= ' || ecrordre || ') a deja ete annulee par l''ecriture numero ' || flag || '.');
      end if;

      -- verifier que les details ne sont pas emarges
      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;

         if (ecd.ecd_reste_emarger < abs (ecd.ecd_montant)) then
            raise_application_error (-20001, 'L''ecriture ' || ecr.ecr_numero || ' ecr_ordre= ' || ecrordre || ' a ete emargee pour le compte ' || ecd.pco_num || '. Impossible d''annuler');
         end if;
      end loop;

      close c1;

--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--
--

      --             END LOOP;
--             CLOSE c1;
      newecrordre := api_plsql_journal.creerecriture (ecr.com_ordre, sysdate, str, ecr.exe_ordre, ecr.ori_ordre, ecr.tjo_ordre, ecr.top_ordre, ecr.utl_ordre);

      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;
         newecdordre := api_plsql_journal.creerecrituredetail (ecd.ecd_commentaire, ecd.ecd_libelle, -ecd.ecd_montant, ecd.ecd_secondaire, ecd.ecd_sens, newecrordre, ecd.ges_code, ecd.pco_num);
         x := creeremargementmemesens (ecd.ecd_ordre, newecdordre, 1);
      end loop;

      close c1;

      numerotationobject.numeroter_ecriture (newecrordre);
   end;

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer
   is
   begin
      return api_emargement.creeremargementmemesens (ecdordresource, ecdordredest, typeemargement);
   end;

--
--       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
--            emaordre EMARGEMENT.EMA_ORDRE%type;
--            ecdSource ecriture_detail%rowtype;
--            ecdDest ecriture_detail%rowtype;
--
--            utlOrdre ecriture.utl_ordre%type;
--            comOrdre ecriture.com_ordre%type;
--            exeOrdre ecriture.exe_ordre%type;
--            emaMontant emargement.ema_montant%type;
--            flag integer;
--       BEGIN

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
--            end if;

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
--            end if;

   --            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

   --            -- verifier que les ecriture_detail sont sur le meme sens
--            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;
--
--            -- verifier que les ecriture_detail ont le meme compte
--            if (ecdSource.pco_num <> ecdDest.pco_num) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les ecriture_detail ne sont pas emargees
--            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
--            end if;
--            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
--            end if;

   --            -- verifier que les montant s'annulent
--            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
--                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les exercices sont les memes
--            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
--                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- trouver le montant a emarger
--            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;

   --            -- trouver l'utilisateur
--            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;

   --            -- creation de l emargement
--             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

   --             INSERT INTO EMARGEMENT
--              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
--             VALUES
--              (
--              SYSDATE,
--              0,
--              EMAORDRE,
--              ecdSource.exe_ordre,
--              typeEmargement,
--              utlOrdre,
--              comordre,
--              emaMontant,
--              'VALIDE'
--              );

   --              -- creation de l emargement detail
--              INSERT INTO EMARGEMENT_DETAIL
--              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
--              VALUES
--              (
--              ecdSource.ecd_ordre,
--              ecdDest.ecd_ordre,
--              EMAORDRE,
--              emaMontant,
--              emargement_detail_seq.NEXTVAL,
--              exeOrdre
--              );

   --              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
--              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;

   --              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);

   --              return emaOrdre;
--       END;
--
--
   procedure annuler_emargement (emaordre integer)
   as
   begin
      api_emargement.annuler_emargement (emaordre);
   end;

   procedure supprimer_bordereau_dep (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de depense.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les mandat_brouillards
      delete from maracuja.mandat_brouillard
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- supprimer les depenses
      delete from maracuja.depense
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- mettre a jour les depense_ctrl_planco
      update jefy_depense.depense_ctrl_planco
         set man_id = null
       where man_id in (select man_id
                        from   maracuja.mandat
                        where  bor_id = borid);

      -- supprimer les mandats
      delete from maracuja.mandat
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   procedure supprimer_bordereau_btms (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau b, type_bordereau tb
      where  bor_id = borid and b.tbo_ordre = tb.tbo_ordre and tb.tbo_type = 'BTMS';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas ou n''est pas de type BTMS');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      update jefy_paye.jefy_paye_compta
         set jpc_etat = 'LIQUIDEE',
             bor_id = null
       where bor_id in (borid);

      --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
      supprimer_bordereau_dep (borid);
   end;

   procedure supprimer_bordereau_rec (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de recette.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les titre_brouillards
      delete from maracuja.titre_brouillard
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- supprimer les recettes
      delete from maracuja.recette
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- mettre a jour les recette_ctrl_planco
      update jefy_recette.recette_ctrl_planco
         set tit_id = null
       where tit_id in (select tit_id
                        from   maracuja.titre
                        where  bor_id = borid);

      -- supprimer les titres
      delete from maracuja.titre
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer)
   is
      flag             integer;
      lerecouvrement   recouvrement%rowtype;
      tmpemaordre      emargement.ema_ordre%type;
      tmpecrordre      ecriture.ecr_ordre%type;

      cursor emargements
      is
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_source in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))))
         union
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_destination in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))));

      cursor ecritures
      is
         select distinct e.ecr_ordre
         from            ecriture_detail ecd, ecriture e
         where           ecd.ecr_ordre = e.ecr_ordre and e.ecr_numero >= ecrnumeromin and e.ecr_numero <= ecrnumeromax and ecd_ordre in (select ecd_ordre
                                                                                                                                         from   titre_detail_ecriture
                                                                                                                                         where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                                                                                                           from   echeancier
                                                                                                                                                                                           where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                                                                                                            from   prelevement
                                                                                                                                                                                                                            where  reco_ordre = recoordre)));
   begin
      -- recuperer les numeros des ecritures min et max pour les passer en parametre
      --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
      select count (*)
      into   flag
      from   prelevement
      where  reco_ordre = recoordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun prelevement correspondant a recoordre= ' || recoordre);
      end if;

      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- verifier les dates des ecritures avec la date du prelevement
      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromin;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromax;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      -- supprimer les emargements
      open emargements;

      loop
         fetch emargements
         into  tmpemaordre;

         exit when emargements%notfound;
         annuler_emargement (tmpemaordre);
      end loop;

      close emargements;

      -- modifier les etats des prelevements
      update prelevement
         set prel_etat_maracuja = 'ATTENTE'
       where reco_ordre = recoordre;

      --supprimer le contenu du fichier (prelevement_fichier)
      delete from prelevement_fichier
            where reco_ordre = recoordre;

      -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
      open ecritures;

      loop
         fetch ecritures
         into  tmpecrordre;

         exit when ecritures%notfound;
         creer_ecriture_annulation (tmpecrordre);
      end loop;

      close ecritures;

      delete from titre_detail_ecriture
            where ecd_ordre in (
                         select distinct ecd.ecd_ordre
                         from            ecriture_detail ecd, ecriture e
                         where           ecd.ecr_ordre = e.ecr_ordre
                         and             e.ecr_numero >= ecrnumeromin
                         and             e.ecr_numero <= ecrnumeromax
                         and             ecd_ordre in (select ecd_ordre
                                                       from   titre_detail_ecriture
                                                       where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                         from   echeancier
                                                                                                         where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                          from   prelevement
                                                                                                                                          where  reco_ordre = recoordre))));

      -- supprimer association des prélèvements au recouvrement
      update prelevement
         set reco_ordre = null
       where reco_ordre = recoordre;

      -- supprimer l'objet recouvrement
      delete from recouvrement
            where reco_ordre = recoordre;
--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))

   --        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--
   end;

   -- suppression d'un paiement de type virement avec annulation des émargements et écritures de paiements
   procedure supprimer_paiement_virement (paiordre integer)
   is
      flag       integer;
      ecdp       ecriture_detail%rowtype;
      ecdordre   ecriture_detail.ecd_ordre%type;
      emaordre   emargement.ema_ordre%type;
      ecrordre   ecriture.ecr_ordre%type;

      cursor ecdpaiement5
      is
         select ecd.*
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.man_id = mde.man_id
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    mde_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.*
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.odp_ordre = mde.odp_ordre
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    ope_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%';

      cursor ecdpaiementall
      is
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor ecrordres
      is
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor emaordreforecd
      is
         select emd.ema_ordre
         from   emargement_detail emd, emargement ema
         where  ema.ema_ordre = emd.ema_ordre and ema_etat = 'VALIDE' and ecd_ordre_source = ecdordre or ecd_ordre_destination = ecdordre;
   begin
      select count (*)
      into   flag
      from   maracuja.paiement
      where  pai_ordre = paiordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun paiement trouvé correspondant a pai_ordre= ' || paiordre);
      end if;

      --verifier que classe 5 non émargée
      open ecdpaiement5;

      loop
         fetch ecdpaiement5
         into  ecdp;

         exit when ecdpaiement5%notfound;

         if (ecdp.ecd_reste_emarger <> abs (ecdp.ecd_credit)) then
            raise_application_error (-20001, 'L''écriture ' || ecdp.pco_num || ' - ' || ecdp.ecd_libelle || ' a été émargée, supprimez l''émargement.');
         end if;
      end loop;

      close ecdpaiement5;

      -- supprimer tous les emargements
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         open emaordreforecd;

         loop
            fetch emaordreforecd
            into  emaordre;

            exit when emaordreforecd%notfound;
            api_emargement.annuler_emargement (emaordre);
         end loop;

         close emaordreforecd;
      end loop;

      close ecdpaiementall;

      --  creer ecritures annulation
      open ecrordres;

      loop
         fetch ecrordres
         into  ecrordre;

         exit when ecrordres%notfound;
         creer_ecriture_annulation (ecrordre);
      end loop;

      close ecrordres;

      -- supprimer les mde et ope
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre = ecdordre;

         delete from odpaiem_detail_ecriture
               where ecd_ordre = ecdordre;
      end loop;

      close ecdpaiementall;

      -- changer etats des mandats et ODP
      update mandat
         set man_etat = 'VISE',
             pai_ordre = null
       where pai_ordre = paiordre;

      update ordre_de_paiement
         set odp_etat = 'VALIDE',
             pai_ordre = null
       where pai_ordre = paiordre;

      -- supprimer le paiement
      delete from virement_fichier
            where pai_ordre = paiordre;

      delete from paiement
            where pai_ordre = paiordre;
   end;

   procedure corriger_etat_mandats
   is
   begin
      update maracuja.mandat
         set man_etat = 'VISE'
       where man_id in (select m.man_id
                        from   maracuja.mandat m, maracuja.mandat_detail_ecriture mde
                        where  m.man_id = mde.man_id and m.man_etat = 'ATTENTE' and mde.mde_origine = 'VISA');
   end;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type
   is
      flag     integer;
      pconum   plan_comptable_exer.pco_num%type;
   begin
      select count (*)
      into   flag
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' n''a pas été trouvé dans la table maracuja.parametre pour l''exercice ' || exeordre);
      end if;

      select par_value
      into   pconum
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      select count (*)
      into   flag
      from   plan_comptable_exer
      where  pco_num = pconum and exe_ordre = exeordre and pco_validite = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' contient la valeur ' || pconum || ' qui ne correspond pas a un compte valide du plan comptable de ' || exeordre);
      end if;

      return pconum;
   end;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   parametre
      where  exe_ordre = exeordre and par_key = parkey;

      if (flag = 0) then
         insert into parametre
                     (par_ordre,
                      exe_ordre,
                      par_description,
                      par_key,
                      par_value
                     )
         values      (parametre_seq.nextval,
                      exeordre,
                      pardescription,
                      parkey,
                      parvalue
                     );
      end if;
   end;

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat <> 'C';

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;


   procedure log_operation (description varchar2)
   is
   begin
      insert into maracuja.zlog_util
                  (zlog_id,
                   zlog_when,
                   zlog_desc
                  )
      values      (zlog_util_seq.nextval,
                   sysdate,
                   description
                  );
   end;
end;
/



CREATE OR REPLACE PACKAGE MARACUJA.extourne
is
   -- constantes
   c_mod_dom_a_extourner   constant maracuja.mode_paiement.mod_dom%type   := 'A EXTOURNER';

   -- bordidn : bordereau sur l'exercice n
   -- bordidn1 : bordereau cree sur l'exercice n+1 par cette procedure
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type);

   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type);

   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer;

   function is_dpco_extourne_def (dpcoid integer)
      return integer;

   function is_dpco_extourne (dpcoid integer)
      return integer;

   function is_exercice_existe (exeordre integer)
      return integer;

   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type);

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type;

   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type;

   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type;

   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type;

   function get_orgid_reprise (exeOrdreNew integer, orgid jefy_admin.organ.org_id%type)
      return jefy_admin.organ.org_id%type;
      
   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type;
      
      
end extourne;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.extourne
is
   -- creer le bordereau de mandats d'extourne sur N+1(ainsi que les depenses sur jefy_depense) a partir du bordereau de mandats a extourner
   -- il faut que les UB de N+1 soient les memes pour un bordereau
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type)
   is
      bordereau_n     bordereau%rowtype;
      mandat_n        mandat%rowtype;
      depense_n       depense%rowtype;
      flag            integer;
      depidn          jefy_depense.depense_budget.dep_id%type;
      depidn1         jefy_depense.depense_budget.dep_id%type;
      ubnew           bordereau.ges_code%type;

      cursor c_mandat_n
      is
         select   m.*
         from     mandat m, mode_paiement mp
         where    m.mod_ordre = mp.mod_ordre and m.bor_id = bordereau_n.bor_id and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE'
         order by ges_code, man_numero;

      cursor c_depense_n
      is
         select d.*
         from   depense d
         where  man_id = mandat_n.man_id;

      cursor c_depensectrlplanco_n1
      is
         select dpco.*
         from   jefy_depense.depense_ctrl_planco dpco
         where  dep_id = depidn1;

      chainedpcoids   varchar2 (30000);
      exeordren1      jefy_admin.exercice.exe_ordre%type;
      res             integer;
      manidn1         mandat.man_id%type;
   begin
      select *
      into   bordereau_n
      from   bordereau
      where  bor_id = boridn;

      exeordren1 := bordereau_n.exe_ordre + 1;

      -- verifier que le bordereau d'origine des mandats avec mode de paiement extourne
      if (is_bord_aextourner (boridn) = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner".');
      end if;

      -- verifier que le bordereau d'origine est visé
      if (bordereau_n.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau initial (bord_id=' || boridn || ' n''est pas vise.');
      end if;

      -- verifier qu'au moins un mandats a extourner du bordereau d'origine sont vises
      select count (*)
      into   flag
      from   mandat m, mode_paiement mp
      where  m.mod_ordre = mp.mod_ordre and bor_id = boridn and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner" qui soit VISE.');
      end if;

      -- verifier que l'exercice n+1 existe
      if (is_exercice_existe (exeordren1) = 0) then
         raise_application_error (-20001, 'L''exercice ' || (exeordren1) || ' n''existe pas, impossible de réserver des crédts d''extourne. Créez l''exercice en question ou attendez qu''il soit créé pour viser le bordereau initial.');
      end if;

      -- lister les UB de reprise présentes par mandat. si elles sont multiples, exception
      select count (*)
      into   flag
      from   (select   man_id,
                       man_numero,
                       ges_code,
                       count (*)
              from     (select m.man_id,
                               m.man_numero,
                               m.ges_code,
                               dep_id,
                               org_id_ub
                        from   mandat m inner join depense d on m.man_id = d.man_id
                               inner join jefy_admin.v_ub_for_organ on org_id = extourne.get_orgid_reprise (d.exe_ordre, d.org_ordre)
                        where  bor_id = boridn)
              group by man_id, man_numero, ges_code
              having   count (*) > 1);

      if (flag > 0) then
         raise_application_error
            (-20001,
             'Le bordereau initial de l''exercice N  contient des mandats passés sur des lignes budgétaires cloturées. Les lignes de reprises configurées pointent vers des UB différentes, il est impossible de créer un bordereau d''extourne sur deux UB. Solution : regroupez les liquidations de N par UB de reprise pour créer le bordereau.'
            );
      end if;

      -- récupérer le nouveau ges_code
      select org_ub
      into   ubnew
      from   jefy_admin.organ o
             inner join
             (select distinct org_id_ub
              from            mandat m inner join depense d on m.man_id = d.man_id
                              inner join jefy_admin.v_ub_for_organ on org_id = extourne.get_orgid_reprise (d.exe_ordre, d.org_ordre)
              where           bor_id = boridn) x on x.org_id_ub = o.org_id
             ;

      -- creer bordereau sur N+1
      boridn1 := bordereau_abricot.get_num_borid (get_tboordre_mandat_ext, exeordren1, ubnew, bordereau_n.utl_ordre);

      -- créer chaque dépense négative sur n+1 correspondant à chaque dépense de n (intégrée dans un mandat d'extourne visé)
      open c_mandat_n;

      loop
         fetch c_mandat_n
         into  mandat_n;

         exit when c_mandat_n%notfound;
         chainedpcoids := '';

         open c_depense_n;

         loop
            fetch c_depense_n
            into  depense_n;

            exit when c_depense_n%notfound;
            -- creer la depense negative jefy_depense sur N+1
            depidn1 := prv_creer_jdep_n1 (depense_n);

            -- memoriser lien dep_idn/dep_idn1
            select dep_id
            into   depidn
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = depense_n.dep_ordre;

            insert into jefy_depense.extourne_liq
                        (el_id,
                         dep_id_n,
                         dep_id_n1
                        )
               select jefy_depense.extourne_liq_seq.nextval,
                      depidn,
                      depidn1
               from   dual;

            chainedpcoids := chainedpcoids || prv_chaine_dpcoids (depidn1) || '$';
         end loop;

         close c_depense_n;

         chainedpcoids := chainedpcoids || '$';
         -- creer mandat et brouillards sur N+1
         manidn1 := bordereau_abricot.set_mandat_depenses (chainedpcoids, boridn1);

         -- memoriser le lien man_idn/man_idn1
         insert into maracuja.extourne_mandat
                     (em_id,
                      man_id_n,
                      man_id_n1
                     )
            select maracuja.extourne_mandat_seq.nextval,
                   mandat_n.man_id,
                   manidn1
            from   dual;

         -- creer les depenses Maracuja sur N+1
         bordereau_abricot.get_depense_jefy_depense (manidn1);
         -- brouillard du mandat
         prv_creer_brouillard_inverse (mandat_n.man_id, manidn1);
      end loop;

      close c_mandat_n;

      bordereau_abricot.numeroter_bordereau (boridn1);
      bordereau_abricot.controle_bordereau (boridn1);
--   exception
--      when others then
--         raise_application_error (-20001, '[Creer Mandats Extourne]' || sqlerrm);
   end;

   -- renvoi >0 si le bordereau contient des mandats non rejetes passes sur un mode de paiement à extourner.
   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   mandat m inner join mode_paiement mp on (m.mod_ordre = mp.mod_ordre)
      where  bor_id = borid and mod_dom = c_mod_dom_a_extourner and m.brj_ordre is null and man_ttc > 0;

      return cpt;
   end;

   function is_exercice_existe (exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   jefy_admin.exercice
      where  exe_ordre = exeordre;

      return cpt;
   end;

   -- creation de la depense jefy_depense sur N+1 a partir de la depense maracuja sur N
   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type
   is
      a_dep_id                number;
      a_exe_ordre             number;
      a_dpp_id                number;
      a_dep_ht_saisie         number;
      a_dep_ttc_saisie        number;
      a_fou_ordre             number;
      a_org_id                number;
      a_tcd_ordre             number;
      a_tap_id                number;
      a_eng_libelle           varchar2 (500);
      a_tyap_id               number;
      a_utl_ordre             number;
      a_dep_id_reversement    number;
      a_mod_ordre             number;
      a_chaine_action         varchar2 (30000);
      a_chaine_analytique     varchar2 (30000);
      a_chaine_convention     varchar2 (30000);
      a_chaine_hors_marche    varchar2 (30000);
      a_chaine_marche         varchar2 (30000);
      a_chaine_planco         varchar2 (30000);
      jdepense_ctrl_plancon   jefy_depense.depense_ctrl_planco%rowtype;
      jdepense_budgetn        jefy_depense.depense_budget%rowtype;
      jengage_budgetn         jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   jdepense_ctrl_plancon
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = depense_n.dep_ordre and man_id = depense_n.man_id;

      select *
      into   jdepense_budgetn
      from   jefy_depense.depense_budget
      where  dep_id = jdepense_ctrl_plancon.dep_id;

      select *
      into   jengage_budgetn
      from   jefy_depense.engage_budget
      where  eng_id = jdepense_budgetn.eng_id;

      a_dep_id := null;
      a_exe_ordre := jdepense_ctrl_plancon.exe_ordre + 1;
      a_dpp_id := null;
      a_dep_ht_saisie := -jdepense_ctrl_plancon.dpco_ht_saisie;
      a_dep_ttc_saisie := -jdepense_ctrl_plancon.dpco_ttc_saisie;
      a_fou_ordre := depense_n.fou_ordre;
      a_org_id := get_orgid_extourne (depense_n);
      a_tcd_ordre := get_tcdordre_extourne (depense_n);
      a_tap_id := jdepense_budgetn.tap_id;
      a_eng_libelle := substr ('Extourne : ' || jengage_budgetn.eng_libelle, 1, 500);
      a_tyap_id := jengage_budgetn.tyap_id;
      a_utl_ordre := jdepense_budgetn.utl_ordre;
      a_dep_id_reversement := null;
      -- on ne recupere pas les actions
      a_chaine_action := '$';
      -- on ne recupere pas les codes nomenclature
      a_chaine_hors_marche := '$';
      a_chaine_analytique := prv_chaine_analytique_dep (jdepense_budgetn.dep_id);
      a_chaine_convention := prv_chaine_convention_dep (jdepense_budgetn.dep_id);
      a_chaine_marche := prv_chaine_marche_dep (jdepense_budgetn.dep_id);
      a_chaine_planco := prv_chaine_planco_dep (jdepense_budgetn.dep_id);
      a_mod_ordre := get_modordre_extourne (depense_n);
      jefy_depense.reverser.ins_reverse_papier (a_dpp_id, a_exe_ordre, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie, a_fou_ordre, null, a_mod_ordre, sysdate, sysdate, sysdate, sysdate, 1, a_utl_ordre, null);
      jefy_depense.liquider.ins_depense_directe (a_dep_id,
                                                 a_exe_ordre,
                                                 a_dpp_id,
                                                 a_dep_ht_saisie,
                                                 a_dep_ttc_saisie,
                                                 a_fou_ordre,
                                                 a_org_id,
                                                 a_tcd_ordre,
                                                 a_tap_id,
                                                 a_eng_libelle,
                                                 a_tyap_id,
                                                 a_utl_ordre,
                                                 a_dep_id_reversement,
                                                 a_chaine_action,
                                                 a_chaine_analytique,
                                                 a_chaine_convention,
                                                 a_chaine_hors_marche,
                                                 a_chaine_marche,
                                                 a_chaine_planco
                                                );
      return a_dep_id;
   end;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine               varchar2 (30000);
      chaine_inventaires   varchar2 (30000);
      repart               jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.pco_num || '$-' || repart.dpco_ht_saisie || '$-' || repart.dpco_ttc_saisie || '$' || repart.ecd_ordre || '$';
         -- FIXME recuperer les inventaires ????
         chaine_inventaires := '||';
         chaine := chaine || chaine_inventaires || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_action%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_action
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.tyac_id || '$-' || repart.dact_ht_saisie || '$-' || repart.dact_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_analytique%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_analytique
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.can_id || '$-' || repart.dana_ht_saisie || '$-' || repart.dana_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_convention%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_convention
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.conv_ordre || '$-' || repart.dcon_ht_saisie || '$-' || repart.dcon_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_hors_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.typa_id || '$' || repart.ce_ordre || '$-' || repart.dhom_ht_saisie || '$-' || repart.dhom_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.att_ordre || '$-' || repart.dmar_ht_saisie || '$-' || repart.dmar_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.dpco_id || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   -- creer un brouillard inverse a celui d'origine
   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type)
   is
   begin
      -- inverser le sens et l'affecter au mandat d'extourne
      delete from mandat_brouillard
            where man_id = manidn1;

      insert into maracuja.mandat_brouillard
                  (ecr_ordre,
                   exe_ordre,
                   ges_code,
                   mab_montant,
                   mab_operation,
                   mab_ordre,
                   mab_sens,
                   man_id,
                   pco_num
                  )
         select null,
                exe_ordre + 1,
                ges_code,
                mab_montant,
                mab_operation,
                mandat_brouillard_seq.nextval,
                decode (mab_sens, 'C', 'D', 'C'),
                manidn1,
                pco_num
         from   mandat_brouillard
         where  man_id = manidn;
   end;

   -- viser automatiquement le bordereau de mandats d'extourne en entier
   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type)
   is
      lemandat   mandat%rowtype;
      flag       integer;

      cursor lesmandats
      is
         select *
         from   mandat
         where  bor_id = boridn1;
   begin
      -- verifier que le bordereau existe
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''existe pas.');
      end if;

      -- verifier que le bordereau  est valide
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1 and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''est pas à l''etat VALIDE.');
      end if;

      -- verifier s'il y a des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' ne contient pas de mandats.');
      end if;

      -- viser chaque mandat
      open lesmandats;

      loop
         fetch lesmandats
         into  lemandat;

         exit when lesmandats%notfound;

         -- verifier que le mandat est bien un madat d''extourne
         select count (*)
         into   flag
         from   mandat m, extourne_mandat em
         where  m.man_id = em.man_id_n1 and m.man_id = lemandat.man_id;

         if (flag = 0) then
            raise_application_error (-20001, 'Le mandat man_id ' || lemandat.man_id || ' n''est pas un mandat d''extourne.');
         end if;

         api_mandat.viser_mandat (lemandat.man_id, datevisa, utlordre, 'Extourne');
      end loop;

      close lesmandats;

      -- mettre a jour le bordereau
      update bordereau
         set bor_etat = 'VISE',
             bor_date_visa = datevisa,
             utl_ordre_visa = utlordre
       where bor_id = boridn1;
--   exception
--      when others then
--         raise_application_error (-20001, '[Viser Mandats Extourne] ' || sqlerrm);
   end;

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type
   is
   begin
      return 50;
   end;

   -- renvoie le mod_ordre pour le mandat d'extourne
   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type
   is
      modordren1      mode_paiement.mod_ordre%type;
      modepaiementn   mode_paiement%rowtype;
      flag            integer;
   begin
      modordren1 := null;

      select *
      into   modepaiementn
      from   mode_paiement
      where  mod_ordre = ladepensen.mod_ordre;

      select count (*)
      into   flag
      from   mode_paiement
      where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';

      if (flag > 0) then
         select mod_ordre
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (modordren1 is null) then
         select count (*)
         into   flag
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';

         if (flag = 0) then
            raise_application_error (-20001, 'Aucun mode de paiement interne trouvé sur  ' || (modepaiementn.exe_ordre + 1) || ' pour l''affectation au mandat d''extourne.');
         end if;

         select max (mod_ordre)
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';
      end if;

      return modordren1;
   end;

   -- renvoie le tcd_ordre pour le mandat d'extourne
   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type
   is
      tcdordren1    jefy_admin.type_credit.tcd_ordre%type;
      typecreditn   jefy_admin.type_credit%rowtype;
      flag          integer;
   begin
      tcdordren1 := null;

      select *
      into   typecreditn
      from   jefy_admin.type_credit
      where  tcd_ordre = ladepensen.tcd_ordre;

      select count (*)
      into   flag
      from   jefy_admin.type_credit
      where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;

      if (flag > 0) then
         select tcd_ordre
         into   tcdordren1
         from   jefy_admin.type_credit
         where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (tcdordren1 is null) then
         raise_application_error (-20001, 'Le type de crédit ' || (typecreditn.tcd_code) || ' n''est pas valide pour l''exercice ' || (typecreditn.exe_ordre + 1) || '. Impossible de créer les crédits d''extourne.');
      end if;

      return tcdordren1;
   end;

-- renvoie le orgid de reprise s''il c'est pertinent , sinon renvoie org_id ou
-- une exception si le org_id est cloture sans ligne de reprise configuree
   function get_orgid_reprise (exeordrenew integer, orgid jefy_admin.organ.org_id%type)
      return jefy_admin.organ.org_id%type
   is
      orgid1       jefy_admin.organ.org_id%type;
      organn       jefy_admin.organ%rowtype;
      organn1      jefy_admin.organ%rowtype;
      flag         integer;
      datedebut1   date;
   begin
      orgid1 := null;
      datedebut1 := to_date ('01/01/' || exeordrenew, 'dd/mm/yyyy');

      select *
      into   organn
      from   jefy_admin.organ
      where  org_id = orgid;

      -- si la ligne est fermée sur l'exercice n+1 on prend la ligne de reprise
      if (organn.org_date_cloture is not null and organn.org_date_cloture < datedebut1) then
         orgid1 := organn.org_id_reprise;

         if (orgid1 is null) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') n''est pas ouverte pour l''exercice '
                                     || exeordrenew
                                     || ', et aucune ligne de reprise n''a été définie. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise.'
                                    );
         end if;

         select *
         into   organn1
         from   jefy_admin.organ
         where  org_id = orgid1;

         if ((organn1.org_date_cloture is not null and organn1.org_date_cloture < datedebut1) or organn1.org_date_ouverture > datedebut1) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') a une ligne de reprise définie ( '
                                     || organn1.org_etab
                                     || organn1.org_ub
                                     || '/'
                                     || organn1.org_cr
                                     || '/'
                                     || organn1.org_souscr
                                     || ') mais celle-ci n''est pas ouverte pour l''exercice '
                                     || exeordrenew
                                     || '. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise ouverte.'
                                    );
         end if;
      else
         orgid1 := organn.org_id;
      end if;

      return orgid1;
   end;

   -- renvoie le orgid a utiliser pour le mandat d'extourne
   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type
   is
   begin
      return get_orgid_reprise ((ladepensen.exe_ordre + 1), ladepensen.org_ordre);
   end;

   -- s'agit-il d'une liquidation définitive sur crédit d'extourne ?
   function is_dpco_extourne_def (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq_def eld on (dpco.dep_id = eld.dep_id)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- s'agit-il d'une liquidation d'extourne ?
   function is_dpco_extourne (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq el on (dpco.dep_id = el.dep_id_n1)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- renvoie le montant budgétaire calculé de la liquidation
   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type
   as
      db     jefy_depense.depense_budget%rowtype;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;
      eb     jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   dpco
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select *
      into   db
      from   jefy_depense.depense_budget
      where  dep_id = dpco.dep_id;

      select *
      into   eb
      from   jefy_depense.engage_budget
      where  eng_id = db.eng_id;

      return jefy_depense.budget.calculer_budgetaire (db.exe_ordre, db.tap_id, eb.org_id, dpco.dpco_ht_saisie, dpco.dpco_ttc_saisie);
   end;
end extourne;
/


CREATE OR REPLACE PACKAGE MARACUJA."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco2(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    procedure checkCadre2VsBalance(exeOrdre INTEGER);
    procedure checkEcritureDetailGestion(exeordre integer); 
    procedure checkMandatVsJefyDepense(exeordre integer); 
    procedure checkMontantsEmargements(exeOrdre integer);
    procedure checkEcritureDates(exeordre integer);
    procedure checkAllProblemes(exeOrdre integer);
    
    
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  objet maracuja.mandat%ROWTYPE;
  manid mandat.man_id%type;
  manttc mandat.man_ttc%type;
  ecddebit ecriture_detail.ecd_debit%type;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and 
    (
    (man_ETAT   not in ('VISE','PAYE', 'ANNULE') and bor_etat  in ('VISE', 'PAYE', 'PAIEMENT'))
    or
    (man_ETAT   not in ('ANNULE') and brj_ordre is not null)
    )
    ;
    
    
    cursor c2 is
        select m.* from mandat m, mandat_detail_ecriture mde, ecriture_detail ecd, mode_paiement mp  
            where m.man_id=mde.man_id and m.exe_ordre=2011 and mde_origine='VISA' 
            and m.mod_ordre=mp.mod_ordre and m.pai_ordre is null
            and mp.MOD_DOM='VIREMENT'
            and mde.ecd_ordre=ecd.ecd_ordre
            and abs(ecd_reste_emarger)<>abs(ecd_montant)
            and man_ht>0;
            
            
     cursor c3 is 
        SELECT m.man_id, man_ttc, SUM(ecd_debit) 
        FROM mandat m, mandat_detail_ecriture mde, ecriture_detail ecd 
        WHERE m.man_id=mde.man_id AND mde.ecd_ordre=ecd.ecd_ordre
        AND man_ttc>=0
        AND m.exe_ordre=exeordre
        AND mde_origine LIKE 'VISA%'
        GROUP BY m.man_id, man_ttc
        HAVING man_ttc<> SUM(ecd_debit);
            
            
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau ('|| borEtat ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') ou bien mandat sur un bordereau de rejet et non ANNULE';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de données : remettre la bonne valeur dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
  
   OPEN C2;
  LOOP
    FETCH c2 INTO objet;
    EXIT
  WHEN c2%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'La contrepartie de prise en charge du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') a été émargée alors que le mode de paiement est de type VIREMENT.';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Provoque un message d''erreur lors de la création d''un paiement (écritures déséquilibrées)', -- ZAP_CONSEQUENCE
        'Vous devez supprimer l''émargement puis modifier le mode de paiement du mandat si le mandat a réellement été payé autrement que par virement via Maracuja.'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c2;
  
  
    OPEN C3;
  LOOP
    FETCH c3 INTO manid, manttc, ecddebit ;
    EXIT
  WHEN c3%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau b, mandat m
    where b.bor_id=m.bor_id and m.man_id=manid;
    
    probleme := 'Le montant de l''écriture de visa du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||
    ') n''est pas cohérent avec le montant du mandat (man: '||manttc|| ' / ecr: '||ecddebit||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Incohérence comptabilité ordonnateur/comptable. Impossible de générer un paiement', -- ZAP_CONSEQUENCE
        'Intervention dans la base de données pour régulariser (suppression de l''écriture en trop si le problème vient de là) .'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c3;
  
  
  
  
END;

PROCEDURE checkMandatVsJefyDepense
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  pco_num_depense jefy_depense.depense_ctrl_planco.PCO_NUM%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b, 
      jefy_depense.depense_ctrl_planco dpco
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and m.man_id=dpco.man_id
    and dpco.pco_num <> m.pco_num
   
    ;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    select pco_num into pco_num_depense from jefy_depense.depense_ctrl_planco where man_id = objet.man_id;

    
    probleme := 'Imputation du mandat (' || objet.pco_num || ') incoherente avec l''imputation  de la depense côté budgétaire ('|| pco_num_depense ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Une raison possible est qu''il y a eu une réimputation comptable du mandat qui n''a pas été réimpactée budgétairement. Il est nécessaire d''intervenir dans la base de données pour le man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



procedure checkEcritureDates(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture
    where exe_ordre=exeOrdre
    and to_char(ecr_date_saisie,'yyyy')<>to_char(exeOrdre) ;
BEGIN
  categorie := 'ECRITURES';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'La date de saisie de l''écriture est incohérente avec l''exercice (date '|| lecritureDetail.ecr_date_saisie ||' / code gestion ' || lecritureDetail.exe_ordre ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE.ECR_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecr_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Corriger le champ ecr_date_saisie dans la table maracuja.ecriture ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDetailGestion(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code not in
      (select ges_code
      from maracuja.gestion_exercice
      where exe_ordre              =exeOrdre
      );
BEGIN
  categorie := 'CODES GESTION';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un code gestion non actif sur l''exercice (numero '|| ecrNumero ||' / code gestion ' || lecritureDetail.ges_code ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Activer le code gestion sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco2
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   not in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre      
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte supprimé sur '|| exeOrdre || '(numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;

procedure checkCadre2VsBalance(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vgescode gestion.ges_code%type;
  vpconum plan_comptable_exer.pco_num%type;
  vbaldebit number;
  vbalCredit number;
  vmandate number;
  vreverse number;
  vsolde number;
  vnet  number;
  

    cursor c1 is 
        select ges_code, pco_num,  sum(bal_debit) bal_debit, sum(bal_credit) bal_credit,  sum(bal_solde) bal_solde , sum(mandate) mandate, sum(reverse) reverse, sum(montant_net) montant_net
        from (
        select ges_code, ecd.pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '6%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, substr(ecd.pco_num,3,10) pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '186%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, pco_num, 0,0,0,sum(MANDATS), sum(reversements), sum(montant_net)
        from COMPTEFI.V_DVLOP_DEP
        where exe_ordre=exeOrdre
        and pco_num like '6%'
        group by ges_code, pco_num
        )
        group by ges_code, pco_num
        having (sum(bal_debit)<>sum(mandate) or sum(bal_credit)<> sum(reverse))
        order by ges_code, pco_num;

begin
    categorie     := 'COMPTE FINANCIER';
    sousCategorie := 'DIFFERENCE BALANCE/CADRE 2';
  OPEN C1;
  LOOP
    FETCH c1 INTO vgescode ,vpconum ,vbaldebit,vbalCredit,vSolde,vmandate, vreverse, vnet;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Code gestion: ' || vgescode|| ' Compte: ' || vPcoNum || ' : Balance <> cadre 2 (balance : D='||vBalDebit ||', C='|| vBalCredit ||') (Cadre 2 :  Mandats='||vMandate ||', ORV='|| vReverse ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          'Normal si certain bordereaux ne sont pas encore visés. Sinon incoherence dans les tables.',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;



procedure checkMontantsEmargements(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vecd_ordre maracuja.ecriture_detail.ecd_ordre%type;
  vecr_numero maracuja.ecriture.ecr_numero%type;
  vecd_index maracuja.ecriture_detail.ecd_index%type;
  vecd_montant maracuja.ecriture_detail.ecd_montant%type;
  vecd_reste_emarge maracuja.ecriture_detail.ecd_reste_emarger%type;
  vemd_montant maracuja.emargement_detail.emd_montant%type;
  vcalcreste maracuja.ecriture_detail.ecd_reste_emarger%type;
  vpco_num maracuja.ecriture_detail.pco_num%type;

    cursor c1 is       
        select ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger , sum(emd_montant), abs(ecd_montant)-sum(emd_montant) 
        from (
            select distinct * from 
            (select ed.* from emargement_detail ed, emargement ema where ema.ema_ordre=ed.ema_ordre and ema_etat='VALIDE') emd, 
            ecriture_detail ecd ,
            ecriture ecr
            where (ecd_ordre=ecd_ordre_source or ecd_ordre = ecd_ordre_destination)
            and ecd.exe_ordre=exeOrdre
            and ecd.ecr_ordre=ecr.ecr_ordre
        ) 
        group by ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger
        having abs(ecd_montant)-sum(emd_montant)<> ecd_reste_emarger;

begin
    categorie     := 'ECRITURES';
    sousCategorie := 'MONTANTS EMARGEMENTS';
  OPEN C1;
  LOOP
    FETCH c1 INTO vecd_ordre, vpco_num, vecr_numero, vecd_index, vecd_montant , vecd_reste_emarge, vemd_montant, vcalcreste;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Ecriture : ' || vecr_numero || '/'||  vecd_index ||', Compte: ' || vpco_num || ' : Montant du reste à émarger (='|| vecd_reste_emarge ||') différent du reste à émarger calculé (='||vcalcreste ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les états de développement de solde', -- ZAP_CONSEQUENCE
          'Corrigez le reste à émarger de l''écriture (Bouton disponible dans l''écran de consultation des écritures de Maracuja).',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;


procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
   checkMandatVsJefyDepense(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkEcritureDetailPlanco2(exeordre);
  checkEcritureDetailGestion(exeordre); 
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
  checkCadre2VsBalance(exeOrdre);
  checkMontantsEmargements(exeOrdre);
  
end;
END;
/




create or replace procedure grhum.inst_patch_maracuja_1930
is
begin

	
   jefy_admin.patch_util.end_patch (4, '1.9.3.0');
end;
/

