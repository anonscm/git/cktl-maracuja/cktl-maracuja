SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.6.3
-- Date de publication : 08/04/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Creation de la fonction pour impression des situations débiteurs
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.6.3',  null, '');
commit;

create procedure grhum.inst_patch_maracuja_1863 is
begin
	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 567, 'IMPR015', 'Impressions', null, 'Situation débiteurs', 'N', 'N', 4 );
		

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.6.3';           
end;
/



