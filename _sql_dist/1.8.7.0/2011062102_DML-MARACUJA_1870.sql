--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.8.7.0 
-- Date de publication : 21/06/2011 
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- 
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_maracuja_1870;
commit;

drop procedure grhum.inst_patch_maracuja_1870;