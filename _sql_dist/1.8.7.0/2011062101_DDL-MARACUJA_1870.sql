SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.7.0
-- Date de publication : 21/06/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Modifications pour le SEPA
-- Gestion de l'utilisateur pour le visa des ecritures PAF
-- Prise en comptes de reductions collectives dans le compte financier
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.7.0',  null, '');
commit;


-- ajout 

CREATE TABLE MARACUJA.VIREMENT_PARAM_SEPA
(
  VPS_ORDRE          NUMBER  NOT NULL,
  TVI_ORDRE          NUMBER  NOT NULL,
  VPS_TG_NOM             VARCHAR2(70 BYTE) NOT NULL,
  VPS_TG_BIC             VARCHAR2(11 BYTE) NOT NULL,
  VPs_tg_iban             VARCHAR2(34 BYTE) NOT NULL,
  VPS_TG_codique             VARCHAR2(7 BYTE) NOT NULL,
  vps_DFT_IBAN  VARCHAR2(34 BYTE) NOT NULL,
  VPS_DFT_TITULAIRE   VARCHAR2(70 BYTE) NOT NULL,
  VPS_emetteur_nom  VARCHAR2(70 BYTE) NOT NULL,
  VPS_dft_transfert_ID    VARCHAR2(8 BYTE) NOT NULL,
  VPS_dft_DEVISE    VARCHAR2(3 BYTE) NOT NULL,
  VPs_ETAT      varchar2(10) NOT NULL
)
TABLESPACE GFC;


ALTER TABLE MARACUJA.VIREMENT_PARAM_SEPA ADD (
  PRIMARY KEY
 (VPS_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
   );


GRANT SELECT ON MARACUJA.VIREMENT_PARAM_SEPA TO JEFY_PAYE;


COMMENT ON TABLE MARACUJA.VIREMENT_PARAM_SEPA IS 'Les parametres utilises pour la creation des fichiers de virement de type SEPA';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_ORDRE IS 'Identifiant de l''enregistrement';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.TVI_ORDRE IS 'Identifiant du type de format';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_TG_NOM IS 'Nom du teneur de compte (TG ou RF)';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_TG_BIC IS 'BIC du teneur de compte (BDFEFRPPCCT pour la banque de France / IDDOFRP1 pour l''IEDOM)';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_TG_IBAN IS 'IBAN du teneur de compte';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_TG_codique IS 'Transmis par la DGFIP, permet d''identifier le teneur de compte';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_DFT_IBAN IS 'IBAN du compte au tresor';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_DFT_TITULAIRE IS 'Titulaire du compte au tresor';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_dft_transfert_ID IS 'Identification de l''établissement coté TG. Permet à la TG d''identifier l''émetteur du fichier de virement. Utilisé dans le tag MessageIdentification';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_emetteur_nom IS 'Nom de l''établissement qui emet le virement';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPS_dft_devise IS 'Code de la devise pour les virements';
COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_SEPA.VPs_ETAT IS 'Enregistrement valide ou non';

ALTER TABLE MARACUJA.VIREMENT_FICHIER ADD (VIR_COMPTE  VARCHAR2(34));


COMMENT ON COLUMN MARACUJA.VIREMENT_FICHIER.VIR_CONTENU IS 'Contenu du fichier de virement';
COMMENT ON COLUMN MARACUJA.VIREMENT_FICHIER.UTL_ORDRE IS 'Reference a l''utilisateur qui cree le fichier';
COMMENT ON COLUMN MARACUJA.VIREMENT_FICHIER.VIR_COMPTE IS 'No du compte a partir du quel les virements seront effectues';


CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Paf IS

/*
PARISDESCARTES 
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER, utlOrdre integer);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR, utlOrdre integer);

PROCEDURE passerEcritureSACDBord ( borid INTEGER, utlOrdre integer);

-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Paf IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER, utlOrdre integer)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA PAF';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA PAF' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 20 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    utlOrdre
    );

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

    SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentmab.mab_montant,
    NULL,
    currentmab.mab_sens,
    ecrordre,
    currentmab.ges_code,
    currentmab.pco_num
    );

    SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

    -- Insertion des mandat_detail_ecritures
    INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
    ecdordre,
    currentmab.exe_ordre,
    currentmab.man_id,
    SYSDATE,
    mdeordre,
    'VISA',
    oriordre
    );



    END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  boretat := 'PAIEMENT';

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'), man_etat = 'VISE' WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = utlOrdre,bor_etat = boretat WHERE bor_id = borid;

 UPDATE jefy_paf.paf_etape SET pae_etat=boretat WHERE bor_id=borid;

  passerecrituresacdbord(borid, utlOrdre);

END IF;

END ;






-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR, utlOrdre integer)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT PAF'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paf.paf_ecritures e, jefy_paf.paf_mois p
WHERE e.exe_ordre = p.mois_annee and e.mois = p.mois_numero
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
AND e.ecr_sens='D'
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
     gescodes := '';
    
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
         IF (LENGTH(gescodes)>0) THEN
             gescodes := gescodes || ', ';
         END IF;
          gescodes := gescodes || curgescode;
    END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees (Kaki ou Abricot) : ' || gescodes);
  END IF;
  

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT' AND B.BOR_ETAT <> 'PAYE';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;

-- MODIF CYRIL 
-- Pour le moment on ne pass pas les ecritures de paiement

IF passer_ecritures = 'O'
THEN

           SELECT DISTINCT b.exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20 and  rownum=1;

           -- On verifie que les ecritures de paiement ne soient pas deja passees.
           SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT PAF '||borlibelle_mois;

           IF (cpt = 0)
           THEN
            ecrordre := creerecriture(
            1,
            SYSDATE,
            'PAIEMENT PAF '||borlibelle_mois,
            exeordre,
            oriordre,
            14,
            6,
            utlOrdre
            );

          -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
          OPEN bordereaux;
          LOOP
            FETCH bordereaux INTO currentbob;
            EXIT WHEN bordereaux%NOTFOUND;

            ecdordre := creerecrituredetail (
            NULL,
            'PAIEMENT PAF '||borlibelle_mois,
            currentbob.bob_montant,
            NULL,
            currentbob.bob_sens,
            ecrordre,
            currentbob.ges_code,
            currentbob.pco_num
            );

            END LOOP;
          CLOSE bordereaux;

          Api_Plsql_Journal.validerecriture(ecrordre);
 
   
  END IF;

    UPDATE jefy_paf.paf_etape SET pae_etat='TERMINEE' WHERE MOIS_LIBELLE=borlibelle_mois;

  END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bor_id in (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE MANDAT
  SET
  man_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = utlOrdre,
  bor_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
    localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
    localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE         --        INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;


-- permet de passer les  ecritures SACD  d un bordereau de PAF --
PROCEDURE passerEcritureSACDBord ( borid INTEGER, utlOrdre integer)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD PAF';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD PAF' AND bor_id = borid;

   IF (cpt > 0)
   THEN

    ecrordre := creerecriture(
    1,
    SYSDATE, 
    'SACD PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    utlOrdre
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'SACD PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

  END IF;

END ;



END;
/


CREATE OR REPLACE FORCE VIEW maracuja.v_ecd_emargement (exe_ordre,
                                                        ecd_ordre,
                                                        ecd_libelle,
                                                        ecd_ordre2,
                                                        ecd_index,
                                                        ema_numero,
                                                        ema_date,
                                                        emd_montant,
                                                        ecr_numero
                                                       )
AS
   SELECT emd.exe_ordre, emd.ecd_ordre_source AS ecd_ordre, ecd_libelle,
          ecd_ordre_destination AS ecd_ordre2, ecd_index, ema_numero,
          ema_date, emd.emd_montant, e.ecr_numero
     FROM maracuja.emargement_detail emd,
          maracuja.emargement ema,
          maracuja.ecriture_detail ecd,
          maracuja.ecriture e
    WHERE ema.ema_ordre = emd.ema_ordre
      AND emd.ecd_ordre_destination = ecd.ecd_ordre
      AND ecd.ecr_ordre = e.ecr_ordre
      AND SUBSTR (ema_etat, 1, 1) = 'V'
   UNION
   SELECT emd.exe_ordre, emd.ecd_ordre_destination AS ecd_ordre, ecd_libelle,
          ecd_ordre_source AS ecd_ordre2, ecd_index, ema_numero, ema_date,
          emd.emd_montant, e.ecr_numero
     FROM maracuja.emargement_detail emd,
          maracuja.emargement ema,
          maracuja.ecriture_detail ecd,
          maracuja.ecriture e
    WHERE ema.ema_ordre = emd.ema_ordre
      AND emd.ecd_ordre_source = ecd.ecd_ordre
      AND ecd.ecr_ordre = e.ecr_ordre
      AND SUBSTR (ema_etat, 1, 1) = 'V';
/




CREATE OR REPLACE FORCE VIEW maracuja.v_recette_recouvrement_date (rec_id,
                                                                reste_recouvrer,
                                                                 date_recouvrement,
                                                                 ecr_num_recouvrement
                                                                )
AS
   SELECT   r.rec_id, SUM (ecd_reste_emarger) AS reste_recouvrer, max(EMA_DATE) date_recouvrement, max(ecr_numero) ecr_num_recouvrement
       FROM recette r,
            titre_detail_ecriture tde,
            ecriture_detail ecd,
            maracuja.v_ecd_emargement vem,
            jefy_admin.exercice e
      WHERE r.rec_id = tde.rec_id
        AND tde.ecd_ordre = ecd.ecd_ordre
        and ecd.ecd_ordre = vem.ecd_ordre
        AND rec_mont >= 0
        AND ecd.ecd_credit = 0
        AND ecd.ecd_debit <> 0
        AND ecd.exe_ordre = e.exe_ordre
        AND e.exe_stat = 'O'
        AND SUBSTR (ecd.pco_num, 1, 1) = '4'
   GROUP BY r.rec_id
   UNION ALL
   SELECT   r.rec_id, SUM (ecd_reste_emarger) AS reste_recouvrer, max(EMA_DATE) date_recouvrement, max(ecr_numero) ecr_num_recouvrement
       FROM recette r,
            titre_detail_ecriture tde,
            ecriture_detail ecd,
            maracuja.v_ecd_emargement vem,
            jefy_admin.exercice e
      WHERE r.rec_id = tde.rec_id
        AND tde.ecd_ordre = ecd.ecd_ordre
         and ecd.ecd_ordre = vem.ecd_ordre
        AND rec_mont >= 0
        AND ecd.ecd_credit = 0
        AND ecd.ecd_debit <> 0
        AND ecd.exe_ordre = e.exe_ordre
        AND e.exe_stat = 'R'
        AND SUBSTR (ecd.pco_num, 1, 1) = '4'
        AND NOT EXISTS (
               SELECT tde_ordre
                 FROM titre_detail_ecriture xx,
                      ecriture_detail xecd,
                      jefy_admin.exercice xe
                WHERE xx.ecd_ordre = xecd.ecd_ordre
                  AND xecd.exe_ordre = xe.exe_ordre
                  AND xe.exe_stat = 'O'
                  AND xx.tit_id = r.tit_id)
   GROUP BY r.rec_id
  ;
/

CREATE OR REPLACE FORCE VIEW maracuja.v_rib (rib_ordre,
                                             fou_ordre,
                                             rib_bic,
                                             rib_cle,
                                             rib_codbanc,
                                             rib_guich,
                                             rib_iban,
                                             rib_domicil,
                                             rib_num,
                                             rib_titco,
                                             rib_valide
                                            )
AS
   SELECT r.rib_ordre, r.fou_ordre, b.bic, r.cle_rib, b.c_banque, b.c_guichet,
          r.iban, b.domiciliation, r.no_compte, r.rib_titco, r.rib_valide
     FROM grhum.ribfour_ulr r, grhum.banque b
    WHERE r.BANQ_ORDRE = b.banq_ordre;
/

CREATE OR REPLACE FORCE VIEW maracuja.v_titre_reimp_0 (exe_ordre,
                                                       ecr_date_saisie,
                                                       ecr_date,
                                                       ges_code,
                                                       pco_num,
                                                       bor_id,
                                                       tbo_ordre,
                                                       tit_id,
                                                       tit_num,
                                                       tit_lib,
                                                       tit_mont,
                                                       tit_tva,
                                                       tit_ttc,
                                                       tit_etat,
                                                       fou_ordre,
                                                       rec_debiteur,
                                                       rec_interne,
                                                       ecr_ordre,
                                                       ecr_sacd,
                                                       tde_origine,
                                                       ecd_montant
                                                      )
AS
   SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num,
          t.bor_id, b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle,
          ecd_credit, (SIGN (ecd_credit)) * t.tit_tva,
          ecd_credit + ((SIGN (ecd_credit)) * t.tit_tva), t.tit_etat,
          jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
          ei.ecr_sacd, tde_origine, ecd_montant
     FROM bordereau b,
          titre t,
          titre_detail_ecriture tde,
          ecriture_detail ed,
          v_ecriture_infos ei,
          ecriture e,
          v_jefy_multiex_titre jt
    WHERE t.bor_id = b.bor_id
      AND t.tit_id = tde.tit_id
      AND tde.ecd_ordre = ed.ecd_ordre
      AND ed.ecd_ordre = ei.ecd_ordre
      AND ed.ecr_ordre = e.ecr_ordre
      AND t.tit_ordre = jt.tit_ordre
      AND jt.exe_exercice = ed.exe_ordre
      --AND r.pco_num_ancien = ed.pco_num
      AND tde_origine IN ('VISA', 'REIMPUTATION')
      AND tit_etat IN ('VISE', 'PAYE')
      AND ecd_credit <> 0
      AND tbo_ordre <> 9
      AND tbo_ordre <> 11
      AND tbo_ordre <> 200
      AND ed.pco_num NOT LIKE '185%'
      AND t.tit_id IN (SELECT tit_id
                         FROM (SELECT   tit_id, COUNT (*)
                                   FROM recette
                               GROUP BY tit_id
                                 HAVING COUNT (*) = 1))
      AND ABS (ecd_montant) = ABS (tit_ht)
   UNION ALL
-- titres co
   SELECT x.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, x.pco_num,
          t.bor_id, b.tbo_ordre, t.tit_id, t.tit_numero, ecd_libelle,
          ecd_credit, (SIGN (ecd_credit)) * t.tit_tva,
          ecd_credit + ((SIGN (ecd_credit)) * t.tit_tva), t.tit_etat,
          jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, e.ecr_ordre,
          ecr_sacd, tde_origine, ecd_montant
     FROM bordereau b,
          titre t,
          (SELECT   ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre, pco_num,
                    tde_origine, 'Titre collectif' ecd_libelle,
                    SUM (ecd_montant) ecd_montant, SUM (ecd_credit)
                                                                   ecd_credit
               FROM titre_detail_ecriture tde,
                    ecriture_detail ed,
                    v_ecriture_infos ei
              WHERE tde.ecd_ordre = ed.ecd_ordre
                AND ed.ecd_ordre = ei.ecd_ordre
                AND ecd_credit <> 0
                AND ed.pco_num NOT LIKE '185%'
                AND tde_origine IN ('VISA', 'REIMPUTATION')
           GROUP BY ed.exe_ordre,
                    ecr_sacd,
                    tit_id,
                    ecr_ordre,
                    pco_num,
                    tde_origine) x,
          ecriture e,
          v_jefy_multiex_titre jt
    WHERE t.bor_id = b.bor_id
      AND x.ecr_ordre = e.ecr_ordre
      AND t.tit_id = x.tit_id
      AND t.tit_ordre = jt.tit_ordre
      AND jt.exe_exercice = x.exe_ordre
      --AND r.pco_num_ancien = ed.pco_num
      AND tit_etat IN ('VISE', 'PAYE')
      -- AND ecd_credit<>0
      AND tbo_ordre <> 9
      AND tbo_ordre <> 11
      AND tbo_ordre <> 200
      AND t.tit_id IN (SELECT tit_id
                         FROM (SELECT   tit_id, COUNT (*)
                                   FROM recette
                               GROUP BY tit_id
                                 HAVING COUNT (*) > 1))
      AND ABS (ecd_montant) = ABS (tit_ht)
   UNION ALL
-- titres de recettes des PI
   SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code,
          SUBSTR (ed.pco_num, 3, 20) AS pco_num, t.bor_id, b.tbo_ordre,
          t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit,
          (SIGN (ecd_credit)) * t.tit_tva,
          ecd_credit + ((SIGN (ecd_credit)) * t.tit_tva), t.tit_etat,
          jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
          ei.ecr_sacd, tde_origine, ecd_montant
     FROM bordereau b,
          titre t,
          titre_detail_ecriture tde,
          ecriture_detail ed,
          v_ecriture_infos ei,
          ecriture e,
          v_jefy_multiex_titre jt
    WHERE t.bor_id = b.bor_id
      AND t.tit_id = tde.tit_id
      AND tde.ecd_ordre = ed.ecd_ordre
      AND ed.ecd_ordre = ei.ecd_ordre
      AND ed.ecr_ordre = e.ecr_ordre
      AND t.tit_ordre = jt.tit_ordre
      AND jt.exe_exercice = ed.exe_ordre
      --AND r.pco_num_ancien = ed.pco_num
      AND tde_origine IN ('VISA', 'REIMPUTATION')
      AND tit_etat IN ('VISE', 'PAYE')
      AND ecd_credit <> 0
      AND (tbo_ordre = 11 OR tbo_ordre = 200)
      AND ed.pco_num NOT LIKE '185%'
      AND ABS (ecd_montant) = ABS (tit_ht)
   UNION ALL
-- reductions de recettes
   SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie AS ecr_date,
          t.ges_code, ed.pco_num, t.bor_id, b.tbo_ordre, t.tit_id,
          t.tit_numero, ed.ecd_libelle, ecd_debit,
          (SIGN (ecd_debit)) * ABS (t.tit_tva),
          ecd_debit + ((SIGN (ecd_debit)) * ABS (t.tit_tva)), t.tit_etat,
          jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
          ei.ecr_sacd, tde_origine, ecd_montant
     FROM bordereau b,
          titre t,
          titre_detail_ecriture tde,
          ecriture_detail ed,
          v_ecriture_infos ei,
          ecriture e,
          v_jefy_multiex_titre jt
    WHERE t.bor_id = b.bor_id
      AND t.tit_id = tde.tit_id
      AND tde.ecd_ordre = ed.ecd_ordre
      AND ed.ecd_ordre = ei.ecd_ordre
      AND ed.ecr_ordre = e.ecr_ordre
      AND t.tit_ordre = jt.tit_ordre
      AND jt.exe_exercice = ed.exe_ordre
      --AND r.pco_num_ancien = ed.pco_num
      AND tde_origine IN ('VISA', 'REIMPUTATION')
      AND tit_etat IN ('VISE', 'PAYE')
      AND ecd_debit <> 0
      AND tbo_ordre = 9
      AND ed.pco_num NOT LIKE '185%'
      AND ABS (ecd_montant) = ABS (tit_ht)
union all  
-- reduc co    
SELECT x.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, x.pco_num,
          t.bor_id, b.tbo_ordre, t.tit_id, t.tit_numero, ecd_libelle,
          ecd_debit, (SIGN (ecd_debit)) * t.tit_tva,
          ecd_debit + ((SIGN (ecd_debit)) * t.tit_tva), t.tit_etat,
          jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, e.ecr_ordre,
          ecr_sacd, tde_origine, ecd_montant
     FROM bordereau b,
          titre t,
          (SELECT   ed.exe_ordre, ecr_sacd, tit_id, ecr_ordre, pco_num,
                    tde_origine, 'Titre collectif' ecd_libelle,
                    SUM (ecd_montant) ecd_montant, SUM (ecd_debit) ecd_debit
               FROM titre_detail_ecriture tde,
                    ecriture_detail ed,
                    v_ecriture_infos ei
              WHERE tde.ecd_ordre = ed.ecd_ordre
                AND ed.ecd_ordre = ei.ecd_ordre
                AND ecd_debit <> 0
                AND ed.pco_num NOT LIKE '185%'
                AND tde_origine IN ('VISA', 'REIMPUTATION')
           GROUP BY ed.exe_ordre,
                    ecr_sacd,
                    tit_id,
                    ecr_ordre,
                    pco_num,
                    tde_origine) x,
          ecriture e,
          v_jefy_multiex_titre jt
    WHERE t.bor_id = b.bor_id
      AND x.ecr_ordre = e.ecr_ordre
      AND t.tit_id = x.tit_id
      AND t.tit_ordre = jt.tit_ordre
      AND jt.exe_exercice = x.exe_ordre
      --AND r.pco_num_ancien = ed.pco_num
      AND tit_etat IN ('VISE', 'PAYE')
      -- AND ecd_credit<>0
      AND tbo_ordre = 9
      AND t.tit_id IN (SELECT tit_id
                         FROM (SELECT   tit_id, COUNT (*)
                                   FROM recette
                               GROUP BY tit_id
                                 HAVING COUNT (*) > 1))
      AND ABS (ecd_montant) = ABS (tit_ht)
      ;
/


create procedure grhum.inst_patch_maracuja_1870 is
begin
	INSERT INTO MARACUJA.TYPE_VIREMENT (TVI_LIBELLE, TVI_ORDRE, TVI_VALIDITE,MOD_DOM, TVI_FORMAT) 
		VALUES ('FICHIER SEPA SCT' , 6, 'O','VIREMENT','SEPASCT' );
		
	update maracuja.type_virement set tvi_validite='N' where tvi_ordre in (2,3);	
		

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.7.0';           
end;
/


CREATE OR REPLACE PACKAGE MARACUJA."NUMEROTATIONOBJECT" IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (broid INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);

function next_numero_paiement(comordre integer, exeordre integer) return integer; 


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid INTEGER);

PROCEDURE numeroter_titre (borid INTEGER);

PROCEDURE numeroter_orv (borid INTEGER);

PROCEDURE numeroter_aor (borid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


GRANT EXECUTE ON MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE BODY MARACUJA."NUMEROTATIONOBJECT" IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (broid INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM BROUILLARD WHERE bro_id = broid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BROUILLARD INEXISTANTE, CLE '||broid);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM BROUILLARD
WHERE bro_id = broid;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE BROUILLARD SET bro_numero = lenumero
WHERE bro_id = broid;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

    SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
    IF cpt = 0 THEN
     RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
    END IF;

    -- recup des infos de l objet -
    SELECT com_ordre,exe_ordre
    --select 1,1
    INTO COMORDRE,EXEORDRE
    FROM PAIEMENT
    WHERE pai_ordre = paiordre;

    --SELECT tnu_ordre INTO tnuordre
    --FROM TYPE_NUMEROTATION
    --WHERE tnu_libelle ='VIREMENT';

    ---- recup du numero -
    --lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);


    lenumero := next_numero_paiement(comordre, exeordre);

    -- affectation du numero -
    UPDATE PAIEMENT SET pai_numero = lenumero
    WHERE pai_ordre = paiordre;

END;




function next_numero_paiement(comordre integer, exeordre integer) return integer is
    lenumero INTEGER;
    tnuordre integer;
begin

    SELECT tnu_ordre INTO tnuordre
    FROM TYPE_NUMEROTATION
    WHERE tnu_libelle ='VIREMENT';

    -- recup du numero -
    lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
    return lenumero;
end;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_id = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre, exe_ordre
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_id = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE RETENUE SET ret_numero = lenumero
WHERE RET_ID=retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

    LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

    --COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

    IF gescode IS NULL THEN
    SELECT COUNT(*) INTO cpt
    FROM NUMEROTATION
    WHERE com_ordre = comordre
    AND exe_ordre = exeordre
    AND ges_code IS NULL
    AND tnu_ordre = tnuordre;
    ELSE
    SELECT COUNT(*) INTO cpt
    FROM NUMEROTATION
    WHERE com_ordre = comordre
    AND exe_ordre = exeordre
    AND ges_code = gescode
    AND tnu_ordre = tnuordre;
    END IF;

    IF cpt  = 0 THEN
    --raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
     SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


     INSERT INTO NUMEROTATION
     ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
     VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
     RETURN 1;
    ELSE
     IF gescode IS NOT NULL THEN
      SELECT num_numero+1 INTO cpt
      FROM NUMEROTATION
      WHERE com_ordre = comordre
      AND exe_ordre = exeordre
      AND ges_code = gescode
      AND tnu_ordre = tnuordre;

      UPDATE NUMEROTATION SET num_numero = cpt
      WHERE com_ordre = comordre
      AND exe_ordre = exeordre
      AND ges_code = gescode
      AND tnu_ordre = tnuordre;
     ELSE
      SELECT num_numero+1 INTO cpt
      FROM NUMEROTATION
      WHERE com_ordre = comordre
      AND exe_ordre = exeordre
      AND ges_code IS NULL
      AND tnu_ordre = tnuordre;

      UPDATE NUMEROTATION SET num_numero = cpt
      WHERE com_ordre = comordre
      AND exe_ordre = exeordre
      AND ges_code IS NULL
      AND tnu_ordre = tnuordre;
     END IF;
     RETURN cpt;
    END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) IS


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau BORDEREAU%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



SELECT  COUNT(*) INTO cpt
FROM maracuja.TYPE_BORDEREAU tb , maracuja.TYPE_NUMEROTATION tn
WHERE tn.tnu_ordre = tb.tnu_ordre
AND tb.tbo_ordre = tboordre;

IF cpt = 1 THEN
 SELECT tn.tnu_ordre INTO tnuordre
 FROM maracuja.TYPE_BORDEREAU tb , TYPE_NUMEROTATION tn
 WHERE tn.tnu_ordre = tb.tnu_ordre
 AND tb.tbo_ordre = tboordre;
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

IF lenumero IS NULL THEN
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
END IF;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

END;


PROCEDURE numeroter_mandat (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid ORDER BY pco_num;

BEGIN

-- si c un bordereau orv
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id=borid;

select exe_ordre into exeordre from bordereau  WHERE bor_id=borid;
if (exeordre>2010) then
  IF (tboordre in (8,18,21) ) THEN
     numeroter_orv(borid);
     RETURN;
  END IF;
else
 IF (tboordre in (8,18) ) THEN
     numeroter_orv(borid);
     RETURN;
  END IF;
end if;



-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;



-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_titre (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid ORDER BY pco_num, tit_id;


BEGIN


-- si c un bordereau aor
SELECT tbo_ordre INTO  tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre=9) THEN
   numeroter_aor(borid);
   RETURN;
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;

 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;



PROCEDURE numeroter_orv (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_aor (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;

END NUMEROTATIONOBJECT;
/

CREATE OR REPLACE PACKAGE MARACUJA.Util  IS

    PROCEDURE annuler_visa_bor_mandat(borid INTEGER);
    PROCEDURE annuler_visa_bor_titre(borid INTEGER);

    PROCEDURE supprimer_visa_btme(borid INTEGER);
    
    PROCEDURE supprimer_visa_btms(borid INTEGER);

    PROCEDURE supprimer_visa_btte(borid INTEGER);

    procedure creer_ecriture_annulation(ecrordre INTEGER);
    
    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    procedure supprimer_bordereau_dep(borid integer);
    
    procedure supprimer_bordereau_rec(borid integer);
    
    procedure supprimer_bordereau_btms(borid integer);
    
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer);
    
    procedure supprimer_paiement_virement(paiordre INTEGER);
    
    procedure corriger_etat_mandats;
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Util IS

    -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
    -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

 -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE annuler_visa_bor_mandat(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, mandat_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.man_id=tmpmanid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME' and tbotype <> 'BTRU') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME ou BTRU');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandats associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
--             
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

--                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

--                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;



 PROCEDURE annuler_visa_bor_titre(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid TITRE.tit_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                  CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, titre_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.tit_id=tmpmanid;

       BEGIN
     SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associé au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;




            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmpmanid;
                             EXIT WHEN lestitres%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmpmanid;
                                UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmpmanid;



             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;




       -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE supprimer_visa_btme(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
                            if (flag>0) then
                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                            end if;
                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;


 -- suppression des ecritures du visa d'un bordereau de depense BTMS 
       PROCEDURE supprimer_visa_btms(borid INTEGER) IS
         flag INTEGER;
         lebordereau BORDEREAU%ROWTYPE;
         tbotype TYPE_BORDEREAU.tbo_type%TYPE;
         tmpmanid MANDAT.man_id%TYPE;
         tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

         CURSOR lesecritures IS
         SELECT distinct ed.ecr_ordre from ecriture_detail ed, mandat_detail_ecriture mde, mandat m WHERE m.man_id = mde.man_id and mde.ecd_ordre= ed.ecd_ordre and m.bor_id = borid;

         CURSOR lesmandats IS
         SELECT man_id FROM MANDAT WHERE bor_id = borid;

         BEGIN
           SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTMS') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTMS');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat = 'VALIDE'  ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

--            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;

            OPEN lesecritures;
             LOOP
              FETCH lesecritures INTO tmpEcrOrdre;
             EXIT WHEN lesecritures%NOTFOUND;

                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE ecd_ordre in (select ecd_ordre from ecriture_detail where ecr_ordre = tmpEcrOrdre);

                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;

                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

             END LOOP;
             CLOSE lesecritures;


            OPEN lesmandats;
            LOOP
            FETCH lesmandats INTO tmpmanid;
            EXIT WHEN lesmandats%NOTFOUND;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;

             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

            UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_visa = null,ecr_ordre_sacd = null,ecr_ordre_opp = null  WHERE bor_id IN (borid);

       END;




   -- annulation du visa d'un bordereau de depense BTTE
       PROCEDURE supprimer_visa_btte(borid INTEGER) IS
                    flag INTEGER;
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmptitid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associé au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmptitid;
                             EXIT WHEN lestitres%NOTFOUND;

                            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, TITRE_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.tit_id=tmptitid;

                            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                            DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmptitid;
                            UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmptitid;


             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;
       
       
       
       procedure creer_ecriture_annulation(ecrordre INTEGER) is
            ecr maracuja.ecriture%rowtype;
            ecd maracuja.ecriture_detail%rowtype;
            flag integer;
            str ecriture.ecr_libelle%type;
            newEcrOrdre ecriture.ecr_ordre%type;
            newEcdOrdre ecriture_detail.ecd_ordre%type;
            x  integer;
            cursor c1 is select * from maracuja.ecriture_detail where ecr_ordre=ecrordre;
       
       begin
            
            
            select count(*) into flag from maracuja.ecriture where ecr_ordre=ecrordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture retrouvee pour ecr_ordre= '|| ecrordre || ' .');
            end if;

            select * into ecr from maracuja.ecriture where ecr_ordre=ecrordre;              
            
            str := 'Annulation Ecriture '|| ecr.exe_ordre || '/'||ecr.ecr_numero;
            
            -- verifier que l'ecriture n'a pas deja ete annulee
            select count(*) into flag from maracuja.ecriture where ecr_libelle = str; 
            if (flag > 0) then
                select ecr_numero into flag from maracuja.ecriture where ecr_libelle = str; 
                RAISE_APPLICATION_ERROR (-20001,'L''ecriture numero '||ecr.ecr_numero ||' (ecr_ordre= '|| ecrordre || ') a deja ete annulee par l''ecriture numero '|| flag ||'.');
            end if;
                   
            
            -- verifier que les details ne sont pas emarges
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                if (ecd.ecd_reste_emarger<abs(ecd.ecd_montant) ) then
                    RAISE_APPLICATION_ERROR (-20001,'L''ecriture '||ecr.ecr_numero ||' ecr_ordre= '|| ecrordre || ' a ete emargee pour le compte '||ecd.pco_num||'. Impossible d''annuler');                    
                end if;

             END LOOP;
             CLOSE c1;


--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...              
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--                    
--                    

--             END LOOP;
--             CLOSE c1;

            newEcrOrdre := api_plsql_journal.creerecriture(
                ecr.com_ordre,
                SYSDATE,
                str,
                ecr.exe_ordre,
                ecr.ori_ordre,
                ecr.tjo_ordre,
                ecr.top_ordre,
                ecr.utl_ordre);            
            
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                newEcdOrdre := api_plsql_journal.creerEcritureDetail (
                        ecd.ecd_commentaire,
                        ecd.ecd_libelle,  
                        -ecd.ECD_MONTANT,
                        ecd.ECD_SECONDAIRE,
                        ecd.ECD_SENS,
                        newEcrOrdre,
                        ecd.GES_CODE,
                        ecd.PCO_NUM );
                        
                x := creerEmargementMemeSens(ecd.ecd_ordre, newEcdOrdre, 1);
             END LOOP;
             CLOSE c1;                  
            NUMEROTATIONOBJECT.numeroter_ecriture(newEcrOrdre);

       end;
           
       
       
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
    BEGIN
        api_emargement.annuler_emargement(emaordre);
    END;
    
    
    
        
    procedure supprimer_bordereau_dep(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des mandats
        select count(*) into flag from mandat where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de depense.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les mandat_brouillards
        DELETE FROM maracuja.mandat_BROUILLARD WHERE man_id in (select man_id from mandat where bor_id=borid);
                
        -- supprimer les depenses
        DELETE FROM maracuja.depense WHERE man_id in (select man_id from mandat where bor_id=borid);

        -- mettre a jour les depense_ctrl_planco
        UPDATE jefy_depense.depense_ctrl_planco SET man_id = NULL WHERE man_id IN (SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid) ;

        -- supprimer les mandats        
        delete from  maracuja.mandat where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
        
        
    end;
    
    
    procedure supprimer_bordereau_btms(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau b, type_bordereau tb where bor_id=borid and b.tbo_ordre=tb.tbo_ordre and tb.TBO_TYPE='BTMS';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas ou n''est pas de type BTMS');
        end if;
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;          
        
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        
        supprimer_bordereau_dep(borid);
        
    end;
    
    
    
    procedure supprimer_bordereau_rec(borid integer)  as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des titres
        select count(*) into flag from titre where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de recette.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les titre_brouillards
        DELETE FROM maracuja.titre_BROUILLARD WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);
                
        -- supprimer les recettes
        DELETE FROM maracuja.recette WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);

        -- mettre a jour les recette_ctrl_planco
        UPDATE jefy_recette.recette_ctrl_planco SET tit_id = NULL WHERE tit_id IN (SELECT tit_id FROM maracuja.titre WHERE bor_id = borid) ;

        -- supprimer les titres        
        delete from  maracuja.titre where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
    
    end;
 



    /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer) IS
        flag INTEGER;                 
        leRecouvrement recouvrement%rowtype;
        tmpemaOrdre emargement.ema_ordre%type;
        tmpEcrOrdre ecriture.ecr_ordre%type;

        cursor emargements is 
                select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' 
                        and rec_id in (
                                select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ))) 
                    union
                        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where 
                        eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )));

                    
       cursor ecritures is
               select distinct e.ecr_ordre from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ));

        begin
        
            -- recuperer les numeros des ecritures min et max pour les passer en parametre
            --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
        
            select count(*) into flag from prelevement where RECO_ORDRE = recoordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucun prelevement correspondant a recoordre= '|| recoordre);
            end if;
            
            select * into leRecouvrement from recouvrement where RECO_ORDRE = recoordre;
            
            -- verifier les dates des ecritures avec la date du prelevement
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMin;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if; 
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMax;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if;             
            

        -- supprimer les emargements
        OPEN emargements;
             LOOP
                 FETCH emargements INTO tmpemaOrdre;
                      EXIT WHEN emargements%NOTFOUND;
                      
                annuler_emargement(tmpEmaOrdre);      
        END LOOP;
        CLOSE emargements;        
        
        -- modifier les etats des prelevements
        update prelevement set PREL_ETAT_MARACUJA = 'ATTENTE' where reco_ordre=recoOrdre;

        --supprimer le contenu du fichier (prelevement_fichier)
        delete from prelevement_fichier where reco_ordre=recoordre; 

        -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
        OPEN ecritures;
             LOOP
                 FETCH ecritures INTO tmpecrOrdre;
                      EXIT WHEN ecritures%NOTFOUND;
                      
                creer_ecriture_annulation(tmpEcrOrdre);
        END LOOP;
        CLOSE ecritures;         

        delete from titre_detail_ecriture where ecd_ordre in (select distinct ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
        where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre 
        from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in 
        (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )) );

        -- supprimer association des prélèvements au recouvrement
        update  prelevement set reco_ordre=null where reco_ordre=recoOrdre;

        -- supprimer l'objet recouvrement
        delete from recouvrement where reco_ordre=recoOrdre;





--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))


--        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))) 
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--                 
                 
                 
                     
    end;
    
    
    
    -- suppression d'un paiement de type virement avec annulation des émargements et écritures de paiements
    procedure supprimer_paiement_virement(paiordre INTEGER) is
        flag INTEGER;
        ecdp ecriture_detail%rowtype;
        ecdOrdre ecriture_detail.ecd_ordre%type;
        emaOrdre emargement.ema_ordre%type;
        ecrOrdre ecriture.ecr_ordre%type;
        cursor ecdpaiement5 is
             select ecd.* from ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.man_id=mde.man_id
                and mde.ecd_ordre=ecd.ecd_ordre
                and mde_origine='VIREMENT'
                and ecd.ecd_sens='C'
                and ecd.pco_num like '5%'
                and p.pai_ordre=paiordre
                and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' 
            union all
                select ecd.* from ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.odp_ordre=mde.odp_ordre
                and mde.ecd_ordre=ecd.ecd_ordre
                and ope_origine='VIREMENT'
                and ecd.ecd_sens='C'
                and ecd.pco_num like '5%'
                 and p.pai_ordre=paiordre
                 and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' ;  
                 
                 
         cursor ecdpaiementAll is
             select ecd.ecd_ordre from ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.man_id=mde.man_id
                and mde.ecd_ordre=ecd.ecd_ordre
                and mde_origine='VIREMENT'
                and p.pai_ordre=paiordre
                and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' 
            union all
                select ecd.ecd_ordre from ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.odp_ordre=mde.odp_ordre
                and mde.ecd_ordre=ecd.ecd_ordre
                and ope_origine='VIREMENT'
                and p.pai_ordre=paiordre
                and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' 
                ;  
                
           cursor ecrOrdres is
             select distinct ecd.ecr_ordre from ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.man_id=mde.man_id
                and mde.ecd_ordre=ecd.ecd_ordre
                and mde_origine='VIREMENT'
                and p.pai_ordre=paiordre
                and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' 
            union
                select distinct ecd.ecr_ordre from ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
                where p.pai_ordre=m.pai_ordre
                and m.odp_ordre=mde.odp_ordre
                and mde.ecd_ordre=ecd.ecd_ordre
                and ope_origine='VIREMENT'
                and p.pai_ordre=paiordre
                and ecd.ecr_ordre=e.ecr_ordre
                and e.ecr_libelle like 'PAIEMENT%' 
                ;         
                
         cursor emaOrdreForEcd is 
            select emd.ema_ordre from emargement_detail emd, emargement ema where ema.ema_ordre= emd.ema_ordre and ema_etat='VALIDE' and ecd_ordre_source = ecdOrdre or ecd_ordre_destination=ecdOrdre;
                           
    begin
        select count(*) into flag from maracuja.paiement where pai_ordre=paiordre;
        if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucun paiement trouvé correspondant a pai_ordre= '|| paiordre);
        end if;
        
        --verifier que classe 5 non émargée
        OPEN ecdpaiement5;
             LOOP
               FETCH ecdpaiement5 INTO ecdp;
                    EXIT WHEN ecdpaiement5%NOTFOUND;
               if (ecdp.ecd_reste_emarger<>abs(ecdp.ecd_credit) ) then
                 RAISE_APPLICATION_ERROR (-20001,'L''écriture '|| ecdp.pco_num || ' - ' || ecdp.ecd_libelle|| ' a été émargée, supprimez l''émargement.');
               end if;
        END LOOP;
        CLOSE ecdpaiement5;        
        
        -- supprimer tous les emargements 
        OPEN ecdpaiementAll;
             LOOP
               FETCH ecdpaiementAll INTO ecdOrdre;
                    EXIT WHEN ecdpaiementAll%NOTFOUND;
             
             OPEN emaOrdreForEcd;
             LOOP
               FETCH emaOrdreForEcd INTO emaOrdre;
                    EXIT WHEN emaOrdreForEcd%NOTFOUND;
                api_emargement.annuler_emargement(emaOrdre);
            END LOOP;
            CLOSE emaOrdreForEcd;               
        END LOOP;
        CLOSE ecdpaiementAll;   
        
        
        --  creer ecritures annulation
         OPEN ecrOrdres;
             LOOP
               FETCH ecrOrdres INTO ecrOrdre;
                    EXIT WHEN ecrOrdres%NOTFOUND;
             creer_ecriture_annulation(ecrOrdre);
         END LOOP;
         CLOSE ecrOrdres;   
        
        
        -- supprimer les mde et ope
        OPEN ecdpaiementAll;
             LOOP
               FETCH ecdpaiementAll INTO ecdOrdre;
                    EXIT WHEN ecdpaiementAll%NOTFOUND;
             
            delete from mandat_detail_ecriture where ecd_ordre=ecdOrdre;
            delete from odpaiem_detail_ecriture  where ecd_ordre=ecdOrdre;         
        END LOOP;
        CLOSE ecdpaiementAll;         
        
        
        
        -- changer etats des mandats et ODP
        update mandat set man_etat='VISE', pai_ordre=null where pai_ordre=paiordre;
        update ordre_de_paiement set odp_etat='VALIDE', pai_ordre=null where pai_ordre=paiordre;
        
        -- supprimer le paiement
        delete from virement_fichier where pai_ordre=paiordre;
        delete from paiement where pai_ordre=paiordre;
        
    
    
    
    end;
    
    
    procedure corriger_etat_mandats is
    begin
        update maracuja.mandat set man_etat = 'VISE'
        where man_id in
        (select m.man_id from maracuja.mandat m, maracuja.mandat_detail_ecriture mde
        where m.man_id = mde.man_id
        and m.man_etat = 'ATTENTE'
        and mde.MDE_ORIGINE = 'VISA');
    end;
    
END;
/

CREATE OR REPLACE PROCEDURE MARACUJA.autowash_local IS
/******************************************************************************
 Lance des traitements pour corriger les donnees (vous pouvez personnaliser)

******************************************************************************/
BEGIN

    return;

END autowash_local;
/


CREATE OR REPLACE PROCEDURE MARACUJA.autowash IS
/******************************************************************************
 Lance des traitements pour corriger les donnees

******************************************************************************/
BEGIN

    util.CORRIGER_ETAT_MANDATS;
    
    maracuja.autowash_local;

END autowash;
/





GRANT EXECUTE ON MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;


