set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM ou dba
-- Numéro de version :  1.9.3.4
-- Date de publication : 18/04/2013
-- Licence : CeCILL version 2
--
--


-- prerequis
-- Patch sql jefy_admin 1.3.0.4



----------------------------------------------
-- Mises à jour pour l'evolution reglementaire concernant les intérêts moratoires (penalite de 40 euros)
----------------------------------------------
whenever sqlerror exit sql.sqlcode;




exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.3', 'MARACUJA' );
exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 1, '1.3.0.4', 'JEFY_ADMIN' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.4', null );
commit ;



ALTER TABLE MARACUJA.IM ADD (IM_penalite  NUMBER(10,2)  DEFAULT 0  NOT NULL);
COMMENT ON COLUMN MARACUJA.IM.IM_penalite IS 'Montant de la penalite a ajouter au montant de l''IM calcule (recupere a partir de jefy_admin.im_taux.imta_penalite lors de la creation de cet enregistrement';

COMMENT ON COLUMN MARACUJA.IM.IM_MONTANT IS 'Montant de l''interet moratoire, calcule a partir du montant regle, du taux applicable, de la penalité et du nombre de jours de depassement.';


create or replace force view maracuja.v_im_taux (imta_id, imtt_id, imta_debut, imta_fin, imta_taux, imta_penalite, utl_ordre, date_creation, date_modification)
as
   select imta_id,
          imtt_id,
          imta_debut,
          imta_fin,
          imta_taux,
          imta_penalite,
          utl_ordre,
          date_creation,
          date_modification
   from   jefy_admin.im_taux;
/


create or replace force view maracuja.v_accords_contrat (pers_id_partenaire,
                                                         ll_partenaire,
                                                         pers_id_service,
                                                         con_ordre,
                                                         exe_ordre,
                                                         con_index,
                                                         con_etablissement,
                                                         con_cr,
                                                         con_nature,
                                                         con_reference_externe,
                                                         con_objet,
                                                         con_objet_court,
                                                         con_observations,
                                                         tr_ordre,
                                                         utl_ordre_creation,
                                                         con_date_creation,
                                                         utl_ordre_modif,
                                                         con_date_modif,
                                                         utl_ordre_valid_adm,
                                                         con_date_valid_adm,
                                                         con_date_cloture,
                                                         con_date_apurement,
                                                         con_groupe_bud,
                                                         con_suppr,
                                                         org_id_composante,
                                                         tcc_id,
                                                         avis_favorable,
                                                         avis_defavorable,
                                                         contexte,
                                                         remarques,
                                                         motifs_avis,
                                                         c_naf,
                                                         con_groupe_partenaire,
                                                         con_duree,
                                                         date_migration,
                                                         con_duree_mois,
                                                         numero
                                                        )
as
   select pers_id_partenaire,
          ll_partenaire,
          pers_id_service,
          con_ordre,
          exe_ordre,
          con_index,
          con_etablissement,
          con_cr,
          con_nature,
          con_reference_externe,
          con_objet,
          con_objet_court,
          con_observations,
          tr_ordre,
          utl_ordre_creation,
          con_date_creation,
          utl_ordre_modif,
          con_date_modif,
          utl_ordre_valid_adm,
          con_date_valid_adm,
          con_date_cloture,
          con_date_apurement,
          con_groupe_bud,
          con_suppr,
          org_id_composante,
          tcc_id,
          avis_favorable,
          avis_defavorable,
          contexte,
          remarques,
          motifs_avis,
          c_naf,
          con_groupe_partenaire,
          con_duree,
          date_migration,
          con_duree_mois,
          to_char (exe_ordre, 'FM0000') || '-' || to_char (con_index, 'FM0000') as numero
   from   accords.v_convention_partenaire;
/







create or replace procedure grhum.inst_patch_maracuja_1934
is
begin

	
   jefy_admin.patch_util.end_patch (4, '1.9.3.4');
end;
/

