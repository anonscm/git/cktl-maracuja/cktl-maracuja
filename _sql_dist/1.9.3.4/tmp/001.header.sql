set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM ou dba
-- Numéro de version :  1.9.3.4
-- Date de publication : 18/04/2013
-- Licence : CeCILL version 2
--
--


-- prerequis
-- Patch sql jefy_admin 1.3.0.4



----------------------------------------------
-- Mises à jour pour l'evolution reglementaire concernant les intérêts moratoires (penalite de 40 euros)
----------------------------------------------
whenever sqlerror exit sql.sqlcode;




exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.3', 'MARACUJA' );
exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.3.0.4', 'JEFY_ADMIN' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.4', null );
commit ;



