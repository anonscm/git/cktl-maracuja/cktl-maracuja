-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Av_Mission IS
/*
 * Copyright Cocktail, 2001-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- rodolphe prin

-- permet d ajouter des details a une ecriture.
FUNCTION creerAvanceMission (
		 misOrdre INTEGER,
		 exeOrdre INTEGER,    -- exeOrdre sur lequel generer l'avance
		 fouOrdre INTEGER,
		 ribOrdre INTEGER,
		 orgId NUMBER,
		 vamMontant NUMBER,
		 modOrdreAvance NUMBER,
		 modOrdreRegul NUMBER,
		 utlOrdreDemandeur NUMBER,
		 odpOrdre NUMBER --reference a OP qui serait deja genere
		 )
		  RETURN INTEGER;

PROCEDURE supprimerAvanceMission(vamId VISA_AV_MISSION.vam_id%TYPE) ; 

FUNCTION recup_gescode(exeOrdre NUMBER, orgId NUMBER) RETURN VARCHAR2;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Av_Mission IS
-- PUBLIC --



FUNCTION creerAvanceMission (
		 misOrdre INTEGER,
		 exeOrdre INTEGER,    -- exeOrdre sur lequel generer l'avance
		 fouOrdre INTEGER,
		 ribOrdre INTEGER,
		 orgId NUMBER,
		 vamMontant NUMBER,
		 modOrdreAvance NUMBER,
		 modOrdreRegul NUMBER,
		 utlOrdreDemandeur NUMBER,
		 odpOrdre NUMBER
) RETURN INTEGER
IS
  flag              NUMBER;
  gescode GESTION.ges_code%TYPE;
  s					VARCHAR2(1000);
  mpAv			MODE_PAIEMENT%ROWTYPE;
  mpReg			MODE_PAIEMENT%ROWTYPE;
  miss				v_mission%ROWTYPE;
  vamId				VISA_AV_MISSION.vam_id%TYPE;
  
BEGIN
		s := '';
		IF misOrdre IS NULL THEN
		   s := s || 'misOrdre, ';
		END IF;

		IF exeOrdre IS NULL THEN
		   s := s || 'exeOrdre, ';
		END IF;

		IF fouOrdre IS NULL THEN
		   s := s || 'fouOrdre, ';
		END IF;

		IF (vamMontant IS NULL OR vamMontant=0) THEN
		   s := s || 'vamMontant, ';
		END IF;

		IF modOrdreAvance IS NULL THEN
		   s := s || 'modOrdreAvance, ';
		END IF;

		IF utlOrdreDemandeur IS NULL THEN
		   s := s || 'utlOrdreDemandeur, ';
		END IF;


		IF (LENGTH(s)>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Les parametres suivants sont obligatoires : '|| s ||'.'  );
		END IF;



		-- verifier que la mission existe
		SELECT COUNT(*) INTO flag FROM v_mission WHERE mis_ordre=misOrdre;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer une mission correspondant au mis_ordre ='|| misOrdre ||'.'  );
		END IF;

		-- Verifier exercice ouvert (pas possible de payer un OP sinon)
		SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_ordre=exeOrdre AND  exe_stat='O';
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Exercice ' || exeOrdre || ' non defini ou non ouvert. Impossible de creer une avance sur un exercice autre que l''exercice de trésorerie.'  );
		END IF;

		-- Verifier fournisseur existe
		SELECT COUNT(*) INTO flag FROM v_fournisseur WHERE fou_ordre=fouOrdre;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un fournisseur correspondant au fou_ordre ='|| fouOrdre ||'.'  );
		END IF;

		-- Verifier organ existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM v_organ_exer WHERE exe_ordre=exeOrdre AND org_id=orgId;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un organ correspondant a org_id ='|| orgId ||' et exe_ordre='||exeOrdre  );
		END IF;

		-- Verifier mode paiement avance existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM MODE_PAIEMENT WHERE exe_ordre=exeOrdre AND mod_ordre=modOrdreAvance;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un mode_paiement (modOrdreAvance) correspondant a mod_ordre ='|| modOrdreAvance ||' et exe_ordre='||exeOrdre  );
		END IF;

		-- Verifier mode paiement regul existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM MODE_PAIEMENT WHERE exe_ordre=exeOrdre AND mod_ordre=modOrdreRegul;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un mode_paiement (modOrdreRegul) correspondant a mod_ordre ='|| modOrdreRegul ||' et exe_ordre='||exeOrdre  );
		END IF;

		-- Verifier utilisateur existe
		SELECT COUNT(*) INTO flag FROM utilisateur WHERE utl_ordre=utlOrdreDemandeur;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un utilisateur correspondant au utl_ordre ='|| utlOrdreDemandeur ||'.'  );
		END IF;

		--
		SELECT * INTO mpAv FROM MODE_PAIEMENT WHERE mod_ordre=modOrdreAvance;
		SELECT * INTO mpReg FROM MODE_PAIEMENT WHERE mod_ordre=modOrdreRegul;
		SELECT * INTO miss FROM v_mission WHERE mis_ordre=misOrdre;

		-- verifier que RIB specifie si on a un mode de paiement du domaine virement
		IF (mpAv.MOD_DOM = 'VIREMENT') THEN
		   IF (ribOrdre IS NULL) THEN
		   	  RAISE_APPLICATION_ERROR (-20001,'Le ribOrdre est obligatoire pour un mode de paiement qui donne lieu a un VIREMENT' ||'.'  );
		   END IF;
		END IF;

		IF (mpAv.MOD_VALIDITE <> 'VALIDE') THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement avance mod_ordre ='|| modOrdreAvance ||' n''est pas VALIDE.'  );
		END IF;

		IF (mpReg.MOD_VALIDITE <> 'VALIDE') THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement regul mod_ordre ='|| modOrdreRegul ||' n''est pas VALIDE.'  );
		END IF;

		-- Si precise Verifier rib existe (si rib non valide, il y aura un warning lors de la gen de la disquette)
		IF (ribOrdre IS NOT NULL) THEN
			SELECT COUNT(*) INTO flag FROM v_rib WHERE rib_ordre=ribOrdre;
			IF flag = 0 THEN
			   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un rib correspondant au rib_ordre ='|| ribOrdre ||'.'  );
			END IF;

			-- verifier que le rib est bien affecte au fournisseur
			SELECT COUNT(*) INTO flag FROM v_rib WHERE rib_ordre=ribOrdre AND fou_ordre=fouOrdre;
			IF flag = 0 THEN
			   RAISE_APPLICATION_ERROR (-20001,'Le rib rib_ordre ='|| ribOrdre ||' n''est pas affecte au fournisseur  correspondant pas au fou_ordre '|| fouOrdre );
			END IF;
		END IF;

		---------
		-- verifier les pco_num
		IF (mpAv.pco_num_paiement IS NULL) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement avance mod_ordre='|| modOrdreAvance ||' n''a pas de PCO_NUM_PAIEMENT defini.' );
		END IF;
		IF (mpReg.pco_num_visa IS NULL) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement de regul mod_ordre='|| modOrdreRegul ||' n''a pas de PCO_NUM_VISA defini.' );
		END IF;


		---------
		-- recuperer le ges_code en fonction de l'org_ub
		gescode := recup_gescode(exeOrdre, orgId);
		
		
		----- TODO : verifier l''etat de la mission


		SELECT maracuja.visa_av_mission_SEQ.NEXTVAL INTO vamId FROM dual;

		INSERT INTO MARACUJA.VISA_AV_MISSION (
		   VAM_ID, MIS_ORDRE, EXE_ORDRE,
		   FOU_ORDRE, ORG_ID, VAM_MONTANT,
		   MOD_ORDRE_AVANCE, MOD_ORDRE_REGUL, RIB_ORDRE,
		   VAM_DATE_DEM, UTL_ORDRE_DEMANDEUR, TYET_ID,
		   VAM_DATE_VISA, UTL_ORDRE_VALIDEUR, ODP_ORDRE, ges_code)
		VALUES (
			   vamId,
			   misOrdre,
			   exeOrdre,
		   	   fouOrdre,
			   orgId,
			   vamMontant,
		   	   modOrdreAvance, --MOD_ORDRE_AVANCE,
			   modOrdreRegul, --MOD_ORDRE_REGUL,
			   ribOrdre, --RIB_ORDRE,
		   	   SYSDATE, --VAM_DATE_DEM,
			   utlOrdreDemandeur, --UTL_ORDRE_DEMANDEUR,
			   6, --TYET_ID,
		   	   NULL, --VAM_DATE_VISA,
			   NULL, --UTL_ORDRE_VALIDEUR,
			   odpOrdre, --ODP_ORDRE
			   gescode -- ges_code
			   );

		   RETURN vamId;

END;



PROCEDURE supprimerAvanceMission(vamId VISA_AV_MISSION.vam_id%TYPE) 
IS
  flag INTEGER;
  vam VISA_AV_MISSION%ROWTYPE;
BEGIN
	 	
		SELECT COUNT(*) INTO flag FROM VISA_AV_MISSION WHERE vam_id=vamId;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer une avance de mission correspondant au vam_id ='|| vamId ||'.'  );
		END IF;
	 
	   SELECT * INTO vam FROM VISA_AV_MISSION WHERE vam_id=vamId;
	   
	   -- si op existe on crie
	   IF (vam.odp_ordre IS NOT NULL) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Un ordre de paiement a deja ete genere/associe pour cette avance de mission. Adressez-vous a l''agence comptable pour qu''il l''annule.'  );
		END IF;
	 
	 -- verifier en plus l''etat de visa_av_mission (si different de EN ATTENTE on crie aussi)
	   IF (vam.tyet_id <> 6) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Cette avance de mission a deja ete (partiellement) traitee. Adressez vous a  l''agence comptable pour qu''elle soit eventuellemnt rejetee.'  );
		END IF;
	 
	 
	 -- on supprime violemment
	 DELETE FROM VISA_AV_MISSION WHERE vam_id=vamId;
	 
END;



FUNCTION recup_gescode(exeOrdre NUMBER, orgId NUMBER) RETURN VARCHAR2
IS
	   flag INTEGER;
	   gescode GESTION.ges_code%TYPE;
	   
BEGIN
		SELECT COUNT(*) INTO flag FROM v_organ_exer WHERE exe_ordre=exeOrdre AND org_id=orgId;
		
		
		IF (flag = 0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un organ correspondant a org_id ='|| orgId ||' et exe_ordre='||exeOrdre  );
		END IF;
	   
	   SELECT org_ub INTO gescode FROM v_organ_exer WHERE exe_ordre=exeOrdre AND org_id=orgId;
	   
	   SELECT COUNT(*) INTO flag FROM GESTION_EXERCICE WHERE exe_ordre=exeOrdre AND ges_code=gescode;
		IF (flag = 0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le code gestion '|| gescode ||' n''est pas defini pour l''exercice '||exeOrdre  );
		END IF;			   
	   		

	   RETURN gescode;
	   
END;


END;
/


GRANT EXECUTE ON  MARACUJA.AV_MISSION TO JEFY_MISSION;











---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------







-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Numerotationobject IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (ecrordre INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid INTEGER);

PROCEDURE numeroter_titre (borid INTEGER);

PROCEDURE numeroter_orv (borid INTEGER);

PROCEDURE numeroter_aor (borid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Numerotationobject IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE BROUILLARD INEXISTANTE , CLE '||ecrordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero_brouillard = lenumero
WHERE ecr_ordre = ecrordre;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_id = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre, exe_ordre
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_id = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE RETENUE SET ret_numero = lenumero
WHERE RET_ID=retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) IS


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau BORDEREAU%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



SELECT  COUNT(*) INTO cpt
FROM maracuja.TYPE_BORDEREAU tb , maracuja.TYPE_NUMEROTATION tn
WHERE tn.tnu_ordre = tb.tnu_ordre
AND tb.tbo_ordre = tboordre;

IF cpt = 1 THEN
 SELECT tn.tnu_ordre INTO tnuordre
 FROM maracuja.TYPE_BORDEREAU tb , TYPE_NUMEROTATION tn
 WHERE tn.tnu_ordre = tb.tnu_ordre
 AND tb.tbo_ordre = tboordre;
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

IF lenumero IS NULL THEN
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
END IF;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

END;


PROCEDURE numeroter_mandat (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid ORDER BY pco_num;

BEGIN

-- si c un bordereau orv
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre=8) THEN
   numeroter_orv(borid);
   RETURN;
END IF;



-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;



-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_titre (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid ORDER BY pco_num;


BEGIN


-- si c un bordereau aor
SELECT tbo_ordre INTO  tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre=9) THEN
   numeroter_aor(borid);
   RETURN;
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;

 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;



PROCEDURE numeroter_orv (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_aor (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;

END;
/


GRANT EXECUTE ON  MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;




-----------------
-----------------
-----------------


-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

	SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentmab.mab_montant,
	NULL,
	currentmab.mab_sens,
	ecrordre,
	currentmab.ges_code,
	currentmab.pco_num
	);

	SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

	-- Insertion des mandat_detail_ecritures
	INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
	ecdordre,
	currentmab.exe_ordre,
	currentmab.man_id,
	SYSDATE,
	mdeordre,
	'VISA',
	oriordre
	);



	END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
  	  boretat := 'RETENUES';
  ELSE
  	  boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

  IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;
  END IF;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

    IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET jou_ordre_sacd = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_sacd = ecrordre WHERE bor_id = borid;
  END IF;

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
				ecrordre := creerecriture(
				1,
				SYSDATE,
				'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
				exeordre,
				oriordre,
				14,
				9,
				0
				);
			
			  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
			  OPEN bordereaux;
			  LOOP
			    FETCH bordereaux INTO currentbob;
			    EXIT WHEN bordereaux%NOTFOUND;
			
						ecdordre := creerecrituredetail (
						NULL,
						'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
						currentbob.bob_montant,
						NULL,
						currentbob.bob_sens,
						ecrordre,
						currentbob.ges_code,
						currentbob.pco_num
						);
			
				END LOOP;
			  CLOSE bordereaux;
			
			  Api_Plsql_Journal.validerecriture(ecrordre);
			
			 
			  --UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;
			  
  END IF;

END IF;

	

		  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
		  UPDATE BORDEREAU
		  SET
		  bor_date_visa = SYSDATE,
		  utl_ordre_visa = 0,
		  bor_etat = 'PAIEMENT'
		  WHERE bor_id = borid;
		
		  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;		
		
		      IF (exeordre<2007) THEN
		      	 		UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;
		  	 ELSE
		  	  	 	 	UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'PAIEMENT', ECR_ORDRE_OPP = ecrordre WHERE bor_id = borid;
		  	 END IF;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
	 gescodes := '';
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
		 IF (LENGTH(gescodes)>0) THEN
		 	gescodes := gescodes || ', ';
		 END IF;
		  gescodes := gescodes || curgescode;
	END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
  	 RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;



IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	exeordre,
	oriordre,
	14,
	6,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);


	      IF (exeordre<2007) THEN
	     UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
	  END IF;
  END IF;

 ELSE
	      IF (exeordre<2007) THEN
	       UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;
	  END IF;


END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bob_libelle2 = borlibelle_mois;

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
	localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
	localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE 		--		INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/




