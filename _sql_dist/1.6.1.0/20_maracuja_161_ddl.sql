SET define OFF
SET scan OFF

ALTER TABLE MARACUJA.RETENUE_DETAIL DROP PRIMARY KEY CASCADE;
DROP TABLE MARACUJA.RETENUE_DETAIL CASCADE CONSTRAINTS;

ALTER TABLE MARACUJA.RETENUE DROP PRIMARY KEY CASCADE;
DROP TABLE MARACUJA.RETENUE CASCADE CONSTRAINTS;

ALTER TABLE MARACUJA.TYPE_RETENUE DROP PRIMARY KEY CASCADE;
DROP TABLE MARACUJA.TYPE_RETENUE CASCADE CONSTRAINTS;


DROP SEQUENCE MARACUJA.RETENUE_DETAIL_seq;




--ALTER TABLE MARACUJA.TYPE_RETENUE DROP PRIMARY KEY CASCADE;
--DROP TABLE MARACUJA.TYPE_RETENUE CASCADE CONSTRAINTS;

CREATE TABLE MARACUJA.TYPE_RETENUE
(
  TRE_ORDRE    NUMBER                           NOT NULL,
  TRE_LIBELLE  VARCHAR2(200)                    NOT NULL,
  PCO_NUM      VARCHAR2(20)	NOT NULL,
  tyet_id	   NUMBER NOT NULL
)
TABLESPACE GFC;

COMMENT ON COLUMN MARACUJA.TYPE_RETENUE.TRE_LIBELLE IS 'Libelle de ce type de retenue';
COMMENT ON COLUMN MARACUJA.TYPE_RETENUE.TRE_ORDRE IS 'Identifiant de ce type de retenue';

ALTER TABLE MARACUJA.TYPE_RETENUE ADD (  PRIMARY KEY (TRE_ORDRE)    USING INDEX    TABLESPACE GFC_INDX);

ALTER TABLE MARACUJA.TYPE_RETENUE ADD ( CONSTRAINT FK_TYPE_RETENUE_PCO_NUM FOREIGN KEY (PCO_NUM)   REFERENCES MARACUJA.PLAN_COMPTABLE (PCO_NUM));
ALTER TABLE MARACUJA.TYPE_RETENUE ADD ( CONSTRAINT FK_TYPE_RETENUE_TYET_ID FOREIGN KEY (tyet_id)   REFERENCES jefy_admin.type_etat(tyet_id));



CREATE TABLE MARACUJA.RETENUE
(	
  RET_ID     				NUMBER                           NOT NULL,
  ret_numero				NUMBER NOT NULL,
  com_ordre					NUMBER NOT NULL,
  exe_ordre					NUMBER  NOT NULL,
  DEP_ID       				NUMBER,
  RET_DATE     		  DATE                             NOT NULL,
  RET_MONTANT  NUMBER(12,2) NOT NULL,
  RET_LIBELLE  				VARCHAR2(2000) NOT NULL,
  tre_ordre	  				 NUMBER					   NOT NULL,
  utl_ordre					 NUMBER NOT NULL
);

COMMENT ON COLUMN MARACUJA.RETENUE.RET_ID IS 'Identifiant de la retenue';
COMMENT ON COLUMN MARACUJA.RETENUE.DEP_ID IS 'R�f�rence � la facture (d�pense) sur laquelle porte la retenue';
COMMENT ON COLUMN MARACUJA.RETENUE.RET_DATE IS 'Date de cr�ation';
COMMENT ON COLUMN MARACUJA.RETENUE.RET_MONTANT IS 'Montant de la retenue';
COMMENT ON COLUMN MARACUJA.RETENUE.RET_LIBELLE IS 'Libelle de la retenue';
COMMENT ON COLUMN MARACUJA.RETENUE.TRE_ORDRE IS 'reference au type de la retenue';
COMMENT ON COLUMN MARACUJA.RETENUE.UTL_ORDRE IS 'reference a l''utilisateur qui cr�e la retenue';


ALTER TABLE MARACUJA.RETENUE ADD (PRIMARY KEY (RET_ID) USING INDEX  TABLESPACE GFC_INDX);

ALTER TABLE MARACUJA.RETENUE ADD (  CONSTRAINT FK_RETENUE_DEP_ID FOREIGN KEY (DEP_ID)    REFERENCES MARACUJA.DEPENSE (DEP_ID));
ALTER TABLE MARACUJA.RETENUE ADD (  CONSTRAINT FK_RETENUE_TRE_ORDRE FOREIGN KEY (TRE_ORDRE)    REFERENCES MARACUJA.TYPE_RETENUE (TRE_ORDRE));
ALTER TABLE MARACUJA.RETENUE ADD (  CONSTRAINT FK_RETENUE_UTL_ORDRE FOREIGN KEY (UTL_ORDRE)    REFERENCES JEFY_ADMIN.UTILISATEUR (UTL_ORDRE));
ALTER TABLE MARACUJA.RETENUE ADD (  CONSTRAINT FK_RETENUE_COM_ORDRE FOREIGN KEY (COM_ORDRE)    REFERENCES MARACUJA.COMPTABILITE (COM_ORDRE));
ALTER TABLE MARACUJA.RETENUE ADD (  CONSTRAINT FK_RETENUE_EXE_ORDRE FOREIGN KEY (EXE_ORDRE)    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE));





CREATE OR REPLACE FORCE VIEW MARACUJA.v_odp_suivi
(
  ODP_ORDRE,
  EXE_ORDRE,
  ODP_NUMERO,
  FOU_ORDRE,
  ODP_ETAT,
  ODP_DATE_SAISIE,
  ODP_HT,
  ODP_TVA,
  ODP_TTC,
  PAI_NUMERO,
  TVI_LIBELLE,
  VIR_DATE_VALEUR,
  MOD_DOM,
  MOD_CODE,
  MOD_LIBELLE,
  ORI_ORDRE,
  PAI_ORDRE,
  ORG_ORDRE,
  RIB_ORDRE,
  VIR_DATE_CREATION
)
AS SELECT 
ODP_ORDRE, m.EXE_ORDRE, ODP_NUMERO,    
   FOU_ORDRE,  ODP_ETAT,ODP_DATE_SAISIE, 
   ODP_HT, ODP_TVA, ODP_TTC,
   pai_numero,tvi_libelle, VIR_DATE_VALEUR,  
   mp.MOD_DOM, mp.MOD_CODE, mp.mod_libelle,
   ORI_ORDRE, 
   m.PAI_ORDRE, ORG_ORDRE, 
   RIB_ORDRE,  
    vir_date_creation
FROM MARACUJA.ORDRE_DE_PAIEMENT m,
maracuja.MODE_PAIEMENT mp,
(
	 SELECT p.pai_ordre, p.pai_numero,tvi.tvi_libelle, VIR_DATE_VALEUR, vir_date_creation FROM PAIEMENT p, TYPE_VIREMENT tvi, (
	SELECT vf.pai_ordre, vf.VIR_DATE_VALEUR, vf.vir_date_creation 
	FROM VIREMENT_FICHIER vf,
	(SELECT pai_ordre , MAX(VIR_ordre) vir_ordre FROM maracuja.VIREMENT_FICHIER GROUP BY pai_ordre) x
	WHERE x.vir_ordre=vf.vir_ordre
	) z
	WHERE 
	p.tvi_ordre=tvi.tvi_ordre
	AND p.pai_ordre=z.pai_ordre(+)
) y
WHERE
m.mod_ordre=mp.mod_ordre
AND m.pai_ordre=y.pai_ordre(+);

GRANT SELECT ON maracuja.v_odp_suivi TO jefy_mission;
GRANT SELECT, REFERENCES ON maracuja.VISA_AV_MISSION TO jefy_mission WITH GRANT OPTION;
    
    
CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_TITRE
(EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE)
AS 
SELECT 2006 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE,
TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE,
TIT_VIREMENT, MOD_CODE,
DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
FROM jefy.TITRE f
WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
SELECT 2005 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy05.TITRE f
UNION ALL
SELECT 2004 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy04.TITRE f
UNION ALL
SELECT exe_ordre AS exe_exercice, tit_ordre, TIT_DATE_REMISE AS tit_date, REPLACE( TO_CHAR(TIT_TTC),',','.' )AS tit_mont,  REPLACE( TO_CHAR(TIT_TVA),',','.' )AS tit_monttva, tit_libelle AS tit_lib, NULL AS tit_type, tit_nb_piece AS tit_piece, TO_NUMBER(NULL) AS jou_ordre, f.fou_ordre, pco_num, NULL AS tit_imputtva, bor_ordre, org_ordre,
TO_NUMBER(NULL) AS tit_interne,  TIT_NUMero AS tit_num,utl_ORDRE AS agt_ordre, NULL AS TIT_STAT, GES_CODE,  f.ADR_NOM || ' ' || f.adr_prenom    AS TIT_DEBITEUR, TO_NUMBER(NULL) AS DEP_ORDRE, TO_NUMBER(NULL) AS RIB_ORDRE, NULL AS TIT_MONNAIE,
NULL AS TIT_VIREMENT, NULL AS MOD_CODE,
NULL AS DST_CODE, NULL AS TIT_REF, TO_NUMBER(NULL) AS CONV_ORDRE, NULL AS CAN_CODE, TO_NUMBER(NULL) AS PRES_ORDRE
FROM maracuja.TITRE t, v_fournisseur f
WHERE t.exe_ordre>2006 AND t.fou_ordre=f.fou_ordre;


GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_TITRE TO COMPTEFI;    

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit,
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM BORDEREAU b,
      TITRE t,
      TITRE_DETAIL_ECRITURE tde,
      ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  AND t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine IN ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  AND ecd_credit<>0
  AND tbo_ordre<>9 AND tbo_ordre<>11
  AND ed.pco_num NOT LIKE '185%'
  AND ABS (ecd_montant)=ABS(tit_ht)
UNION ALL
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit,
      (SIGN(ecd_credit))*t.tit_tva, ecd_credit+((SIGN(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM BORDEREAU b,
      TITRE t,
      TITRE_DETAIL_ECRITURE tde,
      ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  AND t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine IN ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  AND ecd_credit<>0
  AND tbo_ordre=11
  AND ed.pco_num NOT LIKE '185%'
  AND ABS (ecd_montant)=ABS(tit_ht)
UNION ALL
-- reductions
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie AS ecr_date, t.ges_code, ed.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_DEBIT,
      (SIGN(ecd_debit))*ABS(t.tit_tva), ecd_debit+((SIGN(ecd_debit))*ABS(t.tit_tva)),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM BORDEREAU b,
      TITRE t,
      TITRE_DETAIL_ECRITURE tde,
      ECRITURE_DETAIL ed,
      v_ecriture_infos ei,
      ECRITURE e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  AND t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  AND t.tit_ordre = jt.tit_ordre AND jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine IN ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  AND ecd_debit<>0
  AND tbo_ordre=9
  AND ed.pco_num NOT LIKE '185%'
  AND ABS (ecd_montant)=ABS(tit_ht);




    
