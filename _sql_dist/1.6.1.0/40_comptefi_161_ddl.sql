-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_DVLOP_REC_LISTE
(EXE_ORDRE, GES_CODE, SECTION, PCO_NUM_BDN, PCO_NUM, 
 RECETTES, REDUCTIONS, CR_OUVERTS, DEP_DATE)
AS 
	SELECT t.EXE_ORDRE, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num,
	SUM(tit_ht), 0, 0, e.date_saisie
	-- Titres Recettes
	FROM maracuja.TITRE T, maracuja.BORDEREAU b, v_planco p,
	(
	   SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
	   FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
	   WHERE
	   m.tit_id=mde.tit_ID
	   AND mde.ecd_ordre=ecd.ecd_ordre
	   AND ecd.ecr_ordre=e.ecr_ordre
	   AND SUBSTR(e.ecr_etat,1,1)='V'
	   GROUP BY m.tit_id
	) e
	WHERE t.PCO_NUM = p.PCO_NUM AND t.bor_id = b.bor_id AND (tbo_ordre = 7 OR tbo_ordre=11)
	AND b.bor_etat = 'VISE' AND t.tit_etat = 'VISE' AND t.tit_id = e.tit_id
	GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num,
	e.date_saisie
UNION ALL
	-- R�ductions de recettes <2007
	SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0,
	SUM (tit_ht), 0, e.date_saisie
	FROM maracuja.TITRE t, maracuja.bordereau b, v_planco p,
	(
	   SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
	   FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
	   WHERE
	   m.tit_id=mde.tit_ID
	   AND mde.ecd_ordre=ecd.ecd_ordre
	   AND ecd.ecr_ordre=e.ecr_ordre
	   AND SUBSTR(e.ecr_etat,1,1)='V'
	   GROUP BY m.tit_id
	) e
	WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 9 AND t.exe_ordre<2007
	AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id
	GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num,
	e.date_saisie
UNION ALL
	-- R�ductions de recettes >=2007
	SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0,
	-SUM (tit_ht), 0, e.date_saisie
	FROM maracuja.TITRE t, maracuja.bordereau b, v_planco p,
	(
	   SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie
	   FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m
	   WHERE
	   m.tit_id=mde.tit_ID
	   AND mde.ecd_ordre=ecd.ecd_ordre
	   AND ecd.ecr_ordre=e.ecr_ordre
	   AND SUBSTR(e.ecr_etat,1,1)='V'
	   GROUP BY m.tit_id
	) e
	WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 9 AND t.exe_ordre>=2007
	AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id
	GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num,
	e.date_saisie	
UNION ALL
	-- Cr�dits ouverts
	SELECT EXE_ORDRE, GES_CODE, '1', PCO_NUM, pco_num, 0,0, CO, DATE_CO
	FROM v_budnat
	WHERE (pco_num LIKE '6%' OR pco_num LIKE '7%')
	AND BDN_QUOI = 'R' AND co <> 0
	UNION ALL
	SELECT EXE_ORDRE, GES_CODE, '2', PCO_NUM, pco_num, 0,0, CO, DATE_CO
	FROM v_budnat
	WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%')
	AND BDN_QUOI = 'R' AND co <> 0;


