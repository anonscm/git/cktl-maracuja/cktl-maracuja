SET define OFF
SET scan OFF
begin

delete from MARACUJA.PLAN_COMPTABLE_EXER;

INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE)   
   SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
          EXE_ORDRE, 
          PCO_NUM, 
          PCO_NIVEAU, 
          PCO_BUDGETAIRE, 
          PCO_EMARGEMENT, 
          PCO_LIBELLE, 
          PCO_NATURE, 
          PCO_SENS_EMARGEMENT, 
          PCO_VALIDITE, 
          PCO_J_EXERCICE, 
          PCO_J_FIN_EXERCICE, 
          PCO_J_BE, 
          PCO_COMPTE_BE, 
          PCO_SENS_SOLDE            
   from maracuja.plan_comptable pco, jefy_admin.exercice ex 
   where ex.exe_ordre=2005
   ;

INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE)   
   SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
          EXE_ORDRE, 
          PCO_NUM, 
          PCO_NIVEAU, 
          PCO_BUDGETAIRE, 
          PCO_EMARGEMENT, 
          PCO_LIBELLE, 
          PCO_NATURE, 
          PCO_SENS_EMARGEMENT, 
          PCO_VALIDITE, 
          PCO_J_EXERCICE, 
          PCO_J_FIN_EXERCICE, 
          PCO_J_BE, 
          PCO_COMPTE_BE, 
          PCO_SENS_SOLDE            
   from maracuja.plan_comptable pco, jefy_admin.exercice ex
   where ex.exe_ordre = 2006
   ;

INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE)   
   SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
          EXE_ORDRE, 
          PCO_NUM, 
          PCO_NIVEAU, 
          PCO_BUDGETAIRE, 
          PCO_EMARGEMENT, 
          PCO_LIBELLE, 
          PCO_NATURE, 
          PCO_SENS_EMARGEMENT, 
          PCO_VALIDITE, 
          PCO_J_EXERCICE, 
          PCO_J_FIN_EXERCICE, 
          PCO_J_BE, 
          PCO_COMPTE_BE, 
          PCO_SENS_SOLDE            
   from maracuja.plan_comptable pco, jefy_admin.exercice ex
   where ex.exe_ordre=2007
   ;
   
INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE)   
   SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
          EXE_ORDRE, 
          PCO_NUM, 
          PCO_NIVEAU, 
          PCO_BUDGETAIRE, 
          PCO_EMARGEMENT, 
          PCO_LIBELLE, 
          PCO_NATURE, 
          PCO_SENS_EMARGEMENT, 
          PCO_VALIDITE, 
          PCO_J_EXERCICE, 
          PCO_J_FIN_EXERCICE, 
          PCO_J_BE, 
          PCO_COMPTE_BE, 
          PCO_SENS_SOLDE            
   from maracuja.plan_comptable pco, jefy_admin.exercice ex
   where ex.exe_ordre=2008
   ;   


--------------------

    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.6.8.0',  SYSDATE, '');

commit;

end;
