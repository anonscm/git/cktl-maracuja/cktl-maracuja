SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.Epn2
IS

FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2;
PROCEDURE  Epn_Proc_Bal_1 (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2, comp VARCHAR2, exe_ordre NUMBER, v_date VARCHAR2);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn2  IS
-- v. du 12/03/2007 (code nomen. M9-3, M9-1, etc.)

FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2
IS
  codeNomenclature maracuja.parametre.par_value%TYPE;
BEGIN
	SELECT PAR_VALUE INTO codeNomenclature FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'EPN_CODE_NOMENCLATURE' AND exe_ordre=exeOrdre;

	IF codeNomenclature IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'EPN_CODE_NOMENCLATURE doit etre renseigne dans maracuja.PARAMETRE');
	END IF;
	RETURN codeNomenclature;
END;


PROCEDURE Epn_Proc_Bal_1 (ordre VARCHAR2,comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2)
IS

JOU_DATE DATE;
IMPUTATION VARCHAR2(15);
GESTION VARCHAR2(10);
ECR_SACD EPN_BALANCE.ECR_SACD%TYPE;
BE_CR EPN_BALANCE.BE_CR%TYPE;
MONTANT_CR EPN_BALANCE.MONTANT_CR%TYPE;
BE_DB EPN_BALANCE.BE_DB%TYPE;
MONTANT_DB EPN_BALANCE.MONTANT_DB%TYPE;
NUM_SEQ NUMBER(5);
num_seq_f NUMBER(5);
bsdeb NUMBER(15,2);
bscre NUMBER(15,2);
debtot NUMBER(15,2);
cretot NUMBER(15,2);
NEW_EPNB_ORDRE INTEGER;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
TYP_DOC VARCHAR2(2);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num NUMBER;
codeNomenclature VARCHAR2(2);
flag INTEGER;

--erreur EXCEPTION(2);


	CURSOR c_tot IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), 	SUM(MONTANT_DB)
	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_sacd IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND GESTION = comp AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_hors_sacd IS SELECT IMPUTATION,ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
	AND GESTION NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY IMPUTATION, ECR_SACD
	ORDER BY IMPUTATION;


BEGIN


	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;

	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp =  'HSA'  THEN

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num  AND
			EPNB_GES_CODE NOT IN (SELECT ges_code FROM   SACD_PARAM );
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num
			AND EPNB_GES_CODE NOT IN (SELECT ges_code FROM jefy.SACD_PARAM );
		END IF;  */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_EXERCICE = exerc
			AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_EXERCICE = exerc
			AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL
		WHERE EPNB_ORDRE = ordre_num AND EPNB_GES_CODE = comp ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num;
		END IF;   */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_GES_CODE = comp
			AND EPNB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE = comp AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_GES_CODE = comp
			AND EPNB2_EXERCICE = exerc;
		END IF;

	END IF;

	--	SELECT SEQ_EPN_BAL.NEXTVAL INTO NEW_EPNB_ORDRE FROM dual;
	--  SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';


	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;

	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	-- TO_DATE(TO_CHAR(fin_premiere,'dd/mm/yyyy'),'dd/mm/yyyy')

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre = '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
	  		VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
			RANG := '03';
			VDATE := '3009' || EXERC;
	  		VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
	  			VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					VDATE:= V_DATE;
					RANG := '05';
	  				VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						VDATE:= V_DATE;
						RANG := '06';
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

--	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');
--	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');

	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');
	VARDATE := TO_DATE(VDATE,'ddmmyyyy');

    	/* INSERT INTO EPN_BAL VALUES (ORDRE, SYSDATE,comp); */

		--type doc := '03'

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;

		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_BAL_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,
EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET,0);

	NUM_SEQ := 2;

	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  		SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
		-- VDATE := '31122005';
		VDATE := '3112'|| EXERCN1;
	 	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	/* pas de consolidation Etablissement + SACD
	IF comp =  'ALL'  THEN
	-- Etablissement + SACD --
		OPEN c_tot;
		LOOP
		FETCH c_tot INTO IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_tot%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB,MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp = 'HSA' THEN
		-- 	Etablissement --
		OPEN c_hors_sacd;
	    LOOP
		FETCH c_hors_sacd INTO  IMPUTATION,  ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_hors_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB, MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- SACD	--
		OPEN c_sacd;
		LOOP
		FETCH c_sacd INTO  IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), BE_DB, MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_BAL_1 SET EPNB1_NB_ENREG = NUM_SEQ
	WHERE EPNB_ORDRe = ORDRE AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;

END;



PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
DEP_MONT  NUMBER(15,2);
REVERS NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c_tot IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS)
 	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd  IS SELECT PCO_NUM,SUM( DEP_MONT), SUM( REVERS)
	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY PCO_NUM
	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) FROM EPN_DEP_BUD
	WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

/*DELETE TAB_MANDAT_MAJ;

INSERT INTO TAB_MANDAT_MAJ SELECT * FROM MANDAT_MAJ;*/

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp = 'HSA'  THEN
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
		AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
			AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
		AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
			AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp
			AND EPNDB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp AND EPNDB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp
			AND EPNDB2_EXERCICE = exerc;
		END IF;

	END IF;


	-- SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre = EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans 	maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE := V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';

	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND EXE_ORDRE = exerc;
	END IF;

	INSERT INTO EPN_DEP_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET, 					EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;
	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		DATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	-- Pas de consolidation Etablissement + SACD
	/* IF comp = 'ALL'  THEN
	-- Etablissement + SACD --
	OPEN c_tot;
	LOOP
		FETCH c_tot INTO  PCO_NUM,  DEP_MONT, REVERS;
		EXIT WHEN c_tot%NOTFOUND;
		compte := PCO_NUM || '              ';
		INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0,  DEP_MONT-REVERS);
		NUM_SEQ := NUM_SEQ + 1;
	END LOOP;
	CLOSE c_tot;

	ELSE */
	IF comp = 'HSA' THEN
		--  hors SACD
		OPEN c_hors_sacd ;
		LOOP
			FETCH c_hors_sacd INTO  PCO_NUM, DEP_MONT, REVERS ;
			EXIT WHEN c_hors_sacd%NOTFOUND ;
			compte := PCO_NUM || '              ' ;
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0, DEP_MONT-REVERS, exerc) ;
			NUM_SEQ := NUM_SEQ + 1 ;
			END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		--  cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO  PCO_NUM,  DEP_MONT, REVERS;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0, DEP_MONT-REVERS, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;
	END IF;

	-- END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_DEP_BUD_1 SET EPNDB1_NBENREG = num_seq
	WHERE EPNDB_ORDRE = ORDRE_NUM AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;

END;


PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
MONT_REC  NUMBER(15,2);
ANNUL_TIT_REC NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c_tot IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND GES_CODE = comp AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
		AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
			AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
		AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
			AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc AND EPNRB1_GES_CODE = comp;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_GES_CODE = comp
			AND EPNRB1_EXERCICE = exerc;
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num  AND EPNRB2_GES_CODE = comp AND EPNRB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_GES_CODE = comp
			AND EPNRB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
						RANG := '05';
						VDATE := V_DATE;
  						VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp,'1', 1, IDENTIFIANT,TYP_DOC, codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;

 	/*
	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
 	*/

	/*IF comp = 'ALL' THEN
	-- Etablissement + SACD
		OPEN c_tot;
		LOOP
			FETCH c_tot INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_tot%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp =  'HSA' THEN
	-- 	hors SACD
		OPEN c_hors_sacd;
		LOOP
			FETCH c_hors_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_hors_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, (SUBSTR(compte,1,15)),
				MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 - MAJ Nb enreg en fonction du ges_code
	UPDATE EPN_REC_BUD_1 SET EPNRB1_NBENREG = num_seq
	WHERE EPNRB_ORDRE = ORDRE_NUM AND EPNRB1_GES_CODE = comp AND EPNRB1_EXERCICE = exerc;


END;



PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)

IS

NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num     NUMBER;
PCONUM      VARCHAR2(6);
MNTOUV      NUMBER(15,2);
MNTNET      NUMBER(15,2);
MNTNOEMPLOI NUMBER(15,2);
SENSCPT		VARCHAR2(1);
TYP_DOC  VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c2_tot IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_hors_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;

	CURSOR c3_tot  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_hors_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;


	/*
	CURSOR c2 IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP
	GROUP BY pco_num_bdn;

	CURSOR c3 IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC
	GROUP BY pco_num_bdn;
	*/

BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	DELETE FROM BUDNAT_PLANCO;

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,2), '1'
	FROM maracuja.PLAN_COMPTABLE
	WHERE (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%') AND PCO_VALIDITE = 'VALIDE';

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,3), '2'
	FROM maracuja.PLAN_COMPTABLE
	WHERE ( PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%')AND PCO_VALIDITE = 'VALIDE';

	/* des donn�es du cadre 2 */

	DELETE FROM EPN_DEP;

	INSERT INTO EPN_DEP
	-- depenses + OR --
	SELECT
		db.PCO_NUM,
		bp.PCO_NUM_BDN,
		db.GES_CODE,
		db.DEP_MONT MDT_MONT,
		db.REVERS TIT_MONT,
		0 CO,
		bp.SECTION,
		db.EPN_DATE EPN_DATE,
		db.EXE_ORDRE
		FROM EPN_DEP_BUD db, BUDNAT_PLANCO bp
		WHERE db.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_DEP
	-- OR --
	SELECT
	  EPN_DEP0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_DEP0.GES_CODE,
	  EPN_TITV.ECD_DATE EPN_DATE,
	  0,
	  EPN_TITV.ECD_MONTANT TIT_MONT,
	  EPN_DEP0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITV, EPN_DEP0, BUDNAT_PLANCO
	WHERE EPN_DEP0.GES_CODE = EPN_TITV.GES_CODE (+)
	AND EPN_DEP0.PCO_NUM = EPN_TITV.PCO_NUM (+)
	AND EPN_DEP0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_DEP 	SET MDT_MONT = 0
		WHERE MDT_MONT IS NULL;

	UPDATE EPN_DEP	SET TIT_MONT = 0
		WHERE TIT_MONT IS NULL;*/

	DELETE FROM EPN_DEP WHERE MDT_MONT = 0 AND CO = 0 AND TIT_MONT = 0;

	INSERT INTO EPN_DEP
	-- pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn , BUDNAT_PLANCO bp
	WHERE bn.PCO_NUM = bp.PCO_NUM_bdn
	AND (bn.PCO_NUM LIKE '6%' OR bn.PCO_NUM LIKE '2%')
	AND CO <> 0
	ORDER BY exe_ordre, ges_code, bn.pco_num;


	/* Impossible de faire des pr�visions budg�taires de d�penses classe 1 dans jefyco --
	INSERT INTO EPN_DEP
	SELECT UNIQUE EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .GES_CODE, DATE_CO,
	0, 0, EPN_BUDNAT .CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT .PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT .PCO_NUM
	AND BUDNAT_PLANCO.PCO_NUM LIKE '1%';*/

/* des donn�es du cadre 3 */

	DELETE FROM EPN_REC;

	INSERT INTO EPN_REC
	-- Recettes et D�ductions--
	SELECT erb.PCO_NUM,
	  bp.PCO_NUM_BDN,
	  erb.GES_CODE,
	  erb.MONT_REC TIT_MONT,
	  erb.ANNUL_TIT_REC RED_MONT,
	  0 CO,
	  bp.SECTION,
	  erb.EPN_DATE,
	  erb.exe_ordre
	FROM EPN_REC_BUD erb, BUDNAT_PLANCO bp
	WHERE erb.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_REC
	-- Reduction recettes --
	SELECT EPN_REC0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_REC0.GES_CODE,
	  EPN_TITD.ECD_DATE EPN_DATE,
	  0 TIT_MONT,
	  EPN_TITD.ECD_MONTANT RED_MONT,
	  EPN_REC0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITD,  EPN_REC0, BUDNAT_PLANCO
	WHERE EPN_REC0.GES_CODE = EPN_TITD.GES_CODE (+)
	AND EPN_REC0.PCO_NUM = EPN_TITD.PCO_NUM (+)
	AND EPN_REC0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_REC SET TIT_MONT = 0 WHERE TIT_MONT IS NULL;

	UPDATE EPN_REC SET RED_MONT = 0 WHERE RED_MONT IS NULL;*/

	DELETE FROM EPN_REC WHERE TIT_MONT = 0 AND RED_MONT = 0 AND CO = 0;


	INSERT INTO EPN_REC
	-- Pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn, BUDNAT_PLANCO bp
	WHERE bp.PCO_NUM_BDN = bn.PCO_NUM
	AND ( bn.PCO_NUM LIKE '7%' OR bn.PCO_NUM LIKE '1%')
	AND CO <> 0;


	/* Impossible de faire des pr�visions budg�taires de recettes classe 2 dans jefyco
	INSERT INTO EPN_REC
	SELECT UNIQUE EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.GES_CODE, DATE_CO, 0, 0,
	EPN_BUDNAT.CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT.PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT.PCO_NUM
	AND (BUDNAT_PLANCO.PCO_NUM LIKE '26%' OR BUDNAT_PLANCO.PCO_NUM LIKE '27%');*/



	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
		AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
			AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
		AND EPNCCB2_GES_CODE  NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
			AND EPNCCB2_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_GES_CODE = comp
			AND EPNCCB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB2_GES_CODE = comp AND EPNCCB2_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_GES_CODE = comp
			AND EPNCCB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C
	FROM maracuja.PARAMETRE WHERE PAR_KEY = 	'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;


	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
				VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE:= V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						RANG := '06';
						VDATE := V_DATE;
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'ddmmyyyy');
	VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

	--	type doc = '03'
	-- SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;

	--IF (comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
	-- Correction M1
	TYP_DOC := '03';


	-- Correction M1 recherche du code du budget
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;

	INSERT INTO EPN_CRE_BUD_1 VALUES (ORDRE,comp, '1', 1, IDENTIFIANT,TYP_DOC , codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);


	/*
	  IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
     END IF;
	*/

	NUM_SEQ := 2;

	/*IF comp = 'ALL' THEN
		OPEN c2_tot;
		LOOP
			FETCH c2_tot INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
			ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2
			VALUES (ORDRE, comp, '2', num_seq, 1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_tot;

		OPEN c3_tot;
		LOOP
			FETCH c3_tot INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_tot;

	ELSE */
	IF comp =  'HSA' THEN

		OPEN c2_hors_sacd;
		LOOP
			FETCH c2_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_hors_sacd;

		OPEN c3_hors_sacd;
		LOOP
			FETCH c3_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_hors_sacd;

	ELSE
		OPEN c2_sacd;
		LOOP
			FETCH c2_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_sacd;


		OPEN c3_sacd;
		LOOP
			FETCH c3_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE  MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

			END LOOP;
			CLOSE c3_sacd;

	END IF;

	--END IF;

UPDATE EPN_CRE_BUD_1 SET EPNCCB1_NBENREG = num_seq
WHERE EPNCCB_ORDRE = ORDRE_NUM AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;

END;

END;
/


SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT" AS

PROCEDURE creer_bordereau (abrid INTEGER) IS
cpt INTEGER;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
monborid_dep INTEGER;
monborid_rec INTEGER;

cursor lesmandats is
select man_id from mandat where bor_id = monborid_dep;

cursor lestitres is
select tit_id from titre where bor_id = monborid_rec;

tmpmandid integer;
tmptitid integer;
tboordre integer;
BEGIN

-- est ce une selection vide ???
SELECT COUNT(*) INTO cpt
FROM ABRICOT_BORD_SELECTION
where abr_id = abrid;


if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/


-- recup du group by pour traiter les cursors
abrgroupby := recup_groupby(abrid);

IF (abrgroupby = 'bordereau_1R1T') THEN
 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1R1T(abrid,monborid_rec);


END IF;

IF (abrgroupby = 'bordereau_1D1M') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);


END IF;


/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);
 bordereau_1R1T(abrid,monborid_rec);


END IF;
*/

IF (abrgroupby NOT IN ( 'bordereau_1R1T','bordereau_1D1M','bordereau_1D1M1R1T')) THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_ND1M(abrid,monborid_dep);



END IF;


IF (monborid_dep is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_dep);

 open lesmandats;
 loop
 fetch lesmandats into tmpmandid;
 exit when lesmandats%notfound;
 get_depense_jefy_depense(tmpmandid);
 end loop;
 close lesmandats;

 controle_bordereau(monborid_dep);

END IF;

IF (monborid_rec  is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_rec);

  open lestitres;
 loop
 fetch lestitres into tmptitid;
 exit when lestitres%notfound;
 -- recup du brouillard
 get_recette_jefy_recette(tmptitid);
 Set_Titre_Brouillard(tmptitid);
 Get_recette_prelevements(tmptitid);

 end loop;
 close lestitres;

 controle_bordereau(monborid_rec);
END IF;

-- maj de l etat dans la selection
IF (monborid_dep is not null OR monborid_rec  is not null) then

 if monborid_rec  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_rec
  WHERE abr_id = abrid;
 end if;

 if monborid_dep  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_dep
  WHERE abr_id = abrid;

  select tbo_ordre into tboordre from bordereau where bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
  if tboordre = 3  then
   bordereau_abricot_paye.basculer_bouillard_paye(monborid_dep);
  end if;


-- pour les bordereaux d'orv de papaye on retravaille le brouillard
  if tboordre = 18  then
   bordereau_abricot_paye.basculer_bouillard_paye_orv(monborid_dep);
  end if;


-- pour les bordereaux de regul de papaye on retravaille le brouillard
  if tboordre = 19  then
   bordereau_abricot_paye.basculer_bouillard_paye_regul(monborid_dep);
  end if;



 end if;

end if;


end if;


END;

PROCEDURE viser_bordereau_rejet (brjordre INTEGER) IS
cpt INTEGER;
manid maracuja.mandat.man_id%type;
titid maracuja.titre.tit_id%type;
tboordre integer;

reduction integer;
utlordre integer;
dpcoid integer;
depsuppression varchar2(20);
rpcoid integer;
recsuppression varchar2(20);
exeordre integer;

cursor mandats is
select man_id from  maracuja.mandat where brj_ordre = brjordre;

cursor depenses is
select dep_ordre,dep_suppression from  maracuja.depense where man_id = manid;


cursor titres is
select tit_id from  maracuja.titre where brj_ordre = brjordre;

cursor recettes is
select rec_ordre,rec_suppression from  maracuja.recette where tit_id = titid;

deliq integer;
BEGIN

open mandats;
loop
fetch mandats into manid;
exit when mandats%notfound;

open depenses;
loop
fetch depenses into dpcoid,depsuppression;
exit when depenses%notfound;
 -- casser le liens des mand_id dans depense_ctrl_planco
   -- supprimer le liens compteble <-> depense dans l inventaire
 jefy_depense.ABRICOT.upd_depense_ctrl_planco (dpcoid,null);

select tbo_ordre,exe_ordre into tboordre,exeordre from jefy_depense.depense_ctrl_planco
where dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 201
 if depsuppression = 'OUI' and  tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
    
  deliq:=jefy_depense.Get_Fonction('DELIQ');
  
  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
  uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';
  
    if utlordre is null then
    deliq:=jefy_depense.Get_Fonction('DELIQINV');
  
  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre 
  AND ufe.exe_ordre=exeordre 
  AND uf.fon_ordre=deliq 
  AND ufe.exe_ordre=e.exe_ordre 
  AND exe_stat_eng='R';
  end if;
  
  jefy_depense.ABRICOT.del_depense_ctrl_planco (dpcoid,utlordre);
 end if;

end loop;
close depenses;

end loop;
close mandats;

open titres;
loop
fetch titres into titid;
exit when titres%notfound;

open recettes;
loop
fetch recettes into rpcoid,recsuppression;
exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
 SELECT r.rec_id_reduction INTO reduction
 FROM jefy_recette.RECETTE r, jefy_recette.RECETTE_CTRL_PLANCO rpco
 WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

 IF reduction IS NOT NULL THEN
  jefy_recette.API.upd_reduction_ctrl_planco (rpcoid,null);
 ELSE
  jefy_recette.API.upd_recette_ctrl_planco (rpcoid,null);
 END IF;

-- PAS DE GESTION DES SUPPRESSIONS

end loop;
close recettes;


end loop;
close titres;

-- on passe le brjordre a VISE
update bordereau_rejet set brj_etat = 'VISE'
where brj_ordre = brjordre;

END;



function get_selection_id (info varchar ) return integer is
selection integer;
begin
select
maracuja.ABRICOT_BORD_SELECTION_SEQ.nextval into selection from dual;
return selection;
end;


function get_selection_borid (abrid integer) return integer is
borid integer;
begin
select distinct bor_id into borid from maracuja.ABRICOT_BORD_SELECTION where abr_id = abrid;
return borid;
end;


procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar) is

chaine varchar(5000);
premier integer;
tmpdepid integer;
tmprecid integer;
cpt integer;

begin
/*
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

-- traitement de la chaine des depid
if a02lesdepid is not null OR length(a02lesdepid) >0 then
chaine := a02lesdepid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdepid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
tmpdepid,--DEP_ID
null,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

 -- traitement de la chaine des recid
if a03lesrecid is not null OR length(a03lesrecid) >0 then
chaine := a03lesrecid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmprecid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
null,--DEP_ID
tmprecid,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

select count(*) into cpt
from jefy_depense.depense_ctrl_planco
where dpco_id in ( select DEP_ID from abricot_bord_selection where abr_id = a01abrid)
and man_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
end if;


select count(*) into cpt
from jefy_recette.recette_ctrl_planco
where rpco_id in ( select REC_ID from abricot_bord_selection where abr_id = a01abrid)
and tit_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
end if;


bordereau_abricot.creer_bordereau(a01abrid);
end;


procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
begin

-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

-- les mandats
set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,201 ,'bordereau_1D1M' ,a08gescodemandat );

-- les titres
set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
boridtmp integer;
moisordre integer;
begin
/*
 -- a07abrgroupby = mois
 select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

 -- CONTROLES
 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

 if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

 select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

 if (cpt = 1) then
  raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
 end if;
*/
-- ATENTION
-- tboordre : 3 salaires

-- les mandats de papaye
 set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,3 ,'bordereau_1D1M' ,a08gescodemandat );
 boridtmp :=get_selection_borid (a01abrid);

 /*
-- maj de l etat de papaye_compta et du bor_ordre -
 update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

-- bascule du brouillard de papaye


-- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


-- creer bordereau (tbo_ordre) + numerotation
FUNCTION get_num_borid (tboordre INTEGER,exeordre INTEGER,gescode VARCHAR,utlordre INTEGER ) RETURN INTEGER IS
cpt INTEGER;
borid INTEGER;
bornum INTEGER;
BEGIN
-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
bornum :=-1;
INSERT INTO BORDEREAU
(
BOR_DATE_VISA,
BOR_ETAT,
BOR_ID,
BOR_NUM,
BOR_ORDRE,
EXE_ORDRE,
GES_CODE,
TBO_ORDRE,
UTL_ORDRE,
UTL_ORDRE_VISA,
BOR_DATE_CREATION
)
VALUES (
NULL ,                         --BOR_DATE_VISA,
'VALIDE',              --BOR_ETAT,
borid,                  --BOR_ID,
bornum,                              --BOR_NUM,
-borid,                             --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
exeordre,                --EXE_ORDRE,
gescode,                          --GES_CODE,
tboordre,              --TBO_ORDRE,
utlordre,               --UTL_ORDRE,
NULL,                  --UTL_ORDRE_VISA
sysdate
);

RETURN borid;

END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
PROCEDURE bordereau_1R1T(abrid INTEGER,monborid INTEGER) IS

cpt INTEGER;
tmprecette jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;

CURSOR rec_tit IS
SELECT r.*
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by r.pco_num ASC;

BEGIN


OPEN rec_tit;
LOOP
FETCH rec_tit INTO tmprecette;
EXIT WHEN rec_tit%NOTFOUND;
cpt:=set_titre_recette(tmprecette.rpco_id,monborid);
END LOOP;
CLOSE rec_tit;

END;

PROCEDURE bordereau_ND1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;

tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
CURSOR mand_dep_convra IS
SELECT distinct d.*
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.engage_budget e,
maracuja.V_CONVENTION_LIMITATIVE c
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND  db.dep_id = d.dep_id
AND e.eng_id = db.eng_id
AND e.org_id = c.org_id (+)
AND e.exe_ordre = c.exe_ordre (+)
AND c.org_id IS NOT NULL
and d.man_id is null
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;


-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.


BEGIN


OPEN mand_dep_convra;
LOOP
FETCH mand_dep_convra INTO tmpdepense;
EXIT WHEN mand_dep_convra%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco_mod'  ) THEN
 cpt:=ndep_mand_org_fou_rib_pco_mod(abrid ,monborid );
END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_fou_rib_pco_mod') THEN
 cpt:=ndep_mand_fou_rib_pco_mod(abrid ,monborid );
END IF;


END;

PROCEDURE bordereau_1D1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;
tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;


CURSOR dep_mand IS
SELECT d.*
FROM ABRICOT_BORD_SELECTION ab,jefy_depense.depense_CTRL_PLANCO d
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;

BEGIN


OPEN dep_mand;
LOOP
FETCH dep_mand INTO tmpdepense;
EXIT WHEN dep_mand%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE dep_mand;

END;

PROCEDURE bordereau_1D1M1R1T(abrid INTEGER,boridep INTEGER,boridrec INTEGER) IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
bordereau_1D1M(abrid,boridep);
bordereau_1R1T(abrid,boridrec);


END;


-- les mandats et titres


FUNCTION set_mandat_depense (dpcoid INTEGER,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;


ladepense       jefy_depense.depense_ctrl_planco%ROWTYPE;
ladepensepapier jefy_depense.depense_papier%ROWTYPE;
leengagebudget  jefy_depense.engage_budget%ROWTYPE;

gescode     GESTION.ges_code%TYPE;
manid         MANDAT.man_id%TYPE;

MANORGINE_KEY   MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB  MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE     MANDAT.ORI_ORDRE%TYPE;
PRESTID     MANDAT.PREST_ID%TYPE;
TORORDRE     MANDAT.TOR_ORDRE%TYPE;
VIRORDRE     MANDAT.PAI_ORDRE%TYPE;
MANNUMERO       MANDAT.MAN_NUMERO%TYPE;
BEGIN

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

SELECT * INTO ladepense
FROM  jefy_depense.depense_ctrl_planco
WHERE dpco_id = dpcoid;

SELECT DISTINCT dpp.* INTO ladepensepapier
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and dpco_id = dpcoid;

SELECT eb.* INTO leengagebudget
FROM jefy_depense.ENGAGE_BUDGET eb,jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.eng_id = eb.eng_id
and  db.dep_id = dpco.dep_id
and dpco_id = dpcoid;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(leengagebudget.org_id,leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.dep_id = ladepense.dep_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 and d.dep_id = ladepense.dep_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- recup du numero de mandat
MANNUMERO := -1;
INSERT INTO MANDAT
(
BOR_ID,
BRJ_ORDRE,
EXE_ORDRE,
FOU_ORDRE,
GES_CODE,
MAN_DATE_REMISE,
MAN_DATE_VISA_PRINC,
MAN_ETAT,
MAN_ETAT_REMISE,
MAN_HT,
MAN_ID,
MAN_MOTIF_REJET,
MAN_NB_PIECE,
MAN_NUMERO,
MAN_NUMERO_REJET,
MAN_ORDRE,
MAN_ORGINE_KEY,
MAN_ORIGINE_LIB,
MAN_TTC,
MAN_TVA,
MOD_ORDRE,
ORI_ORDRE,
PCO_NUM,
PREST_ID,
TOR_ORDRE,
PAI_ORDRE,
ORG_ORDRE,
RIB_ORDRE_ORDONNATEUR,
RIB_ORDRE_COMPTABLE
)
VALUES
(
borid ,                   --BOR_ID,
NULL,                        --BRJ_ORDRE,
ladepensepapier.exe_ordre,                   --EXE_ORDRE,
ladepensepapier.fou_ordre,--FOU_ORDRE,
gescode,                --GES_CODE,
NULL,                    --MAN_DATE_REMISE,
NULL,                    --MAN_DATE_VISA_PRINC,
'ATTENTE',                --MAN_ETAT,
'ATTENTE',                --MAN_ETAT_REMISE,
ladepense.dpco_montant_budgetaire, --MAN_HT,
manid,                    --MAN_ID,
NULL,                    --MAN_MOTIF_REJET,
ladepensepapier.dpp_nb_piece,--MAN_NB_PIECE,
MANNUMERO,--MAN_NUMERO,
NULL,                    --MAN_NUMERO_REJET,
-manid,--MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
MANORGINE_KEY,            --MAN_ORGINE_KEY,
MANORIGINE_LIB,        --MAN_ORIGINE_LIB,
ladepense.dpco_ttc_saisie, --MAN_TTC,
ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,  --MAN_TVA,
ladepensepapier.mod_ordre,                --MOD_ORDRE,
ORIORDRE,                --ORI_ORDRE,
ladepense.pco_num,    --PCO_NUM,
PRESTID,                --PREST_ID,
TORORDRE,                --TOR_ORDRE,
VIRORDRE,                --VIR_ORDRE
leengagebudget.org_id,  --org_ordre
ladepensepapier.rib_ordre, --rib ordo
ladepensepapier.rib_ordre -- rib_comptable
);

-- maj du man_id  dans la depense
UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
SET man_id = manid
WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

-- recup du brouillard
set_mandat_brouillard(manid);


RETURN manid;

END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
FUNCTION set_mandat_depenses (lesdpcoid VARCHAR,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;
premier integer;
tmpdpcoid integer;
chaine varchar(5000);
premierdpcoid integer;
manid integer;

ttc mandat.man_ttc%type;
tva mandat.man_tva%type;
ht mandat.man_ht%type;

utlordre integer;
nb_pieces integer;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);


premierdpcoid:=null;
 -- traitement de la chaine des depid xx$xx$xx$.....$x$$
if lesdpcoid is not null OR length(lesdpcoid) >0 then
chaine := lesdpcoid;
LOOP
  premier:=1;
  -- On recupere le depid
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdpcoid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;
-- creation du mandat lie au borid
 if premierdpcoid is null then
  manid:=set_mandat_depense(tmpdpcoid,borid);
  -- suppression du brouillard car il est uniquement sur la premiere depense
  delete from mandat_brouillard where man_id = manid;
  premierdpcoid:=tmpdpcoid;
 else
  -- maj du man_id  dans la depense
  UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
  SET man_id = manid
  WHERE dpco_id = tmpdpcoid;

  -- recup de la depense (maracuja)
  SELECT DISTINCT dpp.utl_ordre INTO utlordre
  FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
  WHERE db.dep_id = dpco.dep_id
  AND dpp.dpp_id = db.dpp_id
  and dpco_id = tmpdpcoid;

--  get_depense_jefy_depense(manid,utlordre);

 end if;

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
select
sum(dpco_ttc_saisie),
sum(dpco_ttc_saisie - dpco_montant_budgetaire),
sum(dpco_montant_budgetaire)
into
ttc,
tva,
ht
from jefy_depense.DEPENSE_CTRL_PLANCO
where man_id = manid;

-- recup du nb de pieces
SELECT sum(dpp.dpp_nb_piece) INTO nb_pieces
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and man_id = manid;


-- maj du mandat
update mandat set
man_ht = ht,
man_tva = tva,
man_ttc = ttc,
man_nb_piece = nb_pieces
where man_id = manid;

-- recup du brouillard
set_mandat_brouillard(manid);

RETURN cpt;

END;



FUNCTION set_titre_recette (rpcoid INTEGER,borid INTEGER)  RETURN INTEGER IS

jefytitre         jefy.TITRE%ROWTYPE;
gescode       GESTION.ges_code%TYPE;
titid           TITRE.tit_id%TYPE;

TITORGINEKEY     TITRE.tit_ORGINE_KEY%TYPE;
TITORIGINELIB     TITRE.tit_ORIGINE_LIB%TYPE;
ORIORDRE       TITRE.ORI_ORDRE%TYPE;
PRESTID       TITRE.PREST_ID%TYPE;
TORORDRE       TITRE.TOR_ORDRE%TYPE;
modordre      TITRE.mod_ordre%TYPE;
presid          INTEGER;
cpt          INTEGER;
VIRORDRE          INTEGER;


recettepapier jefy_recette.recette_papier%ROWTYPE;
recettebudget jefy_recette.recette_budget%ROWTYPE;
facturebudget jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco    jefy_recette. RECETTE_CTRL_PLANCO%rowtype;

BEGIN


-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);

SELECT * INTO recettectrlplanco
FROM  jefy_recette. RECETTE_CTRL_PLANCO
WHERE rpco_id = rpcoid;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;


-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
TITORGINEKEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
TITORIGINELIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(factureBUDGET.org_id,factureBUDGET.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.rec_id = recettectrlplanco.rec_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 AND d.rec_id = recettectrlplanco.rec_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

SELECT titre_seq.NEXTVAL INTO titid FROM dual;

        INSERT INTO TITRE (
           BOR_ID,
           BOR_ORDRE,
           BRJ_ORDRE,
           EXE_ORDRE,
           GES_CODE,
           MOD_ORDRE,
           ORI_ORDRE,
           PCO_NUM,
           PREST_ID,
           TIT_DATE_REMISE,
           TIT_DATE_VISA_PRINC,
           TIT_ETAT,
           TIT_ETAT_REMISE,
           TIT_HT,
           TIT_ID,
           TIT_MOTIF_REJET,
           TIT_NB_PIECE,
           TIT_NUMERO,
           TIT_NUMERO_REJET,
           TIT_ORDRE,
           TIT_ORGINE_KEY,
           TIT_ORIGINE_LIB,
           TIT_TTC,
           TIT_TVA,
           TOR_ORDRE,
           UTL_ORDRE,
           ORG_ORDRE,
           FOU_ORDRE,
           MOR_ORDRE,
           PAI_ORDRE,
           rib_ordre_ordonnateur,
           rib_ordre_comptable,
           tit_libelle)
        VALUES
            (
            borid,--BOR_ID,
            -borid,--BOR_ORDRE,
            NULL,--BRJ_ORDRE,
            recettepapier.exe_ordre,--EXE_ORDRE,
            gescode,--GES_CODE,
            null,--MOD_ORDRE, n existe plus en 2007 vestige des ORVs
            oriordre,--ORI_ORDRE,
            recettectrlplanco.pco_num,--PCO_NUM,
            PRESTID,--PREST_ID,
            sysdate,--TIT_DATE_REMISE,
            NULL,--TIT_DATE_VISA_PRINC,
            'ATTENTE',--TIT_ETAT,
            'ATTENTE',--TIT_ETAT_REMISE,
            recettectrlplanco.rpco_HT_SAISIE,--TIT_HT,
            titid,--TIT_ID,
            NULL,--TIT_MOTIF_REJET,
            recettepapier.rpp_nb_piece,--TIT_NB_PIECE,
            -1,--TIT_NUMERO, numerotation en fin de transaction
            NULL,--TIT_NUMERO_REJET,
            -titid,--TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
            TITORGINEKEY,--TIT_ORGINE_KEY,
            TITORIGINELIB,--TIT_ORIGINE_LIB,
            recettectrlplanco.rpco_TTC_SAISIE,--TIT_TTC,
            recettectrlplanco.rpco_TVA_SAISIE,--TIT_TVA,
            TORORDRE,--TOR_ORDRE,
            recettepapier.utl_ordre,--UTL_ORDRE
            facturebudget.org_id,        --ORG_ORDRE,
            recettepapier.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
            facturebudget.mor_ordre,                 --MOR_ORDRE
            NULL, -- VIR_ORDRE
            recettepapier.rib_ordre,
            recettepapier.rib_ordre,
            recettebudget.rec_lib
            );


-- maj du tit_id dans la recette
update jefy_recette.RECETTE_CTRL_PLANCO
set tit_id = titid
where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);

RETURN titid;
END;



FUNCTION set_titre_recettes (lesrpcoid VARCHAR,borid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RAISE_APPLICATION_ERROR (-20001,'OPERATION NON TRAITEE');
RETURN cpt;
END;

/*

FUNCTION ndep_mand_org_fou_rib_pco (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_org_fou_rib_pco IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num;

CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;

CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco;
LOOP
FETCH ndep_mand_org_fou_rib_pco INTO
orgid,fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;
  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco;

RETURN cpt;
END;

*/

FUNCTION ndep_mand_org_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;


CURSOR ndep_mand_org_fou_rib_pco_mod IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_org_fou_rib_pco_mod INTO
orgid,fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco_mod;

RETURN cpt;
END;
/*
FUNCTION ndep_mand_fou_rib_pco  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN

OPEN ndep_mand_fou_rib_pco;
LOOP
FETCH ndep_mand_fou_rib_pco INTO
fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco;

RETURN cpt;
END;
*/

FUNCTION ndep_mand_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco_mod IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;

CURSOR lesdpcoidsnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_fou_rib_pco_mod INTO
fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
   OPEN lesdpcoids;
   LOOP
   FETCH lesdpcoids INTO tmpdpcoid;
   EXIT WHEN lesdpcoids%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoids;
 else
   OPEN lesdpcoidsnull;
   LOOP
   FETCH lesdpcoidsnull INTO tmpdpcoid;
   EXIT WHEN lesdpcoidsnull%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoidsnull;
 end if;

 chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco_mod;


RETURN cpt;
END;

-- procedures de verifications

FUNCTION selection_valide (abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- meme exercice

-- si PI somme recette = somme depense

-- recette_valides

-- depense_valides

RETURN cpt;

END;

FUNCTION recette_valide (recid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

RETURN cpt;

END;

FUNCTION depense_valide (depid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RETURN cpt;

END;

FUNCTION verif_bordereau_selection(borid INTEGER,abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

-- verifier sum TTC recette selection = sum TTC titre du bord

-- verifier sum TTC depense  = sum TTC mandat du bord

-- verifier sum TTC recette  = sum TTC titre  du bord


RETURN cpt;

END;



-- procedures de locks de transaction
PROCEDURE lock_mandats IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE lock_titres IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE get_depense_jefy_depense (manid INTEGER)
IS

    depid               DEPENSE.dep_id%TYPE;
    jefydepensebudget       jefy_depense.depense_budget%ROWTYPE;
        tmpdepensepapier        jefy_depense.depense_papier%ROWTYPE;
        jefydepenseplanco       jefy_depense.depense_ctrl_planco%ROWTYPE;
    lignebudgetaire         DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
    fouadresse              DEPENSE.dep_adresse%TYPE;
    founom          DEPENSE.dep_fournisseur%TYPE;
    lotordre          DEPENSE.dep_lot%TYPE;
    marordre        DEPENSE.dep_marches%TYPE;
    fouordre        DEPENSE.fou_ordre%TYPE;
    gescode            DEPENSE.ges_code%TYPE;
    modordre        DEPENSE.mod_ordre%TYPE;
    cpt                    INTEGER;
    tcdordre        TYPE_CREDIT.TCD_ORDRE%TYPE;
    tcdcode            TYPE_CREDIT.tcd_code%TYPE;
    ecd_ordre_ema            ECRITURE_DETAIL.ecd_ordre%TYPE;
        orgid   integer;

        CURSOR depenses IS
     SELECT db.* FROM jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
         WHERE dpco.man_id = manid
         and db.dep_id = dpco.dep_id;

BEGIN

    OPEN depenses;
    LOOP
        FETCH depenses INTO jefydepensebudget;
              EXIT WHEN depenses%NOTFOUND;

        -- creation du depid --
        SELECT depense_seq.NEXTVAL INTO depid FROM dual;

            -- creation de lignebudgetaire--
            SELECT org_ub||' '||org_cr||' '||org_souscr
                INTO lignebudgetaire
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            --recuperer le type de credit a partir de la commande
            SELECT tcd_ordre INTO tcdordre
                        FROM jefy_depense.engage_budget
                        WHERE eng_id = jefydepensebudget.eng_id;
                        --AND eng_stat !='A'

            SELECT org_ub,org_id
                INTO gescode,orgid
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouadresse --
            SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
                INTO fouadresse
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            -- founom --
            SELECT adr_nom||' '||adr_prenom
                INTO founom
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouordre --
                 SELECT fou_ordre INTO fouordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id;
                 --AND eng_stat !='A'

            -- lotordre --
            SELECT COUNT(*) INTO cpt
                FROM marches.attribution
                WHERE att_ordre =
                (
                 SELECT att_ordre
                 FROM jefy_depense.engage_ctrl_marche
                                 WHERE eng_id = jefydepensebudget.eng_id
                );

             IF cpt = 0 THEN
                  lotordre :=NULL;
             ELSE
                  SELECT lot_ordre
                  INTO lotordre
                  FROM marches.attribution
                  WHERE att_ordre =
                  (
                                   SELECT att_ordre
                   FROM jefy_depense.engage_ctrl_marche
                                   WHERE eng_id = jefydepensebudget.eng_id
                  );
             END IF;


        -- marordre --
        SELECT COUNT(*) INTO cpt
            FROM marches.lot
            WHERE lot_ordre = lotordre;

        IF cpt = 0 THEN
                  marordre :=NULL;
        ELSE
                        SELECT mar_ordre
                        INTO marordre
                        FROM marches.lot
                        WHERE lot_ordre = lotordre;
        END IF;



                --MOD_ORDRE --
                SELECT mod_ordre INTO modordre
                FROM jefy_depense.depense_papier
                WHERE dpp_id =jefydepensebudget.dpp_id;


        -- recuperer l'ecriture_detail pour emargements semi-auto
        SELECT ecd_ordre INTO ecd_ordre_ema
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;

                -- recup de la depense papier
                SELECT * INTO tmpdepensepapier
                FROM jefy_depense.DEPENSE_PAPIER
                WHERE dpp_id = jefydepensebudget.dpp_id;

                -- recup des infos de depense_ctrl_planco

                SELECT * INTO jefydepenseplanco
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;
        -- creation de la depense --
        INSERT INTO DEPENSE VALUES
            (
            fouadresse ,           --DEP_ADRESSE,
            NULL ,                   --DEP_DATE_COMPTA,
            tmpdepensepapier.dpp_date_reception,  --DEP_DATE_RECEPTION,
            tmpdepensepapier.dpp_date_service_fait , --DEP_DATE_SERVICE,
            'VALIDE' ,               --DEP_ETAT,
            founom ,               --DEP_FOURNISSEUR,
            jefydepenseplanco.dpco_montant_budgetaire , --DEP_HT,
            depense_seq.NEXTVAL ,  --DEP_ID,
            lignebudgetaire ,       --DEP_LIGNE_BUDGETAIRE,
            lotordre ,               --DEP_LOT,
            marordre ,               --DEP_MARCHES,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_MONTANT_DISQUETTE,
            NULL,-- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
            tmpdepensepapier.dpp_numero_facture  ,--DEP_NUMERO,
            jefydepenseplanco.dpco_id ,--DEP_ORDRE,
            NULL ,                   --DEP_REJET,
            tmpdepensepapier.rib_ordre ,--DEP_RIB,
            'NON' ,                   --DEP_SUPPRESSION,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_TTC,
            jefydepenseplanco.dpco_ttc_saisie - jefydepenseplanco.dpco_montant_budgetaire, -- DEP_TVA,
            tmpdepensepapier.exe_ordre ,               --EXE_ORDRE,
            fouordre,                --FOU_ORDRE,
            gescode,                 --GES_CODE,
            manid ,                   --MAN_ID,
            jefydepenseplanco.man_id, --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
            modordre,
            jefydepenseplanco.pco_num ,  --PCO_ORDRE,
            tmpdepensepapier.utl_ordre,               --UTL_ORDRE
            orgid, --org_ordre
            tcdordre,
            ecd_ordre_ema, -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
            tmpdepensepapier.dpp_date_facture);

    END LOOP;
    CLOSE depenses;

END;

PROCEDURE get_recette_jefy_recette (titid INTEGER) IS

recettepapier         jefy_recette.recette_papier%ROWTYPE;
recettebudget         jefy_recette.recette_budget%ROWTYPE;
facturebudget         jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco     jefy_recette. RECETTE_CTRL_PLANCO%rowtype;
recettectrlplancotva  jefy_recette.recette_ctrl_planco_tva%rowtype;
maracujatitre         maracuja.titre%rowtype;

adrnom                varchar2(200);
letyperecette         varchar2(200);
titinterne            varchar2(200);
lbud            varchar2(200);
tboordre        integer;
cpt integer;
BEGIN



--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
SELECT * INTO recettectrlplanco
FROM  jefy_recette.RECETTE_CTRL_PLANCO
WHERE tit_id = titid;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;

select * into maracujatitre
from maracuja.titre
where tit_id = titid;

if (recettebudget.REC_ID_REDUCTION is null) then
 letyperecette := 'R';
else
 letyperecette := 'T';
end if;

select count(*) into cpt
from JEFY_RECETTE.PI_DEP_REC
where rec_id =recettectrlplanco.rec_id;

if cpt > 0 then
 titinterne := 'O';
else
 titinterne := 'N';
end if;

select adr_nom into adrnom from grhum.v_fournis_grhum where fou_ordre = recettepapier.fou_ordre;

select org_ub||'/'||org_cr||'/'||org_souscr into lbud
from jefy_admin.organ
where org_id = facturebudget.org_id;


select distinct tbo_ordre  into tboordre
from maracuja.titre t,maracuja.bordereau b
where b.bor_id = t.bor_id
and t.tit_id = maracujatitre.tit_id;

-- 200 bordereau de presntation interne recette
if tboordre = 200 then
tboordre:=null;
else
tboordre:=facturebudget.org_id;
end if;



    INSERT INTO RECETTE VALUES
        (
        recettectrlplanco.exe_ordre,--EXE_ORDRE,
        maracujatitre.ges_code,--GES_CODE,
        null,--MOD_CODE,
        recettectrlplanco.pco_num,--PCO_NUM,
        recettebudget.REC_DATE_SAISIE,--jefytitre.tit_date,-- REC_DATE,
        adrnom,-- REC_DEBITEUR,
        recette_seq.nextval,-- REC_ID,
        null,-- REC_IMPUTTVA,
        null,-- REC_INTERNE, // TODO ROD
        facturebudget.FAC_LIB,-- REC_LIBELLE,
        lbud,-- REC_LIGNE_BUDGETAIRE,
        'E',-- REC_MONNAIE,
        maracujatitre.TIT_HT,--HT,
        maracujatitre.TIT_TTC,--TTC,
        maracujatitre.TIT_TTC,--DISQUETTE,
        maracujatitre.TIT_TVA,--   REC_MONTTVA,
        facturebudget.FAC_NUMERO,--   REC_NUM,
        recettectrlplanco.rpco_id,--   REC_ORDRE,
        recettepapier.RPP_NB_PIECE,--   REC_PIECE,
        facturebudget.FAC_NUMERO,--   REC_REF,
        'VALIDE',--   REC_STAT,
        'NON',--    REC_SUPPRESSION,  Modif Rod
        letyperecette,--     REC_TYPE,
        NULL,--     REC_VIREMENT,
        titid,--      TIT_ID,
        -titid,--      TIT_ORDRE,
        recettebudget.utl_ordre,--       UTL_ORDRE
        facturebudget.org_id,        --       ORG_ORDRE --ajout rod
        facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
        null, --mod_ordre
        recettepapier.mor_ordre,  --mor_ordre
        recettepapier.rib_ordre,
        NULL
        );


END;


-- procedures du brouillard

PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta     MANDAT.ges_code%TYPE;
   PVICONTREPARTIE_GESTION      PLANCO_VISA.PVI_CONTREPARTIE_GESTION%type;
   MODCONTREPARTIE_GESTION mode_paiement.MOD_CONTREPARTIE_GESTION%type;
       pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt               INTEGER;
       tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
       sens ECRITURE_DETAIL.ecd_sens%TYPE;

BEGIN

   SELECT * INTO lemandat
       FROM MANDAT
       WHERE man_id = manid;

       SELECT DISTINCT tbo_ordre INTO tboordre
       FROM BORDEREAU
       WHERE bor_id IN (SELECT bor_id FROM MANDAT WHERE man_id = manid);



--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
   IF lemandat.prest_id IS NULL THEN
       -- creation du mandat_brouillard visa DEBIT--
               sens:=inverser_sens_orv(tboordre,'D');
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                             --ECD_ORDRE,
           lemandat.exe_ordre,               --EXE_ORDRE,
           lemandat.ges_code,               --GES_CODE,
           ABS(lemandat.man_ht),               --MAB_MONTANT,
           'VISA MANDAT',                   --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
           sens,                           --MAB_SENS,
           manid,                           --MAN_ID,
           lemandat.pco_num               --PCO_NU
           );


       -- credit=ctrepartie
       --debit = ordonnateur
       -- recup des infos du VISA CREDIT --
       SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre = lemandat.exe_ordre;

       IF cpt = 0 THEN
          RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
       END IF;

       SELECT pco_num_ctrepartie, PCO_NUM_TVA,PVI_CONTREPARTIE_GESTION
           INTO pconum_ctrepartie, PCONUM_TVA,PVICONTREPARTIE_GESTION
           FROM PLANCO_VISA
           WHERE pco_num_ordonnateur = lemandat.pco_num
           and exe_ordre = lemandat.exe_ordre;


       SELECT  COUNT(*) INTO cpt
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;

       IF cpt != 0 THEN
           SELECT  pco_num_visa,mod_CONTREPARTIE_GESTION INTO pconum_ctrepartie,MODCONTREPARTIE_GESTION
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;
       END IF;




           -- modif 15/09/2005 compatibilite avec new gestion_exercice
       SELECT c.ges_code,ge.PCO_NUM_185
           INTO gescodecompta,pconum_185
           FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
           WHERE g.ges_code = lemandat.ges_code
           AND g.com_ordre = c.com_ordre
           AND g.ges_code=ge.ges_code
           AND ge.exe_ordre=lemandat.EXE_ORDRE;

               -- 5/12/2007
               -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
               -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
               -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                     --SELECT par_value   INTO parvalue
       --    FROM PARAMETRE
       --    WHERE par_key ='CONTRE PARTIE VISA'
       --    AND exe_ordre = lemandat.exe_ordre;

       parvalue := PVICONTREPARTIE_GESTION;
       if (MODCONTREPARTIE_GESTION is not null) then
           parValue := MODCONTREPARTIE_GESTION;
       end if;
              IF parvalue = 'COMPOSANTE' THEN
          gescodecompta := lemandat.ges_code;
       END IF;

       IF pconum_185 IS NULL THEN
           -- creation du mandat_brouillard visa CREDIT --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;
           INSERT INTO MANDAT_BROUILLARD VALUES (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           gescodecompta,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       ELSE
           --au SACD --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;

           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       END IF;

       IF lemandat.man_tva != 0 THEN
           -- creation du mandat_brouillard visa CREDIT TVA --
                       sens:=inverser_sens_orv(tboordre,'D');
           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
       END IF;

   ELSE
       Bordereau_abricot.set_mandat_brouillard_intern(manid);
   END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   leplancomptable   PLAN_COMPTABLE%ROWTYPE;

   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta MANDAT.ges_code%TYPE;
   pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt INTEGER;
   lepconum plan_comptable_exer.pco_num%type;

BEGIN

    SELECT * INTO lemandat
    FROM MANDAT
    WHERE man_id = manid;


       -- modif 15/09/2005 compatibilite avec new gestion_exercice
     SELECT c.ges_code,ge.PCO_NUM_185
     INTO gescodecompta,pconum_185
     FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
     WHERE g.ges_code = lemandat.ges_code
     AND g.com_ordre = c.com_ordre
     AND g.ges_code=ge.ges_code
     AND ge.exe_ordre=lemandat.EXE_ORDRE;



   -- recup des infos du VISA CREDIT --
   SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   IF cpt = 0 THEN
      RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
   END IF;
   SELECT pco_num_ctrepartie, PCO_NUM_TVA
       INTO pconum_ctrepartie, PCONUM_TVA
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

--   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

--    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--   
   
   lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, lemandat.pco_num);
   
   
   

--    lemandat.pco_num := '18'||lemandat.pco_num;

   -- creation du mandat_brouillard visa DEBIT--
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                             --ECD_ORDRE,
       lemandat.exe_ordre,               --EXE_ORDRE,
       lemandat.ges_code,               --GES_CODE,
       ABS(lemandat.man_ht),               --MAB_MONTANT,
       'VISA MANDAT',                   --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
       'D',                           --MAB_SENS,
       manid,                           --MAN_ID,
       '18'||lemandat.pco_num               --PCO_NU
       );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

--   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

--   END IF;
   
    lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, '181');


   -- planco de CREDIT 181
   -- creation du mandat_brouillard visa CREDIT --
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                            --ECD_ORDRE,
       lemandat.exe_ordre,              --EXE_ORDRE,
       gescodecompta,                  --GES_CODE,
       ABS(lemandat.man_ttc),                --MAB_MONTANT,
       'VISA MANDAT',                  --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
       'C',                          --MAB_SENS,
       manid,                          --MAN_ID,
       '181'                  --PCO_NU
       );



   IF lemandat.man_tva != 0 THEN
       -- creation du mandat_brouillard visa CREDIT TVA --
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           'D',                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
   END IF;

END; 

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--    niv INTEGER;
--BEGIN


--    


--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE
--         (
--         PCO_BUDGETAIRE,
--         PCO_EMARGEMENT,
--         PCO_LIBELLE,
--         PCO_NATURE,
--         PCO_NIVEAU,
--         PCO_NUM,
--         PCO_SENS_EMARGEMENT,
--         PCO_VALIDITE,
--         PCO_J_EXERCICE,
--         PCO_J_FIN_EXERCICE,
--         PCO_J_BE
--         )
--    VALUES
--     (
--     'N',--PCO_BUDGETAIRE,
--     'O',--PCO_EMARGEMENT,
--     libelle,--PCO_LIBELLE,
--     nature,--PCO_NATURE,
--     niv,--PCO_NIVEAU,
--     pconum,--PCO_NUM,
--     2,--PCO_SENS_EMARGEMENT,
--     'VALIDE',--PCO_VALIDITE,
--     'O',--PCO_J_EXERCICE,
--     'N',--PCO_J_FIN_EXERCICE,
--     'N'--PCO_J_BE
--     );
--END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      recid   integer;

      cursor c_recettes is
      select * from jefy_recette.RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN

    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     select rb.REC_ID_REDUCTION into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

-- si dans le cas d une reduction
if (REDUCTION is not null) then
  lesens :='D';
else
  lesens :='C';
end if;


    IF letitre.prest_id IS NULL THEN

      open c_recettes;
      loop
      fetch c_recettes into RECETTEctrlplanco;
      exit when c_recettes%notfound;

    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  RECETTEctrlplanco.pco_num,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;



               -- recette_ctrl_planco_ctp
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;

    ELSE
     Set_Titre_Brouillard_intern(titid);
    END IF;

    -- suppression des lignes d ecritures a ZERO
    delete from TITRE_BROUILLARD where TIB_MONTANT = 0;
END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      lepconum            maracuja.PLAN_COMPTABLE.pco_num%type;
      libelle             maracuja.PLAN_COMPTABLE.pco_libelle%type;
      chap                varchar2(2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;

      cursor c_recettes is
      select * from jefy_recette. RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN



    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

    -- modif fred 04/2007
-- brouillard de PI mauvais des contre partie
-- gescodecompta,
--   -- ges_code,               --GES_CODE,
      SELECT c.ges_code
      INTO gescodecompta
      FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
      WHERE g.ges_code = letitre.ges_code
      AND g.com_ordre = c.com_ordre
      AND g.ges_code=ge.ges_code
      AND ge.exe_ordre=letitre.EXE_ORDRE;


-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     select rb.REC_ID_REDUCTION into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

    -- si dans le cas d une reduction
    if (REDUCTION is not null) then
     lesens :='D';
    else
     lesens :='C';
    end if;

    open c_recettes;
    loop
    fetch c_recettes into RECETTEctrlplanco;
    exit when c_recettes%notfound;



    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

    -- recup des 2 premiers caracteres du compte
    select substr(RECETTEctrlplanco.pco_num,1,2) into chap from dual;

    if chap != '18' then
--     select pco_libelle into libelle
--     from maracuja.plan_comptable
--     where pco_num = RECETTEctrlplanco.pco_num;

--     lepconum := '18'||RECETTEctrlplanco.pco_num;
--     maj_plancomptable_titre('R',libelle,lepconum);
     
        lepconum := api_planco.CREER_PLANCO_PI(RECETTEctrlplanco.exe_ordre, RECETTEctrlplanco.pco_num);
     
     
    end if;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  lepconum,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
 gescodecompta,
               -- ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;



               -- recette_ctrl_planco_ctp on force le 181
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
 gescodecompta,
               -- ges_code,               --GES_CODE,
                  '181',               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;
END;



--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--  niv INTEGER;
--  cpt integer;
--BEGIN

--select count(*) into cpt from PLAN_COMPTABLE
--where pco_num = pconum;

--if cpt = 0 then
--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
--        VALUES
--        (
--        'N',--PCO_BUDGETAIRE,
--        'O',--PCO_EMARGEMENT,
--        libelle,--PCO_LIBELLE,
--        nature,--PCO_NATURE,
--        niv,--PCO_NIVEAU,
--        pconum,--PCO_NUM,
--        2,--PCO_SENS_EMARGEMENT,
--        'VALIDE',--PCO_VALIDITE,
--        'O',--PCO_J_EXERCICE,
--        'N',--PCO_J_FIN_EXERCICE,
--        'N'--PCO_J_BE
--        );
--end if;

--END;

-- outils

FUNCTION inverser_sens_orv (tboordre INTEGER,sens VARCHAR) RETURN VARCHAR IS
cpt INTEGER;

BEGIN

-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
SELECT count(*)  INTO cpt
FROM TYPE_BORDEREAU
WHERE tbo_sous_type ='REVERSEMENTS'
AND tbo_ordre = tboordre;

IF (cpt != 0) THEN
 IF (sens = 'C') THEN
  RETURN 'D';
 ELSE
  RETURN 'C';
 END IF;
END IF;
RETURN sens;
END ;




FUNCTION recup_gescode (abrid INTEGER) RETURN VARCHAR IS
gescode BORDEREAU.GES_CODE%TYPE;
BEGIN

SELECT DISTINCT ges_code INTO gescode FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN gescode;
END;

FUNCTION recup_utlordre (abrid INTEGER) RETURN INTEGER IS
utlordre BORDEREAU.utl_ordre%TYPE;
BEGIN
SELECT DISTINCT utl_ordre INTO utlordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN utlordre;
END;

FUNCTION recup_exeordre (abrid INTEGER) RETURN INTEGER IS
exeordre BORDEREAU.exe_ordre%TYPE;
BEGIN
SELECT DISTINCT exe_ordre INTO exeordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN exeordre;
END;

FUNCTION recup_tboordre (abrid INTEGER) RETURN INTEGER IS
tboordre BORDEREAU.tbo_ordre%TYPE;
BEGIN
SELECT DISTINCT tbo_ordre INTO tboordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN tboordre;
END;

FUNCTION recup_groupby (abrid INTEGER) RETURN VARCHAR IS
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
BEGIN
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN abrgroupby;
END;


function inverser_sens (sens varchar) return varchar is
begin
if sens = 'D' then
return 'C';
else
return 'D';
end if;

end;



PROCEDURE numeroter_bordereau(borid INTEGER) IS
cpt_mandat integer;
cpt_titre integer;
BEGIN

select count(*) into cpt_mandat from mandat
where bor_id = borid;

select count(*) into cpt_titre from titre
where bor_id = borid;

if cpt_mandat + cpt_titre = 0 then
raise_application_error (-20001,'Bordereau  vide');
else
Numerotationobject.numeroter_bordereau(borid);

-- boucle mandat
Numerotationobject.numeroter_mandat(borid);

-- boucle titre
Numerotationobject.numeroter_titre(borid);
end if;

END;




FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
BEGIN

IF orgid IS NULL THEN RETURN NULL; END IF;

SELECT COUNT(*) INTO cpt
FROM accords.CONVENTION_LIMITATIVE
WHERE org_id = orgid AND exe_ordre = exeordre;

IF cpt >0 THEN
 -- recup du type_origine CONVENTION--
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

 SELECT DISTINCT con_ordre INTO convordre
 FROM accords.CONVENTION_LIMITATIVE
 WHERE org_id = orgid
 AND exe_ordre = exeordre;


 SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET)
 INTO orilibelle
 FROM accords.contrat
 WHERE con_ordre = convordre;

ELSE
 SELECT COUNT(*) INTO cpt
 FROM jefy_admin.organ
 WHERE org_id = orgid
 AND org_lucrativite = 1;

 IF cpt = 1 THEN
 -- recup du type_origine OPERATION LUCRATIVE --
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'OPERATION LUCRATIVE';

 --le libelle utilisateur pour le suivie en compta --
 SELECT org_UB||'-'||org_CR||'-'||org_souscr
 INTO orilibelle
 FROM jefy_admin.organ
 WHERE org_id = orgid;

 ELSE
  RETURN NULL;
 END IF;
END IF;

-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ID'
AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
AND ORI_KEY_ENTITE    =orgid;

IF cpt >= 1 THEN
    SELECT ori_ordre INTO cpt FROM ORIGINE
    WHERE ORI_KEY_NAME = 'ORG_ID'
    AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
    AND ORI_KEY_ENTITE = orgid
    AND ROWNUM=1;

ELSE
    SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

    INSERT INTO ORIGINE
    (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
    VALUES ('JEFY_ADMIN','ORG_ID',orilibelle,cpt,orgid,topordre);

END IF;

RETURN cpt;

END;




procedure controle_bordereau(borid integer) is

ttc         maracuja.titre.TIT_TTC%type;
detailttc   maracuja.titre.TIT_TTC%type;
ordottc     maracuja.titre.TIT_TTC%type;
debit       maracuja.titre.TIT_TTC%type;
credit      maracuja.titre.TIT_TTC%type;
cpt         integer;
message     varchar2(50);
messagedetail varchar2(50);
begin

select count(*) into cpt
from maracuja.titre
where bor_id = borid;

if cpt = 0 then
-- somme des maracuja.titre
 select sum(man_ttc) into ttc
 from maracuja.mandat
 where bor_id = borid;

--somme des maracuja.recette
 select sum(d.dep_ttc) into detailttc
 from maracuja.mandat m,maracuja.depense d
 where m.man_id = d.man_id
 and m.bor_id = borid;

-- la somme des credits
 select sum(mab_montant) into credit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'C';

-- la somme des debits
 select sum(mab_montant) into debit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'D';

-- somme des jefy.recette
 select sum(d.dpco_ttc_saisie) into ordottc
 from maracuja.mandat m,jefy_depense.depense_ctrl_planco d
 where m.man_id = d.man_id
 and m.bor_id = borid;

message := ' mandats ';
messagedetail := ' depenses ';

else
-- somme des maracuja.titre
 select sum(tit_ttc) into ttc
 from maracuja.titre
 where bor_id = borid;

--somme des maracuja.recette
 select sum(r.rec_monttva+r.rec_mont) into detailttc
 from maracuja.titre t,maracuja.recette r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

-- la somme des credits
 select sum(tib_montant) into credit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'C';

-- la somme des debits
 select sum(tib_montant) into debit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'D';

-- somme des jefy.recette
 select sum(r.rpco_ttc_saisie) into ordottc
 from maracuja.titre t,jefy_recette.RECETTE_CTRL_PLANCO r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

message := ' titres ';
messagedetail := ' recettes ';

end if;


-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  debit <> credit : '||debit||' '||credit);
end if;

-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  ecriture <> budgetaire : '||debit||' '||ttc);
end if;

-- somme des maracuja.titre = somme des maracuja.recette
if (nvl(ttc,0) != nvl(detailttc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant des '||messagedetail||' :'||ttc||' '||detailttc);
end if;

-- somme des jefy.recette = somme des maracuja.recette
if (nvl(ttc,0) != nvl(ordottc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant ordonnateur des '||messagedetail||' :'||ttc||' '||ordottc);
end if;


bordereau_abricot.ctrl_date_exercice(borid);
end;


procedure  Get_recette_prelevements (titid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;

ORIORDRE INTEGER;
modordre INTEGER;
recid integer;

echeid                  integer;
echeancier_data         jefy_echeancier.ECHEANCIER%ROWTYPE;
echeancier_prelev_data  jefy_echeancier.ECHEANCIER_prelev%ROWTYPE;
facture_data            jefy_recette.facture_budget%rowtype;
personne_data           grhum.v_personne%ROWTYPE;
premieredate            date;
BEGIN

-- verifier s il existe un echancier pour ce titre
select count(*) into cpt
from
jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;


IF (cpt != 1) THEN
      RETURN;
END IF;


-- recup du eche_id / ech_id
select eche_id into echeid
from jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
SELECT * INTO echeancier_data FROM jefy_echeancier.echeancier where ech_id = echeid;
select * into echeancier_prelev_data from jefy_echeancier.echeancier_prelev where ech_id = echeid;
select * into facture_data from jefy_recette.facture_budget where eche_id = echeid;

SELECT * INTO personne_data  FROM GRHUM.V_PERSONNE WHERE pers_id = facture_data.pers_id;

select echd_date_prevue into premieredate
from jefy_echeancier.echeancier_detail
where echd_numero = 1
and ech_id = echeid;

select rec_id into recid from recette where tit_id = titid;


/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
         RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/


-- recup ??
ORIORDRE :=Gestionorigine.traiter_orgid(facture_data.org_id,facture_data.exe_ordre);


INSERT INTO MARACUJA.ECHEANCIER (ECHE_AUTORIS_SIGNEE,
FOU_ORDRE_CLIENT, CON_ORDRE,
ECHE_DATE_1ERE_ECHEANCE, ECHE_DATE_CREATION, ECHE_DATE_MODIF,
ECHE_ECHEANCIER_ORDRE,
ECHE_ETAT_PRELEVEMENT, FT_ORDRE, ECHE_LIBELLE, ECHE_MONTANT, ECHE_MONTANT_EN_LETTRES,
ECHE_NOMBRE_ECHEANCES, ECHE_NUMERO_INDEX, ORG_ORDRE, PREST_ORDRE, ECHE_PRISE_EN_CHARGE,
ECHE_REF_FACTURE_EXTERNE, ECHE_SUPPRIME, EXE_ORDRE, TIT_ID, REC_ID, TIT_ORDRE, ORI_ORDRE,
PERS_ID, ORG_ID, PERS_DESCRIPTION)  VALUES
(
'O'  ,--ECHE_AUTORIS_SIGNEE
facture_data.FOU_ORDRE  ,--FOU_ORDRE_CLIENT
null,--echancier_data.CON_ORDRE  ,--CON_ORDRE
premieredate,--echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
sysdate,--echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
sysdate,--echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echeancier_data.ech_id,--echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
'V',--echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
facture_data.fac_id,--echancier_data.FT_ORDRE  ,--FT_ORDRE
echeancier_data.ech_libelle,--echancier_data.LIBELLE,--ECHE_LIBELLE
echeancier_data.ech_MONTANT  ,--ECHE_MONTANT
echeancier_data.ech_MONTANT_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echeancier_data.ech_NB_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echeancier_data.ech_id,--echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
facture_data.org_id,--echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
null,--echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
'O'  ,--ECHE_PRISE_EN_CHARGE
facture_data.fac_lib,--cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
'N'  ,--ECHE_SUPPRIME
facture_data.exe_ordre  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
-titid,
ORIORDRE,--ORI_ORDRE,
personne_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
facture_data.org_id,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE,
RECO_ORDRE, FOU_ORDRE,
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT,
PREL_PRELEV_DATE_SAISIE,
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT,
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ech_id,--ECHE_ECHEANCIER_ORDRE
null,--PREL_FICP_ORDRE
facture_data.FOU_ORDRE,--FOU_ORDRE
echd_COMMENTAIRE,--PREL_COMMENTAIRE
sysdate,--DATE_MODIF,--PREL_DATE_MODIF
echd_date_prevue,--PREL_DATE_PRELEVEMENT
sysdate,--,--PREL_PRELEV_DATE_SAISIE
'ATTENTE',--PREL_PRELEV_ETAT
echd_numero,--PREL_NUMERO_INDEX
echd_MONTANT,--PREL_PRELEV_MONTANT
echd_id,--PREL_PRELEV_ORDRE
echeancier_prelev_data.RIB_ORDRE_DEBITEUR,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM jefy_echeancier.echeancier_detail
WHERE ech_id = echeancier_data.ECH_id;

END;


PROCEDURE ctrl_date_exercice(borid INTEGER) IS
exeordre integer;
annee integer;
BEGIN

select to_char(bor_date_creation,'YYYY'), exe_ordre into annee, exeordre 
from bordereau
where bor_id = borid 
and exe_ordre >= 2007;

IF exeordre <> annee THEN
update bordereau set bor_date_creation = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update mandat set man_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update titre set tit_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
end if;

END;


END;
/


