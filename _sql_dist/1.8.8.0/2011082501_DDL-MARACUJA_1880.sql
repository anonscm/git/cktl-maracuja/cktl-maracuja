SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.8.0
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- ajout agregats de code gestion
-- modifications compte fi en fonction
-- ajout du plan comptable de reference M9-3 pour 2012
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.8.0',  null, '');
commit;
CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN_bak AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

--  procedure deletePlancoBilanPostes(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--         );
--

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
        );

        -- Calcule le poste pour l'exercice en fonction de la formule trouvÃ©e
--  FUNCTION calcPoste(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      )
--      RETURN NUMBER;
    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2, inverser char) RETURN VARCHAR2;
   

  FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2,
              doCheckSens char
      )
      RETURN NUMBER;

--    FUNCTION prv_getCritSqlForExcept(
--              compte VARCHAR2,
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;
    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 ;

--  FUNCTION getFormule(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type
--      )
--      RETURN varchar2;

   PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE, exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR);
   PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   
    PROCEDURE prv_prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR, isDebug char);
   PROCEDURE prv_prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR, isDebug char);
   
   PROCEDURE checkFormule(formule VARCHAR2);

   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER);
   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE);
   
   function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2;

END API_BILAN_bak;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN_bak AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
  ) IS
    btId bilan_type.bt_id%TYPE;
  BEGIN
    SELECT bt_id INTO btId FROM bilan_type WHERE bt_str_id=btstrid;
    UPDATE bilan_poste SET BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement WHERE bt_id=btId AND exe_ordre=exeOrdre AND bp_str_id=bpStrId;
  END setBilanPosteFormule;


    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2, inverser char) RETURN VARCHAR2 IS
    signe VARCHAR2(1);
    signeNum INTEGER;
    sens VARCHAR2(2);
    compteext VARCHAR2(2000);
    critStrPlus VARCHAR2(2000);
    res VARCHAR2(4000);
    LC$req VARCHAR2(4000);
    lavue VARCHAR2(50);
   BEGIN
        res := '';
        IF (formule IS NOT NULL AND LENGTH(formule)>0) THEN
             dbms_output.put_line('traitement de ='|| formule);
            signe := SUBSTR(formule,1,1); -- +
            signeNum := 1;
            IF (signe='-') THEN
                signeNum := -1;
            END IF;
            sens :=  SUBSTR(formule,2,2); -- SD
            IF (sens IS NULL OR sens NOT IN ('SD','SC')) THEN
                 RAISE_APPLICATION_ERROR (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            END IF;
            compteext := SUBSTR(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            IF (LENGTH(compteext)=0) THEN
                RAISE_APPLICATION_ERROR (-20001, 'compte non recupere dans la formule '|| formule);
            END IF;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';

            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('sacd='||sacd);

            lavue := vue;
            IF lavue IS NULL THEN
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
            END IF;

            if (inverser='O') then
                if (sens = 'SD') then
                    sens := 'SC';
                else
                    sens := 'SD';
                end if;
            end if;


           IF sens = 'SD'   THEN
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           ELSE
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        END IF;
        RETURN res;
   END getSoldeReq;

    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2,
              doCheckSens char
      )
      RETURN NUMBER IS

    str VARCHAR2(4000);
    compteext VARCHAR2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos INTEGER;
    nb INTEGER;
    i INTEGER;
    j INTEGER;
    nextS INTEGER;
    signe VARCHAR2(10);
    sens VARCHAR2(10);
    pconum plan_comptable_exer.pco_num%TYPE;
    subdiv plan_comptable_exer.pco_num%TYPE;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction VARCHAR2(255);
    --critStrPlus varchar2(2000);
    resTmp NUMBER;
    signeNum NUMBER;
    sInstructionReelle VARCHAR2(4000);
    req VARCHAR2(4000);
    solde2 NUMBER;
    req2 VARCHAR2(4000);
    solde NUMBER;
  BEGIN
    /* une formule est composÃ©es de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */
dbms_output.put_line('');
dbms_output.put_line('Formule brute = ' || formule);
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := REPLACE(formule, ' ', '');

    IF (LENGTH(str)=0) THEN
     RAISE_APPLICATION_ERROR (-20001, 'formule vide' );
    END IF;

    str := UPPER(str);

    IF (SUBSTR(str,1,1) = 'S') THEN
        str := '+'||str;
    END IF;

    str := REPLACE(str, '+S', '|+S');
    str := REPLACE(str, '-S', '|-S');
    str := SUBSTR(str,2);
dbms_output.put_line('Formule analysee = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := SUBSTR(sinstruction,1,1); -- +
        signeNum := 1;
        IF (signe='-') THEN
            signeNum := -1;
        END IF;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures', 'N');
        dbms_output.put_line('REQ = '|| req);
        IF ( NVL(faireCalcul, 1)=1) THEN
            EXECUTE IMMEDIATE req INTO solde ;
        END IF;

        IF (solde IS NULL) THEN
            solde := 0;
        END IF;
        IF solde < 0
           THEN solde := 0;  
        END IF;
        IF (SOLDE=0) then
         if (doCheckSens='O') then
             req2 := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures', 'O');
            
                EXECUTE IMMEDIATE req2 INTO solde2 ;
                IF solde2 > 0 then
                    dbms_output.put_line('ATTENTION = le solde est inverse '|| sinstruction);
                end if;
       
           end if;
         end if;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    RETURN resTmp;
    END getSoldeMontant;





    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 IS
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum VARCHAR2(2000);
            critStrMoins VARCHAR2(2000);
            critStrPlus VARCHAR2(2000);
            str VARCHAR2(2000);
            pconumTmp VARCHAR2(2000);
            subdiv VARCHAR2(1000);
    BEGIN
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les Ã©ventuelles parenthese de debut / fin
            IF (SUBSTR(str,1,1) = '(') THEN
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               IF (SUBSTR(str,LENGTH(str),1) <> ')' ) THEN
                RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(str,LENGTH(str),1)) || ': '|| formule);
               END IF;
               str := SUBSTR(str,2,LENGTH(str)-2);
             END IF;

            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                IF (SUBSTR(pconum,1,1)='(' ) THEN
                    IF (SUBSTR(pconum,LENGTH(pconum),1) <> ')' ) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(pconum,LENGTH(pconum),1)) || ': '|| formule);
                    END IF;
                    pconum := SUBSTR(pconum,2,LENGTH(pconum)-2);
                    IF (INSTR(pconum,'(' )>0) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non générée ici :'|| pconum || ': '|| formule);
                    END IF;
                END IF;
                IF (INSTR(pconum,')' )>0) THEN
                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                END IF;
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   IF (j=1) THEN
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' ';
                            dbms_output.put_line(pconumTmp);
                   ELSE
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            IF (LENGTH(subdiv)>0) THEN
                                IF (subdiv NOT LIKE pconumTmp||'%') THEN
                                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autorisÃ©es : '|| pconum);
                                END IF;

                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';
                            END IF;
                   END IF;
                END LOOP;
                IF (LENGTH(critStrMoins)>0) THEN
                    critStrMoins := '(' || critStrMoins ||')';
                    IF (LENGTH(critStrPlus)>0) THEN
                        critStrPlus := critStrPlus || ' or ';
                    END IF;
                    critStrPlus := critStrPlus || critStrMoins;
                END IF;

            END LOOP;

            RETURN critStrPlus;

    END;


 PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
 begin
   prv_prepare_bilan_passif(btId,exeOrdre, gescode , isSacd , 'N');
   
 end;
 

 PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
 begin
    prv_prepare_bilan_actif(btId,exeOrdre, gescode , isSacd , 'N');
 
 end;
 

 PROCEDURE prv_prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR, isDebug char) AS
    bp_id0 bilan_poste.bp_id%TYPE;
    bp_id1 bilan_poste.bp_id%TYPE;
    bp_id2 bilan_poste.bp_id%TYPE;
    rec_bp1 bilan_poste%ROWTYPE;
    rec_bp2 bilan_poste%ROWTYPE;
    rec_bp3 bilan_poste%ROWTYPE;
    bilan_groupe1 VARCHAR2(1000);
    bilan_groupe2 VARCHAR2(1000);
    bilan_libelle VARCHAR2(1000);
    bilan_montant NUMBER;
     bilan_amort NUMBER;
    bpStrId bilan_poste.bp_str_id%TYPE;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%TYPE;
    CURSOR carbre IS
            SELECT *
            FROM bilan_poste
            WHERE exe_ordre=exeOrdre
            CONNECT BY PRIOR BP_ID = bp_id_pere
            START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bp_str_id=bpStrId);

    CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
    CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
    CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
    sqlreq VARCHAR2(4000);
   flag INTEGER;

   BEGIN
        bpStrId := 'actif';

        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        END IF;

        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;

--*************** CREATION TABLE ACTIF *********************************
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                    dbms_output.put_line('rec_bp1.bp_libelle = '|| rec_bp1.bp_id || ' / '|| rec_bp1.bp_libelle );
                    dbms_output.put_line('rec_bp2.bp_libelle = '|| rec_bp2.bp_id || ' / '||rec_bp2.bp_libelle );
                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                            formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq,
                                              isDebug);
                             
                            bilan_amort := 0;
                            IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq,
                                              isDebug);
                            END IF;
                            INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, gescode,exeordre);
                            end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                    formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq,
                                                      isDebug);
                                    bilan_amort := 0;
                                    IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                        bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq,
                                                      isDebug);
                                    END IF;
                                    INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;


    PROCEDURE prv_prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR, isDebug char) AS
        bp_id0 bilan_poste.bp_id%TYPE;
        bp_id1 bilan_poste.bp_id%TYPE;
        bp_id2 bilan_poste.bp_id%TYPE;
        rec_bp1 bilan_poste%ROWTYPE;
        rec_bp2 bilan_poste%ROWTYPE;
        rec_bp3 bilan_poste%ROWTYPE;
        bilan_groupe1 VARCHAR2(1000);
        bilan_groupe2 VARCHAR2(1000);
        bilan_libelle VARCHAR2(1000);
        bilan_montant NUMBER;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
        bpStrId bilan_poste.bp_str_id%TYPE;
        CURSOR carbre IS
                SELECT *
                FROM bilan_poste
                WHERE exe_ordre=exeOrdre
                CONNECT BY PRIOR BP_ID = bp_id_pere
                START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bt_id=btId AND bp_str_id=bpStrId);

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
        CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
        CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
        sqlreq VARCHAR2(4000);
        flag INTEGER;


   BEGIN
        bpStrId := 'passif';
        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;

        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        END IF;
        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId;
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;

                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlreq,
                                              isDebug);


                            INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                      end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq,
                                              isDebug);


                                    INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;

    PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE,exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR) IS
    BEGIN
         prepare_bilan_actif(btId, exeOrdre, gescode, sacd) ;
         prepare_bilan_passif(btId, exeOrdre, gescode, sacd) ;
    END;




    PROCEDURE checkFormule(formule VARCHAR2) IS
        sqlReq VARCHAR2(4000);
        res NUMBER;
    BEGIN
        res := getSoldeMontant(2010, formule, 'AGREGE','G', 0,sqlReq,'N' );

        RETURN;
    END checkFormule;


   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER) IS
        rec_bp bilan_poste%ROWTYPE;
        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeordreOld AND BP_ID_PERE IS NULL;
        bpIdNew bilan_poste.bp_id%TYPE;
        flag INTEGER;

   BEGIN
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordreNew AND BP_ID_PERE IS NULL;
        IF (flag >0) THEN
            RAISE_APPLICATION_ERROR (-20001, 'Bilan deja cree pour   : '|| exeordreNew);
        END IF;


       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;

             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   NULL, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );

            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        END LOOP;
        CLOSE c1;
   END;

   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE) IS
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%ROWTYPE;
        bpIdNew bilan_poste.bp_id%TYPE;

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE bp_id_Pere = racineOld;
   BEGIN




       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;


             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   racineNew, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);


           END LOOP;
       CLOSE c1;

   END prv_duplicateExercice;


    function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2
    is  
        cursor c1 is select distinct pco_num from ecriture_detail where exe_ordre=exeOrdre and pco_num like pcoRacine||'%'; 
        cursor c2 is select bp_formule_montant from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_montant,''))>1 union all select bp_formule_amortissement from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_amortissement,''))>1;
        pcoNum plan_comptable_exer.pco_num%type; 
        spco varchar2(500); 
        formule bilan_poste.BP_FORMULE_MONTANT%type;
        formuleClean bilan_poste.BP_FORMULE_MONTANT%type;
        v_array ZtableForSplit;
        flag smallint;
        res varchar2(4000);
    begin
        -- verifier si les comptes présents dans les écritures sont tous pris en compte par le bilan
        res := '';
    
       OPEN c1;
           LOOP
             FETCH C1 INTO pcoNum;
               EXIT WHEN c1%NOTFOUND;
             flag := 0;
             
             OPEN c2;
               LOOP
                 FETCH C2 INTO formule;
                   EXIT WHEN c2%NOTFOUND;
                   -- on recupere les comptes utilises dans la formule
                  --dbms_output.put_line('formule = '|| formule );
                 formuleClean := REPLACE(formule, 'SD', ' ');
                 formuleClean := REPLACE(formuleClean, 'SC', ' ');
                 formuleClean := REPLACE(formuleClean, '-', ' ');
                 formuleClean := REPLACE(formuleClean, '+', ' ');
                 formuleClean := REPLACE(formuleClean, '(', ' ');
                 formuleClean := REPLACE(formuleClean, ')', ' ');
                 v_array := str2tbl(formuleClean, ' ');
                 FOR i IN 1 .. v_array.COUNT LOOP
                   spco := v_array(i);
                   spco := trim(spco); 
                   if (length(spco)>0) then
                       --dbms_output.put('pcoNum / spco = '||pcoNum ||' / '|| spco );
                       if (pcoNum like spco||'%') then
                        flag := 1;
                        --dbms_output.put_line('ok ' );
                        exit;
                       end if; 
                        --dbms_output.put_line('ko ' );
                   end if;
                 END LOOP;
                 
                 if (flag =0) then
                    dbms_output.put_line(pconum || ' non trouve dans ' || formule );
                 else
                    dbms_output.put_line(pconum || ' TROUVE dans ' || formule );
                    exit;
                 
                 end if;
                 
                 
                 
                 
                  END LOOP;
                 --dbms_output.put_line(' ' );
               CLOSE c2;
               
               
               
               if (flag =0) then
                 
                res := res || pcoNum || ',';
                if (length(res)>=4000) then
                    RAISE_APPLICATION_ERROR (-20001, 'Comptes non pris en compte dans le bilan : '||res);
                end if;
               end if;
               
                END LOOP;
               
          
       CLOSE c1;
        
        return res;
    
    end;


END API_BILAN_bak;
/

------------------
--****************
------------------
CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

--  procedure deletePlancoBilanPostes(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--         );
--

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
        );

        -- Calcule le poste pour l'exercice en fonction de la formule trouvÃ©e
--  FUNCTION calcPoste(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      )
--      RETURN NUMBER;
    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, agregatlib VARCHAR2, vue VARCHAR2, inverser char) RETURN VARCHAR2;
   

  FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              agregatlib varchar2,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2,
              doCheckSens char
      )
      RETURN NUMBER;

--    FUNCTION prv_getCritSqlForExcept(
--              compte VARCHAR2,
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;
    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 ;

--  FUNCTION getFormule(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type
--      )
--      RETURN varchar2;

   PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE, exeOrdre bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2);
   PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2);
   PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2);
   
    PROCEDURE prv_prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2, isDebug char);
   PROCEDURE prv_prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2, isDebug char);
   
   PROCEDURE checkFormule(formule VARCHAR2);

   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER);
   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE);
   
   function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2;

END API_BILAN;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
  ) IS
    btId bilan_type.bt_id%TYPE;
  BEGIN
    SELECT bt_id INTO btId FROM bilan_type WHERE bt_str_id=btstrid;
    UPDATE bilan_poste SET BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement WHERE bt_id=btId AND exe_ordre=exeOrdre AND bp_str_id=bpStrId;
  END setBilanPosteFormule;


    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, agregatlib VARCHAR2, vue VARCHAR2, inverser char) RETURN VARCHAR2 IS
    signe VARCHAR2(1);
    signeNum INTEGER;
    sens VARCHAR2(2);
    compteext VARCHAR2(2000);
    critStrPlus VARCHAR2(2000);
    res VARCHAR2(30000);
    LC$req VARCHAR2(30000);
    lavue VARCHAR2(50);
   BEGIN
        res := '';
        IF (formule IS NOT NULL AND LENGTH(formule)>0) THEN
             dbms_output.put_line('traitement de ='|| formule);
            signe := SUBSTR(formule,1,1); -- +
            signeNum := 1;
            IF (signe='-') THEN
                signeNum := -1;
            END IF;
            sens :=  SUBSTR(formule,2,2); -- SD
            IF (sens IS NULL OR sens NOT IN ('SD','SC')) THEN
                 RAISE_APPLICATION_ERROR (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            END IF;
            compteext := SUBSTR(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            IF (LENGTH(compteext)=0) THEN
                RAISE_APPLICATION_ERROR (-20001, 'compte non recupere dans la formule '|| formule);
            END IF;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';

            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('agregatlib='||agregatlib);

            lavue := vue;
            IF lavue IS NULL THEN
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVT_SOLDE_2';
            END IF;

            if (inverser='O') then
                if (sens = 'SD') then
                    sens := 'SC';
                else
                    sens := 'SD';
                end if;
            end if;


           IF sens = 'SD'   THEN
                LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue || ' d, maracuja.v_agregat_gestion ag WHERE  d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=''' || agregatlib || ''' and ' || critStrPlus  || ' and d.exe_ordre ='||exeordre ;
            
--           
--           
--               IF sacd = 'O' THEN
--                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
--               ELSIF sacd = 'G' THEN
--                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
--               ELSE
--                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
--               END IF;
           ELSE
                LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue || ' d, maracuja.v_agregat_gestion ag WHERE  d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=''' || agregatlib || ''' and ' || critStrPlus  || ' and d.exe_ordre ='||exeordre ;
--                
--               IF sacd = 'O' THEN
--                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
--               ELSIF sacd = 'G' THEN
--                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
--               ELSE
--                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
--               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        END IF;
        RETURN res;
   END getSoldeReq;

    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              agregatlib varchar2,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2,
              doCheckSens char
      )
      RETURN NUMBER IS

    str VARCHAR2(30000);
    compteext VARCHAR2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos INTEGER;
    nb INTEGER;
    i INTEGER;
    j INTEGER;
    nextS INTEGER;
    signe VARCHAR2(10);
    sens VARCHAR2(10);
    pconum plan_comptable_exer.pco_num%TYPE;
    subdiv plan_comptable_exer.pco_num%TYPE;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction VARCHAR2(255);
    --critStrPlus varchar2(2000);
    resTmp NUMBER;
    signeNum NUMBER;
    sInstructionReelle VARCHAR2(30000);
    req VARCHAR2(30000);
    solde2 NUMBER;
    req2 VARCHAR2(30000);
    solde NUMBER;
  BEGIN
    /* une formule est composÃ©es de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */
dbms_output.put_line('');
dbms_output.put_line('Formule brute = ' || formule);
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := REPLACE(formule, ' ', '');

    IF (LENGTH(str)=0) THEN
     RAISE_APPLICATION_ERROR (-20001, 'formule vide' );
    END IF;

    str := UPPER(str);

    IF (SUBSTR(str,1,1) = 'S') THEN
        str := '+'||str;
    END IF;

    str := REPLACE(str, '+S', '|+S');
    str := REPLACE(str, '-S', '|-S');
    str := SUBSTR(str,2);
dbms_output.put_line('Formule analysee = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := SUBSTR(sinstruction,1,1); -- +
        signeNum := 1;
        IF (signe='-') THEN
            signeNum := -1;
        END IF;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, agregatlib,'MARACUJA.cfi_ecritures_2', 'N');
        dbms_output.put_line('REQ = '|| req);
        IF ( NVL(faireCalcul, 1)=1) THEN
            EXECUTE IMMEDIATE req INTO solde ;
        END IF;

        IF (solde IS NULL) THEN
            solde := 0;
        END IF;
        IF solde < 0
           THEN solde := 0;  
        END IF;
        IF (SOLDE=0) then
         if (doCheckSens='O') then
             req2 := getSoldeReq(sinstruction, exeOrdre, agregatlib,'MARACUJA.cfi_ecritures_2', 'O');
            
                EXECUTE IMMEDIATE req2 INTO solde2 ;
                IF solde2 > 0 then
                    dbms_output.put_line('ATTENTION = le solde est inverse '|| sinstruction);
                end if;
       
           end if;
         end if;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    RETURN resTmp;
    END getSoldeMontant;





    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 IS
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum VARCHAR2(2000);
            critStrMoins VARCHAR2(2000);
            critStrPlus VARCHAR2(2000);
            str VARCHAR2(2000);
            pconumTmp VARCHAR2(2000);
            subdiv VARCHAR2(1000);
    BEGIN
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les Ã©ventuelles parenthese de debut / fin
            IF (SUBSTR(str,1,1) = '(') THEN
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               IF (SUBSTR(str,LENGTH(str),1) <> ')' ) THEN
                RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(str,LENGTH(str),1)) || ': '|| formule);
               END IF;
               str := SUBSTR(str,2,LENGTH(str)-2);
             END IF;

            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                IF (SUBSTR(pconum,1,1)='(' ) THEN
                    IF (SUBSTR(pconum,LENGTH(pconum),1) <> ')' ) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(pconum,LENGTH(pconum),1)) || ': '|| formule);
                    END IF;
                    pconum := SUBSTR(pconum,2,LENGTH(pconum)-2);
                    IF (INSTR(pconum,'(' )>0) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non générée ici :'|| pconum || ': '|| formule);
                    END IF;
                END IF;
                IF (INSTR(pconum,')' )>0) THEN
                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                END IF;
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   IF (j=1) THEN
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' ';
                            dbms_output.put_line(pconumTmp);
                   ELSE
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            IF (LENGTH(subdiv)>0) THEN
                                IF (subdiv NOT LIKE pconumTmp||'%') THEN
                                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autorisÃ©es : '|| pconum);
                                END IF;

                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';
                            END IF;
                   END IF;
                END LOOP;
                IF (LENGTH(critStrMoins)>0) THEN
                    critStrMoins := '(' || critStrMoins ||')';
                    IF (LENGTH(critStrPlus)>0) THEN
                        critStrPlus := critStrPlus || ' or ';
                    END IF;
                    critStrPlus := critStrPlus || critStrMoins;
                END IF;

            END LOOP;

            RETURN critStrPlus;

    END;


 PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2) AS
 begin
   prv_prepare_bilan_passif(btId,exeOrdre, agregatlib  , 'N');
   
 end;
 

 PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2) AS
 begin
    prv_prepare_bilan_actif(btId,exeOrdre, agregatlib  , 'N');
 
 end;
 

 PROCEDURE prv_prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2, isDebug char) AS
    bp_id0 bilan_poste.bp_id%TYPE;
    bp_id1 bilan_poste.bp_id%TYPE;
    bp_id2 bilan_poste.bp_id%TYPE;
    rec_bp1 bilan_poste%ROWTYPE;
    rec_bp2 bilan_poste%ROWTYPE;
    rec_bp3 bilan_poste%ROWTYPE;
    bilan_groupe1 VARCHAR2(1000);
    bilan_groupe2 VARCHAR2(1000);
    bilan_libelle VARCHAR2(1000);
    bilan_montant NUMBER;
     bilan_amort NUMBER;
    bpStrId bilan_poste.bp_str_id%TYPE;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%TYPE;
    CURSOR carbre IS
            SELECT *
            FROM bilan_poste
            WHERE exe_ordre=exeOrdre
            CONNECT BY PRIOR BP_ID = bp_id_pere
            START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bp_str_id=bpStrId);

    CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
    CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
    CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
    sqlreq VARCHAR2(30000);
   flag INTEGER;

   BEGIN
        bpStrId := 'actif';

        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (agregatlib IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' agregatlib obligatoire');
        END IF;
       
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        END IF;

        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;

--*************** CREATION TABLE ACTIF *********************************
        DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--        IF issacd = 'O' THEN
--            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--        ELSIF issacd = 'G' THEN
--            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
--        ELSE
--            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                    dbms_output.put_line('rec_bp1.bp_libelle = '|| rec_bp1.bp_id || ' / '|| rec_bp1.bp_libelle );
                    dbms_output.put_line('rec_bp2.bp_libelle = '|| rec_bp2.bp_id || ' / '||rec_bp2.bp_libelle );
                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                            formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              agregatlib,                                             
                                              1,
                                              sqlReq,
                                              isDebug);
                             
                            bilan_amort := 0;
                            IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                              agregatlib,
                                              1,
                                              sqlReq,
                                              isDebug);
                            END IF;
                            INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, agregatlib,exeordre);
                            end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                    formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                                      agregatlib,
                                                      1,
                                                      sqlReq,
                                                      isDebug);
                                    bilan_amort := 0;
                                    IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                        bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                                      agregatlib,
                                                      1,
                                                      sqlReq,
                                                      isDebug);
                                    END IF;
                                    INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, agregatlib,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;


    PROCEDURE prv_prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2,  isDebug char) AS
        bp_id0 bilan_poste.bp_id%TYPE;
        bp_id1 bilan_poste.bp_id%TYPE;
        bp_id2 bilan_poste.bp_id%TYPE;
        rec_bp1 bilan_poste%ROWTYPE;
        rec_bp2 bilan_poste%ROWTYPE;
        rec_bp3 bilan_poste%ROWTYPE;
        bilan_groupe1 VARCHAR2(1000);
        bilan_groupe2 VARCHAR2(1000);
        bilan_libelle VARCHAR2(1000);
        bilan_montant NUMBER;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
        bpStrId bilan_poste.bp_str_id%TYPE;
        CURSOR carbre IS
                SELECT *
                FROM bilan_poste
                WHERE exe_ordre=exeOrdre
                CONNECT BY PRIOR BP_ID = bp_id_pere
                START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bt_id=btId AND bp_str_id=bpStrId);

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
        CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
        CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
        sqlreq VARCHAR2(30000);
        flag INTEGER;


   BEGIN
        bpStrId := 'passif';
        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (agregatlib IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' agregatlib obligatoire');
        END IF;

        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        END IF;
        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId;
        
        DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--        IF issacd = 'O' THEN
--            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
--        ELSIF issacd = 'G' THEN
--            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
--        ELSE
--            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;

                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              agregatlib,
                                              1,
                                              sqlreq,
                                              isDebug);


                            INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, agregatlib,exeordre);
                      end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              agregatlib,
                                              1,
                                              sqlReq,
                                              isDebug);


                                    INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, agregatlib,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;

    PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE,exeOrdre bilan_poste.exe_ordre%TYPE, agregatlib VARCHAR2) IS
    BEGIN
         prepare_bilan_actif(btId, exeOrdre, agregatlib) ;
         prepare_bilan_passif(btId, exeOrdre, agregatlib) ;
    END;




    PROCEDURE checkFormule(formule VARCHAR2) IS
        sqlReq VARCHAR2(30000);
        res NUMBER;
    BEGIN
        res := getSoldeMontant(2010, formule, 'AGREGE', 0,sqlReq,'N' );

        RETURN;
    END checkFormule;


   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER) IS
        rec_bp bilan_poste%ROWTYPE;
        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeordreOld AND BP_ID_PERE IS NULL;
        bpIdNew bilan_poste.bp_id%TYPE;
        flag INTEGER;

   BEGIN
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordreNew AND BP_ID_PERE IS NULL;
        IF (flag >0) THEN
            RAISE_APPLICATION_ERROR (-20001, 'Bilan deja cree pour   : '|| exeordreNew);
        END IF;


       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;

             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   NULL, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );

            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        END LOOP;
        CLOSE c1;
   END;

   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE) IS
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%ROWTYPE;
        bpIdNew bilan_poste.bp_id%TYPE;

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE bp_id_Pere = racineOld;
   BEGIN




       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;


             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   racineNew, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);


           END LOOP;
       CLOSE c1;

   END prv_duplicateExercice;


    function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2
    is  
        cursor c1 is select distinct pco_num from ecriture_detail where exe_ordre=exeOrdre and pco_num like pcoRacine||'%'; 
        cursor c2 is select bp_formule_montant from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_montant,''))>1 union all select bp_formule_amortissement from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_amortissement,''))>1;
        pcoNum plan_comptable_exer.pco_num%type; 
        spco varchar2(500); 
        formule bilan_poste.BP_FORMULE_MONTANT%type;
        formuleClean bilan_poste.BP_FORMULE_MONTANT%type;
        v_array ZtableForSplit;
        flag smallint;
        res varchar2(30000);
    begin
        -- verifier si les comptes présents dans les écritures sont tous pris en compte par le bilan
        res := '';
    
       OPEN c1;
           LOOP
             FETCH C1 INTO pcoNum;
               EXIT WHEN c1%NOTFOUND;
             flag := 0;
             
             OPEN c2;
               LOOP
                 FETCH C2 INTO formule;
                   EXIT WHEN c2%NOTFOUND;
                   -- on recupere les comptes utilises dans la formule
                  --dbms_output.put_line('formule = '|| formule );
                 formuleClean := REPLACE(formule, 'SD', ' ');
                 formuleClean := REPLACE(formuleClean, 'SC', ' ');
                 formuleClean := REPLACE(formuleClean, '-', ' ');
                 formuleClean := REPLACE(formuleClean, '+', ' ');
                 formuleClean := REPLACE(formuleClean, '(', ' ');
                 formuleClean := REPLACE(formuleClean, ')', ' ');
                 v_array := str2tbl(formuleClean, ' ');
                 FOR i IN 1 .. v_array.COUNT LOOP
                   spco := v_array(i);
                   spco := trim(spco); 
                   if (length(spco)>0) then
                       --dbms_output.put('pcoNum / spco = '||pcoNum ||' / '|| spco );
                       if (pcoNum like spco||'%') then
                        flag := 1;
                        --dbms_output.put_line('ok ' );
                        exit;
                       end if; 
                        --dbms_output.put_line('ko ' );
                   end if;
                 END LOOP;
                 
                 if (flag =0) then
                    dbms_output.put_line(pconum || ' non trouve dans ' || formule );
                 else
                    dbms_output.put_line(pconum || ' TROUVE dans ' || formule );
                    exit;
                 
                 end if;
                 
                 
                 
                 
                  END LOOP;
                 --dbms_output.put_line(' ' );
               CLOSE c2;
               
               
               
               if (flag =0) then
                 
                res := res || pcoNum || ',';
                if (length(res)>=4000) then
                    RAISE_APPLICATION_ERROR (-20001, 'Comptes non pris en compte dans le bilan : '||res);
                end if;
               end if;
               
                END LOOP;
               
          
       CLOSE c1;
        
        return res;
    
    end;


END API_BILAN;
/




------------------
--****************
------------------





ALTER TABLE MARACUJA.PARAMETRE MODIFY(PAR_KEY VARCHAR2(2000));
/
ALTER TABLE MARACUJA.GESTION_EXERCICE ADD (PCO_NUM_185_CTP_SACD  VARCHAR2(20));

COMMENT ON COLUMN MARACUJA.GESTION_EXERCICE.PCO_NUM_185 IS 'Dans le cas ou un code gestion represente un SACD, reference au PLAN_COMPTABLE pour indiquer le compte a utiliser sur l''agence (par exemple 18512)';
COMMENT ON COLUMN MARACUJA.GESTION_EXERCICE.PCO_NUM_185_CTP_SACD IS 'Si on est sur un SACD, indique quel est le compte à utiliser pour les écritures de liaison passées sur le SACD (par exemple 185 si on veut globaliser, sinon le même compte que pco_num_185)';




CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- Vérifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185, PCO_NUM_185_CTP_SACD
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    

    --  -------------------------------------------------------
    -- Récupération des plan comptables
    INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
SELECT 
MARACUJA.PLAN_COMPTABLE_EXER_SEQ.NEXTVAL, nouvelExercice, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
FROM MARACUJA.PLAN_COMPTABLE_EXER where exe_ordre=precedentExercice;
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM, PCA_DUREE) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM, PCA_DUREE
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- Récupération des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
 

    -- --------------------------------------------------------
    -- codes budgets pour les epn
    insert into epn.code_budget_sacd(sacd, cod_bud, exe_ordre) 
        select sacd, cod_bud, nouvelExercice from epn.code_budget_sacd where exe_ordre=precedentExercice; 


    -- formules pour le calcul du bilan 
    MARACUJA.API_BILAN.DUPLICATEEXERCICE ( precedentExercice, nouvelExercice ); 
   
END;
/


GRANT EXECUTE ON MARACUJA.PREPARE_EXERCICE TO JEFY_ADMIN;

CREATE OR REPLACE PACKAGE BODY MARACUJA.Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007

PROCEDURE apres_visa_bordereau (borid INTEGER)
IS
BEGIN
    prv_apres_visa_bordereau(borid);
END;



PROCEDURE prv_apres_visa_bordereau(borid INTEGER)
IS
  leBordereau BORDEREAU%ROWTYPE;
  leSousType TYPE_BORDEREAU.TBO_SOUS_TYPE%TYPE;
  autoViserBordRejet MARACUJA.PARAMETRE.PAR_VALUE%TYPE;
  brjordre maracuja.bordereau_rejet.brj_ordre%type;
  flag integer;
  
BEGIN
     SELECT * INTO leBordereau FROM BORDEREAU WHERE bor_id=borid;
     SELECT TBO_SOUS_TYPE INTO leSousType FROM TYPE_BORDEREAU WHERE tbo_ordre = leBordereau.tbo_ordre;
     IF leBordereau.bor_etat = 'VISE' THEN
     
        -- s il y a un bordereau de rejet, si param = auto viser, viser le bordereau de rejet
        select count(*) into flag from maracuja.mandat where bor_id=leBordereau.bor_id and brj_ordre is not null;
        if (flag>0) then
            autoViserBordRejet := jefy_admin.api_parametre.GET_PARAM_MARACUJA('AUTO_VISER_BORD_REJET_DEP',leBordereau.exe_ordre,'NON' );
            if (autoViserBordRejet = 'OUI') then
                select min(brj_ordre) into brjordre from maracuja.mandat where bor_id=leBordereau.bor_id and brj_ordre is not null;
                bordereau_abricot.viser_bordereau_rejet(brjOrdre);
            end if;
        
        end if;    
            
     
         IF (leSousType = 'REVERSEMENTS') THEN
             prv_apres_visa_reversement(borid);
         END IF;
          emarger_visa_bord_prelevement(borid);
    END IF;

END;


-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
PROCEDURE prv_apres_visa_reversement(borid INTEGER)
IS
  manid MANDAT.man_id%TYPE;

  CURSOR c_mandatVises IS
           SELECT man_id FROM MANDAT WHERE bor_id=borid AND man_etat='VISE';

BEGIN
     OPEN c_mandatVises;
        LOOP
            FETCH c_mandatVises INTO manid;
                  EXIT WHEN c_mandatVises%NOTFOUND;
            jefy_depense.apres_visa.viser_reversement(manid);
        END LOOP;
        CLOSE c_mandatVises;
END;



PROCEDURE apres_reimputation (reiordre INTEGER)
IS
    ex INTEGER;
    manid mandat.man_id%type;
    titid titre.tit_id%type;
    pconumnouveau plan_comptable.pco_num%type;
    pconumancien plan_comptable.pco_num%type;
BEGIN

        SELECT man_id, tit_id, pco_num_nouveau, pco_num_ancien INTO manid, titid, pconumnouveau, pconumancien FROM REIMPUTATION WHERE rei_ordre=reiordre;
        IF manid IS NOT NULL THEN
            jefy_depense.reimputer.reimputation_maracuja(manid, pconumnouveau); 
            --update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;        
        end if;          
          
        IF titid IS NOT NULL THEN
            update jefy_recette.recette_ctrl_planco set pco_num=pconumnouveau where tit_id=titid and pco_num=pconumancien;        
        end if;          
          

END;


PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
             --SELECT 1 INTO  cpt FROM dual;
         emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
         emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
     -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
             SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
         emarger_prelevement(recoordre);
END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS
begin
    if (recoordre is null) then
        RAISE_APPLICATION_ERROR (-20001,'Le parametre recoordre est null.');
    end if;
    emarger_prelev_ac_titre(recoordre);
    emarger_prelev_ac_ecr(recoordre);
end;



-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER) is

    CURSOR titres (lerecoordre INTEGER ) IS
     SELECT * FROM TITRE
     WHERE tit_id IN
     (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
     WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
     AND p.reco_ordre = lerecoordre)
     ;

    CURSOR non_emarge_credit (letitid INTEGER) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';

    letitre maracuja.TITRE%ROWTYPE;
    ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
    ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
    EMAORDRE EMARGEMENT.ema_ordre%TYPE;
    lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
    cpt INTEGER;

BEGIN

        -- recup infos
         SELECT * INTO lerecouvrement FROM RECOUVREMENT
         WHERE reco_ordre = recoordre;

        -- creation de l emargement !
         SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

         INSERT INTO EMARGEMENT
          (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
         VALUES
          (
          SYSDATE,
          -1,
          EMAORDRE,
          lerecouvrement.exe_ordre,
          3,
          lerecouvrement.UTL_ORDRE,
          lerecouvrement.COM_ORDRE,
          0,
          'VALIDE'
          );

        -- on fetch les titres du recrouvement
        OPEN titres(recoordre);
        LOOP
        FETCH titres INTO letitre;
        EXIT WHEN titres%NOTFOUND;
        -- on recupere les ecritures non emargees debits
         OPEN non_emarge_credit (letitre.tit_id);
         LOOP
         FETCH non_emarge_credit INTO ecriture_credit;
         EXIT WHEN non_emarge_credit%NOTFOUND;

        -- on recupere les ecritures non emargees credit
         OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
         LOOP
         FETCH non_emarge_debit_compte INTO ecriture_debit;
         EXIT WHEN non_emarge_debit_compte%NOTFOUND;

         IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
         THEN
          -- creation de l emargement detail !
          INSERT INTO EMARGEMENT_DETAIL
          (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
          VALUES
          (
          ecriture_credit.ecd_ordre,
          ecriture_debit.ecd_ordre,
          EMAORDRE,
          ecriture_credit.ecd_reste_emarger,
          emargement_detail_seq.NEXTVAL,
          ecriture_debit.exe_ordre
          );

          -- maj de l ecriture debit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_debit.ecd_ordre;

          -- maj de lecriture credit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_credit.ecd_ordre;
         END IF;

         END LOOP;
         CLOSE non_emarge_debit_compte;

        END LOOP;
        CLOSE non_emarge_credit;

        END LOOP;
        CLOSE titres;
        -- suppression de lemagenet si pas de details;
        SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
        WHERE ema_ordre = EMAORDRE;

        IF cpt = 0 THEN
            DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
        else
            Numerotationobject.numeroter_emargement(EMAORDRE);
        END IF;



END;




-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER)
is
  CURSOR lesPrelevements (lerecoordre INTEGER ) IS
     SELECT * FROM maracuja.prelevement
     WHERE reco_ordre=lerecoordre;
     
  ecrDetailCredit ecriture_detail%rowtype;       
  ecrDetailDebit ecriture_detail%rowtype;
    ecriture_credit ecriture_detail%rowtype;    
    ecriture_debit ecriture_detail%rowtype;         
  lePrelevement maracuja.prelevement%rowtype;
  flag integer;
  flag2 integer;
  retVal integer;
  utlOrdre integer;
  
    CURSOR non_emarge_credit (prelevOrdre INTEGER) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (prelevOrdre INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';  
    
begin

        select utl_ordre into utlOrdre from recouvrement where reco_ordre=recoordre;


        OPEN lesPrelevements(recoordre);
        LOOP
            FETCH lesPrelevements INTO lePrelevement;
            EXIT WHEN lesPrelevements%NOTFOUND;
            ecrDetailCredit := null;
            ecrDetailDebit := null;
            
            
            -- recuperation de l'ecriture de l'echeancier non emargee en debit
            SELECT count(*) into flag2 FROM maracuja.Ecriture_detail 
             where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
             select ecd_ordre from maracuja.echeancier_detail_ecr  
             WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
            if (flag2=1) then
                SELECT * into ecrDetailDebit FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
                 select ecd_ordre from maracuja.echeancier_detail_ecr  
                 WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
             
             
                 -- s'il y a une ecriture non emargee en debit sur l'echeancier, 
                 -- on emarge avec le credit correspondant du prelevement
                 -- recuperation de l'ecriture du prelevement non emargee en credit
             
                SELECT count(*) into flag FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                 select ecd_ordre from maracuja.prelevement_detail_ecr  
                 WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                if (flag=1) then
                     SELECT * into ecrDetailCredit FROM maracuja.Ecriture_detail 
                     where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                     select ecd_ordre from maracuja.prelevement_detail_ecr  
                     WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                    -- si les deux ecritures ont ete recuperees, on les emarge  
                    IF (Afaireaprestraitement.verifier_emar_exercice(ecrDetailCredit.ecr_ordre,ecrDetailDebit.ecr_ordre) = 1) then          
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecrDetailCredit.ecd_ordre, ecrDetailDebit.ecd_ordre, 3, UTLORDRE );
                    end if;             
                end if;

            end if;                        
            
            
            -- emargement entre les debits et credits associes au prelevement 
            -- (suite a saisie releve)
             
            -- on recupere les ecritures non emargees debits
             OPEN non_emarge_credit (lePrelevement.prel_prelev_ordre );
             LOOP
             FETCH non_emarge_credit INTO ecriture_credit;
             EXIT WHEN non_emarge_credit%NOTFOUND;

                -- on recupere les ecritures non emargees credit
                 OPEN non_emarge_debit_compte (lePrelevement.prel_prelev_ordre,ecriture_credit.pco_num);
                 LOOP
                 FETCH non_emarge_debit_compte INTO ecriture_debit;
                 EXIT WHEN non_emarge_debit_compte%NOTFOUND;
                    
                     IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
                     THEN
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, UTLORDRE );         
                     END IF;

                 END LOOP;
                 CLOSE non_emarge_debit_compte;

            END LOOP;
            CLOSE non_emarge_credit;            
            
            
            
            

            
   
            
         END LOOP;
        CLOSE lesPrelevements;

end;





PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_code = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
    DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
else
    Numerotationobject.numeroter_emargement(EMAORDRE);
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/









CREATE TABLE MARACUJA.GESTION_AGREGAT
(
  GA_ID           NUMBER(38)                    NOT NULL,
  EXE_ORDRE           NUMBER(38)                NOT NULL,
  GA_LIBELLE      VARCHAR2(200)                 NOT NULL,
  GA_DESCRIPTION  VARCHAR2(1000)
);

ALTER TABLE MARACUJA.GESTION_AGREGAT ADD (CONSTRAINT PK_GESTION_AGREGAT PRIMARY KEY (GA_ID));
alter table maracuja.GESTION_AGREGAT add constraint UNQ_GESTION_AGREGAT unique (EXE_ORDRE, GA_LIBELLE);
alter table maracuja.GESTION_AGREGAT add constraint CHK_GESTION_AGREGAT CHECK (ga_libelle <>'G' and ga_libelle<>'O' and ga_libelle<>'N' and ga_libelle<>'AGREGE' and ga_libelle<>'ETAB');

COMMENT ON TABLE MARACUJA.GESTION_AGREGAT IS 'Les agrégations de codes gestion';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT.GA_ID IS 'Cle de la table';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT.GA_LIBELLE IS 'Libelle de l''agregation';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT.GA_DESCRIPTION IS 'Description optionnelle de l''agregation';


CREATE TABLE MARACUJA.GESTION_AGREGAT_REPART
(
  GAR_ID           	NUMBER(38)                    NOT NULL,
  GA_ID           	NUMBER(38)                    NOT NULL,
  EXE_ORDRE			NUMBER(38)					  not null,
  GES_CODE  		VARCHAR2(10)				  NOT NULL
);


ALTER TABLE MARACUJA.GESTION_AGREGAT_REPART ADD (CONSTRAINT PK_GESTION_AGREGAT_REPART PRIMARY KEY (GAR_ID));
alter table maracuja.GESTION_AGREGAT_REPART add constraint FK_gar_ges_code foreign key (exe_ordre, ges_code) references MARACUJA.gestion_exercice  deferrable initially deferred;
alter table maracuja.GESTION_AGREGAT_REPART add constraint FK_Gar_GA_ID foreign key (GA_ID) references MARACUJA.gestion_AGREGAT deferrable initially deferred ;
alter table maracuja.GESTION_AGREGAT_REPART add constraint UNQ_GESTION_AGREGAT_REPART unique (GA_ID, EXE_ORDRE, GES_CODE);

COMMENT ON TABLE MARACUJA.GESTION_AGREGAT_REPART IS 'Repartition entre les agrégations et les codes gestions';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT_REPART.GAR_ID IS 'Clé';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT_REPART.GA_ID IS 'Référence à l''agrégation';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT_REPART.EXE_ORDRE IS 'Référence à l''exercice';
COMMENT ON COLUMN MARACUJA.GESTION_AGREGAT_REPART.GES_CODE IS 'Référence au code gestion';

create sequence maracuja.GESTION_AGREGAT_seq NOCYCLE  NOCACHE  NOORDER;
create sequence maracuja.GESTION_AGREGAT_REPART_seq NOCYCLE  NOCACHE  NOORDER;

CREATE OR REPLACE FORCE VIEW MARACUJA.v_agregat_gestion
(EXE_ORDRE, AGREGAT, GES_CODE)
	AS 
	select ga.exe_ordre, ga.GA_LIBELLE as agregat , ges_code 
	from
	gestion_agregat ga, gestion_agregat_repart gar
	where ga.GA_ID=gar.ga_id
	union all
	select exe_ordre, 'AGREGE' as agregat, ges_code
	from gestion_exercice
	union all
	select exe_ordre, 'ETAB', ges_code
	from gestion_exercice where pco_num_185 is null
	union all
	select exe_ordre, 'SACD_'|| ges_code, ges_code 
	from gestion_exercice where pco_num_185 is not null;
/

grant select on maracuja.v_agregat_gestion to comptefi with grant option;


CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
--
--
    --nouvelExercice  EXERICE.exe_exercice%TYPE;
    precedentExercice EXERCICE.exe_exercice%TYPE;
    flag NUMBER;

    rec_agregat gestion_agregat%rowtype;
    gaid gestion_agregat.ga_id%type;

    CURSOR cAgregats is
        SELECT G.GA_ID, G.EXE_ORDRE, G.GA_LIBELLE, G.GA_DESCRIPTION FROM MARACUJA.GESTION_AGREGAT G where g.exe_ordre=precedentExercice;


BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- Vérifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185, PCO_NUM_185_CTP_SACD
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    

    --  -------------------------------------------------------
    -- Récupération des plan comptables
    INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
SELECT 
MARACUJA.PLAN_COMPTABLE_EXER_SEQ.NEXTVAL, nouvelExercice, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
FROM MARACUJA.PLAN_COMPTABLE_EXER where exe_ordre=precedentExercice;
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM, PCA_DUREE) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM, PCA_DUREE
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- Récupération des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
 

    -- --------------------------------------------------------
    -- codes budgets pour les epn
    insert into epn.code_budget_sacd(sacd, cod_bud, exe_ordre) 
        select sacd, cod_bud, nouvelExercice from epn.code_budget_sacd where exe_ordre=precedentExercice; 


    -- formules pour le calcul du bilan 
    MARACUJA.API_BILAN.DUPLICATEEXERCICE ( precedentExercice, nouvelExercice ); 
   
    ------
    OPEN cAgregats;
      LOOP
         FETCH cAgregats INTO rec_agregat;
               EXIT WHEN cAgregats%NOTFOUND;
         SELECT GESTION_AGREGAT_SEQ.NEXTVAL into gaid from dual;
         INSERT INTO MARACUJA.GESTION_AGREGAT ( GA_ID, EXE_ORDRE, GA_LIBELLE, GA_DESCRIPTION) 
            VALUES (
                gaid, --GA_ID, 
                nouvelExercice, --EXE_ORDRE, 
                rec_agregat.ga_libelle,--GA_LIBELLE, 
                rec_agregat.ga_description --GA_DESCRIPTION
                );
         INSERT INTO MARACUJA.GESTION_AGREGAT_REPART (GAR_ID, GA_ID, EXE_ORDRE, GES_CODE) 
            select MARACUJA.GESTION_AGREGAT_REPART_SEQ.NEXTVAL, --GAR_ID, 
                gaid, --GA_ID, 
                nouvelExercice, --EXE_ORDRE, 
                ges_code --GES_CODE
                from MARACUJA.GESTION_AGREGAT_REPART where ga_id=rec_agregat.ga_id;
               
      
      end loop;
    close cAgregats;  
END;
/


GRANT EXECUTE ON MARACUJA.PREPARE_EXERCICE TO JEFY_ADMIN;


CREATE OR REPLACE FORCE VIEW maracuja.cfi_totaux_6_7_avt_solde_2 (pco_num,
                                                                  exe_ordre,
                                                                  ges_code,
                                                                  credit,
                                                                  debit
                                                                 )
AS
   SELECT   ed.pco_num, e.exe_ordre, ed.ges_code, SUM (ecd_credit) credit,
            SUM (ecd_debit) debit
       FROM ecriture_detail ed, ecriture e, gestion g
      WHERE ed.ecr_ordre = e.ecr_ordre
        AND SUBSTR (ecr_etat, 1, 1) = 'V'
        AND ecr_numero > 0
        AND g.ges_code = ed.ges_code
        AND pco_num LIKE '7%'
        AND e.top_ordre <> 12
   GROUP BY ed.pco_num, e.exe_ordre, ed.ges_code
   UNION ALL
   SELECT   ed.pco_num, e.exe_ordre, ed.ges_code, SUM (ecd_credit) credit,
            SUM (ecd_debit) debit
       FROM ecriture_detail ed, ecriture e, gestion g
      WHERE ed.ecr_ordre = e.ecr_ordre
        AND SUBSTR (ecr_etat, 1, 1) = 'V'
        AND ecr_numero > 0
        AND g.ges_code = ed.ges_code
        AND pco_num LIKE '6%'
        AND e.top_ordre <> 12
   GROUP BY ed.pco_num, e.exe_ordre, ed.ges_code
   UNION ALL
   SELECT   ed.pco_num, e.exe_ordre, ed.ges_code, SUM (ecd_credit) credit,
            SUM (ecd_debit) debit
       FROM ecriture_detail ed, ecriture e, gestion g
      WHERE ed.ecr_ordre = e.ecr_ordre
        AND SUBSTR (ecr_etat, 1, 1) = 'V'
        AND ecr_numero > 0
        AND g.ges_code = ed.ges_code
        AND pco_num LIKE '187%'
        AND e.top_ordre <> 12
   GROUP BY ed.pco_num, e.exe_ordre, ed.ges_code
   UNION ALL
   SELECT   ed.pco_num, e.exe_ordre, ed.ges_code, SUM (ecd_credit) credit,
            SUM (ecd_debit) debit
       FROM ecriture_detail ed, ecriture e, gestion g
      WHERE ed.ecr_ordre = e.ecr_ordre
        AND SUBSTR (ecr_etat, 1, 1) = 'V'
        AND ecr_numero > 0
        AND g.ges_code = ed.ges_code
        AND pco_num LIKE '186%'
        AND e.top_ordre <> 12
   GROUP BY ed.pco_num, e.exe_ordre, ed.ges_code
/
grant select on  maracuja.cfi_totaux_6_7_avt_solde_2 to comptefi with grant option;


CREATE OR REPLACE FORCE VIEW maracuja.cfi_ecritures_2 (exe_ordre,
                                                     ges_code,
                                                     pco_num,
                                                     pco_libelle,
                                                     credit,
                                                     debit
                                                    )
AS
   SELECT   e.exe_ordre, ed.ges_code, ed.pco_num,
            api_planco.get_pco_libelle (ed.pco_num,
                                        e.exe_ordre
                                       ) AS pco_libelle,
            SUM (ed.ecd_credit) credit, 0
       FROM ecriture_detail ed,
            ecriture e
      WHERE ed.ecd_credit <> 0
        AND ed.ecr_ordre = e.ecr_ordre
   GROUP BY e.exe_ordre, ed.ges_code, ed.pco_num
   UNION ALL
   SELECT   e.exe_ordre, ed.ges_code, ed.pco_num,
            api_planco.get_pco_libelle (ed.pco_num,
                                        e.exe_ordre
                                       ) AS pco_libelle,
            0, SUM (ed.ecd_debit) debit
       FROM ecriture_detail ed,
            ecriture e
      WHERE ed.ecd_debit <> 0
        AND ed.ecr_ordre = e.ecr_ordre
   GROUP BY e.exe_ordre, ed.ges_code, ed.pco_num;
/

GRANT SELECT ON MARACUJA.CFI_ECRITURES_2 TO COMPTEFI;




CREATE TABLE MARACUJA.PLAN_COMPTABLE_REF_EXT
(
  REF_PCO_ID                NUMBER(38),
  REF_PCO_IMPORT_ID         VARCHAR2(50 BYTE)   NOT NULL,
  REF_PCO_NUM               VARCHAR2(30 BYTE)   NOT NULL,
  REF_PCO_LIBELLE           VARCHAR2(500 BYTE)  NOT NULL,
  REF_PCO_REMARQUES         VARCHAR2(2000 BYTE),
  REF_PCO_NUM_OLD           VARCHAR2(30 BYTE),
  REF_PCO_BUDGETAIRE        VARCHAR2(1 BYTE),
  REF_PCO_UTILISATION       VARCHAR2(200 BYTE),
  REF_PCO_SUBDIVISABLE      VARCHAR2(1 BYTE),
  REF_PCO_DATE_DEBUT        DATE                NOT NULL,
  REF_PCO_DATE_FIN        DATE                NULL,
  REF_PCO_NOMENCLATURE_REF  VARCHAR2(200 BYTE)  NOT NULL
)
TABLESPACE GFC;


CREATE UNIQUE INDEX MARACUJA.PK_PLAN_COMPTABLE_REF_EXT ON MARACUJA.PLAN_COMPTABLE_REF_EXT(REF_PCO_ID) TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.PLAN_COMPTABLE_REF_EXT ADD ( CONSTRAINT PK_PLAN_COMPTABLE_REF_EXT PRIMARY KEY (REF_PCO_ID) USING INDEX );
ALTER TABLE MARACUJA.PLAN_COMPTABLE_REF_EXT ADD ( CONSTRAINT UK_PLAN_COMPTABLE_REF_EXT_2 UNIQUE (REF_PCO_NUM,REF_PCO_DATE_DEBUT,REF_PCO_NOMENCLATURE_REF) );




create sequence maracuja.PLAN_COMPTABLE_REF_ext_seq NOCYCLE  NOCACHE  NOORDER;










CREATE OR REPLACE PACKAGE MARACUJA."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco2(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    procedure checkCadre2VsBalance(exeOrdre INTEGER);
    procedure checkEcritureDetailGestion(exeordre integer); 
    procedure checkMandatVsJefyDepense(exeordre integer); 
    
    procedure checkAllProblemes(exeOrdre integer);
    
    
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and 
    (
    (man_ETAT   not in ('VISE','PAYE', 'ANNULE') and bor_etat  in ('VISE', 'PAYE', 'PAIEMENT'))
    or
    (man_ETAT   not in ('ANNULE') and brj_ordre is not null)
    )
    ;
    
    
    cursor c2 is
        select m.* from mandat m, mandat_detail_ecriture mde, ecriture_detail ecd, mode_paiement mp  
            where m.man_id=mde.man_id and m.exe_ordre=2011 and mde_origine='VISA' 
            and m.mod_ordre=mp.mod_ordre and m.pai_ordre is null
            and mp.MOD_DOM='VIREMENT'
            and mde.ecd_ordre=ecd.ecd_ordre
            and abs(ecd_reste_emarger)<>abs(ecd_montant)
            and man_ht>0;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau ('|| borEtat ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') ou bien mandat sur un bordereau de rejet et non ANNULE';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de données : remettre la bonne valeur dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
  
   OPEN C2;
  LOOP
    FETCH c2 INTO objet;
    EXIT
  WHEN c2%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'La contrepartie de prise en charge du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') a été émargée alors que le mode de paiement est de type VIREMENT.';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Provoque un message d''erreur lors de la création d''un paiement (écritures déséquilibrées)', -- ZAP_CONSEQUENCE
        'Vous devez supprimer l''émargement puis modifier le mode de paiement du mandat si le mandat a réellement été payé autrement que par virement via Maracuja.'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c2;
  
  
  
  
  
  
END;

PROCEDURE checkMandatVsJefyDepense
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  pco_num_depense jefy_depense.depense_ctrl_planco.PCO_NUM%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b, 
      jefy_depense.depense_ctrl_planco dpco
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and m.man_id=dpco.man_id
    and dpco.pco_num <> m.pco_num
   
    ;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    select pco_num into pco_num_depense from jefy_depense.depense_ctrl_planco where man_id = objet.man_id;
    
    probleme := 'Imputation du mandat (' || objet.pco_num || ') incoherente avec l''imputation  de la depense côté budgétaire ('|| pco_num_depense ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Une raison possible est qu''il y a eu une réimputation comptable du mandat qui n''a pas été réimpactée budgétairement. Il est nécessaire d''intervenir dans la base de données pour le man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;

procedure checkEcritureDetailGestion(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code not in
      (select ges_code
      from maracuja.gestion_exercice
      where exe_ordre              =exeOrdre
      );
BEGIN
  categorie := 'CODES GESTION';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un code gestion non actif sur l''exercice (numero '|| ecrNumero ||' / code gestion ' || lecritureDetail.ges_code ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Activer le code gestion sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco2
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   not in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre      
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte supprimé sur '|| exeOrdre || '(numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;

procedure checkCadre2VsBalance(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vgescode gestion.ges_code%type;
  vpconum plan_comptable_exer.pco_num%type;
  vbaldebit number;
  vbalCredit number;
  vmandate number;
  vreverse number;
  vsolde number;
  vnet  number;
  

    cursor c1 is 
        select ges_code, pco_num,  sum(bal_debit) bal_debit, sum(bal_credit) bal_credit,  sum(bal_solde) bal_solde , sum(mandate) mandate, sum(reverse) reverse, sum(montant_net) montant_net
        from (
        select ges_code, ecd.pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '6%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, substr(ecd.pco_num,3,10) pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '186%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, pco_num, 0,0,0,sum(MANDATS), sum(reversements), sum(montant_net)
        from COMPTEFI.V_DVLOP_DEP
        where exe_ordre=exeOrdre
        and pco_num like '6%'
        group by ges_code, pco_num
        )
        group by ges_code, pco_num
        having (sum(bal_debit)<>sum(mandate) or sum(bal_credit)<> sum(reverse))
        order by ges_code, pco_num;

begin
    categorie     := 'COMPTE FINANCIER';
    sousCategorie := 'DIFFERENCE BALANCE/CADRE 2';
  OPEN C1;
  LOOP
    FETCH c1 INTO vgescode ,vpconum ,vbaldebit,vbalCredit,vSolde,vmandate, vreverse, vnet;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Code gestion: ' || vgescode|| ' Compte: ' || vPcoNum || ' : Balance <> cadre 2 (balance : D='||vBalDebit ||', C='|| vBalCredit ||') (Cadre 2 :  Mandats='||vMandate ||', ORV='|| vReverse ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          'Normal si certain bordereaux ne sont pas encore visés. Sinon incoherence dans les tables.',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;


procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
   checkMandatVsJefyDepense(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkEcritureDetailPlanco2(exeordre);
  checkEcritureDetailGestion(exeordre); 
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
  checkCadre2VsBalance(exeOrdre);
  
end;
END;
/












create procedure grhum.inst_patch_maracuja_1880 is
begin
	
	--******
	delete from MARACUJA.PLAN_COMPTABLE_REF_EXT where REF_PCO_DATE_DEBUT=TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS');
	
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62888', 'Autres', NULL, '62888', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '629', 'Rabais, remises et ristournes obtenus sur autres services extérieurs', NULL, '629', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63', 'IMPÔTS, TAXES ET VERSEMENTS ASSIMILES-', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '631', 'Impôts, taxes et versements assimilés sur rémunérations (administration des impôts)', NULL, '631', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6311', 'Taxe sur les salaires', NULL, '6311', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6312', 'Taxe d''apprentissage', NULL, 'N', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6318', 'Autres impôts, taxes et versements assimilés sur rémunérations revenant à l''Etat', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '632', 'Charges fiscales sur congés payés', NULL, '632', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '633', 'Impôts, taxes et versements assimilés sur rémunérations (autres organismes)', NULL, '633', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6331', 'Versement de transport', NULL, '6331', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6332', 'Cotisation FNAL (article L 834-1-1° du code de la sécurité sociale)', NULL, '6332', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6333', 'Contributions au fonds interfonction publique en faveur des personnels handicapés', NULL, 'N', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6335', 'Versements libératoires ouvrant droit à l''exonération de la taxe d''apprentissage', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6338', 'Autres impôts, taxes et versements assimilés sur rémunérations ne revenant pas à l''Etat', NULL, '6338', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '635', 'Autres impôts, taxes et versements assimilés (administration des impôts)', NULL, '635', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6351', 'Impôts directs', NULL, '6351', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63511', 'Taxe professionnelle', NULL, '63511', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63512', 'Taxes foncières', NULL, '63512', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63513', 'Autres impôts ou taxes à caractère local', NULL, '63513', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63518', 'Autres impôts directs', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6352', 'Taxes sur le chiffre d''affaires non récupérables', NULL, '6352', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6353', 'Impôts indirects', NULL, '6353', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63541', 'Droits de mutation', NULL, '63541', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6358', 'Autres droits', NULL, '6358', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '637', 'Autres impôts, taxes et versements assimilés (autres organismes)', NULL, '637', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6374', 'Impôts et taxes exigibles à l''étranger', NULL, '6374', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6378', 'Taxes diverses', NULL, '6378', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641', 'Rémunérations du personnel', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641111', 'Rémunérations principales des fonctionnaires et contractuels', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641113', 'Rémunérations du personnel dans le cadre de dispositifs d''aides à l''emploi', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64112', 'Rémunérations accessoires', NULL, ' ', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641122', 'Rémunérations accessoires non indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6413', 'Primes et gratifications', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64132', 'Primes et gratifications non indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64141', 'Droit individuel à la formation', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641421', 'Indemnités indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641422', 'Indemnités non indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64144', 'Indemnités de jurys d''enseignement et de concours', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64145', 'Défiscalisation des heures supplémentaires (part remboursée par l''Etat)', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6414511', 'Compensation nette des réductions des charges de sécurité sociale', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6414519', 'Remboursement des compensations des réductions des charges de sécurité sociale', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64148', 'Indemnités et avantages divers', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641481', 'Indemnités indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6415', 'Supplément familial', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6451', 'Cotisations d''assurance-maladie', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6452', 'Cotisations aux Mutuelles', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64531', 'Cotisations patronales aux régimes de pensions civiles et militaires', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64533', 'Cotisations patronales au régime de retraite additionnelle obligatoire (RAFP)', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64535', 'Cotisations IRCANTEC - agents non titulaires', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64538', 'Autres cotisations', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64541', 'Cotisations au régime d''assurance chômage (part patronale)', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6455', 'Charges sociales sur congés à payer', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6458', 'Cotisations aux autres organismes sociaux', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64581', 'Cotisations d''allocations familiales', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64588', 'Autres cotisations aux organismes sociaux', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6471', 'Prestations directes', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64712', 'AIT- Allocation d''invalidité temporaire', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64713', 'Capital-décès', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64714', 'Congés de longue durée', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6181', 'Documentation générale', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '61831', 'Abonnements', NULL, '61831', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618312', 'Étrangers', NULL, '618312', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618321', 'Français', NULL, '618321', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618322', 'Étrangers', NULL, '618322', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618331', 'Français', NULL, '618331', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6184', 'Reprographie', NULL, '6184', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '619', 'Rabais, remises et ristournes obtenus sur services extérieurs', NULL, '619', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6211', 'Personnel intérimaire', NULL, '6211', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '622', 'Rémunérations d''intermédiaires et honoraires', NULL, '622', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5114', 'Chèques postaux à encaisser', NULL, '5114', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5117', 'Chèques impayés', NULL, '5117', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '512', 'Banques', NULL, '512', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6221', 'Commissions et courtages sur achats', NULL, '6221', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6224', 'Rémunérations des transitaires', NULL, '6224', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6227', 'Frais d''actes et de contentieux', NULL, '6227', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6231', 'Annonces et insertions', NULL, '6231', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '671', 'Charges exceptionnelles sur opérations de gestion', NULL, '671', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6233', 'Foires et expositions', NULL, '6233', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6237', 'Publications', NULL, '6237', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6238', 'Divers', NULL, '6238', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6241', 'Transports sur achats', NULL, '6241', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6243', 'Transports entre établissements', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6247', 'Transports collectifs du personnel', NULL, '6247', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6251', 'Voyages et déplacements', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '28161', 'Collections de documentation', NULL, '28161', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '283', 'Amortissements des immobilisations incorporelles dont la charge du renouvellement n''incombe pas à l''établissement  ,(mêmes subdivisions que le compte 280)', NULL, '283', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2908', 'Autres immobilisations incorporelles', NULL, '2908', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '293', 'Dépréciations des immobilisations en cours', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2932', 'Immobilisations incorporelles en cours', NULL, '2932', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2961', 'Titres de participation', NULL, '2961', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2967', 'Créances rattachées à des participations (même ventilation que celle du compte 267)', NULL, '2967', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2972', 'Titres immobilisés (droits de créance)', NULL, '2972', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29743', 'Prêts au personnel', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2975', 'Dépôts et cautionnements versés', NULL, '2975', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29755', 'Cautionnements', NULL, '29755', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2976', 'Autres créances immobilisées', NULL, '2976', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3', 'COMPTES DE STOCKS ET D''EN-COURS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '31', 'MATIÈRES PREMIÈRES (et fournitures)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '312', 'Matière B', NULL, '312', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '32', 'AUTRES APPROVISIONNEMENTS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3211', 'Carburants', NULL, '3211', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '322', 'Fournitures consommables', NULL, '322', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3222', 'Produits d''entretien', NULL, '3222', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3224', 'Fournitures de magasin', NULL, '3224', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '411', 'Clients et usagers', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4117', 'Retenues de garanties', NULL, '4117', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '413', 'Clients - Effets à recevoir', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '418', 'Clients et usagers - Produits non encore facturés', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4192', 'Étudiants - avances et acomptes reçus', NULL, '4192', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4197', 'Clients et étudiants - autres avoirs', NULL, '4197', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '421', 'Personnel - Rémunérations dues', NULL, '421', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '425', 'Personnel - Avances et acomptes', NULL, '425', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '428', 'Personnel - charges à payer et produits à recevoir', NULL, '428', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4287', 'Produits à recevoir', NULL, '4287', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4291', 'Déficits constatés', NULL, '4291', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42911', 'Comptables', NULL, '42911', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4292', 'Ordres de versement émis suite à constatation de déficit', NULL, '4292', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4294', 'Débets émis par arrêté du ministre', NULL, '4294', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42942', 'Régisseurs', NULL, '42942', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42951', 'Comptables', NULL, '42951', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4296', 'Redevables d''intérêts sur débet', NULL, '4296', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42962', 'Régisseurs', NULL, '42962', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4297', 'Redevables des condamnations pécuniaires', NULL, '4297', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '43', 'SÉCURITÉ SOCIALE ET AUTRES ORGANISMES SOCIAUX', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64718', 'Autres prestations directes', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6472', 'Versements aux comités d''entreprise et d''établissement', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6475', 'Médecine du travail', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '65', 'AUTRES CHARGES DE GESTION COURANTE', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6255', 'Frais de déménagement', NULL, '6255', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6256', 'Missions', NULL, '6256', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62561', 'Personnels de l''EPSCP', NULL, '62561', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62563', 'Personnalités extérieures', NULL, '62563', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6264', 'Téléphone', NULL, '6264', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618', 'Divers', NULL, '618', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6571', 'Bourses', NULL, '6571', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6575', 'Subventions reçues et réparties par l''EPSCP', NULL, '6575', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6576', 'Subventions diverses (dont subventions versées par la fondation)', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6578', 'Autres charges spécifiques', NULL, '6578', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '658', 'Charges diverses de gestion courante', NULL, '658', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6583', 'Charges de gestion courante provenant de l''annulation d''ordres de recettes des exercices antérieurs', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6588', 'Autres charges diverses de gestion courante', NULL, '6588', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '66', 'CHARGES FINANCIÈRES', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '661', 'Charges d''intérêts', NULL, '661', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6611', 'Intérêts des emprunts et des dettes', NULL, '6611', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6616', 'Intérêts bancaires', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6618', 'Intérêts des autres dettes', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '66181', 'Des dettes commerciales', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '664', 'Pertes sur créances liées à des participations', NULL, '664', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '665', 'Escomptes accordés', NULL, '665', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '666', 'Pertes de change', NULL, '666', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '667', 'Charges nettes sur cessions de valeurs mobilières de placement', NULL, '667', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '668', 'Autres charges financières', NULL, '668', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6683', 'Charges financières provenant de l''annulation d''ordres de recettes des exercices antérieurs', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6685', 'Charges financières provenant de l''encaissement des chèques vacances', NULL, '6685', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6688', 'Autres charges financières', NULL, '6688', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '67', 'CHARGES EXCEPTIONNELLES', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6711', 'Pénalités sur contrats et conventions', NULL, '6711', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6712', 'Pénalités, amendes fiscales et pénales', NULL, '6712', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6713', 'Dons, libéralités', NULL, '6713', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6714', 'Créances devenues irrécouvrables dans l''exercice', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6715', 'Subventions accordées', NULL, '6715', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6716', 'Déficits ou débets admis en décharge ou en remise gracieuse', NULL, '6716', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6717', 'Intérêts sur débets admis en remise gracieuse', NULL, '6717', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6718', 'Autres charges exceptionnelles sur opérations de gestion', NULL, '6718', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '67181', 'Rappels d''impôt (autres qu''IS)', NULL, '67181', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '67182', 'Charges exceptionnelles provenant de l''annulation d''ordres de recettes des exercices antérieurs', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '67188', 'Autres charges exceptionnelles diverses', NULL, '67188', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '675', 'Valeurs comptables des éléments d''actif cédés', NULL, '675', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6751', 'Immobilisations incorporelles', NULL, '6751', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6756', 'Immobilisations financières', NULL, '6756', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '678', 'Autres charges exceptionnelles', NULL, '678', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68', 'DOTATIONS AUX AMORTISSEMENTS, DEPRECIATIONS ET PROVISIONS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6811', 'Dotations aux amortissements sur immobilisations incorporelles et corporelles', NULL, '6811', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68111', 'Immobilisations incorporelles', NULL, '68111', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681111', 'Frais d''établissement', NULL, '681111', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681115', 'Concessions et droits similaires, brevets, licences, droits et valeurs similaires', NULL, '681115', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681116', 'Droit au bail', NULL, '681116', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68112', 'Immobilisations corporelles', NULL, '68112', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681123', 'Constructions (à subdiviser comme le compte 213)', NULL, '681123', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681124', 'Constructions sur sol d''autrui (à subdiviser comme le compte 214)', NULL, '681124', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681126', 'Collections (à subdiviser comme le compte 216)', NULL, '681126', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681128', 'Autres immobilisations corporelles ( à subdiviser comme le compte 218)', NULL, '681128', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6812', 'Dotations aux amortissements des charges d''exploitation à répartir', NULL, '6812', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6815', 'Dotations aux provisions d''exploitation', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68161', 'Immobilisations incorporelles', NULL, '68161', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68162', 'Immobilisations corporelles', NULL, '68162', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6817', 'Dotations aux dépréciations des actifs circulants', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68174', 'Créances', NULL, '68174', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6865', 'Dotations aux provisions financières', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6866', 'Dotations aux dépréciations des éléments financiers', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68665', 'Valeurs mobilières de placement', NULL, '68665', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6871', 'Dotations aux amortissements exceptionnels des immobilisations', NULL, '6871', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6872', 'Dotations aux provisions réglementées (immobilisations)', NULL, 'N', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68725', 'Amortissements dérogatoires', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6876', 'Dotations aux dépréciations exceptionnelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '689', 'Engagements à réaliser sur ressources affectées', NULL, 'N', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6895', 'Engagements à réaliser sur dons manuels affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '69', 'IMPÔT SUR LES BÉNÉFICES ET IMPÔTS ASSIMILÉS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '695', 'Impôt sur les bénéfices', NULL, '695', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7', 'COMPTES DE PRODUITS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '701', 'Ventes de produits finis', NULL, '701', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '702', 'Ventes de produits intermédiaires', NULL, '702', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '703', 'Ventes de produits résiduels', NULL, '703', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '706', 'Prestations de services', NULL, '706', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70611', 'Droits de scolarité applicables aux diplômes nationaux', NULL, '70611', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70613', 'Redevances', NULL, '70613', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7062', 'Prestations de recherche', NULL, '7062', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7064', 'Prestations et travaux informatiques', NULL, '7064', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7066', 'Colloques', NULL, '7066', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7068', 'Autres prestations de services', NULL, '7068', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6511', 'Redevances pour concessions, brevets, licences, marques, procédés, logiciels', NULL, '6511', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6516', 'Droits d''auteur et de reproduction', NULL, '6516', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '654', 'Pertes sur créances irrécouvrables', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '657', 'Charges spécifiques', NULL, '657', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7084', 'Mise à disposition de personnel facturée', NULL, '7084', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5187', 'Intérêts courus à percevoir', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '53', 'CAISSE', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '531', 'Caisse', NULL, '531', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '54', 'RÉGIES D''AVANCES ET ACCRÉDITIFS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '541', 'Comptables secondaires', NULL, '541', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '542', 'Accréditifs', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '543', 'Régies d''avances', NULL, '543', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '545', 'Régies de recettes', NULL, '545', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '548', 'Avances pour menues dépenses', NULL, '548', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '58', 'VIREMENTS INTERNES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '581', 'Virements internes de comptes', NULL, '581', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '585', 'Virements internes de fonds', NULL, '585', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '59', 'DÉPRÉCIATIONS DES COMPTES FINANCIERS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '590', 'Dépréciations des valeurs mobilières de placement (à détailler comme le compte 50)', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5903', 'Actions', NULL, '5903', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5904', 'Autres titres conférant un droit de propriété', NULL, '5904', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5906', 'Obligations', NULL, '5906', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5908', 'Autres valeurs mobilières de placements et autres créances assimilées', NULL, '5908', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6', 'COMPTES DE CHARGES', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60', 'ACHATS ET VARIATION DE STOCKS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '601', 'Achats stockés - matières premières et fournitures', NULL, '601', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6011', 'Matière A', NULL, '6011', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6012', 'Matière B', NULL, '6012', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6017', 'Fournitures A,B,C', NULL, '6017', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '602', 'Achats stockés - autres approvisionnements', NULL, '602', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6021', 'Matières consommables', NULL, '6021', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60211', 'Carburants', NULL, '60211', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6022', 'fournitures consommables', NULL, '6022', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60222', 'Produits d''entretien', NULL, '60222', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60224', 'Fournitures de magasin', NULL, '60224', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60228', 'Autres fournitures consommables', NULL, '60228', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '604', 'Achats d''études et prestations de services', NULL, '604', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '606', 'Achats non stockés de matières et fournitures', NULL, '606', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6061', 'Fournitures non stockables - eau, énergie', NULL, '6061', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60613', 'Gaz', NULL, '60613', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60614', 'Chauffage sur réseau', NULL, '60614', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6062', 'Acquisition de papier', NULL, '6062', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6063', 'Fournitures d''entretien et de petit équipement', NULL, '6063', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6065', 'Linge, vêtements de travail', NULL, '6065', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6068', 'Autres matières et fournitures non stockées', NULL, '6068', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '607', 'Achats de marchandises', NULL, '607', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6072', 'Marchandise B', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '609', 'Rabais, remises et ristournes obtenus sur achats', NULL, '609', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6092', 'D''autres approvisionnements stockés', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6095', 'De matériels, équipements et travaux', NULL, '6095', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6096', 'D''approvisionnements non stockés', NULL, '6096', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6098', 'Rabais, remises, ristournes obtenus non affectés', NULL, '6098', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6031', 'Variation des stocks de matières premières (et fournitures)', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6032', 'Variation des stocks des autres approvisionnements', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '61', 'SERVICES EXTÉRIEURS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '612', 'Redevances de crédit-bail', NULL, '612', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6125', 'Crédit-bail immobilier', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6132', 'Locations immobilières', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6136', 'Malis sur emballages', NULL, '6136', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '615', 'Entretien et réparations', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6155', 'Sur biens mobiliers', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6161', 'Multirisques', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6168', 'Autres assurances', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '617', 'Études et recherches', NULL, '617', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6183', 'Documentation technique et bibliothèque', NULL, '6183', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618311', 'Français', NULL, '618311', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '61832', 'Ouvrages', NULL, '61832', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '61833', 'Ouvrages électroniques', NULL, '61833', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '618332', 'Étrangers', NULL, '618332', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6185', 'Frais de colloques, séminaires, conférences', NULL, '6185', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62', 'AUTRES SERVICES EXTÉRIEURS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '621', 'Personnel extérieur à l''établissement', NULL, '621', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6214', 'Personnel détaché ou prêté', NULL, '6214', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5113', 'Chèques vacances à l''encaissement', NULL, '5113', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5115', 'Cartes bancaires à l''encaissement', NULL, '5115', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5118', 'Autres valeurs à l''encaissement', NULL, '5118', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5121', 'Compte en monnaie nationale', NULL, '5121', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6222', 'Commissions et courtages sur ventes', NULL, '6222', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6226', 'Honoraires', NULL, '6226', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6228', 'Divers', NULL, '6228', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '623', 'Publicité, publications, relations publiques', NULL, '623', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4311', 'Cotisations de sécurité sociale', NULL, '4311', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '437', 'Autres organismes sociaux', NULL, '437', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4372', 'Contributions et retenues pour pensions civiles', NULL, '4372', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4374', 'IRCANTEC', NULL, '4374', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4376', 'Mutuelles', NULL, '4376', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4378', 'Divers', NULL, '4378', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '438', 'Organismes sociaux - Charges à payer et produits à recevoir', NULL, '438', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4387', 'Produits à recevoir', NULL, '4387', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '441', 'État et autres collectivités publiques - Subventions à recevoir', NULL, '441', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4418', 'Subventions d''équilibre', NULL, '4418', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '443', 'Opérations particulières avec l''État, les collectivités publiques, les organismes internationaux', NULL, '443', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4438', 'Intérêts courus sur créances figurant au compte 4431', NULL, 'I', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4442', 'État - Impôts sur les bénéfices - Acomptes', NULL, '4442', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4445', 'Etat - Impôt sur les sociétés (organismes sans but lucratif)', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4452', 'TVA due intra-communautaire', NULL, '4452', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44562', 'TVA sur immobilisations', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445621', 'TVA sur immobilisations - France', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3228', 'Autres fournitures consommables', NULL, '3228', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '33', 'EN-COURS DE PRODUCTION DE BIENS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '335', 'Travaux en cours', NULL, '335', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '341', 'Études en cours', NULL, '341', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '345', 'Prestations de services en cours', NULL, '345', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6232', 'Échantillons', NULL, '6232', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6236', 'Catalogues et imprimés', NULL, '6236', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '624', 'Transports de biens et transports collectifs de personnes (frais payés à des tiers)', NULL, '624', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6242', 'Transports sur ventes', NULL, '6242', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6244', 'Transports administratifs', NULL, '6244', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6248', 'Divers', NULL, '6248', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '625', 'Déplacements, missions et réceptions', NULL, '625', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6254', 'Frais d''inscription aux colloques', NULL, '6254', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4631', 'Exploitation', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '464', 'Dettes sur acquisitions de valeurs mobilières de placement', NULL, '464', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4661', 'ordres de dépenses à payer', NULL, 'I', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4664', 'Excédents de versement à rembourser', NULL, '4664', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4668', 'Avis de paiement (à subdiviser par exercice d''origine)', NULL, '4668', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4674', 'Taxe d''apprentissage', NULL, '4674', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4682', 'Charges à payer sur ressources affectées', NULL, 'I', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4687', 'Produits à recevoir', NULL, '4687', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '471', 'Recettes à classer', NULL, '471', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4712', 'Recettes des comptables secondaires à vérifier', NULL, '4712', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47138', 'Autres', NULL, '47138', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4718', 'Autres recettes à classer', NULL, '4718', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4721', 'Dépenses payées avant ordonnancement', NULL, '4721', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4725', 'Dépenses des régisseurs à vérifier', NULL, '4725', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4729', 'Dépenses dont le paiement est différé', NULL, '4729', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47311', 'Sécurité sociale étudiante', NULL, '47311', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47314', 'Médecine Préventive', NULL, '47314', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47318', 'Autres recettes à transférer', NULL, '47318', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '475', 'Legs et donations en cours de réalisation', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4762', 'Augmentation des dettes', NULL, '4762', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '477', 'Différences de conversion sur opérations en devises - PASSIF', NULL, '477', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4778', 'Différences compensées par couverture de change', NULL, '4778', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '481', 'Charges à répartir sur plusieurs exercices', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4861', 'Exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4862', 'Hors exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4871', 'Exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '489', 'Quotas d''émission alloués par l''État', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '491', 'Dépréciations des comptes de clients, usagers et étudiants', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '495', 'Dépréciations des comptes du groupe et des associés', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4955', 'Comptes courants des associés', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '496', 'Dépréciations des comptes de débiteurs divers', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4967', 'Autres comptes débiteurs', NULL, '4967', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '50', 'VALEURS MOBILIÈRES DE PLACEMENT', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '503', 'Actions', NULL, '503', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5035', 'Titres non cotés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '506', 'Obligations', NULL, '506', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5061', 'Titres cotés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '507', 'Bons du Trésor et bons de caisse à court terme', NULL, '507', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445623', 'TVA sur immobilisations - Autres pays', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445662', 'TVA sur autres biens et services intra-communautaire', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5088', 'Intérêts courus sur obligations, bons et valeurs assimilées', NULL, '5088', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '511', 'Valeurs à l''encaissement', NULL, '511', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60221', 'Combustibles', NULL, '60221', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60223', 'Fournitures d''atelier', NULL, '60223', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60225', 'Fournitures de bureau', NULL, '60225', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6026', 'Emballages', NULL, '6026', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '605', 'Achat de matériel, équipements et travaux', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60611', 'Électricité', NULL, '60611', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60612', 'Carburants et lubrifiants', NULL, '60612', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60617', 'Eau', NULL, '60617', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '60618', 'Autres fournitures non stockables', NULL, '60618', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6064', 'Fournitures administratives', NULL, '6064', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6067', 'Fournitures et matériels d''enseignement et de recherche non immobilisés', NULL, '6067', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6071', 'Marchandise A', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '608', 'Frais accessoires d''achat', NULL, '608', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6091', 'De matières premières (et fournitures)', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6094', 'D''études et de prestations de services', NULL, '6094', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6097', 'De marchandises', NULL, '6097', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '603', 'Variation des stocks (approvisionnements et marchandises)', NULL, '603', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6037', 'Variation des stocks de marchandises', NULL, '6037', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '611', 'Sous-traitance générale', NULL, '611', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6122', 'Crédit-bail mobilier', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '613', 'Locations', NULL, '613', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6135', 'Locations mobilières', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '614', 'Charges locatives et de copropriété', NULL, '614', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6152', 'Sur biens immobiliers', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6156', 'Maintenance', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '616', 'Primes d''assurance', NULL, '616', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6162', 'Assurance obligatoire dommage-construction', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7085', 'Ports et frais accessoires facturés aux clients', NULL, '7085', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7087', 'Hébergements et restauration', NULL, '7087', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7088', 'Autres produits d''activités annexes', NULL, '7088', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '709', 'Rabais, remises et ristournes accordés par l''établissement ou la fondation', NULL, '709', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7091', 'Sur ventes de produits finis', NULL, '7091', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7092', 'Sur ventes de produits intermédiaires', NULL, '7092', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7093', 'Sur ventes de produits résiduels', NULL, '7093', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7094', 'Sur travaux', NULL, '7094', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7095', 'Sur études', NULL, '7095', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7096', 'Sur prestations de services', NULL, '7096', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7097', 'Sur ventes de marchandises', NULL, '7097', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7098', 'Sur produits des activités annexes', NULL, '7098', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '71', 'PRODUCTION STOCKÉE (ou destockage)', NULL, NULL, 
    'O', 'I', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '713', 'Variation des stocks (en-cours de production, produits)', NULL, '713', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7133', 'Variation des en-cours de production de biens', NULL, '7133', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7134', 'Variation des en-cours de production de services', NULL, '7134', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7135', 'Variation des stocks de produits', NULL, '7135', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '72', 'PRODUCTION IMMOBILISÉE', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '721', 'Immobilisations incorporelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '722', 'Immobilisations corporelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '74', 'SUBVENTIONS D''EXPLOITATION', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '741', 'État', NULL, '741', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7411', 'Ministère de tutelle', NULL, '7411', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7418', 'Autres ministères', NULL, '7418', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '744', 'Collectivités publiques et organismes internationaux', NULL, '744', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7441', 'ANR', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7442', 'Région', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7443', 'Département', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7444', 'Communes et groupements de communes', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7445', 'ASP', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7446', 'Union Européenne', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7447', 'Organismes internationaux', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7448', 'Autres collectivités publiques et organismes internationaux', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '746', 'Dons et legs', NULL, '746', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7481', 'Produits des versements libératoires ouvrant droit à l''exonération de la taxe d''apprentissage', NULL, '7481', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '75', 'AUTRES PRODUITS DE GESTION COURANTE', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7511', 'Redevances pour concessions, brevets, licences, marques, procédés et logiciels', NULL, '7511', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7516', 'Droits d''auteur et de reproduction', NULL, '7516', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7518', 'Autres droits et valeurs similaires', NULL, '7518', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '755', 'Quote-part de résultat sur opérations faites en commun', NULL, 'I', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '756', 'Quote-part d''élements virés au compte de résultat', NULL, 'N', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7562', 'Quote-part des apports virée au compte de résultat', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '757', 'Produits spécifiques', NULL, '757', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '758', 'Produits divers de gestion courante', NULL, '758', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7582', 'Dons manuels affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7584', 'Frais de poursuite et de contentieux', NULL, '7584', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7585', 'Legs et donations non affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7586', 'Legs et donations affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7587', 'Ventes de dons en nature', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '761', 'Produits de participations', NULL, '761', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7616', 'Revenus sur autres formes de participation', NULL, '7616', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '762', 'Produits des autres immobilisations financières', NULL, '762', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7621', 'Revenus des titres immobilisés', NULL, '7621', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7627', 'Revenus des créances immobilisées', NULL, '7627', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7638', 'Revenus sur créances diverses', NULL, '7638', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '765', 'Escomptes obtenus', NULL, '765', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '766', 'Gains de change', NULL, '766', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '768', 'Autres produits financiers', NULL, '768', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7681', 'Intérêts des comptes financiers débiteurs', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '771', 'Produits exceptionnels sur opérations de gestion', NULL, '771', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7712', 'Dégrèvements d''impôts (autres qu''impôts sur les bénéfices)', NULL, '7712', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7713', 'Libéralités reçues', NULL, '7713', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7715', 'Condamnations pécuniaires prononcées par le juge des comptes', NULL, '7715', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7717', 'Dettes atteintes par la prescription quadriennale', NULL, '7717', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7718', 'Autres produits exceptionnels sur opérations de gestion de l''exercice', NULL, '7718', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '77182', 'Produits exceptionnels provenant de l''annulation d''ordres de dépenses des exercices antérieurs', NULL, 'I', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '772', 'Produits des exercices antérieurs', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '775', 'Produits des cessions d''éléments d''actif', NULL, '775', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7752', 'Immobilisations corporelles', NULL, '7752', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7758', 'Autres éléments d''actif', NULL, '7758', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '777', 'Quote-part des subventions d''investissement virée au résultat de l''exercice', NULL, '777', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '778', 'Autres produits exceptionnels', NULL, '778', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '781', 'Reprises sur amortissements, dépréciations et provisions (à inscrire dans les produits d''exploitation)', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7811', 'Reprises sur amortissements des immobilisations incorporelles et corporelles', NULL, '7811', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78112', 'Immobilisations corporelles', NULL, '78112', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7816', 'Reprises sur dépréciations des immobilisations incorporelles et corporelles', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78161', 'Immobilisations incorporelles', NULL, '78161', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7817', 'Reprises sur dépréciations des actifs circulants', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78174', 'Créances', NULL, '78174', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70688', 'autres prestations de services', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '707', 'Ventes de marchandises', NULL, '707', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7081', 'Produits des services exploités dans l''intérêt du personnel', NULL, '7081', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7082', 'Commissions et courtages', NULL, '7082', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7865', 'Reprises sur provisions financières', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63514', 'Taxe sur les bureaux - région Ile de France (article 231 du CGl)', NULL, '63514', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6354', 'Droits d''enregistrement et de timbre', NULL, '6354', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '63542', 'Taxe différentielle sur les véhicules à moteur', NULL, '63542', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6372', 'Taxes perçues par les organismes publics internationaux', NULL, '6372', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64', 'CHARGES DE PERSONNEL', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6411', 'Salaires', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64111', 'Rémunérations principales', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641112', 'Indemnité mensuelle forfaitaire (congé de formation)', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641121', 'Rémunérations accessoires indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6412', 'Congés payés', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64131', 'Primes et gratifications indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6414', 'Indemnités et avantages divers', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64142', 'Indemnités liées à la résidence et à la mobilité', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64143', 'Indemnités attribuées dans le cadre de contrats et conventions', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641451', 'Compensation des réductions des charges de sécurité sociale', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64146', 'Indemnités de préavis et de licenciement et allocations de retour à l''emploi', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '641482', 'Indemnités non indexées', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '645', 'Charges de Sécurité Sociale et de prévoyance', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6453', 'Cotisations aux caisses de retraites', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64532', 'Cotisations patronales CNRACL', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64534', 'Cotisations d''assurance vieillesse- agents non titulaires', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6454', 'Cotisations aux ASSEDIC', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64548', 'Autres cotisations aux ASSEDIC', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6456', 'Cotisations liées au risque invalidité', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64582', 'Contribution solidarité autonomie', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '647', 'Autres charges sociales', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64711', 'Accidents du travail et maladies professionnelles des agents titulaires et non-titulaires permanents', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '64715', 'Remboursements forfaitaires de transports', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6474', 'Oeuvres sociales', NULL, NULL, 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '648', 'Autres charges de personnel', NULL, NULL, 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '651', 'Redevances pour concessions, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires', NULL, '651', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62562', 'Étudiants', NULL, '62562', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6257', 'Réceptions', NULL, '6257', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '626', 'Frais postaux et de télécommunications', NULL, '626', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '672', 'Charges sur exercices antérieurs (à reclasser)', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6752', 'Immobilisations corporelles', NULL, '6752', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6758', 'Autres éléments d''actif', NULL, '6758', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6788', 'Charges exceptionnelles diverses', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681', 'Dotations aux amortissements, dépréciations et provisions - Charges d''exploitation', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681113', 'Frais de recherche et de développement', NULL, '681113', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681118', 'Autres immobilisations incorporelles', NULL, '681118', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681122', 'Agencements, aménagements de terrains', NULL, '681122', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '681125', 'Installations techniques, matériel et outillage (à subdiviser comme le compte 215)', NULL, '681125', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68126', 'Frais d''émission des emprunts', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6816', 'Dotations aux dépréciations des immobilisations incorporelles et corporelles', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68173', 'Stocks et en-cours', NULL, '68173', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '686', 'Dotations aux amortissements, dépréciations et provisions - Charges financières', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '68662', 'Immobilisations financières', NULL, '68662', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '687', 'Dotations aux amortissements, dépréciations et provisions - Charges exceptionnelles', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6875', 'Dotations aux provisions exceptionnelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6894', 'Engagements à réaliser sur subventions attribuées', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6897', 'Engagement à réaliser sur legs et donations affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '699', 'Produits - report en arrière des déficits', NULL, '699', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70', 'VENTES DE PRODUITS FABRIQUES, PRESTATIONS DE SERVICES, MARCHANDISES', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '704', 'Travaux', NULL, '704', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '705', 'Études', NULL, '705', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7061', 'Droits de scolarité et redevances', NULL, '7061', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7065', 'Prestations de formation continue', NULL, '7065', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7067', 'Ventes de publications', NULL, '7067', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70681', 'validation des acquis de l''expérience (VAE)', NULL, 'N', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6518', 'Autres droits et valeurs similaires', NULL, '6518', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '655', 'Quotes-parts de résultat sur opérations faites en commun', NULL, 'I', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '748', 'Autres subventions d''exploitation', NULL, '748', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7488', 'Autres', NULL, '7488', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '751', 'Redevances pour concessions, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires', NULL, '751', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '752', 'Revenus des immeubles non affectés aux activités de l''établissement', NULL, '752', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7561', 'Quote-part de subventions d''investissement (non renouvelables) virée au compte de résultat', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7581', 'Dons manuels non affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7583', 'Produits de gestion courante provenant de l''annulation d''ordres de dépenses des exercices antérieurs', NULL, '7583', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7588', 'Autres', NULL, '7588', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '76', 'PRODUITS FINANCIERS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7611', 'Revenus des titres de participation', NULL, '7611', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7617', 'Revenus des créances rattachées à des participations', NULL, '7617', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7624', 'Revenus des prêts', NULL, '7624', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '763', 'Revenus des autres créances', NULL, '763', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '764', 'Revenus des valeurs mobilières de placement', NULL, '764', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '767', 'Produits nets sur cessions de valeurs mobilières de placement', NULL, '767', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78', 'REPRISES SUR AMORTISSEMENTS, DEPRECIATIONS ET PROVISIONS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78111', 'Immobilisations incorporelles', NULL, '78111', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7815', 'Reprises sur provisions d''exploitation', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78162', 'Immobilisations corporelles', NULL, '78162', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78173', 'Stocks et en-cours', NULL, '78173', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '786', 'Reprises sur dépréciations et provisions (à inscrire dans les produits financiers)', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '708', 'Produits des activités annexes', NULL, '708', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7083', 'Locations diverses', NULL, '7083', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '103179', 'Autres fonds propres - dotations consomptibles inscrites au compte de résultat', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '106', 'Réserves', NULL, '106', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1068', 'Autres réserves', NULL, '1068', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10682', 'Réserves facultatives', NULL, '10682', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '11', 'REPORT A NOUVEAU (solde créditeur ou débiteur)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '115', 'Résultat sous contrôle de tiers financeurs', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '120', 'Résultat de l''exercice (bénéfice)', NULL, '120', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13', 'SUBVENTIONS D''INVESTISSEMENT', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1311', 'État', NULL, '1311', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1312', 'Régions', NULL, '1312', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1314', 'Communes et groupements de communes', NULL, '1314', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1316', 'Union Européenne', NULL, '1316', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1318', 'Autres', NULL, '1318', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '139', 'Subventions d''investissement inscrites au compte de résultat', NULL, '139', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13912', 'Régions', NULL, '13912', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13913', 'Départements', NULL, '13913', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13915', 'Autres collectivités et établissements publics', NULL, '13915', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13918', 'Autres', NULL, '13918', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '14', 'PROVISIONS REGLEMENTEES', NULL, NULL, 
    NULL, 'N', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '70612', 'Droits des diplômes propres à chaque établissement', NULL, '70612', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7063', 'Mesures et expertises', NULL, '7063', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1517', 'Provision pour risques d''emploi', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '154', 'Provisions pour restructurations', NULL, 'N', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1572', 'Provisions pour gros entretien ou grandes révisions', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1587', 'Provisions pour allocation perte d''emploi d''indemnités de licenciement', NULL, '1587', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '165', 'Dépôts et cautionnements reçus', NULL, '165', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1655', 'Cautionnements', NULL, '1655', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1674', 'Avances de l''État et des collectivités publiques', NULL, '1674', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '168', 'Autres emprunts et dettes assimilées', NULL, '168', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1687', 'Autres dettes', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1688', 'Intérêts courus', NULL, '1688', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '171', 'Dettes rattachées à des participations (groupe)', NULL, '171', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '18', 'COMPTES DE LIAISON', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '181', 'Comptes de liaison', NULL, '181', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1851', 'SAIC', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1852', 'Fondation universitaire', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1854', 'CFA', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '186', 'Biens et prestations de services échangés (charges)', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '19', 'Fonds dédiés', NULL, NULL, 
    NULL, 'N', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21218', 'Autres terrains nus', NULL, '21218', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21226', 'Reçus en dotation ou en affectation', NULL, '21226', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2125', 'Agencements, aménagements de terrains bâtis', NULL, '2125', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21257', 'Acquis', NULL, '21257', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '213', 'Constructions', NULL, '213', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2131', 'Bâtiments', NULL, '2131', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21317', 'Bâtiments acquis', NULL, '21317', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2135', 'Installations générales, agencements, aménagements des constructions (même ventilation que celle du compte 2131)', NULL, '2135', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '214', 'Constructions sur sol d''autrui (même ventilation que celle du compte 213)', NULL, '214', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6265', 'Affranchissements', NULL, '6265', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6266', 'Internet', NULL, '6266', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6267', 'Liaisons informatiques spécialisées', NULL, '6267', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '627', 'Services bancaires et assimilés', NULL, '627', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6271', 'Frais sur titres (achat, vente, garde)', NULL, '6271', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6272', 'Commissions sur cartes bancaires', NULL, '6272', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6278', 'Autres frais et commissions', NULL, '6278', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '628', 'Divers', NULL, '628', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6281', 'Concours divers (cotisations...)', NULL, '6281', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6282', 'Blanchissage', NULL, '6282', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6283', 'Formation continue du personnel de l''établissement', NULL, '6283', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6284', 'Frais de recrutement du personnel', NULL, '6284', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6286', 'Contrats de nettoyage', NULL, '6286', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '6288', 'Autres prestations extérieures diverses', NULL, '6288', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7688', 'Autres', NULL, 'N', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '77', 'PRODUITS EXCEPTIONNELS', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7711', 'Dédits et pénalités perçus sur achats et ventes', NULL, '7711', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7714', 'Rentrées sur créances amorties', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7716', 'Recouvrements sur créances admises en non-valeur', NULL, '7716', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '77181', 'Subvention d''équilibre', NULL, '77181', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '77183', 'Intérêts issus d''arrêtés de débets', NULL, '7714', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7751', 'Immobilisations incorporelles', NULL, '7751', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7756', 'Immobilisations financières', NULL, '7756', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '776', 'Produits issus de la neutralisation des amortissements', NULL, '776', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '62885', 'Facturation des payes à façon effectuées par les services déconcentrés du Trésor', NULL, '62885', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23822', 'Terrains', NULL, '23822', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23823', 'Constructions', NULL, '23823', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23824', 'Constructions sur sol d''autrui', NULL, '23824', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23825', 'Installations techniques, matériel et outillage', NULL, '23825', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23828', 'Autres immobilisations corporelles', NULL, '23828', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '261', 'Titres de participation', NULL, '261', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '267', 'Créances rattachées à des participations', NULL, '267', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2674', 'Créances rattachées à des participations (hors groupe)', NULL, '2674', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2676', 'Avances consolidables', NULL, '2676', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '268', 'Créances rattachées à des sociétés en participation', NULL, '268', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '269', 'Versement restant à effectuer sur titres de participation non libérés', NULL, '269', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '271', 'Titres immobilisés (droit de propriété)', NULL, '271', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2718', 'Autres titres', NULL, '2718', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2721', 'Obligations', NULL, '2721', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2728', 'Autres titres', NULL, '2728', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2743', 'Prêts au personnel', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2751', 'Dépôts', NULL, '2751', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '276', 'Autres créances immobilisées', NULL, '276', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2768', 'Intérêts courus (à détailler)', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '28', 'AMORTISSEMENTS DES IMMOBILISATIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '280', 'Amortissements des immobilisations incorporelles', NULL, '280', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2803', 'Frais de recherche et de développement', NULL, '2803', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2805', 'Concessions et droits similaires, brevets, licences, logiciels, droits et valeurs similaires (même ventilation que celle du compte 205)', NULL, '2805', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2808', 'Autres immobilisations incorporelles', NULL, '2808', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2812', 'Agencements et aménagements de terrains (même ventilation que celle du compte 212)', NULL, '2812', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2813', 'Constructions (même ventilation que celle du compte 213)', NULL, '2813', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2815', 'Installations techniques, matériel et outillage (même ventilation que celle du compte 215)', NULL, '2815', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2816', 'Collections', NULL, '2816', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2818', 'Autres immobilisations corporelles (même ventilation que celle du compte 218)', NULL, '2818', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '284', 'Amortissements des immobilisations corporelles dont la charge du renouvellement n''incombe pas à l''établissement ,(mêmes subdivisions que le compte 281)', NULL, '284', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29', 'DÉPRÉCIATIONS DES IMMOBILISATIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '290', 'Dépréciations des immobilisations incorporelles', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2905', 'Marques, procédés, droits et valeurs similaires', NULL, '2905', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2906', 'Droit au bail', NULL, '2906', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '291', 'Dépréciations des immobilisations corporelles (même ventilation que celle du compte 21)', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2911', 'Terrains', NULL, '2911', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2931', 'Immobilisations corporelles en cours', NULL, '2931', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '296', 'Dépréciations des participations et créances rattachées à des participations', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2966', 'Autres formes de participation', NULL, '2966', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2968', 'Créances rattachées à des sociétés en participations (même ventilation que celle du compte 268)', NULL, '2968', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '297', 'Dépréciations des autres immobilisations financières', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2971', 'Titres immobilisés (droit de propriété)', NULL, '2971', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2974', 'Prêts', NULL, '2974', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29741', 'Prêts étudiants', NULL, '29741', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29748', 'Autres prêts', NULL, '29748', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29751', 'Dépôts', NULL, '29751', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29761', 'Créances diverses', NULL, '29761', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '29768', 'Intérêts courus', NULL, '29768', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '311', 'Matière A', NULL, '311', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '317', 'Fournitures A,B,C...', NULL, '317', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '321', 'Matières consommables', NULL, '321', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3221', 'Combustibles', NULL, '3221', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3223', 'Fournitures d''atelier', NULL, '3223', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '351', 'Produits intermédiaires', NULL, '351', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '355', 'Produits finis', NULL, '355', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '358', 'Produits résiduels (ou matières de récupération)', NULL, '358', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '37', 'STOCKS DE MARCHANDISES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '371', 'Marchandises A', NULL, '371', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '372', 'Marchandises B', NULL, '372', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '39', 'DÉPRÉCIATIONS DES STOCKS ET EN-COURS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '391', 'Dépréciations des matières premières (et fournitures)', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '392', 'Dépréciations des autres approvisionnements', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '393', 'Dépréciations des en-cours de production de biens', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '394', 'Dépréciations des en-cours de production de services', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '395', 'Dépréciations des stocks de produits', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '397', 'Dépréciations des stocks de marchandises', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4', 'COMPTES DE TIERS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '40', 'FOURNISSEURS ET COMPTES RATTACHES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '401', 'Fournisseurs', NULL, '401', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4011', 'Fournisseurs - Achats de biens ou de prestations de services', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4017', 'Retenues de garanties et oppositions', NULL, '4017', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '403', 'Fournisseurs - Effets à payer', NULL, '403', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '404', 'Fournisseurs d''immobilisations', NULL, '404', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4041', 'Fournisseurs - achats d''immobilisations', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4047', 'Fournisseurs d''immobilisations - Retenues de garantie et oppositions', NULL, '4047', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '405', 'Fournisseurs d''immobilisations - Effets à payer', NULL, '405', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '408', 'Fournisseurs - Factures non parvenues', NULL, '408', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4081', 'Fournisseurs - Achats de biens ou de prestations de services', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4084', 'Fournisseurs - Achats d''immobilisations', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4088', 'Fournisseurs - Intérêts courus', NULL, '4088', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '409', 'Fournisseurs débiteurs', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4091', 'Fournisseurs - Avances et acomptes versés sur commandes', NULL, '4091', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '40911', 'Avances versées sur commandes', NULL, '40911', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '40912', 'Acomptes versés sur commandes', NULL, '40912', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4092', 'Avances à l''UGAP', NULL, '4092', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4096', 'Fournisseurs - Créances pour emballages et matériel à rendre', NULL, '4096', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4098', 'Rabais, remises, ristournes à obtenir et autres avoirs non encore reçus', NULL, '4098', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '41', 'CLIENTS,  USAGERS ET COMPTES RATTACHES', NULL, NULL, 
    NULL, 'I', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4111', 'Clients', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '412', 'Étudiants', NULL, '412', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '416', 'Clients douteux ou litigieux', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '419', 'Clients, usagers et étudiants créditeurs', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4191', 'Clients - Avances et acomptes reçus sur commande', NULL, '4191', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4196', 'Clients - Dettes sur emballages et matériels consignés', NULL, '4196', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4198', 'Clients - Rabais, remises, ristournes à accorder et autres avoirs à établir', NULL, '4198', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42', 'PERSONNEL ET COMPTES RATTACHES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '422', 'Oeuvres sociales', NULL, '422', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '427', 'Personnel - Oppositions', NULL, '427', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4282', 'Dettes provisionnées pour congés payés', NULL, '4282', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4286', 'Autres charges à payer', NULL, '4286', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '429', 'Déficits et débets des comptables et régisseurs et redevables d''intérêts et condamnations pécuniaires', NULL, '429', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42912', 'Régisseurs', NULL, '42912', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42921', 'Comptables', NULL, '42921', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42922', 'Régisseurs', NULL, '42922', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42941', 'Comptables', NULL, '42941', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4295', 'Débets émis par jugement ou arrêt du juge des comptes', NULL, '4295', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42952', 'Régisseurs', NULL, '42952', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42961', 'Comptables', NULL, '42961', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42971', 'Comptables', NULL, '42971', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '42972', 'Régisseurs', NULL, '42972', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '431', 'Sécurité sociale', NULL, '431', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4312', 'Contribution sociale généralisée', NULL, '4312', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4313', 'Contribution au remboursement de la dette sociale (CRDS)', NULL, '4313', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4371', 'Contribution exceptionnelle de solidarité', NULL, '4371', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4373', 'Caisses de retraite et de prévoyance', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4375', 'PREFON', NULL, '4375', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4377', 'ASSEDIC', NULL, '4377', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4382', 'Charges sociales sur congés à payer', NULL, '4382', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4386', 'Autres charges à payer', NULL, '4386', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44', 'ÉTAT ET AUTRES COLLECTIVITÉS PUBLIQUES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4411', 'Subventions d''investissement', NULL, '4411', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4417', 'Subventions d''exploitation', NULL, '4417', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4419', 'Avances sur subventions', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4431', 'Créances sur l''État résultant de la suppression de la règle du décalage d''un mois en matière de TVA,(à subdiviser en fonction des collectivités publiques)', NULL, '4431', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4432', 'Créances sur l''Etat résultant des compensations des réductions de charges sociales', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4435', 'Opérations particulières avec l''ASP', NULL, 'I', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '444', 'État - Impôts sur les bénéfices', NULL, '444', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4441', 'État - Créance de carry-back', NULL, '4441', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4443', 'État - Imposition forfaitaire annuelle', NULL, '4443', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4449', 'Etat - avances sur subventions', NULL, '4449', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445', 'État - Taxes sur le chiffre d''affaires (TVA)', NULL, '445', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4455', 'TVA à décaisser', NULL, '4455', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456', 'TVA déductible', NULL, '4456', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445622', 'TVA sur immobilisations intra-communautaire', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '3225', 'Fournitures de bureau', NULL, '3225', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '326', 'Emballages', NULL, '326', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '331', 'Produits en cours', NULL, '331', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '34', 'EN-COURS DE PRODUCTION DE SERVICES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '35', 'STOCKS DE PRODUITS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44567', 'Crédit de T.V.A. à reporter', NULL, '44567', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445671', 'sur achats - France', NULL, '445671', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456711', 'Exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456712', 'Hors exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445672', 'sur achats intra-communautaire', NULL, '445672', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456721', 'Exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456722', 'Hors exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445673', 'sur achats - Autres pays', NULL, '445673', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456731', 'Exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4456732', 'Hors exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4457', 'TVA collectée par l''établissement', NULL, '4457', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4458', 'TVA à régulariser ou en attente', NULL, '4458', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44581', 'Acomptes-régime simplifié d''imposition', NULL, '44581', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44583', 'Remboursement de TVA demandé', NULL, '44583', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44584', 'TVA récupérée d''avance', NULL, '44584', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44587', 'TVA sur factures à établir', NULL, '44587', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '447', 'Autres impôts, taxes et versements assimilés', NULL, '447', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4471', 'Impôts, taxes et versements assimilés sur rémunérations (administration des impôts)', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44711', 'Taxe sur les salaires', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44718', 'Autres impôts, taxes et versements assimilés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4473', 'Impôts, taxes et versements assimilés sur rémunérations (autres organismes)', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4475', 'Autres impôts, taxes et versements assimilés (administration des impôts)', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4477', 'Autres impôts, taxes et versements assimilés (autres organismes)', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '448', 'État et autres collectivités publiques - Charges à payer et produits à recevoir', NULL, '448', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4482', 'Charges fiscales sur congés à payer', NULL, '4482', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4486', 'Charges à payer', NULL, '4486', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4487', 'Produits à recevoir', NULL, '4487', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '449', 'État - Quotas d''émission à restituer à l''État', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '45', 'GROUPE ET ASSOCIÉS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '451', 'Groupe', NULL, '451', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '455', 'Associés - Compte courant', NULL, '455', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '458', 'Associés - Opérations faites en commun et en GIE', NULL, '458', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '46', 'DÉBITEURS DIVERS ET CRÉDITEURS DIVERS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '462', 'Créances sur cessions d''immobilisations', NULL, '462', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '463', 'Autres comptes débiteurs - Ordres de recettes ou ordres de reversement à recouvrer', NULL, '463', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4632', 'Hors exploitation', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '465', 'Créances sur cessions de valeurs mobilières de placement', NULL, '465', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '466', 'Autres comptes créditeurs', NULL, '466', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4663', 'Virements à réimputer', NULL, '4663', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4666', 'Ordres de paiement émis pour les dépenses à l''étranger', NULL, '4666', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4667', 'Oppositions', NULL, '4667', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '467', 'Autres comptes débiteurs ou créditeurs', NULL, '467', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '468', 'Divers - Charges à payer et produits à recevoir', NULL, '468', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4684', 'Produits à recevoir sur ressources affectées', NULL, 'I', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4686', 'Charges à payer', NULL, '4686', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47', 'COMPTES TRANSITOIRES OU D''ATTENTE', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4713', 'Recettes perçues avant émission de titres', NULL, '4713', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47132', 'Droits de scolarité et redevances au comptant', NULL, '47132', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4715', 'Recettes des régisseurs à vérifier', NULL, '4715', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '472', 'Dépenses à classer et à régulariser', NULL, '472', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4722', 'Dépenses des comptables secondaires à vérifier', NULL, '4722', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4728', 'Autres dépenses à régulariser', NULL, '4728', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '473', 'Recettes et dépenses à transférer', NULL, '473', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4731', 'Recettes à transférer', NULL, '4731', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47312', 'Mutuelles', NULL, '47312', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '47315', 'Droits des bibliothèques universitaires non rattachées à l''établissement', NULL, '47315', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4735', 'Dépenses à transférer', NULL, '4735', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '476', 'Différences de conversion sur opérations en devises - ACTIF', NULL, '476', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4761', 'Diminution des créances', NULL, '4761', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4768', 'Différences compensées par couverture de change', NULL, '4768', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4771', 'Augmentation des créances', NULL, '4771', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4772', 'Diminution des dettes', NULL, '4772', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '48', 'COMPTES DE RÉGULARISATION', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4816', 'Frais d''émission des emprunts', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '486', 'Charges constatées d''avance (imputables à l''exercice suivant)', NULL, '486', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '487', 'Produits constatés d''avance (à rattacher à l''exercice suivant)', NULL, '487', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4872', 'Hors exploitation', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '49', 'DÉPRÉCIATIONS DES COMPTES DE TIERS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4911', 'Clients divers', NULL, '4911', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4912', 'Étudiants', NULL, '4912', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4951', 'Compte du groupe', NULL, '4951', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4958', 'Opérations faites en commun et en GIE', NULL, '4958', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4962', 'Créances sur cessions d''immobilisations', NULL, '4962', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '4965', 'Créances sur cessions de valeurs mobilières de placement', NULL, '4965', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5', 'COMPTES FINANCIERS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5031', 'Titres cotés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '504', 'Autres titres conférant un droit de propriété', NULL, '504', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5065', 'Titres non cotés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '508', 'Autres valeurs mobilières de placement et autres créances assimilées', NULL, '508', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '44566', 'TVA sur autres biens et services', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445661', 'TVA sur autres biens et services - France', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '445663', 'TVA sur autres biens et services - Autres pays', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5081', 'Autres valeurs mobilières', NULL, '5081', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '509', 'Versements restant à effectuer sur valeurs mobilières de placement non libérées', NULL, '509', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '51', 'BANQUES,  ÉTABLISSEMENTS FINANCIERS ET ASSIMILES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5112', 'Chèques bancaires à encaisser', NULL, '5112', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5124', 'Compte en devises', NULL, '5124', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '515', 'Trésor', NULL, '515', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5151', 'Compte au Trésor', NULL, '5151', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5159', 'Chèques à payer', NULL, '5159', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '517', 'Autres organismes financiers', NULL, '517', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5171', 'Compte à terme', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5172', 'Compte de placement rémunéré', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '518', 'Intérêts courus', NULL, '518', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '5186', 'Intérêts courus à payer', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21418', 'Autres bâtiments sur sol d''autrui', NULL, '21418', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21456', 'Installations générales, agencements, aménagements des constructions sur sol d''autrui affectés ou remis en dotation', NULL, '21456', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21516', 'Installations techniques complexes affectées', NULL, '21516 erreur ancienne version', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2153', 'Matériel scientifique', NULL, '2153', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21537', 'Matériel scientifique acquis', NULL, '21537', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2155', 'Outillage industriel', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21557', 'Outillage acquis', NULL, '21557', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2156', 'Matériel d''enseignement', NULL, '2154', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21567', 'Matériel d''enseignement acquis', NULL, '21547', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2157', 'Agencements et aménagements du matériel et outillage', NULL, '2157', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21578', 'Autres agencements et aménagements des autres matériels et outillages', NULL, '21578', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21616', 'Collections affectées ou remises en dotation', NULL, '21616', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2164', 'Collections littéraires, scientifiques, artistiques', NULL, '2164', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21648', 'Autres collections littéraires, scientifiques, artistiques', NULL, '21648', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21816', 'Installations générales, agencements, aménagements divers affectés ou remis en dotation', NULL, '21816', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21826', 'Matériel de transport affecté ou remis en dotation', NULL, '21826', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2183', 'Matériel de bureau', NULL, '2183', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21836', 'Matériel de bureau affecté ou remis en dotation', NULL, '21836', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2184', 'Mobilier', NULL, '2184', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21847', 'Mobilier acquis', NULL, '21847', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2185', 'Cheptel', NULL, '2185', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2186', 'Emballages récupérables', NULL, '2186', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21876', 'Matériel informatique affecté ou remis en dotation', NULL, '21876', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '26', 'PARTICIPATIONS ET CRÉANCES RATTACHÉES À DES PARTICIPATIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '266', 'Autres formes de participation', NULL, '266', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2671', 'Créances rattachées à des participations (groupe)', NULL, '2671', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2677', 'Autres créances rattachées à des participations', NULL, '2677', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '27', 'AUTRES IMMOBILISATIONS FINANCIÈRES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2711', 'Actions', NULL, '2711', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '145', 'Amortissements dérogatoires', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '151', 'Provisions pour risques', NULL, '151', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1515', 'Provisions pour pertes de change', NULL, '1515', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '272', 'Titres immobilisés (droits de créance)', NULL, '272', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2722', 'Bons', NULL, '2722', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '274', 'Prêts', NULL, '274', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2741', 'Prêts étudiants', NULL, '2741', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2748', 'Autres prêts', NULL, '2748', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '275', 'Dépôts et cautionnements versés', NULL, '275', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2755', 'Cautionnements', NULL, '2755', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2761', 'Créances diverses', NULL, '2761', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '279', 'Versements restant à effectuer sur titres immobilisés non libérés', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2801', 'Frais d''établissement (même ventilation que celle du compte 201)', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2806', 'Droit au bail', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '281', 'Amortissements des immobilisations corporelles', NULL, '281', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2814', 'Constructions sur sol d''autrui (même ventilation que celle du compte 214)', NULL, '2814', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7866', 'Reprises sur dépréciations des éléments financiers', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78662', 'Immobilisations financières', NULL, '78662', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78665', 'Valeurs mobilières de placement.', NULL, '78665', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '787', 'Reprises sur dépréciations et provisions (à inscrire dans les produits exceptionnels)', NULL, 'I', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7872', 'Reprise sur provisions réglementées (immobilisations)', NULL, 'N', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '78725', 'Amortissements dérogatoires', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7875', 'Reprises sur provisions exceptionnelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7876', 'Reprises sur dépréciations exceptionnelles', NULL, 'I', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '789', 'Report des ressources non utilisées des exercices antérieurs', NULL, 'N', 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7894', 'Report des ressources non utilisées sur subventions attribuées', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7895', 'Report des ressources non utilisées sur dons manuels reçus', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '7897', 'Report des ressources non utilisées sur legs et donations affectés', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '79', 'TRANSFERTS DE CHARGES', NULL, NULL, 
    'O', NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '791', 'Transferts de charges d''exploitation', NULL, '791', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '796', 'Transferts de charges financières', NULL, '796', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '797', 'Transferts de charges exceptionnelles', NULL, '797', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8', 'COMPTES SPÉCIAUX', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80', 'ENGAGEMENTS HORS BILAN', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '801', 'Engagements donnés et reçus', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8011', 'Engagements donnés par l''établissement (y compris fondations universitaires)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80111', 'Avals, cautions, garanties', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80116', 'Redevances crédit-bail restant à courir', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '801161', 'Crédit-bail mobilier', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '801165', 'Crédit-bail immobilier', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80118', 'Autres engagements donnés', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8012', 'Engagements reçus par l''établissement (y compris fondations universitaires)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80121', 'Avals, cautions, garanties', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80126', 'Engagements reçus pour utilisation en crédit-bail', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '801261', 'Crédit-bail mobilier', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '801265', 'crédit-bail immobilier', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80128', 'Autres engagements reçus', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '802', 'Comptes spécifiques aux fondations', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80275', 'Legs acceptés par les organes statutairement compétents', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80276', 'Legs autorisés par l''organisme de tutelle', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '80277', 'Dons en nature restant à vendre', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '803', 'Autorisations de programme', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '804', 'Engagements juridiques', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '805', 'Crédits de paiement', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '806', 'Engagements comptables annuels', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '807', 'Mandatement', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '808', 'Recettes pluriannuelles', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8081', 'Prévision annuelle du financement', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8082', 'Constatation des droits pluriannuels', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8083', 'Constatation de la part annuelle du financement', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8084', 'Encaissement attendu', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '809', 'Contrepartie des engagements', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8091', 'Donnés par l''établissement (contrepartie 801)', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8092', 'Reçus par l''établissement (contrepartie 802)', NULL, NULL, 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '84', 'Emplois des contributions volontaires en nature', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '840', 'Secours en nature', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8401', 'Alimentaires', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8402', 'Vestimentaires', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '841', 'Mise à disposition gratuite de biens', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8411', 'Locaux', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '8412', 'Matériels', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '842', 'Prestations', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '85', 'Contributions volontaires en nature', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '851', 'Prestations en nature', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '855', 'Dons en nature', NULL, NULL, 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '86', 'VALEURS INACTIVES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '861', 'Comptes de position : titres et valeurs en portefeuille', NULL, NULL, 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '862', 'Comptes de position : titres et valeurs chez les correspondants', NULL, NULL, 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '863', 'Comptes de prise en charge', NULL, NULL, 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '88', 'RÉSULTAT EN INSTANCE D''AFFECTATION (FACULTATIF)', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '89', 'BILAN (FACULTATIF)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '890', 'Bilan d''ouverture', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '891', 'Bilan de clôture', NULL, NULL, 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1', 'COMPTES DE CAPITAUX', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10', 'CAPITAL ET RÉSERVES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102', 'Biens remis en pleine propriété ou mis à disposition des établissements (hors fondations universitaires)', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1021', 'biens mis à disposition des établissements (hors fondations universitaires)', NULL, '102', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10211', 'Dotation', NULL, '1021', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102111', 'Biens remis en dotation et dont la charge du renouvellement n''incombe pas à l''établissement', NULL, '10211', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102112', 'Biens remis en dotation et dont la charge du renouvellement incombe à l''établissement', NULL, '10212', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10212', 'Complément de dotation (État)', NULL, '1022', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102121', 'Biens remis en complément de dotation et dont la charge du renouvellement n''incombe pas à l''établissement', NULL, '10221', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102122', 'Biens remis en complément de dotation et dont la charge du renouvellement incombe à l''établissement', NULL, '10222', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10213', 'Complément de dotation (autres organismes)', NULL, '1023', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102131', 'Biens remis en complément de dotation et dont la charge du renouvellement n''incombe pas à l''établissement', NULL, '10231', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102132', 'Biens remis en complément de dotation et dont la charge du renouvellement incombe à l''établissement', NULL, '10232', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10217', 'Affectation', NULL, '1027', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102171', 'Biens remis en affectation dont la charge du renouvellement n''incombe pas à l''établissement', NULL, '10271', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '102172', 'Biens remis en affectation dont la charge du renouvellement incombe à l''établissement', NULL, '10272', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1022', 'Biens remis en pleine propriété aux établissements (hors fondations universitaires)', NULL, '103', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10221', 'Fonds propres', NULL, '1031', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10222', 'Autres compléments de dotation - État', NULL, '1032', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10223', 'Autres compléments de dotation - Autres organismes', NULL, '1033', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10225', 'Dons et legs en capital', NULL, '1035', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '103', 'Fonds propres et réserves des fondations', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
COMMIT;
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1031', 'Fonds propres sans droit de reprise', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10312', 'Fonds propres et autres fonds propres', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '103121', 'Dotations pérennes représentatives d''actifs inaliénables', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1031211', 'Dotations pérennes représentatives de biens immobiliers inaliénables', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1031212', 'Dotations pérennes représentatives de biens mobiliers inaliénables', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1031213', 'Dotations pérennes représentatives d''autres actifs inaliénables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '103122', 'Dotations pérennes représentatives d''actifs aliénables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10314', 'Apports sans droit de reprise', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10315', 'Legs et donations avec contrepartie d''actifs immobilisés', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10316', 'Subventions d''investissement affectées à des biens renouvelables', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10317', 'Autres fonds propres - dotations consomptibles', NULL, 'N', 
    'O', 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '105', 'Écarts de réévaluation', NULL, '105', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1051', 'Ecart de réévaluation sur des biens sans droit de reprise', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1054', 'Ecart de réévaluation libre', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10681', 'Réserve de propre assureur', NULL, '10681', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '10688', 'Réserves diverses', NULL, 'N', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '110', 'Report à nouveau (solde créditeur)', NULL, '110', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '119', 'Report à nouveau (solde débiteur)', NULL, '119', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '12', 'RÉSULTAT DE L''EXERCICE (bénéfice ou perte)', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '129', 'Résultat de l''exercice (perte)', NULL, '129', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '131', 'Subventions d''équipement (dont subventions d''investissements affectées à un bien non renouvelable; même ventilation que celle du compte 131)', NULL, '131', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1313', 'Départements', NULL, '1313', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1315', 'Autres collectivités et établissements publics', NULL, '1315', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1317', 'Autres organismes', NULL, '1317', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '138', 'Autres subventions d''investissement', NULL, '138', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1391', 'Subventions d''équipement (dont subventions d''investissements affectées à un bien non renouvelable; même ventilation que celle du compte 1391)', NULL, '1391', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13911', 'État', NULL, '13911', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13914', 'Communes et groupements de communes', NULL, '13914', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13916', 'Union Européenne', NULL, '13916', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '13917', 'Autres organismes', NULL, '13917', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1398', 'Autres subventions d''investissement', NULL, '1398', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '15', 'PROVISIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1511', 'Provisions pour litiges', NULL, '1511', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1516', 'Provisions pour pertes sur contrats', NULL, 'N', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1518', 'Autres provisions pour risques', NULL, '1518', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '153', 'Provisions pour pensions et obligations similaires', NULL, 'N', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '157', 'Provisions pour charges à répartir sur plusieurs exercices', NULL, '157', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '158', 'Autres provisions pour charges', NULL, '158', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1581', 'Provisions pour remises en état', NULL, 'N', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '16', 'EMPRUNTS ET DETTES ASSIMILÉES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '164', 'Emprunts auprès des établissements de crédit', NULL, '164', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1651', 'Dépôts', NULL, '1651', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '167', 'Emprunts et dettes assortis de conditions particulières', NULL, '167', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1678', 'Autres', NULL, '1678', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1681', 'Autes emprunts', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1685', 'Rentes viagères capitalisées', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '17', 'DETTES RATTACHÉES A DES PARTICIPATIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '174', 'Dettes rattachées à des participations (hors groupe)', NULL, '174', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '178', 'Dettes rattachées à des sociétés en participation', NULL, '178', 
    'O', 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '185', 'Opérations de trésorerie entre l''établissement et les autres structures dotées de l''autonomie financière (SAIC, fondation universitaire, service interétablissement)', NULL, 'I', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1853', 'SIU', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '1855', 'Autres', NULL, 'N', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '187', 'Biens et prestations de services échangés (produits)', NULL, 'I', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '194', 'Fonds dédiés sur subventions de fonctionnement', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '195', 'Fonds dédiés sur dons manuels affectés', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '197', 'Fonds dédiés sur legs et donations affectées', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2', 'COMPTES D''IMMOBILISATIONS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '20', 'IMMOBILISATIONS INCORPORELLES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '201', 'Frais d''établissement', NULL, '201', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2012', 'Frais de premier établissement', NULL, 'N', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '203', 'Frais de recherche et de développement', NULL, '203', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '205', 'Concessions et droits similaires, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires', NULL, '205', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2053', 'Logiciels', NULL, '2053', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '20531', 'Logiciels acquis ou sous-traités', NULL, '20531', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '20532', 'Logiciels créés', NULL, '20532', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2058', 'Autres concessions et droits similaires, brevets, licences, marques, procédés, droits et valeurs similaires', NULL, '2058', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '206', 'Droit au bail', NULL, '206', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '208', 'Autres immobilisations incorporelles', NULL, '208', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21', 'IMMOBILISATIONS CORPORELLES', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '211', 'Terrains', NULL, '211', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2111', 'Terrains nus', NULL, '2111', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21116', 'Reçus en dotation ou en affectation', NULL, '21116', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21117', 'Acquis', NULL, '21117', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21118', 'Autres terrains nus', NULL, '21118', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2112', 'Terrains aménagés', NULL, '2112', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21126', 'Reçus en dotation ou en affectation', NULL, '21126', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21127', 'Acquis', NULL, '21127', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21128', 'Autres terrains aménagés', NULL, '21128', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2115', 'Terrains bâtis', NULL, '2115', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21156', 'Reçus en dotation ou en affectation', NULL, '21156', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21157', 'Acquis', NULL, '21157', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21158', 'Autres terrains bâtis', NULL, '21158', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '212', 'Agencements et aménagements de terrains (même ventilation que celle du compte 211)', NULL, '212', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2121', 'Agencements, aménagements de terrains nus', NULL, '2121', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21216', 'Reçus en dotation ou en affectation', NULL, '21216', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21217', 'Acquis', NULL, '21217', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2122', 'Agencements, aménagements de terrains aménagés', NULL, '2122', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21227', 'Acquis', NULL, '21227', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21228', 'Autres terrains aménagés', NULL, '21228', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21256', 'Reçus en dotation ou en affectation', NULL, '21256', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21258', 'Autres terrains bâtis', NULL, '21258', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21316', 'Bâtiments affectés ou remis en dotation', NULL, '21316', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21318', 'Autres bâtiments', NULL, '21318', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21356', 'Installations générales, agencements, aménagements des constructions affectées ou remises en dotation', NULL, '21356', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21357', 'Installations générales, agencements, aménagements des constructions acquises', NULL, '21357', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21358', 'Autres installations générales, agencements, aménagements des autres constructions', NULL, '21358', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2141', 'Bâtiments sur sol d''autrui', NULL, '2141', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21416', 'Bâtiments sur sol d''autrui affectés ou remis en dotation', NULL, '21416', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21417', 'Bâtiments sur sol d''autrui acquis', NULL, '21417', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2145', 'Installations générales, agencements, aménagements des constructions sur sol d''autrui', NULL, '2145', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21457', 'Installations générales, agencements, aménagements des constructions sur sol d''autrui acquises', NULL, '21457', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21458', 'Autres installations générales, agencements, aménagements des constructions sur sol d''autrui', NULL, '21458', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '215', 'Installations techniques, matériels et outillage', NULL, '215', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2151', 'Installations techniques complexes', NULL, '2151', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21517', 'Installations techniques complexes acquises', NULL, '21517 erreur ancienne version', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21518', 'Autres Installations techniques complexes', NULL, '21518 erreur ancienne version', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21536', 'Matériel scientifique affecté ou remis en dotation', NULL, '21536', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21538', 'Autres matériels scientifiques', NULL, '21538', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21556', 'Outillage affecté ou remis en dotation', NULL, '21556', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21558', 'Autres outillages', NULL, '21558', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21566', 'Matériel d''enseignement affecté ou remis en dotation', NULL, '21546', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21568', 'Autres matériels d''enseignement', NULL, '21548', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21576', 'Agencements et aménagements du matériel et outillage affectés ou remis en dotation', NULL, '21576', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21577', 'Agencements et aménagements du matériel et outillage acquis', NULL, '21577', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '216', 'Collections', NULL, '216', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2161', 'Collections de documentation', NULL, '2161', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21617', 'Collections acquises', NULL, '21617', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21618', 'Autres', NULL, '21618', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21646', 'Collections littéraires, scientifiques, artistiques affectées ou remises en dotation', NULL, '21646', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21647', 'Collections littéraires, scientifiques, artistiques acquises', NULL, '21647', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '218', 'Autres immobilisations corporelles', NULL, '218', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2181', 'Installations générales, agencements, aménagements divers (dans des constructions dont l''établissement n''est pas propriétaire ou affectataire ou qu''il n''a pas reçues en dotation)', NULL, '2181', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21817', 'Installations générales, agencements, aménagements divers acquis', NULL, '21817', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21818', 'Autres installations générales, agencements, aménagements divers', NULL, '21818', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2182', 'Matériel de transport', NULL, '2182', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21827', 'Matériel de transport acquis', NULL, '21827', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21828', 'Autre matériel de transport', NULL, '21828', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21837', 'Matériel de bureau acquis', NULL, '21837', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21838', 'Autre matériel de bureau', NULL, '21838', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21846', 'Mobilier affecté ou remis en dotation', NULL, '21846', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21848', 'Autre mobilier', NULL, '21848', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2187', 'Matériel informatique', NULL, '2187', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21877', 'Matériel informatique acquis', NULL, '21877', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21878', 'Autre matériel informatique', NULL, '21878', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2188', 'Matériels divers', NULL, '2188', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21886', 'Matériels divers affectés ou remis en dotation', NULL, '21886', 
    NULL, 'E', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21887', 'Matériels divers acquis', NULL, '21887', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '21888', 'Autres matériels divers', NULL, '21888', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '22', 'Immobilisations grevées de droits', NULL, 'N', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '228', 'Immobilisations grevées de droit', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '229', 'Droits des propriétaires', NULL, 'N', 
    NULL, 'F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23', 'IMMOBILISATIONS EN COURS', NULL, NULL, 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '231', 'Immobilisations corporelles en cours', NULL, '231', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2312', 'Terrains', NULL, '2312', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2313', 'Constructions', NULL, '2313', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2314', 'Constructions sur sol d''autrui', NULL, '2314', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2315', 'Installations techniques, matériel et outillage', NULL, '2315', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2318', 'Autres immobilisations corporelles', NULL, '2318', 
    'O', 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '232', 'Immobilisations incorporelles en cours', NULL, '232', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2325', 'Logiciels', NULL, '2325', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23251', 'Logiciels sous-traités', NULL, '23251', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23252', 'Logiciels créés', NULL, '23252', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '237', 'Avances et acomptes versés sur commandes d''immobilisations incorporelles', NULL, '237', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2371', 'Avances versées sur commandes d''immobilisations incorporelles', NULL, '2371', 
    NULL, 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2372', 'Acomptes versés sur commandes d''immobilisations incorporelles', NULL, '2372', 
    'O', 'E/?', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '238', 'Avances et acomptes versés sur commandes d''immobilisations corporelles', NULL, '238', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2381', 'Avances versées sur commande d''immobilisations corporelles', NULL, '2381', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23812', 'Terrains', NULL, '23812', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23813', 'Constructions', NULL, '23813', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23814', 'Constructions sur sol d''autrui', NULL, '23814', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23815', 'Installations techniques, matériel et outillage', NULL, '23815', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '23818', 'Autres immobilisations corporelles', NULL, '23818', 
    NULL, 'E/F', NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
Insert into MARACUJA.PLAN_COMPTABLE_REF_EXT
   (ref_pco_id, REF_PCO_IMPORT_ID, REF_PCO_NUM, REF_PCO_LIBELLE, REF_PCO_REMARQUES, REF_PCO_NUM_OLD, 
    REF_PCO_BUDGETAIRE, REF_PCO_UTILISATION, REF_PCO_SUBDIVISABLE, REF_PCO_DATE_DEBUT, REF_PCO_NOMENCLATURE_REF)
 Values
   (maracuja.plan_comptable_ref_ext_seq.nextval, 'M9-3 (juillet 2011)', '2382', 'Acomptes versés sur commande d''immobilisations corporelles', NULL, '2382', 
    NULL, NULL, NULL, TO_DATE('01/01/2012 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'M9-3');
COMMIT;

	
	--*******
	
	
	
	INSERT INTO MARACUJA.PARAMETRE (
   EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, 
   PAR_ORDRE, PAR_VALUE) 
select exe_ordre, 
'Nombre de jours pour la vitesse de prelevement acceleree (donné par la TG)', 
'org.cocktail.gfc.prelevement.vitesse.acceleree',
maracuja.parametre_seq.nextval,
'2'
from jefy_admin.exercice where exe_stat<>'C'; 

INSERT INTO MARACUJA.PARAMETRE (
   EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, 
   PAR_ORDRE, PAR_VALUE) 
select exe_ordre, 
'Nombre de jours pour la vitesse de prelevement normale (donné par la TG)', 
'org.cocktail.gfc.prelevement.vitesse.normale',
maracuja.parametre_seq.nextval,
'5'
from jefy_admin.exercice where exe_stat<>'C'; 
	
	
	update maracuja.gestion_exercice set pco_num_185_ctp_sacd='185' where pco_num_185 is not null;		

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.8.0';           
end;
/






