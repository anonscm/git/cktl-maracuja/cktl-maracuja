SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DDL
-- Schéma modifié :  COMPTEFI
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.8.0
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_comptefi_1880;
commit;

drop procedure grhum.inst_patch_comptefi_1880;


