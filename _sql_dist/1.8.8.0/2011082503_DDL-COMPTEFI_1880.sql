SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  COMPTEFI
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.8.0
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- modifications pour gérer les agregats
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

--grant select on maracuja.v_agregat_gestion to comptefi with grant option;

-- modifications pour compte financier
ALTER TABLE COMPTEFI.EXECUTION_BUDGET MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.CPTE_RTAT_CHARGES MODIFY(GES_CODE VARCHAR2(200 ));
ALTER TABLE COMPTEFI.CPTE_RTAT_PRODUITS MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.CAF MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.SIG MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.FRNG MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.BILAN_ACTIF MODIFY(GES_CODE VARCHAR2(200));
ALTER TABLE COMPTEFI.BILAN_PASSIF MODIFY(GES_CODE VARCHAR2(200));




CREATE OR REPLACE FUNCTION COMPTEFI.EXECUTION_BUD_AGR (exeordre number, compte VARCHAR2, agregatlib VARCHAR2, pconature char)
RETURN NUMBER

IS
 nombre NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
        IF pconature = 'D' THEN
         SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvlopdep d, maracuja.v_agregat_gestion ag
            WHERE PCO_NUM LIKE compte and d.exe_ordre =exeordre 
            and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;
     ELSE
        SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvloprec d, maracuja.v_agregat_gestion ag
            WHERE PCO_NUM LIKE compte and d.exe_ordre =exeordre 
            and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;
     END IF;

--dbms_output.put_line(nombre);

ELSE
    -- A Refaire ---
    nombre := 0;
END IF;

IF  nombre IS NULL
   THEN nombre := 0;
END IF;

RETURN nombre;
END ;
/

CREATE OR REPLACE FUNCTION COMPTEFI.Resultat_Compte_AGR (exeordre number, compte VARCHAR2, agregatlib VARCHAR2)
RETURN NUMBER

IS
 nombre NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
        CASE
     WHEN (SUBSTR(compte,1,1) = '6') OR (SUBSTR(compte,1,3) = '186') THEN
            SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO nombre
            FROM MARACUJA.CFI_TOTAUX_6_7_AVT_SOLDE_2 d, maracuja.v_agregat_gestion ag
            WHERE PCO_NUM LIKE compte and d.exe_ordre =exeordre
            and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;
     WHEN (SUBSTR(compte,1,1) = '7') OR (SUBSTR(compte,1,3) = '187') THEN
         SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO nombre
         FROM MARACUJA.CFI_TOTAUX_6_7_AVT_SOLDE_2 d, maracuja.v_agregat_gestion ag
         WHERE PCO_NUM LIKE compte and d.exe_ordre =exeordre
         and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;
     END CASE;
    -- A Refaire ---
--    nombre := 0;
END IF;

IF  nombre IS NULL
   THEN nombre := 0;
END IF;

RETURN nombre;
END ;
/

CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_agr (exeordre number, compte VARCHAR2, sens_compte CHAR, agregatlib VARCHAR2)
RETURN NUMBER

IS
 solde NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
    IF sens_compte = 'D'   THEN
         SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO solde FROM MARACUJA.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
            WHERE pco_num LIKE compte AND d.exe_ordre=exeordre
            and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;
    ELSE
     SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO solde FROM MARACUJA.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
            WHERE pco_num LIKE compte and d.exe_ordre = exeordre 
            and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=agregatlib ;   
    END IF;
ELSE
    solde := 0;
END IF;

IF solde < 0
    THEN solde := 0;
END IF;


RETURN solde;
END ;
/

CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_ext_agr (exeordre number, pco_condition VARCHAR2, sens_compte CHAR, agregatlib VARCHAR2, vue varchar2)
RETURN NUMBER

IS
solde NUMBER(12,2);
 LC$req varchar2(1000);
 lavue varchar2(50);
BEGIN

lavue := vue;
if lavue is null then
  lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVT_SOLDE_2';
end if;

IF exeordre >= 2005 THEN
   IF sens_compte = 'D'   THEN
     LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' d, maracuja.v_agregat_gestion ag WHERE  d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=''' || agregatlib || '''  and '|| pco_condition  || ' and d.exe_ordre ='||exeordre ;   
      
   ELSE
     LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' d , maracuja.v_agregat_gestion ag WHERE d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.AGREGAT=''' || agregatlib || '''  and ' || pco_condition  || ' and d.exe_ordre ='||exeordre ;
      
   END IF;
ELSE
   --- A Refaire ---
   solde := 0;
END IF;

-- dbms_output.put_line( LC$req  );

EXECUTE IMMEDIATE LC$req INTO solde ;

IF solde < 0
   THEN solde := 0;
END IF;



RETURN solde;
END ;
/


CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_Ext_Avt_S67_agr (exeordre NUMBER, listeCompte VARCHAR2, sens_solde CHAR, agregatlib VARCHAR2)
RETURN NUMBER

IS
  pco_condition VARCHAR2(500);
  lavue VARCHAR2(50);
  LC$Token VARCHAR2(20);
  i          PLS_INTEGER := 1 ;
BEGIN
     lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVT_SOLDE_2';

      LOOP
        LC$Token := Zsplit( listeCompte, i , ',') ;
        EXIT WHEN LC$Token IS NULL ;
        IF i>1 THEN
           pco_condition := pco_condition || ' or ';
        END IF;
        --dbms_output.put_line( '>' || LC$Token );
        pco_condition := pco_condition || ' pco_num like '''|| LC$Token ||'''';
           i := i + 1 ;
     END LOOP ;
     pco_condition := '('||pco_condition || ' )';
     --dbms_output.put_line( pco_condition );

        RETURN Solde_Compte_Ext_agr(exeordre, pco_condition , sens_solde , agregatlib, lavue);
END;
/










CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_EXECUTION_BUDGET_bak (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  montant NUMBER(12,2);
  montant_depenses NUMBER(12,2);
  montant_recettes NUMBER(12,2);
  ebe NUMBER(12,2);
  montant_dep NUMBER(12,2);
  montant_rec NUMBER(12,2);

  groupe1 VARCHAR2(50);
  groupe2 VARCHAR2(50);
  --groupe3 varchar2(50);

  lib_dep VARCHAR2(100);
  lib_rec VARCHAR2(100);
  --formule varchar2(1000);
  flag INTEGER;

BEGIN

IF sacd = 'O' THEN
    DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE from EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
ELSE
    DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



    SELECT COUNT(*) INTO flag FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N' AND formule = 'RTAT';

    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Vous devez calculer la CAF a partir du resultat avant de calculer le cadre 4.');
    END IF;


--************* 1ère section FONCTIONNEMENT ***********************************
   groupe1 := '1ERE SECTION : FONCTIONNEMENT';
   montant_depenses := 0;
   montant_recettes := 0;

-- Dépenses
   groupe2 := ' ';

    lib_dep := 'Charges de fonctionnement';
    montant_dep := Execution_Bud(exeordre, '60%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '61%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '62%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '63', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '634%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '635%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '636%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '637%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '638%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '639%', gescode, sacd, 'D')
        --+ Execution_Bud(exeordre, '64%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '65%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '681%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '66%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '686%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '186%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

    lib_rec := 'Produits de fonctionnement';
    montant_rec := Execution_Bud(exeordre, '70%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '71%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '72%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '74%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '781%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '791%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '75%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '76%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '786%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '796%', gescode, sacd, 'R')
            + Execution_Bud(exeordre, '187%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

-- personnel
   groupe2 := ' ';
    lib_dep := 'Charges de personnel';
    montant_dep := 
        Execution_Bud(exeordre, '631%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '632%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '633%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '64%', gescode, sacd, 'D');

    montant_depenses := montant_depenses+montant_dep;

    lib_rec := ' ';
    montant_rec :=0;
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

    lib_dep := 'Charges exceptionnelles';
    montant_dep := Execution_Bud(exeordre, '67%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '687%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

       lib_rec := 'Produits exceptionnels';
    montant_rec := Execution_Bud(exeordre, '77%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '787%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '797%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);




-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';



    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N'  AND formule = 'RTAT';
    IF montant >= 0 THEN
           montant_dep := montant;
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := ABS(montant);
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Excédent de l''exercice';
    lib_rec := 'Déficit de l''exercice';

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);



--***************** 2EME SECTION : CAPITAL *******************
    groupe1 := '2EME SECTION : OPERATIONS EN CAPITAL';
       montant_depenses := 0;
      montant_recettes := 0;

       groupe2 := ' ';

     lib_dep := 'Dépenses en capital';
    montant_dep := Execution_Bud(exeordre, '2%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '1%', gescode, sacd, 'D');
    montant_depenses := montant_depenses+montant_dep;

     lib_rec := 'Recettes en capital (1)';
    montant_rec := Execution_Bud(exeordre, '2%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '1%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '775%', gescode, sacd, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';

    -- CAF ou IAF
    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = gescode
    AND methode_ebe = 'N' AND formule = 'CAF';
    IF montant < 0 THEN
           montant_dep := ABS(montant);
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant;
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;
    lib_dep:= 'Insuffisance d''autofinancement';
    lib_rec := 'Capacité d''autofinancement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


    -- AFR ou DFR
    IF montant_depenses < montant_recettes THEN
        montant_dep := montant_recettes - montant_depenses;
        montant_rec := 0;
        montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant_depenses - montant_recettes;
        montant_dep := 0;
        montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Augmentation du fond de roulement';
    lib_rec := 'Diminution du fond de roulement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


END;
/

----------------------------------

CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_EXECUTION_BUDGET (exeordre NUMBER, AGREGAT VARCHAR2)
IS
  montant NUMBER(12,2);
  montant_depenses NUMBER(12,2);
  montant_recettes NUMBER(12,2);
  ebe NUMBER(12,2);
  montant_dep NUMBER(12,2);
  montant_rec NUMBER(12,2);

  groupe1 VARCHAR2(50);
  groupe2 VARCHAR2(50);
  --groupe3 varchar2(50);

  lib_dep VARCHAR2(100);
  lib_rec VARCHAR2(100);
  --formule varchar2(1000);
  flag INTEGER;

BEGIN

    DELETE FROM EXECUTION_BUDGET WHERE exe_ordre = exeordre AND ges_code = AGREGAT;


    SELECT COUNT(*) INTO flag FROM CAF WHERE exe_ordre = exeordre AND ges_code = AGREGAT
    AND methode_ebe = 'N' AND formule = 'RTAT';

    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'Vous devez calculer la CAF a partir du resultat avant de calculer le cadre 4.');
    END IF;


--************* 1ère section FONCTIONNEMENT ***********************************
   groupe1 := '1ERE SECTION : FONCTIONNEMENT';
   montant_depenses := 0;
   montant_recettes := 0;

-- Dépenses
   groupe2 := ' ';

    lib_dep := 'Charges de fonctionnement';
    montant_dep := Execution_Bud_Agr(exeordre, '60%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '61%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '62%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '63', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '634%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '635%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '636%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '637%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '638%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '639%', agregat, 'D')
        --+ Execution_Bud_Agr(exeordre, '64%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '65%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '681%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '66%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '686%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '186%', agregat, 'D');
    montant_depenses := montant_depenses+montant_dep;

    lib_rec := 'Produits de fonctionnement';
    montant_rec := Execution_Bud_Agr(exeordre, '70%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '71%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '72%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '74%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '781%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '791%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '75%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '76%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '786%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '796%', agregat, 'R')
            + Execution_Bud_Agr(exeordre, '187%', agregat, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

-- personnel
   groupe2 := ' ';
    lib_dep := 'Charges de personnel';
    montant_dep := 
        Execution_Bud_Agr(exeordre, '631%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '632%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '633%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '64%', agregat, 'D');

    montant_depenses := montant_depenses+montant_dep;

    lib_rec := ' ';
    montant_rec :=0;
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);

    lib_dep := 'Charges exceptionnelles';
    montant_dep := Execution_Bud_Agr(exeordre, '67%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '687%', agregat, 'D');
    montant_depenses := montant_depenses+montant_dep;

       lib_rec := 'Produits exceptionnels';
    montant_rec := Execution_Bud_Agr(exeordre, '77%', agregat, 'R')
        + Execution_Bud_Agr(exeordre, '787%', agregat, 'R')
        + Execution_Bud_Agr(exeordre, '797%', agregat, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);




-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';



    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = agregat
    AND methode_ebe = 'N'  AND formule = 'RTAT';
    IF montant >= 0 THEN
           montant_dep := montant;
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := ABS(montant);
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Excédent de l''exercice';
    lib_rec := 'Déficit de l''exercice';

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);



--***************** 2EME SECTION : CAPITAL *******************
    groupe1 := '2EME SECTION : OPERATIONS EN CAPITAL';
       montant_depenses := 0;
      montant_recettes := 0;

       groupe2 := ' ';

     lib_dep := 'Dépenses en capital';
    montant_dep := Execution_Bud_Agr(exeordre, '2%', agregat, 'D')
        + Execution_Bud_Agr(exeordre, '1%', agregat, 'D');
    montant_depenses := montant_depenses+montant_dep;

     lib_rec := 'Recettes en capital (1)';
    montant_rec := Execution_Bud_Agr(exeordre, '2%', agregat, 'R')
        + Execution_Bud_Agr(exeordre, '1%', agregat, 'R')
        + Execution_Bud_Agr(exeordre, '775%', agregat, 'R');
    montant_recettes := montant_recettes+montant_rec;

    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


-- Equilibre
    groupe2 := 'MODE DE REALISATION DE L''EQUILIBRE';

    -- CAF ou IAF
    SELECT NVL(caf_montant,0) INTO montant FROM CAF WHERE exe_ordre = exeordre AND ges_code = agregat
    AND methode_ebe = 'N' AND formule = 'CAF';
    IF montant < 0 THEN
           montant_dep := ABS(montant);
        montant_rec := 0;
           montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant;
        montant_dep := 0;
           montant_recettes := montant_recettes+montant_rec;
    END IF;
    lib_dep:= 'Insuffisance d''autofinancement';
    lib_rec := 'Capacité d''autofinancement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


    -- AFR ou DFR
    IF montant_depenses < montant_recettes THEN
        montant_dep := montant_recettes - montant_depenses;
        montant_rec := 0;
        montant_depenses := montant_depenses+montant_dep;
    ELSE
        montant_rec := montant_depenses - montant_recettes;
        montant_dep := 0;
        montant_recettes := montant_recettes+montant_rec;
    END IF;

    lib_dep := 'Augmentation du fond de roulement';
    lib_rec := 'Diminution du fond de roulement';
    INSERT INTO EXECUTION_BUDGET VALUES (EXBUD_SEQ.NEXTVAL, exeordre, agregat,  groupe1, groupe2, lib_dep, montant_dep, lib_rec, montant_rec);


END;
/

-------
-------
CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_CPTE_RTAT_bak (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);
  benef NUMBER(12,2);
  perte NUMBER(12,2);
  benefant NUMBER(12,2);
  perteant NUMBER(12,2);

  -- version du 03/03/2006 - Prestations Internes

BEGIN

--*************** CHARGES *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = gescode;
--ELSE
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

IF sacd = 'O' THEN
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


benef := 0;
perte := 0;
benefant := 0;
perteant := 0;

lib1 := 'I';

----- CHARGES EXPLOITATION ----
lib2:= 'CHARGES D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte(exeordre, '607%', gescode, sacd)
        + resultat_compte(exeordre, '6087%', gescode, sacd)
        + resultat_compte(exeordre, '6097%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '607%', gescode, sacd)
        + resultat_compte(exeordre-1, '6087%', gescode, sacd)
        + resultat_compte(exeordre-1, '6097%', gescode, sacd);
    lib := 'Achats de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Variation marchandises ---
    total := resultat_compte(exeordre, '6037%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6037%', gescode, sacd);
    lib := 'Variation de stocks de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CONSOMMATION EXERCICE ----
lib2:= 'CONSOMMATION DE L''EXERCICE EN PROVENANCE DES TIERS';

-- Achats matières 1ères ---
    total := resultat_compte(exeordre, '601%', gescode, sacd)
        + resultat_compte(exeordre, '6081%', gescode, sacd)
        + resultat_compte(exeordre, '6091%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '601%', gescode, sacd)
        + resultat_compte(exeordre-1, '6081%', gescode, sacd)
        + resultat_compte(exeordre-1, '6091%', gescode, sacd);
    lib := 'Achats stockés - Matières premières';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats approvisionnements ---
    total := resultat_compte(exeordre, '602%', gescode, sacd)
        + resultat_compte(exeordre, '6082%', gescode, sacd)
        + resultat_compte(exeordre, '6092%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '602%', gescode, sacd)
        + resultat_compte(exeordre-1, '6082%', gescode, sacd)
        + resultat_compte(exeordre-1, '6092%', gescode, sacd);
    lib := 'Achats stockés - Autres approvisionnements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- variation ---
    total := resultat_compte(exeordre, '6031%', gescode, sacd)
        + resultat_compte(exeordre, '6032%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6031%', gescode, sacd)
        + resultat_compte(exeordre-1, '6032%', gescode, sacd);
    lib := 'Variation de stocks d''approvisionnement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats sous-traitance ---
    total := resultat_compte(exeordre, '604%', gescode, sacd)
        + resultat_compte(exeordre, '6084%', gescode, sacd)
        + resultat_compte(exeordre, '6094%', gescode, sacd)
        + resultat_compte(exeordre, '605%', gescode, sacd)
        + resultat_compte(exeordre, '6085%', gescode, sacd)
        + resultat_compte(exeordre, '6095%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '604%', gescode, sacd)
        + resultat_compte(exeordre-1, '6084%', gescode, sacd)
        + resultat_compte(exeordre-1, '6094%', gescode, sacd)
        + resultat_compte(exeordre-1, '605%', gescode, sacd)
        + resultat_compte(exeordre-1, '6085%', gescode, sacd)
        + resultat_compte(exeordre-1, '6095%', gescode, sacd);
    lib := 'Achats de sous-traitance';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats matériel et fournitures ---
    total := resultat_compte(exeordre, '606%', gescode, sacd)
        + resultat_compte(exeordre, '6086%', gescode, sacd)
        + resultat_compte(exeordre, '6096%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '606%', gescode, sacd)
        + resultat_compte(exeordre-1, '6086%', gescode, sacd)
        + resultat_compte(exeordre-1, '6096%', gescode, sacd);
    lib := 'Achats non stockés de matières et de fournitures';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces extérieurs  Prsel intérim ---
    total := resultat_compte(exeordre, '6211%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6211%', gescode, sacd);
    lib := 'Services extérieurs : Personnel intérimaire';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces extérieurs loyers crédit-bail ---
    total := resultat_compte(exeordre, '612%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '612%', gescode, sacd);
    lib := 'Services extérieurs : Loyers en crédit-bail';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres Sces extérieurs ---
    total := resultat_compte(exeordre, '61%', gescode, sacd)
        + resultat_compte(exeordre, '62%', gescode, sacd)
        -(resultat_compte(exeordre, '612%', gescode, sacd)
        + resultat_compte(exeordre, '6211%', gescode, sacd))
        + resultat_compte(exeordre, '619%', gescode, sacd)
        + resultat_compte(exeordre, '629%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '61%', gescode, sacd)
        + resultat_compte(exeordre-1, '62%', gescode, sacd)
        -(resultat_compte(exeordre-1, '612%', gescode, sacd)
        + resultat_compte(exeordre-1, '6211%', gescode, sacd))
        + resultat_compte(exeordre-1, '619%', gescode, sacd)
        + resultat_compte(exeordre-1, '629%', gescode, sacd);
    lib := 'Autres services extérieurs';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- IMPOTS----
lib2:= 'IMPOTS, TAXES ET VERSEMENTS ASSIMILES';

-- Sur rémunérations ---
    total := resultat_compte(exeordre, '631%', gescode, sacd)
        + resultat_compte(exeordre, '632%', gescode, sacd)
        + resultat_compte(exeordre, '633%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '631%', gescode, sacd)
        + resultat_compte(exeordre-1, '632%', gescode, sacd)
        + resultat_compte(exeordre-1, '633%', gescode, sacd);
    lib := 'Sur rémunérations';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres ---
    total := resultat_compte(exeordre, '635%', gescode, sacd)
        + resultat_compte(exeordre, '637%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '635%', gescode, sacd)
        + resultat_compte(exeordre-1, '637%', gescode, sacd);
    lib := 'Autres impôts, taxes et versements assimilés';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CHARGES PERSONNEL ----
lib2:= 'CHARGES DE PERSONNEL';

-- Salaire ---
    total := resultat_compte(exeordre, '641%', gescode, sacd)
        + resultat_compte(exeordre, '642%', gescode, sacd)
        + resultat_compte(exeordre, '643%', gescode, sacd)
        + resultat_compte(exeordre, '644%', gescode, sacd)
        + resultat_compte(exeordre, '646%', gescode, sacd)
        + resultat_compte(exeordre, '648%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '641%', gescode, sacd)
        + resultat_compte(exeordre-1, '642%', gescode, sacd)
        + resultat_compte(exeordre-1, '643%', gescode, sacd)
        + resultat_compte(exeordre-1, '644%', gescode, sacd)
        + resultat_compte(exeordre-1, '646%', gescode, sacd)
        + resultat_compte(exeordre-1, '648%', gescode, sacd);
    lib := 'Salaires et traitements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sociales ---
    total := resultat_compte(exeordre, '645%', gescode, sacd)
        + resultat_compte(exeordre, '647%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '645%', gescode, sacd)
        + resultat_compte(exeordre-1, '647%', gescode, sacd);
    lib := 'Charges sociales';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- AMORTISSEMENTS ET PROVISIONS ----
lib2:= 'DOTATIONS AUX AMORTISSEMENTS ET AUX PROVISIONS';

-- Amort sur Immobilisations ---
    total := resultat_compte(exeordre, '6811%', gescode, sacd)
        + resultat_compte(exeordre, '6812%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6811%', gescode, sacd)
        + resultat_compte(exeordre-1, '6812%', gescode, sacd);
    lib := 'Sur immobilisations : Dotations aux amortissements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Immobilisations ---
    total := resultat_compte(exeordre, '6816%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6816%', gescode, sacd);
    lib := 'Sur immobilisations : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Actifs circulants ---
    total := resultat_compte(exeordre, '6817%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6817%', gescode, sacd);
    lib := 'Sur actif circulant : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov pour Risques et Charges ---
    total := resultat_compte(exeordre, '6815%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '6815%', gescode, sacd);
    lib := 'Pour risques et charges : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- AUTRES CHARGES ----
lib2:= 'AUTRES CHARGES';

-- Autres charges ---
    total := resultat_compte(exeordre, '65%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '65%', gescode, sacd);
    lib := 'Autres charges de gestion courante' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations internes ---
    total := resultat_compte(exeordre, '186%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '186%', gescode, sacd);
    lib := 'Charges de Prestations Internes' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);


--********************************************
lib1 := 'II';

---- CHARGES FINANCIERES----
lib2:= 'CHARGES FINANCIERES';

-- Amort et provisions ---
    total := resultat_compte(exeordre, '686%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '686%', gescode, sacd);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Intérêts ---
    total := resultat_compte(exeordre, '661%', gescode, sacd)
        + resultat_compte(exeordre, '664%', gescode, sacd)
        + resultat_compte(exeordre, '665%', gescode, sacd)
        + resultat_compte(exeordre, '668%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '661%', gescode, sacd)
        + resultat_compte(exeordre-1, '664%', gescode, sacd)
        + resultat_compte(exeordre-1, '665%', gescode, sacd)
        + resultat_compte(exeordre-1, '668%', gescode, sacd);
    lib := 'Intérêts et charges assimilées';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Perte de change---
    total := resultat_compte(exeordre, '666%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '666%', gescode, sacd);
    lib := 'Différence négative de change';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sur cession VMP ---
    total := resultat_compte(exeordre, '667%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '667%', gescode, sacd);
    lib := 'Charges nettes sur cessions de valeurs mobilières de placement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- CHARGES EXCEPTIONNELLES ----
lib2:= 'CHARGES EXCEPTIONNELLES';

-- Opérations de gestion ---
    total := resultat_compte(exeordre, '671%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '671%', gescode, sacd);
    lib := 'Sur opération de gestion';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Opérations en capital VC élement actifs cédés ---
    total := resultat_compte(exeordre, '675%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
    lib := 'Sur opération en capital : Valeur comptable des éléments d''actif cédés';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres Opérations ---
    total := resultat_compte(exeordre, '678%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '678%', gescode, sacd);
    lib := 'Sur autres opérations en capital';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et Provisions ---
    total := resultat_compte(exeordre, '687%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '687%', gescode, sacd);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- IMPOTS SUR BENEF----
lib2:= 'IMPOTS SUR LES BENEFICES';

-- impots sur les bénéfices ---
    total := resultat_compte(exeordre, '69%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '69%', gescode, sacd);
    lib := '  ';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Bénéfice ou Perte --
lib1 := 'III';
lib2 := '  ';
    IF sacd = 'O' THEN
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre-1;
    ELSif sacd = 'G' then
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre-1;        
    ELSE
        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre-1;
    END IF;
    IF total >= 0 THEN
        benef := total;
        ELSE perte := -total;
    END IF;
    IF totalant >= 0 THEN
        benefant := totalant;
        ELSE perteant := -totalant;
    END IF;
    lib := 'SOLDE CREDITEUR = BENEFICE';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, benef, benefant, gescode, exeordre);


--*************** PRODUITS *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = gescode;
--ELSE
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

IF sacd = 'O' THEN
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;

lib1 := 'I';

----- PRODUITS EXPLOITATION ----
lib2:= 'PRODUITS D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte(exeordre, '707%', gescode, sacd)
        + resultat_compte(exeordre, '7097%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '707%', gescode, sacd)
        + resultat_compte(exeordre-1, '7097%', gescode, sacd);
    lib := 'Vente de marchandises';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Ventes ---
    total := resultat_compte(exeordre, '701%', gescode, sacd)
        + resultat_compte(exeordre, '702%', gescode, sacd)
        + resultat_compte(exeordre, '703%', gescode, sacd)
        + resultat_compte(exeordre, '7091%', gescode, sacd)
        + resultat_compte(exeordre, '7092%', gescode, sacd)
        + resultat_compte(exeordre, '7093%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '701%', gescode, sacd)
        + resultat_compte(exeordre-1, '702%', gescode, sacd)
        + resultat_compte(exeordre-1, '703%', gescode, sacd)
        + resultat_compte(exeordre-1, '7091%', gescode, sacd)
        + resultat_compte(exeordre-1, '7092%', gescode, sacd)
        + resultat_compte(exeordre-1, '7093%', gescode, sacd);
    lib := 'Production vendue : Ventes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Travaux ---
    total := resultat_compte(exeordre, '704%', gescode, sacd)
        + resultat_compte(exeordre, '7094%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '704%', gescode, sacd)
        + resultat_compte(exeordre-1, '7094%', gescode, sacd);
    lib := 'Production vendue : Travaux';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations ---
    total := resultat_compte(exeordre, '705%', gescode, sacd)
        + resultat_compte(exeordre, '706%', gescode, sacd)
        + resultat_compte(exeordre, '708%', gescode, sacd)
        + resultat_compte(exeordre, '7095%', gescode, sacd)
        + resultat_compte(exeordre, '7096%', gescode, sacd)
        + resultat_compte(exeordre, '7098%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '705%', gescode, sacd)
        + resultat_compte(exeordre-1, '706%', gescode, sacd)
        + resultat_compte(exeordre-1, '708%', gescode, sacd)
        + resultat_compte(exeordre-1, '7095%', gescode, sacd)
        + resultat_compte(exeordre-1, '7096%', gescode, sacd)
        + resultat_compte(exeordre-1, '7098%', gescode, sacd);
    lib := 'Production vendue : Prestations de services, études et activités annexes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Biens en-cours ---
    total := resultat_compte(exeordre, '7133%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7133%', gescode, sacd);
    lib := 'Production stockée : En-cours de production de biens';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Services en-cours ---
    total := resultat_compte(exeordre, '7134%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7134%', gescode, sacd);
    lib := 'Production stockée : En-cours de production de services';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Produits ---
    total := resultat_compte(exeordre, '7135%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '7135%', gescode, sacd);
    lib := 'Production stockée : Produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Production Immobilisée ---
    total := resultat_compte(exeordre, '72%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '72%', gescode, sacd);
    lib := 'Production immobilisée';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Subventions exploitation ---
    total := resultat_compte(exeordre, '74%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '74%', gescode, sacd);
    lib := 'Subventions d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et provisions ---
    total := resultat_compte(exeordre, '781%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '781%', gescode, sacd);
    lib := 'Reprise sur amortissements et sur provisions';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Transfert de charges  ---
    total := resultat_compte(exeordre, '791%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '791%', gescode, sacd);
    lib := 'Transferts de charges d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres produits  ---
    total := resultat_compte(exeordre, '75%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '75%', gescode, sacd);
    lib := 'Autres produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Prestations internes ---
    total := resultat_compte(exeordre, '187%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '187%', gescode, sacd);
    lib := 'Produits de Prestations internes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);


lib1 := 'II';

----- PRODUITS FINANCIERS ----
lib2:= 'PRODUITS FINANCIERS';

-- Participations ---
    total := resultat_compte(exeordre, '761%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '761%', gescode, sacd);
    lib := 'De participation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres VM et créances immobilisées ---
    total := resultat_compte(exeordre, '762%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '762%', gescode, sacd);
    lib := 'D''autres valeurs mobilières et créances de l''actif immobilisé';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres produits ---
    total := resultat_compte(exeordre, '763%', gescode, sacd)
        + resultat_compte(exeordre, '764%', gescode, sacd)
        + resultat_compte(exeordre, '765%', gescode, sacd)
        + resultat_compte(exeordre, '768%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '763%', gescode, sacd)
        + resultat_compte(exeordre-1, '764%', gescode, sacd)
        + resultat_compte(exeordre-1, '765%', gescode, sacd)
        + resultat_compte(exeordre-1, '768%', gescode, sacd);
    lib := 'Autres intérêts et produits assimilés';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte(exeordre, '786%', gescode, sacd)
        + resultat_compte(exeordre, '796%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '786%', gescode, sacd)
        + resultat_compte(exeordre-1, '796%', gescode, sacd);
    lib := 'Reprise sur provisions et transferts de charges financières';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Gain de change ---
    total := resultat_compte(exeordre, '766%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '766%', gescode, sacd);
    lib := 'Différence positive de change';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Pduits sur cession VMP ---
    total := resultat_compte(exeordre, '767%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '767%', gescode, sacd);
    lib := 'Produits nets sur cession de valeurs mobilières de placement';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- PRODUITS EXCEPTIONNELS ----
lib2:= 'PRODUITS EXCEPTIONNELS';

-- Opération de gestion ---
    total := resultat_compte(exeordre, '771%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '771%', gescode, sacd);
    lib := 'Sur opération de gestion';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital cession éléménts actifs ---
    total := resultat_compte(exeordre, '775%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
    lib := 'Sur opération en capital : Produits des cessions d''élements d''actif';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital subvention exploitation ---
    total := resultat_compte(exeordre, '777%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
    lib := 'Sur opération en capital : Subventions d''investissement virées au résultat';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital AUTRES ---
    total := resultat_compte(exeordre, '778%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '778%', gescode, sacd);
    lib := 'Sur autres opérations en capital';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Neutralisation des amortissements ---
    total := resultat_compte(exeordre, '776%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
    lib := 'Neutralisation des amortissements';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte(exeordre, '787%', gescode, sacd)
        + resultat_compte(exeordre, '797%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '787%', gescode, sacd)
        + resultat_compte(exeordre-1, '797%', gescode, sacd);
    lib := 'Reprise sur provisions et transferts de charges exceptionnelles';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Bénéfice ou Perte --
lib1 := 'III';
lib2 := '  ';
lib := 'SOLDE DEBITEUR = PERTE';
INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, perte, perteant, gescode, exeordre);

END;
/

--********************
CREATE OR REPLACE PROCEDURE COMPTEFI.PREPARE_CPTE_RTAT (exeordre NUMBER, agregatlib VARCHAR2)
IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);
  benef NUMBER(12,2);
  perte NUMBER(12,2);
  benefant NUMBER(12,2);
  perteant NUMBER(12,2);

  

BEGIN

--*************** CHARGES *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--ELSE
--    DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

DELETE CPTE_RTAT_CHARGES WHERE exe_ordre = exeordre AND ges_code = agregatlib;

benef := 0;
perte := 0;
benefant := 0;
perteant := 0;

lib1 := 'I';

----- CHARGES EXPLOITATION ----
lib2:= 'CHARGES D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte_agr(exeordre, '607%', agregatlib)
        + resultat_compte_agr(exeordre, '6087%', agregatlib)
        + resultat_compte_agr(exeordre, '6097%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '607%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6087%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6097%', agregatlib);
    lib := 'Achats de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Variation marchandises ---
    total := resultat_compte_agr(exeordre, '6037%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6037%', agregatlib);
    lib := 'Variation de stocks de marchandises';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

----- CONSOMMATION EXERCICE ----
lib2:= 'CONSOMMATION DE L''EXERCICE EN PROVENANCE DES TIERS';

-- Achats matières 1ères ---
    total := resultat_compte_agr(exeordre, '601%', agregatlib)
        + resultat_compte_agr(exeordre, '6081%', agregatlib)
        + resultat_compte_agr(exeordre, '6091%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '601%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6081%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6091%', agregatlib);
    lib := 'Achats stockés - Matières premières';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Achats approvisionnements ---
    total := resultat_compte_agr(exeordre, '602%', agregatlib)
        + resultat_compte_agr(exeordre, '6082%', agregatlib)
        + resultat_compte_agr(exeordre, '6092%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '602%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6082%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6092%', agregatlib);
    lib := 'Achats stockés - Autres approvisionnements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- variation ---
    total := resultat_compte_agr(exeordre, '6031%', agregatlib)
        + resultat_compte_agr(exeordre, '6032%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6031%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6032%', agregatlib);
    lib := 'Variation de stocks d''approvisionnement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Achats sous-traitance ---
    total := resultat_compte_agr(exeordre, '604%', agregatlib)
        + resultat_compte_agr(exeordre, '6084%', agregatlib)
        + resultat_compte_agr(exeordre, '6094%', agregatlib)
        + resultat_compte_agr(exeordre, '605%', agregatlib)
        + resultat_compte_agr(exeordre, '6085%', agregatlib)
        + resultat_compte_agr(exeordre, '6095%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '604%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6084%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6094%', agregatlib)
        + resultat_compte_agr(exeordre-1, '605%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6085%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6095%', agregatlib);
    lib := 'Achats de sous-traitance';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Achats matériel et fournitures ---
    total := resultat_compte_agr(exeordre, '606%', agregatlib)
        + resultat_compte_agr(exeordre, '6086%', agregatlib)
        + resultat_compte_agr(exeordre, '6096%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '606%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6086%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6096%', agregatlib);
    lib := 'Achats non stockés de matières et de fournitures';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

--  Sces extérieurs  Prsel intérim ---
    total := resultat_compte_agr(exeordre, '6211%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6211%', agregatlib);
    lib := 'Services extérieurs : Personnel intérimaire';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

--  Sces extérieurs loyers crédit-bail ---
    total := resultat_compte_agr(exeordre, '612%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '612%', agregatlib);
    lib := 'Services extérieurs : Loyers en crédit-bail';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

--  Autres Sces extérieurs ---
    total := resultat_compte_agr(exeordre, '61%', agregatlib)
        + resultat_compte_agr(exeordre, '62%', agregatlib)
        -(resultat_compte_agr(exeordre, '612%', agregatlib)
        + resultat_compte_agr(exeordre, '6211%', agregatlib))
        + resultat_compte_agr(exeordre, '619%', agregatlib)
        + resultat_compte_agr(exeordre, '629%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '61%', agregatlib)
        + resultat_compte_agr(exeordre-1, '62%', agregatlib)
        -(resultat_compte_agr(exeordre-1, '612%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6211%', agregatlib))
        + resultat_compte_agr(exeordre-1, '619%', agregatlib)
        + resultat_compte_agr(exeordre-1, '629%', agregatlib);
    lib := 'Autres services extérieurs';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

----- IMPOTS----
lib2:= 'IMPOTS, TAXES ET VERSEMENTS ASSIMILES';

-- Sur rémunérations ---
    total := resultat_compte_agr(exeordre, '631%', agregatlib)
        + resultat_compte_agr(exeordre, '632%', agregatlib)
        + resultat_compte_agr(exeordre, '633%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '631%', agregatlib)
        + resultat_compte_agr(exeordre-1, '632%', agregatlib)
        + resultat_compte_agr(exeordre-1, '633%', agregatlib);
    lib := 'Sur rémunérations';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Autres ---
    total := resultat_compte_agr(exeordre, '635%', agregatlib)
        + resultat_compte_agr(exeordre, '637%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '635%', agregatlib)
        + resultat_compte_agr(exeordre-1, '637%', agregatlib);
    lib := 'Autres impôts, taxes et versements assimilés';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

----- CHARGES PERSONNEL ----
lib2:= 'CHARGES DE PERSONNEL';

-- Salaire ---
    total := resultat_compte_agr(exeordre, '641%', agregatlib)
        + resultat_compte_agr(exeordre, '642%', agregatlib)
        + resultat_compte_agr(exeordre, '643%', agregatlib)
        + resultat_compte_agr(exeordre, '644%', agregatlib)
        + resultat_compte_agr(exeordre, '646%', agregatlib)
        + resultat_compte_agr(exeordre, '648%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '641%', agregatlib)
        + resultat_compte_agr(exeordre-1, '642%', agregatlib)
        + resultat_compte_agr(exeordre-1, '643%', agregatlib)
        + resultat_compte_agr(exeordre-1, '644%', agregatlib)
        + resultat_compte_agr(exeordre-1, '646%', agregatlib)
        + resultat_compte_agr(exeordre-1, '648%', agregatlib);
    lib := 'Salaires et traitements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Charges sociales ---
    total := resultat_compte_agr(exeordre, '645%', agregatlib)
        + resultat_compte_agr(exeordre, '647%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '645%', agregatlib)
        + resultat_compte_agr(exeordre-1, '647%', agregatlib);
    lib := 'Charges sociales';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

----- AMORTISSEMENTS ET PROVISIONS ----
lib2:= 'DOTATIONS AUX AMORTISSEMENTS ET AUX PROVISIONS';

-- Amort sur Immobilisations ---
    total := resultat_compte_agr(exeordre, '6811%', agregatlib)
        + resultat_compte_agr(exeordre, '6812%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6811%', agregatlib)
        + resultat_compte_agr(exeordre-1, '6812%', agregatlib);
    lib := 'Sur immobilisations : Dotations aux amortissements';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Prov sur Immobilisations ---
    total := resultat_compte_agr(exeordre, '6816%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6816%', agregatlib);
    lib := 'Sur immobilisations : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Prov sur Actifs circulants ---
    total := resultat_compte_agr(exeordre, '6817%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6817%', agregatlib);
    lib := 'Sur actif circulant : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Prov pour Risques et Charges ---
    total := resultat_compte_agr(exeordre, '6815%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '6815%', agregatlib);
    lib := 'Pour risques et charges : Dotations aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

---- AUTRES CHARGES ----
lib2:= 'AUTRES CHARGES';

-- Autres charges ---
    total := resultat_compte_agr(exeordre, '65%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '65%', agregatlib);
    lib := 'Autres charges de gestion courante' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Prestations internes ---
    total := resultat_compte_agr(exeordre, '186%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '186%', agregatlib);
    lib := 'Charges de Prestations Internes' ;
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);


--********************************************
lib1 := 'II';

---- CHARGES FINANCIERES----
lib2:= 'CHARGES FINANCIERES';

-- Amort et provisions ---
    total := resultat_compte_agr(exeordre, '686%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '686%', agregatlib);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Intérêts ---
    total := resultat_compte_agr(exeordre, '661%', agregatlib)
        + resultat_compte_agr(exeordre, '664%', agregatlib)
        + resultat_compte_agr(exeordre, '665%', agregatlib)
        + resultat_compte_agr(exeordre, '668%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '661%', agregatlib)
        + resultat_compte_agr(exeordre-1, '664%', agregatlib)
        + resultat_compte_agr(exeordre-1, '665%', agregatlib)
        + resultat_compte_agr(exeordre-1, '668%', agregatlib);
    lib := 'Intérêts et charges assimilées';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Perte de change---
    total := resultat_compte_agr(exeordre, '666%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '666%', agregatlib);
    lib := 'Différence négative de change';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Charges sur cession VMP ---
    total := resultat_compte_agr(exeordre, '667%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '667%', agregatlib);
    lib := 'Charges nettes sur cessions de valeurs mobilières de placement';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

---- CHARGES EXCEPTIONNELLES ----
lib2:= 'CHARGES EXCEPTIONNELLES';

-- Opérations de gestion ---
    total := resultat_compte_agr(exeordre, '671%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '671%', agregatlib);
    lib := 'Sur opération de gestion';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Opérations en capital VC élement actifs cédés ---
    total := resultat_compte_agr(exeordre, '675%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '675%', agregatlib);
    lib := 'Sur opération en capital : Valeur comptable des éléments d''actif cédés';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Autres Opérations ---
    total := resultat_compte_agr(exeordre, '678%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '678%', agregatlib);
    lib := 'Sur autres opérations en capital';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Amort et Provisions ---
    total := resultat_compte_agr(exeordre, '687%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '687%', agregatlib);
    lib := 'Dotations aux amortissements et aux provisions';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

---- IMPOTS SUR BENEF----
lib2:= 'IMPOTS SUR LES BENEFICES';

-- impots sur les bénéfices ---
    total := resultat_compte_agr(exeordre, '69%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '69%', agregatlib);
    lib := '  ';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Bénéfice ou Perte --
lib1 := 'III';
lib2 := '  ';

       SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
        WHERE (pco_num = '120' OR pco_num = '129') AND d.exe_ordre = exeordre
        and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.agregat=agregatlib ;


        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures_2 d , maracuja.v_agregat_gestion ag
        WHERE (pco_num = '120' OR pco_num = '129') AND d.exe_ordre = exeordre-1
         and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.agregat=agregatlib ;


--    IF sacd = 'O' THEN
--        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures_2 d, maracuja.v_agregatlib_gestion ag
--        WHERE (pco_num = '120' OR pco_num = '129') AND d.ges_code = agregatlib AND d.exe_ordre = exeordre
--        and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.agregatlib=agregatliblib ;
--        

--        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = agregatlib AND exe_ordre = exeordre-1;
--    ELSif sacd = 'G' then
--        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
--        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre-1;        
--    ELSE
--        SELECT SUM(credit)- SUM(debit) INTO total FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
--        SELECT SUM(credit)-SUM(debit) INTO totalant FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre-1;
--    END IF;
    IF total >= 0 THEN
        benef := total;
        ELSE perte := -total;
    END IF;
    IF totalant >= 0 THEN
        benefant := totalant;
        ELSE perteant := -totalant;
    END IF;
    lib := 'SOLDE CREDITEUR = BENEFICE';
    INSERT INTO cpte_rtat_charges VALUES (seq_cr_charges.NEXTVAL, lib1, lib2, lib, benef, benefant, agregatlib, exeordre);


--*************** PRODUITS *********************************
--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--ELSE
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;

--IF sacd = 'O' THEN
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--ELSif sacd = 'G' then
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
--else
--    DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;


 DELETE CPTE_RTAT_PRODUITS WHERE exe_ordre = exeordre AND ges_code = agregatlib;

lib1 := 'I';

----- PRODUITS EXPLOITATION ----
lib2:= 'PRODUITS D''EXPLOITATION';

-- Marchandises ---
    total := resultat_compte_agr(exeordre, '707%', agregatlib)
        + resultat_compte_agr(exeordre, '7097%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '707%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7097%', agregatlib);
    lib := 'Vente de marchandises';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Ventes ---
    total := resultat_compte_agr(exeordre, '701%', agregatlib)
        + resultat_compte_agr(exeordre, '702%', agregatlib)
        + resultat_compte_agr(exeordre, '703%', agregatlib)
        + resultat_compte_agr(exeordre, '7091%', agregatlib)
        + resultat_compte_agr(exeordre, '7092%', agregatlib)
        + resultat_compte_agr(exeordre, '7093%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '701%', agregatlib)
        + resultat_compte_agr(exeordre-1, '702%', agregatlib)
        + resultat_compte_agr(exeordre-1, '703%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7091%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7092%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7093%', agregatlib);
    lib := 'Production vendue : Ventes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Travaux ---
    total := resultat_compte_agr(exeordre, '704%', agregatlib)
        + resultat_compte_agr(exeordre, '7094%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '704%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7094%', agregatlib);
    lib := 'Production vendue : Travaux';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Prestations ---
    total := resultat_compte_agr(exeordre, '705%', agregatlib)
        + resultat_compte_agr(exeordre, '706%', agregatlib)
        + resultat_compte_agr(exeordre, '708%', agregatlib)
        + resultat_compte_agr(exeordre, '7095%', agregatlib)
        + resultat_compte_agr(exeordre, '7096%', agregatlib)
        + resultat_compte_agr(exeordre, '7098%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '705%', agregatlib)
        + resultat_compte_agr(exeordre-1, '706%', agregatlib)
        + resultat_compte_agr(exeordre-1, '708%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7095%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7096%', agregatlib)
        + resultat_compte_agr(exeordre-1, '7098%', agregatlib);
    lib := 'Production vendue : Prestations de services, études et activités annexes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Stocks Biens en-cours ---
    total := resultat_compte_agr(exeordre, '7133%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '7133%', agregatlib);
    lib := 'Production stockée : En-cours de production de biens';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Stocks Services en-cours ---
    total := resultat_compte_agr(exeordre, '7134%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '7134%', agregatlib);
    lib := 'Production stockée : En-cours de production de services';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Stocks Produits ---
    total := resultat_compte_agr(exeordre, '7135%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '7135%', agregatlib);
    lib := 'Production stockée : Produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Production Immobilisée ---
    total := resultat_compte_agr(exeordre, '72%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '72%', agregatlib);
    lib := 'Production immobilisée';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Subventions exploitation ---
    total := resultat_compte_agr(exeordre, '74%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '74%', agregatlib);
    lib := 'Subventions d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Amort et provisions ---
    total := resultat_compte_agr(exeordre, '781%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '781%', agregatlib);
    lib := 'Reprise sur amortissements et sur provisions';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Transfert de charges  ---
    total := resultat_compte_agr(exeordre, '791%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '791%', agregatlib);
    lib := 'Transferts de charges d''exploitation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

--  Autres produits  ---
    total := resultat_compte_agr(exeordre, '75%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '75%', agregatlib);
    lib := 'Autres produits';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

--  Prestations internes ---
    total := resultat_compte_agr(exeordre, '187%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '187%', agregatlib);
    lib := 'Produits de Prestations internes';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);


lib1 := 'II';

----- PRODUITS FINANCIERS ----
lib2:= 'PRODUITS FINANCIERS';

-- Participations ---
    total := resultat_compte_agr(exeordre, '761%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '761%', agregatlib);
    lib := 'De participation';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Autres VM et créances immobilisées ---
    total := resultat_compte_agr(exeordre, '762%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '762%', agregatlib);
    lib := 'D''autres valeurs mobilières et créances de l''actif immobilisé';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Autres produits ---
    total := resultat_compte_agr(exeordre, '763%', agregatlib)
        + resultat_compte_agr(exeordre, '764%', agregatlib)
        + resultat_compte_agr(exeordre, '765%', agregatlib)
        + resultat_compte_agr(exeordre, '768%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '763%', agregatlib)
        + resultat_compte_agr(exeordre-1, '764%', agregatlib)
        + resultat_compte_agr(exeordre-1, '765%', agregatlib)
        + resultat_compte_agr(exeordre-1, '768%', agregatlib);
    lib := 'Autres intérêts et produits assimilés';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte_agr(exeordre, '786%', agregatlib)
        + resultat_compte_agr(exeordre, '796%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '786%', agregatlib)
        + resultat_compte_agr(exeordre-1, '796%', agregatlib);
    lib := 'Reprise sur provisions et transferts de charges financières';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Gain de change ---
    total := resultat_compte_agr(exeordre, '766%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '766%', agregatlib);
    lib := 'Différence positive de change';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Pduits sur cession VMP ---
    total := resultat_compte_agr(exeordre, '767%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '767%', agregatlib);
    lib := 'Produits nets sur cession de valeurs mobilières de placement';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

----- PRODUITS EXCEPTIONNELS ----
lib2:= 'PRODUITS EXCEPTIONNELS';

-- Opération de gestion ---
    total := resultat_compte_agr(exeordre, '771%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '771%', agregatlib);
    lib := 'Sur opération de gestion';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Op capital cession éléménts actifs ---
    total := resultat_compte_agr(exeordre, '775%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '775%', agregatlib);
    lib := 'Sur opération en capital : Produits des cessions d''élements d''actif';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Op capital subvention exploitation ---
    total := resultat_compte_agr(exeordre, '777%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '777%', agregatlib);
    lib := 'Sur opération en capital : Subventions d''investissement virées au résultat';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Op capital AUTRES ---
    total := resultat_compte_agr(exeordre, '778%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '778%', agregatlib);
    lib := 'Sur autres opérations en capital';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Neutralisation des amortissements ---
    total := resultat_compte_agr(exeordre, '776%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '776%', agregatlib);
    lib := 'Neutralisation des amortissements';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Reprise sur amort et prov ---
    total := resultat_compte_agr(exeordre, '787%', agregatlib)
        + resultat_compte_agr(exeordre, '797%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '787%', agregatlib)
        + resultat_compte_agr(exeordre-1, '797%', agregatlib);
    lib := 'Reprise sur provisions et transferts de charges exceptionnelles';
    INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, total, totalant, agregatlib, exeordre);

-- Bénéfice ou Perte --
lib1 := 'III';
lib2 := '  ';
lib := 'SOLDE DEBITEUR = PERTE';
INSERT INTO cpte_rtat_produits VALUES (seq_cr_produits.NEXTVAL, lib1, lib2, lib, perte, perteant, agregatlib, exeordre);

END;
/


-----------------
--**********************
-----------------

CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_DETERMINATION_CAF_bak" (exeordre NUMBER, gescode VARCHAR2, sacd CHAR, methodeEBE VARCHAR2)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  libant VARCHAR2(50);
  total_produits NUMBER(12,2);
  total_produits_ant NUMBER(12,2);
  total_charges NUMBER(12,2);
  total_charges_ant NUMBER(12,2);
  formule VARCHAR2(50);
  ebe NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat NUMBER(12,2);
  resultat_ant NUMBER(12,2);
  cpt NUMBER;

 
BEGIN

--*************** DETERMINATION A PARTIR DE EBE  *********************************
IF methodeEBE = 'O' THEN

    IF sacd = 'O' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'O';
    ELSIF sacd = 'G' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'O';    
    ELSE
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'O';
    END IF;

    total_produits :=0 ;
    total_charges :=0;
    total_produits_ant :=0 ;
    total_charges_ant :=0;
    formule := '';

    --**** RECUPERATION EBE *******
    SELECT NVL(sig_montant,0), groupe2, sig_libelle, NVL(sig_montant_ant,0), groupe_ant
    INTO ebe, lib1, lib, ebe_ant, libant FROM SIG
    WHERE gescode = ges_code AND exe_ordre = exeordre
    AND (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') AND commentaire = 'EBE';
    IF lib1 = 'charges' THEN
        ebe := -ebe;
    END IF;
    IF libant = 'charges' THEN
        ebe_ant := -ebe_ant;
    END IF;

    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, ebe, ebe_ant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte(exeordre, '75%', gescode, sacd)
        + resultat_compte(exeordre, '1875%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '75%', gescode, sacd)
        + resultat_compte(exeordre-1, '1875%', gescode, sacd);
    lib := '+ Autres produits "encaissables" d''exploitation';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


    total := resultat_compte(exeordre, '791%', gescode, sacd)
        + resultat_compte(exeordre, '18791%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '791%', gescode, sacd)
        + resultat_compte(exeordre-1, '18791%', gescode, sacd);
    lib := '+ Transferts de charges';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+resultat_compte(exeordre, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+ resultat_compte(exeordre-1, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
    lib := '+ Produits financiers "encaissables" (a)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '771%', gescode, sacd)+resultat_compte(exeordre, '778%', gescode, sacd)+resultat_compte(exeordre, '797%', gescode, sacd)+resultat_compte(exeordre, '18771%', gescode, sacd)+resultat_compte(exeordre, '18778%', gescode, sacd)+resultat_compte(exeordre, '18797%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '771%', gescode, sacd)+resultat_compte(exeordre-1, '778%', gescode, sacd)+resultat_compte(exeordre-1, '797%', gescode, sacd)+resultat_compte(exeordre-1, '18771%', gescode, sacd)+resultat_compte(exeordre-1, '18778%', gescode, sacd)+resultat_compte(exeordre-1, '18797%', gescode, sacd);
    lib := '+ Produits exceptionnels "encaissables" (b)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  charges ***********
    lib1 := 'charges';

    total := resultat_compte(exeordre, '65%', gescode, sacd)
        + resultat_compte(exeordre, '1865%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '65%', gescode, sacd)
        + resultat_compte(exeordre-1, '1865%', gescode, sacd);
    lib := '- Autres charges "décaissables" d''exploitation';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '66%', gescode, sacd)+resultat_compte(exeordre, '1866%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '66%', gescode, sacd)+resultat_compte(exeordre-1, '1866%', gescode, sacd);
    lib := '- Charges financières "décaissables" (c)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '671%', gescode, sacd)+resultat_compte(exeordre, '678%', gescode, sacd)+resultat_compte(exeordre, '18671%', gescode, sacd)+resultat_compte(exeordre, '18678%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '671%', gescode, sacd)+resultat_compte(exeordre-1, '678%', gescode, sacd)+resultat_compte(exeordre-1, '18671%', gescode, sacd)+resultat_compte(exeordre-1, '18678%', gescode, sacd);
    lib := '- Charges exceptionnelles "décaissables" (d)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '695%', gescode, sacd)+resultat_compte(exeordre, '18695%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '695%', gescode, sacd)+resultat_compte(exeordre-1, '18695%', gescode, sacd);
    lib := '- Impôts sur les bénéfices';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := ebe+total_produits-total_charges;
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;


    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


ELSE

    --********* Détermination à partir du résultat **********************

    IF sacd = 'O' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
    ELSIF sacd = 'G' THEN
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'N';        
    ELSE
        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
    END IF;

    total_produits :=0 ;
    total_charges :=0;
    formule := '';

    --**** RECUPERATION RESULTAT ********
    totalant := 0;
    lib := 'Résultat de l''exercice';
    IF sacd = 'O' THEN
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
    ELSIF sacd = 'G' THEN
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
    ELSE
        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
    END IF;
    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, resultat, totalant, 'RTAT');

    -- ********  Charges ***********
    lib1 := 'charges';

    total := resultat_compte(exeordre, '681%', gescode, sacd)+resultat_compte(exeordre, '686%', gescode, sacd)+resultat_compte(exeordre, '687%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '681%', gescode, sacd)+resultat_compte(exeordre-1, '686%', gescode, sacd)+resultat_compte(exeordre-1, '687%', gescode, sacd);
    lib := '+ Dotations aux amortissements et provisions';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte(exeordre, '675%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
    lib := '+ Valeur comptable des éléments actifs cédés';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte(exeordre, '781%', gescode, sacd)+resultat_compte(exeordre, '786%', gescode, sacd)+resultat_compte(exeordre, '787%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '781%', gescode, sacd)+resultat_compte(exeordre-1, '786%', gescode, sacd)+resultat_compte(exeordre-1, '787%', gescode, sacd);
    lib := '- Reprises sur amortissements et provisions';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '775%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
    lib := '- Produits de cessions des éléments actifs cédés';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '776%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
    lib := '- Produits issus de la neutralisation des amortissements';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte(exeordre, '777%', gescode, sacd);
    totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
    lib := '- Quote-part des subventions d''investissement virées au compte de résultat';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := resultat+total_charges-total_produits;
    totalant := 0;
    formule := 'CAF';
    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;

    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


END IF;

END;
/


-------*********

CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_DETERMINATION_CAF" (exeordre NUMBER, agregatlib VARCHAR2, methodeEBE VARCHAR2)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  libant VARCHAR2(50);
  total_produits NUMBER(12,2);
  total_produits_ant NUMBER(12,2);
  total_charges NUMBER(12,2);
  total_charges_ant NUMBER(12,2);
  formule VARCHAR2(50);
  ebe NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat NUMBER(12,2);
  resultat_ant NUMBER(12,2);
  cpt NUMBER;

 
BEGIN

--*************** DETERMINATION A PARTIR DE EBE  *********************************
IF methodeEBE = 'O' THEN
    DELETE CAF WHERE exe_ordre = exeordre AND ges_code = agregatlib AND methode_ebe = 'O';
 

    total_produits :=0 ;
    total_charges :=0;
    total_produits_ant :=0 ;
    total_charges_ant :=0;
    formule := '';

    --**** RECUPERATION EBE *******
    SELECT NVL(sig_montant,0), groupe2, sig_libelle, NVL(sig_montant_ant,0), groupe_ant
    INTO ebe, lib1, lib, ebe_ant, libant FROM SIG
    WHERE ges_code=agregatlib AND exe_ordre = exeordre
    AND (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') AND commentaire = 'EBE';
    IF lib1 = 'charges' THEN
        ebe := -ebe;
    END IF;
    IF libant = 'charges' THEN
        ebe_ant := -ebe_ant;
    END IF;

    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, ebe, ebe_ant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte_agr(exeordre, '75%', agregatlib)
        + resultat_compte_agr(exeordre, '1875%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '75%', agregatlib)
        + resultat_compte_agr(exeordre-1, '1875%', agregatlib);
    lib := '+ Autres produits "encaissables" d''exploitation';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);


    total := resultat_compte_agr(exeordre, '791%', agregatlib)
        + resultat_compte_agr(exeordre, '18791%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '791%', agregatlib)
        + resultat_compte_agr(exeordre-1, '18791%', agregatlib);
    lib := '+ Transferts de charges';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte_agr(exeordre, '76%', agregatlib)+resultat_compte_agr(exeordre, '796%', agregatlib)+resultat_compte_agr(exeordre, '1876%', agregatlib)+resultat_compte_agr(exeordre, '18796%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '76%', agregatlib)+resultat_compte_agr(exeordre, '796%', agregatlib)+ resultat_compte_agr(exeordre-1, '1876%', agregatlib)+resultat_compte_agr(exeordre, '18796%', agregatlib);
    lib := '+ Produits financiers "encaissables" (a)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte_agr(exeordre, '771%', agregatlib)+resultat_compte_agr(exeordre, '778%', agregatlib)+resultat_compte_agr(exeordre, '797%', agregatlib)+resultat_compte_agr(exeordre, '18771%', agregatlib)+resultat_compte_agr(exeordre, '18778%', agregatlib)+resultat_compte_agr(exeordre, '18797%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '771%', agregatlib)+resultat_compte_agr(exeordre-1, '778%', agregatlib)+resultat_compte_agr(exeordre-1, '797%', agregatlib)+resultat_compte_agr(exeordre-1, '18771%', agregatlib)+resultat_compte_agr(exeordre-1, '18778%', agregatlib)+resultat_compte_agr(exeordre-1, '18797%', agregatlib);
    lib := '+ Produits exceptionnels "encaissables" (b)';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  charges ***********
    lib1 := 'charges';

    total := resultat_compte_agr(exeordre, '65%', agregatlib)
        + resultat_compte_agr(exeordre, '1865%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '65%', agregatlib)
        + resultat_compte_agr(exeordre-1, '1865%', agregatlib);
    lib := '- Autres charges "décaissables" d''exploitation';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '66%', agregatlib)+resultat_compte_agr(exeordre, '1866%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '66%', agregatlib)+resultat_compte_agr(exeordre-1, '1866%', agregatlib);
    lib := '- Charges financières "décaissables" (c)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '671%', agregatlib)+resultat_compte_agr(exeordre, '678%', agregatlib)+resultat_compte_agr(exeordre, '18671%', agregatlib)+resultat_compte_agr(exeordre, '18678%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '671%', agregatlib)+resultat_compte_agr(exeordre-1, '678%', agregatlib)+resultat_compte_agr(exeordre-1, '18671%', agregatlib)+resultat_compte_agr(exeordre-1, '18678%', agregatlib);
    lib := '- Charges exceptionnelles "décaissables" (d)';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '695%', agregatlib)+resultat_compte_agr(exeordre, '18695%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '695%', agregatlib)+resultat_compte_agr(exeordre-1, '18695%', agregatlib);
    lib := '- Impôts sur les bénéfices';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := ebe+total_produits-total_charges;
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code=agregatlib AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = agregatlib AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;


    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);


ELSE

    --********* Détermination à partir du résultat **********************
    DELETE CAF WHERE exe_ordre = exeordre AND ges_code = agregatlib AND methode_ebe = 'N';
--    IF sacd = 'O' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
--    ELSIF sacd = 'G' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'N';        
--    ELSE
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
--    END IF;

    total_produits :=0 ;
    total_charges :=0;
    formule := '';

    --**** RECUPERATION RESULTAT ********
    totalant := 0;
    lib := 'Résultat de l''exercice';
    
     SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
        WHERE (pco_num = '120' OR pco_num = '129') AND d.exe_ordre = exeordre
        and d.exe_ordre=ag.exe_ordre and d.ges_code=ag.ges_code and ag.agregat=agregatlib ;


    
--    
--    
--    IF sacd = 'O' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
--    ELSIF sacd = 'G' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
--    ELSE
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
--    END IF;

    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = agregatlib AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = agregatlib AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, resultat, totalant, 'RTAT');

    -- ********  Charges ***********
    lib1 := 'charges';

    total := resultat_compte_agr(exeordre, '681%', agregatlib)+resultat_compte_agr(exeordre, '686%', agregatlib)+resultat_compte_agr(exeordre, '687%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '681%', agregatlib)+resultat_compte_agr(exeordre-1, '686%', agregatlib)+resultat_compte_agr(exeordre-1, '687%', agregatlib);
    lib := '+ Dotations aux amortissements et provisions';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);

    total := resultat_compte_agr(exeordre, '675%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '675%', agregatlib);
    lib := '+ Valeur comptable des éléments actifs cédés';
    total_charges := total_charges+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);

    -- ********  Produits ***********
    lib1 := 'produits';

    total := resultat_compte_agr(exeordre, '781%', agregatlib)+resultat_compte_agr(exeordre, '786%', agregatlib)+resultat_compte_agr(exeordre, '787%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '781%', agregatlib)+resultat_compte_agr(exeordre-1, '786%', agregatlib)+resultat_compte_agr(exeordre-1, '787%', agregatlib);
    lib := '- Reprises sur amortissements et provisions';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '775%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '775%', agregatlib);
    lib := '- Produits de cessions des éléments actifs cédés';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '776%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '776%', agregatlib);
    lib := '- Produits issus de la neutralisation des amortissements';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    total := resultat_compte_agr(exeordre, '777%', agregatlib);
    totalant := resultat_compte_agr(exeordre-1, '777%', agregatlib);
    lib := '- Quote-part des subventions d''investissement virées au compte de résultat';
    total_produits := total_produits+total;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, -total, -totalant, formule);

    --- ************ Calcul de la caf *****************
    total := resultat+total_charges-total_produits;
    totalant := 0;
    formule := 'CAF';
    IF total >= 0 THEN
        lib1:= 'produits';
        lib := '= CAPACITE D''AUTOFINANCEMENT';
    ELSE
        lib1:= 'charges';
        lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
    END IF;

    -- N-1
    SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = agregatlib AND exe_ordre = exeordre-1 AND formule = 'CAF';
    IF (cpt > 0) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE ges_code = agregatlib AND exe_ordre = exeordre-1 AND formule = 'CAF';
    ELSE
        totalant := 0;
    END IF;
    INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, agregatlib, methodeEBE, lib1, lib, total, totalant, formule);


END IF;

END;
/


-------------------
--******************
-------------------
CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_SIG_bak" (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS

  -- version du 14/09/2009

  montant NUMBER(12,2);
  montant_charges NUMBER(12,2);
  montant_produits NUMBER(12,2);
  montant_groupe NUMBER(12,2);
  -- MAJ SIG exercice antérieur
  montant_ant NUMBER(12,2);
  montant_charges_ant NUMBER(12,2);
  montant_produits_ant NUMBER(12,2);
  montant_groupe_ant NUMBER(12,2);


  va NUMBER(12,2);
  ebe NUMBER(12,2);
  resultat_exploitation NUMBER(12,2);
  resultat_courant NUMBER(12,2);
  resultat_exceptionnel NUMBER(12,2);
  resultat_net NUMBER(12,2);
  -- Calcul SIG exercice antérieur
  va_ant NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat_exploitation_ant NUMBER(12,2);
  resultat_courant_ant NUMBER(12,2);
  resultat_exceptionnel_ant NUMBER(12,2);
  resultat_net_ant NUMBER(12,2);


  anneeExer NUMBER;

  pconum NUMBER;
  groupe1 VARCHAR(50);
  groupe2 VARCHAR(50);
  groupe_ant VARCHAR(50);

  lib VARCHAR(100);
  formule VARCHAR(1000);
  commentaire VARCHAR(1000);
  cpt NUMBER;

BEGIN

--*************** CREATION TABLE *********************************
IF sacd = 'O' THEN
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE SIG WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
ELSE    
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



----- VALEUR AJOUTEE ----
   groupe1 := 'Valeur ajoutée';
   montant_charges := 0;
   montant_produits := 0;

   groupe2 := 'produits';

    lib := 'Vente et prestations de services (C.A.)';
    formule := 'SC(70+1870) - SD (709+18709)';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '70%,1870%', 'C', gescode, sacd)
        - Solde_Compte_Ext_Avt_S67(exeordre, '709%,18709%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '70%,1870%', 'C', gescode, sacd)
        - Solde_Compte_Ext_Avt_S67(exeordre-1, '709%,18709%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production stockée';
    formule := 'SC713+18713 - SD 713+18713';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '713%,18713%', 'C', gescode, sacd)
        - Solde_Compte_Ext_Avt_S67(exeordre, '713%,18713%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '713%,18713%', 'C', gescode, sacd)
        - Solde_Compte_Ext_Avt_S67(exeordre-1, '713%,18713%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production immobilisée';
    formule := 'SC72+1872';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '72%,1872%', 'C', gescode, sacd);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '72%,1872%', 'C', gescode, sacd);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire, montant_ant, groupe2);


   groupe2 := 'charges';

    lib := 'Achats';
    formule := 'SD(601+602+604+605+606+607+608)+SD603-SC603-SC609';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '609%,18609%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre-1, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '609%,18609%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Services exterieurs';
    formule := 'SD61 - SC619';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '619%,18619%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '619%,18619%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Autres services exterieurs (sauf personnel exterieur)';
    formule := 'SD(62 - 621) - SC629';
    commentaire := '';
    montant := Solde_Compte_Ext(exeordre, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext(exeordre, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67(exeordre, '629%,18629%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext(exeordre-1, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext(exeordre-1, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '629%,18629%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


    va := montant_produits - montant_charges;

-----------------------------------------
 groupe1 := 'Excédent/Insuffisance brut(e) d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
   WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO va_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   END IF;
   /*IF groupe_ant = 'charges' THEN
           va_ant := -va_ant;
   END IF;*/

  IF (va>=0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Valeur ajoutée', va, ' ', 'VAL', va_ant, groupe_ant);
   montant_produits := va;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Valeur ajoutée', -va, ' ', 'VAL', va_ant, groupe_ant);
   montant_charges := -va;
  END IF;

 groupe2 := 'produits';

  lib := 'Subventions d''exploitation d''etat';
  formule := 'SC741';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '741%,18741%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '741%,18741%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Subventions d''exploitation collectivités publiques et organismes internationaux';
  formule := 'SC744';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '744%,18744%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '744%,18744%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dons / legs et autres subventions d''exploitation';
  formule := 'SC(746+748)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


 groupe2 := 'charges';

  lib := 'Charges de personnel (y c. le personnel extérieur)';
  formule := 'SD(64+621)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Impots, taxes et versements assimilés s/ rémunérations';
  formule := 'SD(631+632+633)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres Impots, taxes et versements assimilés';
  formule := 'SD(635+637)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  ebe := montant_produits - montant_charges;
-----------------------------------------

 groupe1 := 'Résultat d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO ebe_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   END IF;

  IF (ebe >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Excédent brut d''exploitation',
        ebe, '','EBE', ebe_ant, groupe_ant);
   montant_produits := ebe;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Insuffisance brute d''exploitation',
        -ebe, '','EBE', ebe_ant, groupe_ant);
   montant_charges := -ebe;
  END IF;



 groupe2 := 'produits';

  lib := 'Reprise sur amortissements et provisions';
  formule := 'SC781';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '781%,18781%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '781%,18781%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Transfert de charges d''exploitation';
  formule := 'SC791';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '791%,18791%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '791%,18791%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres produits';
  formule := 'SC75+SC187%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '75%,1875%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '75%,1875%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Autres charges';
  formule := 'SD65+SD186%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '65%,1865%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '65%,1865%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dotations aux amortissements et provisions';
  formule := 'SD681';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '681%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '681%,18681%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'produits';

  lib := 'Produits issus de la neutralisation des amortissements';
  formule := 'SC776';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '776%,18776%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '776%,18776%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  lib := 'Quote-part des subventions d''investissement virée au résultat de l''exercice';
  formule := 'SC777';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '777%,18777%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '777%,18777%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  resultat_exploitation := montant_produits - montant_charges;

----------------------------------------------------------------------

  groupe1 := 'Resultat courant';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exploitation_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL';
   END IF;

  IF (resultat_exploitation >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat d''exploitation',
        resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_produits := resultat_exploitation;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat d''exploitation',
         -resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_charges := -resultat_exploitation;
  END IF;




 groupe2 := 'produits';

  lib := 'Produits financiers';
  formule := 'SC(76+786+796)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges financières';
  formule := 'SD(66+686)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  resultat_courant := montant_produits - montant_charges;
----------------------------------------------------------------------------------

 groupe1 := 'Resultat exceptionnel';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits exceptionnels (sauf c/ 776 et 777)';
  formule := 'SC(77 - 776 -777) + SC(787 + 797)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre-1, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges exceptionnelles';
  formule := 'SD(67+687)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '67%,687%,1867%,18687%', 'D', gescode, sacd);
   montant_charges := montant_charges + montant;

  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '67%,687%,1867%,18687%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 resultat_exceptionnel := montant_produits - montant_charges;

-------------------------------------------------------------------

 groupe1 := 'Resultat net';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_courant_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR';
   END IF;

  IF (resultat_courant >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat courant',
        resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat courant',
        -resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  END IF;

  IF (groupe_ant = 'charges') THEN resultat_courant_ant := -resultat_courant_ant;
  END IF;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exceptionnel_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP';
   END IF;

  IF (resultat_exceptionnel >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat exceptionnel',
        resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat exceptionnel',
        -resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  END IF;

  IF (groupe_ant = 'charges') THEN resultat_exceptionnel_ant := -resultat_exceptionnel_ant;
  END IF;

  resultat_net := resultat_courant + resultat_exceptionnel;
  resultat_net_ant := resultat_courant_ant + resultat_exceptionnel_ant;
--------------------------------------------------------------
 groupe1 := 'Resultat net après impots';
  montant_charges := 0;
  montant_produits := 0;
  -- N-1
  IF resultat_net_ant >=0 THEN
      groupe_ant := 'produits';
  ELSE
      groupe_ant := 'charges';
  END IF;

  IF (resultat_net >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat net',resultat_net, '','', ABS(resultat_net_ant), groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat net', -resultat_net, '','', ABS(resultat_net_ant), groupe_ant);
  END IF;

 groupe2 := 'charges';

  lib := 'Impots sur les bénéfices et impots assimilés';
  formule := 'SD(695+697+699)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

---------------------------------------------------------------------

 groupe1 := 'Plus ou moins-value sur cession d''actif';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits des cessions d''éléments d''actif';
  formule := 'SC775';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '775%,18775%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '775%,18775%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Valeurs comptable des éléments d''actif cédés';
  formule := 'SD675';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '675%,18675%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '675%,18675%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


END;
/

-------------------
-------------------

CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_SIG" (exeordre NUMBER, agregatlib VARCHAR2)
IS

  -- version du 14/09/2009

  montant NUMBER(12,2);
  montant_charges NUMBER(12,2);
  montant_produits NUMBER(12,2);
  montant_groupe NUMBER(12,2);
  -- MAJ SIG exercice antérieur
  montant_ant NUMBER(12,2);
  montant_charges_ant NUMBER(12,2);
  montant_produits_ant NUMBER(12,2);
  montant_groupe_ant NUMBER(12,2);


  va NUMBER(12,2);
  ebe NUMBER(12,2);
  resultat_exploitation NUMBER(12,2);
  resultat_courant NUMBER(12,2);
  resultat_exceptionnel NUMBER(12,2);
  resultat_net NUMBER(12,2);
  -- Calcul SIG exercice antérieur
  va_ant NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat_exploitation_ant NUMBER(12,2);
  resultat_courant_ant NUMBER(12,2);
  resultat_exceptionnel_ant NUMBER(12,2);
  resultat_net_ant NUMBER(12,2);


  anneeExer NUMBER;

  pconum NUMBER;
  groupe1 VARCHAR(50);
  groupe2 VARCHAR(50);
  groupe_ant VARCHAR(50);

  lib VARCHAR(100);
  formule VARCHAR(1000);
  commentaire VARCHAR(1000);
  cpt NUMBER;

BEGIN

--*************** CREATION TABLE *********************************
DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = agregatlib;


--IF sacd = 'O' THEN
-- DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--ELSif sacd = 'G' then
--    DELETE SIG WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
--ELSE    
-- DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--END IF;



----- VALEUR AJOUTEE ----
   groupe1 := 'Valeur ajoutée';
   montant_charges := 0;
   montant_produits := 0;

   groupe2 := 'produits';

    lib := 'Vente et prestations de services (C.A.)';
    formule := 'SC(70+1870) - SD (709+18709)';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '70%,1870%', 'C', agregatlib)
        - Solde_Compte_Ext_Avt_S67_agr(exeordre, '709%,18709%', 'D', agregatlib);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '70%,1870%', 'C', agregatlib)
        - Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '709%,18709%', 'D', agregatlib);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production stockée';
    formule := 'SC713+18713 - SD 713+18713';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '713%,18713%', 'C', agregatlib)
        - Solde_Compte_Ext_Avt_S67_agr(exeordre, '713%,18713%', 'D', agregatlib);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '713%,18713%', 'C', agregatlib)
        - Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '713%,18713%', 'D', agregatlib);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production immobilisée';
    formule := 'SC72+1872';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '72%,1872%', 'C', agregatlib);
    montant_produits := montant_produits + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '72%,1872%', 'C', agregatlib);
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule,commentaire, montant_ant, groupe2);


   groupe2 := 'charges';

    lib := 'Achats';
    formule := 'SD(601+602+604+605+606+607+608)+SD603-SC603-SC609';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', agregatlib) +
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '603%,18603%', 'D', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '603%,18603%', 'C', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '609%,18609%', 'C', agregatlib) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', agregatlib) +
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '603%,18603%', 'D', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '603%,18603%', 'C', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '609%,18609%', 'C', agregatlib) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Services exterieurs';
    formule := 'SD61 - SC619';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '61%,1861%', 'D', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '619%,18619%', 'C', agregatlib) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '61%,1861%', 'D', agregatlib) -
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '619%,18619%', 'C', agregatlib) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Autres services exterieurs (sauf personnel exterieur)';
    formule := 'SD(62 - 621) - SC629';
    commentaire := '';
    montant := Solde_Compte_Ext_agr(exeordre, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', agregatlib, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext_agr(exeordre, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', agregatlib, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '629%,18629%', 'C', agregatlib) ;
    montant_charges := montant_charges + montant;
    -- N-1
    montant_ant := Solde_Compte_Ext_agr(exeordre-1, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', agregatlib, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext_agr(exeordre-1, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', agregatlib, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '629%,18629%', 'C', agregatlib) ;
    INSERT INTO SIG VALUES
        (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


    va := montant_produits - montant_charges;

-----------------------------------------
 groupe1 := 'Excédent/Insuffisance brut(e) d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
   WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'VAL';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO va_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'VAL';
   END IF;
   /*IF groupe_ant = 'charges' THEN
           va_ant := -va_ant;
   END IF;*/

  IF (va>=0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Valeur ajoutée', va, ' ', 'VAL', va_ant, groupe_ant);
   montant_produits := va;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Valeur ajoutée', -va, ' ', 'VAL', va_ant, groupe_ant);
   montant_charges := -va;
  END IF;

 groupe2 := 'produits';

  lib := 'Subventions d''exploitation d''etat';
  formule := 'SC741';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '741%,18741%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '741%,18741%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Subventions d''exploitation collectivités publiques et organismes internationaux';
  formule := 'SC744';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '744%,18744%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '744%,18744%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dons / legs et autres subventions d''exploitation';
  formule := 'SC(746+748)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '746%,748%,18746%,18748%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '746%,748%,18746%,18748%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


 groupe2 := 'charges';

  lib := 'Charges de personnel (y c. le personnel extérieur)';
  formule := 'SD(64+621)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '64%,1864%,621%,18621%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '64%,1864%,621%,18621%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Impots, taxes et versements assimilés s/ rémunérations';
  formule := 'SD(631+632+633)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '631%,632%,633%,18631%,18632%,18633%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '631%,632%,633%,18631%,18632%,18633%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres Impots, taxes et versements assimilés';
  formule := 'SD(635+637)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '635%,637%,18635%,18637%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '635%,637%,18635%,18637%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  ebe := montant_produits - montant_charges;
-----------------------------------------

 groupe1 := 'Résultat d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'EBE';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO ebe_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'EBE';
   END IF;

  IF (ebe >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Excédent brut d''exploitation',
        ebe, '','EBE', ebe_ant, groupe_ant);
   montant_produits := ebe;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Insuffisance brute d''exploitation',
        -ebe, '','EBE', ebe_ant, groupe_ant);
   montant_charges := -ebe;
  END IF;



 groupe2 := 'produits';

  lib := 'Reprise sur amortissements et provisions';
  formule := 'SC781';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '781%,18781%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '781%,18781%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Transfert de charges d''exploitation';
  formule := 'SC791';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '791%,18791%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '791%,18791%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres produits';
  formule := 'SC75+SC187%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '75%,1875%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '75%,1875%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Autres charges';
  formule := 'SD65+SD186%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '65%,1865%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '65%,1865%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dotations aux amortissements et provisions';
  formule := 'SD681';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '681%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '681%,18681%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'produits';

  lib := 'Produits issus de la neutralisation des amortissements';
  formule := 'SC776';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '776%,18776%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '776%,18776%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  lib := 'Quote-part des subventions d''investissement virée au résultat de l''exercice';
  formule := 'SC777';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '777%,18777%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '777%,18777%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  resultat_exploitation := montant_produits - montant_charges;

----------------------------------------------------------------------

  groupe1 := 'Resultat courant';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_EXPL' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exploitation_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_EXPL';
   END IF;

  IF (resultat_exploitation >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Resultat d''exploitation',
        resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_produits := resultat_exploitation;
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Resultat d''exploitation',
         -resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_charges := -resultat_exploitation;
  END IF;




 groupe2 := 'produits';

  lib := 'Produits financiers';
  formule := 'SC(76+786+796)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '76%,786%,796%,1876%,18786%,18796%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '76%,786%,796%,1876%,18786%,18796%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges financières';
  formule := 'SD(66+686)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '66%,686%,1866%,18686%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '66%,686%,1866%,18686%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  resultat_courant := montant_produits - montant_charges;
----------------------------------------------------------------------------------

 groupe1 := 'Resultat exceptionnel';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits exceptionnels (sauf c/ 776 et 777)';
  formule := 'SC(77 - 776 -777) + SC(787 + 797)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', agregatlib) +
     Solde_Compte_Ext_Avt_S67_agr(exeordre, '787%,797%,18787%,18797%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', agregatlib) +
     Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '787%,797%,18787%,18797%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges exceptionnelles';
  formule := 'SD(67+687)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '67%,687%,1867%,18687%', 'D', agregatlib);
   montant_charges := montant_charges + montant;

  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '67%,687%,1867%,18687%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 resultat_exceptionnel := montant_produits - montant_charges;

-------------------------------------------------------------------

 groupe1 := 'Resultat net';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_COUR' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_courant_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_COUR';
   END IF;

  IF (resultat_courant >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Resultat courant',
        resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Resultat courant',
        -resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  END IF;

  IF (groupe_ant = 'charges') THEN resultat_courant_ant := -resultat_courant_ant;
  END IF;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
      WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_EXCEP' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exceptionnel_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND commentaire = 'R_EXCEP';
   END IF;

  IF (resultat_exceptionnel >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Resultat exceptionnel',
        resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Resultat exceptionnel',
        -resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  END IF;

  IF (groupe_ant = 'charges') THEN resultat_exceptionnel_ant := -resultat_exceptionnel_ant;
  END IF;

  resultat_net := resultat_courant + resultat_exceptionnel;
  resultat_net_ant := resultat_courant_ant + resultat_exceptionnel_ant;
--------------------------------------------------------------
 groupe1 := 'Resultat net après impots';
  montant_charges := 0;
  montant_produits := 0;
  -- N-1
  IF resultat_net_ant >=0 THEN
      groupe_ant := 'produits';
  ELSE
      groupe_ant := 'charges';
  END IF;

  IF (resultat_net >= 0) THEN
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'produits', 'Resultat net',resultat_net, '','', ABS(resultat_net_ant), groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
           (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, 'charges', 'Resultat net', -resultat_net, '','', ABS(resultat_net_ant), groupe_ant);
  END IF;

 groupe2 := 'charges';

  lib := 'Impots sur les bénéfices et impots assimilés';
  formule := 'SD(695+697+699)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '695%,697%,699%,18695%,18697%,18699%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '695%,697%,699%,18695%,18697%,18699%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

---------------------------------------------------------------------

 groupe1 := 'Plus ou moins-value sur cession d''actif';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits des cessions d''éléments d''actif';
  formule := 'SC775';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '775%,18775%', 'C', agregatlib);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '775%,18775%', 'C', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Valeurs comptable des éléments d''actif cédés';
  formule := 'SD675';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67_agr(exeordre, '675%,18675%', 'D', agregatlib);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67_agr(exeordre-1, '675%,18675%', 'D', agregatlib);
  INSERT INTO SIG VALUES
          (SIG_SEQ.NEXTVAL, exeordre, agregatlib, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


END;
/

------------------
--******************
-------------------
CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_VARIATION_FRNG_bak" (exeordre NUMBER, gescode VARCHAR, sacd CHAR)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR(100);
  lib1 VARCHAR(50);
  total_emplois NUMBER(12,2);
  total_ressources NUMBER(12,2);
  total_emploisant NUMBER(12,2);
  total_ressourcesAnt NUMBER(12,2);
  formule VARCHAR(50);
  vcaf NUMBER(12,2);
  flag INTEGER;
  --resultat NUMBER(12,2);

BEGIN

    IF sacd = 'O' THEN
        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = gescode;
  ELSIF sacd = 'G' THEN
        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';            
    ELSE
        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
    END IF;

    total_emplois :=0 ;
    total_ressources :=0;
    total_emploisant :=0;
    total_ressourcesant :=0;
    totalant := 0;
    formule := '';
    lib1 := 'RESSOURCES';

    --**** RECUPERATION CAF ou IAF ********
    SELECT NVL(caf_montant,0), caf_libelle INTO total, lib FROM CAF
    WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
    total_ressources := total_ressources+total;
    lib := SUBSTR(lib,3);

    SELECT COUNT(*) INTO flag FROM CAF
    WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
    IF (    flag >0 ) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
        total_ressourcesant := total_ressourcesant+totalant;
    END IF;

    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

    -- ********  Ressources ***********

    total := Execution_Bud(exeordre, '7751%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '7752%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '7756%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '274%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '275%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '276%', gescode, sacd, 'R');
    totalant := Execution_Bud(exeordre-1, '7751%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '7752%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '7756%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '274%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '275%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '276%', gescode, sacd, 'R');
    lib := '+ Cessions ou réductions de l''actif immobilisé';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


    total := Execution_Bud(exeordre, '102%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '103%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '131%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '138%', gescode, sacd, 'R');
    totalant := Execution_Bud(exeordre-1, '102%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '103%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '131%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '138%', gescode, sacd, 'R');
    lib := '+ Augmentation des capitaux propres';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

    total := Execution_Bud(exeordre, '16%', gescode, sacd, 'R')
        - Execution_Bud(exeordre, '1688%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '17%', gescode, sacd, 'R');
    totalant := Execution_Bud(exeordre-1, '16%', gescode, sacd, 'R')
        - Execution_Bud(exeordre-1, '1688%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '17%', gescode, sacd, 'R');
    lib := '+ Augmentation des dettes financières';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


    -- ********  Emplois ***********
    lib1 := 'EMPLOIS';

    total := Execution_Bud(exeordre, '20%', gescode, sacd, 'D')
        - Execution_Bud(exeordre, '20%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '21%', gescode, sacd, 'D')
        - Execution_Bud(exeordre, '21%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '23%', gescode, sacd, 'D')
        - Execution_Bud(exeordre, '23%', gescode, sacd, 'R')
        + Execution_Bud(exeordre, '26%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '27%', gescode, sacd, 'D');
    totalant :=Execution_Bud(exeordre-1, '20%', gescode, sacd, 'D')
        - Execution_Bud(exeordre-1, '20%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '21%', gescode, sacd, 'D')
        - Execution_Bud(exeordre-1, '21%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '23%', gescode, sacd, 'D')
        - Execution_Bud(exeordre-1, '23%', gescode, sacd, 'R')
        + Execution_Bud(exeordre-1, '26%', gescode, sacd, 'D')
        + Execution_Bud(exeordre-1, '27%', gescode, sacd, 'D');
    lib := '- Acquisition d''actifs immobilisés';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


    total := Solde_Compte(exeordre, '481%', 'D', gescode, sacd);
    totalant := Solde_Compte(exeordre-1, '481%', 'D', gescode, sacd);
    lib := '- Charges à répartir sur plusieurs exercices';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


    total := Execution_Bud(exeordre, '102%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '103%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '131%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '138%', gescode, sacd, 'D');
    totalant := Execution_Bud(exeordre-1, '102%', gescode, sacd, 'D')
        + Execution_Bud(exeordre-1, '103%', gescode, sacd, 'D')
        + Execution_Bud(exeordre-1, '131%', gescode, sacd, 'D')
        + Execution_Bud(exeordre-1, '138%', gescode, sacd, 'D');
    lib := '- Réduction de capitaux propres';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

    total := Execution_Bud(exeordre, '16%', gescode, sacd, 'D')
        - Execution_Bud(exeordre, '1688%', gescode, sacd, 'D')
        + Execution_Bud(exeordre, '17%', gescode, sacd, 'D');
    totalant := Execution_Bud(exeordre-1, '16%', gescode, sacd, 'D')
        - Execution_Bud(exeordre-1, '1688%', gescode, sacd, 'D')
        + Execution_Bud(exeordre-1, '17%', gescode, sacd, 'D');
    lib := '- Remboursement de dettes financières';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


    --- ************ Calcul du FRNG *****************
    total := total_ressources-total_emplois;
    totalant := total_ressourcesant-total_emploisant;
    lib1 := ' ';
    IF total >= 0 THEN
        lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (ressource nette)';
    ELSE
        lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (emploi net)';
    END IF;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);



END;
/

------------------
-------------------
CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_VARIATION_FRNG" (exeordre NUMBER, agregatlib VARCHAR)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR(100);
  lib1 VARCHAR(50);
  total_emplois NUMBER(12,2);
  total_ressources NUMBER(12,2);
  total_emploisant NUMBER(12,2);
  total_ressourcesAnt NUMBER(12,2);
  formule VARCHAR(50);
  vcaf NUMBER(12,2);
  flag INTEGER;
  --resultat NUMBER(12,2);

BEGIN
    DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = agregatlib;

--    IF sacd = 'O' THEN
--        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = agregatlib;
--  ELSIF sacd = 'G' THEN
--        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';            
--    ELSE
--        DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
--    END IF;

    total_emplois :=0 ;
    total_ressources :=0;
    total_emploisant :=0;
    total_ressourcesant :=0;
    totalant := 0;
    formule := '';
    lib1 := 'RESSOURCES';

    --**** RECUPERATION CAF ou IAF ********
    SELECT NVL(caf_montant,0), caf_libelle INTO total, lib FROM CAF
    WHERE exe_ordre = exeordre AND ges_code = agregatlib AND methode_ebe = 'N' AND formule = 'CAF';
    total_ressources := total_ressources+total;
    lib := SUBSTR(lib,3);

    SELECT COUNT(*) INTO flag FROM CAF
    WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND methode_ebe = 'N' AND formule = 'CAF';
    IF (    flag >0 ) THEN
        SELECT NVL(caf_montant,0) INTO totalant FROM CAF
        WHERE exe_ordre = exeordre-1 AND ges_code = agregatlib AND methode_ebe = 'N' AND formule = 'CAF';
        total_ressourcesant := total_ressourcesant+totalant;
    END IF;

    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);

    -- ********  Ressources ***********

    total := Execution_Bud_agr(exeordre, '7751%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '7752%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '7756%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '274%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '275%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '276%', agregatlib, 'R');
    totalant := Execution_Bud_agr(exeordre-1, '7751%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '7752%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '7756%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '274%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '275%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '276%', agregatlib, 'R');
    lib := '+ Cessions ou réductions de l''actif immobilisé';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);


    total := Execution_Bud_agr(exeordre, '102%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '103%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '131%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '138%', agregatlib, 'R');
    totalant := Execution_Bud_agr(exeordre-1, '102%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '103%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '131%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '138%', agregatlib, 'R');
    lib := '+ Augmentation des capitaux propres';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);

    total := Execution_Bud_agr(exeordre, '16%', agregatlib, 'R')
        - Execution_Bud_agr(exeordre, '1688%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '17%', agregatlib, 'R');
    totalant := Execution_Bud_agr(exeordre-1, '16%', agregatlib, 'R')
        - Execution_Bud_agr(exeordre-1, '1688%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '17%', agregatlib, 'R');
    lib := '+ Augmentation des dettes financières';
    total_ressources := total_ressources+total;
    total_ressourcesant := total_ressourcesant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);


    -- ********  Emplois ***********
    lib1 := 'EMPLOIS';

    total := Execution_Bud_agr(exeordre, '20%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre, '20%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '21%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre, '21%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '23%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre, '23%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre, '26%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre, '27%', agregatlib, 'D');
    totalant :=Execution_Bud_agr(exeordre-1, '20%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre-1, '20%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '21%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre-1, '21%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '23%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre-1, '23%', agregatlib, 'R')
        + Execution_Bud_agr(exeordre-1, '26%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre-1, '27%', agregatlib, 'D');
    lib := '- Acquisition d''actifs immobilisés';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);


    total := Solde_Compte_agr(exeordre, '481%', 'D', agregatlib);
    totalant := Solde_Compte_agr(exeordre-1, '481%', 'D', agregatlib);
    lib := '- Charges à répartir sur plusieurs exercices';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);


    total := Execution_Bud_agr(exeordre, '102%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre, '103%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre, '131%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre, '138%', agregatlib, 'D');
    totalant := Execution_Bud_agr(exeordre-1, '102%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre-1, '103%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre-1, '131%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre-1, '138%', agregatlib, 'D');
    lib := '- Réduction de capitaux propres';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);

    total := Execution_Bud_agr(exeordre, '16%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre, '1688%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre, '17%', agregatlib, 'D');
    totalant := Execution_Bud_agr(exeordre-1, '16%', agregatlib, 'D')
        - Execution_Bud_agr(exeordre-1, '1688%', agregatlib, 'D')
        + Execution_Bud_agr(exeordre-1, '17%', agregatlib, 'D');
    lib := '- Remboursement de dettes financières';
    total_emplois := total_emplois+total;
    total_emploisant := total_emploisant+totalant;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);


    --- ************ Calcul du FRNG *****************
    total := total_ressources-total_emplois;
    totalant := total_ressourcesant-total_emploisant;
    lib1 := ' ';
    IF total >= 0 THEN
        lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (ressource nette)';
    ELSE
        lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (emploi net)';
    END IF;
    INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, agregatlib, lib1, lib, total, totalant, formule);



END;
/

------------------
--******************
-------------------
















create procedure grhum.inst_patch_comptefi_1880 is
begin
	update comptefi.cpte_rtat_produits set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.cpte_rtat_charges set ges_code='SACD '|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.bilan_actif set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.bilan_passif set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.caf set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.sig set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.frng set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
	update comptefi.execution_budget set ges_code='SACD_'|| ges_code where ges_code not like 'SACD_%' and exe_ordre<=2010 and ges_code not like 'ETAB' and ges_code not like 'AGREGE';		
end;
/








