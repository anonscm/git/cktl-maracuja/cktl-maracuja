SET DEFINE OFF;
CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de pr�parer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 28/11/2007
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- V�rifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de pr�parer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de pr�parer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- R�cup�ration des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- R�cup�ration des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- R�cup�ration des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    
    
    --  -------------------------------------------------------
    -- R�cup�ration des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- R�cup�ration des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- R�cup�ration des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
    
END;
/


GRANT EXECUTE ON  MARACUJA.PREPARE_EXERCICE TO JEFY_ADMIN;





SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.Epn2
IS

FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2;
PROCEDURE  Epn_Proc_Bal_1 (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2, comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2);
PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2, comp VARCHAR2, exe_ordre NUMBER, v_date VARCHAR2);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn2  IS
-- v. du 12/03/2007 (code nomen. M9-3, M9-1, etc.)

FUNCTION epn_getCodeNomenclature(exeOrdre NUMBER) RETURN VARCHAR2
IS
  codeNomenclature maracuja.parametre.par_value%TYPE;
BEGIN
	SELECT PAR_VALUE INTO codeNomenclature FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'EPN_CODE_NOMENCLATURE' AND exe_ordre=exeOrdre;

	IF codeNomenclature IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'EPN_CODE_NOMENCLATURE doit etre renseigne dans maracuja.PARAMETRE');
	END IF;
	RETURN codeNomenclature;
END;


PROCEDURE Epn_Proc_Bal_1 (ordre VARCHAR2,comp VARCHAR2,exe_ordre NUMBER, v_date VARCHAR2)
IS

JOU_DATE DATE;
IMPUTATION VARCHAR2(15);
GESTION VARCHAR2(10);
ECR_SACD EPN_BALANCE.ECR_SACD%TYPE;
BE_CR EPN_BALANCE.BE_CR%TYPE;
MONTANT_CR EPN_BALANCE.MONTANT_CR%TYPE;
BE_DB EPN_BALANCE.BE_DB%TYPE;
MONTANT_DB EPN_BALANCE.MONTANT_DB%TYPE;
NUM_SEQ NUMBER(5);
num_seq_f NUMBER(5);
bsdeb NUMBER(15,2);
bscre NUMBER(15,2);
debtot NUMBER(15,2);
cretot NUMBER(15,2);
NEW_EPNB_ORDRE INTEGER;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
TYP_DOC VARCHAR2(2);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num NUMBER;
codeNomenclature VARCHAR2(2);
flag INTEGER;

--erreur EXCEPTION(2);


	CURSOR c_tot IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), 	SUM(MONTANT_DB)
	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_sacd IS SELECT IMPUTATION, ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND GESTION = comp AND exe_ordre = exerc
 	GROUP BY IMPUTATION, ECR_SACD
 	ORDER BY IMPUTATION ;

	CURSOR c_hors_sacd IS SELECT IMPUTATION,ECR_SACD, SUM(BE_CR), SUM(MONTANT_CR), SUM(BE_DB), SUM(MONTANT_DB)
 	FROM EPN_BALANCE WHERE TO_DATE(JOU_DATE) <= VARDATE AND exe_ordre = exerc
	AND GESTION NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY IMPUTATION, ECR_SACD
	ORDER BY IMPUTATION;


BEGIN


	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;

	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp =  'HSA'  THEN

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num  AND
			EPNB_GES_CODE NOT IN (SELECT ges_code FROM   SACD_PARAM );
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num
			AND EPNB_GES_CODE NOT IN (SELECT ges_code FROM jefy.SACD_PARAM );
		END IF;  */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_EXERCICE = exerc
			AND EPNB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
		AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_EXERCICE = exerc
			AND EPNB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		/* SELECT COUNT(*) INTO CPT FROM EPN_BAL
		WHERE EPNB_ORDRE = ordre_num AND EPNB_GES_CODE = comp ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL WHERE EPNB_ORDRE = ordre_num;
		END IF;   */

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num
		AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_1 WHERE EPNB_ORDRE = ordre_num AND EPNB1_GES_CODE = comp
			AND EPNB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num
		AND EPNB2_GES_CODE = comp AND EPNB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_BAL_2 WHERE EPNB_ORDRE = ordre_num AND EPNB2_GES_CODE = comp
			AND EPNB2_EXERCICE = exerc;
		END IF;

	END IF;

	--	SELECT SEQ_EPN_BAL.NEXTVAL INTO NEW_EPNB_ORDRE FROM dual;
	--  SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';


	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;

	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	-- TO_DATE(TO_CHAR(fin_premiere,'dd/mm/yyyy'),'dd/mm/yyyy')

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre = '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
	  		VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
			RANG := '03';
			VDATE := '3009' || EXERC;
	  		VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
	  			VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					VDATE:= V_DATE;
					RANG := '05';
	  				VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						VDATE:= V_DATE;
						RANG := '06';
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');
	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');


    	/* INSERT INTO EPN_BAL VALUES (ORDRE, SYSDATE,comp); */

		--type doc := '03'

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;

		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_BAL_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,
EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET,0);

	NUM_SEQ := 2;

	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  		SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
		-- VDATE := '31122005';
		VDATE := '3112'|| EXERCN1;
	 	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	/* pas de consolidation Etablissement + SACD
	IF comp =  'ALL'  THEN
	-- Etablissement + SACD --
		OPEN c_tot;
		LOOP
		FETCH c_tot INTO IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_tot%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB,MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp = 'HSA' THEN
		-- 	Etablissement --
		OPEN c_hors_sacd;
	    LOOP
		FETCH c_hors_sacd INTO  IMPUTATION,  ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_hors_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), BE_DB, MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- SACD	--
		OPEN c_sacd;
		LOOP
		FETCH c_sacd INTO  IMPUTATION, ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB;
		EXIT WHEN c_sacd%NOTFOUND;

		debtot  :=  BE_DB  +  MONTANT_DB;
		cretot  :=  BE_CR  +  MONTANT_CR;

		IF debtot - cretot > 0 THEN
			bsdeb := debtot - cretot;
			bscre := 0;
		ELSE
			bscre := cretot - debtot;
			bsdeb := 0;
		END IF;

		compte := IMPUTATION || '               ';

		INSERT INTO EPN_BAL_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), BE_DB, MONTANT_DB, debtot, BE_CR, MONTANT_CR, cretot, bsdeb, bscre, exerc);

		NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_BAL_1 SET EPNB1_NB_ENREG = NUM_SEQ
	WHERE EPNB_ORDRe = ORDRE AND EPNB1_GES_CODE = comp AND EPNB1_EXERCICE = exerc;

END;



PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
DEP_MONT  NUMBER(15,2);
REVERS NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c_tot IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS)
 	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd  IS SELECT PCO_NUM,SUM( DEP_MONT), SUM( REVERS)
	FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY PCO_NUM
	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) FROM EPN_DEP_BUD
	WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

/*DELETE TAB_MANDAT_MAJ;

INSERT INTO TAB_MANDAT_MAJ SELECT * FROM MANDAT_MAJ;*/

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp = 'HSA'  THEN
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
		AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
			AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
		AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
			AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp
			AND EPNDB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
		WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp AND EPNDB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp
			AND EPNDB2_EXERCICE = exerc;
		END IF;

	END IF;


	-- SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre = EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans 	maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE := V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';

	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND EXE_ORDRE = exerc;
	END IF;

	INSERT INTO EPN_DEP_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET, 					EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;
	/*
  	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		DATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
	*/


	-- Pas de consolidation Etablissement + SACD
	/* IF comp = 'ALL'  THEN
	-- Etablissement + SACD --
	OPEN c_tot;
	LOOP
		FETCH c_tot INTO  PCO_NUM,  DEP_MONT, REVERS;
		EXIT WHEN c_tot%NOTFOUND;
		compte := PCO_NUM || '              ';
		INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0,  DEP_MONT-REVERS);
		NUM_SEQ := NUM_SEQ + 1;
	END LOOP;
	CLOSE c_tot;

	ELSE */
	IF comp = 'HSA' THEN
		--  hors SACD
		OPEN c_hors_sacd ;
		LOOP
			FETCH c_hors_sacd INTO  PCO_NUM, DEP_MONT, REVERS ;
			EXIT WHEN c_hors_sacd%NOTFOUND ;
			compte := PCO_NUM || '              ' ;
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0, DEP_MONT-REVERS, exerc) ;
			NUM_SEQ := NUM_SEQ + 1 ;
			END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		--  cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO  PCO_NUM,  DEP_MONT, REVERS;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0, DEP_MONT-REVERS, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;
	END IF;

	-- END IF;

	-- Correction M1 MAJ du Nb enreg de l'�dition du ges_code
	UPDATE EPN_DEP_BUD_1 SET EPNDB1_NBENREG = num_seq
	WHERE EPNDB_ORDRE = ORDRE_NUM AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;

END;


PROCEDURE Epn_Proc_Recbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
IS


NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
MONT_REC  NUMBER(15,2);
ANNUL_TIT_REC NUMBER(15,2);
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
CPT NUMBER;
COD_BUDGET VARCHAR2(2);
ordre_num NUMBER;
TYP_DOC VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c_tot IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND GES_CODE = comp AND exe_ordre = exerc
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;

	CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
 	FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) < VARDATE AND exe_ordre = exerc
 	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
 	GROUP BY PCO_NUM
 	ORDER BY PCO_NUM;


BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
		AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc
			AND EPNRB1_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
		AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_EXERCICE = exerc
			AND EPNRB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
		END IF;

	ELSE
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_1
		WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_EXERCICE = exerc AND EPNRB1_GES_CODE = comp;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_1 WHERE EPNRB_ORDRE = ordre_num AND EPNRB1_GES_CODE = comp
			AND EPNRB1_EXERCICE = exerc;
		END IF;
		SELECT COUNT(*) INTO CPT FROM EPN_REC_BUD_2
		WHERE EPNRB_ORDRE = ordre_num  AND EPNRB2_GES_CODE = comp AND EPNRB2_EXERCICE = exerc;
		IF CPT <> 0  THEN
			DELETE FROM EPN_REC_BUD_2 WHERE EPNRB_ORDRE = ordre_num AND EPNRB2_GES_CODE = comp
			AND EPNRB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;

	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
					RANG := '04';
					VDATE := '3112' || EXERC;
					VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
						RANG := '05';
						VDATE := V_DATE;
  						VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
							RANG := '06';
							VDATE := V_DATE;
							VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');

		/*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
		IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
   	  	 	TYP_DOC := '02';
		ELSE
       		TYP_DOC := '03';
   		END IF;*/

	-- Correction type document --
	TYP_DOC := '03';


	/*	IF  comp = 'ALL' THEN
   			COD_BUDGET := '01';
		ELSE  IF comp =  'HSA' THEN
				COD_BUDGET := '00';
			ELSE
				SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
			END IF;
		END IF;*/

	-- Correction du code budget pour l'�tablissement ou SACD --
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;


	INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp,'1', 1, IDENTIFIANT,TYP_DOC, codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

	NUM_SEQ := 2;

 	/*
	IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
  	END IF;
 	*/

	/*IF comp = 'ALL' THEN
	-- Etablissement + SACD
		OPEN c_tot;
		LOOP
			FETCH c_tot INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_tot%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_tot;

	ELSE */

	IF comp =  'HSA' THEN
	-- 	hors SACD
		OPEN c_hors_sacd;
		LOOP
			FETCH c_hors_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_hors_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, (SUBSTR(compte,1,15)),
				MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_hors_sacd;

	ELSE
		-- cas SACD
		OPEN c_sacd;
		LOOP
			FETCH c_sacd INTO PCO_NUM, MONT_REC, ANNUL_TIT_REC;
			EXIT WHEN c_sacd%NOTFOUND;
			compte := PCO_NUM || '              ';
			INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,
			(SUBSTR(compte,1,15)), MONT_REC, 0, ANNUL_TIT_REC, 0,  MONT_REC - ANNUL_TIT_REC, exerc);
			NUM_SEQ := NUM_SEQ + 1;
		END LOOP;
		CLOSE c_sacd;

	END IF;

	--END IF;

	-- Correction M1 - MAJ Nb enreg en fonction du ges_code
	UPDATE EPN_REC_BUD_1 SET EPNRB1_NBENREG = num_seq
	WHERE EPNRB_ORDRE = ORDRE_NUM AND EPNRB1_GES_CODE = comp AND EPNRB1_EXERCICE = exerc;


END;



PROCEDURE Epn_Cons_Cred_Budget (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)

IS

NUM_SEQ NUMBER(5);
PCO_NUM VARCHAR2(15);
EPN_DATE DATE;
EXERC VARCHAR2(4);
EXERCN1 VARCHAR2(4);
SIRET NUMBER(14);
SIRET_C VARCHAR2(14);
IDENTIFIANT NUMBER(10);
SIREN NUMBER(9);
VDATE VARCHAR2(9);
VDATE1 VARCHAR2(9);
VARDATE DATE;
VARDATE1 DATE;
RANG VARCHAR2(2);
compte VARCHAR2(30);
COD_BUDGET VARCHAR2(2);
CPT NUMBER;
ordre_num     NUMBER;
PCONUM      VARCHAR2(6);
MNTOUV      NUMBER(15,2);
MNTNET      NUMBER(15,2);
MNTNOEMPLOI NUMBER(15,2);
SENSCPT		VARCHAR2(1);
TYP_DOC  VARCHAR2(2);
codeNomenclature VARCHAR2(2);
flag INTEGER;

	CURSOR c2_tot IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c2_hors_sacd IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;

	CURSOR c3_tot  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC  WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
	GROUP BY pco_num_bdn;

	CURSOR c3_hors_sacd  IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
	AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE EXE_ORDRE = exerc)
	GROUP BY pco_num_bdn;


	/*
	CURSOR c2 IS
	SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
	FROM EPN_DEP
	GROUP BY pco_num_bdn;

	CURSOR c3 IS
	SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
	FROM EPN_REC
	GROUP BY pco_num_bdn;
	*/

BEGIN

	ORDRE_NUM := ordre;
	EXERC := EXE_ORDRE;
	codeNomenclature := epn_getCodeNomenclature(EXERC);

	DELETE FROM BUDNAT_PLANCO;

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,2), '1'
	FROM maracuja.PLAN_COMPTABLE
	WHERE (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%') AND PCO_VALIDITE = 'VALIDE';

	INSERT INTO BUDNAT_PLANCO
	SELECT PCO_NUM, SUBSTR(PCO_NUM,1,3), '2'
	FROM maracuja.PLAN_COMPTABLE
	WHERE ( PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%')AND PCO_VALIDITE = 'VALIDE';

	/* des donn�es du cadre 2 */

	DELETE FROM EPN_DEP;

	INSERT INTO EPN_DEP
	-- depenses + OR --
	SELECT
		db.PCO_NUM,
		bp.PCO_NUM_BDN,
		db.GES_CODE,
		db.DEP_MONT MDT_MONT,
		db.REVERS TIT_MONT,
		0 CO,
		bp.SECTION,
		db.EPN_DATE EPN_DATE,
		db.EXE_ORDRE
		FROM EPN_DEP_BUD db, BUDNAT_PLANCO bp
		WHERE db.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_DEP
	-- OR --
	SELECT
	  EPN_DEP0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_DEP0.GES_CODE,
	  EPN_TITV.ECD_DATE EPN_DATE,
	  0,
	  EPN_TITV.ECD_MONTANT TIT_MONT,
	  EPN_DEP0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITV, EPN_DEP0, BUDNAT_PLANCO
	WHERE EPN_DEP0.GES_CODE = EPN_TITV.GES_CODE (+)
	AND EPN_DEP0.PCO_NUM = EPN_TITV.PCO_NUM (+)
	AND EPN_DEP0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_DEP 	SET MDT_MONT = 0
		WHERE MDT_MONT IS NULL;

	UPDATE EPN_DEP	SET TIT_MONT = 0
		WHERE TIT_MONT IS NULL;*/

	DELETE FROM EPN_DEP WHERE MDT_MONT = 0 AND CO = 0 AND TIT_MONT = 0;

	INSERT INTO EPN_DEP
	-- pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn , BUDNAT_PLANCO bp
	WHERE bn.PCO_NUM = bp.PCO_NUM_bdn
	AND (bn.PCO_NUM LIKE '6%' OR bn.PCO_NUM LIKE '2%')
	AND CO <> 0
	ORDER BY exe_ordre, ges_code, bn.pco_num;


	/* Impossible de faire des pr�visions budg�taires de d�penses classe 1 dans jefyco --
	INSERT INTO EPN_DEP
	SELECT UNIQUE EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .PCO_NUM, EPN_BUDNAT .GES_CODE, DATE_CO,
	0, 0, EPN_BUDNAT .CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT .PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT .PCO_NUM
	AND BUDNAT_PLANCO.PCO_NUM LIKE '1%';*/

/* des donn�es du cadre 3 */

	DELETE FROM EPN_REC;

	INSERT INTO EPN_REC
	-- Recettes et D�ductions--
	SELECT erb.PCO_NUM,
	  bp.PCO_NUM_BDN,
	  erb.GES_CODE,
	  erb.MONT_REC TIT_MONT,
	  erb.ANNUL_TIT_REC RED_MONT,
	  0 CO,
	  bp.SECTION,
	  erb.EPN_DATE,
	  erb.exe_ordre
	FROM EPN_REC_BUD erb, BUDNAT_PLANCO bp
	WHERE erb.PCO_NUM = bp.PCO_NUM;

	/*INSERT INTO EPN_REC
	-- Reduction recettes --
	SELECT EPN_REC0.PCO_NUM,
	  BUDNAT_PLANCO.PCO_NUM_BDN,
	  EPN_REC0.GES_CODE,
	  EPN_TITD.ECD_DATE EPN_DATE,
	  0 TIT_MONT,
	  EPN_TITD.ECD_MONTANT RED_MONT,
	  EPN_REC0.CO CO,
	  BUDNAT_PLANCO.SECTION
	FROM EPN_TITD,  EPN_REC0, BUDNAT_PLANCO
	WHERE EPN_REC0.GES_CODE = EPN_TITD.GES_CODE (+)
	AND EPN_REC0.PCO_NUM = EPN_TITD.PCO_NUM (+)
	AND EPN_REC0.PCO_NUM = BUDNAT_PLANCO.PCO_NUM;


	UPDATE EPN_REC SET TIT_MONT = 0 WHERE TIT_MONT IS NULL;

	UPDATE EPN_REC SET RED_MONT = 0 WHERE RED_MONT IS NULL;*/

	DELETE FROM EPN_REC WHERE TIT_MONT = 0 AND RED_MONT = 0 AND CO = 0;


	INSERT INTO EPN_REC
	-- Pr�visions budg�taires --
	SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
	FROM EPN_BUDNAT bn, BUDNAT_PLANCO bp
	WHERE bp.PCO_NUM_BDN = bn.PCO_NUM
	AND ( bn.PCO_NUM LIKE '7%' OR bn.PCO_NUM LIKE '1%')
	AND CO <> 0;


	/* Impossible de faire des pr�visions budg�taires de recettes classe 2 dans jefyco
	INSERT INTO EPN_REC
	SELECT UNIQUE EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.PCO_NUM, EPN_BUDNAT.GES_CODE, DATE_CO, 0, 0,
	EPN_BUDNAT.CO, BUDNAT_PLANCO.SECTION
	FROM EPN_BUDNAT , BUDNAT_PLANCO, EPN_DEP
	WHERE BUDNAT_PLANCO.PCO_NUM_BDN LIKE EPN_BUDNAT.PCO_NUM||'%'
	AND EPN_DEP.PCO_NUM_BDN = EPN_BUDNAT.PCO_NUM
	AND (BUDNAT_PLANCO.PCO_NUM LIKE '26%' OR BUDNAT_PLANCO.PCO_NUM LIKE '27%');*/



	IF comp = 'HSA' THEN
		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
		AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_EXERCICE = exerc
			AND EPNCCB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2
		WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
		AND EPNCCB2_GES_CODE  NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_EXERCICE = exerc
			AND EPNCCB2_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
		END IF;

	ELSE

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_1 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB1_GES_CODE = comp
			AND EPNCCB1_EXERCICE = exerc;
		END IF;

		SELECT COUNT(*) INTO CPT FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num
		AND EPNCCB2_GES_CODE = comp AND EPNCCB2_EXERCICE = exerc;
		IF CPT > 0  THEN
			DELETE FROM EPN_CRE_BUD_2 WHERE EPNCCB_ORDRE = ordre_num AND EPNCCB2_GES_CODE = comp
			AND EPNCCB2_EXERCICE = exerc;
		END IF;

	END IF;


	--   SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

	SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C
	FROM maracuja.PARAMETRE WHERE PAR_KEY = 	'NUMERO_SIRET' AND exe_ordre=EXERC;
	SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
	SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

	SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
	WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre=EXERC;
	IF IDENTIFIANT IS NULL THEN
		RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseign� dans maracuja.PARAMETRE');
	END IF;


	IF ordre = '1' THEN
		RANG := '01';
		VDATE := '3103' || EXERC;
		VDATE1 := '3103' || EXERC;
	ELSE IF ordre =  '2' THEN
			RANG := '02';
			VDATE := '3006' || EXERC;
			VDATE1 := '3006' || EXERC;
		ELSE IF ordre = '3' THEN
				RANG := '03';
				VDATE := '3009' || EXERC;
				VDATE1 := '3009' || EXERC;
			ELSE IF ordre = '4' THEN
				RANG := '04';
				VDATE := '3112' || EXERC;
				VDATE1 := '3112' || EXERC;
				ELSE IF ordre = '5' THEN
					RANG := '05';
					VDATE:= V_DATE;
					VDATE1 := '3112' || EXERC;
					ELSE IF ordre = '6' THEN
						RANG := '06';
						VDATE := V_DATE;
						VDATE1 := '3112' || EXERC;
						ELSE
							RAISE_APPLICATION_ERROR (-20001,'type �dition non renseign�');
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

	VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
	VARDATE1 := TO_DATE(VDATE1,'dd/mm/yyyy');

	--	type doc = '03'
	-- SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;

	--IF (comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
	-- Correction M1
	TYP_DOC := '03';


	-- Correction M1 recherche du code du budget
	IF comp = 'HSA' THEN
		COD_BUDGET := '01';
	ELSE
		SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
		IF (flag=0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
		END IF;
		SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
		WHERE sacd = comp AND exe_ordre = exerc;
	END IF;

	INSERT INTO EPN_CRE_BUD_1 VALUES (ORDRE,comp, '1', 1, IDENTIFIANT,TYP_DOC , codeNomenclature, COD_BUDGET,
	EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);


	/*
	  IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
  	SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
	--		   VDATE := '31122005';
		   VDATE := '3112'|| EXERCN1;
	 	   VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
     END IF;
	*/

	NUM_SEQ := 2;

	/*IF comp = 'ALL' THEN
		OPEN c2_tot;
		LOOP
			FETCH c2_tot INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
			ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2
			VALUES (ORDRE, comp, '2', num_seq, 1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_tot;

		OPEN c3_tot;
		LOOP
			FETCH c3_tot INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_tot%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';
			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_tot;

	ELSE */
	IF comp =  'HSA' THEN

		OPEN c2_hors_sacd;
		LOOP
			FETCH c2_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1,  compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_hors_sacd;

		OPEN c3_hors_sacd;
		LOOP
			FETCH c3_hors_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_hors_sacd%NOTFOUND;
			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN
				MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE   MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c3_hors_sacd;

	ELSE
		OPEN c2_sacd;
		LOOP
			FETCH c2_sacd INTO  PCONUM,  MNTOUV, MNTNET,  MNTNOEMPLOI;
			EXIT WHEN c2_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '            ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '6' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1, compte, MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

		END LOOP;
		CLOSE c2_sacd;


		OPEN c3_sacd;
		LOOP
			FETCH c3_sacd INTO  PCONUM,  MNTOUV, MNTNET;
			EXIT WHEN c3_sacd%NOTFOUND;

			compte := (SUBSTR(PCONUM,1,3)) || '             ';

			IF (SUBSTR(compte,1,1)) = '1' OR (SUBSTR(compte,1,1)) = '7' THEN
				SENSCPT := 'R';
				ELSE IF (SUBSTR(compte,1,1)) = '2' OR (SUBSTR(compte,1,1)) = '2' THEN
					SENSCPT := 'D';
				END IF;
			END IF;

			IF (SUBSTR(compte,1,1)) = '6' OR (SUBSTR(compte,1,1)) = '7' THEN
				compte := compte  || ' ';
			END IF;

			IF MNTNET < MNTOUV THEN MNTNOEMPLOI := MNTOUV - MNTNET;
				ELSE  MNTNOEMPLOI := MNTNET - MNTOUV;
			END IF;

			INSERT INTO EPN_CRE_BUD_2 VALUES (ORDRE,comp, '2', num_seq,1,  (SUBSTR(compte,1,15)), MNTOUV, NULL, MNTNOEMPLOI, NULL, SENSCPT, exerc);

			NUM_SEQ := NUM_SEQ + 1;

			END LOOP;
			CLOSE c3_sacd;

	END IF;

	--END IF;

UPDATE EPN_CRE_BUD_1 SET EPNCCB1_NBENREG = num_seq
WHERE EPNCCB_ORDRE = ORDRE_NUM AND EPNCCB1_GES_CODE = comp AND EPNCCB1_EXERCICE = exerc;

END;

END;
/


SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Basculer_Be IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree.

Version du 09/01/2008 

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);
PROCEDURE basculer_detail_du_cpt_ges_n(pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT LONG);


-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR, gescode VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
 PROCEDURE priv_archiver_la_bascule_ecd ( ecdordre	INTEGER, utlordre   INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR;

FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER;
FUNCTION priv_getTopOrdre RETURN NUMBER;
FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	  topordre := priv_getTopOrdre;

--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );
--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND pco_num = pconumtmp;
--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND pco_num = pconumtmp;

--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
--
--          --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);
--          SELECT ABS (lesdebits - lescredits)
--            INTO lesolde
--            FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND pco_num = pconum;
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;

         --raise_application_error (-20001,' '||lesdebits||' '||lesens||' '||lesens890||' '||lesolde);
--          SELECT top_ordre
--            INTO topordre
--            FROM TYPE_OPERATION
--           WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
--
--          SELECT com_ordre, ges_code
--            INTO comordre, gescodeagence
--            FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );


--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
-- --and ges_code = gescode
--          AND pco_num = pconum;
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
-- --and ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
	  priv_nettoie_ecriture(monecdordre);
   END;

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

	  chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;


		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );

--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;
--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;


 -- Cr�e une ecriture par ecriture_detail recupere
PROCEDURE basculer_detail_du_cpt_ges_n (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
	  gescode			VARCHAR,
      ecrordres   OUT   LONG
   )
   IS
      cpt             INTEGER;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
	  leGesCode       ECRITURE_DETAIL.ges_code%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monEcrOrdre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesEcd
      IS
         SELECT ecd_ordre, ecd_sens, ges_code, ecd_libelle,NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_reste_emarger <> 0
            AND pco_num = pconum
			AND ges_code=gescode;

   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

	  ecrordres := '';
      OPEN lesEcd;

      LOOP
         FETCH lesEcd
          INTO ecdordreOld, lesens, leGesCode, ecdlibelle, lemontant;
         EXIT WHEN lesEcd%NOTFOUND;

		-- creation de lecriture  --
		monEcrOrdre := maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

         -- creation du detail ecriture --
         ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,       --ECDSENS,
                                                     monecrordre,  --ECRORDRE,
                                                     leGesCode, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
		  -- basculer les titre_ecriture_detail  --
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);

		  -- creation de du log pour ne plus retraiter les ecritures ! --
		  priv_archiver_la_bascule_ecd(ecdordreOld, utlordre);

		  IF (lesens='C') THEN
			lesens890 := 'D';
		  ELSE
			lesens890 := 'C';
		  END IF;
		  -- creer contrepartie



		  ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                    'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '|| pconum,--ECDLIBELLE,
                                                    lemontant,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    monecrordre,      --ECRORDRE,
                                                    priv_getGesCodeCtrePartie(exeordre, legescode),  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
		  maracuja.Api_Plsql_Journal.validerecriture (monEcrOrdre);
		  ecrordres := ecrordres || monecrordre || '$';

      END LOOP;
      CLOSE lesEcd;


   END;


-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

			  chaine := lespconums;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );

--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

		   chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
--
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
--
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                       substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

	   SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
    	IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a �t� transf�r�e en BE pour le compte '|| pconum);
    	END IF;

		DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;



-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
--
--    -- PRIVATE --
--    PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR)
--    IS
--       cpt             INTEGER;
--       lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
--       lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
--       topordre        TYPE_OPERATION.top_ordre%TYPE;
--       comordre        COMPTABILITE.com_ordre%TYPE;
--       ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
--       gescodeagence   COMPTABILITE.ges_code%TYPE;
-- 	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
--       monecdordre     INTEGER;
--    BEGIN
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;
--
-- 		SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';
--
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';
--
--       IF (ABS (lesdebits) >= ABS (lescredits))
--       THEN
--          lesens890 := 'C';
--          lesolde := lesdebits - lescredits;
--       ELSE
--          lesens890 := 'D';
--          lesolde := lescredits - lesdebits;
--       END IF;
--
-- --       SELECT ABS (lesdebits - lescredits)
-- --         INTO lesolde
-- --         FROM DUAL;
-- --
--  --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde||' '||ecrordre);
--       IF lesolde != 0
--       THEN
-- --           IF (lesdebits >= lescredits)
-- --           THEN
-- --              lesens890 := 'C';
-- --           ELSE
-- --              lesens890 := 'D';
-- --           END IF;
--
--          -- creation du detail pour l'ecriture selon le CREDIT --
--          ecdordre :=
--             maracuja.Api_Plsql_Journal.creerecrituredetail
--                                                    (NULL,    --ECDCOMMENTAIRE,
--                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
--                                                     || pconumlibelle,
--                                                     --ECDLIBELLE,
--                                                     lesolde,     --ECDMONTANT,
--                                                     NULL,     --ECDSECONDAIRE,
--                                                     lesens890,      --ECDSENS,
--                                                     ecrordre,      --ECRORDRE,
--                                                     gescodeagence,  --GESCODE,
--                                                     '890'             --PCONUM
--                                                    );
-- -- Rod : ne pas bloquer (sinon pas possible de reprendre detail equilibre)
-- --      ELSE
-- --          raise_application_error (-20001,
-- --                                      'OUPS PROBLEME ECRITURE NON fred!!! '
-- --                                   || lesdebits
-- --                                   || ' '
-- --                                   || lescredits
-- --                                  );
--       END IF;
--    END;



PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR, gescode VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;

	  SELECT com_ordre, ges_code INTO comordre, gescodeagence
        FROM COMPTABILITE;

		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		gescodeagence := priv_getGesCodeCtrePartie(exeordre, gescode);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         -- creation du detail pour l'ecriture
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
      END IF;
   END;






   PROCEDURE priv_archiver_la_bascule_ecd (
      ecdordre	INTEGER,
      utlordre   INTEGER
   )
   IS
   BEGIN

		INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre, ecdordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre = ecdordre
			 AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG);

   END;


   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum;
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
   		DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR
   IS
   	 pconumnew PLAN_COMPTABLE.pco_num%TYPE;
   BEGIN
   		SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE WHERE pco_num=pconumold;
		IF pconumnew IS NULL THEN
		   pconumnew := pconumold;
		END IF;
		RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
    	IF cpt = 1 THEN
    		SELECT * INTO infos
       		FROM TITRE_DETAIL_ECRITURE
       	 WHERE ecd_ordre = ecdordreold;
      	INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
			   VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
    	END IF;


    END;

	-- renvoi le solde non emarge des ecritures non prises en compte en BE
	FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER
	IS
	  lesolde NUMBER;
	BEGIN
	   IF (gescode IS NULL) THEN
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;
		ELSE
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
				 AND ges_code = gescode
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;
		END IF;
		RETURN lesolde;
	END;


	FUNCTION priv_getTopOrdre RETURN NUMBER
	IS
	  topordre NUMBER;
	BEGIN
	      SELECT top_ordre INTO topordre
        		FROM TYPE_OPERATION
       			WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
			RETURN  topordre;
	END;

	-- renvoie gescode si sacd ou gescodeagence
	FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR
	IS
	  gescodeagence COMPTABILITE.ges_code%TYPE;
	  cpt INTEGER;
	BEGIN
	  SELECT ges_code INTO gescodeagence
        FROM COMPTABILITE;

		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		IF gescode IS NOT NULL THEN
		   		   SELECT COUNT(*) INTO cpt FROM GESTION_EXERCICE
				   		  WHERE exe_ordre = exeordre AND ges_code=gescode AND pco_num_185 IS NOT NULL;
					IF cpt<>0 THEN
					   		  gescodeagence := gescode;
					END IF;
		END IF;

		RETURN gescodeagence;

	END;



END;
/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
   WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont>=0
  AND ecd.ecd_credit=0
  AND ecd.ecd_debit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'  
GROUP BY r.rec_id
union all
SELECT  r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
  FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
  WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont>=0
  AND ecd.ecd_credit=0
  AND ecd.ecd_debit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='R'
  and not exists(select tde_ordre from titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe where xx.ecd_ordre=xecd.ecd_ordre and xecd.exe_ordre=xe.exe_ordre and xe.exe_stat='O' and xx.tit_id=r.tit_id)  
GROUP BY r.rec_id
UNION ALL
SELECT   r.rec_id, -SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont<0
  AND ecd.ecd_debit=0
  AND ecd.ecd_credit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'
GROUP BY r.rec_id
union all
SELECT  r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
  FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
  WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont<0
  AND ecd.ecd_debit=0
  AND ecd.ecd_credit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='R'
  and not exists(select tde_ordre from titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe where xx.ecd_ordre=xecd.ecd_ordre and xecd.exe_ordre=xe.exe_ordre and xe.exe_stat='O' and xx.tit_id=r.tit_id)  
GROUP BY r.rec_id;


