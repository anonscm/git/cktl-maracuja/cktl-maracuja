set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA, GARNUCHE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.9
-- Date de publication : 25/01/2013
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Correction des origines de VISA pour les mandats/ODP générés par Scolarix
-- Modification de la procedure de scolarix qui crée les mandats/ODP
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.2.8', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.2.9', null );
commit ;




CREATE OR REPLACE PROCEDURE GARNUCHE.Ins_Bordereau_Rembourse_New
(
  borid             NUMBER,
  erembnumbordereau NUMBER,
  erembannee        NUMBER,
  gescode           VARCHAR2,
  etabcode          VARCHAR2,
  libelle           VARCHAR2,
  utlordre          NUMBER
)
IS
  /*******************************/
  /* NOUVELLE VERSION 2006/10/16 */
  /*******************************/
  /* Depuis la rentrée 2006/2007 CUM remplace AGM */
  v_reference   HISTORIQUE.hist_annee_scol%TYPE;
  histanneescol HISTORIQUE.hist_annee_scol%TYPE;
  mynom         VARCHAR2(80);
  myprenom      VARCHAR2(60);
  mymarital     VARCHAR2(50);
  mylibelle     VARCHAR2(80);
  mymodcode     VARCHAR2(03);
  nbenr         INTEGER;
  myetudiant    NUMBER;
  myhistorique  NUMBER;
  myinscription NUMBER;
  myrembourse   NUMBER;
  mysomme       NUMBER(12,2);
  mysco         NUMBER(12,2);
  mybu          NUMBER(12,2);
  mycmp         NUMBER(12,2);
  mymed         NUMBER(12,2);
  myfav         NUMBER(12,2);
  myaud         NUMBER(12,2);
  myspo         NUMBER(12,2);
  mycum         NUMBER(12,2);
  myaff         NUMBER(12,2);
  mycm          NUMBER(12,2);
  mysec         NUMBER(12,2);
  mymna         NUMBER(12,2);
  mymnb         NUMBER(12,2);
  mymnc         NUMBER(12,2);
  mymnd         NUMBER(12,2);
  mymne         NUMBER(12,2);
  mysma         NUMBER(12,2);
  mysmb         NUMBER(12,2);
  mysmc         NUMBER(12,2);
  mysmd         NUMBER(12,2);
  mysme         NUMBER(12,2);
  mysmf         NUMBER(12,2);
  r_man         maracuja.mandat%ROWTYPE;
  r_dep         maracuja.depense%ROWTYPE;
  r_mab         maracuja.mandat_brouillard%ROWTYPE;
  CURSOR c1
  IS SELECT UNIQUE er.hist_numero, er.eremb_numero,
            er.rib_ordre, rb.fou_ordre, NVL(rb.mod_code,90)
     FROM   DETAIL_REMBOURSE dr,
            ETUDIANT_REMBOURSE er,
			grhum.ribfour_ulr rb
     WHERE  er.rib_ordre           = rb.rib_ordre (+)
	 AND    dr.eremb_numero        = er.eremb_numero
     AND    dr.dremb_etat          = 'V'
     AND    er.eremb_num_bordereau = erembnumbordereau
     AND    er.etab_code           = etabcode
     AND    er.eremb_annee         = erembannee;
  CURSOR c1_bis
  IS SELECT UNIQUE dr.idipl_numero
     FROM   DETAIL_REMBOURSE dr,
            ETUDIANT_REMBOURSE er
     WHERE  dr.eremb_numero        = er.eremb_numero
     AND    dr.dremb_etat          = 'V'
	 AND    er.hist_numero         = myhistorique
     AND    er.eremb_num_bordereau = erembnumbordereau
     AND    er.etab_code           = etabcode
     AND    er.eremb_annee         = erembannee;
  CURSOR c2
  IS SELECT rp.pco_num, pg.ges_code, ABS(dr.dremb_montant), 'D'
     FROM   DETAIL_REMBOURSE dr,
	        REPARTITION_COMPTE rp,
			PLANCO_GESTION pg
     WHERE  pg.etab_code    = etabcode
	 AND    pg.pco_num      = rp.pco_num
	 AND    dr.rcpt_code    = rp.rcpt_code
	 AND    dr.eremb_numero = myrembourse
	 ORDER BY rp.pco_num;
BEGIN
  -- Initialisations...
  -- MANDAT
  r_man.bor_id              := borid;
  r_man.brj_ordre           := NULL;
  r_man.exe_ordre           := erembannee;
  r_man.ges_code            := gescode;
  r_man.man_date_remise     := NULL;
  r_man.man_date_visa_princ := NULL;
  r_man.man_etat            := 'ATTENTE';
  r_man.man_etat_remise     := 'ATTENTE';
  r_man.man_motif_rejet     := NULL;
  r_man.man_nb_piece        := 1;
  r_man.man_numero_rejet    := NULL;
  r_man.man_orgine_key      := NULL;
  r_man.man_origine_lib     := NULL;
  r_man.man_tva             := 0;
  r_man.ori_ordre           := NULL;
  r_man.prest_id            := NULL;
  r_man.tor_ordre           := 2;
  r_man.pai_ordre           := NULL;
  r_man.org_ordre           := NULL;

  r_man.pco_num := garnuche.F_Recup_Garnuche_Param_String('param_commentaires','param_key = ''GARNUCHE_CODE_REMBOURSE'' AND param_value = '''||etabcode||'''');
  v_reference   := TO_NUMBER(grhum.F_Recup_PValue_Number_For_User('SCOLARITE','SCOL','param_key = ''SCOL_ANNEE_EXPLOITATION'''));

  -- Initialisations...
  -- DEPENSE
  r_dep.dep_date_compta      := NULL;
  r_dep.dep_date_reception   := SYSDATE;
  r_dep.dep_date_service     := SYSDATE;
  r_dep.dep_etat             := 'VALIDE';
  r_dep.dep_ligne_budgetaire := ' ';
  r_dep.dep_lot              := NULL;
  r_dep.dep_marches          := NULL;
  r_dep.dep_nomenclature     := NULL;
  r_dep.dep_rejet            := NULL;
  r_dep.dep_suppression      := 'NON';
  r_dep.dep_tva              := 0;
  r_dep.exe_ordre            := erembannee;
  r_dep.ges_code             := gescode;
  r_dep.pco_ordre            := r_man.pco_num;
  r_dep.utl_ordre            := utlordre;
  r_dep.org_ordre            := NULL;
  r_dep.tcd_ordre            := NULL;

  -- Initialisations...
  -- MANDAT_BROUILLARD
  r_mab.ecr_ordre     := NULL;
  r_mab.exe_ordre     := erembannee;

  -- Suppression des anciennes données
  DELETE FROM BORDEREAU_REMBOURSE
  WHERE eremb_num_bordereau = erembnumbordereau
  AND   etab_code           = etabcode
  AND   eremb_annee         = erembannee;

  -- Traitement des nouvelles données
  OPEN c1;
  LOOP
    FETCH c1 INTO myhistorique,myrembourse,r_man.rib_ordre_ordonnateur,r_man.fou_ordre,mymodcode;
    EXIT WHEN c1%NOTFOUND;

    -- Recherche des informations complémentaires
	SELECT UNIQUE hist_annee_scol
	INTO   histanneescol
	FROM   HISTORIQUE
	WHERE  hist_numero  = myhistorique;

	IF (histanneescol >= v_reference)
	THEN -- Version LMD
         SELECT UNIQUE etud_numero, hist_numero,
                adr_nom, adr_prenom, etud_nom_marital
         INTO   myetudiant, myhistorique,
                mynom, myprenom, mymarital
         FROM   scolarite.scol_inscription_etudiant
         WHERE  hist_numero = myhistorique;
	ELSE -- Version Classique
         SELECT UNIQUE etud_numero, hist_numero,
                adr_nom, adr_prenom, etud_nom_marital
         INTO   myetudiant, myhistorique,
                mynom, myprenom, mymarital
         FROM   peda.pedagogie_etudiant
         WHERE  hist_numero = myhistorique;
	END IF;

	SELECT ll_rne
	INTO   mylibelle
	FROM   grhum.rne
	WHERE  c_rne = etabcode;

	-- PARTIE PAIEMENT !!!
	----------------------
    mybu  := 0; mycmp := 0; mymed := 0;
    myaud := 0; myspo := 0; mycum := 0;
    myaff := 0; mycm  := 0; mysec := 0;
    mymna := 0; mymnb := 0; mymnc := 0;
    mymnd := 0; mymne := 0; mysma := 0;
    mysmb := 0; mysmc := 0; mysmd := 0;
    mysme := 0; mysmf := 0; mysco := 0;
	myfav := 0; mysomme := 0;

	OPEN c1_bis;
	LOOP
      FETCH c1_bis INTO myinscription;
      EXIT WHEN c1_bis%NOTFOUND;

      -- Recherche Générale du détail des paiements
      mybu  := mybu  + Recuperer_Montant(myinscription,'BU','P');
      mycmp := mycmp + Recuperer_Montant(myinscription,'CMP','P');
      mymed := mymed + Recuperer_Montant(myinscription,'MED','P');
      myaud := myaud + Recuperer_Montant(myinscription,'AUD','P');
      myspo := myspo + Recuperer_Montant(myinscription,'SPO','P');
      mycum := mycum + Recuperer_Montant(myinscription,'CUM','P');
      myaff := myaff + Recuperer_Montant(myinscription,'AFF','P');
      mycm  := mycm  + Recuperer_Montant(myinscription,'CM','P');
      mysec := mysec + Recuperer_Montant(myinscription,'SEC','P');
      mymna := mymna + Recuperer_Montant(myinscription,'MNA','P');
      mymnb := mymnb + Recuperer_Montant(myinscription,'MNB','P');
      mymnc := mymnc + Recuperer_Montant(myinscription,'MNC','P');
      mymnd := mymnd + Recuperer_Montant(myinscription,'MND','P');
      mymne := mymne + Recuperer_Montant(myinscription,'MNE','P');
      mysma := mysma + Recuperer_Montant(myinscription,'SMA','P');
      mysmb := mysmb + Recuperer_Montant(myinscription,'SMB','P');
      mysmc := mysmc + Recuperer_Montant(myinscription,'SMC','P');
      mysmd := mysmd + Recuperer_Montant(myinscription,'SMD','P');
      mysme := mysme + Recuperer_Montant(myinscription,'SME','P');
      mysmf := mysmf + Recuperer_Montant(myinscription,'SMF','P');

      -- Recherche Spécifiques du détail des paiements
      mysco := mysco + Recuperer_Montant(myinscription,'SCO','P');
      mysco := mysco + Recuperer_Montant(myinscription,'ULR','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC1','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC2','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC3','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC4','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC5','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC6','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC7','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC8','P');
      mysco := mysco + Recuperer_Montant(myinscription,'SC9','P');

      -- Recherche Spécifiques du détail des paiements
      myfav := myfav + Recuperer_Montant(myinscription,'FAV','P');

	  IF (myfav = 0.00)
	  THEN myfav := myfav + Recuperer_Montant(myinscription,'FSD','P');
	  END IF;

	END LOOP;
	CLOSE c1_bis;

	-- Recherche du total des paiements
/*
    SELECT SUM(UNIQUE pa.paie_somme)
    INTO   mysomme
    FROM   PAIEMENT pa, DETAIL_PAIEMENT dp
    WHERE  pa.paie_numero = dp.paie_numero
	AND    dp.eremb_numero IS NULL
	AND    dp.idipl_numero = myinscription;
*/
    SELECT SUM(UNIQUE paie_somme)
    INTO   mysomme
    FROM   PAIEMENT
	WHERE  paie_numero IN
    (
	  SELECT UNIQUE paie_numero
      FROM   DETAIL_PAIEMENT
      WHERE  eremb_numero IS NULL
	  AND    idipl_numero IN
	  (
	    SELECT UNIQUE dr.idipl_numero
        FROM   DETAIL_REMBOURSE dr,
               ETUDIANT_REMBOURSE er
        WHERE  dr.eremb_numero        = er.eremb_numero
        AND    dr.dremb_etat          = 'V'
	    AND    er.hist_numero         = myhistorique
        AND    er.eremb_num_bordereau = erembnumbordereau
        AND    er.etab_code           = etabcode
        AND    er.eremb_annee         = erembannee
	  )
	);

    -- Insertion de la ligne des paiements
    INSERT INTO BORDEREAU_REMBOURSE
    VALUES (erembnumbordereau,erembannee,myrembourse,'P',
	        etabcode,mylibelle,
            myetudiant,myhistorique,mynom,myprenom,mymarital,
            mysco,mybu,mycmp,mymed,myfav,myaud,myspo,mycum,myaff,mycm,mysec,
		    mymna,mymnb,mymnc,mymnd,mymne,mysma,mysmb,mysmc,mysmd,mysme,mysmf,
		    mysomme);

	-- PARTIE REMBOURSEMENT !!!
	---------------------------
    mybu  := 0; mycmp := 0; mymed := 0;
    myaud := 0; myspo := 0; mycum := 0;
    myaff := 0; mycm  := 0; mysec := 0;
    mymna := 0; mymnb := 0; mymnc := 0;
    mymnd := 0; mymne := 0; mysma := 0;
    mysmb := 0; mysmc := 0; mysmd := 0;
    mysme := 0; mysmf := 0; mysco := 0;
	myfav := 0; mysomme := 0;

	OPEN c1_bis;
	LOOP
      FETCH c1_bis INTO myinscription;
      EXIT WHEN c1_bis%NOTFOUND;

      -- Recherche Générale du détail des paiements
      mybu  := mybu  + Recuperer_Montant(myinscription,'BU','R');
      mycmp := mycmp + Recuperer_Montant(myinscription,'CMP','R');
      mymed := mymed + Recuperer_Montant(myinscription,'MED','R');
      myaud := myaud + Recuperer_Montant(myinscription,'AUD','R');
      myspo := myspo + Recuperer_Montant(myinscription,'SPO','R');
      mycum := mycum + Recuperer_Montant(myinscription,'CUM','R');
      myaff := myaff + Recuperer_Montant(myinscription,'AFF','R');
      mycm  := mycm  + Recuperer_Montant(myinscription,'CM','R');
      mysec := mysec + Recuperer_Montant(myinscription,'SEC','R');
      mymna := mymna + Recuperer_Montant(myinscription,'MNA','R');
      mymnb := mymnb + Recuperer_Montant(myinscription,'MNB','R');
      mymnc := mymnc + Recuperer_Montant(myinscription,'MNC','R');
      mymnd := mymnd + Recuperer_Montant(myinscription,'MND','R');
      mymne := mymne + Recuperer_Montant(myinscription,'MNE','R');
      mysma := mysma + Recuperer_Montant(myinscription,'SMA','R');
      mysmb := mysmb + Recuperer_Montant(myinscription,'SMB','R');
      mysmc := mysmc + Recuperer_Montant(myinscription,'SMC','R');
      mysmd := mysmd + Recuperer_Montant(myinscription,'SMD','R');
      mysme := mysme + Recuperer_Montant(myinscription,'SME','R');
      mysmf := mysmf + Recuperer_Montant(myinscription,'SMF','R');

      -- Recherche Spécifiques du détail des paiements
      mysco := mysco + Recuperer_Montant(myinscription,'SCO','R');
      mysco := mysco + Recuperer_Montant(myinscription,'ULR','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC1','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC2','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC3','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC4','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC5','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC6','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC7','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC8','R');
      mysco := mysco + Recuperer_Montant(myinscription,'SC9','R');

      -- Recherche Spécifiques du détail des paiements
      myfav := myfav + Recuperer_Montant(myinscription,'FAV','R');

	  IF (myfav = 0.00)
	  THEN myfav := myfav + Recuperer_Montant(myinscription,'FSD','R');
	  END IF;

	END LOOP;
	CLOSE c1_bis;

    -- Recherche du total des remboursements
    SELECT eremb_somme
    INTO   mysomme
    FROM   ETUDIANT_REMBOURSE
    WHERE  eremb_numero = myrembourse;

    -- Insertion de la ligne des remboursements
    INSERT INTO BORDEREAU_REMBOURSE
    VALUES (erembnumbordereau,erembannee,myrembourse,'R',
	        etabcode,mylibelle,
            myetudiant,myhistorique,mynom,myprenom,mymarital,
            mysco,mybu,mycmp,mymed,myfav,myaud,myspo,mycum,myaff,mycm,mysec,
		    mymna,mymnb,mymnc,mymnd,mymne,mysma,mysmb,mysmc,mysmd,mysme,mysmf,
		    mysomme);

    -- Traitement MARACUJA
	--
	-- MANDAT
	--
	r_man.man_numero           := myrembourse;
	r_man.man_ordre            := 0; -- modif rodolphe
	r_man.rib_ordre_comptable  := r_man.rib_ordre_ordonnateur;
	r_man.man_attente_paiement := 4;

	SELECT COUNT(*)
	INTO   nbenr
	FROM   maracuja.mode_paiement
	WHERE  mod_dom   = 'VIREMENT'
	AND    mod_code  = mymodcode
	AND    exe_ordre = erembannee;

	IF (nbenr > 0)
	THEN SELECT mod_ordre
	     INTO   r_man.mod_ordre
	     FROM   maracuja.mode_paiement
	     WHERE  mod_dom   = 'VIREMENT'
	     AND    mod_code  = mymodcode
	     AND    exe_ordre = erembannee;
	ELSE SELECT mod_ordre
	     INTO   r_man.mod_ordre
	     FROM   maracuja.mode_paiement
	     WHERE  mod_dom   = 'INTERNE'
	     AND    mod_code  = mymodcode
	     AND    exe_ordre = erembannee;

/*
		 SELECT fou_ordre
		 INTO   r_man.fou_ordre
		 FROM   grhum.fournis_ulr
		 WHERE  fou_code = 'BID00000';
*/
         r_man.fou_ordre := TO_NUMBER(garnuche.F_Recup_Garnuche_Pvalue_Number('param_key = ''GARNUCHE_FOURNISSEUR_DEFAULT'''));
	END IF;

	SELECT maracuja.mandat_seq.NEXTVAL
	INTO   r_man.man_id
	FROM   dual;

	INSERT INTO maracuja.mandat
	VALUES (r_man.bor_id,r_man.brj_ordre,r_man.exe_ordre,r_man.fou_ordre,r_man.ges_code,
	        r_man.man_date_remise,r_man.man_date_visa_princ,r_man.man_etat,r_man.man_etat_remise,
			mySomme,r_man.man_id,
			r_man.man_motif_rejet,r_man.man_nb_piece,r_man.man_numero,r_man.man_numero_rejet,r_man.man_ordre,
			r_man.man_orgine_key,r_man.man_origine_lib,
			mySomme,r_man.man_tva,r_man.mod_ordre,
			r_man.ori_ordre,r_man.pco_num,r_man.prest_id,r_man.tor_ordre,r_man.pai_ordre,r_man.org_ordre,
			r_man.rib_ordre_ordonnateur,r_man.rib_ordre_comptable,
			r_man.man_attente_paiement,r_man.man_attente_date,r_man.man_attente_objet,r_man.utl_ordre_attente);

    -- Traitement MARACUJA
	--
	-- DEPENSE
	--
	r_dep.dep_numero := myrembourse;
	r_dep.dep_ordre  := 0; -- modif rodolphe
	r_dep.dep_rib    := r_man.rib_ordre_ordonnateur;
	r_dep.fou_ordre  := r_man.fou_ordre;
	r_dep.man_id     := r_man.man_id;
	r_dep.man_ordre  := r_man.man_ordre;
	r_dep.mod_ordre  := r_man.mod_ordre;

	IF (nbenr > 0)
	THEN SELECT SUBSTR((adr_adresse1||' '||adr_adresse2||' '||adr_cp||' '||adr_ville),1,196)||'...',
	            adr_nom||' '||adr_prenom
	     INTO   r_dep.dep_adresse, r_dep.dep_fournisseur
	     FROM   grhum.v_fournis_grhum
	     WHERE  fou_ordre = r_dep.fou_ordre;
	ELSE r_dep.dep_adresse     := '';
	     r_dep.dep_fournisseur := 'REGULARISATION ECRITURES SCOLARITE';
	END IF;

	SELECT maracuja.depense_seq.NEXTVAL
	INTO   r_dep.dep_id
	FROM   dual;

	INSERT INTO maracuja.depense
	VALUES (r_dep.dep_adresse,r_dep.dep_date_compta,r_dep.dep_date_reception,r_dep.dep_date_service,
	        r_dep.dep_etat,r_dep.dep_fournisseur,
			mySomme,r_dep.dep_id,
			r_dep.dep_ligne_budgetaire,r_dep.dep_lot,r_dep.dep_marches,
			mySomme,
			r_dep.dep_nomenclature,r_dep.dep_numero,r_dep.dep_ordre,r_dep.dep_rejet,r_dep.dep_rib,r_dep.dep_suppression,
			mySomme,r_dep.dep_tva,r_dep.exe_ordre,r_dep.fou_ordre,r_dep.ges_code,
			r_dep.man_id,r_dep.man_ordre,r_dep.mod_ordre,r_dep.pco_ordre,r_dep.utl_ordre,r_dep.org_ordre,r_dep.tcd_ordre,NULL,SYSDATE);

    -- Traitement MARACUJA
	--
	-- MANDAT_BROUILLARD
	--
	r_mab.ges_code      := r_man.ges_code;
	r_mab.mab_montant   := mySomme;
	r_mab.mab_sens      := 'C';
	r_mab.man_id        := r_man.man_id;
	r_mab.pco_num       := r_man.pco_num;
    r_mab.mab_operation := 'VISA CTP';
    
	INSERT INTO maracuja.mandat_brouillard
	VALUES (r_mab.ecr_ordre,r_mab.exe_ordre,r_mab.ges_code,
	        r_mab.mab_montant,r_mab.mab_operation,
			maracuja.mandat_brouillard_seq.NEXTVAL,r_mab.mab_sens,
			r_mab.man_id,r_mab.pco_num);

    r_mab.mab_operation := 'VISA SCOLARITE';
    
    OPEN c2;
    LOOP
      FETCH c2 INTO r_mab.pco_num,r_mab.ges_code,r_mab.mab_montant,r_mab.mab_sens;
      EXIT WHEN c2%NOTFOUND;

	  INSERT INTO maracuja.mandat_brouillard
	  VALUES (r_mab.ecr_ordre,r_mab.exe_ordre,r_mab.ges_code,
	          r_mab.mab_montant,r_mab.mab_operation,
			  maracuja.mandat_brouillard_seq.NEXTVAL,r_mab.mab_sens,
			  r_mab.man_id,r_mab.pco_num);

    END LOOP;
    CLOSE c2;

  END LOOP;
  CLOSE c1;
END;
/




create or replace procedure grhum.inst_patch_maracuja_1929
is
begin

	
	-- mettre à jout les mandat_brouillard issus de scolarix
  	update maracuja.mandat_brouillard set mab_operation='VISA CTP' where mab_ordre in (
    select mab_ordre from maracuja.mandat_brouillard mab 
    inner join maracuja.mandat t on (t.man_id=mab.man_id) 
    inner join maracuja.bordereau b on (b.bor_id=t.bor_id)
    where b.tbo_ordre=16 and man_etat='ATTENTE' and Mab_sens='C' and mab_operation='VISA SCOLARITE'  ); 
	
	-- mettre à jour les mandat_detail_ecriture issus de scolarix
	update maracuja.mandat_detail_ecriture
    set mde_origine='VISA CTP'  
    where 
        mde_origine='VISA' and
    	ecd_ordre in (
	        select ecd.ecd_ordre 
	        from maracuja.ecriture_detail ecd 
	        inner join maracuja.mandat_detail_ecriture tde on (ecd.ecd_ordre=tde.ecd_ordre)
	        inner join maracuja.mandat t on (tde.man_id= t.man_id)
	        inner join maracuja.bordereau b on b.bor_id=t.bor_id
	        where 
	        man_ttc>=0 and ecd_credit>0 and ecd.pco_num not like '445%' and t.exe_ordre=tde.exe_ordre and b.tbo_ordre=16
        );
	
	
   jefy_admin.patch_util.end_patch (4, '1.9.2.9');
end;
/

