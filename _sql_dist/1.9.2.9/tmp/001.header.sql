set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.9
-- Date de publication : 
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Correction des origines de VISA pour les mandats/ODP générés par Scolarix
-- Modification de la procedure de scolarix qui crée les mandats/ODP
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.2.8', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.2.9', null );
commit ;



