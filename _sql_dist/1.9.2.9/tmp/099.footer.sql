

create or replace procedure grhum.inst_patch_maracuja_1929
is
begin

	
	-- mettre à jout les mandat_brouillard issus de scolarix
  	update maracuja.mandat_brouillard set mab_operation='VISA CTP' where mab_ordre in (
    select mab_ordre from maracuja.mandat_brouillard mab 
    inner join maracuja.mandat t on (t.man_id=mab.man_id) 
    inner join maracuja.bordereau b on (b.bor_id=t.bor_id)
    where b.tbo_ordre=16 and man_etat='ATTENTE' and Mab_sens='C' and mab_operation='VISA SCOLARITE'  ); 
	
	-- mettre à jour les mandat_detail_ecriture issus de scolarix
	update maracuja.mandat_detail_ecriture
    set mde_origine='VISA CTP'  
    where 
        mde_origine='VISA' and
    	ecd_ordre in (
	        select ecd.ecd_ordre 
	        from maracuja.ecriture_detail ecd 
	        inner join maracuja.mandat_detail_ecriture tde on (ecd.ecd_ordre=tde.ecd_ordre)
	        inner join maracuja.mandat t on (tde.man_id= t.man_id)
	        inner join maracuja.bordereau b on b.bor_id=t.bor_id
	        where 
	        man_ttc>=0 and ecd_credit>0 and ecd.pco_num not like '445%' and t.exe_ordre=tde.exe_ordre and b.tbo_ordre=16
        );
	
	
   jefy_admin.patch_util.end_patch (4, '1.9.2.9');
end;
/

