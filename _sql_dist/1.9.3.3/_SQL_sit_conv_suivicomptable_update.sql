SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DML
-- Schéma modifié :  JEFY_REPORT
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  
-- Date de publication :10/04/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Correction de la requete associée à 'sit_conv_suivi_comptable_jxls.xls' (problème lors de la selection d'un convention précise)
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;


begin

	
 JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.0', 'MARACUJA' );


update jefy_report.situation set SITU_INSQL_COMMAND = 'select e.ecr_ordre, e.ecr_numero, e.exe_ordre as exe_ordre_ecr, e.ecr_libelle, ecd.ecd_libelle,
ecd.ecd_index, ecd.pco_num, ecd.ecd_debit, ecd.ecd_credit, ecd.ecd_reste_emarger,
c.ll_partenaire, c.con_index, c.EXE_ORDRE , c.con_reference_externe, c.con_objet, c.exe_ordre||'' - ''||to_char(c.con_index,''0000'') as con_numero,
to_char(e.ecr_date_saisie,''dd/mm/yyyy'' ) as date_ecriture
from  maracuja.ecriture e 
inner join  maracuja.ecriture_detail ecd on e.ecr_ordre=ecd.ecr_ordre
inner join maracuja.v_accords_contrat c on ecd.CON_ORDRE=c.con_ordre
where c.exe_ordre||''-''||to_char(c.con_index,''FM0000'') like $P{CA_CODE_CONVENTION}
and to_char(e.exe_ordre) like $P{CA_EXER}
and pco_num like $P{CA_IMPUT}
order by  c.exe_ordre||''-''||c.con_index, e.exe_ordre, e.ecr_numero, ecd.ecd_index'
where SITU_STRID = 'sit_conv_suivi_comptable_jxls.xls';


commit;

end;

