set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM ou dba
-- Numéro de version :  1.9.3.3
-- Date de publication : 
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- DT#5646 : Doublons sur les restes à recouvrer : reecriture de la vue avec des join, ajout d'un distinct.
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.2', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.3', null );
commit ;



