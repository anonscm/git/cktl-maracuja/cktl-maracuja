set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM ou dba
-- Numéro de version :  1.9.3.3
-- Date de publication : 
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- DT#5646 : Doublons sur les restes à recouvrer : reecriture de la vue avec des join, ajout d'un distinct.
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.2', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.3', null );
commit ;



create or replace force view maracuja.v_recette_adresse (rpco_id, rec_id, tit_id, fou_ordre, civilite, nom, prenom, adr_ordre, adr_adresse1, adr_adresse2, adr_bp, code_postal, ville, cp_etranger, adresse, c_pays)
as
 select rpco_id,
          rec_id,
          tit_id,
          fou_ordre,
          civilite,
          nom,
          prenom,
          a.adr_ordre,
          adr_adresse1,
          adr_adresse2,
          adr_bp,
          code_postal,
          ville,
          cp_etranger,
          adr_adresse1 || decode (adr_adresse2, null, '', ' - ') || adr_adresse2 || ' ' || chr (13) || code_postal || ' ' || ville || decode (cp_etranger, null, '', ' ' || cp_etranger) || decode (lc_pays, null, '', 'FRANCE', '', ' - ' || lc_pays),
          a.c_pays
   from   (select rpco.rpco_id,
                  rpco.rec_id,
                  tit_id,
                  rpp.fou_ordre,
                  f.civilite,
                  f.nom,
                  f.prenom,
                  decode (y.adr_ordre, null, x.adr_ordre, y.adr_ordre) adr_ordre
           from   jefy_recette.recette_ctrl_planco rpco 
                  inner join jefy_recette.recette rec on (rpco.rec_id = rec.rec_id)
                  inner join jefy_recette.recette_papier rpp on (rec.rpp_id = rpp.rpp_id)
                  inner join maracuja.v_fournis_light f on (rpp.fou_ordre = f.fou_ordre)
                  inner join v_fournis_adr x on (f.pers_id = x.pers_id and f.fou_ordre = x.fou_ordre)
                  left outer join
                  (select distinct adr.rpp_id,
                          adr.adr_ordre
                   from   jefy_recette.recette_papier_adr_client adr 
                          inner join jefy_recette.recette_papier rpp on (adr.rpp_id = rpp.rpp_id)
                          inner join grhum.fournis_ulr f on (rpp.fou_ordre = f.fou_ordre)
                          inner join grhum.repart_personne_adresse rpa on (rpa.rpa_valide = 'O' and f.pers_id = rpa.pers_id and adr.adr_ordre = rpa.adr_ordre)
                          inner join
                          (select   rpp_id,
                                    max (date_creation) date_creation
                           from     jefy_recette.recette_papier_adr_client rpac
                           group by rpp_id) z on (adr.rpp_id = z.rpp_id and adr.date_creation = z.date_creation)
                          ) y on (rpp.rpp_id = y.rpp_id)
                  ) xx
          inner join grhum.adresse a on (xx.adr_ordre = a.adr_ordre)
          inner join grhum.pays p on (a.c_pays = p.c_pays);
/

create or replace procedure grhum.inst_patch_maracuja_1933
is
begin

	
   jefy_admin.patch_util.end_patch (4, '1.9.3.3');
end;
/

