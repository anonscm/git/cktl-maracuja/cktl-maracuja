SET DEFINE OFF;
	-- Fichier encodé en UTF-8
	-- a executer depuis user dba (grhum par exemple)
	INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 
	'BORDEREAU', 'DOSSIER LIQ', 23); 
	
	
	INSERT INTO MARACUJA.TYPE_BORDEREAU ( TBO_LIBELLE, TBO_ORDRE, TBO_SOUS_TYPE, TBO_TYPE, TBO_TYPE_CREATION,
	TNU_ORDRE ) VALUES ( 
	'DOSSIER DE LIQUIDATION', 300, 'DOSSIER LIQ.', 'BTME', 'DEP_30'
	, 23); 
	
	INSERT INTO maracuja.PARAMETRE (SELECT exe_ordre, 'Indique s''il faut forcer la date des écritures à la date du bordereau lors du visa d''un bordereau de droits d''inscription (facultatif)', 'SCOLADM_FORCER_DATE_VISA_DATE_BORD', maracuja.parametre_seq.NEXTVAL, 'NON'
                             FROM MARACUJA.EXERCICE  WHERE exe_ordre>=2010);
	
	INSERT INTO maracuja.PARAMETRE (SELECT exe_ordre, 'Indique s''il faut que les bordereaux de rejet de depense soient visés automatiquement', 'AUTO_VISER_BORD_REJET_DEP', maracuja.parametre_seq.NEXTVAL, 'NON'
                             FROM MARACUJA.EXERCICE  WHERE exe_ordre>=2010);
      commit;    


 --   INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
 --   TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.2.0',  null, '');
    
    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.2.0';
    
commit;
