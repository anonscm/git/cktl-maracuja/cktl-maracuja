-- Attention, ce fichier est encodé en UTF-8.
-- Exécutez-le dans un environnement UTF-8

whenever sqlerror exit sql.sqlcode ;

INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.3.0',  null, '');
commit;


--
--DROP TABLE MARACUJA.BILAN_POSTE CASCADE CONSTRAINTS;
--drop TABLE MARACUJA.BILAN_TYPE  CASCADE CONSTRAINTS;
--
--drop SEQUENCE MARACUJA.BILAN_TYPE_SEQ  ;
--drop SEQUENCE MARACUJA.BILAN_POSTE_SEQ ;


CREATE SEQUENCE MARACUJA.BILAN_TYPE_SEQ START WITH 1 NOCYCLE nocache;
CREATE SEQUENCE MARACUJA.BILAN_POSTE_SEQ START WITH 1 NOCYCLE nocache;

CREATE TABLE MARACUJA.BILAN_TYPE
(
  BT_ID          NUMBER(38,0)                     NOT NULL,
  BT_STR_ID		VARCHAR2(200 BYTE)				not null,
  BT_LIBELLE     VARCHAR2(200 BYTE)             NOT NULL
  ) tablespace GFC;
  
alter table MARACUJA.BILAN_TYPE add constraint PK_BILAN_TYPE primary key (BT_ID);
alter table MARACUJA.BILAN_TYPE add constraint UK_BILAN_TYPE unique (BT_STR_ID);
  
CREATE TABLE MARACUJA.BILAN_POSTE
(
  BP_ID          NUMBER(38,0)                     NOT NULL,
  EXE_ORDRE      NUMBER(38,0)                     NOT NULL,
  bt_id      NUMBER(38,0)                     NOT NULL,	
  BP_ID_PERE     NUMBER(38,0)				null,
  BP_NIVEAU      NUMBER(38,0)                     NOT NULL,
  BP_ORDRE       number(38,0)                   not null,
  BP_STR_ID		VARCHAR2(1000 BYTE)				not null,
  BP_LIBELLE     VARCHAR2(500 BYTE)             NOT NULL,
  BP_GROUPE      VARCHAR2(1 BYTE)               NOT NULL,
  BP_MODIFIABLE  VARCHAR2(1 BYTE)               NOT NULL,
  BP_FORMULE_MONTANT		VARCHAR2(2000 BYTE)	null,
  BP_FORMULE_AMORTISSEMENT	VARCHAR2(2000 BYTE)	null
)
tablespace GFC;

alter table MARACUJA.BILAN_POSTE add constraint PK_BILAN_POSTE primary key (BP_ID);
alter table MARACUJA.BILAN_POSTE add constraint UK_BILAN_POSTE unique (EXE_ORDRE,bt_id,BP_STR_ID);
alter table maracuja.BILAN_POSTE add (constraint FK_BILAN_POSTE_PERE FOREIGN KEY (BP_ID_PERE)  REFERENCES MARACUJA.BILAN_POSTE (BP_ID) deferrable initially deferred);
alter table maracuja.BILAN_POSTE add (constraint FK_BILAN_POSTE_BT_ID FOREIGN KEY (BT_ID)  REFERENCES MARACUJA.BILAN_TYPE (BT_ID) deferrable initially deferred);
alter table maracuja.BILAN_POSTE add (constraint FK_BILAN_POSTE_EXE_ORDRE FOREIGN KEY (EXE_ORDRE)  REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) deferrable initially deferred);

COMMENT ON TABLE maracuja.BILAN_POSTE IS 'Les postes du bilan';
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_ID IS 'Cle';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.EXE_ORDRE IS 'Ref a l''exercice';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_ID_PERE IS 'Ref au poste pere';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_NIVEAU IS 'Niveau du poste dans l''arbre';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_ORDRE IS 'Ordre du poste dans l''arbre';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_STR_ID IS 'Identifiant signifiant du poste';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_LIBELLE IS 'Libellé du poste';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_GROUPE IS 'O/N.Est-ce un groupe ou un detail';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_MODIFIABLE IS 'O/N.Est-ce que le poste est modifiable';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BT_ID IS 'Reference au type de bilan';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_FORMULE_MONTANT IS 'Formule pour calculer le montant (seulement si groupe=N)';	
COMMENT ON COLUMN maracuja.BILAN_POSTE.BP_FORMULE_AMORTISSEMENT IS 'Formule pour calculer le montant de l''amortissement (eventuel)';	




create or replace type maracuja.ZtableForSplit as table of varchar2(255);
/

create or replace function maracuja.str2tbl (p_str   in varchar2, p_delim in varchar2 default '.')
        return      ZtableForSplit
   as
        l_str        long default p_str || p_delim;
        l_n        number;
        l_data     ZtableForSplit := ZtableForSplit();
    begin
        loop
            l_n := instr( l_str, p_delim );
            exit when (nvl(l_n,0) = 0);
            l_data.extend;
            l_data( l_data.count ) := ltrim(rtrim(substr(l_str,1,l_n-1)));
            l_str := substr( l_str, l_n+length(p_delim) );
        end loop;
        return l_data;
   end;
   /

CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,         
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type  
--        );
        
--  procedure deletePlancoBilanPostes(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type 
--         );    
--        

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,         
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type 
--        );
        
  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%type,
         exeordre NUMBER, 
         bpStrId bilan_poste.bp_str_id%type,         
         formuleMontant varchar2,
         formuleAmortissement  varchar2
        );        

        -- Calcule le poste pour l'exercice en fonction de la formule trouvée 
--  FUNCTION calcPoste( 
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type, 
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      ) 
--      RETURN NUMBER;
    function getSoldeReq(formule varchar2, exeordre number, gescode VARCHAR2, sacd CHAR, vue varchar2) return varchar2;      

  FUNCTION getSoldeMontant( 
              exeOrdre bilan_poste.exe_ordre%type,
              formule varchar2,
              gesCode gestion.ges_code%type,
              isSacd char,
              faireCalcul smallint,
              sqlReq out varchar2
      ) 
      RETURN NUMBER;      
      
--    FUNCTION prv_getCritSqlForExcept( 
--              compte VARCHAR2, 
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;    
    function prv_getCritSqlForSingleFormule(formule varchar2) return varchar2 ;

--  FUNCTION getFormule( 
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type, 
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type      
--      ) 
--      RETURN varchar2;
      
   procedure prepare_bilan(btId bilan_poste.bt_id%type, exeOrdre bilan_poste.exe_ordre%type, gescode VARCHAR2, sacd CHAR);
   procedure prepare_bilan_actif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR);
   procedure prepare_bilan_passif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR);
   procedure checkFormule(formule varchar2);

   procedure duplicateExercice(exeordreOld integer, exeordreNew integer);
   procedure prv_duplicateExercice(exeordreNew integer, racineOld bilan_poste.bp_id%type, racineNew bilan_poste.bp_id%type);

END API_BILAN;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%type,
         exeordre NUMBER, 
         bpStrId bilan_poste.bp_str_id%type,         
         formuleMontant varchar2,
         formuleAmortissement  varchar2
  ) is
    btId bilan_type.bt_id%type;
  begin
    select bt_id into btId from bilan_type where bt_str_id=btstrid;
    update bilan_poste set BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement where bt_id=btId and exe_ordre=exeOrdre and bp_str_id=bpStrId;
  end setBilanPosteFormule;
  
  
    function getSoldeReq(formule varchar2, exeordre number, gescode VARCHAR2, sacd CHAR, vue varchar2) return varchar2 is
    signe varchar2(1);
    signeNum integer;
    sens varchar2(2);
    compteext varchar2(2000);
    critStrPlus varchar2(2000);
    res varchar2(4000);
    LC$req varchar2(4000);
    lavue varchar2(50);
   begin
        res := '';
        if (formule is not null and length(formule)>0) then
             dbms_output.put_line('traitement de ='|| formule);
            signe := substr(formule,1,1); -- +
            signeNum := 1;
            if (signe='-') then
                signeNum := -1;
            end if;
            sens :=  substr(formule,2,2); -- SD
            if (sens is null or sens not in ('SD','SC')) then
                 raise_application_error (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            end if;
            compteext := substr(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            if (length(compteext)=0) then
                raise_application_error (-20001, 'compte non recupere dans la formule '|| formule);
            end if;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';
            
            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('sacd='||sacd);
            
            lavue := vue;
            if lavue is null then
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
            end if;

           IF sens = 'SD'   THEN
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;   
               ELSE
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           ELSE
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' then
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        end if;
        return res;
   end getSoldeReq;
  
    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant( 
              exeOrdre bilan_poste.exe_ordre%type,
              formule varchar2,
              gesCode gestion.ges_code%type,
              isSacd char,
              faireCalcul smallint,
              sqlReq out varchar2
      ) 
      RETURN NUMBER is
   
    str varchar2(4000);
    compteext varchar2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos integer;
    nb integer;
    i integer;
    j integer;
    nextS integer;
    signe varchar2(10);
    sens varchar2(10);
    pconum plan_comptable_exer.pco_num%type;
    subdiv plan_comptable_exer.pco_num%type;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction varchar2(255);
    --critStrPlus varchar2(2000);
    resTmp number;
    signeNum number;
    sInstructionReelle varchar2(4000);
    req varchar2(4000);
    solde number;
  begin
    /* une formule est composées de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */   
   
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := replace(formule, ' ', '');
    
    if (length(str)=0) then
     raise_application_error (-20001, 'formule vide' );
    end if;
    
    str := upper(str);
    
    if (substr(str,1,1) = 'S') then
        str := '+'||str; 
    end if;

    str := replace(str, '+S', '|+S');
    str := replace(str, '-S', '|-S');
    str := substr(str,2);
dbms_output.put_line('Formule = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := substr(sinstruction,1,1); -- +
        signeNum := 1; 
        if (signe='-') then
            signeNum := -1; 
        end if;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures');
        dbms_output.put_line('REQ = '|| req); 
        if ( nvl(faireCalcul, 1)=1) then       
            EXECUTE IMMEDIATE req INTO solde ;
        end if;
        
        if (solde is null) then
            solde := 0;
        end if;
        IF solde < 0
           THEN solde := 0;
        END IF;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;    
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    return resTmp;           
    end getSoldeMontant;
    
    
    
  
  
    function prv_getCritSqlForSingleFormule(formule varchar2) return varchar2 is
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum varchar2(2000);
            critStrMoins varchar2(2000);
            critStrPlus varchar2(2000);
            str varchar2(2000);
            pconumTmp varchar2(2000);
            subdiv varchar2(1000);
    begin
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les éventuelles parenthese de debut / fin
            if (substr(str,1,1) = '(') then
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               if (substr(str,length(str),1) <> ')' ) then
                raise_application_error (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (substr(str,length(str),1)) || ': '|| formule);
               end if; 
               str := substr(str,2,length(str)-2);
             end if;
            
            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                if (substr(pconum,1,1)='(' ) then
                    if (substr(pconum,length(pconum),1) <> ')' ) then
                        raise_application_error (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (substr(pconum,length(pconum),1)) || ': '|| formule);
                    end if;
                    pconum := substr(pconum,2,length(pconum)-2);
                    if (instr(pconum,'(' )>0) then
                        raise_application_error (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                    end if;
                end if;
                if (instr(pconum,')' )>0) then
                    raise_application_error (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                end if;                    
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   if (j=1) then
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' '; 
                            dbms_output.put_line(pconumTmp);
                   else
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            if (length(subdiv)>0) then
                                if (subdiv not like pconumTmp||'%') then
                                    raise_application_error (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autorisées : '|| pconum);
                                end if;
                               
                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';                           
                            end if;
                   end if; 
                end loop;
                if (length(critStrMoins)>0) then
                    critStrMoins := '(' || critStrMoins ||')'; 
                    if (length(critStrPlus)>0) then
                        critStrPlus := critStrPlus || ' or ';   
                    end if;  
                    critStrPlus := critStrPlus || critStrMoins;
                end if;
                    
            end loop;
            
            return critStrPlus;
                   
    end;
  
  
 procedure prepare_bilan_actif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR) as
    bp_id0 bilan_poste.bp_id%type;
    bp_id1 bilan_poste.bp_id%type;
    bp_id2 bilan_poste.bp_id%type;
    rec_bp1 bilan_poste%rowtype;
    rec_bp2 bilan_poste%rowtype;
    rec_bp3 bilan_poste%rowtype;
    bilan_groupe1 varchar2(1000);
    bilan_groupe2 varchar2(1000);
    bilan_libelle varchar2(1000);
    bilan_montant number;
     bilan_amort number;
    bpStrId bilan_poste.bp_str_id%type;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%type;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%type;
    cursor carbre is 
            select *
            from bilan_poste
            where exe_ordre=exeOrdre
            connect by prior BP_ID = bp_id_pere
            start with BP_id_pere in (select BP_ID from bilan_poste where bp_id_pere is null and bp_str_id=bpStrId);
            
    cursor c1 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = bp_id0 order by bp_ordre;
    cursor c2 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp1.bp_id order by bp_ordre;
    cursor c3 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp2.bp_id order by bp_ordre;          
    sqlreq varchar2(4000);
   flag integer;
   
   begin
        bpStrId := 'actif';
        
        if (exeordre is null) then
            raise_application_error (-20001, ' exeOrdre obligatoire');
        end if;
        
        if (btId is null) then
            raise_application_error (-20001, ' btId obligatoire');
        end if;  
        if (gesCode is null) then
            raise_application_error (-20001, ' gesCode obligatoire');
        end if; 
        if (isSacd is null) then
            raise_application_error (-20001, ' isSacd obligatoire');
        end if;                         
        select count(*) into flag from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        if (flag=0) then
             raise_application_error (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        end if;
        
        select bp_id into bp_id0 from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        
--*************** CREATION TABLE ACTIF *********************************
        IF issacd = 'O' THEN
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSif issacd = 'G' then
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        else
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;
        
         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;
                
                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                       
                     if (rec_bp2.bp_groupe='N') then
                        bilan_groupe2 := null;   
                        bilan_libelle := rec_bp2.bp_libelle;
                        formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                        formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                        bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                        bilan_amort := 0;
                        if (length(trim(formuleAmortissement)) >0  )   then            
                            bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                        end if;
                        INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, gescode,exeordre);
                      else
                         bilan_groupe2 := rec_bp2.bp_libelle;   
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                bilan_libelle := rec_bp3.bp_libelle; 
                                formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                                  gesCode,
                                                  isSacd,
                                                  1,
                                                  sqlReq);
                                bilan_amort := 0;
                                if (length(trim(formuleAmortissement)) >0  )   then            
                                    bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,      
                                                  gesCode,
                                                  isSacd,
                                                  1,
                                                  sqlReq);
                                end if;            
                                INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, gescode,exeordre);
                               
                          END LOOP;                
                          CLOSE c3;                        

                     end if;  
                       
                  END LOOP;                
                  CLOSE c2; 
               
          END LOOP;                
          CLOSE c1;          
        
   end;
   
   
    procedure prepare_bilan_passif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR) as
        bp_id0 bilan_poste.bp_id%type;
        bp_id1 bilan_poste.bp_id%type;
        bp_id2 bilan_poste.bp_id%type;
        rec_bp1 bilan_poste%rowtype;
        rec_bp2 bilan_poste%rowtype;
        rec_bp3 bilan_poste%rowtype;
        bilan_groupe1 varchar2(1000);
        bilan_groupe2 varchar2(1000);
        bilan_libelle varchar2(1000);
        bilan_montant number;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%type;
        bpStrId bilan_poste.bp_str_id%type;
        cursor carbre is 
                select *
                from bilan_poste
                where exe_ordre=exeOrdre
                connect by prior BP_ID = bp_id_pere
                start with BP_id_pere in (select BP_ID from bilan_poste where bp_id_pere is null and bt_id=btId and bp_str_id=bpStrId);
                
        cursor c1 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = bp_id0 order by bp_ordre;
        cursor c2 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp1.bp_id order by bp_ordre;
        cursor c3 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp2.bp_id order by bp_ordre;          
        sqlreq varchar2(4000);
        flag integer;
   
   
   begin
        bpStrId := 'passif';
        if (exeordre is null) then
            raise_application_error (-20001, ' exeOrdre obligatoire');
        end if;
        
        if (btId is null) then
            raise_application_error (-20001, ' btId obligatoire');
        end if;  
        if (gesCode is null) then
            raise_application_error (-20001, ' gesCode obligatoire');
        end if; 
        if (isSacd is null) then
            raise_application_error (-20001, ' isSacd obligatoire');
        end if;          
        
        select count(*) into flag from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        if (flag=0) then
             raise_application_error (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        end if;
        select bp_id into bp_id0 from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId;
        IF issacd = 'O' THEN
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSif issacd = 'G' then
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        else
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;
        
         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;
                
                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                       
                     if (rec_bp2.bp_groupe='N') then
                        bilan_groupe2 := null;   
                        bilan_libelle := rec_bp2.bp_libelle;
                        formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                       -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                        bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlreq);
                      
                       
                        INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                      else
                         bilan_groupe2 := rec_bp2.bp_libelle;   
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                bilan_libelle := rec_bp3.bp_libelle; 
                                formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                       -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                      
                                 
                                INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                               
                          END LOOP;                
                          CLOSE c3;                        

                     end if;  
                       
                  END LOOP;                
                  CLOSE c2; 
               
          END LOOP;                
          CLOSE c1;          
        
   end;   
   
    procedure prepare_bilan(btId bilan_poste.bt_id%type,exeOrdre bilan_poste.exe_ordre%type, gescode VARCHAR2, sacd CHAR) is
    begin
         prepare_bilan_actif(btId, exeOrdre, gescode, sacd) ;
         prepare_bilan_passif(btId, exeOrdre, gescode, sacd) ;
    end;
   
   
 

    procedure checkFormule(formule varchar2) is
        sqlReq varchar2(4000);
        res number;
    begin
        res := getSoldeMontant(2010, formule, 'AGREGE','G', 0,sqlReq );
        
        return;
    end checkFormule;
   
   
   procedure duplicateExercice(exeordreOld integer, exeordreNew integer) is
        rec_bp bilan_poste%rowtype;
        cursor c1 is select * from bilan_poste where exe_ordre=exeordreOld and BP_ID_PERE is null;
        bpIdNew bilan_poste.bp_id%type;
        flag integer;
        
   begin
        select count(*) into flag from bilan_poste where exe_ordre=exeordreNew and BP_ID_PERE is null;
        if (flag >0) then
            raise_application_error (-20001, 'Bilan deja cree pour   : '|| exeordreNew);        
        end if;
   
   
       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            select bilan_poste_seq.nextval into bpIdNew from dual;
               
             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID, 
                   EXE_ORDRE, 
                   BT_ID, 
                   BP_ID_PERE, 
                   BP_NIVEAU, 
                   BP_ORDRE, 
                   BP_STR_ID, 
                   BP_LIBELLE, 
                   BP_GROUPE, 
                   BP_MODIFIABLE, 
                   BP_FORMULE_MONTANT, 
                   BP_FORMULE_AMORTISSEMENT                   
                   ) 
                VALUES (
                    bpIdNew, 
                   exeOrdreNew, --EXE_ORDRE, 
                   rec_bp.bt_id, --BT_ID, 
                   null, --BP_ID_PERE, 
                   rec_bp.bp_niveau, --BP_NIVEAU, 
                   rec_bp.bp_ordre, --BP_ORDRE, 
                   rec_bp.bp_str_id, --BP_STR_ID, 
                   rec_bp.bp_libelle, --BP_LIBELLE, 
                   rec_bp.bp_groupe, --BP_GROUPE, 
                   rec_bp.bp_modifiable, --BP_MODIFIABLE, 
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT, 
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   ); 
           
            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        end loop;
        close c1;
   end;
   
   procedure prv_duplicateExercice(exeordreNew integer, racineOld bilan_poste.bp_id%type, racineNew bilan_poste.bp_id%type) is
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%rowtype;
        bpIdNew bilan_poste.bp_id%type;
       
        cursor c1 is select * from bilan_poste where bp_id_Pere = racineOld;
   begin
        
        
   
   
       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             select bilan_poste_seq.nextval into bpIdNew from dual;
            
               
             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID, 
                   EXE_ORDRE, 
                   BT_ID, 
                   BP_ID_PERE, 
                   BP_NIVEAU, 
                   BP_ORDRE, 
                   BP_STR_ID, 
                   BP_LIBELLE, 
                   BP_GROUPE, 
                   BP_MODIFIABLE, 
                   BP_FORMULE_MONTANT, 
                   BP_FORMULE_AMORTISSEMENT                   
                   ) 
                VALUES (
                    bpIdNew, 
                   exeOrdreNew, --EXE_ORDRE, 
                   rec_bp.bt_id, --BT_ID, 
                   racineNew, --BP_ID_PERE, 
                   rec_bp.bp_niveau, --BP_NIVEAU, 
                   rec_bp.bp_ordre, --BP_ORDRE, 
                   rec_bp.bp_str_id, --BP_STR_ID, 
                   rec_bp.bp_libelle, --BP_LIBELLE, 
                   rec_bp.bp_groupe, --BP_GROUPE, 
                   rec_bp.bp_modifiable, --BP_MODIFIABLE, 
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT, 
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);
               
               
           end loop;
       close c1;
        
   end prv_duplicateExercice;
   
   
END API_BILAN;
/



CREATE OR REPLACE PROCEDURE MARACUJA.import_bilan_defaut(exeOrdre integer) as
    bpIdniveau0 integer;
    bpIdniveau1 integer;
    bpIdniveau2 integer;
    bpIdniveau3 integer;
    btId maracuja.bilan_type.bt_id%type;
BEGIN
   
   
    insert into maracuja.bilan_type (select maracuja.bilan_type_seq.nextval, 'BILAN PAR DEFAUT', 'BILAN PAR DEFAUT' from dual where not exists(select * from maracuja.bilan_type where bt_str_id='BILAN PAR DEFAUT' ));
    select bt_id into btId from maracuja.bilan_type where bt_str_id = 'BILAN PAR DEFAUT';
    delete from MARACUJA.BILAN_POSTE where exe_ordre=exeOrdre and bt_id=btid;


    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau0 from dual;
    INSERT INTO MARACUJA.BILAN_POSTE
        VALUES ( bpIdniveau0, exeordre, btId, null,   0, 1, 'actif','ACTIF', 'O', 'N',null,null);        

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 1, 'actif/actif immobilise','ACTIF IMMOBILISE', 'O', 'N', null, null); 

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/actif immobilise/immobilisations incorporelles','IMMOBILISATIONS INCORPORELLES', 'O', 'N', null, null); 

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations incorporelles/frais d''etablissement', 'Frais d''établissement', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif immobilise/immobilisations incorporelles/frais de recherche et de developpement','Frais de recherche et de développement', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations incorporelles/concessions et droits similaires','Concessions et droits similaires', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations incorporelles/droit au bail','Droit au bail', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations incorporelles/autres immobilisations incorporelles','Autres immobilisations incorporelles', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'actif/actif immobilise/immobilisations incorporelles/immobilisations incorporelles en cours','Immobilisations incorporelles en cours', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'actif/actif immobilise/immobilisations incorporelles/avances et acomptes verses sur immobilisations incorporelles','Avances et acomptes versés sur immobilisations incorporelles', 'N', 'O', null, null);                                                  
                        
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'actif/actif immobilise/immobilisations corporelles','IMMOBILISATIONS CORPORELLES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations corporelles/terrains, agencements et amenagements de terrain','Terrains, agencements et aménagements de terrain', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif immobilise/immobilisations corporelles/constructions et constructions sur sol d''autrui','Constructions et constructions sur sol d''autrui', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations corporelles/installations techniques, materiels et outillage', 'Installations techniques, matériels et outillage', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations corporelles/collections','Collections', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations corporelles/autres immobilisations corporelles','Autres immobilisations corporelles', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'actif/actif immobilise/immobilisations corporelles/immobilisations corporelles en cours','Immobilisations corporelles en cours', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'actif/actif immobilise/immobilisations corporelles/avances et acomptes verses sur commandes d''immobilisation','Avances et acomptes versés sur commandes d''immobilisation', 'N', 'O', null, null);                                                  
                        
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3, 'actif/actif immobilise/immobilisations financieres','IMMOBILISATIONS FINANCIERES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations financieres/participations','Participations', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2,'actif/actif immobilise/immobilisations financieres/creances rattachees a des participations', 'Créances rattachées à des participations', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations financieres/autres titres immobilises', 'Autres titres immobilisés', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations financieres/prets','Prêts', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations financieres/autres immobilisations financieres','Autres immobilisations financières', 'N', 'O', null, null);                          
                                                                 
                        
    
        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 2, 'actif/actif circulant','ACTIF CIRCULANT', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/actif circulant/stocks et en-cours', 'STOCKS ET EN-COURS', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/stocks et en-cours/matieres premieres et autres approvisionnements','Matières premières et autres approvisionnements', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/stocks et en-cours/en-cours de production de biens et de services','En-cours de production de biens et de services', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif circulant/stocks et en-cours/produits intermediaires et finis','Produits intermédiaires et finis', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif circulant/stocks et en-cours/marchandises','Marchandises', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2,'actif/actif circulant/avances et acomptes', 'AVANCES ET ACOMPTES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/avances et acomptes/avances et acomptes verses sur commandes','Avances et acomptes versés sur commandes', 'N', 'O', null, null);                          
    
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3, 'actif/actif circulant/creances d''exploitation','CREANCES D''EXPLOITATION', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/creances d''exploitation/creances clients et comptes rattaches', 'Créances clients et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/creances d''exploitation/autres creances d''exploitation','Autres créances d''exploitation', 'N', 'O', null, null);   

    
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 4,'actif/actif circulant/creances diverses', 'CREANCES DIVERSES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/creances diverses/tva', 'TVA', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/creances diverses/autres creances diverses','Autres créances diverses', 'N', 'O', null, null);   

                   select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 5, 'actif/actif circulant/tresorerie', 'TRESORERIE', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/tresorerie/valeurs mobilieres de placement','Valeurs mobilières de placement', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/tresorerie/disponibilites', 'Disponibilités', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif circulant/tresorerie/charges constatees d''avance','Charges constatées d''avance', 'N', 'O', null, null);                           

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 3, 'actif/comptes de regularisation','COMPTES DE REGULARISATION', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/comptes de regularisation/charges à repartir sur plusieurs exercices', 'Charges à répartir sur plusieurs exercices', 'N', 'O', null, null);  
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'actif/comptes de regularisation/differences de conversion sur operations en devises','Différences de conversion sur opérations en devises', 'N', 'O', null, null);  



----

    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau0 from dual;
    INSERT INTO MARACUJA.BILAN_POSTE 
        VALUES ( bpIdniveau0, exeordre, btId, null,   0, 2, 'passif', 'PASSIF', 'O', 'N', null, null);         

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 1,'passif/capitaux propres', 'CAPITAUX PROPRES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/capitaux propres/capital et reserves','CAPITAL ET RESERVES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/capitaux propres/capital et reserves/dotation','Dotation', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/capitaux propres/capital et reserves/complement de dotation (etat)', 'Complément de dotation (Etat)', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/capitaux propres/capital et reserves/complement de dotation (autres organismes)', 'Complément de dotation (autres organismes)', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'passif/capitaux propres/capital et reserves/affectation','Affectation', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'passif/capitaux propres/capital et reserves/biens remis en pleine propriete aux etablissements', 'Biens remis en pleine propriété aux établissements', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'passif/capitaux propres/capital et reserves/ecarts de reevaluation','Ecarts de réévaluation', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'passif/capitaux propres/capital et reserves/reserves','Réserves', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 8, 'passif/capitaux propres/capital et reserves/depreciation de l''actif', 'Dépréciation de l''actif', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 9, 'passif/capitaux propres/capital et reserves/report à nouveau','Report à nouveau', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 10, 'passif/capitaux propres/capital et reserves/resultat de l''exercice (benefice ou perte)','Résultat de l''exercice (bénéfice ou perte)', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 11, 'passif/capitaux propres/capital et reserves/subventions d''investissement','Subventions d''investissement', 'N', 'O', null, null);                                                                                                                                                  

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 2,'passif/provisions pour risques et charges',  'PROVISIONS POUR RISQUES ET CHARGES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/provisions pour risques et chargesprovisions pour risques', 'Provisions pour risques', 'N', 'O', null, null);  
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/provisions pour risques et chargesprovisions pour charges', 'Provisions pour charges', 'N', 'O', null, null);                      

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 3,'passif/dettes', 'DETTES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/dettes/dettes financieres','DETTES FINANCIERES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/dettes/dettes financieres/emprunts aupres des etablissements de credit','Emprunts auprès des établissements de crédit', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes financieres/emprunts divers','Emprunts divers', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes financieres/avances et acomptes recus sur commandes', 'Avances et acomptes reçus sur commandes', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                      
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'passif/dettes/dettes d''exploitation','DETTES D''EXPLOITATION', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/dettes/dettes d''exploitation/fournisseurs et comptes rattaches', 'Fournisseurs et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes d''exploitation/dettes fiscales et sociales','Dettes fiscales et sociales', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes d''exploitation/autres dettes d''exploitation','Autres dettes d''exploitation', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                      
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3,'passif/dettes/dettes diverses', 'DETTES DIVERSES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1,'passif/dettes/dettes diverses/dettes sur immobilisations et comptes rattaches', 'Dettes sur immobilisations et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes diverses/autres dettes diverses','Autres dettes diverses', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes diverses/produits constates d''avance', 'Produits constatés d''avance', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                                            

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 4, 'passif/comptes de regularisation','COMPTES DE REGULARISATION', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/comptes de regularisation/differences de conversion sur operations en devises','Différences de conversion sur opérations en devises', 'N', 'O', null, null);  



END import_bilan_defaut;
/








CREATE OR REPLACE PROCEDURE MARACUJA.import_bilan_defaut_formules(exeOrdre integer) as
begin
		
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/frais d''etablissement','SD201','SC2801 + SC2831');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/frais de recherche et de developpement','SD203','SC2803 + SC2833');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/concessions et droits similaires','SD205','SC2805 + SC2835 +SC2905');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/droit au bail','SD206','SC2906');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/autres immobilisations incorporelles','SD208','SC2808 + SC2838 + SC2908');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/immobilisations incorporelles en cours','SD232','SC2932');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/avances et acomptes verses sur immobilisations incorporelles','SD237',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/terrains, agencements et amenagements de terrain','SD211+SD212','SC2812 + SC2842 + SC2911');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/constructions et constructions sur sol d''autrui','SD213+SD214','SC2813 + SC2814 + SC2843 + SC2844');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/installations techniques, materiels et outillage','SD215','SC2815 + SC2845');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/collections','SD216','SC2816 + SC2846');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/autres immobilisations corporelles','SD218','SC2818 + SC2848');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/immobilisations corporelles en cours','SD231','SC2931');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/avances et acomptes verses sur commandes d''immobilisation','SD238',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/participations','SD261+SD266','SC2961 + SC2966');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/creances rattachees a des participations','SD267+SD268','SC2967 + SC2968');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/autres titres immobilises','SD271+SD272','SC2971 + SC2972');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/prets','SD274','SC2974');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/autres immobilisations financieres','SD275+SD2761+SD2768','SC2975 + SC2976');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/matieres premieres et autres approvisionnements','SD31+SD32','SC391 + SC392');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/en-cours de production de biens et de services','SD33+SD34','SC393 + SC394');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/produits intermediaires et finis','SD35','SC395');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/marchandises','SD37','SC397');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/avances et acomptes/avances et acomptes verses sur commandes','SD4091+SD4092',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances d''exploitation/creances clients et comptes rattaches','SD411 + SD412 + SD413 + SD416 + SD418','SC491');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances d''exploitation/autres creances d''exploitation','SD4096 + SD4098 + SD425 + SD4287 + SD4387 + SD4417 + SD443 + SD4487 + SD4684 + SD(472-4729) - SC4729 + SD4735 + SD478',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances diverses/tva','SD4456 + SD44581 + SD44583',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances diverses/autres creances diverses','SD429 + SD4411 + SD4412 +SD4413 + SD4418 + SD444 + SD45 + SD462 + SD463 + SD465 + SD467 + SD4687','SC495 + SC496');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/valeurs mobilieres de placement','SD50 - SC509','SC590');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/disponibilites','SD51 + SD53 + SD54 - SC51 - SC54 + SD185 - SC185',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/charges constatees d''avance','SD486',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/comptes de regularisation/charges à repartir sur plusieurs exercices','SD481',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/comptes de regularisation/differences de conversion sur operations en devises','SD476',null);

	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/dotation','SC1021',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/complement de dotation (etat)','SC1022',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/complement de dotation (autres organismes)','SC1023',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/affectation','SC1027',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/biens remis en pleine propriete aux etablissements','SC103',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/ecarts de reevaluation','SC105',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/reserves','SC1068',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/depreciation de l''actif','SC1069',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/report à nouveau','SC110 - SD119',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/resultat de l''exercice (benefice ou perte)','SC120 - SD129',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/subventions d''investissement','SC130 + SC131 + SC138 - SD139',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/provisions pour risques et chargesprovisions pour risques','SC151',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/provisions pour risques et chargesprovisions pour charges','SC157 + SC158',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/emprunts aupres des etablissements de credit','SC164',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/emprunts divers','SC165 + SC167 + SC168 + SC17 + SC45',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/avances et acomptes recus sur commandes','SC4191 + SC4192',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/fournisseurs et comptes rattaches','SC401 + SC403 + SC4081 + SC4088',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/dettes fiscales et sociales','SC421 + SC422 + SC427 + SC4282 + SC4286 + SC431 + SC437 + SC4382 + SC4386 + SC443 + SC444 + SC4452 + SC4455 + SC44584 + SC44587 + SC4457 + SC447 + SC4482 + SC4486',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/autres dettes d''exploitation','SC4196 + SC4197 + SC4198 + SC4682 + SC471 + SC4731 + SC478',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/dettes sur immobilisations et comptes rattaches','SC269 + SC404 + SC405 + SC4084',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/autres dettes diverses','SC429 + SC45 + SC464 + SC466 + SC467 + SC4686 + SC509',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/produits constates d''avance','SC487',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/comptes de regularisation/differences de conversion sur operations en devises','SC477',null);




end import_bilan_defaut_formules;
/




   

   
CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- Vérifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    

    --  -------------------------------------------------------
    -- Récupération des plan comptables
    INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
SELECT 
MARACUJA.PLAN_COMPTABLE_EXER_SEQ.NEXTVAL, nouvelExercice, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
FROM MARACUJA.PLAN_COMPTABLE_EXER where exe_ordre=precedentExercice;
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM, PCA_DUREE) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM, PCA_DUREE
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- Récupération des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
 

    -- --------------------------------------------------------
    -- codes budgets pour les epn
    insert into epn.code_budget_sacd(sacd, cod_bud, exe_ordre) 
        select sacd, cod_bud, nouvelExercice from epn.code_budget_sacd where exe_ordre=precedentExercice; 



    -- formules pour le calcul du bilan 
    MARACUJA.API_BILAN.DUPLICATEEXERCICE ( precedentExercice, nouvelExercice ); 
   
END;
/

CREATE OR REPLACE PACKAGE MARACUJA.Abricot_Impressions  IS

   -- il s'agit du dep_id de DEPENSE_budget
   FUNCTION GET_DEPENSE_CN (depid INTEGER)  RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_CONV (depid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_RECETTE_CONV (recid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_actions (depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)   RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_lbud(depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_infos(depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_RECETTE_analytiques (recid INTEGER)   RETURN VARCHAR2;
END Abricot_Impressions;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Abricot_Impressions IS


    FUNCTION GET_depense_CN (depid INTEGER)
    RETURN VARCHAR2 AS

            ceordre INTEGER;
            lescodes VARCHAR2(2000);
            lescodesdepenses VARCHAR2(2000);
            CURSOR c1 IS
            SELECT ce_ordre FROM jefy_depense.DEPENSE_CTRL_HORS_MARCHE WHERE dep_id = depid;
            BEGIN

            lescodesdepenses :=' ';
            OPEN c1;
            LOOP
            FETCH c1 INTO ceordre;
            EXIT WHEN c1%NOTFOUND;

            SELECT cm.cm_code||' '||cm_lib INTO lescodes
                        --SELECT cm.cm_code INTO lescodes
            FROM jefy_marches.CODE_EXER ce,jefy_marches.CODE_MARCHE cm
            WHERE ce.cm_ordre = cm.cm_ordre
            AND ce.ce_ordre  = ceordre;

            lescodesdepenses:=lescodesdepenses||' '||lescodes;

            END LOOP;
            CLOSE c1;


              RETURN lescodesdepenses;
    END;

    FUNCTION GET_DEPENSE_CONV (depid INTEGER)
    RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
            CURSOR c1 IS
                    SELECT conv_reference FROM jefy_depense.DEPENSE_ctrl_convention dcv, jefy_depense.v_convention c WHERE dcv.dep_id = depid AND dcv.conv_ordre = c.conv_ordre;
    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;

      FUNCTION GET_recette_CONV (recid INTEGER)
          RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT con_reference_externe
                FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE dcv.con_ordre = c.con_ordre and dcv.rec_id=rpco.rec_id
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;



    FUNCTION GET_DEPENSE_actions (depid INTEGER)
    RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
            CURSOR c1 IS
                    SELECT lolf_code FROM jefy_depense.DEPENSE_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c WHERE dca.dep_id = depid AND dca.tyac_id = c.lolf_id AND dca.exe_ordre=c.exe_ordre;
    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;

    FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)
          RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT lolf_code
                FROM jefy_recette.recette_ctrl_action rca, jefy_recette.V_LOLF_NOMENCLATURE_RECETTE l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE rca.LOLF_ID= l.lolf_id and rca.rec_id=rpco.rec_id and rca.EXE_ORDRE = l.exe_ordre
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;


        FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER)
    RETURN VARCHAR2 AS
        lesinv VARCHAR2(4000);
        leinv VARCHAR2(4000);

    CURSOR c1 IS
        SELECT ci.clic_num_complet FROM
        jefy_inventaire.inventaire_comptable ic , jefy_inventaire.CLE_INVENTAIRE_COMPTABLE ci
        where ic.clic_id = ci.clic_id
        and dpco_id = dpcoid;
        BEGIN

    lesinv := null;

     OPEN c1;
      LOOP
      FETCH c1 INTO leinv;
       EXIT WHEN c1%NOTFOUND;
        IF leinv IS NOT NULL THEN
         leinv := leinv||' ';
        END IF;
       lesinv := lesinv || leinv;
      END LOOP;
     CLOSE c1;
    RETURN lesinv;
        END;


    FUNCTION GET_DEPENSE_lbud (depid INTEGER)
    RETURN VARCHAR2 AS
           lbudlib VARCHAR2(2000);
    BEGIN


   SELECT o.org_ub || ' / ' ||org_cr || ' / ' ||org_souscr lbud into lbudlib
FROM
jefy_depense.depense_budget db,
jefy_depense.engage_budget eb,
jefy_admin.organ o
WHERE  db.dep_id = depid
and o.org_id = eb.org_id
AND eb.ENG_ID=db.ENG_ID;

        return lbudlib;


    END;

    FUNCTION GET_DEPENSE_infos (depid INTEGER)
    RETURN VARCHAR2 AS

    BEGIN


        return Abricot_Impressions.GET_depense_CN(depid)||' '||Abricot_Impressions.GET_depense_lbud(depid);


    END;
    
    FUNCTION GET_recette_analytiques (recid INTEGER)
    RETURN VARCHAR2 AS
        lescodes VARCHAR2(2000);
        coderef VARCHAR2(500);
        CURSOR c1 IS
            SELECT a.CAN_CODE
            FROM jefy_recette.recette_ctrl_analytique rca, jefy_admin.code_analytique a, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
            WHERE rca.can_id = a.can_id and rca.rec_id=rpco.rec_id and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         lescodes := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO coderef;
                      EXIT WHEN c1%NOTFOUND;

                IF lescodes IS NOT NULL THEN
                   lescodes := lescodes||', ';
                END IF;
                lescodes := lescodes || coderef;
            END LOOP;
            CLOSE c1;

           RETURN lescodes;
    END;

END Abricot_Impressions;
/














SET DEFINE OFF;
-- Fichier encodé en UTF-8
-- a executer depuis user dba (grhum par exemple)
whenever sqlerror exit sql.sqlcode ;


CREATE OR REPLACE PACKAGE MARACUJA.Util  IS

    PROCEDURE annuler_visa_bor_mandat(borid INTEGER);
    PROCEDURE annuler_visa_bor_titre(borid INTEGER);

    PROCEDURE supprimer_visa_btme(borid INTEGER);
    
    PROCEDURE supprimer_visa_btms(borid INTEGER);

    PROCEDURE supprimer_visa_btte(borid INTEGER);

    procedure creer_ecriture_annulation(ecrordre INTEGER);
    
    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    procedure supprimer_bordereau_dep(borid integer);
    
    procedure supprimer_bordereau_rec(borid integer);
    
    procedure supprimer_bordereau_btms(borid integer);
    
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer);
    
END;
/





CREATE OR REPLACE PACKAGE BODY MARACUJA.Util IS

    -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
    -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

 -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE annuler_visa_bor_mandat(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, mandat_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.man_id=tmpmanid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME' and tbotype <> 'BTRU') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME ou BTRU');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandats associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
--             
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

--                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

--                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;



 PROCEDURE annuler_visa_bor_titre(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid TITRE.tit_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                  CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, titre_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.tit_id=tmpmanid;

       BEGIN
     SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associé au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;




            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmpmanid;
                             EXIT WHEN lestitres%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmpmanid;
                                UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmpmanid;



             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;




       -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE supprimer_visa_btme(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
                            if (flag>0) then
                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                            end if;
                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;


 -- suppression des ecritures du visa d'un bordereau de depense BTMS 
       PROCEDURE supprimer_visa_btms(borid INTEGER) IS
         flag INTEGER;
         lebordereau BORDEREAU%ROWTYPE;
         tbotype TYPE_BORDEREAU.tbo_type%TYPE;
         tmpmanid MANDAT.man_id%TYPE;
         tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

         CURSOR lesecritures IS
         SELECT distinct ed.ecr_ordre from ecriture_detail ed, mandat_detail_ecriture mde, mandat m WHERE m.man_id = mde.man_id and mde.ecd_ordre= ed.ecd_ordre and m.bor_id = borid;

         CURSOR lesmandats IS
         SELECT man_id FROM MANDAT WHERE bor_id = borid;

         BEGIN
           SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTMS') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTMS');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat = 'VALIDE'  ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associé au bordereau bor_id= '|| borid);
               END IF;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

--            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;

            OPEN lesecritures;
             LOOP
              FETCH lesecritures INTO tmpEcrOrdre;
             EXIT WHEN lesecritures%NOTFOUND;

                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE ecd_ordre in (select ecd_ordre from ecriture_detail where ecr_ordre = tmpEcrOrdre);

                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;

                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

             END LOOP;
             CLOSE lesecritures;


            OPEN lesmandats;
            LOOP
            FETCH lesmandats INTO tmpmanid;
            EXIT WHEN lesmandats%NOTFOUND;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;

             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

            UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_visa = null,ecr_ordre_sacd = null,ecr_ordre_opp = null  WHERE bor_id IN (borid);

       END;




   -- annulation du visa d'un bordereau de depense BTTE
       PROCEDURE supprimer_visa_btte(borid INTEGER) IS
                    flag INTEGER;
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmptitid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associé au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associés au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmptitid;
                             EXIT WHEN lestitres%NOTFOUND;

                            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, TITRE_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.tit_id=tmptitid;

                            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                            DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmptitid;
                            UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmptitid;


             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;
       
       
       
       procedure creer_ecriture_annulation(ecrordre INTEGER) is
            ecr maracuja.ecriture%rowtype;
            ecd maracuja.ecriture_detail%rowtype;
            flag integer;
            str ecriture.ecr_libelle%type;
            newEcrOrdre ecriture.ecr_ordre%type;
            newEcdOrdre ecriture_detail.ecd_ordre%type;
            x  integer;
            cursor c1 is select * from maracuja.ecriture_detail where ecr_ordre=ecrordre;
       
       begin
            
            
            select count(*) into flag from maracuja.ecriture where ecr_ordre=ecrordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture retrouvee pour ecr_ordre= '|| ecrordre || ' .');
            end if;

            select * into ecr from maracuja.ecriture where ecr_ordre=ecrordre;              
            
            str := 'Annulation Ecriture '|| ecr.exe_ordre || '/'||ecr.ecr_numero;
            
            -- verifier que l'ecriture n'a pas deja ete annulee
            select count(*) into flag from maracuja.ecriture where ecr_libelle = str; 
            if (flag > 0) then
                select ecr_numero into flag from maracuja.ecriture where ecr_libelle = str; 
                RAISE_APPLICATION_ERROR (-20001,'L''ecriture numero '||ecr.ecr_numero ||' (ecr_ordre= '|| ecrordre || ') a deja ete annulee par l''ecriture numero '|| flag ||'.');
            end if;
                   
            
            -- verifier que les details ne sont pas emarges
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                if (ecd.ecd_reste_emarger<abs(ecd.ecd_montant) ) then
                    RAISE_APPLICATION_ERROR (-20001,'L''ecriture '||ecr.ecr_numero ||' ecr_ordre= '|| ecrordre || ' a ete emargee pour le compte '||ecd.pco_num||'. Impossible d''annuler');                    
                end if;

             END LOOP;
             CLOSE c1;


--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...              
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--                    
--                    

--             END LOOP;
--             CLOSE c1;

            newEcrOrdre := api_plsql_journal.creerecriture(
                ecr.com_ordre,
                SYSDATE,
                str,
                ecr.exe_ordre,
                ecr.ori_ordre,
                ecr.tjo_ordre,
                ecr.top_ordre,
                ecr.utl_ordre);            
            
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                newEcdOrdre := api_plsql_journal.creerEcritureDetail (
                        ecd.ecd_commentaire,
                        ecd.ecd_libelle,  
                        -ecd.ECD_MONTANT,
                        ecd.ECD_SECONDAIRE,
                        ecd.ECD_SENS,
                        newEcrOrdre,
                        ecd.GES_CODE,
                        ecd.PCO_NUM );
                        
                x := creerEmargementMemeSens(ecd.ecd_ordre, newEcdOrdre, 1);
             END LOOP;
             CLOSE c1;                  
            NUMEROTATIONOBJECT.numeroter_ecriture(newEcrOrdre);

       end;
           
       
       
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
        flag integer;
        emargementdetail EMARGEMENT_DETAIL%ROWTYPE;
        CURSOR c1 IS SELECT * INTO emargementdetail FROM EMARGEMENT_DETAIL WHERE ema_ordre = emaordre;
    BEGIN
        select count(*) into flag from emargement where ema_etat='VALIDE' and ema_ordre = emaordre;
        
        if (flag=1) then
             OPEN c1;
            LOOP
            FETCH c1 INTO emargementdetail;
            EXIT WHEN c1%NOTFOUND;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_source;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_destination;

            END LOOP;
            CLOSE c1;
   
            -- annulation de la emargement
            UPDATE EMARGEMENT SET ema_etat = 'ANNULE'
            WHERE ema_ordre = emaordre;        
        
        end if;

       
   
    END;
    
    
    
        
    procedure supprimer_bordereau_dep(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des mandats
        select count(*) into flag from mandat where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de depense.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les mandat_brouillards
        DELETE FROM maracuja.mandat_BROUILLARD WHERE man_id in (select man_id from mandat where bor_id=borid);
                
        -- supprimer les depenses
        DELETE FROM maracuja.depense WHERE man_id in (select man_id from mandat where bor_id=borid);

        -- mettre a jour les depense_ctrl_planco
        UPDATE jefy_depense.depense_ctrl_planco SET man_id = NULL WHERE man_id IN (SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid) ;

        -- supprimer les mandats        
        delete from  maracuja.mandat where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
        
        
    end;
    
    
    procedure supprimer_bordereau_btms(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau b, type_bordereau tb where bor_id=borid and b.tbo_ordre=tb.tbo_ordre and tb.TBO_TYPE='BTMS';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas ou n''est pas de type BTMS');
        end if;
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;          
        
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        
        supprimer_bordereau_dep(borid);
        
    end;
    
    
    
    procedure supprimer_bordereau_rec(borid integer)  as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des titres
        select count(*) into flag from titre where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de recette.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les titre_brouillards
        DELETE FROM maracuja.titre_BROUILLARD WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);
                
        -- supprimer les recettes
        DELETE FROM maracuja.recette WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);

        -- mettre a jour les recette_ctrl_planco
        UPDATE jefy_recette.recette_ctrl_planco SET tit_id = NULL WHERE tit_id IN (SELECT tit_id FROM maracuja.titre WHERE bor_id = borid) ;

        -- supprimer les titres        
        delete from  maracuja.titre where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
    
    end;
 



    /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer) IS
        flag INTEGER;                 
        leRecouvrement recouvrement%rowtype;
        tmpemaOrdre emargement.ema_ordre%type;
        tmpEcrOrdre ecriture.ecr_ordre%type;

        cursor emargements is 
                select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' 
                        and rec_id in (
                                select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ))) 
                    union
                        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where 
                        eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )));

                    
       cursor ecritures is
               select distinct e.ecr_ordre from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ));

        begin
        
            -- recuperer les numeros des ecritures min et max pour les passer en parametre
            --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
        
            select count(*) into flag from prelevement where RECO_ORDRE = recoordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucun prelevement correspondant a recoordre= '|| recoordre);
            end if;
            
            select * into leRecouvrement from recouvrement where RECO_ORDRE = recoordre;
            
            -- verifier les dates des ecritures avec la date du prelevement
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMin;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if; 
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMax;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if;             
            

        -- supprimer les emargements
        OPEN emargements;
             LOOP
                 FETCH emargements INTO tmpemaOrdre;
                      EXIT WHEN emargements%NOTFOUND;
                      
                annuler_emargement(tmpEmaOrdre);      
        END LOOP;
        CLOSE emargements;        
        
        -- modifier les etats des prelevements
        update prelevement set PREL_ETAT_MARACUJA = 'ATTENTE' where reco_ordre=recoOrdre;

        --supprimer le contenu du fichier (prelevement_fichier)
        delete from prelevement_fichier where reco_ordre=recoordre; 

        -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
        OPEN ecritures;
             LOOP
                 FETCH ecritures INTO tmpecrOrdre;
                      EXIT WHEN ecritures%NOTFOUND;
                      
                creer_ecriture_annulation(tmpEcrOrdre);
        END LOOP;
        CLOSE ecritures;         

        delete from titre_detail_ecriture where ecd_ordre in (select distinct ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
        where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre 
        from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in 
        (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )) );

        -- supprimer association des prélèvements au recouvrement
        update  prelevement set reco_ordre=null where reco_ordre=recoOrdre;

        -- supprimer l'objet recouvrement
        delete from recouvrement where reco_ordre=recoOrdre;





--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))


--        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))) 
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--                 
                 
                 
                     
    end;
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paf IS

function getMoisCompletTexte(borId integer) return varchar
-- renvoie le mois sous le forme JANVIER 2009 (a partir d un bordereau PAF)
is
    exeOrdre number;
    moisComplet varchar2(50);
    lemois varchar2(3);
    leMoisNum integer;
    lemoisTxt varchar2(20);
begin

    SELECT DISTINCT exe_ordre 
    INTO exeordre
    FROM maracuja.bordereau m
    WHERE m.bor_id = borid;    
    
    SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    --lemois := lpad(to_char(to_number(lemois)),2,'0' );
    leMoisNum := to_number(lemois);
    
    if leMoisNum = 1 then
       lemoisTxt := 'JANVIER'; 
    elsif leMoisNum = 2 then
       lemoisTxt := 'FEVRIER'; 
    elsif leMoisNum = 3 then
       lemoisTxt := 'MARS'; 
    elsif leMoisNum = 4 then
       lemoisTxt := 'AVRIL';     
    elsif leMoisNum = 5 then
       lemoisTxt := 'MAI';           
    elsif leMoisNum = 6 then
       lemoisTxt := 'JUIN';     
    elsif leMoisNum = 7 then
       lemoisTxt := 'JUILLET'; 
    elsif leMoisNum = 8 then
       lemoisTxt := 'AOUT'; 
    elsif leMoisNum = 9 then
       lemoisTxt := 'SEPTEMBRE'; 
    elsif leMoisNum = 10 then
       lemoisTxt := 'OCTOBRE'; 
    elsif leMoisNum = 11 then
       lemoisTxt := 'NOVEMBRE'; 
    elsif leMoisNum = 12 then
       lemoisTxt := 'DECEMBRE'; 
    end if;                                                     
     
    return lemoisTxt||' '||exeordre;    
end;

PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
lemois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

is_sacd gestion_exercice.pco_num_185%TYPE;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
--fredSELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
--fredWHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paf.paf_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois = lemois and exe_ordre = tmpBordereau.exe_ordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas prepare les ecritures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D'
 and exe_ordre = tmpBordereau.exe_ordre;

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C'
 and exe_ordre = tmpBordereau.exe_ordre;

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paf.set_bord_brouillard_visa(borid);

    select pco_num_185 into is_sacd from gestion_exercice where exe_ordre = tmpBordereau.exe_ordre and ges_code = tmpBordereau.ges_code;
    if (is_sacd is not null)
    then
       Bordereau_Abricot_Paf.set_bord_brouillard_sacd(borid);
    end if;

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, getMoisCompletTexte(borId),NULL);
  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paf.set_bord_brouillard_paiement(lemois, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -
  update jefy_paf.paf_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois=lemois and exe_ordre=tmpBordereau.exe_ordre and liq_ETAT='LIQUIDEE';

--fred UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
--fred    WHERE ges_code=tmpBordereau.ges_code
--fred      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';

 UPDATE jefy_paf.paf_etape SET bor_id=borid, pae_etat='MANDATEE', mois_libelle = getMoisCompletTexte(borid)
    WHERE ges_code=tmpBordereau.ges_code
        and  mois=lemois and exe_ordre=tmpBordereau.exe_ordre
        AND pae_ETAT='LIQUIDEE';



/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

select 1 into cpt from dual;

-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(lemois varchar, borid NUMBER, exeordre NUMBER)
IS


currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet VARCHAR2(50);
mois2 varchar2(50);

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paf.paf_ecritures WHERE mois = lemois AND ecr_type='45' and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
--fred lemois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
--fred SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
--fred WHERE mois_ordre  = moisordre;
SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

mois2 := getMoisCompletTexte(borid);


--SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
--WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||moiscomplet;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||mois2;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT PAF',
 currentecriture.pco_num,
 'PAIEMENT PAF '||mois2, -- 'PAIEMENT PAF '||moiscomplet,
 mois2, --moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ECRITURES WHERE mois = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ))
and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
moisordre INTEGER;
lemoiscomplet varchar2(50);
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

--fred moislibelle jefy_paf.paye_mois.mois_complet%TYPE;
lemois VARCHAR2(50);
mois2 varchar2(50);
BEGIN

    SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

    SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

    SELECT DISTINCT substr(dep_numero,5,3) INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT DISTINCT dep_numero INTO lemoiscomplet
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

    mois2 := getMoisCompletTexte(borid);

    OPEN ecriturescredit64(lemois, currentbordereau.ges_code);
    LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

         SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

         INSERT INTO BORDEREAU_BROUILLARD VALUES
         (
         bobordre,
         borid,
         currentbordereau.exe_ordre,
         currentecriture.ges_code,
         currentecriture.ecr_mont,
         currentecriture.ecr_sens,
         'VALIDE',
         'VISA PAF',
         currentecriture.pco_num,
         'VISA PAF '|| mois2, --'VISA PAF '||lemois,
         mois2, --lemoiscomplet,
         NULL
         );

    END LOOP;
    CLOSE ecriturescredit64;

END;


/******************************************
SET_MANDAT_ORV_BROUILLARD
******************************************/
PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

contre_partie_visa plan_comptable_exer.pco_num%TYPE;
ges_code_contrepartie comptabilite.ges_code%TYPE;

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN


-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

SELECT par_value INTO contre_partie_visa FROM PARAMETRE WHERE exe_ordre = lemandat.exe_ordre and par_key  = 'CONTRE PARTIE VISA';

if (contre_partie_visa = 'AGENCE')
then
    select ges_code into ges_code_contrepartie from maracuja.comptabilite where com_ordre = 1;
else
    ges_code_contrepartie := lemandat.ges_code;
end if;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,                            --ECD_ORDRE,
   lemandat.exe_ordre,              --EXE_ORDRE,
   lemandat.ges_code,               --GES_CODE,
   ABS(lemandat.man_ttc),           --MAB_MONTANT,
   'VISA MANDAT',                   --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,   --MAB_ORDRE,
   'C',                             --MAB_SENS,
   manid,                           --MAN_ID,
   classe6                          --PCO_NU
);

SELECT count(*) into cpt
FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

    SELECT COUNT(*) INTO cpt
    FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
    WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    IF (cpt = 1 )      -- Bulletins negatifs, on va chercher la contrepartie dans jefy_paf.paf_reversements
    THEN

      SELECT PCO_NUM_CONTREPARTIE INTO classe4
      FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
      WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    ELSE                -- OR Manuel

      classe4 := jefy_paf.get_contrepartie('REVERSEMENT', classe6, depid);
    
    END IF;

      -- creation du brouillard DEBITEUR CLASSE 4 !
      INSERT INTO MANDAT_BROUILLARD VALUES (
         NULL,                          --ECD_ORDRE,
         lemandat.exe_ordre,            --EXE_ORDRE,
         ges_code_contrepartie,             --GES_CODE,
         ABS(montant),                  --MAB_MONTANT,
         'VISA MANDAT',                 --MAB_OPERATION,
         mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
         'D',                           --MAB_SENS,
         manid,                         --MAN_ID,
         classe4                        --PCO_NU
      );

 END LOOP;
 CLOSE plancos;


END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
depid   INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;



BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- recup du dpcoid de ce mandat PAF : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)

select dep_id into depid from jefy_depense.depense_ctrl_planco where dpco_id = dpcoid;

select pco_num into classe6 from jefy_depense.depense_ctrl_planco where dpco_id = dpcoid;

select dep_ttc_saisie into montant from jefy_depense.depense_budget where dep_id = depid;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);

classe4 := jefy_paf.get_contrepartie('REGUL', classe6, depid );

-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);

END;




-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
/*
SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES PAF',
 currentecriture.pco_num,
 'RETENUES PAF '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;
*/
END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ecritures
WHERE mois = lemois AND ecr_type='18'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

mois VARCHAR2(50);
mois2 varchar2(50);

BEGIN

    mois2 := getMoisCompletTexte(borid);
    select count(*) into cpt from bordereau_brouillard where bob_operation = 'SACD PAF' and bob_libelle2 = mois2;

    if (cpt = 0)        -- On a pas encore passe les ecritures SACD
    then

        SELECT DISTINCT substr(dep_numero,5,3) INTO moisordre
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

        SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

        SELECT DISTINCT dep_numero INTO mois
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

         OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
         LOOP
         FETCH ecrituressacd INTO currentecriture;
         EXIT WHEN ecrituressacd%NOTFOUND;

             SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

             INSERT INTO BORDEREAU_BROUILLARD VALUES
             (
             bobordre,
             borid,
             currentbordereau.exe_ordre,
             currentecriture.ges_code,
             currentecriture.ecr_mont,
             currentecriture.ecr_sens,
             'VALIDE',
             'SACD PAF',
             currentecriture.pco_num,
             'SACD PAF '||mois2,
             mois2,
             NULL
             );

         END LOOP;
         CLOSE ecrituressacd;

    end if;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA PAF',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;


/*
PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;

*/
END;
/

--
-- PACKAGE BORDEREAU_ABRICOT
--

CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT" 
AS
  PROCEDURE creer_bordereau (abrid INTEGER)
  IS
     cpt            INTEGER;
     abrgroupby     abricot_bord_selection.abr_group_by%TYPE;
     monborid_dep   INTEGER;
     monborid_rec   INTEGER;
     flag integer;

     CURSOR lesmandats
     IS
        SELECT man_id
          FROM mandat
         WHERE bor_id = monborid_dep;

     CURSOR lestitres
     IS
        SELECT tit_id
          FROM titre
         WHERE bor_id = monborid_rec;

     tmpmandid      INTEGER;
     tmptitid       INTEGER;
     tboordre       INTEGER;
  BEGIN
-- est ce une selection vide ???
     SELECT COUNT (*)
       INTO cpt
       FROM abricot_bord_selection
      WHERE abr_id = abrid;

     IF cpt != 0
     THEN
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

        -- verifier l etat de l exercice
        select count(*) into flag from jefy_admin.exercice where exe_ordre=recup_exeordre (abrid) and exe_stat in ('O', 'R');
        if (flag = 0) then
            raise_application_error
                 (-20001,
                  'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.'
                 );
        end if;


        -- recup du group by pour traiter les cursors
        abrgroupby := recup_groupby (abrid);

        IF (abrgroupby = 'bordereau_1R1T')
        THEN
           monborid_rec :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_1r1t (abrid, monborid_rec);
        END IF;

        IF (abrgroupby = 'bordereau_NR1T')
        THEN
           monborid_rec :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_nr1t (abrid, monborid_rec);

-- controle RA
           SELECT COUNT (*)
             INTO cpt
             FROM titre
            WHERE ori_ordre IS NOT NULL AND bor_id = monborid_rec;

           IF cpt != 0
           THEN
              raise_application_error
                 (-20001,
                  'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !'
                 );
           END IF;
        END IF;

        IF (abrgroupby = 'bordereau_1D1M')
        THEN
           monborid_dep :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_1d1m (abrid, monborid_dep);
        END IF;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
monborid_dep := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

monborid_rec := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

bordereau_1D1M(abrid,monborid_dep);
bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
        IF (abrgroupby NOT IN
               ('bordereau_1R1T',
                'bordereau_NR1T',
                'bordereau_1D1M',
                'bordereau_1D1M1R1T'
               )
           )
        THEN
           monborid_dep :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_nd1m (abrid, monborid_dep);
        END IF;

        IF (monborid_dep IS NOT NULL)
        THEN
           bordereau_abricot.numeroter_bordereau (monborid_dep);

           OPEN lesmandats;

           LOOP
              FETCH lesmandats
               INTO tmpmandid;

              EXIT WHEN lesmandats%NOTFOUND;
              get_depense_jefy_depense (tmpmandid);
           END LOOP;

           CLOSE lesmandats;

           controle_bordereau (monborid_dep);
        END IF;

        IF (monborid_rec IS NOT NULL)
        THEN
           bordereau_abricot.numeroter_bordereau (monborid_rec);

           OPEN lestitres;

           LOOP
              FETCH lestitres
               INTO tmptitid;

              EXIT WHEN lestitres%NOTFOUND;
              -- recup du brouillard
              get_recette_jefy_recette (tmptitid);
              set_titre_brouillard (tmptitid);
              get_recette_prelevements (tmptitid);
           END LOOP;

           CLOSE lestitres;

           controle_bordereau (monborid_rec);
        END IF;

-- maj de l etat dans la selection
        IF (monborid_dep IS NOT NULL OR monborid_rec IS NOT NULL)
        THEN
           IF monborid_rec IS NOT NULL
           THEN
              UPDATE abricot_bord_selection
                 SET abr_etat = 'TRAITE',
                     bor_id = monborid_rec
               WHERE abr_id = abrid;
           END IF;

           IF monborid_dep IS NOT NULL
           THEN
              UPDATE abricot_bord_selection
                 SET abr_etat = 'TRAITE',
                     bor_id = monborid_dep
               WHERE abr_id = abrid;

              SELECT tbo_ordre
                INTO tboordre
                FROM bordereau
               WHERE bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
              IF tboordre = 3
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
              IF tboordre = 18
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye_orv
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
              IF tboordre = 19
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye_regul
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
              IF tboordre = 22
              THEN
                 bordereau_abricot_paf.basculer_bouillard_paye_regul
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de PAF on retravaille le brouillard
              IF tboordre = 20
              THEN
                 bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
              END IF;
-- pour les bordereaux d'orv de PAF on retravaille le brouillard
 if tboordre = 21  then
  bordereau_abricot_paf.basculer_bouillard_paye_orv(monborid_dep);
  end if;

           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
           END IF;
        END IF;
     END IF;
  END;

  PROCEDURE viser_bordereau_rejet (brjordre INTEGER)
  IS
     cpt              INTEGER;
     manid            maracuja.mandat.man_id%TYPE;
     titid            maracuja.titre.tit_id%TYPE;
     tboordre         INTEGER;
     reduction        INTEGER;
     utlordre         INTEGER;
     dpcoid           INTEGER;
     recid            INTEGER;
     depsuppression   VARCHAR2 (20);
     rpcoid           INTEGER;
     recsuppression   VARCHAR2 (20);
     exeordre         INTEGER;
     depid             integer;
     boridInitial    integer;

     CURSOR mandats
     IS
        SELECT man_id
          FROM maracuja.mandat
         WHERE brj_ordre = brjordre;

     CURSOR depenses
     IS
        SELECT dep_ordre, dep_suppression
          FROM maracuja.depense
         WHERE man_id = manid;

     CURSOR titres
     IS
        SELECT tit_id
          FROM maracuja.titre
         WHERE brj_ordre = brjordre;

     CURSOR recettes
     IS
        SELECT rec_ordre, rec_suppression
          FROM maracuja.recette
         WHERE tit_id = titid;

     deliq            INTEGER;
  BEGIN
     OPEN mandats;

     LOOP
        FETCH mandats
         INTO manid;

        EXIT WHEN mandats%NOTFOUND;

        select bor_id into boridInitial from mandat where man_id = manid;
        -- memoriser le bor-id du mandat


        OPEN depenses;

        LOOP
           FETCH depenses
            INTO dpcoid, depsuppression;

           EXIT WHEN depenses%NOTFOUND;
           -- casser le liens des mand_id dans depense_ctrl_planco
             -- supprimer le liens compteble <-> depense dans l inventaire
           jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, NULL);

           SELECT tbo_ordre, exe_ordre
             INTO tboordre, exeordre
             FROM jefy_depense.depense_ctrl_planco
            WHERE dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
           IF depsuppression = 'OUI' AND tboordre != 201
           THEN
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
              SELECT utl_ordre
                INTO utlordre
                FROM jefy_depense.depense_budget
               WHERE dep_id IN (SELECT dep_id
                                  FROM jefy_depense.depense_ctrl_planco
                                 WHERE dpco_id = dpcoid);

             select dep_id into depid from jefy_depense.depense_ctrl_planco where dpco_id=dpcoid;
              -- si cest le rejet d'un bordereau de paye
              if (tboordre=18) then
                  jefy_paye. paye_reversement.viser_rejet_reversement(depid);
              end if;
              jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
           END IF;
        END LOOP;

        CLOSE depenses;
     END LOOP;

     CLOSE mandats;

     -- pour les bordereau de PAF, appeler la proc
     jefy_paf.paf_budget.viser_rejet_paf(boridInitial);
     jefy_paye.paye_budget.viser_rejet_papaye(boridInitial);






     OPEN titres;

     LOOP
        FETCH titres
         INTO titid;

        EXIT WHEN titres%NOTFOUND;

        OPEN recettes;

        LOOP
           FETCH recettes
            INTO rpcoid, recsuppression;

           EXIT WHEN recettes%NOTFOUND;

-- casser le liens des tit_id dans recette_ctrl_planco
           SELECT r.rec_id_reduction
             INTO reduction
             FROM jefy_recette.recette r,
                  jefy_recette.recette_ctrl_planco rpco
            WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

           IF reduction IS NOT NULL
           THEN
              jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, NULL);
           ELSE
              jefy_recette.api.upd_recette_ctrl_planco (rpcoid, NULL);
           END IF;

           SELECT tbo_ordre, exe_ordre
             INTO tboordre, exeordre
             FROM jefy_recette.recette_ctrl_planco
            WHERE rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
           IF recsuppression = 'OUI' AND tboordre != 200
           THEN
              SELECT utl_ordre
                INTO utlordre
                FROM jefy_recette.recette_budget
               WHERE rec_id IN (SELECT rec_id
                                  FROM jefy_recette.recette_ctrl_planco
                                 WHERE rpco_id = rpcoid);

              SELECT rec_id
                INTO recid
                FROM jefy_recette.recette_ctrl_planco
               WHERE rpco_id = rpcoid;
          IF reduction IS NOT NULL
           THEN
              jefy_recette.api.del_reduction (recid, utlordre);
           ELSE
              jefy_recette.api.del_recette (recid, utlordre);
           END IF;
                        END IF;
        END LOOP;

        CLOSE recettes;
     END LOOP;

     CLOSE titres;

-- on passe le brjordre a VISE
     UPDATE bordereau_rejet
        SET brj_etat = 'VISE'
      WHERE brj_ordre = brjordre;
  END;

  FUNCTION get_selection_id (info VARCHAR)
     RETURN INTEGER
  IS
     selection   INTEGER;
  BEGIN
     SELECT maracuja.abricot_bord_selection_seq.NEXTVAL
       INTO selection
       FROM DUAL;

     RETURN selection;
  END;

  FUNCTION get_selection_borid (abrid INTEGER)
     RETURN INTEGER
  IS
     borid   INTEGER;
  BEGIN
     SELECT DISTINCT bor_id
                INTO borid
                FROM maracuja.abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN borid;
  END;

  PROCEDURE set_selection_id (
     a01abrid        INTEGER,
     a02lesdepid     VARCHAR,
     a03lesrecid     VARCHAR,
     a04utlordre     INTEGER,
     a05exeordre     INTEGER,
     a06tboordre     INTEGER,
     a07abrgroupby   VARCHAR,
     a08gescode      VARCHAR
  )
  IS
     chaine     VARCHAR (32000);
     premier    INTEGER;
     tmpdepid   INTEGER;
     tmprecid   INTEGER;
     cpt        INTEGER;
  BEGIN
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

     -- traitement de la chaine des depid
     IF a02lesdepid IS NOT NULL OR LENGTH (a02lesdepid) > 0
     THEN
        chaine := a02lesdepid;

        LOOP
           premier := 1;

           -- On recupere le depordre
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmpdepid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

           INSERT INTO maracuja.abricot_bord_selection
                       (abr_id, utl_ordre, dep_id, rec_id, exe_ordre,
                        tbo_ordre, abr_etat, abr_group_by,
                        ges_code
                       )
                VALUES (a01abrid,                                    --ABR_ID
                                 a04utlordre,                     --ult_ordre
                                             tmpdepid,               --DEP_ID
                                                      NULL,          --REC_ID
                                                           a05exeordre,
                                                                  --EXE_ORDRE
                        a06tboordre,                             --TBO_ORDRE,
                                    'ATTENTE',                    --ABR_ETAT,
                                              a07abrgroupby,
                                                     --,ABR_GROUP_BY,GES_CODE
                        a08gescode                                 --ges_code
                       );

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

     -- traitement de la chaine des recid
     IF a03lesrecid IS NOT NULL OR LENGTH (a03lesrecid) > 0
     THEN
        chaine := a03lesrecid;

        LOOP
           premier := 1;

           -- On recupere le depordre
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmprecid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

           INSERT INTO maracuja.abricot_bord_selection
                       (abr_id, utl_ordre, dep_id, rec_id, exe_ordre,
                        tbo_ordre, abr_etat, abr_group_by,
                        ges_code
                       )
                VALUES (a01abrid,                                    --ABR_ID
                                 a04utlordre,                     --ult_ordre
                                             NULL,                   --DEP_ID
                                                  tmprecid,          --REC_ID
                                                           a05exeordre,
                                                                  --EXE_ORDRE
                        a06tboordre,                             --TBO_ORDRE,
                                    'ATTENTE',                    --ABR_ETAT,
                                              a07abrgroupby,
                                                     --,ABR_GROUP_BY,GES_CODE
                        a08gescode                                 --ges_code
                       );

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM jefy_depense.depense_ctrl_planco
      WHERE dpco_id IN (SELECT dep_id
                          FROM abricot_bord_selection
                         WHERE abr_id = a01abrid) AND man_id IS NOT NULL;

     IF cpt > 0
     THEN
        raise_application_error
               (-20001,
                'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !'
               );
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.recette_ctrl_planco
      WHERE rpco_id IN (SELECT rec_id
                          FROM abricot_bord_selection
                         WHERE abr_id = a01abrid) AND tit_id IS NOT NULL;

     IF cpt > 0
     THEN
        raise_application_error
                (-20001,
                 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !'
                );
     END IF;

     bordereau_abricot.creer_bordereau (a01abrid);
  END;

  PROCEDURE set_selection_intern (
     a01abrid           INTEGER,
     a02lesdepid        VARCHAR,
     a03lesrecid        VARCHAR,
     a04utlordre        INTEGER,
     a05exeordre        INTEGER,
     a07abrgroupby      VARCHAR,
     a08gescodemandat   VARCHAR,
     a09gescodetitre    VARCHAR
  )
  IS
   borIdDep bordereau.bor_id%type;
   borIdRec bordereau.bor_id%type;
   flag integer;
    BEGIN
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

     -- les mandats
     set_selection_id (a01abrid,
                       a02lesdepid,
                       NULL,
                       a04utlordre,
                       a05exeordre,
                       201,
                       'bordereau_1D1M',
                       a08gescodemandat
                      );
-- les titres
     set_selection_id (-a01abrid,
                       NULL,
                       a03lesrecid,
                       a04utlordre,
                       a05exeordre,
                       200,
                       'bordereau_1R1T',
                       a09gescodetitre
                      );
                          -- verifier que les bordereaux crees sont coherents entre eux                     
                          select count(*) into flag from (  select distinct bor_id from abricot_bord_selection where abr_id=a01abrid );
                     if (flag<>1) then 
                        raise_application_error (-20001,'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id='||a01abrid);  
                     end if;
     select max(bor_id) into borIdDep from abricot_bord_selection where abr_id=a01abrid;
          select count(*) into flag from (                       
          select distinct bor_id from abricot_bord_selection where abr_id=-a01abrid
     );
     if (flag<>1) then raise_application_error (-20001,'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id='||-a01abrid);  end if;
     select max(bor_id) into borIdRec from abricot_bord_selection where abr_id=-a01abrid;                           
     
     -- verifier qu'on a 1 prest_id par titre/mandat
     select count(*) into flag from (select prest_id, count(distinct tit_id) nb from titre where bor_id=borIdRec group by prest_id) where nb>1;
     if (flag >0) then
        raise_application_error (-20001,'Plusieurs titres concernant la mÃªme prestation ne peuvent Ãªtre intÃ©grÃ©s sur un seul bordereau. CrÃ©ez plusieurs bordereaux.');
     end if;
     
     select count(*) into flag from (select prest_id, count(distinct man_id) nb from mandat where bor_id=borIdDep group by prest_id) where nb>1;
     if (flag >0) then
        raise_application_error (-20001,'Plusieurs mandats concernant la mÃªme prestation ne peuvent Ãªtre intÃ©grÃ©s sur un seul bordereau. CrÃ©ez plusieurs bordereaux.');
     end if;     
     ctrl_bordereaux_PI(borIdDep, borIdRec);                                                                                 
   END;

  PROCEDURE set_selection_paye (
     a01abrid           INTEGER,
     a02lesdepid        VARCHAR,
     a03lesrecid        VARCHAR,
     a04utlordre        INTEGER,
     a05exeordre        INTEGER,
     a07abrgroupby      VARCHAR,
     a08gescodemandat   VARCHAR,
     a09gescodetitre    VARCHAR
  )
  IS
     boridtmp    INTEGER;
     moisordre   INTEGER;
  BEGIN
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

     -- les mandats de papaye
     set_selection_id (a01abrid,
                       a02lesdepid,
                       NULL,
                       a04utlordre,
                       a05exeordre,
                       3,
                       'bordereau_1D1M',
                       a08gescodemandat
                      );
     boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

  -- bascule du brouillard de papaye

  -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
  END;

-- creer bordereau (tbo_ordre) + numerotation
  FUNCTION get_num_borid (
     tboordre   INTEGER,
     exeordre   INTEGER,
     gescode    VARCHAR,
     utlordre   INTEGER
  )
     RETURN INTEGER
  IS
     cpt      INTEGER;
     borid    INTEGER;
     bornum   INTEGER;
  BEGIN
-- creation du bor_id --
     SELECT bordereau_seq.NEXTVAL
       INTO borid
       FROM DUAL;

-- creation du bordereau --
     bornum := -1;

     INSERT INTO bordereau
                 (bor_date_visa, bor_etat, bor_id, bor_num, bor_ordre,
                  exe_ordre, ges_code, tbo_ordre, utl_ordre, utl_ordre_visa,
                  bor_date_creation
                 )
          VALUES (NULL,                                      --BOR_DATE_VISA,
                       'VALIDE',                                  --BOR_ETAT,
                                borid,                              --BOR_ID,
                                      bornum,                      --BOR_NUM,
                                             -borid,             --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                  exeordre,                                      --EXE_ORDRE,
                           gescode,                               --GES_CODE,
                                   tboordre,                     --TBO_ORDRE,
                                            utlordre,            --UTL_ORDRE,
                                                     NULL,   --UTL_ORDRE_VISA
                  SYSDATE
                 );

     RETURN borid;
  END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
  PROCEDURE bordereau_1r1t (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

     CURSOR rec_tit
     IS
        SELECT   r.*
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        ORDER BY r.pco_num, r.rec_id ASC;
  BEGIN
     OPEN rec_tit;

     LOOP
        FETCH rec_tit
         INTO tmprecette;

        EXIT WHEN rec_tit%NOTFOUND;
        cpt := set_titre_recette (tmprecette.rpco_id, monborid);
     END LOOP;

     CLOSE rec_tit;
  END;

  PROCEDURE bordereau_nr1t (abrid INTEGER, monborid INTEGER)
  IS
     ht           NUMBER (12, 2);
     tva          NUMBER (12, 2);
     ttc          NUMBER (12, 2);
     pconumero    VARCHAR (20);
     nbpieces     INTEGER;
     cpt          INTEGER;
     titidtemp    INTEGER;
     tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

-- curseur de regroupement
     CURSOR rec_tit_group_by
     IS
        SELECT   r.pco_num, SUM (r.rpco_ht_saisie), SUM (r.rpco_tva_saisie),
                 SUM (r.rpco_ttc_saisie)
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        GROUP BY r.pco_num
        ORDER BY r.pco_num ASC;

     CURSOR rec_tit
     IS
        SELECT   r.*
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
             AND r.pco_num = pconumero
        ORDER BY r.pco_num ASC, r.rec_id;
  BEGIN
     OPEN rec_tit_group_by;

     LOOP
        FETCH rec_tit_group_by
         INTO pconumero, ht, tva, ttc;

        EXIT WHEN rec_tit_group_by%NOTFOUND;
        titidtemp := 0;

        OPEN rec_tit;

        LOOP
           FETCH rec_tit
            INTO tmprecette;

           EXIT WHEN rec_tit%NOTFOUND;

           IF titidtemp = 0
           THEN
              titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
           ELSE
              UPDATE jefy_recette.recette_ctrl_planco
                 SET tit_id = titidtemp
               WHERE rpco_id = tmprecette.rpco_id;
           END IF;
        END LOOP;

        CLOSE rec_tit;

-- recup du nombre de pieces
-- TODO
        nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

        -- maj des montants du titre
        UPDATE titre
           SET tit_ht = ht,
               tit_nb_piece = nbpieces,
               tit_ttc = ttc,
               tit_tva = tva,
               tit_libelle = 'TITRE COLLECTIF'
         WHERE tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
     END LOOP;

     CLOSE rec_tit_group_by;
  END;

  PROCEDURE bordereau_nd1m (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;
     abrgroupby   abricot_bord_selection.abr_group_by%TYPE;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
     CURSOR mand_dep_convra
     IS
        SELECT DISTINCT d.*
                   FROM abricot_bord_selection ab,
                        jefy_depense.depense_ctrl_planco d,
                        jefy_depense.depense_budget db,
                        jefy_depense.engage_budget e,
                        maracuja.v_convention_limitative c
                  WHERE d.dpco_id = ab.dep_id
                    AND abr_id = abrid
                    AND db.dep_id = d.dep_id
                    AND e.eng_id = db.eng_id
                    AND e.org_id = c.org_id(+)
                    AND e.exe_ordre = c.exe_ordre(+)
                    AND c.org_id IS NOT NULL
                    AND d.man_id IS NULL
                    AND ab.abr_etat = 'ATTENTE'
               ORDER BY d.pco_num ASC, d.DEP_ID;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
  BEGIN
     OPEN mand_dep_convra;

     LOOP
        FETCH mand_dep_convra
         INTO tmpdepense;

        EXIT WHEN mand_dep_convra%NOTFOUND;
        cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
     END LOOP;

     CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
     SELECT DISTINCT abr_group_by
                INTO abrgroupby
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
     IF (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod')
     THEN
        cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
     END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
     IF (abrgroupby = 'ndep_mand_fou_rib_pco_mod')
     THEN
        cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
     END IF;
  END;

  PROCEDURE bordereau_1d1m (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;

     CURSOR dep_mand
     IS
        SELECT   d.*
            FROM abricot_bord_selection ab,
                 jefy_depense.depense_ctrl_planco d
           WHERE d.dpco_id = ab.dep_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        ORDER BY d.pco_num ASC, d.DEP_ID;
  BEGIN
     OPEN dep_mand;

     LOOP
        FETCH dep_mand
         INTO tmpdepense;

        EXIT WHEN dep_mand%NOTFOUND;
        cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
     END LOOP;

     CLOSE dep_mand;
  END;

  PROCEDURE bordereau_1d1m1r1t (
     abrid      INTEGER,
     boridep    INTEGER,
     boridrec   INTEGER
  )
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     bordereau_1d1m (abrid, boridep);
     bordereau_1r1t (abrid, boridrec);
         ctrl_bordereaux_PI(boridep, boridrec);
          END;

-- les mandats et titres
  FUNCTION set_mandat_depense (dpcoid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt               INTEGER;
     flag              INTEGER;
     ladepense         jefy_depense.depense_ctrl_planco%ROWTYPE;
     ladepensepapier   jefy_depense.depense_papier%ROWTYPE;
     leengagebudget    jefy_depense.engage_budget%ROWTYPE;
     gescode           gestion.ges_code%TYPE;
     manid             mandat.man_id%TYPE;
     manorgine_key     mandat.man_orgine_key%TYPE;
     manorigine_lib    mandat.man_origine_lib%TYPE;
     oriordre          mandat.ori_ordre%TYPE;
     prestid           mandat.prest_id%TYPE;
     torordre          mandat.tor_ordre%TYPE;
     virordre          mandat.pai_ordre%TYPE;
     mannumero         mandat.man_numero%TYPE;
  BEGIN
-- recuperation du ges_code --
     SELECT ges_code
       INTO gescode
       FROM bordereau
      WHERE bor_id = borid;

     SELECT *
       INTO ladepense
       FROM jefy_depense.depense_ctrl_planco
      WHERE dpco_id = dpcoid;

     SELECT DISTINCT dpp.*
                INTO ladepensepapier
                FROM jefy_depense.depense_papier dpp,
                     jefy_depense.depense_budget db,
                     jefy_depense.depense_ctrl_planco dpco
               WHERE db.dep_id = dpco.dep_id
                 AND dpp.dpp_id = db.dpp_id
                 AND dpco_id = dpcoid;

     SELECT eb.*
       INTO leengagebudget
       FROM jefy_depense.engage_budget eb,
            jefy_depense.depense_budget db,
            jefy_depense.depense_ctrl_planco dpco
      WHERE db.eng_id = eb.eng_id
        AND db.dep_id = dpco.dep_id
        AND dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
     SELECT COUNT (*)
       INTO flag
       FROM maracuja.v_organ_exer
      WHERE org_id = leengagebudget.org_id
        AND exe_ordre = leengagebudget.exe_ordre;

     IF (flag = 0)
     THEN
        raise_application_error
                    (-20001,
                        'La ligne budgetaire affectee a l''engagement num. '
                     || leengagebudget.eng_numero
                     || ' n''est pas ouverte sur '
                     || leengagebudget.exe_ordre
                     || '.'
                    );
     END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
     manorgine_key := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
     manorigine_lib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
     oriordre :=
        gestionorigine.traiter_orgid (leengagebudget.org_id,
                                      leengagebudget.exe_ordre
                                     );

--PRESTID : PRESTATION INTERNE --
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;

     IF cpt = 1
     THEN
        SELECT prest_id
          INTO prestid
          FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;
     ELSE
        prestid := NULL;
     END IF;

--TORORDRE : ORIGINE DU MANDAT --
     torordre := 1;
--VIRORDRE --
     virordre := NULL;

-- creation du man_id --
     SELECT mandat_seq.NEXTVAL
       INTO manid
       FROM DUAL;

-- recup du numero de mandat
     mannumero := -1;

     INSERT INTO mandat
                 (bor_id, brj_ordre, exe_ordre,
                  fou_ordre, ges_code, man_date_remise, man_date_visa_princ,
                  man_etat, man_etat_remise, man_ht,
                  man_id, man_motif_rejet, man_nb_piece, man_numero,
                  man_numero_rejet, man_ordre, man_orgine_key,
                  man_origine_lib, man_ttc,
                  man_tva,
                  mod_ordre, ori_ordre, pco_num,
                  prest_id, tor_ordre, pai_ordre, org_ordre,
                  rib_ordre_ordonnateur,
                  rib_ordre_comptable
                 )
          VALUES (borid,                                            --BOR_ID,
                        NULL,                                    --BRJ_ORDRE,
                             ladepensepapier.exe_ordre,          --EXE_ORDRE,
                  ladepensepapier.fou_ordre,                     --FOU_ORDRE,
                                            gescode,              --GES_CODE,
                                                    NULL,  --MAN_DATE_REMISE,
                                                         NULL,
                                                       --MAN_DATE_VISA_PRINC,
                  'ATTENTE',                                      --MAN_ETAT,
                            'ATTENTE',                     --MAN_ETAT_REMISE,
                                      ladepense.dpco_montant_budgetaire,
                                                                    --MAN_HT,
                  manid,                                            --MAN_ID,
                        NULL,                              --MAN_MOTIF_REJET,
                             ladepensepapier.dpp_nb_piece,    --MAN_NB_PIECE,
                                                          mannumero,
                                                                --MAN_NUMERO,
                  NULL,                                   --MAN_NUMERO_REJET,
                       -manid,                                   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                              manorgine_key,                --MAN_ORGINE_KEY,
                  manorigine_lib,                          --MAN_ORIGINE_LIB,
                                 ladepense.dpco_ttc_saisie,        --MAN_TTC,
                    ladepense.dpco_ttc_saisie
                  - ladepense.dpco_montant_budgetaire,             --MAN_TVA,
                  ladepensepapier.mod_ordre,                     --MOD_ORDRE,
                                            oriordre,            --ORI_ORDRE,
                                                     ladepense.pco_num,
                                                                   --PCO_NUM,
                  prestid,                                        --PREST_ID,
                          torordre,                              --TOR_ORDRE,
                                   virordre,                      --VIR_ORDRE
                                            leengagebudget.org_id,
                                                                  --org_ordre
                  ladepensepapier.rib_ordre,                       --rib ordo
                  ladepensepapier.rib_ordre                  -- rib_comptable
                 );

-- maj du man_id  dans la depense
     UPDATE jefy_depense.depense_ctrl_planco
        SET man_id = manid
      WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

     -- recup du brouillard
     set_mandat_brouillard (manid);
     RETURN manid;
  END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
  FUNCTION set_mandat_depenses (lesdpcoid VARCHAR, borid INTEGER)
     RETURN INTEGER
  IS
     cpt             INTEGER;
     premier         INTEGER;
     tmpdpcoid       INTEGER;
     chaine          VARCHAR (5000);
     premierdpcoid   INTEGER;
     manid           INTEGER;
     ttc             mandat.man_ttc%TYPE;
     tva             mandat.man_tva%TYPE;
     ht              mandat.man_ht%TYPE;
     utlordre        INTEGER;
     nb_pieces       INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
     premierdpcoid := NULL;

     -- traitement de la chaine des depid xx$xx$xx$.....$x$$
     IF lesdpcoid IS NOT NULL OR LENGTH (lesdpcoid) > 0
     THEN
        chaine := lesdpcoid;

        LOOP
           premier := 1;

           -- On recupere le depid
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmpdpcoid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

-- creation du mandat lie au borid
           IF premierdpcoid IS NULL
           THEN
              manid := set_mandat_depense (tmpdpcoid, borid);

              -- suppression du brouillard car il est uniquement sur la premiere depense
              DELETE FROM mandat_brouillard
                    WHERE man_id = manid;

              premierdpcoid := tmpdpcoid;
           ELSE
              -- maj du man_id  dans la depense
              UPDATE jefy_depense.depense_ctrl_planco
                 SET man_id = manid
               WHERE dpco_id = tmpdpcoid;

              -- recup de la depense (maracuja)
              SELECT DISTINCT dpp.utl_ordre
                         INTO utlordre
                         FROM jefy_depense.depense_papier dpp,
                              jefy_depense.depense_budget db,
                              jefy_depense.depense_ctrl_planco dpco
                        WHERE db.dep_id = dpco.dep_id
                          AND dpp.dpp_id = db.dpp_id
                          AND dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
           END IF;

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
     SELECT SUM (dpco_ttc_saisie),
            SUM (dpco_ttc_saisie - dpco_montant_budgetaire),
            SUM (dpco_montant_budgetaire)
       INTO ttc,
            tva,
            ht
       FROM jefy_depense.depense_ctrl_planco
      WHERE man_id = manid;

-- recup du nb de pieces
     SELECT SUM (dpp.dpp_nb_piece)
       INTO nb_pieces
       FROM jefy_depense.depense_papier dpp,
            jefy_depense.depense_budget db,
            jefy_depense.depense_ctrl_planco dpco
      WHERE db.dep_id = dpco.dep_id
        AND dpp.dpp_id = db.dpp_id
        AND man_id = manid;

-- maj du mandat
     UPDATE mandat
        SET man_ht = ht,
            man_tva = tva,
            man_ttc = ttc,
            man_nb_piece = nb_pieces
      WHERE man_id = manid;

-- recup du brouillard
     set_mandat_brouillard (manid);
     RETURN cpt;
  END;

  FUNCTION set_titre_recette (rpcoid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
--     jefytitre           jefy.titre%ROWTYPE;
     gescode             gestion.ges_code%TYPE;
     titid               titre.tit_id%TYPE;
     titorginekey        titre.tit_orgine_key%TYPE;
     titoriginelib       titre.tit_origine_lib%TYPE;
     oriordre            titre.ori_ordre%TYPE;
     prestid             titre.prest_id%TYPE;
     torordre            titre.tor_ordre%TYPE;
     modordre            titre.mod_ordre%TYPE;
     presid              INTEGER;
     cpt                 INTEGER;
     virordre            INTEGER;
     flag                INTEGER;
     recettepapier       jefy_recette.recette_papier%ROWTYPE;
     recettebudget       jefy_recette.recette_budget%ROWTYPE;
     facturebudget       jefy_recette.facture_budget%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
  BEGIN
-- recuperation du ges_code --
     SELECT ges_code
       INTO gescode
       FROM bordereau
      WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
     SELECT *
       INTO recettectrlplanco
       FROM jefy_recette.recette_ctrl_planco
      WHERE rpco_id = rpcoid;

     SELECT *
       INTO recettebudget
       FROM jefy_recette.recette_budget
      WHERE rec_id = recettectrlplanco.rec_id;

     SELECT *
       INTO facturebudget
       FROM jefy_recette.facture_budget
      WHERE fac_id = recettebudget.fac_id;

     SELECT *
       INTO recettepapier
       FROM jefy_recette.recette_papier
      WHERE rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
     SELECT COUNT (*)
       INTO flag
       FROM maracuja.v_organ_exer
      WHERE org_id = facturebudget.org_id
        AND exe_ordre = facturebudget.exe_ordre;

     IF (flag = 0)
     THEN
        raise_application_error
                       (-20001,
                           'La ligne budgetaire affectee a la recette num. '
                        || recettebudget.rec_numero
                        || ' n''est pas ouverte sur '
                        || facturebudget.exe_ordre
                        || '.'
                       );
     END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
     titorginekey := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
     titoriginelib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
     oriordre :=
        gestionorigine.traiter_orgid (facturebudget.org_id,
                                      facturebudget.exe_ordre
                                     );

--PRESTID : PRESTATION INTERNE --
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;

     IF cpt = 1
     THEN
        SELECT prest_id
          INTO prestid
          FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;
     ELSE
        prestid := NULL;
     END IF;

--TORORDRE : ORIGINE DU MANDAT --
     torordre := 1;
--VIRORDRE --
     virordre := NULL;

     SELECT titre_seq.NEXTVAL
       INTO titid
       FROM DUAL;

     INSERT INTO titre
                 (bor_id, bor_ordre, brj_ordre, exe_ordre, ges_code,
                  mod_ordre, ori_ordre, pco_num, prest_id,
                  tit_date_remise, tit_date_visa_princ, tit_etat,
                  tit_etat_remise, tit_ht, tit_id, tit_motif_rejet,
                  tit_nb_piece, tit_numero, tit_numero_rejet, tit_ordre,
                  tit_orgine_key, tit_origine_lib,
                  tit_ttc,
                  tit_tva, tor_ordre,
                  utl_ordre, org_ordre,
                  fou_ordre, mor_ordre, pai_ordre,
                  rib_ordre_ordonnateur, rib_ordre_comptable,
                  tit_libelle
                 )
          VALUES (borid,                                            --BOR_ID,
                        -borid,                                  --BOR_ORDRE,
                               NULL,                             --BRJ_ORDRE,
                                    recettepapier.exe_ordre,     --EXE_ORDRE,
                                                            gescode,
                                                                  --GES_CODE,
                  NULL,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                       oriordre,                                 --ORI_ORDRE,
                                recettectrlplanco.pco_num,         --PCO_NUM,
                                                          prestid,
                                                                  --PREST_ID,
                  SYSDATE,                                 --TIT_DATE_REMISE,
                          NULL,                        --TIT_DATE_VISA_PRINC,
                               'ATTENTE',                         --TIT_ETAT,
                  'ATTENTE',                               --TIT_ETAT_REMISE,
                            recettectrlplanco.rpco_ht_saisie,       --TIT_HT,
                                                             titid, --TIT_ID,
                                                                   NULL,
                                                           --TIT_MOTIF_REJET,
                  recettepapier.rpp_nb_piece,                 --TIT_NB_PIECE,
                                             -1,
                             --TIT_NUMERO, numerotation en fin de transaction
                                                NULL,     --TIT_NUMERO_REJET,
                                                     -titid,
                       --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                  titorginekey,                             --TIT_ORGINE_KEY,
                               titoriginelib,              --TIT_ORIGINE_LIB,
                  recettectrlplanco.rpco_ttc_saisie,               --TIT_TTC,
                  recettectrlplanco.rpco_tva_saisie,               --TIT_TVA,
                                                    torordre,    --TOR_ORDRE,
                  recettepapier.utl_ordre,                        --UTL_ORDRE
                                          facturebudget.org_id,  --ORG_ORDRE,
                  recettepapier.fou_ordre,
                                -- FOU_ORDRE  --TOCHECK certains sont nuls...
                                          facturebudget.mor_ordre,
                                                                  --MOR_ORDRE
                                                                  NULL,
                                                                 -- VIR_ORDRE
                  recettepapier.rib_ordre, recettepapier.rib_ordre,
                  recettebudget.rec_lib
                 );

-- maj du tit_id dans la recette
     UPDATE jefy_recette.recette_ctrl_planco
        SET tit_id = titid
      WHERE rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
     RETURN titid;
  END;

  FUNCTION set_titre_recettes (lesrpcoid VARCHAR, borid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     raise_application_error (-20001, 'OPERATION NON TRAITEE');
     RETURN cpt;
  END;

/*

FUNCTION ndep_mand_org_fou_rib_pco (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_org_fou_rib_pco IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num;

CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;

CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco;
LOOP
FETCH ndep_mand_org_fou_rib_pco INTO
orgid,fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
OPEN lesdpcoids;
LOOP
FETCH lesdpcoids INTO tmpdpcoid;
EXIT WHEN lesdpcoids%NOTFOUND;
 chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
END LOOP;
CLOSE lesdpcoids;
else
 OPEN lesdpcoidsribnull;
LOOP
FETCH lesdpcoidsribnull INTO tmpdpcoid;
EXIT WHEN lesdpcoidsribnull%NOTFOUND;
 chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
END LOOP;
CLOSE lesdpcoidsribnull;
end if;
 chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco;

RETURN cpt;
END;

*/
  FUNCTION ndep_mand_org_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt            INTEGER;
     fouordre       v_fournisseur.fou_ordre%TYPE;
     ribordre       v_rib.rib_ordre%TYPE;
     pconum         plan_comptable.pco_num%TYPE;
     modordre       mode_paiement.mod_ordre%TYPE;
     orgid          jefy_admin.organ.org_id%TYPE;
     ht             mandat.man_ht%TYPE;
     tva            mandat.man_ht%TYPE;
     ttc            mandat.man_ht%TYPE;
     budgetaire     mandat.man_ht%TYPE;
      
      Cursor Ndep_Mand_Org_Fou_Rib_Pco_Mod
      IS                 
        SELECT e.org_id, dpp.fou_ordre, dpp.rib_ordre, d.pco_num,
          dpp.mod_ordre, SUM (dpco_ht_saisie) ht,
          SUM (dpco_tva_saisie) tva, SUM (dpco_ttc_saisie) ttc,
          Sum (Dpco_Montant_Budgetaire) Budgetaire
        FROM MARACUJA.abricot_bord_selection ab,
          jefy_depense.depense_ctrl_planco d,
          jefy_depense.depense_budget db,
          Jefy_Depense.Depense_Papier Dpp,
          Jefy_Depense.Engage_Budget E,
          jefy_Admin.organ Vo,
          Maracuja.V_Fournisseur Vf
        WHERE d.dpco_id = ab.dep_id
          And Dpp.Dpp_Id = Db.Dpp_Id
          and dpp.fou_ordre = vf.fou_ordre
          And Db.Dep_Id = D.Dep_Id
          AND abr_id = abrid
          And E.Eng_Id = Db.Eng_Id
          and vo.org_id = e.org_id
         AND ab.abr_etat = 'ATTENTE'
         And D.Man_Id Is Null
        Group By vo.org_univ, vo.org_etab, vo.org_UB, vo.org_CR, vo.org_souscr,E.Org_Id,
          Dpp.Fou_Ordre,
          vf.fou_code,
          dpp.rib_ordre,
          d.pco_num,
          Dpp.Mod_Ordre
        order by vo.org_univ, vo.org_etab, vo.org_UB, vo.org_CR, vo.org_souscr,
          vf.fou_code,
          DPP.RIB_ORDRE,
          D.Pco_Num,
          DPP.MOD_ORDRE;

     CURSOR lesdpcoids
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp,
               jefy_depense.engage_budget e
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND e.eng_id = db.eng_id
           AND ab.abr_etat = 'ATTENTE'
           AND e.org_id = orgid
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre = ribordre
           AND d.pco_num = pconum
           AND dpp.mod_ordre = modordre
           AND d.man_id IS NULL
           order by d.dpco_id;

     CURSOR lesdpcoidsribnull
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp,
               jefy_depense.engage_budget e
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND e.eng_id = db.eng_id
           AND ab.abr_etat = 'ATTENTE'
           AND e.org_id = orgid
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre IS NULL
           AND d.pco_num = pconum
           AND dpp.mod_ordre = modordre
           AND d.man_id IS NULL
           order by d.dpco_id;

     chainedpcoid   VARCHAR (5000);
     tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     OPEN ndep_mand_org_fou_rib_pco_mod;

     LOOP
        FETCH ndep_mand_org_fou_rib_pco_mod
         INTO orgid, fouordre, ribordre, pconum, modordre, ht, tva, ttc,
              budgetaire;

        EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
        chainedpcoid := NULL;

        IF ribordre IS NOT NULL
        THEN
           OPEN lesdpcoids;

           LOOP
              FETCH lesdpcoids
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoids%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoids;
        ELSE
           OPEN lesdpcoidsribnull;

           LOOP
              FETCH lesdpcoidsribnull
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoidsribnull%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoidsribnull;
        END IF;

        chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
        cpt := set_mandat_depenses (chainedpcoid, borid);
     END LOOP;

     CLOSE ndep_mand_org_fou_rib_pco_mod;

     RETURN cpt;
  END;

/*
FUNCTION ndep_mand_fou_rib_pco  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN

OPEN ndep_mand_fou_rib_pco;
LOOP
FETCH ndep_mand_fou_rib_pco INTO
fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
OPEN lesdpcoids;
LOOP
FETCH lesdpcoids INTO tmpdpcoid;
EXIT WHEN lesdpcoids%NOTFOUND;
 chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
END LOOP;
CLOSE lesdpcoids;
else
 OPEN lesdpcoidsribnull;
LOOP
FETCH lesdpcoidsribnull INTO tmpdpcoid;
EXIT WHEN lesdpcoidsribnull%NOTFOUND;
 chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
END LOOP;
CLOSE lesdpcoidsribnull;
end if;

 chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco;

RETURN cpt;
END;
*/
  FUNCTION ndep_mand_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt            INTEGER;
     fouordre       v_fournisseur.fou_ordre%TYPE;
     ribordre       v_rib.rib_ordre%TYPE;
     pconum         plan_comptable.pco_num%TYPE;
     modordre       mode_paiement.mod_ordre%TYPE;
     orgid          jefy_admin.organ.org_id%TYPE;
     ht             mandat.man_ht%TYPE;
     tva            mandat.man_ht%TYPE;
     ttc            mandat.man_ht%TYPE;
     budgetaire     mandat.man_ht%TYPE;

      Cursor Ndep_Mand_Fou_Rib_Pco_Mod
      IS
        SELECT dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre,
          SUM (dpco_ht_saisie) ht, SUM (dpco_tva_saisie) tva,
          SUM (dpco_ttc_saisie) ttc,
          Sum (Dpco_Montant_Budgetaire) Budgetaire
        FROM MARACUJA.abricot_bord_selection ab,
          jefy_depense.depense_ctrl_planco d,
          Jefy_Depense.Depense_Budget Db,
          Jefy_Depense.Depense_Papier Dpp,
          Maracuja.V_Fournisseur Vf
        WHERE d.dpco_id = ab.dep_id
          And Dpp.Dpp_Id = Db.Dpp_Id
          And Dpp.Fou_Ordre = Vf.Fou_Ordre 
          And Db.Dep_Id = D.Dep_Id
          AND abr_id = abrid
          And Ab.Abr_Etat = 'ATTENTE'
          And D.Man_Id Is Null
        Group By Dpp.Fou_Ordre, Vf.Fou_Code, Dpp.Rib_Ordre, D.Pco_Num, Dpp.Mod_Ordre
        Order By Vf.Fou_Code, Dpp.Rib_Ordre, D.Pco_Num, Dpp.Mod_Ordre;

     CURSOR lesdpcoids
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND ab.abr_etat = 'ATTENTE'
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre = ribordre
           AND d.pco_num = pconum
           AND d.man_id IS NULL
           AND dpp.mod_ordre = modordre
           order by d.dpco_id;

     CURSOR lesdpcoidsnull
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND ab.abr_etat = 'ATTENTE'
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre IS NULL
           AND d.pco_num = pconum
           AND d.man_id IS NULL
           AND dpp.mod_ordre = modordre
           order by d.dpco_id;

     chainedpcoid   VARCHAR (5000);
     tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     OPEN ndep_mand_fou_rib_pco_mod;

     LOOP
        FETCH ndep_mand_fou_rib_pco_mod
         INTO fouordre, ribordre, pconum, modordre, ht, tva, ttc,
              budgetaire;

        EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
        chainedpcoid := NULL;

        IF ribordre IS NOT NULL
        THEN
           OPEN lesdpcoids;

           LOOP
              FETCH lesdpcoids
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoids%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoids;
        ELSE
           OPEN lesdpcoidsnull;

           LOOP
              FETCH lesdpcoidsnull
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoidsnull%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoidsnull;
        END IF;

        chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
        cpt := set_mandat_depenses (chainedpcoid, borid);
     END LOOP;

     CLOSE ndep_mand_fou_rib_pco_mod;

     RETURN cpt;
  END;

-- procedures de verifications
  FUNCTION selection_valide (abrid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

-- meme exercice

     -- si PI somme recette = somme depense

     -- recette_valides

     -- depense_valides
     RETURN cpt;
  END;

  FUNCTION recette_valide (recid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     RETURN cpt;
  END;

  FUNCTION depense_valide (depid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     RETURN cpt;
  END;

  FUNCTION verif_bordereau_selection (borid INTEGER, abrid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

     -- verifier sum TTC recette selection = sum TTC titre du bord

     -- verifier sum TTC depense  = sum TTC mandat du bord

     -- verifier sum TTC recette  = sum TTC titre  du bord
     RETURN cpt;
  END;

-- procedures de locks de transaction
  PROCEDURE lock_mandats
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;
  END;

  PROCEDURE lock_titres
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;
  END;

  PROCEDURE get_depense_jefy_depense (manid INTEGER)
  IS
     depid               depense.dep_id%TYPE;
     jefydepensebudget   jefy_depense.depense_budget%ROWTYPE;
     tmpdepensepapier    jefy_depense.depense_papier%ROWTYPE;
     jefydepenseplanco   jefy_depense.depense_ctrl_planco%ROWTYPE;
     lignebudgetaire     depense.dep_ligne_budgetaire%TYPE;
     fouadresse          depense.dep_adresse%TYPE;
     founom              depense.dep_fournisseur%TYPE;
     lotordre            depense.dep_lot%TYPE;
     marordre            depense.dep_marches%TYPE;
     fouordre            depense.fou_ordre%TYPE;
     gescode             depense.ges_code%TYPE;
     modordre            depense.mod_ordre%TYPE;
     cpt                 INTEGER;
     tcdordre            type_credit.tcd_ordre%TYPE;
     tcdcode             type_credit.tcd_code%TYPE;
     ecd_ordre_ema       ecriture_detail.ecd_ordre%TYPE;
     orgid               INTEGER;

     CURSOR depenses
     IS
        SELECT db.*
          FROM jefy_depense.depense_budget db,
               jefy_depense.depense_ctrl_planco dpco
         WHERE dpco.man_id = manid AND db.dep_id = dpco.dep_id;
  BEGIN
     OPEN depenses;

     LOOP
        FETCH depenses
         INTO jefydepensebudget;

        EXIT WHEN depenses%NOTFOUND;

        -- creation du depid --
        SELECT depense_seq.NEXTVAL
          INTO depid
          FROM DUAL;

        -- creation de lignebudgetaire--
        SELECT org_ub || ' ' || org_cr || ' ' || org_souscr
          INTO lignebudgetaire
          FROM jefy_admin.organ
         WHERE org_id = (SELECT org_id
                           FROM jefy_depense.engage_budget
                          WHERE eng_id = jefydepensebudget.eng_id
                                                                 --AND eng_stat !='A'
                       );

        --recuperer le type de credit a partir de la commande
        SELECT tcd_ordre
          INTO tcdordre
          FROM jefy_depense.engage_budget
         WHERE eng_id = jefydepensebudget.eng_id;

        --AND eng_stat !='A'
        SELECT org_ub, org_id
          INTO gescode, orgid
          FROM jefy_admin.organ
         WHERE org_id = (SELECT org_id
                           FROM jefy_depense.engage_budget
                          WHERE eng_id = jefydepensebudget.eng_id
                                                                 --AND eng_stat !='A'
                       );

        -- fouadresse --
        SELECT    SUBSTR ((   adr_adresse1
                           || ' '
                           || adr_adresse2
                           || ' '
                           || adr_cp
                           || ' '
                           || adr_ville
                          ),
                          1,
                          196
                         )
               || '...'
          INTO fouadresse
          FROM v_fournisseur
         WHERE fou_ordre = (SELECT fou_ordre
                              FROM jefy_depense.engage_budget
                             WHERE eng_id = jefydepensebudget.eng_id
                                                                    --AND eng_stat !='A'
                          );

        -- founom --
        SELECT adr_nom || ' ' || adr_prenom
          INTO founom
          FROM v_fournisseur
         WHERE fou_ordre = (SELECT fou_ordre
                              FROM jefy_depense.engage_budget
                             WHERE eng_id = jefydepensebudget.eng_id
                                                                    --AND eng_stat !='A'
                          );

        -- fouordre --
        SELECT fou_ordre
          INTO fouordre
          FROM jefy_depense.engage_budget
         WHERE eng_id = jefydepensebudget.eng_id;

        --AND eng_stat !='A'

        -- lotordre --
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_marches.attribution
         WHERE att_ordre = (SELECT att_ordre
                              FROM jefy_depense.engage_ctrl_marche
                             WHERE eng_id = jefydepensebudget.eng_id);

        IF cpt = 0
        THEN
           lotordre := NULL;
        ELSE
           SELECT lot_ordre
             INTO lotordre
             FROM jefy_marches.attribution
            WHERE att_ordre = (SELECT att_ordre
                                 FROM jefy_depense.engage_ctrl_marche
                                WHERE eng_id = jefydepensebudget.eng_id);
        END IF;

        -- marordre --
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_marches.lot
         WHERE lot_ordre = lotordre;

        IF cpt = 0
        THEN
           marordre := NULL;
        ELSE
           SELECT mar_ordre
             INTO marordre
             FROM jefy_marches.lot
            WHERE lot_ordre = lotordre;
        END IF;

        --MOD_ORDRE --
        SELECT mod_ordre
          INTO modordre
          FROM jefy_depense.depense_papier
         WHERE dpp_id = jefydepensebudget.dpp_id;

        -- recuperer l'ecriture_detail pour emargements semi-auto
        SELECT ecd_ordre
          INTO ecd_ordre_ema
          FROM jefy_depense.depense_ctrl_planco
         WHERE dep_id = jefydepensebudget.dep_id;

        -- recup de la depense papier
        SELECT *
          INTO tmpdepensepapier
          FROM jefy_depense.depense_papier
         WHERE dpp_id = jefydepensebudget.dpp_id;

        -- recup des infos de depense_ctrl_planco
        SELECT *
          INTO jefydepenseplanco
          FROM jefy_depense.depense_ctrl_planco
         WHERE dep_id = jefydepensebudget.dep_id;

        -- creation de la depense --
        INSERT INTO depense
             VALUES (fouadresse,                               --DEP_ADRESSE,
                                NULL,                      --DEP_DATE_COMPTA,
                                     tmpdepensepapier.dpp_date_reception,
                                                        --DEP_DATE_RECEPTION,
                     tmpdepensepapier.dpp_date_service_fait,
                                                          --DEP_DATE_SERVICE,
                                                            'VALIDE',
                                                                  --DEP_ETAT,
                                                                     founom,
                                                           --DEP_FOURNISSEUR,
                     jefydepenseplanco.dpco_montant_budgetaire,     --DEP_HT,
                     depense_seq.NEXTVAL,                           --DEP_ID,
                                         lignebudgetaire,
                                                      --DEP_LIGNE_BUDGETAIRE,
                                                         lotordre, --DEP_LOT,
                                                                  marordre,
                                                               --DEP_MARCHES,
                     jefydepenseplanco.dpco_ttc_saisie,
                                                     --DEP_MONTANT_DISQUETTE,
                                                       NULL,
               -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                     SUBSTR (tmpdepensepapier.dpp_numero_facture, 1, 199),
                                                                --DEP_NUMERO,
                     jefydepenseplanco.dpco_id,                  --DEP_ORDRE,
                                               NULL,             --DEP_REJET,
                     tmpdepensepapier.rib_ordre,                   --DEP_RIB,
                                                'NON',     --DEP_SUPPRESSION,
                     jefydepenseplanco.dpco_ttc_saisie,            --DEP_TTC,
                       jefydepenseplanco.dpco_ttc_saisie
                     - jefydepenseplanco.dpco_montant_budgetaire, -- DEP_TVA,
                     tmpdepensepapier.exe_ordre,                 --EXE_ORDRE,
                                                fouordre,        --FOU_ORDRE,
                                                         gescode, --GES_CODE,
                                                                 manid,
                                                                    --MAN_ID,
                     jefydepenseplanco.man_id,                   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                                              modordre,
                     jefydepenseplanco.pco_num,                  --PCO_ORDRE,
                                               tmpdepensepapier.utl_ordre,
                                                                  --UTL_ORDRE
                     orgid,                                       --org_ordre
                           tcdordre, ecd_ordre_ema,
               -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                     tmpdepensepapier.dpp_date_facture);
     END LOOP;

     CLOSE depenses;
  END;

  PROCEDURE get_recette_jefy_recette (titid INTEGER)
  IS
     recettepapier          jefy_recette.recette_papier%ROWTYPE;
     recettebudget          jefy_recette.recette_budget%ROWTYPE;
     facturebudget          jefy_recette.facture_budget%ROWTYPE;
     recettectrlplanco      jefy_recette.recette_ctrl_planco%ROWTYPE;
     recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%ROWTYPE;
     maracujatitre          maracuja.titre%ROWTYPE;
     adrnom                 VARCHAR2 (200);
     letyperecette          VARCHAR2 (200);
     titinterne             VARCHAR2 (200);
     lbud                   VARCHAR2 (200);
     tboordre               INTEGER;
     cpt                    INTEGER;

     CURSOR c_recette
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
     OPEN c_recette;

     LOOP
        FETCH c_recette
         INTO recettectrlplanco;

        EXIT WHEN c_recette%NOTFOUND;

        SELECT *
          INTO recettebudget
          FROM jefy_recette.recette_budget
         WHERE rec_id = recettectrlplanco.rec_id;

        SELECT *
          INTO facturebudget
          FROM jefy_recette.facture_budget
         WHERE fac_id = recettebudget.fac_id;

        SELECT *
          INTO recettepapier
          FROM jefy_recette.recette_papier
         WHERE rpp_id = recettebudget.rpp_id;

        SELECT *
          INTO maracujatitre
          FROM maracuja.titre
         WHERE tit_id = titid;

        IF (recettebudget.rec_id_reduction IS NULL)
        THEN
           letyperecette := 'R';
        ELSE
           letyperecette := 'T';
        END IF;

        SELECT COUNT (*)
          INTO cpt
          FROM jefy_recette.pi_dep_rec
         WHERE rec_id = recettectrlplanco.rec_id;

        IF cpt > 0
        THEN
           titinterne := 'O';
        ELSE
           titinterne := 'N';
        END IF;

        SELECT adr_nom
          INTO adrnom
          FROM grhum.v_fournis_grhum
         WHERE fou_ordre = recettepapier.fou_ordre;

        SELECT org_ub || '/' || org_cr || '/' || org_souscr
          INTO lbud
          FROM jefy_admin.organ
         WHERE org_id = facturebudget.org_id;

        SELECT DISTINCT tbo_ordre
                   INTO tboordre
                   FROM maracuja.titre t, maracuja.bordereau b
                  WHERE b.bor_id = t.bor_id AND t.tit_id = titid;

-- 200 bordereau de presntation interne recette
        IF tboordre = 200
        THEN
           tboordre := NULL;
        ELSE
           tboordre := facturebudget.org_id;
        END IF;

        INSERT INTO recette
             VALUES (recettectrlplanco.exe_ordre,                --EXE_ORDRE,
                                                 maracujatitre.ges_code,
                                                                  --GES_CODE,
                     NULL,                                        --MOD_CODE,
                          recettectrlplanco.pco_num,               --PCO_NUM,
                     recettebudget.rec_date_saisie,
                                            --jefytitre.tit_date,-- REC_DATE,
                                                   adrnom,   -- REC_DEBITEUR,
                     recette_seq.NEXTVAL,                          -- REC_ID,
                                         NULL,               -- REC_IMPUTTVA,
                                              NULL,
                                                  -- REC_INTERNE, // TODO ROD
                                                   facturebudget.fac_lib,
                                                              -- REC_LIBELLE,
                     lbud,                           -- REC_LIGNE_BUDGETAIRE,
                          'E',                                -- REC_MONNAIE,
                              recettectrlplanco.rpco_ht_saisie,         --HT,
                     recettectrlplanco.rpco_ttc_saisie,                --TTC,
                     recettectrlplanco.rpco_ttc_saisie,          --DISQUETTE,
                     recettectrlplanco.rpco_tva_saisie,     --   REC_MONTTVA,
                     facturebudget.fac_numero,                  --   REC_NUM,
                                              recettectrlplanco.rpco_id,
                                                              --   REC_ORDRE,
                     recettepapier.rpp_nb_piece,              --   REC_PIECE,
                                                facturebudget.fac_numero,
                                                                --   REC_REF,
                     'VALIDE',                                 --   REC_STAT,
                              'NON',        --    REC_SUPPRESSION,  Modif Rod
                                    letyperecette,           --     REC_TYPE,
                                                  NULL,  --     REC_VIREMENT,
                                                       titid, --      TIT_ID,
                                                             -titid,
                                                           --      TIT_ORDRE,
                     recettebudget.utl_ordre,              --       UTL_ORDRE
                                             facturebudget.org_id,
                                               --       ORG_ORDRE --ajout rod
                     facturebudget.fou_ordre,         --FOU_ORDRE --ajout rod
                                             NULL,                --mod_ordre
                                                  recettepapier.mor_ordre,
                                                                  --mor_ordre
                     recettepapier.rib_ordre, NULL);
     END LOOP;

     CLOSE c_recette;
  END;

-- procedures du brouillard
  PROCEDURE set_mandat_brouillard (manid INTEGER)
  IS
     lemandat                  mandat%ROWTYPE;
     pconum_ctrepartie         mandat.pco_num%TYPE;
     pconum_tva                planco_visa.pco_num_tva%TYPE;
     gescodecompta             mandat.ges_code%TYPE;
     pvicontrepartie_gestion   planco_visa.pvi_contrepartie_gestion%TYPE;
     modcontrepartie_gestion   mode_paiement.mod_contrepartie_gestion%TYPE;
     pconum_185                planco_visa.pco_num_tva%TYPE;
     parvalue                  parametre.par_value%TYPE;
     cpt                       INTEGER;
     tboordre                  type_bordereau.tbo_ordre%TYPE;
     sens                      ecriture_detail.ecd_sens%TYPE;
  BEGIN
     SELECT *
       INTO lemandat
       FROM mandat
      WHERE man_id = manid;

     SELECT DISTINCT tbo_ordre
                INTO tboordre
                FROM bordereau
               WHERE bor_id IN (SELECT bor_id
                                  FROM mandat
                                 WHERE man_id = manid);

--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
     IF lemandat.prest_id IS NULL
     THEN
        -- creation du mandat_brouillard visa DEBIT--
        sens := inverser_sens_orv (tboordre, 'D');

        INSERT INTO mandat_brouillard
             VALUES (NULL,                                      --ECD_ORDRE,
                          lemandat.exe_ordre,                   --EXE_ORDRE,
                                             lemandat.ges_code,  --GES_CODE,
                     ABS (lemandat.man_ht),                   --MAB_MONTANT,
                                           'VISA MANDAT',   --MAB_OPERATION,
                     mandat_brouillard_seq.NEXTVAL,             --MAB_ORDRE,
                                                   sens,         --MAB_SENS,
                                                        manid,     --MAN_ID,
                     lemandat.pco_num                               --PCO_NU
                                     );

        -- credit=ctrepartie
        --debit = ordonnateur
        -- recup des infos du VISA CREDIT --
        SELECT COUNT (*)
          INTO cpt
          FROM planco_visa
         WHERE pco_num_ordonnateur = lemandat.pco_num
           AND exe_ordre = lemandat.exe_ordre;

        IF cpt = 0
        THEN
           raise_application_error (-20001,
                                       'PROBLEM DE CONTRE PARTIE '
                                    || lemandat.pco_num
                                   );
        END IF;

        SELECT pco_num_ctrepartie, pco_num_tva, pvi_contrepartie_gestion
          INTO pconum_ctrepartie, pconum_tva, pvicontrepartie_gestion
          FROM planco_visa
         WHERE pco_num_ordonnateur = lemandat.pco_num
           AND exe_ordre = lemandat.exe_ordre;

        SELECT COUNT (*)
          INTO cpt
          FROM mode_paiement
         WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;

        IF cpt != 0
        THEN
           SELECT pco_num_visa, mod_contrepartie_gestion
             INTO pconum_ctrepartie, modcontrepartie_gestion
             FROM mode_paiement
            WHERE exe_ordre = lemandat.exe_ordre
              AND mod_ordre = lemandat.mod_ordre
              AND pco_num_visa IS NOT NULL;
        END IF;

        -- modif 15/09/2005 compatibilite avec new gestion_exercice
        SELECT c.ges_code, ge.pco_num_185
          INTO gescodecompta, pconum_185
          FROM gestion g, comptabilite c, gestion_exercice ge
         WHERE g.ges_code = lemandat.ges_code
           AND g.com_ordre = c.com_ordre
           AND g.ges_code = ge.ges_code
           AND ge.exe_ordre = lemandat.exe_ordre;

                -- 5/12/2007
                -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
                -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
                -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                      --SELECT par_value   INTO parvalue
        --    FROM PARAMETRE
        --    WHERE par_key ='CONTRE PARTIE VISA'
        --    AND exe_ordre = lemandat.exe_ordre;
        parvalue := pvicontrepartie_gestion;

        IF (modcontrepartie_gestion IS NOT NULL)
        THEN
           parvalue := modcontrepartie_gestion;
        END IF;

        IF parvalue = 'COMPOSANTE'
        THEN
           gescodecompta := lemandat.ges_code;
        END IF;

        IF pconum_185 IS NULL
        THEN
           -- creation du mandat_brouillard visa CREDIT --
           sens := inverser_sens_orv (tboordre, 'C');

           IF sens = 'D'
           THEN
              pconum_ctrepartie := '4632';
           END IF;

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                    --ECD_ORDRE,
                             lemandat.exe_ordre,                 --EXE_ORDRE,
                                                gescodecompta,    --GES_CODE,
                        ABS (lemandat.man_ttc),                --MAB_MONTANT,
                                               'VISA MANDAT',
                                                             --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,           --MAB_ORDRE,
                                                      sens,       --MAB_SENS,
                                                           manid,   --MAN_ID,
                        pconum_ctrepartie                            --PCO_NU
                                         );
        ELSE
           --au SACD --
           sens := inverser_sens_orv (tboordre, 'C');

           IF sens = 'D'
           THEN
              pconum_ctrepartie := '4632';
           END IF;

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                    --ECD_ORDRE,
                             lemandat.exe_ordre,                 --EXE_ORDRE,
                                                lemandat.ges_code,
                                                                  --GES_CODE,
                        ABS (lemandat.man_ttc),                --MAB_MONTANT,
                                               'VISA MANDAT',
                                                             --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,           --MAB_ORDRE,
                                                      sens,       --MAB_SENS,
                                                           manid,   --MAN_ID,
                        pconum_ctrepartie                            --PCO_NU
                                         );
        END IF;

        IF lemandat.man_tva != 0
        THEN
           -- creation du mandat_brouillard visa CREDIT TVA --
           sens := inverser_sens_orv (tboordre, 'D');

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                   --ECD_ORDRE,
                             lemandat.exe_ordre,                --EXE_ORDRE,
                                                lemandat.ges_code,
                                                                 --GES_CODE,
                        ABS (lemandat.man_tva),               --MAB_MONTANT,
                                               'VISA TVA',  --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,          --MAB_ORDRE,
                                                      sens,      --MAB_SENS,
                                                           manid,  --MAN_ID,
                        pconum_tva                                  --PCO_NU
                                  );
        END IF;
     ELSE
        bordereau_abricot.set_mandat_brouillard_intern (manid);
     END IF;
  END;

  PROCEDURE set_mandat_brouillard_intern (manid INTEGER)
  IS
     lemandat            mandat%ROWTYPE;
     leplancomptable     plan_comptable%ROWTYPE;
     pconum_ctrepartie   mandat.pco_num%TYPE;
     pconum_tva          planco_visa.pco_num_tva%TYPE;
     gescodecompta       mandat.ges_code%TYPE;
     ctpgescode          mandat.ges_code%TYPE;
     pconum_185          planco_visa.pco_num_tva%TYPE;
     parvalue            parametre.par_value%TYPE;
     cpt                 INTEGER;
     lepconum            plan_comptable_exer.pco_num%TYPE;
     chap varchar2(2);
  BEGIN
     SELECT *
       INTO lemandat
       FROM mandat
      WHERE man_id = manid;

     -- modif 15/09/2005 compatibilite avec new gestion_exercice
     SELECT c.ges_code, ge.pco_num_185
       INTO gescodecompta, pconum_185
       FROM gestion g, comptabilite c, gestion_exercice ge
      WHERE g.ges_code = lemandat.ges_code
        AND g.com_ordre = c.com_ordre
        AND g.ges_code = ge.ges_code
        AND ge.exe_ordre = lemandat.exe_ordre;

     -- recup des infos du VISA CREDIT --
     SELECT COUNT (*)
       INTO cpt
       FROM planco_visa
      WHERE pco_num_ordonnateur = lemandat.pco_num
        AND exe_ordre = lemandat.exe_ordre;

     IF cpt = 0
     THEN
        raise_application_error (-20001,
                                    'PROBLEM DE CONTRE PARTIE '
                                 || lemandat.pco_num
                                );
     END IF;

     SELECT pco_num_ctrepartie, pco_num_tva
       INTO pconum_ctrepartie, pconum_tva
       FROM planco_visa
      WHERE pco_num_ordonnateur = lemandat.pco_num
        AND exe_ordre = lemandat.exe_ordre;

  -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

     --   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

     --    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--
        -- recup des 2 premiers caracteres du compte
        SELECT SUBSTR (lemandat.pco_num, 1, 2)
          INTO chap
          FROM DUAL;


        IF chap != '18'
        THEN
           lepconum :=
              api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
        ELSE
           raise_application_error (-20001,'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num ||')');
        END IF;

     lepconum :=
            api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);

--    lemandat.pco_num := '18'||lemandat.pco_num;

     -- creation du mandat_brouillard visa DEBIT--
     INSERT INTO mandat_brouillard
          VALUES (NULL,                                          --ECD_ORDRE,
                       lemandat.exe_ordre,                       --EXE_ORDRE,
                                          lemandat.ges_code,      --GES_CODE,
                  ABS (lemandat.man_ht),                       --MAB_MONTANT,
                                        'VISA MANDAT',       --MAB_OPERATION,
                  mandat_brouillard_seq.NEXTVAL,                 --MAB_ORDRE,
                                                'D',              --MAB_SENS,
                                                    manid,          --MAN_ID,
                  '18' || lemandat.pco_num                           --PCO_NU
                                          );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

     --   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

     --   END IF;
     lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');

     -- planco de CREDIT 181
     -- creation du mandat_brouillard visa CREDIT --

     -- si on est sur un sacd, la contrepartie reste sur le sacd
     IF (pconum_185 IS NOT NULL)
     THEN
        ctpgescode := lemandat.ges_code;
     ELSE
        ctpgescode := gescodecompta;
     END IF;

     INSERT INTO mandat_brouillard
          VALUES (NULL,                                          --ECD_ORDRE,
                       lemandat.exe_ordre,                       --EXE_ORDRE,
                                          ctpgescode,             --GES_CODE,
                                                     ABS (lemandat.man_ttc),
                                                               --MAB_MONTANT,
                  'VISA MANDAT',                             --MAB_OPERATION,
                                mandat_brouillard_seq.NEXTVAL,   --MAB_ORDRE,
                                                              'C',
                                                                  --MAB_SENS,
                                                                  manid,
                                                                    --MAN_ID,
                  '181'                                              --PCO_NU
                       );

     IF lemandat.man_tva != 0
     THEN
        -- creation du mandat_brouillard visa CREDIT TVA --
        INSERT INTO mandat_brouillard
             VALUES (NULL,                                      --ECD_ORDRE,
                          lemandat.exe_ordre,                   --EXE_ORDRE,
                                             lemandat.ges_code,  --GES_CODE,
                     ABS (lemandat.man_tva),                  --MAB_MONTANT,
                                            'VISA TVA',     --MAB_OPERATION,
                     mandat_brouillard_seq.NEXTVAL,             --MAB_ORDRE,
                                                   'D',          --MAB_SENS,
                                                       manid,      --MAN_ID,
                     pconum_tva                                     --PCO_NU
                               );
     END IF;
  END;

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--    niv INTEGER;
--BEGIN

  --

  --    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

  --    --
--    INSERT INTO PLAN_COMPTABLE
--         (
--         PCO_BUDGETAIRE,
--         PCO_EMARGEMENT,
--         PCO_LIBELLE,
--         PCO_NATURE,
--         PCO_NIVEAU,
--         PCO_NUM,
--         PCO_SENS_EMARGEMENT,
--         PCO_VALIDITE,
--         PCO_J_EXERCICE,
--         PCO_J_FIN_EXERCICE,
--         PCO_J_BE
--         )
--    VALUES
--     (
--     'N',--PCO_BUDGETAIRE,
--     'O',--PCO_EMARGEMENT,
--     libelle,--PCO_LIBELLE,
--     nature,--PCO_NATURE,
--     niv,--PCO_NIVEAU,
--     pconum,--PCO_NUM,
--     2,--PCO_SENS_EMARGEMENT,
--     'VALIDE',--PCO_VALIDITE,
--     'O',--PCO_J_EXERCICE,
--     'N',--PCO_J_FIN_EXERCICE,
--     'N'--PCO_J_BE
--     );
--END;
  PROCEDURE set_titre_brouillard (titid INTEGER)
  IS
     letitre             titre%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
     lesens              VARCHAR2 (20);
     reduction           INTEGER;
     recid               INTEGER;

     CURSOR c_recettes
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
     SELECT *
       INTO letitre
       FROM titre
      WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
     SELECT MAX (rb.rec_id_reduction)
       INTO reduction
       FROM jefy_recette.recette_budget rb,
            jefy_recette.recette_ctrl_planco rcpo
      WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

-- si dans le cas d une reduction
     IF (reduction IS NOT NULL)
     THEN
        lesens := 'D';
     ELSE
        lesens := 'C';
     END IF;

     IF letitre.prest_id IS NULL
     THEN
        OPEN c_recettes;

        LOOP
           FETCH c_recettes
            INTO recettectrlplanco;

           EXIT WHEN c_recettes%NOTFOUND;

           SELECT MAX (rec_id)
             INTO recid
             FROM recette
            WHERE rec_ordre = recettectrlplanco.rpco_id;

           -- creation du titre_brouillard visa --
           --  RECETTE_CTRL_PLANCO
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          recettectrlplanco.exe_ordre,           --EXE_ORDRE,
                                                      letitre.ges_code,
                                                                  --GES_CODE,
                     recettectrlplanco.pco_num,                     --PCO_NUM
                     ABS (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                                                            'VISA TITRE',
                                                             --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  lesens,         --TIB_SENS,
                                                         titid,     --TIT_ID,
                                                               recid
                FROM jefy_recette.recette_ctrl_planco
               WHERE rpco_id = recettectrlplanco.rpco_id;

           -- recette_ctrl_planco_tva
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          exe_ordre,                             --EXE_ORDRE,
                                    ges_code,                     --GES_CODE,
                                             pco_num,               --PCO_NUM
                     ABS (rpcotva_tva_saisie),                 --TIB_MONTANT,
                                              'VISA TITRE',  --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  lesens,         --TIB_SENS,
                                                         titid,     --TIT_ID,
                                                               recid
                FROM jefy_recette.recette_ctrl_planco_tva
               WHERE rpco_id = recettectrlplanco.rpco_id;

           -- recette_ctrl_planco_ctp
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          recettectrlplanco.exe_ordre,           --EXE_ORDRE,
                                                      ges_code,   --GES_CODE,
                                                               pco_num,
                                                                    --PCO_NUM
                     ABS (rpcoctp_ttc_saisie),                 --TIB_MONTANT,
                                              'VISA TITRE',  --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  inverser_sens (lesens),
                                                                  --TIB_SENS,
                     titid,                                         --TIT_ID,
                           recid
                FROM jefy_recette.recette_ctrl_planco_ctp
               WHERE rpco_id = recettectrlplanco.rpco_id;
        END LOOP;

        CLOSE c_recettes;
     ELSE
        set_titre_brouillard_intern (titid);
     END IF;

     -- suppression des lignes d ecritures a ZERO
     DELETE FROM titre_brouillard
           WHERE tib_montant = 0;
  END;

  PROCEDURE set_titre_brouillard_intern (titid INTEGER)
  IS
     letitre             titre%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
     lesens              VARCHAR2 (20);
     reduction           INTEGER;
     lepconum            maracuja.plan_comptable.pco_num%TYPE;
     libelle             maracuja.plan_comptable.pco_libelle%TYPE;
     chap                VARCHAR2 (2);
     recid               INTEGER;
     gescodecompta       maracuja.titre.ges_code%TYPE;
     ctpgescode          titre.ges_code%TYPE;
     pconum_185          gestion_exercice.pco_num_185%TYPE;

     CURSOR c_recettes
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
     SELECT *
       INTO letitre
       FROM titre
      WHERE tit_id = titid;

     -- modif fred 04/2007
     SELECT c.ges_code, ge.pco_num_185
       INTO gescodecompta, pconum_185
       FROM gestion g, comptabilite c, gestion_exercice ge
      WHERE g.ges_code = letitre.ges_code
        AND g.com_ordre = c.com_ordre
        AND g.ges_code = ge.ges_code
        AND ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     SELECT rb.rec_id_reduction
       INTO reduction
       FROM jefy_recette.recette_budget rb,
            jefy_recette.recette_ctrl_planco rcpo
      WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

     -- si dans le cas d une reduction
     IF (reduction IS NOT NULL)
     THEN
        lesens := 'D';
     ELSE
        lesens := 'C';
     END IF;

     OPEN c_recettes;

     LOOP
        FETCH c_recettes
         INTO recettectrlplanco;

        EXIT WHEN c_recettes%NOTFOUND;

        SELECT MAX (rec_id)
          INTO recid
          FROM recette
         WHERE rec_ordre = recettectrlplanco.rpco_id;

        -- recup des 2 premiers caracteres du compte
        SELECT SUBSTR (recettectrlplanco.pco_num, 1, 2)
          INTO chap
          FROM DUAL;

        IF chap != '18'
        THEN
           lepconum :=
              api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
        ELSE
           raise_application_error (-20001,'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num ||')');
        END IF;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       recettectrlplanco.exe_ordre,              --EXE_ORDRE,
                                                   letitre.ges_code,
                                                                  --GES_CODE,
                  lepconum,                                         --PCO_NUM
                           ABS (recettectrlplanco.rpco_ht_saisie),
                                                               --TIB_MONTANT,
                  'VISA TITRE',                              --TIB_OPERATION,
                               titre_brouillard_seq.NEXTVAL,     --TIB_ORDRE,
                                                            lesens,
                                                                  --TIB_SENS,
                                                                   titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco
            WHERE rpco_id = recettectrlplanco.rpco_id;

        -- recette_ctrl_planco_tva
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       exe_ordre,                                --EXE_ORDRE,
                                 gescodecompta,
                                               -- ges_code,               --GES_CODE,
                                               pco_num,             --PCO_NUM
                  ABS (rpcotva_tva_saisie),                    --TIB_MONTANT,
                                           'VISA TITRE',     --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,                  --TIB_ORDRE,
                                               inverser_sens (lesens),
                                                                  --TIB_SENS,
                                                                      titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco_tva
            WHERE rpco_id = recettectrlplanco.rpco_id;

        -- si on est sur un sacd, la contrepartie reste sur le sacd
        IF (pconum_185 IS NOT NULL)
        THEN
           ctpgescode := letitre.ges_code;
        ELSE
           ctpgescode := gescodecompta;
        END IF;

        -- recette_ctrl_planco_ctp on force le 181
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       recettectrlplanco.exe_ordre,              --EXE_ORDRE,
                                                   ctpgescode,    --GES_CODE,
                                                              '181',
                                                                    --PCO_NUM
                  ABS (rpcoctp_ttc_saisie),                    --TIB_MONTANT,
                                           'VISA TITRE',     --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,                  --TIB_ORDRE,
                                               inverser_sens (lesens),
                                                                  --TIB_SENS,
                                                                      titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco_ctp
            WHERE rpco_id = recettectrlplanco.rpco_id;
     END LOOP;

     CLOSE c_recettes;
  END;

--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--  niv INTEGER;
--  cpt integer;
--BEGIN

  --select count(*) into cpt from PLAN_COMPTABLE
--where pco_num = pconum;

  --if cpt = 0 then
--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

  --    --
--    INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
--        VALUES
--        (
--        'N',--PCO_BUDGETAIRE,
--        'O',--PCO_EMARGEMENT,
--        libelle,--PCO_LIBELLE,
--        nature,--PCO_NATURE,
--        niv,--PCO_NIVEAU,
--        pconum,--PCO_NUM,
--        2,--PCO_SENS_EMARGEMENT,
--        'VALIDE',--PCO_VALIDITE,
--        'O',--PCO_J_EXERCICE,
--        'N',--PCO_J_FIN_EXERCICE,
--        'N'--PCO_J_BE
--        );
--end if;

  --END;

  -- outils
  FUNCTION inverser_sens_orv (tboordre INTEGER, sens VARCHAR)
     RETURN VARCHAR
  IS
     cpt   INTEGER;
  BEGIN
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
     SELECT COUNT (*)
       INTO cpt
       FROM type_bordereau
      WHERE tbo_sous_type = 'REVERSEMENTS' AND tbo_ordre = tboordre;

     IF (cpt != 0)
     THEN
        IF (sens = 'C')
        THEN
           RETURN 'D';
        ELSE
           RETURN 'C';
        END IF;
     END IF;

     RETURN sens;
  END;

  FUNCTION recup_gescode (abrid INTEGER)
     RETURN VARCHAR
  IS
     gescode   bordereau.ges_code%TYPE;
  BEGIN
     SELECT DISTINCT ges_code
                INTO gescode
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN gescode;
  END;

  FUNCTION recup_utlordre (abrid INTEGER)
     RETURN INTEGER
  IS
     utlordre   bordereau.utl_ordre%TYPE;
  BEGIN
     SELECT DISTINCT utl_ordre
                INTO utlordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN utlordre;
  END;

  FUNCTION recup_exeordre (abrid INTEGER)
     RETURN INTEGER
  IS
     exeordre   bordereau.exe_ordre%TYPE;
  BEGIN
     SELECT DISTINCT exe_ordre
                INTO exeordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN exeordre;
  END;

  FUNCTION recup_tboordre (abrid INTEGER)
     RETURN INTEGER
  IS
     tboordre   bordereau.tbo_ordre%TYPE;
  BEGIN
     SELECT DISTINCT tbo_ordre
                INTO tboordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN tboordre;
  END;

  FUNCTION recup_groupby (abrid INTEGER)
     RETURN VARCHAR
  IS
     abrgroupby   abricot_bord_selection.abr_group_by%TYPE;
  BEGIN
     SELECT DISTINCT abr_group_by
                INTO abrgroupby
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN abrgroupby;
  END;

  FUNCTION inverser_sens (sens VARCHAR)
     RETURN VARCHAR
  IS
  BEGIN
     IF sens = 'D'
     THEN
        RETURN 'C';
     ELSE
        RETURN 'D';
     END IF;
  END;

  PROCEDURE numeroter_bordereau (borid INTEGER)
  IS
     cpt_mandat   INTEGER;
     cpt_titre    INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt_mandat
       FROM mandat
      WHERE bor_id = borid;

     SELECT COUNT (*)
       INTO cpt_titre
       FROM titre
      WHERE bor_id = borid;

     IF cpt_mandat + cpt_titre = 0
     THEN
        raise_application_error (-20001, 'Bordereau  vide');
     ELSE
        numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
        numerotationobject.numeroter_mandat (borid);
-- boucle titre
        numerotationobject.numeroter_titre (borid);
     END IF;
  END;

  FUNCTION traiter_orgid (orgid INTEGER, exeordre INTEGER)
     RETURN INTEGER
  IS
     topordre     INTEGER;
     cpt          INTEGER;
     orilibelle   origine.ori_libelle%TYPE;
     convordre    INTEGER;
  BEGIN
     IF orgid IS NULL
     THEN
        RETURN NULL;
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM accords.convention_limitative
      WHERE org_id = orgid AND exe_ordre = exeordre;

     IF cpt > 0
     THEN
        -- recup du type_origine CONVENTION--
        SELECT top_ordre
          INTO topordre
          FROM type_operation
         WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

        SELECT DISTINCT con_ordre
                   INTO convordre
                   FROM accords.convention_limitative
                  WHERE org_id = orgid AND exe_ordre = exeordre;

        SELECT (exe_ordre || '-' || LPAD (con_index, 5, '0') || ' '
                || con_objet
               )
          INTO orilibelle
          FROM accords.contrat
         WHERE con_ordre = convordre;
     ELSE
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_admin.organ
         WHERE org_id = orgid AND org_lucrativite = 1;

        IF cpt = 1
        THEN
           -- recup du type_origine OPERATION LUCRATIVE --
           SELECT top_ordre
             INTO topordre
             FROM type_operation
            WHERE top_libelle = 'OPERATION LUCRATIVE';

           --le libelle utilisateur pour le suivie en compta --
           SELECT org_ub || '-' || org_cr || '-' || org_souscr
             INTO orilibelle
             FROM jefy_admin.organ
            WHERE org_id = orgid;
        ELSE
           RETURN NULL;
        END IF;
     END IF;

-- l origine est t elle deja  suivie --
     SELECT COUNT (*)
       INTO cpt
       FROM origine
      WHERE ori_key_name = 'ORG_ID'
        AND ori_entite = 'JEFY_ADMIN.ORGAN'
        AND ori_key_entite = orgid;

     IF cpt >= 1
     THEN
        SELECT ori_ordre
          INTO cpt
          FROM origine
         WHERE ori_key_name = 'ORG_ID'
           AND ori_entite = 'JEFY_ADMIN.ORGAN'
           AND ori_key_entite = orgid
           AND ROWNUM = 1;
     ELSE
        SELECT origine_seq.NEXTVAL
          INTO cpt
          FROM DUAL;

        INSERT INTO origine
                    (ori_entite, ori_key_name, ori_libelle, ori_ordre,
                     ori_key_entite, top_ordre
                    )
             VALUES ('JEFY_ADMIN', 'ORG_ID', orilibelle, cpt,
                     orgid, topordre
                    );
     END IF;

     RETURN cpt;
  END;

  PROCEDURE controle_bordereau (borid INTEGER)
  IS
     ttc             maracuja.titre.tit_ttc%TYPE;
     detailttc       maracuja.titre.tit_ttc%TYPE;
     ordottc         maracuja.titre.tit_ttc%TYPE;
     debit           maracuja.titre.tit_ttc%TYPE;
     credit          maracuja.titre.tit_ttc%TYPE;
     cpt             INTEGER;
     MESSAGE         VARCHAR2 (50);
     messagedetail   VARCHAR2 (50);
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM maracuja.titre
      WHERE bor_id = borid;

     IF cpt = 0
     THEN
-- somme des maracuja.titre
        SELECT SUM (man_ttc)
          INTO ttc
          FROM maracuja.mandat
         WHERE bor_id = borid;

--somme des maracuja.recette
        SELECT SUM (d.dep_ttc)
          INTO detailttc
          FROM maracuja.mandat m, maracuja.depense d
         WHERE m.man_id = d.man_id AND m.bor_id = borid;

-- la somme des credits
        SELECT SUM (mab_montant)
          INTO credit
          FROM maracuja.mandat m, maracuja.mandat_brouillard mb
         WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'C';

-- la somme des debits
        SELECT SUM (mab_montant)
          INTO debit
          FROM maracuja.mandat m, maracuja.mandat_brouillard mb
         WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'D';

-- somme des jefy.recette
        SELECT SUM (d.dpco_ttc_saisie)
          INTO ordottc
          FROM maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         WHERE m.man_id = d.man_id AND m.bor_id = borid;

        MESSAGE := ' mandats ';
        messagedetail := ' depenses ';
     ELSE
-- somme des maracuja.titre
        SELECT SUM (tit_ttc)
          INTO ttc
          FROM maracuja.titre
         WHERE bor_id = borid;

--somme des maracuja.recette
        SELECT SUM (r.rec_monttva + r.rec_mont)
          INTO detailttc
          FROM maracuja.titre t, maracuja.recette r
         WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

-- la somme des credits
        SELECT SUM (tib_montant)
          INTO credit
          FROM maracuja.titre t, maracuja.titre_brouillard tb
         WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'C';

-- la somme des debits
        SELECT SUM (tib_montant)
          INTO debit
          FROM maracuja.titre t, maracuja.titre_brouillard tb
         WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'D';

-- somme des jefy.recette
        SELECT SUM (r.rpco_ttc_saisie)
          INTO ordottc
          FROM maracuja.titre t, jefy_recette.recette_ctrl_planco r
         WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

        MESSAGE := ' titres ';
        messagedetail := ' recettes ';
     END IF;

-- la somme des credits = sommes des debits
     IF (NVL (debit, 0) != NVL (credit, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' :  debit <> credit : '
                                 || debit
                                 || ' '
                                 || credit
                                );
     END IF;

-- la somme des credits = sommes des debits
     IF (NVL (debit, 0) != NVL (credit, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' :  ecriture <> budgetaire : '
                                 || debit
                                 || ' '
                                 || ttc
                                );
     END IF;

-- somme des maracuja.titre = somme des maracuja.recette
     IF (NVL (ttc, 0) != NVL (detailttc, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' : montant des '
                                 || MESSAGE
                                 || ' <>  du montant des '
                                 || messagedetail
                                 || ' :'
                                 || ttc
                                 || ' '
                                 || detailttc
                                );
     END IF;

-- somme des jefy.recette = somme des maracuja.recette
     IF (NVL (ttc, 0) != NVL (ordottc, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' : montant des '
                                 || MESSAGE
                                 || ' <>  du montant ordonnateur des '
                                 || messagedetail
                                 || ' :'
                                 || ttc
                                 || ' '
                                 || ordottc
                                );
     END IF;

     bordereau_abricot.ctrl_date_exercice (borid);
  END;


   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer) is
       flag integer;
       nbDep integer;
       nbRec integer;
             manId mandat.man_id%type;
       titId titre.tit_id%type;
       tmpMandat mandat%rowtype;
       tmpTitre titre%rowtype;
       tmpPrest integer;
       montantDep mandat.man_ht%type;
       montantRec titre.tit_ht%type;
             cursor prests is
           select distinct prest_id from (
               select prest_id from mandat where bor_id = borIdDep
               union
               select prest_id from titre where bor_id = borIdRec
          );
                                    begin
       if (borIdDep is null) then  raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.'); end if;
       if (borIdRec is null) then  raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.'); end if;
         -- verifier qu'il s'agit bien de bordereaux de PI
       select count(*) into flag from bordereau where tbo_ordre=201 and bor_id=borIdDep;
       if (flag = 0) then  raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.'); end if;
         select count(*) into flag from bordereau where tbo_ordre=200 and bor_id=borIdRec;
       if (flag = 0) then  raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.'); end if;
         -- comparer le nombre de titres et de mandats
       select count(*) into nbDep from mandat where bor_id=borIdDep;
       select count(*) into nbRec from titre where bor_id=borIdRec;
             if (nbDep = 0) then
           raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
       end if;
                   if (nbDep <> nbRec) then
           raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbDep || ' / Titres : '|| nbRec);
       end if;
                  OPEN prests;

     LOOP
        FETCH prests
         INTO tmpPrest;
        EXIT WHEN prests%NOTFOUND;
               select count(*) into nbDep from mandat where prest_id=tmpPrest and bor_id=borIdDep;
        select count(*) into nbRec from titre where prest_id=tmpPrest  and bor_id=borIdRec;                if (nbDep <> nbRec) then  raise_application_error (-20001, 'Incoherence : Nombre de titres ('|| nbRec ||') different du nombre de mandats ('|| nbDep ||') ('||'prest_id='||tmpPrest||')'); end if;
               select sum(man_ht) into montantDep from mandat where prest_id=tmpPrest and bor_id=borIdDep;
        select sum(tit_ht) into montantRec from titre where prest_id=tmpPrest  and bor_id=borIdRec;                if (montantDep <> montantRec) then  raise_application_error (-20001, 'Incoherence : Montant des titres ('|| montantRec ||') different du montant des mandats  ('|| montantDep ||') ('||'prest_id='||tmpPrest||')'); end if;

     END LOOP;
     CLOSE prests;


     select sum(man_ht) into montantDep from mandat where bor_id=borIdDep;      select sum(tit_ht) into montantRec from titre where bor_id=borIdDep;
       if (montantDep <> montantRec) then  raise_application_error (-20001, 'Incoherence : Montant total des titres ('|| montantRec ||') different du montant total des mandats  ('|| montantDep ||')'); end if;

     end;





  PROCEDURE get_recette_prelevements (titid INTEGER)
  IS
     cpt                      INTEGER;
     facture_titre_data       prestation.facture_titre%ROWTYPE;
--     client_data              prelev.client%ROWTYPE;
     oriordre                 INTEGER;
     modordre                 INTEGER;
     recid                    INTEGER;
     echeid                   INTEGER;
     echeancier_data          jefy_echeancier.echeancier%ROWTYPE;
     echeancier_prelev_data   jefy_echeancier.echeancier_prelev%ROWTYPE;
     facture_data             jefy_recette.facture_budget%ROWTYPE;
     personne_data            grhum.v_personne%ROWTYPE;
     premieredate             DATE;
  BEGIN
-- verifier s il existe un echancier pour ce titre
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.recette_ctrl_planco pco,
            jefy_recette.recette r,
            jefy_recette.facture f
      WHERE pco.tit_id = titid
        AND pco.rec_id = r.rec_id
        AND r.fac_id = f.fac_id
        AND eche_id IS NOT NULL
        AND r.rec_id_reduction IS NULL;

     IF (cpt != 1)
     THEN
        RETURN;
     END IF;

-- recup du eche_id / ech_id
     SELECT eche_id
       INTO echeid
       FROM jefy_recette.recette_ctrl_planco pco,
            jefy_recette.recette r,
            jefy_recette.facture f
      WHERE pco.tit_id = titid
        AND pco.rec_id = r.rec_id
        AND r.fac_id = f.fac_id
        AND eche_id IS NOT NULL
        AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
     SELECT *
       INTO echeancier_data
       FROM jefy_echeancier.echeancier
      WHERE ech_id = echeid;

     SELECT *
       INTO echeancier_prelev_data
       FROM jefy_echeancier.echeancier_prelev
      WHERE ech_id = echeid;

     SELECT *
       INTO facture_data
       FROM jefy_recette.facture_budget
      WHERE eche_id = echeid;

     SELECT *
       INTO personne_data
       FROM grhum.v_personne
      WHERE pers_id = facture_data.pers_id;

     SELECT echd_date_prevue
       INTO premieredate
       FROM jefy_echeancier.echeancier_detail
      WHERE echd_numero = 1 AND ech_id = echeid;

     SELECT rec_id
       INTO recid
       FROM recette
      WHERE tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

     -- recup ??
     oriordre :=
        gestionorigine.traiter_orgid (facture_data.org_id,
                                      facture_data.exe_ordre
                                     );

     INSERT INTO maracuja.echeancier
                 (eche_autoris_signee, fou_ordre_client, con_ordre,
                  eche_date_1ere_echeance, eche_date_creation,
                  eche_date_modif, eche_echeancier_ordre,
                  eche_etat_prelevement, ft_ordre, eche_libelle,
                  eche_montant,
                  eche_montant_en_lettres,
                  eche_nombre_echeances, eche_numero_index,
                  org_ordre, prest_ordre, eche_prise_en_charge,
                  eche_ref_facture_externe, eche_supprime, exe_ordre,
                  tit_id, rec_id, tit_ordre, ori_ordre, pers_id,
                  org_id,
                  pers_description
                 )
          VALUES ('O',                                  --ECHE_AUTORIS_SIGNEE
                      facture_data.fou_ordre,              --FOU_ORDRE_CLIENT
                                             NULL,
                                     --echancier_data.CON_ORDRE  ,--CON_ORDRE
                  premieredate,
              --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                               SYSDATE,
                        --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                  SYSDATE,    --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                          echeancier_data.ech_id,
                  --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                  'V',
                  --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                      facture_data.fac_id,
                                       --echancier_data.FT_ORDRE  ,--FT_ORDRE
                                          echeancier_data.ech_libelle,
                                      --echancier_data.LIBELLE,--ECHE_LIBELLE
                  echeancier_data.ech_montant,                 --ECHE_MONTANT
                  echeancier_data.ech_montant_lettres,
                                                    --ECHE_MONTANT_EN_LETTRES
                  echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                                                   echeancier_data.ech_id,
                         --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                  facture_data.org_id,
                                    --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                                      NULL,
                                --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                                           'O',        --ECHE_PRISE_EN_CHARGE
                  facture_data.fac_lib,
            --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                                       'N',                   --ECHE_SUPPRIME
                                           facture_data.exe_ordre,
                                                                  --EXE_ORDRE
                  titid, recid,                                     --REC_ID,
                               -titid, oriordre,                 --ORI_ORDRE,
                                                personne_data.pers_id,
                                            --CLIENT_data.pers_id  ,--PERS_ID
                  facture_data.org_id,          --orgid a faire plus tard....
                  personne_data.pers_libelle           --    PERS_DESCRIPTION
                 );

     INSERT INTO maracuja.prelevement
                 (eche_echeancier_ordre, reco_ordre, fou_ordre,
                  prel_commentaire, prel_date_modif, prel_date_prelevement,
                  prel_prelev_date_saisie, prel_prelev_etat,
                  prel_numero_index, prel_prelev_montant, prel_prelev_ordre,
                  rib_ordre, prel_etat_maracuja)
        SELECT ech_id,                                --ECHE_ECHEANCIER_ORDRE
                      NULL,                                 --PREL_FICP_ORDRE
                           facture_data.fou_ordre,                --FOU_ORDRE
                                                  echd_commentaire,
                                                           --PREL_COMMENTAIRE
                                                                   SYSDATE,
                                               --DATE_MODIF,--PREL_DATE_MODIF
               echd_date_prevue,                      --PREL_DATE_PRELEVEMENT
                                SYSDATE,         --,--PREL_PRELEV_DATE_SAISIE
                                        'ATTENTE',         --PREL_PRELEV_ETAT
                                                  echd_numero,
                                                          --PREL_NUMERO_INDEX
                                                              echd_montant,
                                                        --PREL_PRELEV_MONTANT
               echd_id,                                   --PREL_PRELEV_ORDRE
                       echeancier_prelev_data.rib_ordre_debiteur,
                             --RIB_ORDRE
               'ATTENTE'                                 --PREL_ETAT_MARACUJA
          FROM jefy_echeancier.echeancier_detail
         WHERE ech_id = echeancier_data.ech_id;
  END;

  PROCEDURE ctrl_date_exercice (borid INTEGER)
  IS
     exeordre   INTEGER;
     annee      INTEGER;
  BEGIN
     SELECT TO_CHAR (bor_date_creation, 'YYYY'), exe_ordre
       INTO annee, exeordre
       FROM bordereau
      WHERE bor_id = borid AND exe_ordre >= 2007;

     IF exeordre <> annee
     THEN
        UPDATE bordereau
           SET bor_date_creation =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;

        UPDATE mandat
           SET man_date_remise =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;

        UPDATE titre
           SET tit_date_remise =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;
     END IF;
  END;
  
  
  
  
-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.  
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE
   IS

   current_mandat mandat%ROWTYPE;

   pconumsacd gestion_exercice.pco_num_185%TYPE;    

   visa_mode_paiement planco_visa.pvi_contrepartie_gestion%TYPE;    
   visa_planco planco_visa.pvi_contrepartie_gestion%TYPE;    

   code_agence_comptable comptabilite.ges_code%TYPE;
      
   BEGIN

    select ges_code into code_agence_comptable from comptabilite;

    select * into current_mandat from mandat where man_id = manid;
    
    -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
    select pco_num_185 into pconumsacd from gestion_exercice where exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

    if (pconumsacd is not null)
    then

        return current_mandat.ges_code;

    else    -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
    
        -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
        select mod_contrepartie_gestion into visa_mode_paiement from mode_paiement where mod_ordre = current_mandat.mod_ordre;
        if (visa_mode_paiement is not null)
        then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE')
            then

                return code_agence_comptable;

            else    -- COMPOSANTE

                return current_mandat.ges_code;

            end if;
        
        else    -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
        
            select pvi_contrepartie_gestion into visa_planco from planco_visa where pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') 
            then
        
                return code_agence_comptable;

            else    -- COMPOSANTE

                return current_mandat.ges_code;

            end if;
                
        end if;

    end if;
   
   END;
     
END;
/

