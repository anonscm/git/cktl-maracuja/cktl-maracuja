-- Attention, ce fichier est encodé en UTF-8.
-- Exécutez-le dans un environnement UTF-8

whenever sqlerror exit sql.sqlcode ;

grant execute on comptefi.SOLDE_COMPTE_EXT to maracuja;
grant execute on comptefi.SOLDE_COMPTE to maracuja;

grant insert, update, delete on comptefi.bilan_actif to maracuja;
grant insert, update, delete on comptefi.bilan_passif to maracuja;



CREATE OR REPLACE FORCE VIEW comptefi.v_section (section_type,
                                                section,
                                                 section_lib,
                                                 ordre_presentation
                                                )
AS
   SELECT 'D','1', 'Charges de fonctionnement (hors personnel)', 1
     FROM DUAL
   UNION ALL
   SELECT 'D','2', 'Dépenses d''investissement', 3
     FROM DUAL
   UNION ALL
   SELECT 'D','3', 'Charges de personnel', 2
     FROM DUAL
   UNION ALL
   SELECT 'R','1', 'Section 1', 1
     FROM DUAL
   UNION ALL
   SELECT 'R','2', 'Section 2', 2
     FROM DUAL;     
/     

CREATE OR REPLACE FORCE VIEW comptefi.v_planco (pco_num,
                                                pco_num_bdn,
                                                section,
                                                section_lib,
                                                ordre_presentation
                                               )
AS
   SELECT pco_num, SUBSTR (pco_num, 1, 2), s.section, s.section_lib,
          s.ordre_presentation
     FROM maracuja.plan_comptable, comptefi.v_section s
    WHERE (    (pco_num LIKE '6%' OR pco_num LIKE '7%')
           AND pco_num NOT LIKE '64%'
           AND pco_num NOT LIKE '631%'
           AND pco_num NOT LIKE '632%'
           AND pco_num NOT LIKE '633%'
          )
      AND s.section = 1
      AND s.section_type = 'D'
   UNION ALL
   SELECT pco_num, SUBSTR (pco_num, 1, 3), s.section, s.section_lib,
          s.ordre_presentation
     FROM maracuja.plan_comptable, comptefi.v_section s
    WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%')
      AND s.section = 2
      AND s.section_type = 'D'
   UNION ALL
   SELECT pco_num, SUBSTR (pco_num, 1, 2), s.section, s.section_lib,
          s.ordre_presentation
     FROM maracuja.plan_comptable, comptefi.v_section s
    WHERE (   pco_num LIKE '64%'
           OR pco_num LIKE '631%'
           OR pco_num LIKE '632%'
           OR pco_num LIKE '633%'
          )
      AND s.section = 3
      AND s.section_type = 'D';
/       

    

CREATE OR REPLACE FORCE VIEW comptefi.v_budnat2 (exe_ordre,
                                                 ges_code,
                                                 pco_num,
                                                 bdn_quoi,
                                                 bdn_section,
                                                 bdn_numero,
                                                 date_co,
                                                 co
                                                )
AS
   SELECT   bsn.exe_ordre, o.org_ub, SUBSTR (bsn.pco_num, 1, 2),
            SUBSTR (tcb.tcd_type, 0, 1), tcb.tcd_sect, bsn.bdsa_id,
            bdsa_date_validation, SUM (bdsn_saisi)
       FROM jefy_budget.budget_saisie_nature bsn,
            jefy_budget.budget_masque_nature bmn,
            jefy_budget.v_type_credit_budget tcb,
            jefy_budget.budget_saisie bs,
            maracuja.v_organ_exer o
      WHERE bsn.org_id = o.org_id
        AND bsn.exe_ordre = o.exe_ordre
        AND bsn.bdsa_id = bs.bdsa_id
        AND bsn.exe_ordre = bs.exe_ordre
        AND bsn.pco_num = bmn.pco_num
        AND bsn.tcd_ordre = tcb.tcd_ordre
        AND bmn.tcd_ordre = tcb.tcd_ordre
        AND tcb.tcd_sect = 1
        AND bdsa_date_validation IS NOT NULL
   GROUP BY bsn.exe_ordre,
            o.org_ub,
            SUBSTR (bsn.pco_num, 1, 2),
            SUBSTR (tcb.tcd_type, 0, 1),
            tcb.tcd_sect,
            bsn.bdsa_id,
            bdsa_date_validation
   UNION ALL
   SELECT   bsn.exe_ordre, o.org_ub, SUBSTR (bsn.pco_num, 1, 3),
            SUBSTR (tcb.tcd_type, 0, 1), tcb.tcd_sect, bsn.bdsa_id,
            bdsa_date_validation, SUM (bdsn_saisi)
       FROM jefy_budget.budget_saisie_nature bsn,
            jefy_budget.budget_masque_nature bmn,
            jefy_budget.v_type_credit_budget tcb,
            jefy_budget.budget_saisie bs,
            maracuja.v_organ_exer o
      WHERE bsn.org_id = o.org_id
        AND bsn.exe_ordre = o.exe_ordre
        AND bsn.bdsa_id = bs.bdsa_id
        AND bsn.exe_ordre = bs.exe_ordre
        AND bsn.pco_num = bmn.pco_num
        AND bsn.tcd_ordre = tcb.tcd_ordre
        AND bmn.tcd_ordre = tcb.tcd_ordre
        AND tcb.tcd_sect = 2
        AND bdsa_date_validation IS NOT NULL
   GROUP BY bsn.exe_ordre,
            o.org_ub,
            SUBSTR (bsn.pco_num, 1, 3),
            SUBSTR (tcb.tcd_type, 0, 1),
            tcb.tcd_sect,
            bsn.bdsa_id,
            bdsa_date_validation
   UNION ALL
   SELECT   bsn.exe_ordre, o.org_ub, SUBSTR (bsn.pco_num, 1, 2),
            SUBSTR (tcb.tcd_type, 0, 1), tcb.tcd_sect, bsn.bdsa_id,
            bdsa_date_validation, SUM (bdsn_saisi)
       FROM jefy_budget.budget_saisie_nature bsn,
            jefy_budget.budget_masque_nature bmn,
            jefy_budget.v_type_credit_budget tcb,
            jefy_budget.budget_saisie bs,
            maracuja.v_organ_exer o
      WHERE bsn.org_id = o.org_id
        AND bsn.exe_ordre = o.exe_ordre
        AND bsn.bdsa_id = bs.bdsa_id
        AND bsn.exe_ordre = bs.exe_ordre
        AND bsn.pco_num = bmn.pco_num
        AND bsn.tcd_ordre = tcb.tcd_ordre
        AND bmn.tcd_ordre = tcb.tcd_ordre
        AND tcb.tcd_sect = 3
        AND bdsa_date_validation IS NOT NULL
   GROUP BY bsn.exe_ordre,
            o.org_ub,
            SUBSTR (bsn.pco_num, 1, 2),
            SUBSTR (tcb.tcd_type, 0, 1),
            tcb.tcd_sect,
            bsn.bdsa_id,
            bdsa_date_validation
   UNION ALL
   SELECT exe_exercice, ges_code, b.pco_num, bdn_quoi, p.section, bdn_numero,
          date_co, co
     FROM comptefi.jefy_old_budnat b, comptefi.v_planco p
    WHERE b.pco_num = p.pco_num;
/



CREATE OR REPLACE FORCE VIEW comptefi.v_dvlop_dep_liste (exe_ordre,
                                                         ges_code,
                                                         section,
                                                         pco_num_bdn,
                                                         pco_num,
                                                         mandats,
                                                         reversements,
                                                         cr_ouverts,
                                                         dep_date,
                                                         section_lib,
                                                         ordre_presentation
                                                        )
AS
   SELECT   m.exe_ordre, m.ges_code, p.section, p.pco_num_bdn, m.pco_num,
            SUM (man_ht), 0, 0, e.date_saisie, p.section_lib,
            p.ordre_presentation
       FROM maracuja.mandat m,
            maracuja.bordereau b,
            comptefi.v_planco p,
            (SELECT   m.man_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.mandat_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.mandat m
                WHERE m.man_id = mde.man_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.man_id) e
      WHERE m.pco_num = p.pco_num
        AND m.bor_id = b.bor_id
        AND m.man_id = e.man_id
        AND tbo_ordre <> 8
        AND tbo_ordre <> 21
        AND tbo_ordre <> 18
        AND tbo_ordre <> 16
        AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
   GROUP BY m.exe_ordre,
            m.ges_code,
            p.section,
            p.pco_num_bdn,
            m.pco_num,
            e.date_saisie,
            p.section_lib,
            p.ordre_presentation
   UNION ALL
-- ordre de reversement avant 2007
   SELECT   t.exe_ordre, t.ges_code, p.section, p.pco_num_bdn, t.pco_num, 0,
            SUM (tit_ht), 0, e.date_saisie, p.section_lib,
            p.ordre_presentation
       FROM maracuja.titre t,
            maracuja.bordereau b,
            comptefi.v_planco p,
            (SELECT   m.tit_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.titre_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.titre m
                WHERE m.tit_id = mde.tit_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.tit_id) e
      WHERE t.pco_num = p.pco_num
        AND t.bor_id = b.bor_id
        AND tbo_ordre = 8
        AND t.tit_etat = 'VISE'
        AND t.tit_id = e.tit_id
   GROUP BY t.exe_ordre,
            t.ges_code,
            p.section,
            p.pco_num_bdn,
            t.pco_num,
            e.date_saisie,
            p.section_lib,
            p.ordre_presentation
   UNION ALL
-- ordre de reversement à partir de 2007
   SELECT   m.exe_ordre, m.ges_code, p.section, p.pco_num_bdn, m.pco_num, 0,
            -SUM (man_ht), 0, e.date_saisie, p.section_lib,
            p.ordre_presentation
       FROM maracuja.mandat m,
            maracuja.bordereau b,
            comptefi.v_planco p,
            (SELECT   m.man_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.mandat_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.mandat m
                WHERE m.man_id = mde.man_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.man_id) e
      WHERE m.pco_num = p.pco_num
        AND m.bor_id = b.bor_id
        AND m.man_id = e.man_id
        AND (tbo_ordre = 8 OR tbo_ordre = 18 OR tbo_ordre = 21)
        AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
   GROUP BY m.exe_ordre,
            m.ges_code,
            p.section,
            p.pco_num_bdn,
            m.pco_num,
            e.date_saisie,
            p.section_lib,
            p.ordre_presentation
   UNION ALL
-- Crédits ouverts
   SELECT exe_ordre, ges_code, bdn_section section, pco_num, pco_num, 0, 0,
          co, date_co, s.section_lib, s.ordre_presentation
     FROM comptefi.v_budnat2, comptefi.v_section s
    WHERE co <> 0
      AND bdn_quoi = 'D'
      AND bdn_section = s.section
      AND s.section_type = 'D';
/

CREATE OR REPLACE FORCE VIEW comptefi.v_dvlop_dep (exe_ordre,
                                                   ges_code,
                                                   section,
                                                   pco_num_bdn,
                                                   pco_num,
                                                   mandats,
                                                   reversements,
                                                   montant_net,
                                                   cr_ouverts,
                                                   non_empl,
                                                   dep_date,
                                                   section_lib,
                                                   ordre_presentation
                                                  )
AS
   SELECT   exe_ordre, ges_code, section, pco_num_bdn, pco_num, SUM (mandats),
            SUM (reversements), SUM (mandats) - SUM (reversements),
            SUM (cr_ouverts),
            SUM (cr_ouverts) - (SUM (mandats) - SUM (reversements)),
            TRUNC (dep_date), section_lib, ordre_presentation
       FROM v_dvlop_dep_liste
   GROUP BY exe_ordre,
            ges_code,
            section,
            pco_num_bdn,
            pco_num,
            TRUNC (dep_date),
            section_lib,
            ordre_presentation;


GRANT SELECT ON COMPTEFI.V_DVLOP_DEP TO EPN;

GRANT SELECT ON COMPTEFI.V_DVLOP_DEP TO MARACUJA;



CREATE OR REPLACE PROCEDURE COMPTEFI.Prepare_Bilan (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  totalbrut NUMBER(12,2);
  totalamort NUMBER(12,2);
  totalnet NUMBER(12,2);
  totalnetant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);

  -- version du 12/03/2006
  -- REMPLACE PAR MARACUJA.API_BILAN_PREPARE_EXERCICE POUR LES EXERCICES >=2010

BEGIN

--*************** CREATION TABLE ACTIF *********************************
IF sacd = 'O' THEN
    DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- ACTIF IMMOBILISE ----
lib1:= 'ACTIF IMMOBILISE';
---- Immobilisations Incorporelles ---
lib2:= 'IMMOBILISATIONS INCORPORELLES';

-- Compte 201 ---
    totalbrut := Solde_Compte(exeordre, '201%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2801%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2831%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '201%','D', gescode, sacd)
        -(Solde_Compte(exeordre-1, '2801%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2831%', 'C', gescode, sacd));
    lib := 'Frais d''établissement';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 203 ---
    totalbrut := Solde_Compte(exeordre, '203%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2803%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2833%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '203%','D', gescode, sacd)
        -(Solde_Compte(exeordre-1, '2803%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2833%', 'C', gescode, sacd));
    lib := 'Frais de recherche et de développement';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 205 ---
    totalbrut := Solde_Compte(exeordre, '205%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2805%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2835%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2905%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '205%','D', gescode, sacd)
        - (Solde_Compte(exeordre-1, '2805%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2835%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2905%', 'C', gescode, sacd));
    lib := 'Concessions et droits similaires';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 206 ---
    totalbrut := Solde_Compte(exeordre, '206%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2906%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '206%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '2906%', 'C', gescode, sacd);
    lib := 'Droit au bail';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 208 ---
    totalbrut := Solde_Compte(exeordre, '208%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2808%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2908%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2838%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '208%','D', gescode, sacd)
        - (Solde_Compte(exeordre-1, '2808%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2908%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2838%', 'C', gescode, sacd));
    lib := 'Autres immobilisations incorporelles';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 232 ---
    totalbrut := Solde_Compte(exeordre, '232%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2932%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '232%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '2932%', 'C', gescode, sacd);
    lib := 'Immobilisations incorporelles en cours';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 237 ---
    totalbrut := Solde_Compte(exeordre, '237%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '237%','D', gescode, sacd);
    lib := 'Avances et acomptes versés sur immobilisations incorporelles';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);



---- Immobilisations Corporelles ---
lib2:= 'IMMOBILISATIONS CORPORELLES';

-- Compte 211 et 212 ---
    totalbrut := Solde_Compte(exeordre, '211%','D', gescode, sacd)
        +Solde_Compte(exeordre, '212%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2812%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2911%', 'C',     gescode, sacd)
        +Solde_Compte(exeordre, '2842%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '211%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '212%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2812%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2911%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2842%', 'C', gescode, sacd));
    lib := 'Terrains, agencements et aménagements de terrain';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 213 et 214 ---
    totalbrut := Solde_Compte(exeordre, '213%','D', gescode, sacd)
        +Solde_Compte(exeordre, '214%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2813%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2814%', 'C',     gescode, sacd)
        +Solde_Compte(exeordre, '2843%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2844%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '213%','D', gescode, sacd)
        +Solde_Compte(exeordre-1, '214%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2813%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2814%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2843%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2844%', 'C', gescode, sacd));
    lib := 'Constructions et constructions sur sol d''autrui';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 215 ---
    totalbrut := Solde_Compte(exeordre, '215%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2815%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2845%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '215%','D', gescode, sacd)
        -(Solde_Compte(exeordre-1, '2815%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2845%', 'C', gescode, sacd));
    lib := 'Installations techniques, matériels et outillage';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 216 ---
    totalbrut := Solde_Compte(exeordre, '216%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2816%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2846%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '216%','D', gescode, sacd)
        -(Solde_Compte(exeordre-1, '2816%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2846%', 'C', gescode, sacd));
    lib := 'Collections';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 218 ---
    totalbrut := Solde_Compte(exeordre, '218%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2818%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2848%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '218%','D', gescode, sacd)
        -(Solde_Compte(exeordre-1, '2818%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2848%', 'C', gescode, sacd));
    lib := 'Autres immobilisations corporelles';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 231 ---
    totalbrut := Solde_Compte(exeordre, '231%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2931%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '231%','D', gescode, sacd)
        -Solde_Compte(exeordre-1, '2931%', 'C', gescode, sacd);
    lib := 'Immobilisations corporelles en cours';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 238 ---
    totalbrut := Solde_Compte(exeordre, '238%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '238%','D', gescode, sacd);
    lib := 'Avances et acomptes versés sur commandes d''immobilisation corporelles';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Immobilisations Financières ---
lib2:= 'IMMOBILISATIONS FINANCIERES';

-- Compte 261 et 266 ---
    totalbrut := Solde_Compte(exeordre, '261%','D', gescode, sacd)
        +Solde_Compte(exeordre, '266%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2961%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2966%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '261%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '266%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2961%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2966%', 'C', gescode, sacd));
    lib := 'Participations';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 267 et 268 ---
    totalbrut := Solde_Compte(exeordre, '267%','D', gescode, sacd)
        +Solde_Compte(exeordre, '268%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2967%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2968%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '267%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '268%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2967%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2968%', 'C', gescode, sacd));
    lib := 'Créances rattachées à des participations';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 271 et 272 ---
    totalbrut := Solde_Compte(exeordre, '271%','D', gescode, sacd)
        +Solde_Compte(exeordre, '272%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2971%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '2972%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '271%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '272%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2971%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2972%', 'C', gescode, sacd));
    lib := 'Autres titres immobilisés';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 274 --
    totalbrut := Solde_Compte(exeordre, '274%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2974%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '274%','D', gescode, sacd)
        -Solde_Compte(exeordre-1, '2974%', 'C', gescode, sacd);
    lib := 'Prêts';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 275 et 2761  --
totalbrut := Solde_Compte(exeordre, '275%','D', gescode, sacd)
    +Solde_Compte(exeordre, '2761%','D', gescode, sacd)
    +Solde_Compte(exeordre, '2768%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '2975%', 'C', gescode, sacd)
    +Solde_Compte(exeordre, '2976%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '275%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '2761%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '2768%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '2975%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '2976%', 'C', gescode, sacd));
    lib := 'Autres immobilisations financières';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


----- ACTIF CIRCULANT ----
lib1:= 'ACTIF CIRCULANT';
---- Stocks ---
lib2:= 'STOCKS ET EN-COURS';

-- Compte 31 et 32---
    totalbrut := Solde_Compte(exeordre, '31%','D', gescode, sacd)
        +Solde_Compte(exeordre, '32%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '391%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '392%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '31%','D', gescode, sacd)
        +Solde_Compte(exeordre-1, '32%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '391%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '392%', 'C', gescode, sacd));
    lib := 'Matières premières et autres approvisionnements';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 33 et 34 ---
    totalbrut := Solde_Compte(exeordre, '33%','D', gescode, sacd)
        +Solde_Compte(exeordre, '34%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '393%', 'C', gescode, sacd)
        +Solde_Compte(exeordre, '394%', 'C',     gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '33%','D', gescode, sacd)
        +Solde_Compte(exeordre-1, '34%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '393%', 'C', gescode, sacd)
        +Solde_Compte(exeordre-1, '394%', 'C', gescode, sacd));
    lib := 'En-cours de production de biens et de services';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 35 ---
    totalbrut := Solde_Compte(exeordre, '35%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '395%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '35%','D', gescode, sacd)
        -Solde_Compte(exeordre-1, '395%', 'C', gescode, sacd);
    lib := 'Produits intermédiaires et finis';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 37 ---
    totalbrut := Solde_Compte(exeordre, '37%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '397%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '37%','D', gescode, sacd)
        -Solde_Compte(exeordre-1, '397%', 'C', gescode, sacd);
    lib := 'Marchandises';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Avances et acomptes versés sur commande ---
lib2:= 'AVANCES ET ACOMPTES';
-- Compte 4091 et 4092 ---
    totalbrut := Solde_Compte(exeordre, '4091%','D', gescode, sacd)
        +Solde_Compte(exeordre, '4092%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '4091%','D', gescode, sacd)
        +Solde_Compte(exeordre-1, '4092%','D', gescode, sacd);
    lib := 'Avances et acomptes versés sur commandes';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Créances exploitations ---
lib2:= 'CREANCES D''EXPLOITATION';

-- Compte 411, 412, 413, 416 et 418 ---
    totalbrut := Solde_Compte(exeordre, '411%','D', gescode, sacd)
        + Solde_Compte(exeordre, '412%','D', gescode, sacd)
        + Solde_Compte(exeordre, '413%','D', gescode, sacd)
        + Solde_Compte(exeordre, '416%','D', gescode, sacd)
        + Solde_Compte(exeordre, '418%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '491%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '411%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '412%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '413%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '416%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '418%','D', gescode, sacd);
    lib := 'Créances clients et comptes rattachés';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte autres ---
    totalbrut := Solde_Compte(exeordre, '4096%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4098%','D', gescode, sacd)
        + Solde_Compte(exeordre, '425%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4287%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4387%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4417%','D', gescode, sacd)

        + Solde_Compte(exeordre, '443%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4487%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4684%','D', gescode, sacd)
        + (Solde_Compte_Ext(exeordre, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
        - Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
--        + Solde_Compte(exeordre, '472%','D', gescode, sacd)
--        --- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
        + Solde_Compte(exeordre, '4735%','D', gescode, sacd)
        + Solde_Compte(exeordre, '478%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '4096%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4098%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '425%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4287%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4387%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4417%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '443%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4487%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4684%','D', gescode, sacd)
        + (Solde_Compte_Ext(exeordre-1, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
        - Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd))
--        + Solde_Compte(exeordre-1, '472%','D', gescode, sacd)
        ---Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4735%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '478%','D', gescode, sacd);
    lib := 'Autres créances d''exploitation';
    --dbms_output.put_line(lib||' '||totalbrut);
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Créances diverses ---
lib2:= 'CREANCES DIVERSES';

-- Compte TVA ---
    totalbrut := Solde_Compte(exeordre, '4456%','D', gescode, sacd)
        + Solde_Compte(exeordre, '44581%','D', gescode, sacd)
        + Solde_Compte(exeordre, '44583%','D', gescode, sacd)
        + Solde_Compte(exeordre, '44584%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '4456%','D', gescode, sacd)
    +Solde_Compte(exeordre-1, '44581%','D', gescode, sacd)
    +Solde_Compte(exeordre-1, '44583%','D', gescode, sacd)
    + Solde_Compte(exeordre-1, '44584%','D', gescode, sacd);
    lib := 'TVA';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Comptes Autres --- Spécial LR 4412 4413 !!
    totalbrut :=
        Solde_Compte(exeordre, '429%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4411%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4412%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4413%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4418%','D', gescode, sacd)
        + Solde_Compte(exeordre, '444%','D', gescode, sacd)
        + Solde_Compte(exeordre, '45%','D', gescode, sacd)
        + Solde_Compte(exeordre, '462%','D', gescode, sacd)
        + Solde_Compte(exeordre, '463%','D', gescode, sacd)
        + Solde_Compte(exeordre, '465%','D', gescode, sacd)
        + Solde_Compte(exeordre, '467%','D', gescode, sacd)
        + Solde_Compte(exeordre, '4687%','D', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '495%', 'C', gescode, sacd)
         + Solde_Compte(exeordre, '496%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (
        Solde_Compte(exeordre-1, '429%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4411%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4418%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '444%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '45%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '462%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '463%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '465%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '467%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '4687%','D', gescode, sacd))
        -(Solde_Compte(exeordre-1, '495%', 'C', gescode, sacd)
        + Solde_Compte(exeordre-1, '496%', 'C', gescode, sacd));
    lib := 'Autres créances diverses';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Trésorerie ---
lib2:= 'TRESORERIE';

-- Compte 50---
    totalbrut := Solde_Compte(exeordre, '50%','D', gescode, sacd)
        - Solde_Compte(exeordre, '509%', 'C', gescode, sacd);
    totalamort := Solde_Compte(exeordre, '590%', 'C', gescode, sacd);
    totalnet := totalbrut - totalamort;
    totalnetant := (Solde_Compte(exeordre-1, '50%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '509%', 'C', gescode, sacd))
        - Solde_Compte(exeordre-1, '590%', 'C', gescode, sacd);
    lib := 'Valeurs mobilières de placement';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte Disponibilités ---
    totalbrut := Solde_Compte(exeordre, '51%','D', gescode, sacd)
        + Solde_Compte(exeordre, '53%', 'D', gescode, sacd)
        + Solde_Compte(exeordre, '54%','D', gescode, sacd)
        - Solde_Compte(exeordre, '51%','C', gescode, sacd)
        - Solde_Compte(exeordre, '54%','C', gescode, sacd)
        + Solde_Compte(exeordre, '185%','D', gescode, sacd)
        - Solde_Compte(exeordre, '185%', 'C', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '51%','D', gescode, sacd)
        + Solde_Compte(exeordre-1, '53%', 'D', gescode, sacd)
        + Solde_Compte(exeordre-1, '54%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '51%','C', gescode, sacd)
        - Solde_Compte(exeordre-1, '54%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '185%','D', gescode, sacd)
        - Solde_Compte(exeordre-1, '185%', 'C', gescode, sacd);
    lib:= 'Disponibilités';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- CHARGES CONSTATEES D''AVANCE ---
lib2:= 'CHARGES CONSTATEES D''AVANCE';
-- Compte 486 ---
    totalbrut := Solde_Compte(exeordre, '486%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '486%','D', gescode, sacd);
    lib := 'Charges constatées d''avance';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '';

-- Compte 481---
    totalbrut := Solde_Compte(exeordre, '481%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '481%','D', gescode, sacd);
    lib := 'Charges à répartir sur plusieurs exercices';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 476 ---
    totalbrut := Solde_Compte(exeordre, '476%','D', gescode, sacd);
    totalamort := 0;
    totalnet := totalbrut - totalamort;
    totalnetant := Solde_Compte(exeordre-1, '476%','D', gescode, sacd);
    lib := 'Différences de conversion sur opérations en devises';
    INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


--*************** CREATION TABLE PASSIF *********************************
IF sacd = 'O' THEN
    DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSif sacd = 'G' then
    DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
else
    DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- CAPITAUX PROPRES ----
lib1:= 'CAPITAUX PROPRES';
---- Capital---
lib2:= 'CAPITAL ET RESERVES';

-- Compte 1021 ---
    totalnet := Solde_Compte(exeordre, '1021%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1021%','C', gescode, sacd);
    lib := 'Dotation';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1022 ---
    totalnet := Solde_Compte(exeordre, '1022%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1022%','C', gescode, sacd);
    lib := 'Complément de dotation (Etat)';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1023 ---
    totalnet := Solde_Compte(exeordre, '1023%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1023%','C', gescode, sacd);
    lib := 'Complément de dotation (autres organismes)';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1027 ---
    totalnet := Solde_Compte(exeordre, '1027%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1027%','C', gescode, sacd);
    lib := 'Affectation';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 103 ---
    totalnet := Solde_Compte(exeordre, '103%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '103%','C', gescode, sacd);
    lib := 'Biens remis en pleine propriété aux établissements';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 105 ---
    totalnet := Solde_Compte(exeordre, '105%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '105%','C', gescode, sacd);
    lib := 'Ecarts de réévaluation';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1068 ---
    totalnet := Solde_Compte(exeordre, '1068%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1068%','C', gescode, sacd);
    lib := 'Réserves';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1069 ---
    totalnet := Solde_Compte(exeordre, '1069%','D', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '1069%','D', gescode, sacd);
    lib := 'Dépréciation de l''actif';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, -totalnet, -totalnetant, gescode,exeordre);

-- Compte 110 ou 119 ---
    totalnet := Solde_Compte(exeordre, '110%','C', gescode, sacd)
        - Solde_Compte(exeordre,'119%','D', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '110%','C', gescode, sacd)
        - Solde_Compte(exeordre-1,'119%','D', gescode, sacd);
    lib := 'Report à nouveau';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 120 ou 129 ---
    totalnet := Solde_Compte(exeordre, '120%','C', gescode, sacd)
        - Solde_Compte(exeordre,'129%','D', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '120%','C', gescode, sacd)
        - Solde_Compte(exeordre-1,'129%','D', gescode, sacd);
    lib := 'Résultat de l''exercice (bénéfice ou perte)';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

-- Compte 13 --- Spécial LR 130 !
    totalnet := Solde_Compte(exeordre, '130%', 'C', gescode, sacd)
        + Solde_Compte(exeordre, '131%','C', gescode, sacd)
        + Solde_Compte(exeordre, '138%','C', gescode, sacd)
        -  Solde_Compte(exeordre, '138%','D', gescode, sacd)
        - Solde_Compte(exeordre,'139%','D', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '131%','C', gescode, sacd)
         + Solde_Compte(exeordre-1, '138%','C', gescode, sacd)
         -  Solde_Compte(exeordre-1, '138%','D', gescode, sacd)
         - Solde_Compte(exeordre-1,'139%','D', gescode, sacd);
    lib := 'Subventions d''investissement';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

----- PROVISIONS POUR RISQUES ET CHARGES ----
lib1:= 'PROVISIONS POUR RISQUES ET CHARGES';
lib2:= '  ';

-- Compte 151 ---
    totalnet := Solde_Compte(exeordre, '151%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '151%','C', gescode, sacd);
    lib := 'Provisions pour risques';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 157 et 158 ---
    totalnet := Solde_Compte(exeordre, '157%','C', gescode, sacd)
        + Solde_Compte(exeordre, '158%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '157%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '158%','C', gescode, sacd);
    lib := 'Provisions pour charges';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- DETTES ----
    lib1:= 'DETTES';
---- DETTES FINANCIERES ---
lib2:= 'DETTES FINANCIERES';

-- Compte Emprunts etab crédits ---
    totalnet := Solde_Compte(exeordre, '164%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '164%','C', gescode, sacd);
    lib := 'Emprunts auprès des établissements de crédit';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres emprunts ---
    totalnet := Solde_Compte(exeordre, '165%','C', gescode, sacd)
        + Solde_Compte(exeordre, '167%','C', gescode, sacd)
        + Solde_Compte(exeordre, '168%','C', gescode, sacd)
        + Solde_Compte(exeordre, '17%','C', gescode, sacd)
        + Solde_Compte(exeordre, '45%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '165%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '167%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '168%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '17%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '45%','C', gescode, sacd);
    lib := 'Emprunts divers';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 419 ---
    totalnet := Solde_Compte(exeordre, '4191%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4192%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '4191%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4192%','C', gescode, sacd);
    lib := 'Avances et acomptes reçus sur commandes';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

---- DETTES EXPLOITATION ---
lib2:= 'DETTES D''EXPLOITATION';

-- Compte Dettes fournisseurs ---
    totalnet := Solde_Compte(exeordre, '401%','C', gescode, sacd)
        + Solde_Compte(exeordre, '403%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4081%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4088%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '401%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '403%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4081%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4088%','C', gescode, sacd);
    lib := 'Fournisseurs et comptes rattachés';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Dettes fiscales ---
    totalnet := Solde_Compte(exeordre,'421%','C', gescode, sacd)
        + Solde_Compte(exeordre, '422%','C', gescode, sacd)
        + Solde_Compte(exeordre, '427%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4282%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4286%','C', gescode, sacd)
        + Solde_Compte(exeordre, '431%','C', gescode, sacd)
        + Solde_Compte(exeordre, '437%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4382%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4386%','C', gescode, sacd)
        + Solde_Compte(exeordre, '443%','C', gescode, sacd)
        + Solde_Compte(exeordre, '444%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4452%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4455%','C', gescode, sacd)
        + Solde_Compte(exeordre, '44584%','C', gescode, sacd)
        + Solde_Compte(exeordre, '44587%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4457%','C', gescode, sacd)
        + Solde_Compte(exeordre, '447%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4482%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4486%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1,'421%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '422%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '427%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4282%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4286%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '431%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '437%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4382%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4386%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '443%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '444%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4452%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4455%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '44584%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '44587%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4457%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '447%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4482%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4486%','C', gescode, sacd);
    lib := 'Dettes fiscales et sociales';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres dettes ---
    totalnet := Solde_Compte(exeordre, '4196%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4197%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4198%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4682%','C', gescode, sacd)
        + Solde_Compte(exeordre, '471%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4731%','C', gescode, sacd)
        --+ Solde_Compte(exeordre, '4729%', 'C', gescode, sacd)
        + Solde_Compte(exeordre, '478%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '4196%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4197%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4198%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4682%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '471%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4731%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '478%','C', gescode, sacd);
    lib := 'Autres dettes d''exploitation';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

--- DETTES diverses ---
lib2:= 'DETTES DIVERSES';

-- Compte Dettes sur immo ---
    totalnet := Solde_Compte(exeordre, '269%','C', gescode, sacd)
        + Solde_Compte(exeordre, '404%','C', gescode, sacd)
        + Solde_Compte(exeordre, '405%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4084%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '269%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '404%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '405%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4084%','C', gescode, sacd);
    lib := 'Dettes sur immobilisations et comptes rattachés';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Autres Dettes ---
    totalnet := Solde_Compte(exeordre, '429%','C', gescode, sacd)
        + Solde_Compte(exeordre, '45%','C', gescode, sacd)
        + Solde_Compte(exeordre, '464%','C', gescode, sacd)
        + Solde_Compte(exeordre, '466%','C', gescode, sacd)
        + Solde_Compte(exeordre, '467%','C', gescode, sacd)
        + Solde_Compte(exeordre, '4686%','C', gescode, sacd)
        + Solde_Compte(exeordre, '509%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '429%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '45%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '464%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '466%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '467%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '4686%','C', gescode, sacd)
        + Solde_Compte(exeordre-1, '509%','C', gescode, sacd);
    lib := 'Autres dettes diverses';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 487 ---
    totalnet := Solde_Compte(exeordre, '487%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '487%','C', gescode, sacd);
    lib := 'Produits constatés d''avance';
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '  ';

-- Compte Emprunts etab crédits ---
    totalnet := Solde_Compte(exeordre, '477%','C', gescode, sacd);
    totalnetant := Solde_Compte(exeordre-1, '477%','C', gescode, sacd);
    lib := 'Différences de conversion sur opérations en devises' ;
    INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

END;
/




