-- Attention, ce fichier est encodé en UTF-8.
-- Exécutez-le dans un environnement UTF-8
declare 
    EXEORDRE integer;
    cursor c1 is select exe_ordre from jefy_admin.exercice where exe_stat<>'C' and exe_ordre>=2010;
    cursor c2 is select utl_ordre from jefy_admin.utilisateur_fonct where fon_ordre in (select fon_ordre from jefy_admin.fonction where fon_id_interne='OUTCFI');
begin
	 OPEN c1;
           LOOP
             FETCH C1 INTO exeordre;
               EXIT WHEN c1%NOTFOUND;
              	maracuja.IMPORT_BILAN_DEFAUT(EXEORDRE);
     			MARACUJA.import_bilan_defaut_formules( EXEORDRE );
           end loop;
     close c1;
	
    commit;
    
   	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 565, 'ADBILAN', 'Administration', 'Paramétrer les formules de calcul du bilan', 'Paramétrage du bilan', 'N', 'N', 4 );
 	commit;
 	
 	-- affecter le droit à ceux qui peuvent executer le compte financier
 	insert into jefy_admin.utilisateur_fonct
		select jefy_admin.utilisateur_fonct_seq.nextval, utl_ordre, 565 from jefy_admin.utilisateur_fonct where fon_ordre in (select fon_ordre from jefy_admin.fonction where fon_id_interne='OUTCFI');
	commit;
	
	
	-- Modifs pour abricot
	jefy_admin.api_application.creerfonction(1823, 'DEP_22', 'Mandat', 'CREER BORDEREAU DE REGULARISATION DE SALAIRES PAF','CREER BORDEREAU DE REGULARISATION SALAIRES PAF', 'N', 'N', 18);

	-- Modification d'un type de bordereau auquel seront associes les mandatements de re-imputation
	update maracuja.type_bordereau set tbo_libelle = 'BORDEREAU DE REGULARISATION DE SALAIRES PAF', tbo_sous_type = 'SALAIRES PAF', tbo_type = 'BTME', tbo_type_creation = 'DEP_19', tnu_ordre = 14 where tbo_ordre = 22;

	commit;
    
    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.3.0';
    commit;
end;



