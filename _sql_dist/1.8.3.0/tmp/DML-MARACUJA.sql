﻿
SET DEFINE OFF;

execute jefy_admin.api_application.creerfonction(1823, 'DEP_22', 'Mandat', 'CREER BORDEREAU DE REGULARISATION DE SALAIRES PAF','CREER BORDEREAU DE REGULARISATION SALAIRES PAF', 'N', 'N', 18);

-- Modification d'un type de bordereau auquel seront associes les mandatements de re-imputation
update maracuja.type_bordereau set tbo_libelle = 'BORDEREAU DE REGULARISATION DE SALAIRES PAF', tbo_sous_type = 'SALAIRES PAF', tbo_type = 'BTME', tbo_type_creation = 'DEP_19', tnu_ordre = 14 where tbo_ordre = 22;


