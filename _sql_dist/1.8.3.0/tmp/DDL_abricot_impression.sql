CREATE OR REPLACE PACKAGE MARACUJA.Abricot_Impressions  IS

   -- il s'agit du dep_id de DEPENSE_budget
   FUNCTION GET_DEPENSE_CN (depid INTEGER)  RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_CONV (depid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_RECETTE_CONV (recid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_actions (depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)   RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_lbud(depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_infos(depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_RECETTE_analytiques (recid INTEGER)   RETURN VARCHAR2;
END Abricot_Impressions;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Abricot_Impressions IS


    FUNCTION GET_depense_CN (depid INTEGER)
    RETURN VARCHAR2 AS

            ceordre INTEGER;
            lescodes VARCHAR2(2000);
            lescodesdepenses VARCHAR2(2000);
            CURSOR c1 IS
            SELECT ce_ordre FROM jefy_depense.DEPENSE_CTRL_HORS_MARCHE WHERE dep_id = depid;
            BEGIN

            lescodesdepenses :=' ';
            OPEN c1;
            LOOP
            FETCH c1 INTO ceordre;
            EXIT WHEN c1%NOTFOUND;

            SELECT cm.cm_code||' '||cm_lib INTO lescodes
                        --SELECT cm.cm_code INTO lescodes
            FROM jefy_marches.CODE_EXER ce,jefy_marches.CODE_MARCHE cm
            WHERE ce.cm_ordre = cm.cm_ordre
            AND ce.ce_ordre  = ceordre;

            lescodesdepenses:=lescodesdepenses||' '||lescodes;

            END LOOP;
            CLOSE c1;


              RETURN lescodesdepenses;
    END;

    FUNCTION GET_DEPENSE_CONV (depid INTEGER)
    RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
            CURSOR c1 IS
                    SELECT conv_reference FROM jefy_depense.DEPENSE_ctrl_convention dcv, jefy_depense.v_convention c WHERE dcv.dep_id = depid AND dcv.conv_ordre = c.conv_ordre;
    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;

      FUNCTION GET_recette_CONV (recid INTEGER)
          RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT con_reference_externe
                FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE dcv.con_ordre = c.con_ordre and dcv.rec_id=rpco.rec_id
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;



    FUNCTION GET_DEPENSE_actions (depid INTEGER)
    RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
            CURSOR c1 IS
                    SELECT lolf_code FROM jefy_depense.DEPENSE_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c WHERE dca.dep_id = depid AND dca.tyac_id = c.lolf_id AND dca.exe_ordre=c.exe_ordre;
    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;

    FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)
          RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT lolf_code
                FROM jefy_recette.recette_ctrl_action rca, jefy_recette.V_LOLF_NOMENCLATURE_RECETTE l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE rca.LOLF_ID= l.lolf_id and rca.rec_id=rpco.rec_id and rca.EXE_ORDRE = l.exe_ordre
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;


        FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER)
    RETURN VARCHAR2 AS
        lesinv VARCHAR2(4000);
        leinv VARCHAR2(4000);

    CURSOR c1 IS
        SELECT ci.clic_num_complet FROM
        jefy_inventaire.inventaire_comptable ic , jefy_inventaire.CLE_INVENTAIRE_COMPTABLE ci
        where ic.clic_id = ci.clic_id
        and dpco_id = dpcoid;
        BEGIN

    lesinv := null;

     OPEN c1;
      LOOP
      FETCH c1 INTO leinv;
       EXIT WHEN c1%NOTFOUND;
        IF leinv IS NOT NULL THEN
         leinv := leinv||' ';
        END IF;
       lesinv := lesinv || leinv;
      END LOOP;
     CLOSE c1;
    RETURN lesinv;
        END;


    FUNCTION GET_DEPENSE_lbud (depid INTEGER)
    RETURN VARCHAR2 AS
           lbudlib VARCHAR2(2000);
    BEGIN


   SELECT o.org_ub || ' / ' ||org_cr || ' / ' ||org_souscr lbud into lbudlib
FROM
jefy_depense.depense_budget db,
jefy_depense.engage_budget eb,
jefy_admin.organ o
WHERE  db.dep_id = depid
and o.org_id = eb.org_id
AND eb.ENG_ID=db.ENG_ID;

        return lbudlib;


    END;

    FUNCTION GET_DEPENSE_infos (depid INTEGER)
    RETURN VARCHAR2 AS

    BEGIN


        return Abricot_Impressions.GET_depense_CN(depid)||' '||Abricot_Impressions.GET_depense_lbud(depid);


    END;
    
    FUNCTION GET_recette_analytiques (recid INTEGER)
    RETURN VARCHAR2 AS
        lescodes VARCHAR2(2000);
        coderef VARCHAR2(500);
        CURSOR c1 IS
            SELECT a.CAN_CODE
            FROM jefy_recette.recette_ctrl_analytique rca, jefy_admin.code_analytique a, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
            WHERE rca.can_id = a.can_id and rca.rec_id=rpco.rec_id and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;

    BEGIN
         lescodes := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO coderef;
                      EXIT WHEN c1%NOTFOUND;

                IF lescodes IS NOT NULL THEN
                   lescodes := lescodes||', ';
                END IF;
                lescodes := lescodes || coderef;
            END LOOP;
            CLOSE c1;

           RETURN lescodes;
    END;

END Abricot_Impressions;
/


