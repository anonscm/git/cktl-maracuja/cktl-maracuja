set define off;
CREATE OR REPLACE PROCEDURE MARACUJA.import_bilan_defaut(exeOrdre integer) as
    bpIdniveau0 integer;
    bpIdniveau1 integer;
    bpIdniveau2 integer;
    bpIdniveau3 integer;
    btId maracuja.bilan_type.bt_id%type;
BEGIN
   
   
    insert into maracuja.bilan_type (select maracuja.bilan_type_seq.nextval, 'BILAN PAR DEFAUT', 'BILAN PAR DEFAUT' from dual where not exists(select * from maracuja.bilan_type where bt_str_id='BILAN PAR DEFAUT' ));
    select bt_id into btId from maracuja.bilan_type where bt_str_id = 'BILAN PAR DEFAUT';
    delete from MARACUJA.BILAN_POSTE where exe_ordre=exeOrdre and bt_id=btid;


    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau0 from dual;
    INSERT INTO MARACUJA.BILAN_POSTE
        VALUES ( bpIdniveau0, exeordre, btId, null,   0, 1, 'actif','ACTIF', 'O', 'N',null,null);        

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 1, 'actif/actif immobilise','ACTIF IMMOBILISE', 'O', 'N', null, null); 

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/actif immobilise/immobilisations incorporelles','IMMOBILISATIONS INCORPORELLES', 'O', 'N', null, null); 

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations incorporelles/frais d''etablissement', 'Frais d''établissement', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif immobilise/immobilisations incorporelles/frais de recherche et de developpement','Frais de recherche et de développement', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations incorporelles/concessions et droits similaires','Concessions et droits similaires', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations incorporelles/droit au bail','Droit au bail', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations incorporelles/autres immobilisations incorporelles','Autres immobilisations incorporelles', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'actif/actif immobilise/immobilisations incorporelles/immobilisations incorporelles en cours','Immobilisations incorporelles en cours', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'actif/actif immobilise/immobilisations incorporelles/avances et acomptes verses sur immobilisations incorporelles','Avances et acomptes versés sur immobilisations incorporelles', 'N', 'O', null, null);                                                  
                        
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'actif/actif immobilise/immobilisations corporelles','IMMOBILISATIONS CORPORELLES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations corporelles/terrains, agencements et amenagements de terrain','Terrains, agencements et aménagements de terrain', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif immobilise/immobilisations corporelles/constructions et constructions sur sol d''autrui','Constructions et constructions sur sol d''autrui', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations corporelles/installations techniques, materiels et outillage', 'Installations techniques, matériels et outillage', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations corporelles/collections','Collections', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations corporelles/autres immobilisations corporelles','Autres immobilisations corporelles', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'actif/actif immobilise/immobilisations corporelles/immobilisations corporelles en cours','Immobilisations corporelles en cours', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'actif/actif immobilise/immobilisations corporelles/avances et acomptes verses sur commandes d''immobilisation','Avances et acomptes versés sur commandes d''immobilisation', 'N', 'O', null, null);                                                  
                        
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3, 'actif/actif immobilise/immobilisations financieres','IMMOBILISATIONS FINANCIERES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif immobilise/immobilisations financieres/participations','Participations', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2,'actif/actif immobilise/immobilisations financieres/creances rattachees a des participations', 'Créances rattachées à des participations', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif immobilise/immobilisations financieres/autres titres immobilises', 'Autres titres immobilisés', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif immobilise/immobilisations financieres/prets','Prêts', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'actif/actif immobilise/immobilisations financieres/autres immobilisations financieres','Autres immobilisations financières', 'N', 'O', null, null);                          
                                                                 
                        
    
        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 2, 'actif/actif circulant','ACTIF CIRCULANT', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/actif circulant/stocks et en-cours', 'STOCKS ET EN-COURS', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/stocks et en-cours/matieres premieres et autres approvisionnements','Matières premières et autres approvisionnements', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/stocks et en-cours/en-cours de production de biens et de services','En-cours de production de biens et de services', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif circulant/stocks et en-cours/produits intermediaires et finis','Produits intermédiaires et finis', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'actif/actif circulant/stocks et en-cours/marchandises','Marchandises', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2,'actif/actif circulant/avances et acomptes', 'AVANCES ET ACOMPTES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/avances et acomptes/avances et acomptes verses sur commandes','Avances et acomptes versés sur commandes', 'N', 'O', null, null);                          
    
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3, 'actif/actif circulant/creances d''exploitation','CREANCES D''EXPLOITATION', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/creances d''exploitation/creances clients et comptes rattaches', 'Créances clients et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/creances d''exploitation/autres creances d''exploitation','Autres créances d''exploitation', 'N', 'O', null, null);   

    
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 4,'actif/actif circulant/creances diverses', 'CREANCES DIVERSES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/creances diverses/tva', 'TVA', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/creances diverses/autres creances diverses','Autres créances diverses', 'N', 'O', null, null);   

                   select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 5, 'actif/actif circulant/tresorerie', 'TRESORERIE', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'actif/actif circulant/tresorerie/valeurs mobilieres de placement','Valeurs mobilières de placement', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'actif/actif circulant/tresorerie/disponibilites', 'Disponibilités', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'actif/actif circulant/tresorerie/charges constatees d''avance','Charges constatées d''avance', 'N', 'O', null, null);                           

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 3, 'actif/comptes de regularisation','COMPTES DE REGULARISATION', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'actif/comptes de regularisation/charges à repartir sur plusieurs exercices', 'Charges à répartir sur plusieurs exercices', 'N', 'O', null, null);  
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'actif/comptes de regularisation/differences de conversion sur operations en devises','Différences de conversion sur opérations en devises', 'N', 'O', null, null);  



----

    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau0 from dual;
    INSERT INTO MARACUJA.BILAN_POSTE 
        VALUES ( bpIdniveau0, exeordre, btId, null,   0, 2, 'passif', 'PASSIF', 'O', 'N', null, null);         

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 1,'passif/capitaux propres', 'CAPITAUX PROPRES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/capitaux propres/capital et reserves','CAPITAL ET RESERVES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/capitaux propres/capital et reserves/dotation','Dotation', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/capitaux propres/capital et reserves/complement de dotation (etat)', 'Complément de dotation (Etat)', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/capitaux propres/capital et reserves/complement de dotation (autres organismes)', 'Complément de dotation (autres organismes)', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 4, 'passif/capitaux propres/capital et reserves/affectation','Affectation', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 5, 'passif/capitaux propres/capital et reserves/biens remis en pleine propriete aux etablissements', 'Biens remis en pleine propriété aux établissements', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 6, 'passif/capitaux propres/capital et reserves/ecarts de reevaluation','Ecarts de réévaluation', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 7, 'passif/capitaux propres/capital et reserves/reserves','Réserves', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 8, 'passif/capitaux propres/capital et reserves/depreciation de l''actif', 'Dépréciation de l''actif', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 9, 'passif/capitaux propres/capital et reserves/report à nouveau','Report à nouveau', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 10, 'passif/capitaux propres/capital et reserves/resultat de l''exercice (benefice ou perte)','Résultat de l''exercice (bénéfice ou perte)', 'N', 'O', null, null);  
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 11, 'passif/capitaux propres/capital et reserves/subventions d''investissement','Subventions d''investissement', 'N', 'O', null, null);                                                                                                                                                  

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 2,'passif/provisions pour risques et charges',  'PROVISIONS POUR RISQUES ET CHARGES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/provisions pour risques et chargesprovisions pour risques', 'Provisions pour risques', 'N', 'O', null, null);  
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/provisions pour risques et chargesprovisions pour charges', 'Provisions pour charges', 'N', 'O', null, null);                      

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 3,'passif/dettes', 'DETTES', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/dettes/dettes financieres','DETTES FINANCIERES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/dettes/dettes financieres/emprunts aupres des etablissements de credit','Emprunts auprès des établissements de crédit', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes financieres/emprunts divers','Emprunts divers', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes financieres/avances et acomptes recus sur commandes', 'Avances et acomptes reçus sur commandes', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                      
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 2, 'passif/dettes/dettes d''exploitation','DETTES D''EXPLOITATION', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1, 'passif/dettes/dettes d''exploitation/fournisseurs et comptes rattaches', 'Fournisseurs et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes d''exploitation/dettes fiscales et sociales','Dettes fiscales et sociales', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes d''exploitation/autres dettes d''exploitation','Autres dettes d''exploitation', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                      
                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 3,'passif/dettes/dettes diverses', 'DETTES DIVERSES', 'O', 'N', null, null);  

                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 1,'passif/dettes/dettes diverses/dettes sur immobilisations et comptes rattaches', 'Dettes sur immobilisations et comptes rattachés', 'N', 'O', null, null);                          
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 2, 'passif/dettes/dettes diverses/autres dettes diverses','Autres dettes diverses', 'N', 'O', null, null);   
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                        INSERT INTO MARACUJA.BILAN_POSTE  VALUES ( bpIdniveau3, exeordre, btId, bpIdniveau2,   3, 3, 'passif/dettes/dettes diverses/produits constates d''avance', 'Produits constatés d''avance', 'N', 'O', null, null); 
                        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau3 from dual;
                                            

        select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau1 from dual;
        INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau1, exeordre, btId, bpIdniveau0,   1, 4, 'passif/comptes de regularisation','COMPTES DE REGULARISATION', 'O', 'N', null, null);  

                    select MARACUJA.BILAN_POSTE_SEQ.nextval into bpIdniveau2 from dual;
                    INSERT INTO MARACUJA.BILAN_POSTE VALUES ( bpIdniveau2, exeordre, btId, bpIdniveau1,   2, 1, 'passif/comptes de regularisation/differences de conversion sur operations en devises','Différences de conversion sur opérations en devises', 'N', 'O', null, null);  



END import_bilan_defaut;
/








CREATE OR REPLACE PROCEDURE MARACUJA.import_bilan_defaut_formules(exeOrdre integer) as
begin
		
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/frais d''etablissement','SD201','SC2801 + SC2831');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/frais de recherche et de developpement','SD203','SC2803 + SC2833');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/concessions et droits similaires','SD205','SC2805 + SC2835 +SC2905');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/droit au bail','SD206','SC2906');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/autres immobilisations incorporelles','SD208','SC2808 + SC2838 + SC2908');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/immobilisations incorporelles en cours','SD232','SC2932');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations incorporelles/avances et acomptes verses sur immobilisations incorporelles','SD237',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/terrains, agencements et amenagements de terrain','SD211+SD212','SC2812 + SC2842 + SC2911');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/constructions et constructions sur sol d''autrui','SD213+SD214','SC2813 + SC2814 + SC2843 + SC2844');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/installations techniques, materiels et outillage','SD215','SC2815 + SC2845');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/collections','SD216','SC2816 + SC2846');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/autres immobilisations corporelles','SD218','SC2818 + SC2848');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/immobilisations corporelles en cours','SD231','SC2931');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations corporelles/avances et acomptes verses sur commandes d''immobilisation','SD238',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/participations','SD261+SD266','SC2961 + SC2966');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/creances rattachees a des participations','SD267+SD268','SC2967 + SC2968');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/autres titres immobilises','SD271+SD272','SC2971 + SC2972');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/prets','SD274','SC2974');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif immobilise/immobilisations financieres/autres immobilisations financieres','SD275+SD2761+SD2768','SC2975 + SC2976');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/matieres premieres et autres approvisionnements','SD31+SD32','SC391 + SC392');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/en-cours de production de biens et de services','SD33+SD34','SC393 + SC394');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/produits intermediaires et finis','SD35','SC395');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/stocks et en-cours/marchandises','SD37','SC397');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/avances et acomptes/avances et acomptes verses sur commandes','SD4091+SD4092',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances d''exploitation/creances clients et comptes rattaches','SD411 + SD412 + SD413 + SD416 + SD418','SC491');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances d''exploitation/autres creances d''exploitation','SD4096 + SD4098 + SD425 + SD4287 + SD4387 + SD4417 + SD443 + SD4487 + SD4684 + SD(472-4729) - SC4729 + SD4735 + SD478',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances diverses/tva','SD4456 + SD44581 + SD44583',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/creances diverses/autres creances diverses','SD429 + SD4411 + SD4412 +SD4413 + SD4418 + SD444 + SD45 + SD462 + SD463 + SD465 + SD467 + SD4687','SC495 + SC496');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/valeurs mobilieres de placement','SD50 - SC509','SC590');
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/disponibilites','SD51 + SD53 + SD54 - SC51 - SC54 + SD185 - SC185',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/actif circulant/tresorerie/charges constatees d''avance','SD486',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/comptes de regularisation/charges à repartir sur plusieurs exercices','SD481',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'actif/comptes de regularisation/differences de conversion sur operations en devises','SD476',null);

	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/dotation','SC1021',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/complement de dotation (etat)','SC1022',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/complement de dotation (autres organismes)','SC1023',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/affectation','SC1027',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/biens remis en pleine propriete aux etablissements','SC103',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/ecarts de reevaluation','SC105',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/reserves','SC1068',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/depreciation de l''actif','SC1069',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/report à nouveau','SC110 - SD119',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/resultat de l''exercice (benefice ou perte)','SC120 - SD129',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/capitaux propres/capital et reserves/subventions d''investissement','SC130 + SC131 + SC138 - SD139',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/provisions pour risques et chargesprovisions pour risques','SC151',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/provisions pour risques et chargesprovisions pour charges','SC157 + SC158',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/emprunts aupres des etablissements de credit','SC164',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/emprunts divers','SC165 + SC167 + SC168 + SC17 + SC45',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes financieres/avances et acomptes recus sur commandes','SC4191 + SC4192',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/fournisseurs et comptes rattaches','SC401 + SC403 + SC4081 + SC4088',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/dettes fiscales et sociales','SC421 + SC422 + SC427 + SC4282 + SC4286 + SC431 + SC437 + SC4382 + SC4386 + SC443 + SC444 + SC4452 + SC4455 + SC44584 + SC44587 + SC4457 + SC447 + SC4482 + SC4486',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes d''exploitation/autres dettes d''exploitation','SC4196 + SC4197 + SC4198 + SC4682 + SC471 + SC4731 + SC478',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/dettes sur immobilisations et comptes rattaches','SC269 + SC404 + SC405 + SC4084',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/autres dettes diverses','SC429 + SC45 + SC464 + SC466 + SC467 + SC4686 + SC509',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/dettes/dettes diverses/produits constates d''avance','SC487',null);
	MARACUJA.API_BILAN.setBilanPosteFormule ( 'BILAN PAR DEFAUT', exeOrdre,'passif/comptes de regularisation/differences de conversion sur operations en devises','SC477',null);




end import_bilan_defaut_formules;
/





