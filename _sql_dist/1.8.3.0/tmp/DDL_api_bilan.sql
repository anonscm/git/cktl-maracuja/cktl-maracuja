CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,         
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type  
--        );
        
--  procedure deletePlancoBilanPostes(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type 
--         );    
--        

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER, 
--         bpStrId bilan_poste.bp_str_id%type,         
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type 
--        );
        
  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%type,
         exeordre NUMBER, 
         bpStrId bilan_poste.bp_str_id%type,         
         formuleMontant varchar2,
         formuleAmortissement  varchar2
        );        

        -- Calcule le poste pour l'exercice en fonction de la formule trouv�e 
--  FUNCTION calcPoste( 
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type, 
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      ) 
--      RETURN NUMBER;
    function getSoldeReq(formule varchar2, exeordre number, gescode VARCHAR2, sacd CHAR, vue varchar2) return varchar2;      

  FUNCTION getSoldeMontant( 
              exeOrdre bilan_poste.exe_ordre%type,
              formule varchar2,
              gesCode gestion.ges_code%type,
              isSacd char,
              faireCalcul smallint,
              sqlReq out varchar2
      ) 
      RETURN NUMBER;      
      
--    FUNCTION prv_getCritSqlForExcept( 
--              compte VARCHAR2, 
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;    
    function prv_getCritSqlForSingleFormule(formule varchar2) return varchar2 ;

--  FUNCTION getFormule( 
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type, 
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type      
--      ) 
--      RETURN varchar2;
      
   procedure prepare_bilan(btId bilan_poste.bt_id%type, exeOrdre bilan_poste.exe_ordre%type, gescode VARCHAR2, sacd CHAR);
   procedure prepare_bilan_actif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR);
   procedure prepare_bilan_passif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR);
   procedure checkFormule(formule varchar2);

   procedure duplicateExercice(exeordreOld integer, exeordreNew integer);
   procedure prv_duplicateExercice(exeordreNew integer, racineOld bilan_poste.bp_id%type, racineNew bilan_poste.bp_id%type);

END API_BILAN;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%type,
         exeordre NUMBER, 
         bpStrId bilan_poste.bp_str_id%type,         
         formuleMontant varchar2,
         formuleAmortissement  varchar2
  ) is
    btId bilan_type.bt_id%type;
  begin
    select bt_id into btId from bilan_type where bt_str_id=btstrid;
    update bilan_poste set BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement where bt_id=btId and exe_ordre=exeOrdre and bp_str_id=bpStrId;
  end setBilanPosteFormule;
  
  
    function getSoldeReq(formule varchar2, exeordre number, gescode VARCHAR2, sacd CHAR, vue varchar2) return varchar2 is
    signe varchar2(1);
    signeNum integer;
    sens varchar2(2);
    compteext varchar2(2000);
    critStrPlus varchar2(2000);
    res varchar2(4000);
    LC$req varchar2(4000);
    lavue varchar2(50);
   begin
        res := '';
        if (formule is not null and length(formule)>0) then
             dbms_output.put_line('traitement de ='|| formule);
            signe := substr(formule,1,1); -- +
            signeNum := 1;
            if (signe='-') then
                signeNum := -1;
            end if;
            sens :=  substr(formule,2,2); -- SD
            if (sens is null or sens not in ('SD','SC')) then
                 raise_application_error (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            end if;
            compteext := substr(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            if (length(compteext)=0) then
                raise_application_error (-20001, 'compte non recupere dans la formule '|| formule);
            end if;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';
            
            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('sacd='||sacd);
            
            lavue := vue;
            if lavue is null then
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
            end if;

           IF sens = 'SD'   THEN
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;   
               ELSE
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           ELSE
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' then
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        end if;
        return res;
   end getSoldeReq;
  
    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant( 
              exeOrdre bilan_poste.exe_ordre%type,
              formule varchar2,
              gesCode gestion.ges_code%type,
              isSacd char,
              faireCalcul smallint,
              sqlReq out varchar2
      ) 
      RETURN NUMBER is
   
    str varchar2(4000);
    compteext varchar2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos integer;
    nb integer;
    i integer;
    j integer;
    nextS integer;
    signe varchar2(10);
    sens varchar2(10);
    pconum plan_comptable_exer.pco_num%type;
    subdiv plan_comptable_exer.pco_num%type;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction varchar2(255);
    --critStrPlus varchar2(2000);
    resTmp number;
    signeNum number;
    sInstructionReelle varchar2(4000);
    req varchar2(4000);
    solde number;
  begin
    /* une formule est compos�es de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */   
   
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := replace(formule, ' ', '');
    
    if (length(str)=0) then
     raise_application_error (-20001, 'formule vide' );
    end if;
    
    str := upper(str);
    
    if (substr(str,1,1) = 'S') then
        str := '+'||str; 
    end if;

    str := replace(str, '+S', '|+S');
    str := replace(str, '-S', '|-S');
    str := substr(str,2);
dbms_output.put_line('Formule = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := substr(sinstruction,1,1); -- +
        signeNum := 1; 
        if (signe='-') then
            signeNum := -1; 
        end if;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures');
        dbms_output.put_line('REQ = '|| req); 
        if ( nvl(faireCalcul, 1)=1) then       
            EXECUTE IMMEDIATE req INTO solde ;
        end if;
        
        if (solde is null) then
            solde := 0;
        end if;
        IF solde < 0
           THEN solde := 0;
        END IF;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;    
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    return resTmp;           
    end getSoldeMontant;
    
    
    
  
  
    function prv_getCritSqlForSingleFormule(formule varchar2) return varchar2 is
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum varchar2(2000);
            critStrMoins varchar2(2000);
            critStrPlus varchar2(2000);
            str varchar2(2000);
            pconumTmp varchar2(2000);
            subdiv varchar2(1000);
    begin
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les �ventuelles parenthese de debut / fin
            if (substr(str,1,1) = '(') then
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               if (substr(str,length(str),1) <> ')' ) then
                raise_application_error (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (substr(str,length(str),1)) || ': '|| formule);
               end if; 
               str := substr(str,2,length(str)-2);
             end if;
            
            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                if (substr(pconum,1,1)='(' ) then
                    if (substr(pconum,length(pconum),1) <> ')' ) then
                        raise_application_error (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (substr(pconum,length(pconum),1)) || ': '|| formule);
                    end if;
                    pconum := substr(pconum,2,length(pconum)-2);
                    if (instr(pconum,'(' )>0) then
                        raise_application_error (-20001, 'erreur dans la formule, parenthese non g�r�e ici :'|| pconum || ': '|| formule);
                    end if;
                end if;
                if (instr(pconum,')' )>0) then
                    raise_application_error (-20001, 'erreur dans la formule, parenthese non g�r�e ici :'|| pconum || ': '|| formule);
                end if;                    
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   if (j=1) then
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' '; 
                            dbms_output.put_line(pconumTmp);
                   else
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            if (length(subdiv)>0) then
                                if (subdiv not like pconumTmp||'%') then
                                    raise_application_error (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autoris�es : '|| pconum);
                                end if;
                               
                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';                           
                            end if;
                   end if; 
                end loop;
                if (length(critStrMoins)>0) then
                    critStrMoins := '(' || critStrMoins ||')'; 
                    if (length(critStrPlus)>0) then
                        critStrPlus := critStrPlus || ' or ';   
                    end if;  
                    critStrPlus := critStrPlus || critStrMoins;
                end if;
                    
            end loop;
            
            return critStrPlus;
                   
    end;
  
  
 procedure prepare_bilan_actif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR) as
    bp_id0 bilan_poste.bp_id%type;
    bp_id1 bilan_poste.bp_id%type;
    bp_id2 bilan_poste.bp_id%type;
    rec_bp1 bilan_poste%rowtype;
    rec_bp2 bilan_poste%rowtype;
    rec_bp3 bilan_poste%rowtype;
    bilan_groupe1 varchar2(1000);
    bilan_groupe2 varchar2(1000);
    bilan_libelle varchar2(1000);
    bilan_montant number;
     bilan_amort number;
    bpStrId bilan_poste.bp_str_id%type;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%type;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%type;
    cursor carbre is 
            select *
            from bilan_poste
            where exe_ordre=exeOrdre
            connect by prior BP_ID = bp_id_pere
            start with BP_id_pere in (select BP_ID from bilan_poste where bp_id_pere is null and bp_str_id=bpStrId);
            
    cursor c1 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = bp_id0 order by bp_ordre;
    cursor c2 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp1.bp_id order by bp_ordre;
    cursor c3 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp2.bp_id order by bp_ordre;          
    sqlreq varchar2(4000);
   flag integer;
   
   begin
        bpStrId := 'actif';
        
        if (exeordre is null) then
            raise_application_error (-20001, ' exeOrdre obligatoire');
        end if;
        
        if (btId is null) then
            raise_application_error (-20001, ' btId obligatoire');
        end if;  
        if (gesCode is null) then
            raise_application_error (-20001, ' gesCode obligatoire');
        end if; 
        if (isSacd is null) then
            raise_application_error (-20001, ' isSacd obligatoire');
        end if;                         
        select count(*) into flag from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        if (flag=0) then
             raise_application_error (-20001, bpStrId || ' non param�tr� dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        end if;
        
        select bp_id into bp_id0 from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        
--*************** CREATION TABLE ACTIF *********************************
        IF issacd = 'O' THEN
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSif issacd = 'G' then
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        else
            DELETE from comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;
        
         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;
                
                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                       
                     if (rec_bp2.bp_groupe='N') then
                        bilan_groupe2 := null;   
                        bilan_libelle := rec_bp2.bp_libelle;
                        formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                        formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                        bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                        bilan_amort := 0;
                        if (length(trim(formuleAmortissement)) >0  )   then            
                            bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                        end if;
                        INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, gescode,exeordre);
                      else
                         bilan_groupe2 := rec_bp2.bp_libelle;   
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                bilan_libelle := rec_bp3.bp_libelle; 
                                formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                                  gesCode,
                                                  isSacd,
                                                  1,
                                                  sqlReq);
                                bilan_amort := 0;
                                if (length(trim(formuleAmortissement)) >0  )   then            
                                    bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,      
                                                  gesCode,
                                                  isSacd,
                                                  1,
                                                  sqlReq);
                                end if;            
                                INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, gescode,exeordre);
                               
                          END LOOP;                
                          CLOSE c3;                        

                     end if;  
                       
                  END LOOP;                
                  CLOSE c2; 
               
          END LOOP;                
          CLOSE c1;          
        
   end;
   
   
    procedure prepare_bilan_passif(btId bilan_poste.bt_id%type,exeOrdre  bilan_poste.exe_ordre%type, gescode VARCHAR2, isSacd CHAR) as
        bp_id0 bilan_poste.bp_id%type;
        bp_id1 bilan_poste.bp_id%type;
        bp_id2 bilan_poste.bp_id%type;
        rec_bp1 bilan_poste%rowtype;
        rec_bp2 bilan_poste%rowtype;
        rec_bp3 bilan_poste%rowtype;
        bilan_groupe1 varchar2(1000);
        bilan_groupe2 varchar2(1000);
        bilan_libelle varchar2(1000);
        bilan_montant number;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%type;
        bpStrId bilan_poste.bp_str_id%type;
        cursor carbre is 
                select *
                from bilan_poste
                where exe_ordre=exeOrdre
                connect by prior BP_ID = bp_id_pere
                start with BP_id_pere in (select BP_ID from bilan_poste where bp_id_pere is null and bt_id=btId and bp_str_id=bpStrId);
                
        cursor c1 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = bp_id0 order by bp_ordre;
        cursor c2 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp1.bp_id order by bp_ordre;
        cursor c3 is select * from bilan_poste where exe_ordre=exeOrdre and bp_id_pere = rec_bp2.bp_id order by bp_ordre;          
        sqlreq varchar2(4000);
        flag integer;
   
   
   begin
        bpStrId := 'passif';
        if (exeordre is null) then
            raise_application_error (-20001, ' exeOrdre obligatoire');
        end if;
        
        if (btId is null) then
            raise_application_error (-20001, ' btId obligatoire');
        end if;  
        if (gesCode is null) then
            raise_application_error (-20001, ' gesCode obligatoire');
        end if; 
        if (isSacd is null) then
            raise_application_error (-20001, ' isSacd obligatoire');
        end if;          
        
        select count(*) into flag from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId and bt_id=btId;
        if (flag=0) then
             raise_application_error (-20001, bpStrId || ' non param�tr� dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        end if;
        select bp_id into bp_id0 from bilan_poste where exe_ordre=exeordre and bp_str_id=bpStrId;
        IF issacd = 'O' THEN
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSif issacd = 'G' then
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        else
            DELETE from comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;
        
         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;
                
                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                       
                     if (rec_bp2.bp_groupe='N') then
                        bilan_groupe2 := null;   
                        bilan_libelle := rec_bp2.bp_libelle;
                        formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                       -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                        bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlreq);
                      
                       
                        INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                      else
                         bilan_groupe2 := rec_bp2.bp_libelle;   
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                bilan_libelle := rec_bp3.bp_libelle; 
                                formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                       -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                bilan_montant := getSoldeMontant(exeordre, formuleMontant,      
                                          gesCode,
                                          isSacd,
                                          1,
                                          sqlReq);
                      
                                 
                                INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                               
                          END LOOP;                
                          CLOSE c3;                        

                     end if;  
                       
                  END LOOP;                
                  CLOSE c2; 
               
          END LOOP;                
          CLOSE c1;          
        
   end;   
   
    procedure prepare_bilan(btId bilan_poste.bt_id%type,exeOrdre bilan_poste.exe_ordre%type, gescode VARCHAR2, sacd CHAR) is
    begin
         prepare_bilan_actif(btId, exeOrdre, gescode, sacd) ;
         prepare_bilan_passif(btId, exeOrdre, gescode, sacd) ;
    end;
   
   
 

    procedure checkFormule(formule varchar2) is
        sqlReq varchar2(4000);
        res number;
    begin
        res := getSoldeMontant(2010, formule, 'AGREGE','G', 0,sqlReq );
        
        return;
    end checkFormule;
   
   
   procedure duplicateExercice(exeordreOld integer, exeordreNew integer) is
        rec_bp bilan_poste%rowtype;
        cursor c1 is select * from bilan_poste where exe_ordre=exeordreOld and BP_ID_PERE is null;
        bpIdNew bilan_poste.bp_id%type;
        flag integer;
        
   begin
        select count(*) into flag from bilan_poste where exe_ordre=exeordreNew and BP_ID_PERE is null;
        if (flag >0) then
            raise_application_error (-20001, 'Bilan deja cree pour   : '|| exeordreNew);        
        end if;
   
   
       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            select bilan_poste_seq.nextval into bpIdNew from dual;
               
             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID, 
                   EXE_ORDRE, 
                   BT_ID, 
                   BP_ID_PERE, 
                   BP_NIVEAU, 
                   BP_ORDRE, 
                   BP_STR_ID, 
                   BP_LIBELLE, 
                   BP_GROUPE, 
                   BP_MODIFIABLE, 
                   BP_FORMULE_MONTANT, 
                   BP_FORMULE_AMORTISSEMENT                   
                   ) 
                VALUES (
                    bpIdNew, 
                   exeOrdreNew, --EXE_ORDRE, 
                   rec_bp.bt_id, --BT_ID, 
                   null, --BP_ID_PERE, 
                   rec_bp.bp_niveau, --BP_NIVEAU, 
                   rec_bp.bp_ordre, --BP_ORDRE, 
                   rec_bp.bp_str_id, --BP_STR_ID, 
                   rec_bp.bp_libelle, --BP_LIBELLE, 
                   rec_bp.bp_groupe, --BP_GROUPE, 
                   rec_bp.bp_modifiable, --BP_MODIFIABLE, 
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT, 
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   ); 
           
            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        end loop;
        close c1;
   end;
   
   procedure prv_duplicateExercice(exeordreNew integer, racineOld bilan_poste.bp_id%type, racineNew bilan_poste.bp_id%type) is
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%rowtype;
        bpIdNew bilan_poste.bp_id%type;
       
        cursor c1 is select * from bilan_poste where bp_id_Pere = racineOld;
   begin
        
        
   
   
       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             select bilan_poste_seq.nextval into bpIdNew from dual;
            
               
             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID, 
                   EXE_ORDRE, 
                   BT_ID, 
                   BP_ID_PERE, 
                   BP_NIVEAU, 
                   BP_ORDRE, 
                   BP_STR_ID, 
                   BP_LIBELLE, 
                   BP_GROUPE, 
                   BP_MODIFIABLE, 
                   BP_FORMULE_MONTANT, 
                   BP_FORMULE_AMORTISSEMENT                   
                   ) 
                VALUES (
                    bpIdNew, 
                   exeOrdreNew, --EXE_ORDRE, 
                   rec_bp.bt_id, --BT_ID, 
                   racineNew, --BP_ID_PERE, 
                   rec_bp.bp_niveau, --BP_NIVEAU, 
                   rec_bp.bp_ordre, --BP_ORDRE, 
                   rec_bp.bp_str_id, --BP_STR_ID, 
                   rec_bp.bp_libelle, --BP_LIBELLE, 
                   rec_bp.bp_groupe, --BP_GROUPE, 
                   rec_bp.bp_modifiable, --BP_MODIFIABLE, 
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT, 
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);
               
               
           end loop;
       close c1;
        
   end duplicateExercice;
   
   
END API_BILAN;
/


