SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.4
-- Date de publication : 05/04/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Ajout fonction pour impressions Abricot
-- Ajout vue pour Abricot (v_edition_mandat_origine)
-- DT #4390 - Correction vue v_recette_recouvrer
-- Modification vue des ribs (suite changement gestion rib<->banque)
-- Modification procedure d'analyse des incohérences sur les données
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;
--
-- DB_VERSION
--
-- Rem : test de l'existence de la version précédente
declare
cpt integer;
begin
	select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.5';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
end;
/

exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.1.3', 'MARACUJA' );
exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.1.4', null );
commit;



CREATE OR REPLACE VIEW MARACUJA.V_EDITION_MANDAT_ORIGINE
(EXE_ORDRE, GES_CODE, MAN_ID, MAN_NUMERO, DEP_ID, DEP_MONTANT_BUDGETAIRE, 
BOR_NUM_ORIGINE, MAN_NUM_ORIGINE, MAN_ID_ORIGINE, 
DEP_ID_ORIGINE, DEP_MONTANT_BUDG_ORIGINE)
AS
SELECT m1.exe_ordre, m1.ges_code, m1.man_id, m1.man_numero, jd1.dep_id, jd1.dep_montant_budgetaire,
b.BOR_NUM MAN_BORD_ORIGINE, m2.man_numero MAN_NUMERO_ORIGINE, m2.man_id MAN_ID_ORIGINE, jd2.dep_id DEP_ID_ORIGINE, 
jd2.dep_montant_budgetaire DEP_MONTANT_BUDG_ORIGINE
FROM 
jefy_depense.depense_ctrl_planco dcp1, jefy_depense.depense_ctrl_planco dcp2, jefy_depense.depense_budget jd1,
jefy_depense.depense_budget jd2, maracuja.mandat m1, maracuja.mandat m2, maracuja.BORDEREAU b 
WHERE jd1.dep_id_reversement = jd2.dep_id
AND jd1.dep_id=dcp1.dep_id 
AND jd2.dep_id=dcp2.dep_id 
AND dcp1.man_id = m1.man_id 
AND dcp2.man_id = m2.man_id 
AND m2.BOR_ID = b.bor_id ;
/


create or replace force view maracuja.v_recette_reste_recouvrer (rec_id, reste_recouvrer)
as
   select   r.rec_id,
            sum (ecd_reste_emarger) as reste_recouvrer
   from     recette r, titre_detail_ecriture tde, ecriture_detail ecd, jefy_admin.exercice e
   where    r.rec_id = tde.rec_id and tde.ecd_ordre = ecd.ecd_ordre and rec_mont >= 0 and ecd.ecd_credit = 0 and ecd.ecd_debit <> 0 and ecd.exe_ordre = e.exe_ordre and e.exe_stat = 'O' and substr (ecd.pco_num, 1, 1) = '4'
   group by r.rec_id
   union all
   select   r.rec_id,
            sum (ecd_reste_emarger) as reste_recouvrer
   from     recette r, titre_detail_ecriture tde, ecriture_detail ecd, jefy_admin.exercice e
   where    r.rec_id = tde.rec_id
   and      tde.ecd_ordre = ecd.ecd_ordre
   and      rec_mont >= 0
   and      ecd.ecd_credit = 0
   and      ecd.ecd_debit <> 0
   and      ecd.exe_ordre = e.exe_ordre
   and      (e.exe_stat = 'R' or e.exe_stat = 'C')
   and      substr (ecd.pco_num, 1, 1) = '4'
   and      not exists (select tde_ordre
                        from   titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe
                        where  xx.ecd_ordre = xecd.ecd_ordre and xecd.exe_ordre = xe.exe_ordre and xe.exe_stat = 'O' and xx.tit_id = r.tit_id)
   group by r.rec_id
   union all
   select   r.rec_id,
            -sum (ecd_reste_emarger) as reste_recouvrer
   from     recette r, titre_detail_ecriture tde, ecriture_detail ecd, jefy_admin.exercice e
   where    r.rec_id = tde.rec_id and tde.ecd_ordre = ecd.ecd_ordre and rec_mont < 0 and ecd.ecd_debit = 0 and ecd.ecd_credit <> 0 and ecd.exe_ordre = e.exe_ordre and e.exe_stat = 'O' and substr (ecd.pco_num, 1, 1) = '4'
   group by r.rec_id
   union all
   select   r.rec_id,
            sum (ecd_reste_emarger) as reste_recouvrer
   from     recette r, titre_detail_ecriture tde, ecriture_detail ecd, jefy_admin.exercice e
   where    r.rec_id = tde.rec_id
   and      tde.ecd_ordre = ecd.ecd_ordre
   and      rec_mont < 0
   and      ecd.ecd_debit = 0
   and      ecd.ecd_credit <> 0
   and      ecd.exe_ordre = e.exe_ordre
   and      (e.exe_stat = 'R' or e.exe_stat = 'C')
   and      substr (ecd.pco_num, 1, 1) = '4'
   and      not exists (select tde_ordre
                        from   titre_detail_ecriture xx, ecriture_detail xecd, jefy_admin.exercice xe
                        where  xx.ecd_ordre = xecd.ecd_ordre and xecd.exe_ordre = xe.exe_ordre and xe.exe_stat = 'O' and xx.tit_id = r.tit_id)
   group by r.rec_id;
/



CREATE OR REPLACE VIEW MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 CLE_RIB, DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, 
 PERS_TYPE, PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, 
 ORG_UB, ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, 
 TCD_LIBELLE, DPP_NUMERO_FACTURE, DPP_ID, TAP_TAUX, DPP_HT_SAISIE, 
 DPP_TVA_SAISIE, DPP_TTC_SAISIE, DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, 
 DPP_DATE_SERVICE_FAIT, DPP_NB_PIECE, UTL_ORDRE, TBO_ORDRE, DEP_ID, 
 PCO_NUM, DPCO_ID, DPCO_MONTANT_BUDGETAIRE, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, 
 ADR_CIVILITE, ADR_NOM, ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_VILLE, ADR_CP, UTLNOMPRENOM)
AS 
SELECT bq.c_banque, bq.c_guichet, r.no_compte, r.iban, bq.bic, r.cle_rib,
          bq.domiciliation, mp.mod_libelle, mp.mod_code, mp.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, eb.org_id,
          o.org_ub, o.org_cr, o.org_souscr, eb.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, dp.dpp_numero_facture, dp.dpp_id, tp.tap_taux, dp.dpp_ht_saisie,
          dp.dpp_tva_saisie, dp.dpp_ttc_saisie, dp.dpp_date_facture,
          dp.dpp_date_saisie, dp.dpp_date_reception, dp.dpp_date_service_fait,
          dp.dpp_nb_piece, dp.utl_ordre, dcp.tbo_ordre, dcp.dep_id dep_id,
          dcp.pco_num, dcp.dpco_id dpco_id, dcp.dpco_montant_budgetaire, dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie, dcp.dpco_ttc_saisie, f.adr_civilite, f.adr_nom,
          f.adr_prenom, f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
     FROM jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o,
          jefy_admin.taux_prorata tp                                               --,
--jefy_admin.type_credit tcd
   WHERE  eb.eng_id = db.eng_id
      AND db.dpp_id = dp.dpp_id
      AND db.dep_id = dcp.dep_id
      AND db.tap_id = tp.tap_id
      AND dp.fou_ordre = f.fou_ordre
      AND dp.exe_ordre = e.exe_ordre
      AND dp.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND dp.rib_ordre = r.rib_ordre(+)
      AND r.banq_ordre = bq.banq_ordre(+)
      AND mp.mod_ordre = dp.mod_ordre
      AND eb.tcd_ordre = tc.tcd_ordre
      AND o.org_id = eb.org_id
      AND u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
      AND man_id IS NULL
      AND tbo_ordre != 201;
/


CREATE OR REPLACE PACKAGE MARACUJA.abricot_impressions
is
   -- il s'agit du dep_id de DEPENSE_budget
   function get_depense_cn (depid integer)
      return varchar2;

   function get_depense_conv (depid integer)
      return varchar2;

   function get_recette_conv (recid integer)
      return varchar2;

   function get_depense_actions (depid integer)
      return varchar2;

   function get_recette_actions (recid integer)
      return varchar2;

   function get_depense_inventaire (dpcoid integer)
      return varchar2;

   function get_depense_lbud (depid integer)
      return varchar2;

   function get_depense_infos (depid integer)
      return varchar2;

   function get_recette_analytiques (recid integer)
      return varchar2;

   function get_depense_analytiques (depid integer)
      return varchar2;
end abricot_impressions;
/


GRANT EXECUTE ON MARACUJA.ABRICOT_IMPRESSIONS TO JEFY_DEPENSE;


CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_impressions
is
   function get_depense_cn (depid integer)
      return varchar2
   as
      ceordre            integer;
      lescodes           varchar2 (2000);
      lescodesdepenses   varchar2 (2000);

      cursor c1
      is
         select ce_ordre
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      lescodesdepenses := ' ';

      open c1;

      loop
         fetch c1
         into  ceordre;

         exit when c1%notfound;

         select cm.cm_code || ' ' || cm_lib
         into   lescodes
         --SELECT cm.cm_code INTO lescodes
         from   jefy_marches.code_exer ce, jefy_marches.code_marche cm
         where  ce.cm_ordre = cm.cm_ordre and ce.ce_ordre = ceordre;

         lescodesdepenses := lescodesdepenses || ' ' || lescodes;
      end loop;

      close c1;

      return lescodesdepenses;
   end;

   function get_depense_conv (depid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         select conv_reference
         from   jefy_depense.depense_ctrl_convention dcv, jefy_depense.v_convention c
         where  dcv.dep_id = depid and dcv.conv_ordre = c.conv_ordre;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_recette_conv (recid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select con_reference_externe
         from   jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  dcv.con_ordre = c.con_ordre and dcv.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_depense_actions (depid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);

      cursor c1
      is
         select lolf_code
         from   jefy_depense.depense_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c
         where  dca.dep_id = depid and dca.tyac_id = c.lolf_id and dca.exe_ordre = c.exe_ordre;
   begin
      leslolf := null;

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_recette_actions (recid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select lolf_code
         from   jefy_recette.recette_ctrl_action rca, jefy_recette.v_lolf_nomenclature_recette l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.lolf_id = l.lolf_id and rca.rec_id = rpco.rec_id and rca.exe_ordre = l.exe_ordre and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      leslolf := null;

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_depense_inventaire (dpcoid integer)
      return varchar2
   as
      lesinv   varchar2 (4000);
      leinv    varchar2 (4000);

      cursor c1
      is
         select ci.clic_num_complet
         from   jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable ci
         where  ic.clic_id = ci.clic_id and dpco_id = dpcoid;
   begin
      lesinv := null;

      open c1;

      loop
         fetch c1
         into  leinv;

         exit when c1%notfound;

         if leinv is not null then
            leinv := leinv || ' ';
         end if;

         lesinv := lesinv || leinv;
      end loop;

      close c1;

      return lesinv;
   end;

   function get_depense_lbud (depid integer)
      return varchar2
   as
      lbudlib   varchar2 (2000);
   begin
      select o.org_ub || ' / ' || org_cr || ' / ' || org_souscr lbud
      into   lbudlib
      from   jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.organ o
      where  db.dep_id = depid and o.org_id = eb.org_id and eb.eng_id = db.eng_id;

      return lbudlib;
   end;

   function get_depense_infos (depid integer)
      return varchar2
   as
   begin
      return abricot_impressions.get_depense_cn (depid) || ' ' || abricot_impressions.get_depense_lbud (depid);
   end;

   function get_recette_analytiques (recid integer)
      return varchar2
   as
      lescodes   varchar2 (2000);
      coderef    varchar2 (500);

      cursor c1
      is
         select a.can_code
         from   jefy_recette.recette_ctrl_analytique rca, jefy_admin.code_analytique a, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.can_id = a.can_id and rca.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lescodes := null;

      open c1;

      loop
         fetch c1
         into  coderef;

         exit when c1%notfound;

         if lescodes is not null then
            lescodes := lescodes || ', ';
         end if;

         lescodes := lescodes || coderef;
      end loop;

      close c1;

      return lescodes;
   end;

   function get_depense_analytiques (depid integer)
      return varchar2
   as
      lescanal    varchar2 (2000);
      canalcode   varchar2 (500);

      cursor c1
      is
         select can_code
         from   jefy_depense.depense_ctrl_analytique dca, jefy_admin.v_code_analytique ca
         where  dca.dep_id = depid and dca.can_id = ca.can_id and dca.exe_ordre = ca.exe_ordre;
   begin
      lescanal := null;

      open c1;

      loop
         fetch c1
         into  canalcode;

         exit when c1%notfound;

         if lescanal is not null then
            lescanal := lescanal || ', ';
         end if;

         lescanal := lescanal || canalcode;
      end loop;

      close c1;

      return lescanal;
   end;
end abricot_impressions;
/


GRANT EXECUTE ON MARACUJA.ABRICOT_IMPRESSIONS TO JEFY_DEPENSE;


create or replace force view maracuja.v_rib (rib_ordre, fou_ordre, rib_bic, rib_cle, rib_codbanc, rib_guich, rib_iban, rib_domicil, rib_num, rib_titco, rib_valide, pers_id_creation, pers_id_modification)
as
   select r.rib_ordre,
          r.fou_ordre,
          b.bic,
          r.cle_rib,
          r.c_banque,
          r.c_guichet,
          r.iban,
          b.domiciliation,
          r.no_compte,
          r.rib_titco,
          r.rib_valide,
          r.pers_id_creation,
          r.pers_id_modification
   from   grhum.ribfour_ulr r, grhum.banque b
   where  r.banq_ordre = b.banq_ordre;
/



create or replace force view maracuja.v_abricot_papaye_attente (exercice, mois, org_ub, nb_attente, montant, tbo_ordre)
as
   select   e.exe_exercice exercice,
            dp.dpp_numero_facture mois,
            o.org_ub,
            count (*) nb_attente,
            sum (dcp.dpco_ttc_saisie) montant,
            tbo_ordre
   from     jefy_depense.engage_budget eb,
            jefy_depense.depense_budget db,
            jefy_depense.depense_papier dp,
            jefy_depense.depense_ctrl_planco dcp,
            grhum.v_fournis_grhum f,
            jefy_admin.exercice e,
            jefy_admin.utilisateur u,
            grhum.personne p,
            grhum.personne pu,
            grhum.ribfour_ulr r,
            grhum.banque bq,
            maracuja.mode_paiement mp,
            jefy_admin.type_credit tc,
            jefy_admin.organ o   --,
--jefy_admin.type_credit tcd
   where    eb.eng_id = db.eng_id
   and      db.dpp_id = dp.dpp_id
   and      db.dep_id = dcp.dep_id
   and      dp.fou_ordre = f.fou_ordre
   and      dp.exe_ordre = e.exe_ordre
   and      dp.utl_ordre = u.utl_ordre
   and      p.pers_id = u.pers_id
   and      dp.rib_ordre = r.rib_ordre(+)
   and      r.banq_ordre = bq.banq_ordre(+)
   and      mp.mod_ordre = dp.mod_ordre
   and      eb.tcd_ordre = tc.tcd_ordre
   and      o.org_id = eb.org_id
   and      u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
   and      man_id is null
   and      tbo_ordre = 3
   group by e.exe_exercice, dp.dpp_numero_facture, o.org_ub, tbo_ordre
   union all
   select   e.exe_exercice exercice,
            dp.dpp_numero_facture mois,
            o.org_ub,
            count (*) nb_attente,
            sum (dcp.dpco_ttc_saisie) montant,
            tbo_ordre
   from     jefy_depense.engage_budget eb,
            jefy_depense.depense_budget db,
            jefy_depense.depense_papier dp,
            jefy_depense.depense_ctrl_planco dcp,
            grhum.v_fournis_grhum f,
            jefy_admin.exercice e,
            jefy_admin.utilisateur u,
            grhum.personne p,
            grhum.personne pu,
            grhum.ribfour_ulr r,
            grhum.banque bq,
            maracuja.mode_paiement mp,
            jefy_admin.type_credit tc,
            jefy_admin.organ o   --,
--jefy_admin.type_credit tcd
   where    eb.eng_id = db.eng_id
   and      db.dpp_id = dp.dpp_id
   and      db.dep_id = dcp.dep_id
   and      dp.fou_ordre = f.fou_ordre
   and      dp.exe_ordre = e.exe_ordre
   and      dp.utl_ordre = u.utl_ordre
   and      p.pers_id = u.pers_id
   and      dp.rib_ordre = r.rib_ordre(+)
   and      r.banq_ordre = bq.banq_ordre(+)
   and      mp.mod_ordre = dp.mod_ordre
   and      eb.tcd_ordre = tc.tcd_ordre
   and      o.org_id = eb.org_id
   and      u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
   and      man_id is null
   and      tbo_ordre = 20
   group by e.exe_exercice, dp.dpp_numero_facture, o.org_ub, tbo_ordre;
/


create or replace force view maracuja.v_abricot_recette_pi (c_banque,
                                                            c_guichet,
                                                            no_compte,
                                                            iban,
                                                            bic,
                                                            domiciliation,
                                                            mod_libelle,
                                                            mod_code,
                                                            mod_dom,
                                                            pers_type,
                                                            pers_libelle,
                                                            pers_lc,
                                                            exe_exercice,
                                                            org_id,
                                                            org_ub,
                                                            org_cr,
                                                            org_souscr,
                                                            tcd_ordre,
                                                            tcd_code,
                                                            tcd_libelle,
                                                            fac_numero,
                                                            rec_id,
                                                            rec_ht_saisie,
                                                            rec_tva_saisie,
                                                            rec_ttc_saisie,
                                                            fac_date_saisie,
                                                            rec_nb_piece,
                                                            utl_ordre,
                                                            tbo_ordre,
                                                            pco_num,
                                                            rpco_ht_saisie,
                                                            rpco_id,
                                                            tva,
                                                            ttc,
                                                            adr_civilite,
                                                            adr_nom,
                                                            adr_prenom,
                                                            adr_adresse1,
                                                            adr_adresse2,
                                                            adr_ville,
                                                            adr_cp,
                                                            utlnomprenom
                                                           )
as
   select r.c_banque,
          r.c_guichet,
          r.no_compte,
          r.iban,
          bq.bic,
          bq.domiciliation,
          mr.mod_libelle,
          mr.mod_code,
          mr.mod_dom,
          p.pers_type,
          p.pers_libelle,
          p.pers_lc,
          e.exe_exercice,
          fac.org_id,
          o.org_ub,
          o.org_cr,
          o.org_souscr,
          fac.tcd_ordre,
          tc.tcd_code,
          tc.tcd_libelle,
          fac.fac_numero,
          rec.rec_id,
          rec.rec_ht_saisie,
          rec.rec_tva_saisie,
          rec.rec_ttc_saisie,
          fac.fac_date_saisie,
          0 rec_nb_piece,
          rec.utl_ordre,
          rcp.tbo_ordre,
--rcp.rec_id+tcd.tcd_ordre rec_id,
          rcp.pco_num,
          rcp.rpco_ht_saisie,
          rcp.rpco_id,
          rcp.rpco_tva_saisie tva,
          rcp.rpco_ttc_saisie ttc,
          f.adr_civilite,
          f.adr_nom,
          f.adr_prenom,
          f.adr_adresse1,
          f.adr_adresse2,
          f.adr_ville,
          f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
   from   jefy_recette.facture fac,
          jefy_recette.recette_budget rec,
          jefy_recette.recette_papier recp,
          jefy_recette.recette_ctrl_planco rcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_recouvrement mr,
          jefy_admin.type_credit tc,
          jefy_admin.organ o
   where  fac.fac_id = rec.fac_id
   and    rec.rpp_id = recp.rpp_id
   and    rec.rec_id = rcp.rec_id
   and    fac.fou_ordre = f.fou_ordre
   and    rec.exe_ordre = e.exe_ordre
   and    fac.utl_ordre = u.utl_ordre
   and    p.pers_id = u.pers_id
   and    recp.rib_ordre = r.rib_ordre(+)
   and    r.banq_ordre = bq.banq_ordre(+)
   and    mr.mod_ordre = recp.mor_ordre
   and    fac.tcd_ordre = tc.tcd_ordre
   and    o.org_id = fac.org_id
   and    u.pers_id = pu.pers_id
   and    rcp.tit_id is null
   and    tbo_ordre = 200;
/


create or replace force view maracuja.v_abricot_depense_pi (c_banque,
                                                            c_guichet,
                                                            no_compte,
                                                            iban,
                                                            bic,
                                                            domiciliation,
                                                            mod_libelle,
                                                            mod_code,
                                                            mod_dom,
                                                            pers_type,
                                                            pers_libelle,
                                                            pers_lc,
                                                            exe_exercice,
                                                            org_id,
                                                            org_ub,
                                                            org_cr,
                                                            org_souscr,
                                                            tcd_ordre,
                                                            tcd_code,
                                                            tcd_libelle,
                                                            dpp_numero_facture,
                                                            dpp_id,
                                                            dpp_ht_saisie,
                                                            dpp_tva_saisie,
                                                            dpp_ttc_saisie,
                                                            dpp_date_facture,
                                                            dpp_date_saisie,
                                                            dpp_date_reception,
                                                            dpp_date_service_fait,
                                                            dpp_nb_piece,
                                                            utl_ordre,
                                                            tbo_ordre,
                                                            dep_id,
                                                            pco_num,
                                                            dpco_id,
                                                            dpco_ht_saisie,
                                                            dpco_tva_saisie,
                                                            dpco_ttc_saisie,
                                                            adr_civilite,
                                                            adr_nom,
                                                            adr_prenom,
                                                            adr_adresse1,
                                                            adr_adresse2,
                                                            adr_ville,
                                                            adr_cp,
                                                            utlnomprenom
                                                           )
as
   select r.c_banque,
          r.c_guichet,
          r.no_compte,
          r.iban,
          bq.bic,
          bq.domiciliation,
          mp.mod_libelle,
          mp.mod_code,
          mp.mod_dom,
          p.pers_type,
          p.pers_libelle,
          p.pers_lc,
          e.exe_exercice,
          eb.org_id,
          o.org_ub,
          o.org_cr,
          o.org_souscr,
          eb.tcd_ordre,
          tc.tcd_code,
          tc.tcd_libelle,
          dp.dpp_numero_facture,
          dp.dpp_id,
          dp.dpp_ht_saisie,
          dp.dpp_tva_saisie,
          dp.dpp_ttc_saisie,
          dp.dpp_date_facture,
          dp.dpp_date_saisie,
          dp.dpp_date_reception,
          dp.dpp_date_service_fait,
          dp.dpp_nb_piece,
          dp.utl_ordre,
          dcp.tbo_ordre,
          dcp.dep_id dep_id,
          dcp.pco_num,
          dcp.dpco_id,
          dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie,
          dcp.dpco_ttc_saisie,
          f.adr_civilite,
          f.adr_nom,
          f.adr_prenom,
          f.adr_adresse1,
          f.adr_adresse2,
          f.adr_ville,
          f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
   from   jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o   --,
--jefy_admin.type_credit tcd
   where  eb.eng_id = db.eng_id
   and    db.dpp_id = dp.dpp_id
   and    db.dep_id = dcp.dep_id
   and    dp.fou_ordre = f.fou_ordre
   and    dp.exe_ordre = e.exe_ordre
   and    dp.utl_ordre = u.utl_ordre
   and    p.pers_id = u.pers_id
   and    dp.rib_ordre = r.rib_ordre(+)
   and    r.banq_ordre = bq.banq_ordre(+)
   and    mp.mod_ordre = dp.mod_ordre
   and    eb.tcd_ordre = tc.tcd_ordre
   and    o.org_id = eb.org_id
   and    u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
   and    man_id is null
   and    tbo_ordre = 201;
/

create or replace force view maracuja.v_abricot_recette_a_titrer (c_banque,
                                                                  c_guichet,
                                                                  no_compte,
                                                                  iban,
                                                                  bic,
                                                                  domiciliation,
                                                                  mod_libelle,
                                                                  mod_code,
                                                                  mod_dom,
                                                                  pers_type,
                                                                  pers_libelle,
                                                                  pers_lc,
                                                                  exe_exercice,
                                                                  org_id,
                                                                  org_ub,
                                                                  org_cr,
                                                                  org_souscr,
                                                                  tcd_ordre,
                                                                  tcd_code,
                                                                  tcd_libelle,
                                                                  fac_numero,
                                                                  rec_numero,
                                                                  fap_numero,
                                                                  rec_id,
                                                                  rec_ht_saisie,
                                                                  rec_tva_saisie,
                                                                  rec_ttc_saisie,
                                                                  fac_date_saisie,
                                                                  rec_nb_piece,
                                                                  utl_ordre,
                                                                  tbo_ordre,
                                                                  pco_num,
                                                                  rpco_id,
                                                                  rpco_ht_saisie,
                                                                  tva,
                                                                  ttc,
                                                                  adr_civilite,
                                                                  adr_nom,
                                                                  adr_prenom,
                                                                  adr_adresse1,
                                                                  adr_adresse2,
                                                                  adr_ville,
                                                                  adr_cp,
                                                                  utlnomprenom
                                                                 )
as
   select r.c_banque,
          r.c_guichet,
          r.no_compte,
          r.iban,
          bq.bic,
          bq.domiciliation,
          mr.mod_libelle,
          mr.mod_code,
          mr.mod_dom,
          p.pers_type,
          p.pers_libelle,
          p.pers_lc,
          e.exe_exercice,
          fac.org_id,
          o.org_ub,
          o.org_cr,
          o.org_souscr,
          fac.tcd_ordre,
          tc.tcd_code,
          tc.tcd_libelle,
          fac.fac_numero,
          jrec.rec_numero,
          fp.fap_numero,
          rec.rec_id,
          rec.rec_ht_saisie,
          rec.rec_tva_saisie,
          rec.rec_ttc_saisie,
          fac.fac_date_saisie,
          0 rec_nb_piece,
          rec.utl_ordre,
          rcp.tbo_ordre,
          rcp.pco_num,
          rcp.rpco_id,
          rcp.rpco_ht_saisie,
          rcp.rpco_tva_saisie tva,
          rcp.rpco_ttc_saisie ttc,
          f.adr_civilite,
          f.adr_nom,
          f.adr_prenom,
          f.adr_adresse1,
          f.adr_adresse2,
          f.adr_ville,
          f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
   from   jefy_recette.facture fac,
          jefy_recette.facture_papier fp,
          jefy_recette.recette jrec,
          jefy_recette.recette_budget rec,
          jefy_recette.recette_papier recp,
          jefy_recette.recette_ctrl_planco rcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_recouvrement mr,
          jefy_admin.type_credit tc,
          jefy_admin.organ o
   where  fac.fac_id = rec.fac_id
   and    rec.rpp_id = recp.rpp_id
   and    rec.rec_id = rcp.rec_id
   and    rec.rec_id = jrec.rec_id
   and    fac.fou_ordre = f.fou_ordre
   and    rec.exe_ordre = e.exe_ordre
   and    fac.utl_ordre = u.utl_ordre
   and    p.pers_id = u.pers_id
   and    recp.rib_ordre = r.rib_ordre(+)
   and    r.banq_ordre = bq.banq_ordre(+)
   and    mr.mod_ordre = recp.mor_ordre
   and    fac.tcd_ordre = tc.tcd_ordre
   and    o.org_id = fac.org_id
   and    u.pers_id = pu.pers_id
   and    rcp.tit_id is null
   and    tbo_ordre != 200
   and    fac.fac_id = fp.fac_id(+);
/

create or replace force view maracuja.v_abricot_depense_a_mandater (c_banque,
                                                                    c_guichet,
                                                                    no_compte,
                                                                    iban,
                                                                    bic,
                                                                    cle_rib,
                                                                    domiciliation,
                                                                    mod_libelle,
                                                                    mod_code,
                                                                    mod_dom,
                                                                    pers_type,
                                                                    pers_libelle,
                                                                    pers_lc,
                                                                    exe_exercice,
                                                                    org_id,
                                                                    org_ub,
                                                                    org_cr,
                                                                    org_souscr,
                                                                    tcd_ordre,
                                                                    tcd_code,
                                                                    tcd_libelle,
                                                                    dpp_numero_facture,
                                                                    dpp_id,
                                                                    tap_taux,
                                                                    dpp_ht_saisie,
                                                                    dpp_tva_saisie,
                                                                    dpp_ttc_saisie,
                                                                    dpp_date_facture,
                                                                    dpp_date_saisie,
                                                                    dpp_date_reception,
                                                                    dpp_date_service_fait,
                                                                    dpp_nb_piece,
                                                                    utl_ordre,
                                                                    tbo_ordre,
                                                                    dep_id,
                                                                    pco_num,
                                                                    dpco_id,
                                                                    dpco_montant_budgetaire,
                                                                    dpco_ht_saisie,
                                                                    dpco_tva_saisie,
                                                                    dpco_ttc_saisie,
                                                                    adr_civilite,
                                                                    adr_nom,
                                                                    adr_prenom,
                                                                    adr_adresse1,
                                                                    adr_adresse2,
                                                                    adr_ville,
                                                                    adr_cp,
                                                                    utlnomprenom
                                                                   )
as
   select r.c_banque,
          r.c_guichet,
          r.no_compte,
          r.iban,
          bq.bic,
          r.cle_rib,
          bq.domiciliation,
          mp.mod_libelle,
          mp.mod_code,
          mp.mod_dom,
          p.pers_type,
          p.pers_libelle,
          p.pers_lc,
          e.exe_exercice,
          eb.org_id,
          o.org_ub,
          o.org_cr,
          o.org_souscr,
          eb.tcd_ordre,
          tc.tcd_code,
          tc.tcd_libelle,
          dp.dpp_numero_facture,
          dp.dpp_id,
          tp.tap_taux,
          dp.dpp_ht_saisie,
          dp.dpp_tva_saisie,
          dp.dpp_ttc_saisie,
          dp.dpp_date_facture,
          dp.dpp_date_saisie,
          dp.dpp_date_reception,
          dp.dpp_date_service_fait,
          dp.dpp_nb_piece,
          dp.utl_ordre,
          dcp.tbo_ordre,
          dcp.dep_id dep_id,
          dcp.pco_num,
          dcp.dpco_id dpco_id,
          dcp.dpco_montant_budgetaire,
          dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie,
          dcp.dpco_ttc_saisie,
          f.adr_civilite,
          f.adr_nom,
          f.adr_prenom,
          f.adr_adresse1,
          f.adr_adresse2,
          f.adr_ville,
          f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom
   from   jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.ribfour_ulr r,
          grhum.banque bq,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o,
          jefy_admin.taux_prorata tp   --,
--jefy_admin.type_credit tcd
   where  eb.eng_id = db.eng_id
   and    db.dpp_id = dp.dpp_id
   and    db.dep_id = dcp.dep_id
   and    db.tap_id = tp.tap_id
   and    dp.fou_ordre = f.fou_ordre
   and    dp.exe_ordre = e.exe_ordre
   and    dp.utl_ordre = u.utl_ordre
   and    p.pers_id = u.pers_id
   and    dp.rib_ordre = r.rib_ordre(+)
   and    r.banq_ordre = bq.banq_ordre(+)
   and    mp.mod_ordre = dp.mod_ordre
   and    eb.tcd_ordre = tc.tcd_ordre
   and    o.org_id = eb.org_id
   and    u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
   and    man_id is null
   and    tbo_ordre != 201;
/

GRANT SELECT ON MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER TO JEFY_DEPENSE;





CREATE OR REPLACE PACKAGE MARACUJA."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco2(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    procedure checkCadre2VsBalance(exeOrdre INTEGER);
    procedure checkEcritureDetailGestion(exeordre integer); 
    procedure checkMandatVsJefyDepense(exeordre integer); 
    procedure checkMontantsEmargements(exeOrdre integer);
    procedure checkEcritureDates(exeordre integer);
    procedure checkAllProblemes(exeOrdre integer);
    
    
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  objet maracuja.mandat%ROWTYPE;
  manid mandat.man_id%type;
  manttc mandat.man_ttc%type;
  ecddebit ecriture_detail.ecd_debit%type;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and 
    (
    (man_ETAT   not in ('VISE','PAYE', 'ANNULE') and bor_etat  in ('VISE', 'PAYE', 'PAIEMENT'))
    or
    (man_ETAT   not in ('ANNULE') and brj_ordre is not null)
    )
    ;
    
    
    cursor c2 is
        select m.* from mandat m, mandat_detail_ecriture mde, ecriture_detail ecd, mode_paiement mp  
            where m.man_id=mde.man_id and m.exe_ordre=2011 and mde_origine='VISA' 
            and m.mod_ordre=mp.mod_ordre and m.pai_ordre is null
            and mp.MOD_DOM='VIREMENT'
            and mde.ecd_ordre=ecd.ecd_ordre
            and abs(ecd_reste_emarger)<>abs(ecd_montant)
            and man_ht>0;
            
            
     cursor c3 is 
        SELECT m.man_id, man_ttc, SUM(ecd_debit) 
        FROM mandat m, mandat_detail_ecriture mde, ecriture_detail ecd 
        WHERE m.man_id=mde.man_id AND mde.ecd_ordre=ecd.ecd_ordre
        AND man_ttc>=0
        AND m.exe_ordre=exeordre
        AND mde_origine LIKE 'VISA%'
        GROUP BY m.man_id, man_ttc
        HAVING man_ttc<> SUM(ecd_debit);
            
            
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau ('|| borEtat ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') ou bien mandat sur un bordereau de rejet et non ANNULE';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de données : remettre la bonne valeur dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
  
   OPEN C2;
  LOOP
    FETCH c2 INTO objet;
    EXIT
  WHEN c2%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'La contrepartie de prise en charge du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') a été émargée alors que le mode de paiement est de type VIREMENT.';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Provoque un message d''erreur lors de la création d''un paiement (écritures déséquilibrées)', -- ZAP_CONSEQUENCE
        'Vous devez supprimer l''émargement puis modifier le mode de paiement du mandat si le mandat a réellement été payé autrement que par virement via Maracuja.'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c2;
  
  
    OPEN C3;
  LOOP
    FETCH c3 INTO manid, manttc, ecddebit ;
    EXIT
  WHEN c3%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau b, mandat m
    where b.bor_id=m.bor_id and m.man_id=manid;
    
    probleme := 'Le montant de l''écriture de visa du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||
    ') n''est pas cohérent avec le montant du mandat (man: '||manttc|| ' / ecr: '||ecddebit||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Incohérence comptabilité ordonnateur/comptable. Impossible de générer un paiement', -- ZAP_CONSEQUENCE
        'Intervention dans la base de données pour régulariser (suppression de l''écriture en trop si le problème vient de là) .'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c3;
  
  
  
  
END;

PROCEDURE checkMandatVsJefyDepense
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  pco_num_depense jefy_depense.depense_ctrl_planco.PCO_NUM%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b, 
      jefy_depense.depense_ctrl_planco dpco
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and m.man_id=dpco.man_id
    and dpco.pco_num <> m.pco_num
   
    ;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    select pco_num into pco_num_depense from jefy_depense.depense_ctrl_planco where man_id = objet.man_id;
    
    probleme := 'Imputation du mandat (' || objet.pco_num || ') incoherente avec l''imputation  de la depense côté budgétaire ('|| pco_num_depense ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Une raison possible est qu''il y a eu une réimputation comptable du mandat qui n''a pas été réimpactée budgétairement. Il est nécessaire d''intervenir dans la base de données pour le man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDates(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture
    where exe_ordre=exeOrdre
    and to_char(ecr_date_saisie,'yyyy')<>to_char(exeOrdre) ;
BEGIN
  categorie := 'ECRITURES';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'La date de saisie de l''écriture est incohérente avec l''exercice (date '|| lecritureDetail.ecr_date_saisie ||' / code gestion ' || lecritureDetail.exe_ordre ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE.ECR_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecr_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Corriger le champ ecr_date_saisie dans la table maracuja.ecriture ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDetailGestion(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code not in
      (select ges_code
      from maracuja.gestion_exercice
      where exe_ordre              =exeOrdre
      );
BEGIN
  categorie := 'CODES GESTION';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un code gestion non actif sur l''exercice (numero '|| ecrNumero ||' / code gestion ' || lecritureDetail.ges_code ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Activer le code gestion sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco2
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   not in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre      
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte supprimé sur '|| exeOrdre || '(numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;

procedure checkCadre2VsBalance(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vgescode gestion.ges_code%type;
  vpconum plan_comptable_exer.pco_num%type;
  vbaldebit number;
  vbalCredit number;
  vmandate number;
  vreverse number;
  vsolde number;
  vnet  number;
  

    cursor c1 is 
        select ges_code, pco_num,  sum(bal_debit) bal_debit, sum(bal_credit) bal_credit,  sum(bal_solde) bal_solde , sum(mandate) mandate, sum(reverse) reverse, sum(montant_net) montant_net
        from (
        select ges_code, ecd.pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '6%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, substr(ecd.pco_num,3,10) pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '186%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, pco_num, 0,0,0,sum(MANDATS), sum(reversements), sum(montant_net)
        from COMPTEFI.V_DVLOP_DEP
        where exe_ordre=exeOrdre
        and pco_num like '6%'
        group by ges_code, pco_num
        )
        group by ges_code, pco_num
        having (sum(bal_debit)<>sum(mandate) or sum(bal_credit)<> sum(reverse))
        order by ges_code, pco_num;

begin
    categorie     := 'COMPTE FINANCIER';
    sousCategorie := 'DIFFERENCE BALANCE/CADRE 2';
  OPEN C1;
  LOOP
    FETCH c1 INTO vgescode ,vpconum ,vbaldebit,vbalCredit,vSolde,vmandate, vreverse, vnet;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Code gestion: ' || vgescode|| ' Compte: ' || vPcoNum || ' : Balance <> cadre 2 (balance : D='||vBalDebit ||', C='|| vBalCredit ||') (Cadre 2 :  Mandats='||vMandate ||', ORV='|| vReverse ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          'Normal si certain bordereaux ne sont pas encore visés. Sinon incoherence dans les tables.',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;



procedure checkMontantsEmargements(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vecd_ordre maracuja.ecriture_detail.ecd_ordre%type;
  vecr_numero maracuja.ecriture.ecr_numero%type;
  vecd_index maracuja.ecriture_detail.ecd_index%type;
  vecd_montant maracuja.ecriture_detail.ecd_montant%type;
  vecd_reste_emarge maracuja.ecriture_detail.ecd_reste_emarger%type;
  vemd_montant maracuja.emargement_detail.emd_montant%type;
  vcalcreste maracuja.ecriture_detail.ecd_reste_emarger%type;
  vpco_num maracuja.ecriture_detail.pco_num%type;

    cursor c1 is       
        select ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger , sum(emd_montant), abs(ecd_montant)-sum(emd_montant) 
        from (
            select distinct * from 
            (select ed.* from emargement_detail ed, emargement ema where ema.ema_ordre=ed.ema_ordre and ema_etat='VALIDE') emd, 
            ecriture_detail ecd ,
            ecriture ecr
            where (ecd_ordre=ecd_ordre_source or ecd_ordre = ecd_ordre_destination)
            and ecd.exe_ordre=exeOrdre
            and ecd.ecr_ordre=ecr.ecr_ordre
        ) 
        group by ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger
        having abs(ecd_montant)-sum(emd_montant)<> ecd_reste_emarger;

begin
    categorie     := 'ECRITURES';
    sousCategorie := 'MONTANTS EMARGEMENTS';
  OPEN C1;
  LOOP
    FETCH c1 INTO vecd_ordre, vpco_num, vecr_numero, vecd_index, vecd_montant , vecd_reste_emarge, vemd_montant, vcalcreste;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Ecriture : ' || vecr_numero || '/'||  vecd_index ||', Compte: ' || vpco_num || ' : Montant du reste à émarger (='|| vecd_reste_emarge ||') différent du reste à émarger calculé (='||vcalcreste ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les états de développement de solde', -- ZAP_CONSEQUENCE
          'Corrigez le reste à émarger de l''écriture (Bouton disponible dans l''écran de consultation des écritures de Maracuja).',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;


procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
   checkMandatVsJefyDepense(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkEcritureDetailPlanco2(exeordre);
  checkEcritureDetailGestion(exeordre); 
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
  checkCadre2VsBalance(exeOrdre);
  checkMontantsEmargements(exeOrdre);
  
end;
END;
/







create or replace procedure grhum.inst_patch_maracuja_1914 is
begin
	

	JEFY_ADMIN.PATCH_UTIL.end_PATCH ( 4, '1.9.1.4');       
end;
/












