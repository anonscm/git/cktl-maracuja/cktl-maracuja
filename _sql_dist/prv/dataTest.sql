-- tests
declare 
	borid number;
	broid number;
begin
select MARACUJA.BORDEREAU_seq.nextval into borid from dual;
select MARACUJA.brouillard_seq.nextval into broid from dual;

INSERT INTO MARACUJA.BORDEREAU (
   BOR_DATE_VISA, BOR_ETAT, BOR_ID, 
   BOR_NUM, BOR_ORDRE, EXE_ORDRE, 
   GES_CODE, TBO_ORDRE, UTL_ORDRE, 
   UTL_ORDRE_VISA, BOR_DATE_CREATION) 
VALUES (
   null, 'VALIDE',borid , 
   1, 1, 2011, 
   900, 400, 173, 
   null, sysdate);
   
   
INSERT INTO MARACUJA.BROUILLARD (
   BRO_ID, EXE_ORDRE, COM_ORDRE, 
   BRO_ETAT, BRO_LIBELLE, BRO_POSTIT, 
   ORI_ORDRE, TJO_ORDRE, TOP_ORDRE, 
   PERS_ID_CREATION, DATE_CREATION, BRO_MOTIF_REJET, 
   BRO_DATE_VISA, PERS_ID_VISA,
   BOR_ID, BRO_NUMERO) 
VALUES ( 
   broid, 
   2011, 
   1, 
   'ATTENTE', 
   'Test de demande de prise en charge de convention RA1', 
   'id convention', 
   null, 
   1, 
   3, 
   61495, 
   sysdate, 
   null, 
   null, 
   null, 
   borid,
   0);   
   
   
   
   INSERT INTO MARACUJA.BROUILLARD_DETAIL (
   BROD_ID, BRO_ID, BROD_INDEX, 
   GES_CODE, BROD_PCO_NUM, BROD_PCO_LIBELLE, 
   BROD_DEBIT, BROD_CREDIT, BROD_SENS, 
   BROD_LIBELLE, BROD_POSTIT, BROD_COMMENTAIRE, 
   ECD_ORDRE) 
VALUES ( 
    MARACUJA.BROUILLARD_DETAIL_seq.nextval, 
    broid,
    1, --BROD_INDEX, 
   '900', --GES_CODE, 
   '4465111', 
   'Test plan comptable', 
   12, 
   0, 
   'D', 
   'Test conv RA', 
   'postit', 
   'BROD_COMMENTAIRE', 
   null
   );
   
   
   INSERT INTO MARACUJA.BROUILLARD_DETAIL (
   BROD_ID, BRO_ID, BROD_INDEX, 
   GES_CODE, BROD_PCO_NUM, BROD_PCO_LIBELLE, 
   BROD_DEBIT, BROD_CREDIT, BROD_SENS, 
   BROD_LIBELLE, BROD_POSTIT, BROD_COMMENTAIRE, 
   ECD_ORDRE) 
VALUES ( 
    MARACUJA.BROUILLARD_DETAIL_seq.nextval, 
    broid,
    2, --BROD_INDEX, 
   '900', --GES_CODE, 
   '4465112', 
   'Test plan comptable 2', 
   0, 
   12, 
   'C', 
   'Test conv RA', 
   'postit', 
   'BROD_COMMENTAIRE', 
   null
   );
   
   
   commit;
   
   end;
   