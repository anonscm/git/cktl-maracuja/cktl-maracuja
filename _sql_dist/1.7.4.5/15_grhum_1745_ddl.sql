SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.api_planco
IS


    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire              VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE;


    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco (
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE;

   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE;
      
      function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type;    
        
        
    procedure valider_Planco( exeordre number,pconum varchar, valide varchar);
    
    /* Creer un compte avec toutes les caracteristiques d'un autre */
    procedure creer_Planco_From_ref(
            exeordre           INTEGER,    
            pconumNew              VARCHAR,
            pcolibelle          VARCHAR,
            pconumRef           varchar);     
            
    procedure set_sens_solde(exeordre integer, pconum varchar, sensSolde varchar);             
    
    /** Affecter tous les types de credit tcdsect */
    procedure affecte_type_credit(exeordre integer, pconum varchar, tcdSect varchar, plaquoi varchar);
    
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.api_planco IS


    /** Creer un compte de prestation interne a partir du compte budgetaire */
    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE
    is
        flag integer;
        plancoexer plan_comptable_exer%rowtype; 
    begin
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        if (flag=0) then
            return null;
        end if;
        
        select * into plancoexer from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        
        return creer_planco(exeordre,    
            '18'||pconumBudgetaire,
            plancoexer.pco_libelle,
            plancoexer.pco_budgetaire,
            plancoexer.pco_emargement,
            plancoexer.pco_nature,
            plancoexer.pco_sens_emargement,
            plancoexer.pco_validite,
            plancoexer.pco_j_exercice,
            plancoexer.pco_j_fin_exercice,
            plancoexer.pco_j_be);
         
    end;

    /** creer un enregistrement dans plan comptable (plan_comptable_exer + plan_comptable)  */
    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE
    is
        res plan_comptable.pco_num%TYPE;
    begin
        res := prv_fn_creer_planco_exer(exeordre, pconum,pcolibelle,pcobudgetaire,pcoemargement,pconature,pcosensemargement,pcovalidite,pcojexercice,pcojfinexercice,pcojbe);
        return res;
    end;

----------------------------------------------

   FUNCTION prv_fn_creer_planco (pconum VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE
   IS
      flag   INTEGER;
      pconiveau integer;
   BEGIN
      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable
       WHERE pco_num = pconum;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable
                  (pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite,                   --PCO_VALIDITE,
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

      RETURN pconum;
   END;
   
   -----------------------------------------------------
   
   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
   RETURN plan_comptable_exer.pco_num%TYPE
    IS
      flag   INTEGER;
      pconiveau integer;
      res plan_comptable_exer.pco_num%TYPE;
   BEGIN
        

      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable_exer
       WHERE pco_num = pconum and exe_ordre=exeordre;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable_exer
                  (pcoe_id, exe_ordre, pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (plan_comptable_exer_seq.nextval, 
                   exeordre, 
                   pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite, --PCO_VALIDITE
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

        -- creer le compte dans plan comptable s'il n'existe pas
       res := prv_fn_creer_planco(pconum,
                    pcolibelle,
                    pcobudgetaire,
                    pcoemargement,
                    pconature,
                    pcosensemargement,
                    pcovalidite,
                    pcojexercice,
                    pcojfinexercice,
                    pcojbe);        
      RETURN pconum;
   END;
   
    function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type
    is
        res plan_comptable_exer.pco_libelle%type;
    begin
        select pco_libelle into res from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        return res;
    end;   
   
   
   
   
       procedure valider_Planco( exeordre number,pconum varchar, valide varchar)
       is
       begin
            if (valide='O' or valide='VALIDE') then
                 update plan_comptable_exer set pco_validite='VALIDE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
            if (valide='N' or valide='ANNULE') then
                 update plan_comptable_exer set pco_validite='ANNULE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
       
           
       end;
    
    /* Creer un compte avec toutes les caracteristiques d'un autre (planco_visa, planco_credit) */
    procedure creer_Planco_From_ref(
            exeordre           INTEGER,    
            pconumNew              VARCHAR,
            pcolibelle          VARCHAR,
            pconumRef           varchar)
      is
        pcoRefExiste integer;
        pcoNewExiste integer;
        plancovisaNewexiste integer;
        plancocreditNewexiste integer;
        planconew plan_comptable_exer.PCO_NUM%type;
        pcoref plan_comptable_exer%rowtype;
      begin
                pcoRefExiste := 0;
                pcoNewExiste := 0;
    
                -- verifier si le compte de reference existe
                select count(*) into pcoRefExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
    
                -- verifier si le nouveau compte existe
                select count(*) into pcoNewExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumNew;

    
                -- si le nouveau compte existe et s'il est invalide, on le valide
                -- si existe pas, on le cree 
                if (pcoNewExiste >0) then
                    valider_planco(pconumNew, exeordre, 'O');
                else
                    if (pcoRefExiste>0) then
                        select * into pcoref from plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
                    
                        planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    pcoref.pco_budgetaire,
                                    pcoref.pco_emargement,
                                    pcoref.pco_nature,
                                    pcoref.pco_sens_emargement,
                                    pcoref.pco_validite,
                                    pcoref.pco_j_exercice,
                                    pcoref.pco_j_fin_exercice,
                                    pcoref.pco_j_be);                                
                    else
                       planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    'O',
                                    'N',
                                    'D',
                                    '0',
                                    'VALIDE',
                                    'N',
                                    'N',
                                    'N');                        
                    end if;
                
                end if;
    
    
                -- dupliquer les donnees sur les tables peripheriques
                  -- planco_visa
                select count(*) into plancovisaNewexiste from planco_visa where pco_num_ordonnateur = pconumNew and exe_ordre=exeordre;
                if (plancovisaNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_VISA (
                                    PVI_ORDRE,
                                    EXE_ORDRE,
                                    PCO_NUM_ORDONNATEUR, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION 
                                    ) 
                            select  planco_visa_seq.nextval,
                                    exeordre,
                                    pconumNew, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION from planco_visa where pco_num_ordonnateur = pconumRef and exe_ordre=exeordre;                    
                end if;
                
                
                
                -- planco_credit                
                select count(*) into plancocreditNewexiste from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num = pconumNew and tc.exe_ordre = exeordre;
                if (plancocreditNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_CREDIT (
                           PCC_ORDRE, 
                           PCO_NUM, 
                           TCD_ORDRE,                            
                           PLA_QUOI, 
                           PCC_ETAT) 
                        select PLANCO_CREDIT_seq.nextval, 
                                pconumNew, 
                               tc.TCD_ORDRE,                                
                               PLA_QUOI, 
                               PCC_ETAT from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num=pconumRef and tc.exe_ordre = exeordre;                    
                end if;                
                
      
      end;
   
   
    procedure set_sens_solde(exeordre integer, pconum varchar, sensSolde varchar)
    is
    begin
        update plan_comptable_exer set PCO_SENS_SOLDE=sensSolde where pco_num=pconum and exe_ordre=exeordre;
    end;
 

    procedure affecte_type_credit(exeordre integer, pconum varchar, tcdSect varchar, plaquoi varchar)
    is
        flag integer;
     begin
        select count(*) into flag from type_credit where exe_ordre=exeordre and tcd_Sect=tcdsect;
        if (flag=0) then
            raise_application_error (-20001,'tcd_sect '|| tcdsect || ' non trouve pour exercice '||exeordre);
        end if;
        
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        if (flag=0) then
            raise_application_error (-20001,'pco_num '|| pconum || ' non trouve pour exercice '||exeordre);
        end if;
        
        
        DELETE FROM MARACUJA.PLANCO_CREDIT where pco_num=pconum and tcd_ordre in (select tcd_ordre from type_credit where exe_ordre=exeordre and tcd_sect=tcdsect); 
        
        INSERT INTO MARACUJA.PLANCO_CREDIT (
                    PCC_ORDRE, 
                    TCD_ORDRE, 
                    PCO_NUM, 
                    PLA_QUOI, 
                    PCC_ETAT) 
                select  
                    PLANCO_CREDIT_seq.nextval, 
                    TCD_ORDRE, 
                    pconum, 
                    plaquoi, 
                    'VALIDE'
                    from type_credit where exe_ordre = exeordre and tcd_sect = tcdsect; 

        
        
     end;    
   
END;
/


